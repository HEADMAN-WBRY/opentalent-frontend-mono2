import { Box } from '@mui/material';
import React from 'react';

import Typography from '../typography';
import Accordion from './Accordion';

export default {
  title: 'Components/Accordion',
  component: Accordion,
};

export const Default = () => {
  return (
    <>
      <Box mb={4}>
        <Accordion
          summary={<Typography variant="body1">Title</Typography>}
          details={<Typography variant="caption">Details</Typography>}
        />
      </Box>
    </>
  );
};
