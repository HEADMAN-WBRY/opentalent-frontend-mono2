import { Box, Card, CardContent } from '@mui/material';
import React from 'react';

import Spoiler from './Spoiler';

export default {
  title: 'Components/Spoiler',
  component: Spoiler,
};

export const Default = () => {
  return (
    <Box width={300}>
      <Card>
        <CardContent>
          <Spoiler>
            I’m with broad technical skill-set, very strong attention to detail,
            and 7 years of experience in front-end web development. Able to
            multitask and juggle multiple pressing projects simultaneously. I’m
            with broad technical skill-set, very strong attention to detail, and
            7 years of experience in front-end web development. Able to
            multitask and juggle multiple pressing projects simultaneously.
          </Spoiler>
        </CardContent>
      </Card>
    </Box>
  );
};
