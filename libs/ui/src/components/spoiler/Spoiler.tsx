/* eslint-disable jsx-a11y/click-events-have-key-events */

/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { useCallback, useState } from 'react';

import Typography, { TypographyProps } from '@libs/ui/components/typography';

import useStyles from './styles';

interface SpoilerProps {
  children?: string;
  wordsCount?: number;
  typographyProps?: TypographyProps;
}

const Spoiler = (props: SpoilerProps) => {
  const classes = useStyles(props);
  const { children, wordsCount = 40, typographyProps } = props;
  const [isFull, setIsFull] = useState(false);
  const toggle = useCallback(() => setIsFull((state) => !state), []);
  const helperText = isFull ? 'less' : 'more...';
  const needButton = (children?.split(' ')?.length ?? 0) > wordsCount;
  const text =
    children &&
    children
      .split(' ')
      .slice(0, !isFull ? wordsCount : undefined)
      .join(' ');

  return (
    <Typography {...typographyProps}>
      {text}
      {needButton && (
        <span role="banner" onClick={toggle} className={classes.moreButton}>
          &nbsp;{children && helperText}
        </span>
      )}
    </Typography>
  );
};

export default Spoiler;
