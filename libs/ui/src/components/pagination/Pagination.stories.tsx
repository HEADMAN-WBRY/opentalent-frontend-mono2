import { Box } from '@mui/material';
import React from 'react';

import Pagination from './Pagination';

export default {
  title: 'Components/Pagination',
  component: Pagination,
};

export const roundedPagination = () => {
  return (
    <>
      <Box mb={4}>
        <Pagination
          showFirstButton
          showLastButton
          count={10}
          variant="outlined"
          shape="rounded"
        />
      </Box>
    </>
  );
};
