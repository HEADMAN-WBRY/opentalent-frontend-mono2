import cn from 'classnames';
import React from 'react';

import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

interface CircleStepperProps {
  steps: number;
  activeStep: number;
  onStepChange: (step: number) => void;
  color: 'tertiary' | 'primary' | 'secondary';
}

const useStyles = makeStyles((theme) => ({
  stepper: {
    height: 32,
    margin: '16px 0 24px',
    width: '100%',
    paddingTop: 0,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    '& i': {
      cursor: 'pointer',
      position: 'relative',
      display: 'block',
      border: ({ color }: CircleStepperProps) =>
        `2px solid ${theme.palette[color].main}`,
      width: 8,
      height: 8,
      borderRadius: '100%',

      '&:not(:last-child)': {
        marginRight: theme.spacing(4),
      },

      '&::before': {
        position: 'absolute',
        content: "''",
        background: ({ color }: CircleStepperProps) =>
          theme.palette[color].main,
        top: '50%',
        left: '50%',
        width: 0,
        height: 0,
        display: 'block',
        transform: 'translate(-50%, -50%)',
        transition: 'all .3s',
        borderRadius: '100%',
      },

      '&$activeStep': {
        '&::before': {
          width: 14,
          height: 14,
        },
      },
    },
  },
  activeStep: {},
}));

const CircleStepper = (props: CircleStepperProps) => {
  const classes = useStyles(props);
  const { steps, activeStep, onStepChange } = props;

  return (
    <Box className={classes.stepper}>
      {Array.from({ length: steps }).map((_, index) => (
        <i
          key={index}
          onClick={() => index !== activeStep && onStepChange(index)}
          className={cn({ [classes.activeStep]: index === activeStep })}
        />
      ))}
    </Box>
  );
};

export default CircleStepper;
