import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import React from 'react';

import Typography from '@libs/ui/components/typography';

import useStyles from './styles';

export interface StepSectionProps {
  children?: React.ReactNode;
  index: number;
  title: string;
}

const StepSection = (props: StepSectionProps) => {
  const { children, index, title } = props;
  const classes = useStyles(props);

  return (
    <Box className={classes.stepContainer}>
      <Grid alignContent="center" alignItems="center" container>
        <Grid item>
          <i className={classes.index}>{index}</i>
        </Grid>
        <Grid item>
          <Typography>{title}</Typography>
        </Grid>
      </Grid>
      <Box className={classes.content}>{children}</Box>
    </Box>
  );
};

export default StepSection;
