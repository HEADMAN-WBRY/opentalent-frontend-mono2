import React from 'react';

import StepSection from '@libs/ui/components/step-section';
import Typography from '@libs/ui/components/typography';

export default {
  title: 'Components/StepSection',
  component: StepSection,
};

export const Default = () => (
  <>
    <StepSection index={1} title="Name and general info">
      <Typography>
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Necessitatibus
        sit praesentium iure magni porro architecto nostrum voluptates autem
        obcaecati, eligendi voluptate libero. Sunt possimus eligendi, nisi
        consequuntur adipisci officiis reprehenderit. Non laudantium nulla
        facilis eveniet vero fuga harum cum deserunt provident natus sit,
        accusantium rerum molestiae?
      </Typography>
    </StepSection>
    <StepSection index={2} title="Name and general info 2">
      <Typography>
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Necessitatibus
        sit praesentium iure magni porro architecto nostrum voluptates autem
        obcaecati, eligendi voluptate libero. Sunt possimus eligendi, nisi
        consequuntur adipisci officiis reprehenderit. Non laudantium nulla
        facilis eveniet vero fuga harum cum deserunt provident natus sit,
        accusantium rerum molestiae?
      </Typography>
    </StepSection>
  </>
);
