import { useField } from 'formik';
import React, { useCallback } from 'react';

import { TextEditor, TextEditorProps } from './TextEditor';

export type ConnectedTextFieldProps = {
  name: string;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
} & TextEditorProps;

export const ConnectedTextEditor = (props: ConnectedTextFieldProps) => {
  const { name, helperText, onChange, ...rest } = props;
  const [field, meta, helpers] = useField({ name });
  const handleChange = useCallback(
    (e: string) => {
      helpers.setValue(e);
      if (onChange) onChange(e);
    },
    [helpers, onChange],
  );

  const error = meta.touched && !!meta.error;
  const finalHelperText = meta.touched && meta.error ? meta.error : helperText;

  return (
    <TextEditor
      {...field}
      helperText={finalHelperText}
      error={error}
      name={name}
      onChange={handleChange}
      {...rest}
    />
  );
};
