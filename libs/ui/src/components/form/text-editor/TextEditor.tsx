import { makeStyles } from '@mui/styles';
import cn from 'classnames';
import React from 'react';
import ReactQuill from 'react-quill';

import Typography from '@libs/ui/components/typography';

export interface TextEditorProps {
  placeholder?: string;
  value?: string;
  disabled?: boolean;
  error?: boolean;
  helperText?: string;
  onChange?: (v: string) => void;
  name?: string;
}

const useStyles = makeStyles((theme) => ({
  root: {},
  editor: {
    background: 'white',
    borderTopRightRadius: 4,
    borderTopLeftRadius: 4,

    '& .ql-toolbar': {
      border: 'none',
      borderBottom: '1px solid #ddd',
    },
    '& .ql-container': {
      border: 'none',
    },

    '& .ql-editor': {
      fontSize: 16,
      minHeight: 124,
    },

    '& .ql-editor.ql-blank::before': {
      fontStyle: 'normal',
      fontSize: 16,
    },
  },
  helperText: {
    '$error &': {
      color: theme.palette.error.main,
    },
  },
  error: {
    '& $editor': {
      border: `1px solid ${theme.palette.error.main}`,
    },
  },
  disabled: {
    '& .ql-editor': {
      color: theme.palette.text.disabled,
    },
  },
}));

export const TextEditor = ({
  value,
  onChange,
  error,
  placeholder,
  disabled,
  name,
  helperText,
}: TextEditorProps) => {
  const classes = useStyles();

  return (
    <div
      data-name={name}
      className={cn(classes.root, {
        [classes.error]: error,
        [classes.disabled]: disabled,
      })}
    >
      <ReactQuill
        className={cn(classes.editor)}
        value={value}
        onChange={onChange}
        placeholder={placeholder}
        readOnly={disabled}
      />
      {helperText && (
        <Typography className={classes.helperText} variant="caption">
          {helperText}
        </Typography>
      )}
    </div>
  );
};
