export interface OptionType {
  [key: string]: any;
  value: string | number;
  text: string;
  disabled?: boolean;
}

export interface OptionsGroup {
  label: string;
  options: OptionType[];
}
