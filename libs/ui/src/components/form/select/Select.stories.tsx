import { Box, Grid } from '@mui/material';
import { Formik } from 'formik';
import React, { useState } from 'react';
import { noop } from 'utils/common';
import * as yup from 'yup';

import Button from '@libs/ui/components/button';

import ConnectedSelect from './ConnectedSelect';
import Select from './Select';

export default {
  title: 'Form/Select',
  component: Select,
};

const mockSelectData = [
  {
    value: 10,
    text: 'Ten',
  },
  {
    value: 20,
    text: 'Twelve',
  },
  {
    value: 30,
    text: 'Thirty',
  },
];

export const MediumSelects = () => {
  const [value, setValue] = useState('');
  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setValue(event.target.value as string);
  };

  return (
    <>
      <Box mb={4}>
        <Select
          label="Line"
          helperText="Loading select"
          value={value}
          onChange={handleChange}
          options={mockSelectData}
          isLoading
        />
      </Box>
      <Box mb={4}>
        <Select
          label="Line"
          helperText="Line"
          value={value}
          onChange={handleChange}
          options={mockSelectData}
        />
      </Box>
      <Box mb={4}>
        <Select
          variant="outlined"
          label="Outlined"
          helperText="Outlined"
          value={value}
          onChange={handleChange}
          options={mockSelectData}
        />
      </Box>
      <Box mb={4}>
        <Select
          variant="filled"
          label="Filled"
          helperText="Filled"
          value={value}
          onChange={handleChange}
          options={mockSelectData}
        />
      </Box>
      <Box mb={4}>
        <Select
          disabled
          label="Line"
          helperText="Line"
          value={value}
          onChange={handleChange}
          options={mockSelectData}
        />
      </Box>
      <Box mb={4}>
        <Select
          disabled
          variant="outlined"
          label="Outlined"
          helperText="Outlined"
          value={value}
          onChange={handleChange}
          options={mockSelectData}
        />
      </Box>
      <Box mb={4}>
        <Select
          disabled
          variant="filled"
          label="Filled"
          helperText="Filled"
          value={value}
          onChange={handleChange}
          options={mockSelectData}
        />
      </Box>
      <Box mb={4}>
        <Select
          error
          label="Line"
          helperText="Line"
          value={value}
          onChange={handleChange}
          options={mockSelectData}
        />
      </Box>
      <Box mb={4}>
        <Select
          error
          variant="outlined"
          label="Outlined"
          helperText="Outlined"
          value={value}
          onChange={handleChange}
          options={mockSelectData}
        />
      </Box>
      <Box mb={4}>
        <Select
          error
          variant="filled"
          label="Filled"
          helperText="Filled"
          value={value}
          onChange={handleChange}
          options={mockSelectData}
        />
      </Box>
    </>
  );
};

export const DenseSelects = () => {
  const [value, setValue] = useState('');
  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    setValue(event.target.value as string);
  };

  return (
    <>
      <Box mb={4}>
        <Select
          margin="dense"
          label="Line"
          helperText="Line"
          value={value}
          onChange={handleChange}
          options={mockSelectData}
        />
      </Box>
      <Box mb={4}>
        <Select
          margin="dense"
          variant="outlined"
          label="Outlined"
          helperText="Outlined"
          value={value}
          onChange={handleChange}
          options={mockSelectData}
        />
      </Box>
      <Box mb={4}>
        <Select
          margin="dense"
          variant="filled"
          label="Filled"
          helperText="Filled"
          value={value}
          onChange={handleChange}
          options={mockSelectData}
        />
      </Box>
      <Box mb={4}>
        <Select
          margin="dense"
          disabled
          label="Line"
          helperText="Line"
          value={value}
          onChange={handleChange}
          options={mockSelectData}
        />
      </Box>
      <Box mb={4}>
        <Select
          margin="dense"
          disabled
          variant="outlined"
          label="Outlined"
          helperText="Outlined"
          value={value}
          onChange={handleChange}
          options={mockSelectData}
        />
      </Box>
      <Box mb={4}>
        <Select
          margin="dense"
          disabled
          variant="filled"
          label="Filled"
          helperText="Filled"
          value={value}
          onChange={handleChange}
          options={mockSelectData}
        />
      </Box>
      <Box mb={4}>
        <Select
          margin="dense"
          error
          label="Line"
          helperText="Line"
          value={value}
          onChange={handleChange}
          options={mockSelectData}
        />
      </Box>
      <Box mb={4}>
        <Select
          margin="dense"
          error
          variant="outlined"
          label="Outlined"
          helperText="Outlined"
          value={value}
          onChange={handleChange}
          options={mockSelectData}
        />
      </Box>
      <Box mb={4}>
        <Select
          margin="dense"
          error
          variant="filled"
          label="Filled"
          helperText="Filled"
          value={value}
          onChange={handleChange}
          options={mockSelectData}
        />
      </Box>
    </>
  );
};

const validation = yup.object().shape({
  number: yup.string().nullable().required('Required'),
});

export const BasicConnectedSelect = () => {
  return (
    <Formik
      onSubmit={noop}
      validationSchema={validation}
      initialValues={{ number: null }}
    >
      {({ handleSubmit }) => {
        return (
          <Grid direction="column" container>
            <Grid item>
              <ConnectedSelect
                name="number"
                margin="dense"
                variant="filled"
                label="Filled"
                helperText="Filled"
                options={mockSelectData}
              />
            </Grid>
            <br />
            <Grid item>
              <Button variant="contained" onClick={handleSubmit}>
                Submit
              </Button>
            </Grid>
          </Grid>
        );
      }}
    </Formik>
  );
};
