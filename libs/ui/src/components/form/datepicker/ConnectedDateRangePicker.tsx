import { useField } from 'formik';
import React from 'react';

import DateRangePicker, { DateRangePickerProps } from './DateRangePicker';

interface ConnectedDatePickerProps
  extends Omit<DateRangePickerProps, 'value' | 'onChange'> {
  name: string;
}

const ConnectedDateRangePicker = (props: ConnectedDatePickerProps) => {
  const { name, FirstTextFieldProps, SecondTextFieldProps, ...rest } = props;
  const [field, meta, helpers] = useField({ name });

  const firstError = meta.touched && !!meta?.error?.[0];
  const secondError = meta.touched && !!meta?.error?.[1];

  const finalFirstHelperText =
    meta.touched && meta?.error?.[0]
      ? meta.error[0]
      : FirstTextFieldProps?.helperText;
  const finalSecondHelperText =
    meta.touched && meta?.error?.[1]
      ? meta.error[1]
      : SecondTextFieldProps?.helperText;
  const fieldValue = field.value;

  return (
    <DateRangePicker
      value={fieldValue}
      onChange={(newValue) => helpers.setValue(newValue, true)}
      TextFieldProps={{
        name: field.name,
      }}
      FirstTextFieldProps={{
        name: `${field.name}.0`,
        helperText: finalFirstHelperText,
        error: firstError,
        ...FirstTextFieldProps,
      }}
      SecondTextFieldProps={{
        name: `${field.name}.1`,
        helperText: finalSecondHelperText,
        error: secondError,
        ...SecondTextFieldProps,
      }}
      {...rest}
    />
  );
};

export default ConnectedDateRangePicker;
