import { Box } from '@mui/material';
import React from 'react';

import MultipleSelect from './MultipleSelect';

export default {
  title: 'Form/MultipleSelect',
  component: MultipleSelect,
};

const mockData = [
  { text: 'The Shawshank Redemption', value: 1994 },
  { text: 'The Godfather', value: 1972 },
  { text: 'The Godfather: Part II', value: 1974 },
];

export const MultipleSelects = () => {
  return (
    <>
      <Box mb={4}>
        <MultipleSelect
          chipProps={{
            color: 'default',
            size: 'small',
          }}
          inputProps={{
            variant: 'filled',
            label: 'MultipleSelect',
            placeholder: 'Items',
          }}
          options={mockData}
        />
      </Box>
    </>
  );
};
