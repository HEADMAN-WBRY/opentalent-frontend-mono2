import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  modal: {
    padding: `${theme.spacing(2)} ${theme.spacing(5)} ${theme.spacing(6)}`,
  },
  cropContainer: {
    width: 368,
    height: 240,
    position: 'relative',
  },
  slider: {
    paddingTop: theme.spacing(8),
  },
}));

export default useStyles;
