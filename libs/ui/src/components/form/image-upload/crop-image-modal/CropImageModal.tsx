import React, { useCallback, useState } from 'react';
import Cropper from 'react-easy-crop';

import CropRotateIcon from '@mui/icons-material/CropRotate';
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  Slider,
} from '@mui/material';

import { getNextIndex } from '@libs/helpers/common';
import Button from '@libs/ui/components/button';

import useStyles from './styles';
import getCroppedImg from './utils';

interface CropImageModalProps {
  open: boolean;
  handleClose: VoidFunction;
  image: string;
  changeImage: any;
}

const CropImageModal = (props: CropImageModalProps) => {
  const classes = useStyles(props);
  const { open, handleClose, image, changeImage } = props;
  const [crop, onCropChange] = useState({ x: 0, y: 0 });
  const [zoom, onZoomChange] = useState(1);
  const [croppedAreaPixels, setCroppedAreaPixels] = useState(null);
  const [rotateCof, setRotateCof] = useState(0);

  const onCropComplete = useCallback((croppedArea: any, cap: any) => {
    setCroppedAreaPixels(cap);
  }, []);
  const getCroppedImage = useCallback(async () => {
    try {
      const finalImage = await getCroppedImg(
        image,
        croppedAreaPixels as any,
        rotateCof * 90,
      );
      changeImage({
        target: { value: [finalImage] },
        preventDefault: () => undefined,
        persist: () => undefined,
      });
      handleClose();
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e);
    }
  }, [changeImage, croppedAreaPixels, handleClose, image, rotateCof]);

  return (
    <Dialog
      classes={{ paper: classes.modal }}
      open={open}
      onClose={handleClose}
    >
      <DialogTitle>Move and scale to crop</DialogTitle>
      <DialogContent>
        <Grid spacing={4} direction="column" container>
          <Grid className={classes.cropContainer} item>
            <Cropper
              image={image}
              crop={crop}
              rotation={rotateCof * 90}
              zoom={zoom}
              onCropChange={onCropChange}
              onZoomChange={onZoomChange}
              cropShape="round"
              cropSize={{ width: 200, height: 200 }}
              minZoom={0.5}
              onCropComplete={onCropComplete}
            />
          </Grid>
          <Grid item>
            <Grid container spacing={4}>
              <Grid style={{ flexGrow: 1 }} item>
                <Slider
                  className={classes.slider}
                  color="secondary"
                  value={zoom}
                  min={0.5}
                  max={3}
                  step={0.1}
                  onChange={(e, z) => onZoomChange(z as number)}
                />
              </Grid>
              <Grid item>
                <IconButton
                  size="large"
                  onClick={() =>
                    setRotateCof((i) => {
                      const next = getNextIndex(i, 4);
                      return next;
                    })
                  }
                >
                  <CropRotateIcon />
                </IconButton>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button
          fullWidth
          onClick={handleClose}
          variant="outlined"
          color="secondary"
        >
          Cancel
        </Button>
        <Button
          fullWidth
          onClick={getCroppedImage}
          variant="contained"
          color="primary"
          autoFocus
        >
          Choose
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default CropImageModal;
