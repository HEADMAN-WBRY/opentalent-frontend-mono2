import { Box, Grid } from '@mui/material';
import { Formik } from 'formik';
import { isValidPhoneNumber } from 'libphonenumber-js';
import React from 'react';
import * as yup from 'yup';

import { noop } from '@libs/helpers/common';
import Button from '@libs/ui/components/button';

import ConnectedPhoneField from './ConnectedPhoneField';
import PhoneField from './PhoneField';

export default {
  title: 'Form/PhoneField',
  component: PhoneField,
};

export const Default = () => {
  return (
    <Box mb={2}>
      <PhoneField format="US" variant="outlined" label="US phone" />
    </Box>
  );
};

const validation = yup.object().shape({
  phone: yup
    .string()
    .test('Phone validation', 'Invalid phone', (value: any) =>
      isValidPhoneNumber(value),
    )
    .required('Required'),
});

export const BasicConnectedPhoneField = () => {
  return (
    <Box mb={2}>
      <Formik
        onSubmit={noop}
        validationSchema={validation}
        initialValues={{ phone: null }}
      >
        {({ handleSubmit }) => {
          return (
            <Grid direction="column" container>
              <Grid item>
                <ConnectedPhoneField
                  label="US Phone"
                  name="phone"
                  helperText="Phone"
                  variant="filled"
                  format="US"
                />
              </Grid>
              <br />
              <Grid item>
                <Button variant="contained" onClick={handleSubmit}>
                  Submit
                </Button>
              </Grid>
            </Grid>
          );
        }}
      </Formik>
    </Box>
  );
};
