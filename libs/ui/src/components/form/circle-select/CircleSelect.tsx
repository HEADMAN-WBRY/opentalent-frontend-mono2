/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events */
import cn from 'classnames';
import React from 'react';

import { Grid } from '@mui/material';

import { noop } from '@libs/helpers/common';

import { OptionType } from '../select';
import useStyles from './styles';

export interface CircleSelectProps {
  options: OptionType[];
  value?: OptionType;
  onChange?: (value: OptionType) => void;
}

const CircleSelect = (props: CircleSelectProps) => {
  const classes = useStyles(props);
  const { options, value, onChange = noop } = props;

  return (
    <Grid container>
      <Grid item>
        <div className={classes.circles}>
          {options.map((option) => {
            if (!option) {
              return null;
            }
            const isActive = value && value.value === option.value;
            const onClick = (e: React.SyntheticEvent) => {
              e.stopPropagation();
              e.preventDefault();
              onChange(option);
            };
            return (
              <div
                key={option?.value}
                className={cn(classes.circle, {
                  [classes.circleActive]: isActive,
                })}
                title={option.text}
                data-title={option.text}
                onClick={onClick}
              />
            );
          })}
        </div>
      </Grid>
    </Grid>
  );
};

export default CircleSelect;
