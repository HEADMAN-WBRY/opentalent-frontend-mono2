import { colors } from '@mui/material';
import { makeStyles } from '@mui/styles';

const CAPTION_PADDING_LEFT = 80;

const useStyles = makeStyles((theme) => ({
  root: {},
  circles: {
    width: 120,
    height: 120,
    position: 'relative',
  },
  circle: {
    position: 'absolute',
    borderRadius: '100%',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    cursor: 'pointer',

    '&::after': {
      content: 'attr(data-title)',
      position: 'absolute',
      display: 'block',
      top: 0,
      left: '50%',
      paddingLeft: CAPTION_PADDING_LEFT,
      borderBottom: '1px solid transparent',
      fontSize: 14,
      lineHeight: '17px',
      color: colors.grey[500],
      whiteSpace: 'nowrap',
    },

    '&:nth-child(1)': {
      width: '50%',
      height: '50%',
      background: colors.grey[400],
      zIndex: 3,

      '&::after': {
        top: '6px',
      },
    },

    '&:nth-child(2)': {
      width: '70%',
      height: '70%',
      background: colors.grey[400],
      zIndex: 2,

      '&::after': {
        top: '54%',
        marginLeft: 22,
        paddingLeft: CAPTION_PADDING_LEFT - 22,
      },
    },

    '&:nth-child(3)': {
      width: '100%',
      height: '100%',
      background: colors.grey[300],
      zIndex: 1,

      '&::after': {
        top: '71%',
        marginLeft: 28,
        paddingLeft: CAPTION_PADDING_LEFT - 28,
      },
    },
  },
  circleActive: {
    'div$circle&': {
      background: theme.palette.primary.main,
    },

    '&::after': {
      borderColor: colors.grey[700],
      color: colors.grey[800],
    },
  },
  labels: {},
}));

export default useStyles;
