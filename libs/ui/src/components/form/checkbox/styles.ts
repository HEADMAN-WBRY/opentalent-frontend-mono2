import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  colorPrimary: {
    color: theme?.palette?.primary?.main ?? '#191919',
    '&$disabled': {
      color: theme?.palette?.other.light ?? 'rgba(0, 0, 0, 0.26)',
    },
  },
  colorSecondary: {
    color: theme?.palette?.secondary?.main ?? '#191919',
    '&$disabled': {
      color: theme?.palette?.other.light ?? 'rgba(0, 0, 0, 0.26)',
    },
  },
}));

export default useStyles;
