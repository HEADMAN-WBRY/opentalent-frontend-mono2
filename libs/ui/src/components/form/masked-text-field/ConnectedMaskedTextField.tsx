import { useField } from 'formik';
import React from 'react';

import MaskedTextField, { MaskedTextFieldProps } from './MaskedTextField';

type ConnectedMaskedTextFieldProps = {
  name: string;
} & MaskedTextFieldProps;

const ConnectedMaskedTextField = (props: ConnectedMaskedTextFieldProps) => {
  const { name, helperText, maskedProps, ...rest } = props;
  const [field, meta] = useField(name);

  const error = meta.touched && !!meta.error;
  const finalHelperText = meta.touched && meta.error ? meta.error : helperText;
  const fieldValue = field.value || '';

  return (
    <MaskedTextField
      maskedProps={{
        ...maskedProps,
        onBlur: field.onBlur,
        onChange: field.onChange,
        value: fieldValue,
      }}
      {...rest}
      id={field.name}
      name={field.name}
      error={error}
      helperText={finalHelperText}
    />
  );
};

export default ConnectedMaskedTextField;
