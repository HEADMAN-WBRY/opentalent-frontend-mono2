import { mergeDeep } from '@apollo/client/utilities';
import React from 'react';

import {
  Slider as MuiSlider,
  SliderProps as MuiSliderProps,
} from '@mui/material';
import { makeStyles } from '@mui/styles';

export interface SliderProps extends Omit<MuiSliderProps, 'onChange'> {
  onChange: (event: any, newValue: number | number[]) => void;
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: 16,
  },
  thumb: {
    height: 26,
    width: 26,
    border: '2px solid currentColor',
    '&:focus, &:hover, &$active': {
      boxShadow: 'inherit',
    },
  },
  active: {},
  valueLabel: {
    fontSize: 20,
    color: theme.palette.secondary.contrastText,
  },
  track: {
    height: 16,
    borderRadius: 8,
  },
  rail: {
    height: 16,
    borderRadius: 8,
    opacity: 1,
    color: theme.palette.grey[800],
  },
  mark: {
    display: 'none',
  },
}));

const Slider = ({ classes, ...rest }: SliderProps) => {
  const classesOverrides = useStyles();

  return <MuiSlider {...rest} classes={mergeDeep(classes, classesOverrides)} />;
};

export default Slider;
