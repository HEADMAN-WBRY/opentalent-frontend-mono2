import { CampaignStatus } from '@libs/ui/components/job/utils';

export const CAMPAIGN_LABELS = {
  [CampaignStatus.Finished]: 'Completed',
  [CampaignStatus.Started]: 'Open',
  [CampaignStatus.NotStarted]: 'Starting soon',
  [CampaignStatus.Archived]: 'Archived',
  [CampaignStatus.Draft]: 'Draft',
};
