import { formatRate } from './common';

describe('Test common functions', () => {
  it(`Test formatRate`, () => {
    expect(formatRate({ min: 1000, max: 10000.2 })).toBe('€1.000-10.000,2');
    expect(formatRate({ min: 1234567.55, max: 12345678 })).toBe(
      '€1.234.567,55-12.345.678',
    );
  });
});
