import { noop } from '@libs/helpers/common';
import { Grid } from '@mui/material';
import { useSnackbar } from 'notistack';
import React from 'react';

import Button from '../button';
import Typography from '../typography';

interface NotificationWithActionProps extends React.PropsWithChildren<unknown> {
  handleAction: VoidFunction;
  actionText: string;
}

export const NotificationWithAction = ({
  handleAction = noop,
  actionText = 'Undo',
  children,
}: NotificationWithActionProps) => {
  const { closeSnackbar } = useSnackbar();

  return (
    <div>
      <Grid container spacing={4}>
        <Grid
          style={{ display: 'flex', alignItems: 'center' }}
          item
          flexGrow={1}
        >
          <Typography variant="body2">{children}</Typography>
        </Grid>
        <Grid item>
          <Button
            variant="text"
            onClick={() => {
              closeSnackbar();
              handleAction();
            }}
            color="primary"
          >
            {actionText}
          </Button>
        </Grid>
      </Grid>
    </div>
  );
};
