/* eslint-disable no-console */
import React, { useState } from 'react';

import { Box } from '@mui/material';

import ArrowTabs from './ArrowTabs';
import { TabItemsType } from './types';

export default {
  title: 'Components/ArrowTabs',
  component: ArrowTabs,
};

const ITEMS: TabItemsType[] = [
  { id: '1', children: 'Instant matches (10)' },
  { id: '2', children: 'Invited (12)' },
  { id: '3', children: 'Hired', bg: 'primary' },
];

export const Default = () => {
  const [current, setCurrent] = useState<string>('1');
  return (
    <Box style={{ background: '#EDEDED' }} p={4} mb={4} width={500}>
      <ArrowTabs currentActive={current} onChange={setCurrent} items={ITEMS} />
    </Box>
  );
};
