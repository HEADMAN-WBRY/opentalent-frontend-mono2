// eslint-disable-next-line import/no-unresolved
import { ComponentsOverrides, Theme } from '@mui/material';

const MuiButtonOverrides: ComponentsOverrides<Theme>['MuiButton'] = {
  root: {
    height: 36,
    borderRadius: 0,
    whiteSpace: 'nowrap',

    '&:not(:active)': {
      boxShadow: 'none',
    },
  },
  sizeLarge: {
    height: 45,
  },
  sizeSmall: {
    height: 30,
  },
};

export default MuiButtonOverrides;
