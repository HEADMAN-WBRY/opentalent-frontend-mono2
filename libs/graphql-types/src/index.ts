import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | undefined;
export type InputMaybe<T> = T | undefined;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** A date string with format `Y-m-d`, e.g. `2011-05-23`. */
  Date: any;
  /** A datetime string with format `Y-m-d H:i:s`, e.g. `2018-05-23 13:43:32`. */
  DateTime: any;
  /** A datetime string in ISO 8601 format in UTC with nanoseconds `YYYY-MM-DDTHH:mm:ss.SSSSSSZ`, e.g. `2020-04-20T16:20:04.000000Z`. */
  DateTimeUtc: any;
  /** Email in the format `user@example.com` */
  Email: any;
  /** Can be used as an argument to upload files using https://github.com/jaydenseric/graphql-multipart-request-spec */
  Upload: any;
};

export type AbacString = {
  __typename?: 'ABACString';
  is_accessible?: Maybe<Scalars['Boolean']>;
  is_presented?: Maybe<Scalars['Boolean']>;
  value?: Maybe<Scalars['String']>;
};

export type AtsRecordActor = {
  __typename?: 'ATSRecordActor';
  avatar?: Maybe<Avatar>;
  first_name: Scalars['String'];
  last_name?: Maybe<Scalars['String']>;
};

export type AtsRecordComment = TalentLogRecordInterface & {
  __typename?: 'ATSRecordComment';
  actor?: Maybe<AtsRecordActor>;
  company_id?: Maybe<Scalars['ID']>;
  date: Scalars['DateTimeUtc'];
  id: Scalars['ID'];
  text?: Maybe<Scalars['String']>;
  type: AtsRecordTypeEnum;
};

export type AtsRecordJobMatchAction = TalentLogRecordInterface & {
  __typename?: 'ATSRecordJobMatchAction';
  actor?: Maybe<AtsRecordActor>;
  company_id?: Maybe<Scalars['ID']>;
  date: Scalars['DateTimeUtc'];
  id: Scalars['ID'];
  job_match_action?: Maybe<JobMatchAction>;
  type: AtsRecordTypeEnum;
};

export const AtsRecordTypeEnum = {
  CompanyUserCommented: 'COMPANY_USER_COMMENTED',
  JobMatchAction: 'JOB_MATCH_ACTION'
} as const;

export type AtsRecordTypeEnum = typeof AtsRecordTypeEnum[keyof typeof AtsRecordTypeEnum];
export type AtsRecordsUnion = AtsRecordComment | AtsRecordJobMatchAction;

export const AccountTypeEnum = {
  Free: 'FREE',
  Matcher: 'MATCHER',
  MatcherComplementary: 'MATCHER_COMPLEMENTARY',
  Member: 'MEMBER'
} as const;

export type AccountTypeEnum = typeof AccountTypeEnum[keyof typeof AccountTypeEnum];
export type AvailableJobForTalent = {
  __typename?: 'AvailableJobForTalent';
  is_applied: Scalars['Boolean'];
  is_invited: Scalars['Boolean'];
  job?: Maybe<Job>;
};

export type Avatar = {
  __typename?: 'Avatar';
  avatar: Scalars['String'];
  hash?: Maybe<Scalars['String']>;
};

export const BooleanOperatorsEnum = {
  And: 'AND',
  Not: 'NOT',
  Or: 'OR'
} as const;

export type BooleanOperatorsEnum = typeof BooleanOperatorsEnum[keyof typeof BooleanOperatorsEnum];
export const CanHelpWithEnum = {
  KnowCompaniesThatBenefits: 'KNOW_COMPANIES_THAT_BENEFITS',
  KnowTheCeo: 'KNOW_THE_CEO',
  PeopleInRecruitment: 'PEOPLE_IN_RECRUITMENT',
  PeopleLookingForTalentPooling: 'PEOPLE_LOOKING_FOR_TALENT_POOLING'
} as const;

export type CanHelpWithEnum = typeof CanHelpWithEnum[keyof typeof CanHelpWithEnum];
export type ChatMessage = {
  __typename?: 'ChatMessage';
  company_id?: Maybe<Scalars['ID']>;
  created_at?: Maybe<Scalars['DateTime']>;
  id?: Maybe<Scalars['ID']>;
  message?: Maybe<Scalars['String']>;
  parent_id?: Maybe<Scalars['ID']>;
  talent_id?: Maybe<Scalars['ID']>;
  updated_at?: Maybe<Scalars['DateTime']>;
  user?: Maybe<User>;
};

/** A paginated list of ChatMessage items. */
export type ChatMessagePaginator = {
  __typename?: 'ChatMessagePaginator';
  /** A list of ChatMessage items. */
  data: Array<ChatMessage>;
  /** Pagination information about the list of items. */
  paginatorInfo: PaginatorInfo;
};

export type CommonAppInfo = {
  __typename?: 'CommonAppInfo';
  total_ot_approved_freelancers_count: Scalars['Int'];
  total_ot_freelancers_count: Scalars['Int'];
  total_ot_freelancers_countries_count: Scalars['Int'];
  total_ot_pool_active_talents_count: Scalars['Int'];
  total_ot_recruiters_count: Scalars['Int'];
  total_ot_recruiters_countries_count: Scalars['Int'];
  total_unique_companies_count: Scalars['Int'];
  total_unique_skills_count: Scalars['Int'];
};

export type Company = {
  __typename?: 'Company';
  about?: Maybe<Scalars['String']>;
  account_type?: Maybe<CompanyAccountTypeEnum>;
  address?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  logo?: Maybe<CompanyLogo>;
  name: Scalars['String'];
  primary_user?: Maybe<User>;
  sign_up_landing_link?: Maybe<Scalars['String']>;
  size?: Maybe<Scalars['String']>;
  type_of_activity?: Maybe<Scalars['String']>;
  website?: Maybe<Scalars['String']>;
};

export const CompanyAccountTypeEnum = {
  Enterprise: 'ENTERPRISE',
  Free: 'FREE',
  None: 'NONE',
  Paid: 'PAID'
} as const;

export type CompanyAccountTypeEnum = typeof CompanyAccountTypeEnum[keyof typeof CompanyAccountTypeEnum];
export type CompanyLogo = {
  __typename?: 'CompanyLogo';
  hash?: Maybe<Scalars['String']>;
  url: Scalars['String'];
};

/** A paginated list of Company items. */
export type CompanyPaginator = {
  __typename?: 'CompanyPaginator';
  /** A list of Company items. */
  data: Array<Company>;
  /** Pagination information about the list of items. */
  paginatorInfo: PaginatorInfo;
};

export type CompanyPublicInfo = {
  __typename?: 'CompanyPublicInfo';
  talents_count?: Maybe<Scalars['Int']>;
};

/** Company user onboarding flow types */
export const CompanyUserOnboardingTypeEnum = {
  Main: 'MAIN',
  Simple: 'SIMPLE'
} as const;

export type CompanyUserOnboardingTypeEnum = typeof CompanyUserOnboardingTypeEnum[keyof typeof CompanyUserOnboardingTypeEnum];
/** Company user roles */
export const CompanyUserRoleEnum = {
  AccountAdmin: 'ACCOUNT_ADMIN',
  AccountUser: 'ACCOUNT_USER'
} as const;

export type CompanyUserRoleEnum = typeof CompanyUserRoleEnum[keyof typeof CompanyUserRoleEnum];
export type CurrentTalentJobBoardSearchResult = Job | TalentJobConnection;

export type DashboardAnalyticsItem = {
  __typename?: 'DashboardAnalyticsItem';
  key?: Maybe<Scalars['String']>;
  slug?: Maybe<Scalars['String']>;
  value?: Maybe<Scalars['Float']>;
};

export type ExternalJob = {
  __typename?: 'ExternalJob';
  application_active: Scalars['Boolean'];
  category?: Maybe<TalentCategory>;
  company_name?: Maybe<Scalars['String']>;
  company_url?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  created?: Maybe<Scalars['DateTime']>;
  deleted: Scalars['Boolean'];
  description: Scalars['String'];
  employment_type?: Maybe<ExternalJobEmploymentTypeEnum>;
  external_url?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  is_saved: Scalars['Boolean'];
  last_updated?: Maybe<Scalars['DateTime']>;
  linkedin_job_id?: Maybe<Scalars['String']>;
  location?: Maybe<Scalars['String']>;
  match_quality?: Maybe<MatchQualityEnum>;
  salary?: Maybe<Scalars['Float']>;
  seniority?: Maybe<ExternalJobSeniorityLevelEnum>;
  title: Scalars['String'];
  url?: Maybe<Scalars['String']>;
};

export const ExternalJobEmploymentTypeEnum = {
  Contract: 'CONTRACT',
  FullTime: 'FULL_TIME',
  Internship: 'INTERNSHIP',
  Other: 'OTHER',
  PartTime: 'PART_TIME',
  Temporary: 'TEMPORARY',
  Volunteer: 'VOLUNTEER'
} as const;

export type ExternalJobEmploymentTypeEnum = typeof ExternalJobEmploymentTypeEnum[keyof typeof ExternalJobEmploymentTypeEnum];
/** A paginated list of ExternalJob items. */
export type ExternalJobPaginator = {
  __typename?: 'ExternalJobPaginator';
  /** A list of ExternalJob items. */
  data: Array<ExternalJob>;
  /** Pagination information about the list of items. */
  paginatorInfo: PaginatorInfo;
};

export const ExternalJobSeniorityLevelEnum = {
  Associate: 'ASSOCIATE',
  Director: 'DIRECTOR',
  EntryLevel: 'ENTRY_LEVEL',
  Executive: 'EXECUTIVE',
  Internship: 'INTERNSHIP',
  MidSeniorLevel: 'MID_SENIOR_LEVEL',
  NotApplicable: 'NOT_APPLICABLE'
} as const;

export type ExternalJobSeniorityLevelEnum = typeof ExternalJobSeniorityLevelEnum[keyof typeof ExternalJobSeniorityLevelEnum];
export type File = {
  __typename?: 'File';
  content_type: Scalars['String'];
  created_at?: Maybe<Scalars['DateTime']>;
  file_type?: Maybe<FileTypeEnum>;
  hash: Scalars['String'];
  size: Scalars['Int'];
  title: Scalars['String'];
  updated_at?: Maybe<Scalars['DateTime']>;
  url: Scalars['String'];
};

export type FileInput = {
  file: Scalars['Upload'];
  file_type: FileTypeEnum;
  owner_id?: InputMaybe<Scalars['ID']>;
};

export const FileTypeEnum = {
  CompanyLogo: 'COMPANY_LOGO',
  CompanyUserAvatar: 'COMPANY_USER_AVATAR',
  TalentAvatar: 'TALENT_AVATAR',
  TalentDocument: 'TALENT_DOCUMENT'
} as const;

export type FileTypeEnum = typeof FileTypeEnum[keyof typeof FileTypeEnum];
/** Order by clause for the `orderBy` argument on the query `posts`. */
export const InstantMatchesNotificationsScoreEnum = {
  Score_50: 'SCORE_50',
  Score_70: 'SCORE_70',
  Score_90: 'SCORE_90'
} as const;

export type InstantMatchesNotificationsScoreEnum = typeof InstantMatchesNotificationsScoreEnum[keyof typeof InstantMatchesNotificationsScoreEnum];
export type InvitationLink = {
  __typename?: 'InvitationLink';
  id: Scalars['ID'];
  inviting_talent?: Maybe<Talent>;
  is_used: Scalars['Boolean'];
  name?: Maybe<Scalars['String']>;
  url: Scalars['String'];
};

export type InvitationLinkInfo = {
  __typename?: 'InvitationLinkInfo';
  email?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  inviting_person_name?: Maybe<Scalars['String']>;
  is_used: Scalars['Boolean'];
  key: Scalars['String'];
  name?: Maybe<Scalars['String']>;
};

export type Job = {
  __typename?: 'Job';
  campaign_end_date?: Maybe<Scalars['DateTimeUtc']>;
  campaign_owner?: Maybe<User>;
  campaign_start_date?: Maybe<Scalars['DateTimeUtc']>;
  campaign_talent_pool?: Maybe<Array<Maybe<SourceTypeEnum>>>;
  capacity?: Maybe<Scalars['String']>;
  category?: Maybe<TalentCategory>;
  city?: Maybe<Scalars['String']>;
  client?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  end_date?: Maybe<Scalars['DateTimeUtc']>;
  finders_fee?: Maybe<Scalars['Float']>;
  hours_per_week?: Maybe<Scalars['Int']>;
  id: Scalars['ID'];
  instant_matches_count: Scalars['Int'];
  invitations?: Maybe<Array<Maybe<JobInvitation>>>;
  is_applied?: Maybe<Scalars['Boolean']>;
  is_applied_as_matcher?: Maybe<Scalars['Boolean']>;
  is_archived: Scalars['Boolean'];
  is_assigned_as_matcher?: Maybe<Scalars['Boolean']>;
  /** Determines whether the job campaign saved as draft */
  is_draft?: Maybe<Scalars['Boolean']>;
  is_instant_matching_in_progress?: Maybe<Scalars['Boolean']>;
  is_old_format: Scalars['Boolean'];
  is_rate_negotiable?: Maybe<Scalars['Boolean']>;
  is_remote_an_option: Scalars['Boolean'];
  is_salary_negotiable?: Maybe<Scalars['Boolean']>;
  /** Context related attributes. Presented only if there is talent context */
  is_saved?: Maybe<Scalars['Boolean']>;
  /** Applications made by matchers to source for the job */
  job_matcher_applications?: Maybe<Array<JobMatcherApplication>>;
  job_source_type?: Maybe<JobSourceType>;
  link_to_details?: Maybe<Scalars['String']>;
  location?: Maybe<Scalars['String']>;
  location_type?: Maybe<JobLocationTypeEnum>;
  /** Talent matchers who was assigned to job */
  matchers?: Maybe<Array<Maybe<Talent>>>;
  matches?: Maybe<Array<Maybe<JobMatch>>>;
  matches_count: Scalars['Int'];
  matches_with_application_count: Scalars['Int'];
  max_project_budget?: Maybe<Scalars['Float']>;
  name: Scalars['String'];
  /** Number of hours in office for 'hybrid' jobs */
  office_hours_per_month?: Maybe<Scalars['Float']>;
  period?: Maybe<Scalars['String']>;
  permanent_capacity_type?: Maybe<PermanentJobCapacityTypeEnum>;
  pitch?: Maybe<Scalars['String']>;
  posted_at?: Maybe<Scalars['DateTimeUtc']>;
  /** Propose to talent matchers */
  propose_to_matchers?: Maybe<Scalars['Boolean']>;
  rate_max?: Maybe<Scalars['Float']>;
  rate_min?: Maybe<Scalars['Float']>;
  salary_max?: Maybe<Scalars['Float']>;
  salary_min?: Maybe<Scalars['Float']>;
  saved_matches?: Maybe<Array<Maybe<JobMatch>>>;
  /** Type of service provided for this job by Opentalent */
  service_type?: Maybe<JobServiceTypeEnum>;
  skills?: Maybe<Array<Maybe<JobSkill>>>;
  skills_boolean_filter?: Maybe<JobSkillsBooleanFilter>;
  start_date?: Maybe<Scalars['DateTimeUtc']>;
  /** Stream chat id */
  stream_chat_subject_id?: Maybe<Scalars['String']>;
  subcategory?: Maybe<Subcategory>;
  type?: Maybe<JobTypeEnum>;
};

export type JobApplication = {
  __typename?: 'JobApplication';
  created_at: Scalars['DateTimeUtc'];
  id: Scalars['ID'];
  job: Job;
  job_id: Scalars['ID'];
  pitch?: Maybe<Scalars['String']>;
  rate?: Maybe<Scalars['Float']>;
  talent_id: Scalars['ID'];
};

export type JobApplicationInput = {
  job_id: Scalars['ID'];
  pitch?: InputMaybe<Scalars['String']>;
  rate?: InputMaybe<Scalars['Float']>;
};

export type JobBoardInfo = {
  __typename?: 'JobBoardInfo';
  talent_matches_count: Scalars['Int'];
  talent_matches_count_archived: Scalars['Int'];
  talent_responses_count: Scalars['Int'];
  talent_responses_count_archived: Scalars['Int'];
};

export type JobInvitation = {
  __typename?: 'JobInvitation';
  application?: Maybe<JobApplication>;
  id: Scalars['ID'];
  is_accepted: Scalars['Boolean'];
  is_declined?: Maybe<Scalars['Boolean']>;
  job: Job;
  job_id: Scalars['ID'];
  message?: Maybe<Scalars['String']>;
  talent_id: Scalars['ID'];
};

export const JobLocationTypeEnum = {
  Hybrid: 'HYBRID',
  OnSite: 'ON_SITE',
  Remote: 'REMOTE'
} as const;

export type JobLocationTypeEnum = typeof JobLocationTypeEnum[keyof typeof JobLocationTypeEnum];
export type JobMatch = {
  __typename?: 'JobMatch';
  cv_skills_keywords?: Maybe<Array<Maybe<Skill>>>;
  id: Scalars['ID'];
  is_applied: Scalars['Boolean'];
  is_instant_match: Scalars['Boolean'];
  is_invited: Scalars['Boolean'];
  is_new: Scalars['Boolean'];
  is_shortlist: Scalars['Boolean'];
  is_talent_notified: Scalars['Boolean'];
  job: Job;
  job_application?: Maybe<JobApplication>;
  job_invitation?: Maybe<JobInvitation>;
  match_percent: Scalars['Int'];
  match_quality?: Maybe<MatchQualityEnum>;
  match_type: JobMatchTypeEnum;
  matched_skills?: Maybe<Array<Maybe<Skill>>>;
  talent?: Maybe<Talent>;
};

export type JobMatchAction = Node & {
  __typename?: 'JobMatchAction';
  id: Scalars['ID'];
  job_match?: Maybe<JobMatch>;
  job_match_action_type: JobMatchActionTypeEnum;
  job_match_type_after?: Maybe<JobMatchTypeEnum>;
  job_match_type_before?: Maybe<JobMatchTypeEnum>;
};

export const JobMatchActionTypeEnum = {
  Application: 'APPLICATION',
  DirectIntake: 'DIRECT_INTAKE',
  /** finalizing types */
  Hired: 'HIRED',
  InitialInvitationMadeByCompanyUser: 'INITIAL_INVITATION_MADE_BY_COMPANY_USER',
  InitialInvitationMadeByTalent: 'INITIAL_INVITATION_MADE_BY_TALENT',
  InitialTalentApplication: 'INITIAL_TALENT_APPLICATION',
  /** generative types */
  InstantMatchCreated: 'INSTANT_MATCH_CREATED',
  /** transforming types */
  Invitation: 'INVITATION',
  InvitationAccepted: 'INVITATION_ACCEPTED',
  Rejected: 'REJECTED',
  Withdrawn: 'WITHDRAWN'
} as const;

export type JobMatchActionTypeEnum = typeof JobMatchActionTypeEnum[keyof typeof JobMatchActionTypeEnum];
export const JobMatchAppearanceStatusEnum = {
  New: 'NEW',
  Ordinary: 'ORDINARY',
  Recent: 'RECENT'
} as const;

export type JobMatchAppearanceStatusEnum = typeof JobMatchAppearanceStatusEnum[keyof typeof JobMatchAppearanceStatusEnum];
export const JobMatchDeclinationReasonsEnum = {
  None: 'NONE',
  NotEnoughSkills: 'NOT_ENOUGH_SKILLS'
} as const;

export type JobMatchDeclinationReasonsEnum = typeof JobMatchDeclinationReasonsEnum[keyof typeof JobMatchDeclinationReasonsEnum];
export const JobMatchTypeEnum = {
  Hired: 'HIRED',
  InstantMatch: 'INSTANT_MATCH',
  Intake: 'INTAKE',
  Invited: 'INVITED',
  OpentalentSuggestion: 'OPENTALENT_SUGGESTION',
  Rejected: 'REJECTED',
  TalentApplication: 'TALENT_APPLICATION',
  Withdrawn: 'WITHDRAWN'
} as const;

export type JobMatchTypeEnum = typeof JobMatchTypeEnum[keyof typeof JobMatchTypeEnum];
export type JobMatcher = {
  __typename?: 'JobMatcher';
  job: Job;
  talent: Talent;
};

export type JobMatcherApplication = {
  __typename?: 'JobMatcherApplication';
  id: Scalars['ID'];
  job: Job;
  matcher: Talent;
  message?: Maybe<Scalars['String']>;
  status: JobMatcherApplicationStatusEnum;
};

export const JobMatcherApplicationStatusEnum = {
  Approved: 'APPROVED',
  Canceled: 'CANCELED',
  Rejected: 'REJECTED',
  Requested: 'REQUESTED',
  Unknown: 'UNKNOWN'
} as const;

export type JobMatcherApplicationStatusEnum = typeof JobMatcherApplicationStatusEnum[keyof typeof JobMatcherApplicationStatusEnum];
export const JobMatcherJobsFilterEnum = {
  IsApplied: 'IS_APPLIED',
  IsAppliedOrAssigned: 'IS_APPLIED_OR_ASSIGNED',
  IsAssigned: 'IS_ASSIGNED',
  IsNotAppliedOrAssigned: 'IS_NOT_APPLIED_OR_ASSIGNED'
} as const;

export type JobMatcherJobsFilterEnum = typeof JobMatcherJobsFilterEnum[keyof typeof JobMatcherJobsFilterEnum];
export type JobOrderByClause = {
  /** The column that is used for ordering. */
  column: JobOrderByColumn;
  /** The direction that is used for ordering. */
  order: SortOrder;
};

/** Order by clause for the `orderBy` argument on the query `posts`. */
export const JobOrderByColumn = {
  CampaignStart: 'CAMPAIGN_START',
  RateMax: 'RATE_MAX',
  RateMin: 'RATE_MIN'
} as const;

export type JobOrderByColumn = typeof JobOrderByColumn[keyof typeof JobOrderByColumn];
/** A paginated list of Job items. */
export type JobPaginator = {
  __typename?: 'JobPaginator';
  /** A list of Job items. */
  data: Array<Job>;
  /** Pagination information about the list of items. */
  paginatorInfo: PaginatorInfo;
};

export type JobRecommendation = {
  __typename?: 'JobRecommendation';
  id: Scalars['ID'];
  job?: Maybe<Job>;
  match_quality?: Maybe<MatchQualityEnum>;
  talent?: Maybe<Talent>;
};

/** A paginated list of JobRecommendation items. */
export type JobRecommendationPaginator = {
  __typename?: 'JobRecommendationPaginator';
  /** A list of JobRecommendation items. */
  data: Array<JobRecommendation>;
  /** Pagination information about the list of items. */
  paginatorInfo: PaginatorInfo;
};

export type JobResponse = {
  __typename?: 'JobResponse';
  created_at: Scalars['DateTimeUtc'];
  job: Job;
  talent: Talent;
};

export type JobResponseCompany = {
  __typename?: 'JobResponseCompany';
  created_at: Scalars['DateTimeUtc'];
  job: Job;
  skills?: Maybe<Array<Maybe<Skill>>>;
  talent: Talent;
};

export const JobServiceTypeEnum = {
  DedicatedSearch: 'DEDICATED_SEARCH',
  SelfService: 'SELF_SERVICE'
} as const;

export type JobServiceTypeEnum = typeof JobServiceTypeEnum[keyof typeof JobServiceTypeEnum];
/** Job requested skill */
export type JobSkill = {
  __typename?: 'JobSkill';
  created_at?: Maybe<Scalars['DateTimeUtc']>;
  id: Scalars['ID'];
  job_skill_pivot?: Maybe<JobSkillPivot>;
  name: Scalars['String'];
  skill_type: SkillTypeEnum;
  slug: Scalars['String'];
  updated_at?: Maybe<Scalars['DateTimeUtc']>;
};

export type JobSkillPivot = {
  __typename?: 'JobSkillPivot';
  is_required?: Maybe<Scalars['Boolean']>;
};

export type JobSkillsBooleanFilter = {
  __typename?: 'JobSkillsBooleanFilter';
  boolean_skills_filter_items?: Maybe<Array<Maybe<JobSkillsBooleanFilterItem>>>;
  not_skills?: Maybe<Array<Maybe<Skill>>>;
};

export type JobSkillsBooleanFilterItem = {
  __typename?: 'JobSkillsBooleanFilterItem';
  all_of_skills?: Maybe<Array<Maybe<Skill>>>;
  boolean_operator?: Maybe<BooleanOperatorsEnum>;
  one_of_skills?: Maybe<Array<Maybe<Skill>>>;
};

export const JobSourceType = {
  Coresignal: 'CORESIGNAL',
  Internal: 'INTERNAL'
} as const;

export type JobSourceType = typeof JobSourceType[keyof typeof JobSourceType];
export const JobStatusEnum = {
  Active: 'ACTIVE',
  Archived: 'ARCHIVED',
  Draft: 'DRAFT'
} as const;

export type JobStatusEnum = typeof JobStatusEnum[keyof typeof JobStatusEnum];
export const JobTypeEnum = {
  Freelance: 'FREELANCE',
  Permanent: 'PERMANENT',
  Project: 'PROJECT'
} as const;

export type JobTypeEnum = typeof JobTypeEnum[keyof typeof JobTypeEnum];
export type JobsCountByStatus = {
  __typename?: 'JobsCountByStatus';
  count: Scalars['Int'];
  type: JobStatusEnum;
};

export const JoinReasonEnum = {
  Candidates: 'CANDIDATES',
  Freelance: 'FREELANCE',
  Networking: 'NETWORKING',
  PerksAndBenefits: 'PERKS_AND_BENEFITS',
  PermanentWork: 'PERMANENT_WORK',
  VentureBuilding: 'VENTURE_BUILDING'
} as const;

export type JoinReasonEnum = typeof JoinReasonEnum[keyof typeof JoinReasonEnum];
export const MatchQualityEnum = {
  Fair: 'FAIR',
  Good: 'GOOD',
  Poor: 'POOR'
} as const;

export type MatchQualityEnum = typeof MatchQualityEnum[keyof typeof MatchQualityEnum];
export type Mutation = {
  __typename?: 'Mutation';
  /** Talent user pooling actions - accept company pool invitation */
  acceptCompanyPoolInvitation?: Maybe<Scalars['Boolean']>;
  /** Talent user pooling actions - apply to company pool */
  applyCurrentTalentToCompanyPool?: Maybe<Scalars['Boolean']>;
  /** Talent flow - create a job application */
  applyForJob: Scalars['Boolean'];
  /** Approve action for company user who owns the job */
  approveJobMatcherApplication?: Maybe<JobMatcherApplication>;
  /** Company user pooling action - approve talent */
  approveTalentCompanyPoolApplication?: Maybe<Scalars['Boolean']>;
  /** Talent flow - cancel existing application */
  cancelApplicationForJob: Scalars['Boolean'];
  cancelCurrentCompanyUserCompanySubscription?: Maybe<StripeSubscription>;
  cancelCurrentTalentSubscription?: Maybe<StripeSubscription>;
  /** Cancel invitation */
  cancelInvitation?: Maybe<Scalars['Boolean']>;
  changeNotificationReadStatus: Scalars['Boolean'];
  /** Invite form for * (particular company, name is hidden for safety reason) - Deprecated */
  companyInviteFormSubmit: Scalars['Boolean'];
  /** Create announcement for talent */
  createAnnouncement: Scalars['Boolean'];
  createChatMessage?: Maybe<ChatMessage>;
  /** Create chief's program application. Return true in case of successful creation */
  createChiefsProgramApplication: Scalars['Boolean'];
  createCompany?: Maybe<Company>;
  createCompanyAccountAccessRequest?: Maybe<Scalars['Boolean']>;
  /** [Deprecated] Create new company and attach it to current company user */
  createCompanyForCurrentUser?: Maybe<Company>;
  /** create new company and attach it to current company user */
  createCompanyForUser?: Maybe<Company>;
  /** Create new company user account */
  createCompanyUserProfile?: Maybe<User>;
  /** Create invitation */
  createInvitation?: Maybe<TalentInvitation>;
  /** Create invitation link */
  createInvitationLink?: Maybe<InvitationLink>;
  /** Create job (for company user flow) */
  createJob: Scalars['ID'];
  /** Create job matcher application for the job (for current talent) */
  createJobMatcherApplication?: Maybe<JobMatcherApplication>;
  /** Talent flow - create a suggestion of the talent for a job */
  createJobSuggestion: Scalars['Boolean'];
  /** Add new skill to catalog */
  createSkill: Skill;
  createTag?: Maybe<Tag>;
  /** Create talent account from invitation link */
  createTalentAccountFromInvitationLink?: Maybe<Scalars['Boolean']>;
  /** Create 'Talent by Talent' invitation */
  createTalentByTalentInvitation?: Maybe<Talent>;
  /** Create talent profile */
  createTalentProfile?: Maybe<Scalars['ID']>;
  /** Create untrusted talent profile */
  createUntrustedTalentProfile?: Maybe<Scalars['ID']>;
  currentUserNotificationsMarkAllAsRead: Scalars['Boolean'];
  /** Talent flow - decline job invitation */
  declineJobInvitation?: Maybe<Scalars['Boolean']>;
  /** Decline job match */
  declineJobMatch?: Maybe<Scalars['Boolean']>;
  /** [Deprecated] Verify talent account */
  declineTalentAccount?: Maybe<Scalars['Boolean']>;
  deleteCompanyUserProfile?: Maybe<Scalars['Boolean']>;
  /** Delete external job from favorites */
  deleteExternalJobFromFavorites?: Maybe<Scalars['Boolean']>;
  /** Talent flow - delete job from favorites */
  deleteJobFromFavorites?: Maybe<Scalars['Boolean']>;
  deleteMatchFromShortList?: Maybe<Scalars['Boolean']>;
  deleteNotification: Scalars['Boolean'];
  /** Hire job match */
  hireJobMatch?: Maybe<Scalars['Boolean']>;
  /** Decline job match */
  intakeJobMatch?: Maybe<Scalars['Boolean']>;
  inviteCompanyToOpentalent?: Maybe<Scalars['Boolean']>;
  inviteCompanyUser?: Maybe<Scalars['Boolean']>;
  /** Invite form for company's public invitation form */
  inviteFormSubmit: Scalars['Boolean'];
  /** Company user pooling action - invite talent to company pool of current company user */
  inviteTalentToCompanyPool?: Maybe<Scalars['Boolean']>;
  /** Company user flow - invite talent to job  */
  inviteToJob?: Maybe<Scalars['Boolean']>;
  /** Talent user pooling actions - leave from company pool */
  leaveCurrentTalentFromCompanyPool?: Maybe<Scalars['Boolean']>;
  markNewJobMatchesAsRead?: Maybe<Scalars['Boolean']>;
  notifyTalentAboutJobMatch?: Maybe<Scalars['Boolean']>;
  /** Talent user pooling actions - reject company pool invitation */
  rejectCompanyPoolInvitation?: Maybe<Scalars['Boolean']>;
  /** Reject action for company user who owns the job */
  rejectJobMatcherApplication?: Maybe<JobMatcherApplication>;
  /** Company user pooling action - reject talent */
  rejectTalentCompanyPoolApplication?: Maybe<Scalars['Boolean']>;
  /** Remind talent to activate account */
  remindToActivateTalentAccount?: Maybe<Scalars['Boolean']>;
  /** Cancel matcher rights for the job */
  removeJobMatcherFromJob?: Maybe<Scalars['Boolean']>;
  /** Company user pooling action - remove connected talent from pool */
  removeTalentFromCompanyPool?: Maybe<Scalars['Boolean']>;
  renewCurrentCompanyUserCompanySubscription?: Maybe<StripeSubscription>;
  renewCurrentTalentSubscription?: Maybe<StripeSubscription>;
  /** Request contract */
  requestOTContract: Scalars['Boolean'];
  /** Resend invitation notification email (if not accepted and has email) */
  resendInvitation?: Maybe<Scalars['Boolean']>;
  /** Restore talent account by delete key */
  restoreDeletedAccountByDeleteKey?: Maybe<RestoreDeletedAccountInfo>;
  /** Company user pooling action - revoke talent invitation to company pool */
  revokeCompanyPoolApplication?: Maybe<Scalars['Boolean']>;
  /** Company user pooling action - revoke talent invitation to company pool */
  revokeCompanyPoolInvitation?: Maybe<Scalars['Boolean']>;
  /** Save external job to favorites */
  saveExternalJobToFavorites?: Maybe<Scalars['Boolean']>;
  /** Talent flow - save job to favorite */
  saveJobToFavorites?: Maybe<Scalars['Boolean']>;
  saveMatchToShortList?: Maybe<Scalars['Boolean']>;
  /** Delete talent account by delete key */
  selfDeleteAccount?: Maybe<Scalars['Boolean']>;
  /** Add company-specific tag to talent (can be added only by company user) */
  setTagsToTalent?: Maybe<Scalars['Boolean']>;
  /** Save temp data for user/talent */
  setTempDataItem?: Maybe<Scalars['Boolean']>;
  /** Suggest a skill to add to the list of skills */
  suggestSkill: Skill;
  updateCompany?: Maybe<Company>;
  /** Update company user profile */
  updateCompanyUserProfile?: Maybe<User>;
  /** Update job (for company user flow) */
  updateJob: Job;
  updateNotification: Notification;
  /** Update talent profile */
  updateTalentProfile?: Maybe<Scalars['ID']>;
  /** Upload talent avatar */
  uploadCompanyLogo?: Maybe<CompanyLogo>;
  /** Upload file */
  uploadFile?: Maybe<File>;
  uploadFileTest?: Maybe<File>;
  /** Upload multiple files */
  uploadFiles?: Maybe<Array<Maybe<File>>>;
  /** Upload talent avatar */
  uploadTalentAvatar?: Maybe<Avatar>;
  /** Upload talent documents */
  uploadTalentDocument?: Maybe<Array<Maybe<TalentDocument>>>;
  /** Upload user avatar */
  uploadUserAvatar?: Maybe<Avatar>;
  /** [Deprecated] Verify talent account */
  verifyTalentAccount?: Maybe<Scalars['Boolean']>;
};


export type MutationAcceptCompanyPoolInvitationArgs = {
  company_id: Scalars['ID'];
};


export type MutationApplyCurrentTalentToCompanyPoolArgs = {
  company_id: Scalars['ID'];
};


export type MutationApplyForJobArgs = {
  job_application_data: JobApplicationInput;
};


export type MutationApproveJobMatcherApplicationArgs = {
  id: Scalars['ID'];
};


export type MutationApproveTalentCompanyPoolApplicationArgs = {
  talent_id: Scalars['ID'];
};


export type MutationCancelApplicationForJobArgs = {
  job_id: Scalars['ID'];
  reason?: InputMaybe<Scalars['String']>;
};


export type MutationCancelInvitationArgs = {
  id: Scalars['ID'];
};


export type MutationChangeNotificationReadStatusArgs = {
  new_status: Scalars['Boolean'];
  notification_id: Scalars['ID'];
};


export type MutationCompanyInviteFormSubmitArgs = {
  email: Scalars['Email'];
  first_name: Scalars['String'];
  last_name: Scalars['String'];
};


export type MutationCreateAnnouncementArgs = {
  need_response: Scalars['Boolean'];
  send_email?: InputMaybe<Scalars['Boolean']>;
  talents_ids: Array<InputMaybe<Scalars['ID']>>;
  text?: InputMaybe<Scalars['String']>;
  title: Scalars['String'];
};


export type MutationCreateChatMessageArgs = {
  message: Scalars['String'];
  parent_id?: InputMaybe<Scalars['ID']>;
  talent_id: Scalars['ID'];
};


export type MutationCreateChiefsProgramApplicationArgs = {
  talent_id: Scalars['ID'];
};


export type MutationCreateCompanyArgs = {
  address?: InputMaybe<Scalars['String']>;
  logo?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  type_of_activity?: InputMaybe<Scalars['String']>;
  website?: InputMaybe<Scalars['String']>;
};


export type MutationCreateCompanyAccountAccessRequestArgs = {
  company_id?: InputMaybe<Scalars['ID']>;
  email: Scalars['Email'];
  first_name: Scalars['String'];
  last_name: Scalars['String'];
  new_company_name?: InputMaybe<Scalars['String']>;
  referral_key?: InputMaybe<Scalars['String']>;
};


export type MutationCreateCompanyForCurrentUserArgs = {
  address?: InputMaybe<Scalars['String']>;
  logo?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  type_of_activity?: InputMaybe<Scalars['String']>;
  website?: InputMaybe<Scalars['String']>;
};


export type MutationCreateCompanyForUserArgs = {
  address?: InputMaybe<Scalars['String']>;
  logo?: InputMaybe<Scalars['String']>;
  name: Scalars['String'];
  primary_company_user_id?: InputMaybe<Scalars['ID']>;
  type_of_activity?: InputMaybe<Scalars['String']>;
  website?: InputMaybe<Scalars['String']>;
};


export type MutationCreateCompanyUserProfileArgs = {
  avatar?: InputMaybe<Scalars['String']>;
  email: Scalars['Email'];
  first_name: Scalars['String'];
  is_onboarding?: InputMaybe<Scalars['Boolean']>;
  is_primary?: InputMaybe<Scalars['Boolean']>;
  last_name: Scalars['String'];
  onboarding_type?: InputMaybe<CompanyUserOnboardingTypeEnum>;
  position?: InputMaybe<Scalars['String']>;
  referral_key?: InputMaybe<Scalars['String']>;
};


export type MutationCreateInvitationArgs = {
  company_id?: InputMaybe<Scalars['ID']>;
  email?: InputMaybe<Scalars['String']>;
  first_name?: InputMaybe<Scalars['String']>;
  inviter_company_user_id?: InputMaybe<Scalars['ID']>;
  inviter_talent_id?: InputMaybe<Scalars['ID']>;
  last_name?: InputMaybe<Scalars['String']>;
  tags_ids?: InputMaybe<Array<Scalars['ID']>>;
};


export type MutationCreateInvitationLinkArgs = {
  inviting_talent_id: Scalars['ID'];
  name: Scalars['String'];
};


export type MutationCreateJobArgs = {
  campaign_end_date: Scalars['DateTimeUtc'];
  campaign_owner_id: Scalars['ID'];
  campaign_start_date: Scalars['DateTimeUtc'];
  campaign_talent_pool?: InputMaybe<Array<InputMaybe<SourceTypeEnum>>>;
  category_id?: InputMaybe<Scalars['ID']>;
  city?: InputMaybe<Scalars['String']>;
  client?: InputMaybe<Scalars['String']>;
  country?: InputMaybe<Scalars['String']>;
  description: Scalars['String'];
  end_date?: InputMaybe<Scalars['DateTimeUtc']>;
  finders_fee?: InputMaybe<Scalars['Float']>;
  hard_skills_required?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  hours_per_week?: InputMaybe<Scalars['Int']>;
  is_archived?: InputMaybe<Scalars['Boolean']>;
  is_draft?: InputMaybe<Scalars['Boolean']>;
  is_rate_negotiable?: InputMaybe<Scalars['Boolean']>;
  is_remote_an_option?: InputMaybe<Scalars['Boolean']>;
  is_salary_negotiable?: InputMaybe<Scalars['Boolean']>;
  location?: InputMaybe<Scalars['String']>;
  location_type?: InputMaybe<JobLocationTypeEnum>;
  max_project_budget?: InputMaybe<Scalars['Float']>;
  name: Scalars['String'];
  office_hours_per_month?: InputMaybe<Scalars['Float']>;
  permanent_capacity_type?: InputMaybe<PermanentJobCapacityTypeEnum>;
  pitch?: InputMaybe<Scalars['String']>;
  propose_to_matchers?: InputMaybe<Scalars['Boolean']>;
  rate_max?: InputMaybe<Scalars['Float']>;
  rate_min?: InputMaybe<Scalars['Float']>;
  salary_max?: InputMaybe<Scalars['Float']>;
  salary_min?: InputMaybe<Scalars['Float']>;
  service_type?: InputMaybe<JobServiceTypeEnum>;
  skills?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  skills_boolean_filter?: InputMaybe<TalentsBooleanSkillsFilterInput>;
  solutions_required?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  start_date?: InputMaybe<Scalars['DateTimeUtc']>;
  subcategory_id?: InputMaybe<Scalars['ID']>;
  type?: InputMaybe<JobTypeEnum>;
};


export type MutationCreateJobMatcherApplicationArgs = {
  job_id: Scalars['ID'];
  message?: InputMaybe<Scalars['String']>;
};


export type MutationCreateJobSuggestionArgs = {
  email: Scalars['String'];
  job_id: Scalars['ID'];
  name: Scalars['String'];
};


export type MutationCreateSkillArgs = {
  name: Scalars['String'];
  skill_type: SkillTypeEnum;
};


export type MutationCreateTagArgs = {
  name?: InputMaybe<Scalars['String']>;
};


export type MutationCreateTalentAccountFromInvitationLinkArgs = {
  email: Scalars['Email'];
  key: Scalars['String'];
};


export type MutationCreateTalentByTalentInvitationArgs = {
  email: Scalars['Email'];
  inviting_talent_id: Scalars['ID'];
  name: Scalars['String'];
};


export type MutationCreateTalentProfileArgs = {
  email: Scalars['Email'];
  general_info: TalentGeneralInfoInput;
  is_ot_pool?: InputMaybe<Scalars['Boolean']>;
  job_application_data?: InputMaybe<JobApplicationInput>;
  origin?: InputMaybe<TalentOriginEnum>;
  tags_ids?: InputMaybe<Array<Scalars['ID']>>;
};


export type MutationCreateUntrustedTalentProfileArgs = {
  email: Scalars['Email'];
  general_info: TalentGeneralInfoInput;
  invitation_uid?: InputMaybe<Scalars['String']>;
  job_application_data?: InputMaybe<JobApplicationInput>;
  origin?: InputMaybe<TalentOriginEnum>;
  source_company_id?: InputMaybe<Scalars['ID']>;
};


export type MutationDeclineJobInvitationArgs = {
  job_invitation_id: Scalars['ID'];
  reason?: InputMaybe<Scalars['String']>;
};


export type MutationDeclineJobMatchArgs = {
  job_match_id: Scalars['ID'];
  message?: InputMaybe<Scalars['String']>;
  reason?: InputMaybe<JobMatchDeclinationReasonsEnum>;
};


export type MutationDeclineTalentAccountArgs = {
  talent_id: Scalars['ID'];
};


export type MutationDeleteCompanyUserProfileArgs = {
  company_user_id: Scalars['ID'];
};


export type MutationDeleteExternalJobFromFavoritesArgs = {
  external_job_id: Scalars['ID'];
};


export type MutationDeleteJobFromFavoritesArgs = {
  job_id: Scalars['ID'];
};


export type MutationDeleteMatchFromShortListArgs = {
  match_id: Scalars['ID'];
};


export type MutationDeleteNotificationArgs = {
  notification_id: Scalars['ID'];
};


export type MutationHireJobMatchArgs = {
  job_match_id: Scalars['ID'];
  with_contract?: InputMaybe<Scalars['Boolean']>;
};


export type MutationIntakeJobMatchArgs = {
  job_match_id: Scalars['ID'];
};


export type MutationInviteCompanyToOpentalentArgs = {
  company_email: Scalars['String'];
  company_name: Scalars['String'];
  contact_person: Scalars['String'];
};


export type MutationInviteCompanyUserArgs = {
  email: Scalars['Email'];
};


export type MutationInviteFormSubmitArgs = {
  company_id: Scalars['ID'];
  email: Scalars['Email'];
  first_name: Scalars['String'];
  last_name: Scalars['String'];
};


export type MutationInviteTalentToCompanyPoolArgs = {
  talent_id: Scalars['ID'];
};


export type MutationInviteToJobArgs = {
  job_id: Scalars['ID'];
  message?: InputMaybe<Scalars['String']>;
  talent_id: Scalars['ID'];
};


export type MutationLeaveCurrentTalentFromCompanyPoolArgs = {
  company_id: Scalars['ID'];
};


export type MutationMarkNewJobMatchesAsReadArgs = {
  job_matches_ids?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
};


export type MutationNotifyTalentAboutJobMatchArgs = {
  match_id: Scalars['ID'];
};


export type MutationRejectCompanyPoolInvitationArgs = {
  company_id: Scalars['ID'];
};


export type MutationRejectJobMatcherApplicationArgs = {
  id: Scalars['ID'];
};


export type MutationRejectTalentCompanyPoolApplicationArgs = {
  message?: InputMaybe<Scalars['String']>;
  talent_id: Scalars['ID'];
};


export type MutationRemindToActivateTalentAccountArgs = {
  talent_id: Scalars['ID'];
};


export type MutationRemoveJobMatcherFromJobArgs = {
  job_id: Scalars['ID'];
  matcher_id: Scalars['ID'];
};


export type MutationRemoveTalentFromCompanyPoolArgs = {
  message?: InputMaybe<Scalars['String']>;
  talent_id: Scalars['ID'];
};


export type MutationResendInvitationArgs = {
  id: Scalars['ID'];
};


export type MutationRestoreDeletedAccountByDeleteKeyArgs = {
  delete_key: Scalars['String'];
};


export type MutationRevokeCompanyPoolApplicationArgs = {
  talent_id: Scalars['ID'];
};


export type MutationRevokeCompanyPoolInvitationArgs = {
  talent_id: Scalars['ID'];
};


export type MutationSaveExternalJobToFavoritesArgs = {
  external_job_id: Scalars['ID'];
};


export type MutationSaveJobToFavoritesArgs = {
  job_id: Scalars['ID'];
};


export type MutationSaveMatchToShortListArgs = {
  match_id: Scalars['ID'];
};


export type MutationSelfDeleteAccountArgs = {
  delete_key: Scalars['String'];
};


export type MutationSetTagsToTalentArgs = {
  tags_ids: Array<Scalars['ID']>;
  talent_id: Scalars['ID'];
};


export type MutationSetTempDataItemArgs = {
  data?: InputMaybe<Scalars['String']>;
  device_id: Scalars['String'];
  email?: InputMaybe<Scalars['Email']>;
};


export type MutationSuggestSkillArgs = {
  name: Scalars['String'];
  reason?: InputMaybe<Scalars['String']>;
  skill_type: SkillTypeEnum;
};


export type MutationUpdateCompanyArgs = {
  about?: InputMaybe<Scalars['String']>;
  address?: InputMaybe<Scalars['String']>;
  company_id: Scalars['ID'];
  country?: InputMaybe<Scalars['String']>;
  logo?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  size?: InputMaybe<Scalars['String']>;
  type_of_activity?: InputMaybe<Scalars['String']>;
  website?: InputMaybe<Scalars['String']>;
};


export type MutationUpdateCompanyUserProfileArgs = {
  avatar?: InputMaybe<Scalars['String']>;
  company_user_id: Scalars['ID'];
  email?: InputMaybe<Scalars['Email']>;
  first_name?: InputMaybe<Scalars['String']>;
  last_name?: InputMaybe<Scalars['String']>;
  position?: InputMaybe<Scalars['String']>;
};


export type MutationUpdateJobArgs = {
  campaign_end_date?: InputMaybe<Scalars['DateTimeUtc']>;
  campaign_owner_id?: InputMaybe<Scalars['ID']>;
  campaign_start_date?: InputMaybe<Scalars['DateTimeUtc']>;
  campaign_talent_pool?: InputMaybe<Array<InputMaybe<SourceTypeEnum>>>;
  category_id?: InputMaybe<Scalars['ID']>;
  city?: InputMaybe<Scalars['String']>;
  client?: InputMaybe<Scalars['String']>;
  country?: InputMaybe<Scalars['String']>;
  description?: InputMaybe<Scalars['String']>;
  end_date?: InputMaybe<Scalars['DateTimeUtc']>;
  finders_fee?: InputMaybe<Scalars['Float']>;
  hard_skills_required?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  hours_per_week?: InputMaybe<Scalars['Int']>;
  id: Scalars['ID'];
  is_archived?: InputMaybe<Scalars['Boolean']>;
  is_draft?: InputMaybe<Scalars['Boolean']>;
  is_rate_negotiable?: InputMaybe<Scalars['Boolean']>;
  is_remote_an_option?: InputMaybe<Scalars['Boolean']>;
  is_salary_negotiable?: InputMaybe<Scalars['Boolean']>;
  location?: InputMaybe<Scalars['String']>;
  location_type?: InputMaybe<JobLocationTypeEnum>;
  max_project_budget?: InputMaybe<Scalars['Float']>;
  name?: InputMaybe<Scalars['String']>;
  office_hours_per_month?: InputMaybe<Scalars['Float']>;
  permanent_capacity_type?: InputMaybe<PermanentJobCapacityTypeEnum>;
  pitch?: InputMaybe<Scalars['String']>;
  propose_to_matchers?: InputMaybe<Scalars['Boolean']>;
  rate_max?: InputMaybe<Scalars['Float']>;
  rate_min?: InputMaybe<Scalars['Float']>;
  salary_max?: InputMaybe<Scalars['Float']>;
  salary_min?: InputMaybe<Scalars['Float']>;
  service_type?: InputMaybe<JobServiceTypeEnum>;
  skills?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  skills_boolean_filter?: InputMaybe<TalentsBooleanSkillsFilterInput>;
  solutions_required?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  start_date?: InputMaybe<Scalars['DateTimeUtc']>;
  subcategory_id?: InputMaybe<Scalars['ID']>;
  type?: InputMaybe<JobTypeEnum>;
  update_auto_matching?: InputMaybe<Scalars['Boolean']>;
};


export type MutationUpdateNotificationArgs = {
  id: Scalars['ID'];
  is_read?: InputMaybe<Scalars['Boolean']>;
};


export type MutationUpdateTalentProfileArgs = {
  about?: InputMaybe<Scalars['String']>;
  account_info?: InputMaybe<TalentAccountVerificationInput>;
  account_settings?: InputMaybe<TalentAccountSettingsInput>;
  available_date?: InputMaybe<Scalars['DateTimeUtc']>;
  available_now?: InputMaybe<Scalars['Boolean']>;
  avatar?: InputMaybe<Scalars['String']>;
  companies?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  documents?: InputMaybe<Array<InputMaybe<Scalars['String']>>>;
  general_info?: InputMaybe<TalentGeneralInfoInput>;
  hours_per_week?: InputMaybe<Scalars['Int']>;
  skills?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  talent_id: Scalars['ID'];
  work_history?: InputMaybe<Array<InputMaybe<WorkHistoryInput>>>;
};


export type MutationUploadCompanyLogoArgs = {
  company_id: Scalars['ID'];
  file: Scalars['Upload'];
};


export type MutationUploadFileArgs = {
  file: Scalars['Upload'];
  file_type: FileTypeEnum;
  owner_id?: InputMaybe<Scalars['ID']>;
};


export type MutationUploadFileTestArgs = {
  file: Scalars['Upload'];
  file_type: FileTypeEnum;
  owner_id: Scalars['ID'];
};


export type MutationUploadFilesArgs = {
  files?: InputMaybe<Array<InputMaybe<FileInput>>>;
};


export type MutationUploadTalentAvatarArgs = {
  file: Scalars['Upload'];
  talent_id: Scalars['ID'];
};


export type MutationUploadTalentDocumentArgs = {
  files: Array<Scalars['Upload']>;
  talent_id: Scalars['ID'];
};


export type MutationUploadUserAvatarArgs = {
  file: Scalars['Upload'];
  user_id: Scalars['ID'];
};


export type MutationVerifyTalentAccountArgs = {
  talent_id: Scalars['ID'];
};

/** Any object implementing this type can be found by ID through `Query.node`. */
export type Node = {
  /** Global identifier that can be used to resolve any Node implementation. */
  id: Scalars['ID'];
};

export type Notification = {
  __typename?: 'Notification';
  buttons?: Maybe<Array<Maybe<NotificationButton>>>;
  created_at: Scalars['DateTime'];
  id: Scalars['ID'];
  is_read: Scalars['Boolean'];
  message: NotificationMessage;
  recipient_id: Scalars['ID'];
  recipient_type: NotificationRecipientTypeEnum;
  related_job_id?: Maybe<Scalars['ID']>;
  reply_to?: Maybe<User>;
  title?: Maybe<Scalars['String']>;
};

export type NotificationActionInterface = {
  type?: Maybe<NotificationActionTypeEnum>;
};

export type NotificationActionRedirect = NotificationActionInterface & {
  __typename?: 'NotificationActionRedirect';
  target_route_id: Scalars['ID'];
  target_route_type?: Maybe<RedirectTargetRouteTypeEnum>;
  type?: Maybe<NotificationActionTypeEnum>;
};

export const NotificationActionTypeEnum = {
  Confirm: 'CONFIRM',
  Redirect: 'REDIRECT'
} as const;

export type NotificationActionTypeEnum = typeof NotificationActionTypeEnum[keyof typeof NotificationActionTypeEnum];
export type NotificationButton = {
  __typename?: 'NotificationButton';
  action: NotificationActionInterface;
  text: Scalars['String'];
};

export type NotificationMessage = {
  __typename?: 'NotificationMessage';
  template?: Maybe<Scalars['String']>;
  template_actions?: Maybe<Array<Maybe<NotificationTemplateAction>>>;
};

/** A paginated list of Notification items. */
export type NotificationPaginator = {
  __typename?: 'NotificationPaginator';
  /** A list of Notification items. */
  data: Array<Notification>;
  /** Pagination information about the list of items. */
  paginatorInfo: PaginatorInfo;
};

export const NotificationRecipientTypeEnum = {
  CompanyUser: 'COMPANY_USER',
  Talent: 'TALENT'
} as const;

export type NotificationRecipientTypeEnum = typeof NotificationRecipientTypeEnum[keyof typeof NotificationRecipientTypeEnum];
export type NotificationTemplateAction = {
  __typename?: 'NotificationTemplateAction';
  action?: Maybe<NotificationActionInterface>;
  key?: Maybe<Scalars['String']>;
  text?: Maybe<Scalars['String']>;
};

export type OtContract = {
  __typename?: 'OTContract';
  id: Scalars['ID'];
  read_contacts_permission: Scalars['Boolean'];
  status: OtContractStatusEnum;
};

/** Status of the contract between company and Opentalent */
export const OtContractStatusEnum = {
  Requested: 'REQUESTED',
  SentToCompany: 'SENT_TO_COMPANY',
  Signed: 'SIGNED',
  Unknown: 'UNKNOWN'
} as const;

export type OtContractStatusEnum = typeof OtContractStatusEnum[keyof typeof OtContractStatusEnum];
/** Allows ordering a list of records. */
export type OrderByClause = {
  /** The column that is used for ordering. */
  column: Scalars['String'];
  /** The direction that is used for ordering. */
  order: SortOrder;
};

/** Aggregate functions when ordering by a relation without specifying a column. */
export const OrderByRelationAggregateFunction = {
  /** Amount of items. */
  Count: 'COUNT'
} as const;

export type OrderByRelationAggregateFunction = typeof OrderByRelationAggregateFunction[keyof typeof OrderByRelationAggregateFunction];
/** Aggregate functions when ordering by a relation that may specify a column. */
export const OrderByRelationWithColumnAggregateFunction = {
  /** Average. */
  Avg: 'AVG',
  /** Amount of items. */
  Count: 'COUNT',
  /** Maximum. */
  Max: 'MAX',
  /** Minimum. */
  Min: 'MIN',
  /** Sum. */
  Sum: 'SUM'
} as const;

export type OrderByRelationWithColumnAggregateFunction = typeof OrderByRelationWithColumnAggregateFunction[keyof typeof OrderByRelationWithColumnAggregateFunction];
/** Information about pagination using a Relay style cursor connection. */
export type PageInfo = {
  __typename?: 'PageInfo';
  /** Number of nodes in the current page. */
  count: Scalars['Int'];
  /** Index of the current page. */
  currentPage: Scalars['Int'];
  /** The cursor to continue paginating forwards. */
  endCursor?: Maybe<Scalars['String']>;
  /** When paginating forwards, are there more items? */
  hasNextPage: Scalars['Boolean'];
  /** When paginating backwards, are there more items? */
  hasPreviousPage: Scalars['Boolean'];
  /** Index of the last available page. */
  lastPage: Scalars['Int'];
  /** The cursor to continue paginating backwards. */
  startCursor?: Maybe<Scalars['String']>;
  /** Total number of nodes in the paginated connection. */
  total: Scalars['Int'];
};

/** Information about pagination using a fully featured paginator. */
export type PaginatorInfo = {
  __typename?: 'PaginatorInfo';
  /** Number of items in the current page. */
  count: Scalars['Int'];
  /** Index of the current page. */
  currentPage: Scalars['Int'];
  /** Index of the first item in the current page. */
  firstItem?: Maybe<Scalars['Int']>;
  /** Are there more pages after this one? */
  hasMorePages: Scalars['Boolean'];
  /** Index of the last item in the current page. */
  lastItem?: Maybe<Scalars['Int']>;
  /** Index of the last available page. */
  lastPage: Scalars['Int'];
  /** Number of items per page. */
  perPage: Scalars['Int'];
  /** Number of total available items. */
  total: Scalars['Int'];
};

export type Perk = {
  __typename?: 'Perk';
  category?: Maybe<Scalars['String']>;
  company_name?: Maybe<Scalars['String']>;
  country?: Maybe<Scalars['String']>;
  description?: Maybe<Scalars['String']>;
  discount?: Maybe<Scalars['String']>;
  get_perk_link?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  learn_more_link?: Maybe<Scalars['String']>;
  logo_url?: Maybe<Scalars['String']>;
  promoCode?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
};

/** A paginated list of Perk items. */
export type PerkPaginator = {
  __typename?: 'PerkPaginator';
  /** A list of Perk items. */
  data: Array<Perk>;
  /** Pagination information about the list of items. */
  paginatorInfo: PaginatorInfo;
};

export const PermanentJobCapacityTypeEnum = {
  FullTime: 'FULL_TIME',
  PartTime: 'PART_TIME'
} as const;

export type PermanentJobCapacityTypeEnum = typeof PermanentJobCapacityTypeEnum[keyof typeof PermanentJobCapacityTypeEnum];
export type Query = {
  __typename?: 'Query';
  ATSRecords?: Maybe<Array<Maybe<TalentLogRecordInterface>>>;
  ATSRecordsUnion?: Maybe<Array<Maybe<AtsRecordsUnion>>>;
  availableJobsForTalent?: Maybe<Array<Maybe<AvailableJobForTalent>>>;
  categoryAnalytics?: Maybe<Array<Maybe<DashboardAnalyticsItem>>>;
  chatMessage?: Maybe<ChatMessage>;
  chatMessages?: Maybe<ChatMessagePaginator>;
  /** Check company user existence by email */
  checkCompanyUserExistsByEmail: Scalars['Boolean'];
  /** Check talent existance by email */
  checkTalentExistsByEmail: Scalars['Boolean'];
  commonAppInfo?: Maybe<CommonAppInfo>;
  companies?: Maybe<CompanyPaginator>;
  company?: Maybe<Company>;
  /** Publicly available company info. No auth required. */
  companyPublicInfo?: Maybe<CompanyPublicInfo>;
  countryAnalytics?: Maybe<Array<Maybe<DashboardAnalyticsItem>>>;
  /** Current company user */
  currentCompanyUser: User;
  currentCompanyUserColleagues: Array<Maybe<User>>;
  currentCompanyUserCompanySubscription?: Maybe<StripeSubscription>;
  currentCompanyUserJobBoardInfo: JobBoardInfo;
  /** Numbres of jobs by job status */
  currentCompanyUserJobsCountByStatus: Array<Maybe<JobsCountByStatus>>;
  /** Current talent */
  currentTalent: Talent;
  currentTalentExternalJobBoardSearch?: Maybe<ExternalJobPaginator>;
  currentTalentExternalJobRecommendations?: Maybe<Array<Maybe<ExternalJob>>>;
  /** Invitation Links belonging to current talent */
  currentTalentInvitationLinks?: Maybe<Array<Maybe<InvitationLink>>>;
  /** Invitations made by current talent */
  currentTalentInvitations?: Maybe<Array<Maybe<TalentInvitation>>>;
  currentTalentJobApplicationByJobId?: Maybe<JobApplication>;
  currentTalentJobBoardSearch?: Maybe<JobPaginator>;
  currentTalentJobRecommedations?: Maybe<JobRecommendationPaginator>;
  currentTalentSubscription?: Maybe<StripeSubscription>;
  currentUserCompanyTags?: Maybe<Array<Maybe<Tag>>>;
  currentUserNotifications?: Maybe<NotificationPaginator>;
  currentUserUnreadNotificationsCount?: Maybe<Scalars['Int']>;
  externalJob: ExternalJob;
  externalJobCompanies?: Maybe<Array<Maybe<Scalars['String']>>>;
  generalAnalytics?: Maybe<Array<Maybe<DashboardAnalyticsItem>>>;
  getPaymentLinkForCompany?: Maybe<Scalars['String']>;
  getPaymentLinkForMatcher?: Maybe<Scalars['String']>;
  getPaymentLinkForTalent?: Maybe<Scalars['String']>;
  getTempDataItem: TempDataItemSearchResult;
  getTypeformPaymentLinkCodeForMatcher?: Maybe<Scalars['String']>;
  /** Public information for Invitation Link by public key */
  invitationLinkInfo?: Maybe<InvitationLinkInfo>;
  job?: Maybe<Job>;
  jobInvitation?: Maybe<JobInvitation>;
  jobMatcherApplications?: Maybe<Array<Maybe<JobMatcherApplication>>>;
  jobMatchers?: Maybe<Array<Maybe<JobMatcher>>>;
  jobMatches?: Maybe<Array<Maybe<JobMatch>>>;
  /** List of Jobs in jobs board */
  jobs?: Maybe<JobPaginator>;
  lastTalents?: Maybe<Array<Maybe<Talent>>>;
  node?: Maybe<Node>;
  perks?: Maybe<PerkPaginator>;
  perksCategories: Array<Scalars['String']>;
  poolingCompanies?: Maybe<Array<Maybe<Company>>>;
  /** Number of potential talents for job */
  potentialTalentsForJobCount: Scalars['Int'];
  /** Data for 'Delete my account' page */
  selfDeletePageInfo?: Maybe<SelfDeletePageInfo>;
  /** Send 'Talents' report to current company user's email */
  sendTalentsReportToCurrentCompanyUserEmail?: Maybe<Scalars['Boolean']>;
  /** Get skill by id */
  skill?: Maybe<Skill>;
  /** Get skill by slug */
  skillBySlug?: Maybe<Skill>;
  /** Check if skill exists by name */
  skillExists?: Maybe<Skill>;
  /** Search skills by substring occurrence */
  skillSearch: Array<Skill>;
  /** Get skills by skill type */
  skills?: Maybe<SkillPaginator>;
  tagsAnalytics?: Maybe<Array<Maybe<DashboardAnalyticsItem>>>;
  talent?: Maybe<Talent>;
  /** Talent categories */
  talentCategories?: Maybe<Array<Maybe<TalentCategory>>>;
  /** Talent documents */
  talentDocuments?: Maybe<Array<Maybe<TalentDocument>>>;
  /** Info for 'Invite new talent' page (talent invite talent) */
  talentInvitationsInfo?: Maybe<TalentInvitationsInfo>;
  /** Talent skills */
  talentSkills?: Maybe<SkillPaginator>;
  /** Talent sources */
  talentSources?: Maybe<Array<Maybe<TalentSource>>>;
  /** Talent working history */
  talentWorkHistory?: Maybe<Array<Maybe<TalentWorkHistory>>>;
  /** Talent list */
  talents?: Maybe<TalentPaginator>;
  talentsCountByCategories?: Maybe<Array<Maybe<TalentsCountItem>>>;
  talentsCountBySubcategories?: Maybe<Array<Maybe<TalentsCountItem>>>;
  talentsSearch?: Maybe<TalentsSearchPaginator>;
};


export type QueryAtsRecordsArgs = {
  job_id?: InputMaybe<Scalars['ID']>;
  talent_id?: InputMaybe<Scalars['ID']>;
};


export type QueryAtsRecordsUnionArgs = {
  talent_id: Scalars['ID'];
};


export type QueryAvailableJobsForTalentArgs = {
  talent_id: Scalars['ID'];
};


export type QueryChatMessageArgs = {
  id: Scalars['ID'];
};


export type QueryChatMessagesArgs = {
  first?: InputMaybe<Scalars['Int']>;
  page?: InputMaybe<Scalars['Int']>;
  talent_id: Scalars['ID'];
};


export type QueryCheckCompanyUserExistsByEmailArgs = {
  email: Scalars['Email'];
};


export type QueryCheckTalentExistsByEmailArgs = {
  talent_email: Scalars['Email'];
};


export type QueryCompaniesArgs = {
  first?: InputMaybe<Scalars['Int']>;
  page?: InputMaybe<Scalars['Int']>;
  search?: InputMaybe<Scalars['String']>;
};


export type QueryCompanyArgs = {
  id: Scalars['ID'];
};


export type QueryCompanyPublicInfoArgs = {
  company_id: Scalars['ID'];
};


export type QueryCurrentTalentExternalJobBoardSearchArgs = {
  categories?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  company_name?: InputMaybe<Scalars['String']>;
  country?: InputMaybe<Scalars['String']>;
  employment_type?: InputMaybe<Array<InputMaybe<ExternalJobEmploymentTypeEnum>>>;
  first?: InputMaybe<Scalars['Int']>;
  is_saved?: InputMaybe<Scalars['Boolean']>;
  page?: InputMaybe<Scalars['Int']>;
  search?: InputMaybe<Scalars['String']>;
  seniority?: InputMaybe<Array<InputMaybe<ExternalJobSeniorityLevelEnum>>>;
  skills?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
};


export type QueryCurrentTalentJobApplicationByJobIdArgs = {
  job_id: Scalars['ID'];
};


export type QueryCurrentTalentJobBoardSearchArgs = {
  categories?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  company_id?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  is_active?: InputMaybe<Scalars['Boolean']>;
  is_applied?: InputMaybe<Scalars['Boolean']>;
  is_applied_as_matcher?: InputMaybe<Scalars['Boolean']>;
  is_archived?: InputMaybe<Scalars['Boolean']>;
  is_assigned_as_matcher?: InputMaybe<Scalars['Boolean']>;
  is_saved?: InputMaybe<Scalars['Boolean']>;
  job_matcher_jobs_filter?: InputMaybe<JobMatcherJobsFilterEnum>;
  job_source_type?: InputMaybe<Array<InputMaybe<JobSourceType>>>;
  order_by?: InputMaybe<Array<QueryCurrentTalentJobBoardSearchOrderByOrderByClause>>;
  page?: InputMaybe<Scalars['Int']>;
  rate_max?: InputMaybe<Scalars['Float']>;
  rate_min?: InputMaybe<Scalars['Float']>;
  search?: InputMaybe<Scalars['String']>;
  skills?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
};


export type QueryCurrentTalentJobRecommedationsArgs = {
  first?: InputMaybe<Scalars['Int']>;
  page?: InputMaybe<Scalars['Int']>;
};


export type QueryCurrentUserNotificationsArgs = {
  first?: InputMaybe<Scalars['Int']>;
  is_read?: InputMaybe<Scalars['Boolean']>;
  page?: InputMaybe<Scalars['Int']>;
};


export type QueryExternalJobArgs = {
  id: Scalars['ID'];
};


export type QueryExternalJobCompaniesArgs = {
  search?: InputMaybe<Scalars['String']>;
};


export type QueryGetPaymentLinkForCompanyArgs = {
  primary_user_id?: InputMaybe<Scalars['ID']>;
};


export type QueryGetPaymentLinkForMatcherArgs = {
  talent_id?: InputMaybe<Scalars['ID']>;
};


export type QueryGetPaymentLinkForTalentArgs = {
  talent_id?: InputMaybe<Scalars['ID']>;
};


export type QueryGetTempDataItemArgs = {
  device_id: Scalars['String'];
  email?: InputMaybe<Scalars['Email']>;
  result?: InputMaybe<Scalars['Boolean']>;
};


export type QueryGetTypeformPaymentLinkCodeForMatcherArgs = {
  talent_id?: InputMaybe<Scalars['ID']>;
};


export type QueryInvitationLinkInfoArgs = {
  key: Scalars['String'];
};


export type QueryJobArgs = {
  id: Scalars['ID'];
};


export type QueryJobInvitationArgs = {
  id: Scalars['ID'];
};


export type QueryJobMatcherApplicationsArgs = {
  job_id: Scalars['ID'];
  status?: InputMaybe<JobMatcherApplicationStatusEnum>;
};


export type QueryJobMatchersArgs = {
  job_id: Scalars['ID'];
};


export type QueryJobMatchesArgs = {
  job_id?: InputMaybe<Scalars['ID']>;
  location?: InputMaybe<Scalars['String']>;
  match_type?: InputMaybe<JobMatchTypeEnum>;
  source_type?: InputMaybe<SourceTypeEnum>;
};


export type QueryJobsArgs = {
  company_id?: InputMaybe<Scalars['ID']>;
  first?: InputMaybe<Scalars['Int']>;
  is_archived?: InputMaybe<Scalars['Boolean']>;
  is_draft?: InputMaybe<Scalars['Boolean']>;
  page?: InputMaybe<Scalars['Int']>;
};


export type QueryNodeArgs = {
  id: Scalars['ID'];
};


export type QueryPerksArgs = {
  category?: InputMaybe<Scalars['String']>;
  country?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  page?: InputMaybe<Scalars['Int']>;
};


export type QueryPotentialTalentsForJobCountArgs = {
  campaign_end_date?: InputMaybe<Scalars['DateTimeUtc']>;
  campaign_owner_id?: InputMaybe<Scalars['ID']>;
  campaign_start_date?: InputMaybe<Scalars['DateTimeUtc']>;
  campaign_talent_pool?: InputMaybe<Array<InputMaybe<SourceTypeEnum>>>;
  category_id?: InputMaybe<Scalars['ID']>;
  country?: InputMaybe<Scalars['String']>;
  end_date?: InputMaybe<Scalars['DateTimeUtc']>;
  hard_skills_required?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  hours_per_week?: InputMaybe<Scalars['Int']>;
  is_remote_an_option?: InputMaybe<Scalars['Boolean']>;
  location_type?: InputMaybe<JobLocationTypeEnum>;
  office_hours_per_month?: InputMaybe<Scalars['Float']>;
  rate_max?: InputMaybe<Scalars['Float']>;
  rate_min?: InputMaybe<Scalars['Float']>;
  salary_max?: InputMaybe<Scalars['Float']>;
  salary_min?: InputMaybe<Scalars['Float']>;
  skills?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  skills_boolean_filter?: InputMaybe<TalentsBooleanSkillsFilterInput>;
  solutions_required?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  start_date?: InputMaybe<Scalars['DateTimeUtc']>;
  subcategory_id?: InputMaybe<Scalars['ID']>;
  type: JobTypeEnum;
};


export type QuerySelfDeletePageInfoArgs = {
  delete_key: Scalars['String'];
};


export type QuerySkillArgs = {
  id: Scalars['ID'];
};


export type QuerySkillBySlugArgs = {
  slug: Scalars['String'];
};


export type QuerySkillExistsArgs = {
  name: Scalars['String'];
  skill_type: SkillTypeEnum;
};


export type QuerySkillSearchArgs = {
  search: Scalars['String'];
  skill_type?: InputMaybe<Array<InputMaybe<SkillTypeEnum>>>;
};


export type QuerySkillsArgs = {
  first?: InputMaybe<Scalars['Int']>;
  page?: InputMaybe<Scalars['Int']>;
  skill_type?: InputMaybe<SkillTypeEnum>;
};


export type QueryTalentArgs = {
  id: Scalars['ID'];
};


export type QueryTalentDocumentsArgs = {
  talent_id: Scalars['ID'];
};


export type QueryTalentInvitationsInfoArgs = {
  inviting_talent_id: Scalars['ID'];
};


export type QueryTalentSkillsArgs = {
  first?: InputMaybe<Scalars['Int']>;
  page?: InputMaybe<Scalars['Int']>;
  skill_type_id?: InputMaybe<Scalars['Int']>;
  talent_id: Scalars['ID'];
};


export type QueryTalentWorkHistoryArgs = {
  talent_id: Scalars['ID'];
};


export type QueryTalentsArgs = {
  available_now?: InputMaybe<Scalars['Boolean']>;
  category_ids?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  connection_with_company_id?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  is_active?: InputMaybe<Scalars['Boolean']>;
  is_matcher?: InputMaybe<Scalars['Boolean']>;
  is_verification_required?: InputMaybe<Scalars['Boolean']>;
  max_rate?: InputMaybe<Scalars['Float']>;
  min_rate?: InputMaybe<Scalars['Float']>;
  orderBy?: InputMaybe<Array<QueryTalentsOrderByOrderByClause>>;
  page?: InputMaybe<Scalars['Int']>;
  search?: InputMaybe<Scalars['String']>;
  skills_ids?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  source_type?: InputMaybe<SourceTypeEnum>;
  worked_in_company_id?: InputMaybe<Scalars['ID']>;
  worked_in_company_str?: InputMaybe<Scalars['ID']>;
};


export type QueryTalentsCountByCategoriesArgs = {
  category_ids?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
};


export type QueryTalentsCountBySubcategoriesArgs = {
  subcategory_ids?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
};


export type QueryTalentsSearchArgs = {
  available_now?: InputMaybe<Scalars['Boolean']>;
  category_ids?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  connection_with_company_id?: InputMaybe<Scalars['ID']>;
  country?: InputMaybe<Scalars['String']>;
  first?: InputMaybe<Scalars['Int']>;
  is_active?: InputMaybe<Scalars['Boolean']>;
  is_matcher?: InputMaybe<Scalars['Boolean']>;
  is_verification_required?: InputMaybe<Scalars['Boolean']>;
  max_rate?: InputMaybe<Scalars['Float']>;
  min_rate?: InputMaybe<Scalars['Float']>;
  page?: InputMaybe<Scalars['Int']>;
  search_string?: InputMaybe<Scalars['String']>;
  skills_boolean_filter?: InputMaybe<TalentsBooleanSkillsFilterInput>;
  skills_ids?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  source_type?: InputMaybe<SourceTypeEnum>;
  tags_ids?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  worked_in_company_id?: InputMaybe<Scalars['ID']>;
  worked_in_company_str?: InputMaybe<Scalars['String']>;
};

/** Order by clause for Query.currentTalentJobBoardSearch.order_by. */
export type QueryCurrentTalentJobBoardSearchOrderByOrderByClause = {
  /** The column that is used for ordering. */
  column: JobOrderByColumn;
  /** The direction that is used for ordering. */
  order: SortOrder;
};

/** Order by clause for Query.talents.orderBy. */
export type QueryTalentsOrderByOrderByClause = {
  /** The column that is used for ordering. */
  column: TalentsOrderByColumn;
  /** The direction that is used for ordering. */
  order: SortOrder;
};

export const RedirectTargetRouteTypeEnum = {
  CompanyUserJob: 'COMPANY_USER_JOB',
  TalentJob: 'TALENT_JOB',
  TalentJobInvitation: 'TALENT_JOB_INVITATION',
  TalentProfile: 'TALENT_PROFILE'
} as const;

export type RedirectTargetRouteTypeEnum = typeof RedirectTargetRouteTypeEnum[keyof typeof RedirectTargetRouteTypeEnum];
/** Information about pagination using a simple paginator. */
export type SimplePaginatorInfo = {
  __typename?: 'SimplePaginatorInfo';
  /** Number of items in the current page. */
  count: Scalars['Int'];
  /** Index of the current page. */
  currentPage: Scalars['Int'];
  /** Index of the first item in the current page. */
  firstItem?: Maybe<Scalars['Int']>;
  /** Are there more pages after this one? */
  hasMorePages: Scalars['Boolean'];
  /** Index of the last item in the current page. */
  lastItem?: Maybe<Scalars['Int']>;
  /** Number of items per page. */
  perPage: Scalars['Int'];
};

/** Talent skill */
export type Skill = {
  __typename?: 'Skill';
  created_at?: Maybe<Scalars['DateTime']>;
  id: Scalars['ID'];
  name: Scalars['String'];
  skill_type: SkillTypeEnum;
  slug: Scalars['String'];
  updated_at?: Maybe<Scalars['DateTime']>;
};

/** A paginated list of Skill items. */
export type SkillPaginator = {
  __typename?: 'SkillPaginator';
  /** A list of Skill items. */
  data: Array<Skill>;
  /** Pagination information about the list of items. */
  paginatorInfo: PaginatorInfo;
};

/** Talent skill type */
export type SkillType = {
  __typename?: 'SkillType';
  created_at?: Maybe<Scalars['DateTime']>;
  id: Scalars['ID'];
  name: Scalars['String'];
  slug: Scalars['String'];
  updated_at?: Maybe<Scalars['DateTime']>;
};

export const SkillTypeEnum = {
  HardSkills: 'HARD_SKILLS',
  SoftSkills: 'SOFT_SKILLS',
  Solutions: 'SOLUTIONS'
} as const;

export type SkillTypeEnum = typeof SkillTypeEnum[keyof typeof SkillTypeEnum];
/** Directions for ordering a list of records. */
export const SortOrder = {
  /** Sort records in ascending order. */
  Asc: 'ASC',
  /** Sort records in descending order. */
  Desc: 'DESC'
} as const;

export type SortOrder = typeof SortOrder[keyof typeof SortOrder];
export const SourceTypeEnum = {
  Community: 'COMMUNITY',
  Opentalent: 'OPENTALENT',
  Own: 'OWN'
} as const;

export type SourceTypeEnum = typeof SourceTypeEnum[keyof typeof SourceTypeEnum];
export type StripeSubscription = {
  __typename?: 'StripeSubscription';
  cancel_at_period_end?: Maybe<Scalars['Boolean']>;
  current_period_end?: Maybe<Scalars['DateTime']>;
  start_date?: Maybe<Scalars['DateTime']>;
};

export type Subcategory = {
  __typename?: 'Subcategory';
  created_at?: Maybe<Scalars['DateTime']>;
  id: Scalars['ID'];
  name: Scalars['String'];
  slug: Scalars['String'];
  updated_at?: Maybe<Scalars['DateTime']>;
};

export type Subscription = {
  __typename?: 'Subscription';
  chatMessageCreated?: Maybe<ChatMessage>;
};

export type Tag = {
  __typename?: 'Tag';
  id: Scalars['ID'];
  name: Scalars['String'];
};

export type Talent = {
  __typename?: 'Talent';
  about?: Maybe<Scalars['String']>;
  account_settings?: Maybe<TalentAccountSettings>;
  account_type: AccountTypeEnum;
  address?: Maybe<Scalars['String']>;
  available_date?: Maybe<Scalars['DateTimeUtc']>;
  available_date_updated_at?: Maybe<Scalars['DateTimeUtc']>;
  available_now?: Maybe<Scalars['Boolean']>;
  avatar?: Maybe<Avatar>;
  category?: Maybe<TalentCategory>;
  companies: Array<Maybe<Company>>;
  company_pool_connections?: Maybe<Array<Maybe<TalentCompanyPoolConnection>>>;
  created_at?: Maybe<Scalars['DateTime']>;
  default_company_referral_key?: Maybe<Scalars['String']>;
  documents?: Maybe<Array<Maybe<TalentDocument>>>;
  email?: Maybe<Scalars['Email']>;
  email_abac: AbacString;
  first_name?: Maybe<Scalars['String']>;
  first_name_abac: AbacString;
  full_name?: Maybe<Scalars['String']>;
  full_name_abac: AbacString;
  hours_per_week?: Maybe<Scalars['Int']>;
  id: Scalars['ID'];
  invited_by?: Maybe<TalentInvitedBy>;
  is_invitation_accepted?: Maybe<Scalars['Boolean']>;
  is_matcher?: Maybe<Scalars['Boolean']>;
  is_ot_pool?: Maybe<Scalars['Boolean']>;
  is_verification_required?: Maybe<Scalars['Boolean']>;
  job_applications?: Maybe<Array<Maybe<JobApplication>>>;
  join_reason?: Maybe<Array<Maybe<JoinReasonEnum>>>;
  last_name?: Maybe<Scalars['String']>;
  last_name_abac: AbacString;
  location?: Maybe<Scalars['String']>;
  number_of_active_matcher_assignments?: Maybe<Scalars['Int']>;
  origin?: Maybe<TalentOriginEnum>;
  rate?: Maybe<Scalars['Float']>;
  rate_max?: Maybe<Scalars['Float']>;
  rate_min?: Maybe<Scalars['Float']>;
  recent_position_title?: Maybe<Scalars['String']>;
  reminded_at?: Maybe<Scalars['DateTime']>;
  salary?: Maybe<Scalars['Float']>;
  skills?: Maybe<SkillPaginator>;
  source?: Maybe<TalentSource>;
  stream_chat_id?: Maybe<Scalars['String']>;
  stream_chat_token?: Maybe<Scalars['String']>;
  subcategories: Array<Maybe<Subcategory>>;
  tags?: Maybe<Array<Maybe<Tag>>>;
  talent_data?: Maybe<TalentData>;
  talent_work_history?: Maybe<Array<Maybe<TalentWorkHistory>>>;
  updated_at?: Maybe<Scalars['DateTime']>;
};


export type TalentSkillsArgs = {
  first: Scalars['Int'];
  page?: InputMaybe<Scalars['Int']>;
};

export type TalentAccountSettings = {
  __typename?: 'TalentAccountSettings';
  instant_matches_notifications_min_score?: Maybe<InstantMatchesNotificationsScoreEnum>;
  receive_company_and_product_updates?: Maybe<Scalars['Boolean']>;
  receive_direct_messages?: Maybe<Scalars['Boolean']>;
};

export type TalentAccountSettingsInput = {
  instant_matches_notifications_min_score?: InputMaybe<InstantMatchesNotificationsScoreEnum>;
  receive_company_and_product_updates?: InputMaybe<Scalars['Boolean']>;
  receive_direct_messages?: InputMaybe<Scalars['Boolean']>;
};

export type TalentAccountVerificationInput = {
  facebook_profile_link?: InputMaybe<Scalars['String']>;
  linkedin_profile_link?: InputMaybe<Scalars['String']>;
  phone?: InputMaybe<Scalars['String']>;
  upwork_profile_link?: InputMaybe<Scalars['String']>;
  vat_number?: InputMaybe<Scalars['String']>;
};

export type TalentCategory = {
  __typename?: 'TalentCategory';
  created_at?: Maybe<Scalars['DateTime']>;
  id: Scalars['ID'];
  name: Scalars['String'];
  slug: Scalars['String'];
  subcategories?: Maybe<Array<Subcategory>>;
  updated_at?: Maybe<Scalars['DateTime']>;
};

export type TalentCompanyPoolConnection = {
  __typename?: 'TalentCompanyPoolConnection';
  cancel_initiator?: Maybe<TalentCompanyPoolingInitiatorEnum>;
  company?: Maybe<Company>;
  initiator?: Maybe<TalentCompanyPoolingInitiatorEnum>;
  status?: Maybe<TalentCompanyPoolingStatusEnum>;
  talent?: Maybe<Talent>;
};

export const TalentCompanyPoolingInitiatorEnum = {
  Company: 'COMPANY',
  System: 'SYSTEM',
  Talent: 'TALENT'
} as const;

export type TalentCompanyPoolingInitiatorEnum = typeof TalentCompanyPoolingInitiatorEnum[keyof typeof TalentCompanyPoolingInitiatorEnum];
export const TalentCompanyPoolingStatusEnum = {
  Approved: 'APPROVED',
  Cancelled: 'CANCELLED',
  Pending: 'PENDING',
  Rejected: 'REJECTED'
} as const;

export type TalentCompanyPoolingStatusEnum = typeof TalentCompanyPoolingStatusEnum[keyof typeof TalentCompanyPoolingStatusEnum];
export type TalentData = {
  __typename?: 'TalentData';
  facebook_profile_link?: Maybe<Scalars['String']>;
  linkedin_profile_link?: Maybe<Scalars['String']>;
  linkedin_profile_link_abac?: Maybe<AbacString>;
  phone?: Maybe<Scalars['String']>;
  phone_abac?: Maybe<Scalars['String']>;
  upwork_profile_link?: Maybe<Scalars['String']>;
  vat_number?: Maybe<Scalars['String']>;
};

export type TalentDocument = {
  __typename?: 'TalentDocument';
  content_type: Scalars['String'];
  created_at?: Maybe<Scalars['DateTime']>;
  hash: Scalars['String'];
  size: Scalars['Int'];
  title: Scalars['String'];
  updated_at?: Maybe<Scalars['DateTime']>;
  url: Scalars['String'];
  url_abac?: Maybe<AbacString>;
};

export type TalentGeneralInfoInput = {
  address?: InputMaybe<Scalars['String']>;
  can_help_with?: InputMaybe<Array<InputMaybe<CanHelpWithEnum>>>;
  first_name: Scalars['String'];
  join_reason?: InputMaybe<Array<InputMaybe<JoinReasonEnum>>>;
  last_name?: InputMaybe<Scalars['String']>;
  location?: InputMaybe<Scalars['String']>;
  rate?: InputMaybe<Scalars['Float']>;
  rate_max?: InputMaybe<Scalars['Float']>;
  rate_min?: InputMaybe<Scalars['Float']>;
  recent_position_title?: InputMaybe<Scalars['String']>;
  salary?: InputMaybe<Scalars['Float']>;
  talent_category_id?: InputMaybe<Scalars['ID']>;
  talent_source_id?: InputMaybe<Scalars['ID']>;
  talent_subcategory_ids?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
};

export type TalentInvitation = {
  __typename?: 'TalentInvitation';
  /** Company id if invitation is for specific company pool */
  company_id?: Maybe<Scalars['ID']>;
  company_user_inviter?: Maybe<Talent>;
  created_at: Scalars['DateTime'];
  email?: Maybe<Scalars['String']>;
  first_name?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  is_accepted: Scalars['Boolean'];
  last_name?: Maybe<Scalars['String']>;
  link?: Maybe<Scalars['String']>;
  talent?: Maybe<Talent>;
  talent_inviter?: Maybe<Talent>;
  uid: Scalars['String'];
};

export const TalentInvitationTypeEnum = {
  InvitationTypeByCompany: 'INVITATION_TYPE_BY_COMPANY',
  InvitationTypeByTalent: 'INVITATION_TYPE_BY_TALENT',
  WithoutInvitation: 'WITHOUT_INVITATION'
} as const;

export type TalentInvitationTypeEnum = typeof TalentInvitationTypeEnum[keyof typeof TalentInvitationTypeEnum];
export type TalentInvitedBy = {
  __typename?: 'TalentInvitedBy';
  company_name?: Maybe<Scalars['String']>;
  first_name_abac?: Maybe<AbacString>;
  id?: Maybe<Scalars['ID']>;
  invitation_type?: Maybe<TalentInvitationTypeEnum>;
  last_name_abac?: Maybe<AbacString>;
};

export type TalentJobConnection = {
  __typename?: 'TalentJobConnection';
  is_applied: Scalars['Boolean'];
  is_saved: Scalars['Boolean'];
};

export type TalentLogRecordInterface = {
  actor?: Maybe<AtsRecordActor>;
  company_id?: Maybe<Scalars['ID']>;
  date: Scalars['DateTimeUtc'];
  id: Scalars['ID'];
  type: AtsRecordTypeEnum;
};

export const TalentOriginEnum = {
  EnterpriseLanding: 'ENTERPRISE_LANDING',
  ExternalJobPageApplication: 'EXTERNAL_JOB_PAGE_APPLICATION',
  InvitedByCompanyUser: 'INVITED_BY_COMPANY_USER',
  InvitedByTalent: 'INVITED_BY_TALENT',
  MainLanding: 'MAIN_LANDING',
  OldUser: 'OLD_USER'
} as const;

export type TalentOriginEnum = typeof TalentOriginEnum[keyof typeof TalentOriginEnum];
/** A paginated list of Talent items. */
export type TalentPaginator = {
  __typename?: 'TalentPaginator';
  /** A list of Talent items. */
  data: Array<Talent>;
  /** Pagination information about the list of items. */
  paginatorInfo: PaginatorInfo;
};

export type TalentSearchHighlight = {
  __typename?: 'TalentSearchHighlight';
  source?: Maybe<Scalars['String']>;
  text?: Maybe<Scalars['String']>;
};

export type TalentSearchResult = {
  __typename?: 'TalentSearchResult';
  highlights?: Maybe<Array<TalentSearchHighlight>>;
  talent?: Maybe<Talent>;
};

export type TalentSource = {
  __typename?: 'TalentSource';
  created_at?: Maybe<Scalars['DateTime']>;
  id: Scalars['ID'];
  logo?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  updated_at?: Maybe<Scalars['DateTime']>;
};

export type TalentWorkHistory = {
  __typename?: 'TalentWorkHistory';
  company_name: Scalars['String'];
  created_at?: Maybe<Scalars['DateTime']>;
  id: Scalars['ID'];
  position_title: Scalars['String'];
  updated_at?: Maybe<Scalars['DateTime']>;
  worked_from?: Maybe<Scalars['DateTime']>;
  worked_to?: Maybe<Scalars['DateTime']>;
};

export type TalentsBooleanSkillsFilterInput = {
  boolean_skills_filter_items?: InputMaybe<Array<InputMaybe<TalentsBooleanSkillsFilterItemInput>>>;
  not_skills_ids?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
};

export type TalentsBooleanSkillsFilterItemInput = {
  all_of_skills_ids?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
  boolean_operator?: InputMaybe<BooleanOperatorsEnum>;
  one_of_skills_ids?: InputMaybe<Array<InputMaybe<Scalars['ID']>>>;
};

/** Allows ordering a list of records. */
export type TalentsOrderByClause = {
  /** The column that is used for ordering. */
  column: TalentsOrderByColumn;
  /** The direction that is used for ordering. */
  order: SortOrder;
};

/** Order by clause for the `orderBy` argument on the query `posts`. */
export const TalentsOrderByColumn = {
  AvailableNow: 'AVAILABLE_NOW',
  IsInvitationAccepted: 'IS_INVITATION_ACCEPTED',
  TalentId: 'TALENT_ID'
} as const;

export type TalentsOrderByColumn = typeof TalentsOrderByColumn[keyof typeof TalentsOrderByColumn];
export type TalentsSearchPaginator = {
  __typename?: 'TalentsSearchPaginator';
  custom_paginator_info: CustomPaginatorInfo;
  data: Array<TalentSearchResult>;
};

export type TempDataItem = {
  __typename?: 'TempDataItem';
  data?: Maybe<Scalars['String']>;
  device_id: Scalars['String'];
  email?: Maybe<Scalars['Email']>;
};

export type TempDataItemSearchResult = {
  __typename?: 'TempDataItemSearchResult';
  result?: Maybe<TempDataItem>;
  success: Scalars['Boolean'];
};

/** Specify if you want to include or exclude trashed results from a query. */
export const Trashed = {
  /** Only return trashed results. */
  Only: 'ONLY',
  /** Return both trashed and non-trashed results. */
  With: 'WITH',
  /** Only return non-trashed results. */
  Without: 'WITHOUT'
} as const;

export type Trashed = typeof Trashed[keyof typeof Trashed];
export type User = {
  __typename?: 'User';
  avatar?: Maybe<Avatar>;
  company?: Maybe<UserCompany>;
  created_at?: Maybe<Scalars['DateTime']>;
  email: Scalars['Email'];
  first_name?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  is_primary?: Maybe<Scalars['Boolean']>;
  last_name?: Maybe<Scalars['String']>;
  position?: Maybe<Scalars['String']>;
  stream_chat_id?: Maybe<Scalars['String']>;
  stream_chat_token?: Maybe<Scalars['String']>;
  updated_at?: Maybe<Scalars['DateTime']>;
};

export type UserCompany = {
  __typename?: 'UserCompany';
  about?: Maybe<Scalars['String']>;
  account_type?: Maybe<CompanyAccountTypeEnum>;
  address?: Maybe<Scalars['String']>;
  contract?: Maybe<OtContract>;
  country?: Maybe<Scalars['String']>;
  has_contract: Scalars['Boolean'];
  id: Scalars['ID'];
  logo?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  sign_up_landing_link?: Maybe<Scalars['String']>;
  size?: Maybe<Scalars['String']>;
  type_of_activity?: Maybe<Scalars['String']>;
  website?: Maybe<Scalars['String']>;
};

export type WorkHistoryInput = {
  company_name: Scalars['String'];
  position_title: Scalars['String'];
  worked_from?: InputMaybe<Scalars['Date']>;
  worked_to?: InputMaybe<Scalars['Date']>;
};

export type CustomPaginatorInfo = {
  __typename?: 'customPaginatorInfo';
  current_page: Scalars['Int'];
  last_page: Scalars['Int'];
  per_page: Scalars['Int'];
  total: Scalars['Int'];
};

export type RestoreDeletedAccountInfo = {
  __typename?: 'restoreDeletedAccountInfo';
  link_to_set_password_page?: Maybe<Scalars['String']>;
};

export type SelfDeletePageInfo = {
  __typename?: 'selfDeletePageInfo';
  invited_by_full_name?: Maybe<Scalars['String']>;
};

export type TalentInvitationsInfo = {
  __typename?: 'talentInvitationsInfo';
  /** Default referral key for company */
  default_company_referral_key?: Maybe<Scalars['String']>;
  /** Talents who have already been invited */
  invited_talents?: Maybe<Array<Maybe<Talent>>>;
  /** Companies that were invited using talent's referral link */
  referred_companies?: Maybe<Array<Maybe<Company>>>;
  /** Number of invitations left for talent to invite another talents */
  talent_invitations_left: Scalars['Int'];
};

export type TalentsCountItem = {
  __typename?: 'talentsCountItem';
  name: Scalars['String'];
  slug: Scalars['String'];
  talents_count: Scalars['Int'];
};

export type GetAtsRecordsQueryVariables = Exact<{
  talent_id?: InputMaybe<Scalars['ID']>;
  job_id?: InputMaybe<Scalars['ID']>;
}>;


export type GetAtsRecordsQuery = { __typename?: 'Query', ATSRecords?: Array<{ __typename: 'ATSRecordComment', text?: string | undefined, id: string, type: AtsRecordTypeEnum, date: any, actor?: { __typename?: 'ATSRecordActor', first_name: string, last_name?: string | undefined, avatar?: { __typename?: 'Avatar', avatar: string } | undefined } | undefined } | { __typename: 'ATSRecordJobMatchAction', id: string, type: AtsRecordTypeEnum, date: any, job_match_action?: { __typename?: 'JobMatchAction', id: string, job_match_action_type: JobMatchActionTypeEnum, job_match_type_before?: JobMatchTypeEnum | undefined, job_match_type_after?: JobMatchTypeEnum | undefined, job_match?: { __typename?: 'JobMatch', job: { __typename?: 'Job', id: string, name: string } } | undefined } | undefined, actor?: { __typename?: 'ATSRecordActor', first_name: string, last_name?: string | undefined, avatar?: { __typename?: 'Avatar', avatar: string } | undefined } | undefined } | undefined> | undefined };

export type ApplyCurrentTalentToCompanyPoolMutationVariables = Exact<{
  company_id: Scalars['ID'];
}>;


export type ApplyCurrentTalentToCompanyPoolMutation = { __typename?: 'Mutation', applyCurrentTalentToCompanyPool?: boolean | undefined };

export type LeaveCurrentTalentFromCompanyPoolMutationVariables = Exact<{
  company_id: Scalars['ID'];
}>;


export type LeaveCurrentTalentFromCompanyPoolMutation = { __typename?: 'Mutation', leaveCurrentTalentFromCompanyPool?: boolean | undefined };

export type AcceptCompanyPoolInvitationMutationVariables = Exact<{
  company_id: Scalars['ID'];
}>;


export type AcceptCompanyPoolInvitationMutation = { __typename?: 'Mutation', acceptCompanyPoolInvitation?: boolean | undefined };

export type RejectCompanyPoolInvitationMutationVariables = Exact<{
  company_id: Scalars['ID'];
}>;


export type RejectCompanyPoolInvitationMutation = { __typename?: 'Mutation', rejectCompanyPoolInvitation?: boolean | undefined };

export type InviteTalentToCompanyPoolMutationVariables = Exact<{
  talent_id: Scalars['ID'];
}>;


export type InviteTalentToCompanyPoolMutation = { __typename?: 'Mutation', inviteTalentToCompanyPool?: boolean | undefined };

export type RejectTalentCompanyPoolApplicationMutationVariables = Exact<{
  talent_id: Scalars['ID'];
}>;


export type RejectTalentCompanyPoolApplicationMutation = { __typename?: 'Mutation', rejectTalentCompanyPoolApplication?: boolean | undefined };

export type RemoveTalentFromCompanyPoolMutationVariables = Exact<{
  talent_id: Scalars['ID'];
  message?: InputMaybe<Scalars['String']>;
}>;


export type RemoveTalentFromCompanyPoolMutation = { __typename?: 'Mutation', removeTalentFromCompanyPool?: boolean | undefined };

export type ApproveTalentCompanyPoolApplicationMutationVariables = Exact<{
  talent_id: Scalars['ID'];
}>;


export type ApproveTalentCompanyPoolApplicationMutation = { __typename?: 'Mutation', approveTalentCompanyPoolApplication?: boolean | undefined };

export type RevokeCompanyPoolInvitationMutationVariables = Exact<{
  talent_id: Scalars['ID'];
}>;


export type RevokeCompanyPoolInvitationMutation = { __typename?: 'Mutation', revokeCompanyPoolInvitation?: boolean | undefined };

export type GetDashboardInfoQueryVariables = Exact<{ [key: string]: never; }>;


export type GetDashboardInfoQuery = { __typename?: 'Query', pendingTalents?: { __typename?: 'TalentPaginator', paginatorInfo: { __typename?: 'PaginatorInfo', total: number } } | undefined, generalAnalytics?: Array<{ __typename?: 'DashboardAnalyticsItem', key?: string | undefined, value?: number | undefined, slug?: string | undefined } | undefined> | undefined, tagsAnalytics?: Array<{ __typename?: 'DashboardAnalyticsItem', key?: string | undefined, value?: number | undefined, slug?: string | undefined } | undefined> | undefined, countryAnalytics?: Array<{ __typename?: 'DashboardAnalyticsItem', key?: string | undefined, value?: number | undefined, slug?: string | undefined } | undefined> | undefined, categoryAnalytics?: Array<{ __typename?: 'DashboardAnalyticsItem', key?: string | undefined, value?: number | undefined, slug?: string | undefined } | undefined> | undefined, lastTalents?: Array<{ __typename?: 'Talent', first_name?: string | undefined, last_name?: string | undefined, id: string, location?: string | undefined, rate?: number | undefined, is_invitation_accepted?: boolean | undefined, recent_position_title?: string | undefined, avatar?: { __typename?: 'Avatar', avatar: string } | undefined } | undefined> | undefined };

export type GetPendingTalentsCountQueryVariables = Exact<{
  source_type?: InputMaybe<SourceTypeEnum>;
  is_verification_required?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  page?: InputMaybe<Scalars['Int']>;
}>;


export type GetPendingTalentsCountQuery = { __typename?: 'Query', talentsSearch?: { __typename?: 'TalentsSearchPaginator', custom_paginator_info: { __typename?: 'customPaginatorInfo', total: number } } | undefined };

export type ExpandJobMutationVariables = Exact<{
  id: Scalars['ID'];
  campaign_end_date?: InputMaybe<Scalars['DateTimeUtc']>;
}>;


export type ExpandJobMutation = { __typename?: 'Mutation', updateJob: { __typename?: 'Job', id: string } };

export type UpdateJobFinderFeeMutationVariables = Exact<{
  id: Scalars['ID'];
  finders_fee?: InputMaybe<Scalars['Float']>;
}>;


export type UpdateJobFinderFeeMutation = { __typename?: 'Mutation', updateJob: { __typename?: 'Job', id: string } };

export type GetJobMatchersApplicationsQueryVariables = Exact<{
  job_id: Scalars['ID'];
  status?: InputMaybe<JobMatcherApplicationStatusEnum>;
}>;


export type GetJobMatchersApplicationsQuery = { __typename?: 'Query', jobMatcherApplications?: Array<{ __typename?: 'JobMatcherApplication', id: string, status: JobMatcherApplicationStatusEnum, job: { __typename?: 'Job', id: string }, matcher: { __typename?: 'Talent', first_name?: string | undefined, last_name?: string | undefined, id: string, address?: string | undefined, location?: string | undefined, avatar?: { __typename?: 'Avatar', avatar: string } | undefined } } | undefined> | undefined, jobMatchers?: Array<{ __typename?: 'JobMatcher', job: { __typename?: 'Job', id: string }, talent: { __typename?: 'Talent', id: string, first_name?: string | undefined, last_name?: string | undefined, address?: string | undefined, location?: string | undefined, avatar?: { __typename?: 'Avatar', avatar: string } | undefined } } | undefined> | undefined };

export type ApproveJobMacherApplicationMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type ApproveJobMacherApplicationMutation = { __typename?: 'Mutation', approveJobMatcherApplication?: { __typename?: 'JobMatcherApplication', id: string } | undefined };

export type RejectJobMacherApplicationMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type RejectJobMacherApplicationMutation = { __typename?: 'Mutation', rejectJobMatcherApplication?: { __typename?: 'JobMatcherApplication', id: string } | undefined };

export type RemoveJobMatcherFromJobMutationVariables = Exact<{
  matcher_id: Scalars['ID'];
  job_id: Scalars['ID'];
}>;


export type RemoveJobMatcherFromJobMutation = { __typename?: 'Mutation', removeJobMatcherFromJob?: boolean | undefined };

export type CreateCompanyAccountAccessRequestMutationVariables = Exact<{
  first_name: Scalars['String'];
  last_name: Scalars['String'];
  email: Scalars['Email'];
  company_id?: InputMaybe<Scalars['ID']>;
  new_company_name?: InputMaybe<Scalars['String']>;
  referral_key?: InputMaybe<Scalars['String']>;
}>;


export type CreateCompanyAccountAccessRequestMutation = { __typename?: 'Mutation', createCompanyAccountAccessRequest?: boolean | undefined };

export type GetPendingTalentsQueryVariables = Exact<{
  source_type?: InputMaybe<SourceTypeEnum>;
  is_verification_required?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  page?: InputMaybe<Scalars['Int']>;
}>;


export type GetPendingTalentsQuery = { __typename?: 'Query', talentsSearch?: { __typename?: 'TalentsSearchPaginator', custom_paginator_info: { __typename?: 'customPaginatorInfo', current_page: number, last_page: number, per_page: number, total: number }, data: Array<{ __typename?: 'TalentSearchResult', highlights?: Array<{ __typename?: 'TalentSearchHighlight', source?: string | undefined, text?: string | undefined }> | undefined, talent?: { __typename?: 'Talent', id: string, reminded_at?: any | undefined, created_at?: any | undefined, is_invitation_accepted?: boolean | undefined, is_verification_required?: boolean | undefined, first_name?: string | undefined, last_name?: string | undefined, address?: string | undefined, rate?: number | undefined, recent_position_title?: string | undefined, email?: any | undefined, available_now?: boolean | undefined, location?: string | undefined, stream_chat_id?: string | undefined, is_ot_pool?: boolean | undefined, company_pool_connections?: Array<{ __typename?: 'TalentCompanyPoolConnection', status?: TalentCompanyPoolingStatusEnum | undefined, initiator?: TalentCompanyPoolingInitiatorEnum | undefined, cancel_initiator?: TalentCompanyPoolingInitiatorEnum | undefined, company?: { __typename?: 'Company', id: string, name: string } | undefined } | undefined> | undefined, subcategories: Array<{ __typename?: 'Subcategory', id: string, name: string } | undefined>, first_name_abac: { __typename?: 'ABACString', value?: string | undefined }, last_name_abac: { __typename?: 'ABACString', value?: string | undefined }, job_applications?: Array<{ __typename?: 'JobApplication', id: string, job: { __typename?: 'Job', id: string, name: string } } | undefined> | undefined, source?: { __typename?: 'TalentSource', id: string } | undefined, avatar?: { __typename?: 'Avatar', avatar: string } | undefined, talent_data?: { __typename?: 'TalentData', phone?: string | undefined } | undefined } | undefined }> } | undefined };

export type GetLeavePageInfoQueryVariables = Exact<{
  delete_key: Scalars['String'];
}>;


export type GetLeavePageInfoQuery = { __typename?: 'Query', selfDeletePageInfo?: { __typename?: 'selfDeletePageInfo', invited_by_full_name?: string | undefined } | undefined };

export type RestoreAccountMutationVariables = Exact<{
  delete_key: Scalars['String'];
}>;


export type RestoreAccountMutation = { __typename?: 'Mutation', restoreDeletedAccountByDeleteKey?: { __typename?: 'restoreDeletedAccountInfo', link_to_set_password_page?: string | undefined } | undefined };

export type DeleteAccountMutationVariables = Exact<{
  delete_key: Scalars['String'];
}>;


export type DeleteAccountMutation = { __typename?: 'Mutation', selfDeleteAccount?: boolean | undefined };

export type SetTagsToTalentMutationVariables = Exact<{
  tags_ids: Array<Scalars['ID']> | Scalars['ID'];
  talent_id: Scalars['ID'];
}>;


export type SetTagsToTalentMutation = { __typename?: 'Mutation', setTagsToTalent?: boolean | undefined };

export type CreateJobMatcherApplicationMutationVariables = Exact<{
  job_id: Scalars['ID'];
  message?: InputMaybe<Scalars['String']>;
}>;


export type CreateJobMatcherApplicationMutation = { __typename?: 'Mutation', createJobMatcherApplication?: { __typename?: 'JobMatcherApplication', id: string } | undefined };

export type GetCompaniesScreenDataQueryVariables = Exact<{ [key: string]: never; }>;


export type GetCompaniesScreenDataQuery = { __typename?: 'Query', poolingCompanies?: Array<{ __typename?: 'Company', id: string, name: string, about?: string | undefined, country?: string | undefined, size?: string | undefined, type_of_activity?: string | undefined, logo?: { __typename?: 'CompanyLogo', url: string } | undefined } | undefined> | undefined };

export type InviteCompanyToOpentalentMutationVariables = Exact<{
  company_name: Scalars['String'];
  contact_person: Scalars['String'];
  company_email: Scalars['String'];
}>;


export type InviteCompanyToOpentalentMutation = { __typename?: 'Mutation', inviteCompanyToOpentalent?: boolean | undefined };

export type GetCompanyViewScreenDataQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type GetCompanyViewScreenDataQuery = { __typename?: 'Query', company?: { __typename?: 'Company', id: string, name: string, about?: string | undefined, country?: string | undefined, size?: string | undefined, type_of_activity?: string | undefined, address?: string | undefined, website?: string | undefined, logo?: { __typename?: 'CompanyLogo', url: string } | undefined } | undefined, talentsSearch?: { __typename?: 'TalentsSearchPaginator', custom_paginator_info: { __typename?: 'customPaginatorInfo', total: number }, data: Array<{ __typename?: 'TalentSearchResult', talent?: { __typename?: 'Talent', avatar?: { __typename?: 'Avatar', avatar: string } | undefined } | undefined }> } | undefined, jobs?: { __typename?: 'JobPaginator', paginatorInfo: { __typename?: 'PaginatorInfo', total: number } } | undefined };

export type PublicPageGetJobQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type PublicPageGetJobQuery = { __typename?: 'Query', job?: { __typename?: 'Job', id: string, name: string, country?: string | undefined, city?: string | undefined, is_draft?: boolean | undefined, is_remote_an_option: boolean, link_to_details?: string | undefined, office_hours_per_month?: number | undefined, description?: string | undefined, pitch?: string | undefined, start_date?: any | undefined, end_date?: any | undefined, rate_min?: number | undefined, rate_max?: number | undefined, is_rate_negotiable?: boolean | undefined, campaign_start_date?: any | undefined, campaign_end_date?: any | undefined, campaign_talent_pool?: Array<SourceTypeEnum | undefined> | undefined, is_archived: boolean, matches_count: number, instant_matches_count: number, matches_with_application_count: number, salary_max?: number | undefined, salary_min?: number | undefined, finders_fee?: number | undefined, location?: string | undefined, hours_per_week?: number | undefined, location_type?: JobLocationTypeEnum | undefined, type?: JobTypeEnum | undefined, period?: string | undefined, client?: string | undefined, capacity?: string | undefined, posted_at?: any | undefined, skills?: Array<{ __typename?: 'JobSkill', id: string, name: string, skill_type: SkillTypeEnum, job_skill_pivot?: { __typename?: 'JobSkillPivot', is_required?: boolean | undefined } | undefined } | undefined> | undefined, skills_boolean_filter?: { __typename?: 'JobSkillsBooleanFilter', not_skills?: Array<{ __typename?: 'Skill', id: string, name: string, skill_type: SkillTypeEnum } | undefined> | undefined, boolean_skills_filter_items?: Array<{ __typename?: 'JobSkillsBooleanFilterItem', boolean_operator?: BooleanOperatorsEnum | undefined, one_of_skills?: Array<{ __typename?: 'Skill', id: string, name: string, skill_type: SkillTypeEnum } | undefined> | undefined, all_of_skills?: Array<{ __typename?: 'Skill', id: string, name: string, skill_type: SkillTypeEnum } | undefined> | undefined } | undefined> | undefined } | undefined, category?: { __typename?: 'TalentCategory', id: string, name: string } | undefined, subcategory?: { __typename?: 'Subcategory', id: string, name: string } | undefined, campaign_owner?: { __typename?: 'User', id: string, first_name?: string | undefined, last_name?: string | undefined, email: any, company?: { __typename?: 'UserCompany', name: string, id: string } | undefined } | undefined, invitations?: Array<{ __typename?: 'JobInvitation', id: string, talent_id: string } | undefined> | undefined } | undefined };

export type GetTalentSourcesQueryVariables = Exact<{ [key: string]: never; }>;


export type GetTalentSourcesQuery = { __typename?: 'Query', talentSources?: Array<{ __typename?: 'TalentSource', id: string, name: string, created_at?: any | undefined, updated_at?: any | undefined } | undefined> | undefined };

export type ChooseJobServiceQueryVariables = Exact<{ [key: string]: never; }>;


export type ChooseJobServiceQuery = { __typename?: 'Query', commonAppInfo?: { __typename?: 'CommonAppInfo', total_ot_freelancers_count: number, total_ot_approved_freelancers_count: number, total_ot_recruiters_count: number } | undefined, talentsSearch?: { __typename?: 'TalentsSearchPaginator', custom_paginator_info: { __typename?: 'customPaginatorInfo', total: number } } | undefined };

export type GetTmpDataQueryVariables = Exact<{
  email?: InputMaybe<Scalars['Email']>;
  device_id: Scalars['String'];
}>;


export type GetTmpDataQuery = { __typename?: 'Query', getTempDataItem: { __typename?: 'TempDataItemSearchResult', success: boolean, result?: { __typename?: 'TempDataItem', email?: any | undefined, device_id: string, data?: string | undefined } | undefined } };

export type SetTmpDataMutationVariables = Exact<{
  email?: InputMaybe<Scalars['Email']>;
  device_id: Scalars['String'];
  data?: InputMaybe<Scalars['String']>;
}>;


export type SetTmpDataMutation = { __typename?: 'Mutation', setTempDataItem?: boolean | undefined };

export type UploadFileMutationVariables = Exact<{
  file_type: FileTypeEnum;
  owner_id?: InputMaybe<Scalars['ID']>;
  file: Scalars['Upload'];
}>;


export type UploadFileMutation = { __typename?: 'Mutation', uploadFile?: { __typename?: 'File', hash: string } | undefined };

export type CancelCurrentCompanySubscriptionMutationVariables = Exact<{ [key: string]: never; }>;


export type CancelCurrentCompanySubscriptionMutation = { __typename?: 'Mutation', cancelCurrentCompanyUserCompanySubscription?: { __typename?: 'StripeSubscription', start_date?: any | undefined, current_period_end?: any | undefined, cancel_at_period_end?: boolean | undefined } | undefined };

export type CheckCompanyUserExistsByEmailQueryVariables = Exact<{
  email: Scalars['Email'];
}>;


export type CheckCompanyUserExistsByEmailQuery = { __typename?: 'Query', checkCompanyUserExistsByEmail: boolean };

export type CreateAnnouncementMutationVariables = Exact<{
  text?: InputMaybe<Scalars['String']>;
  title: Scalars['String'];
  need_response: Scalars['Boolean'];
  talents_ids: Array<InputMaybe<Scalars['ID']>> | InputMaybe<Scalars['ID']>;
  send_email?: InputMaybe<Scalars['Boolean']>;
}>;


export type CreateAnnouncementMutation = { __typename?: 'Mutation', createAnnouncement: boolean };

export type CreateCompanyForUserMutationVariables = Exact<{
  name: Scalars['String'];
  address?: InputMaybe<Scalars['String']>;
  type_of_activity?: InputMaybe<Scalars['String']>;
  website?: InputMaybe<Scalars['String']>;
  logo?: InputMaybe<Scalars['String']>;
  primary_company_user_id?: InputMaybe<Scalars['ID']>;
}>;


export type CreateCompanyForUserMutation = { __typename?: 'Mutation', createCompanyForUser?: { __typename?: 'Company', id: string } | undefined };

export type CreateCompanyUserMutationVariables = Exact<{
  first_name: Scalars['String'];
  last_name: Scalars['String'];
  email: Scalars['Email'];
  avatar?: InputMaybe<Scalars['String']>;
  position?: InputMaybe<Scalars['String']>;
  is_primary?: InputMaybe<Scalars['Boolean']>;
  onboarding_type?: InputMaybe<CompanyUserOnboardingTypeEnum>;
  referral_key?: InputMaybe<Scalars['String']>;
}>;


export type CreateCompanyUserMutation = { __typename?: 'Mutation', createCompanyUserProfile?: { __typename?: 'User', id: string } | undefined };

export type CurrentCompanySubscriptionQueryVariables = Exact<{ [key: string]: never; }>;


export type CurrentCompanySubscriptionQuery = { __typename?: 'Query', currentCompanyUserCompanySubscription?: { __typename?: 'StripeSubscription', start_date?: any | undefined, current_period_end?: any | undefined, cancel_at_period_end?: boolean | undefined } | undefined };

export type GetCurrentUserCompanyTagsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetCurrentUserCompanyTagsQuery = { __typename?: 'Query', currentUserCompanyTags?: Array<{ __typename?: 'Tag', name: string, id: string } | undefined> | undefined };

export type GetPaymentLinkForCompanyQueryVariables = Exact<{
  primary_user_id?: InputMaybe<Scalars['ID']>;
}>;


export type GetPaymentLinkForCompanyQuery = { __typename?: 'Query', getPaymentLinkForCompany?: string | undefined };

export type InviteCompanyUserMutationVariables = Exact<{
  email: Scalars['Email'];
}>;


export type InviteCompanyUserMutation = { __typename?: 'Mutation', inviteCompanyUser?: boolean | undefined };

export type RenewCurrentCompanySubscriptionMutationVariables = Exact<{ [key: string]: never; }>;


export type RenewCurrentCompanySubscriptionMutation = { __typename?: 'Mutation', renewCurrentCompanyUserCompanySubscription?: { __typename?: 'StripeSubscription', start_date?: any | undefined, current_period_end?: any | undefined, cancel_at_period_end?: boolean | undefined } | undefined };

export type UpdateCompanyUserMutationVariables = Exact<{
  company_user_id: Scalars['ID'];
  first_name?: InputMaybe<Scalars['String']>;
  last_name?: InputMaybe<Scalars['String']>;
  email?: InputMaybe<Scalars['Email']>;
  position?: InputMaybe<Scalars['String']>;
  avatar?: InputMaybe<Scalars['String']>;
}>;


export type UpdateCompanyUserMutation = { __typename?: 'Mutation', updateCompanyUserProfile?: { __typename?: 'User', id: string } | undefined };

export type CreateTalentInvitationMutationVariables = Exact<{
  inviter_talent_id?: InputMaybe<Scalars['ID']>;
  inviter_company_user_id?: InputMaybe<Scalars['ID']>;
  email: Scalars['String'];
  first_name?: InputMaybe<Scalars['String']>;
  last_name?: InputMaybe<Scalars['String']>;
  tags_ids?: InputMaybe<Array<Scalars['ID']> | Scalars['ID']>;
}>;


export type CreateTalentInvitationMutation = { __typename?: 'Mutation', createInvitation?: { __typename?: 'TalentInvitation', id: string, uid: string, is_accepted: boolean, email?: string | undefined, first_name?: string | undefined, last_name?: string | undefined, created_at: any, talent?: { __typename?: 'Talent', id: string, first_name?: string | undefined, last_name?: string | undefined, email?: any | undefined, is_invitation_accepted?: boolean | undefined, avatar?: { __typename?: 'Avatar', avatar: string } | undefined } | undefined, talent_inviter?: { __typename?: 'Talent', id: string, first_name?: string | undefined, last_name?: string | undefined, email?: any | undefined, is_invitation_accepted?: boolean | undefined, avatar?: { __typename?: 'Avatar', avatar: string } | undefined } | undefined, company_user_inviter?: { __typename?: 'Talent', id: string, first_name?: string | undefined, last_name?: string | undefined, email?: any | undefined } | undefined } | undefined };

export type ResendInvitationMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type ResendInvitationMutation = { __typename?: 'Mutation', resendInvitation?: boolean | undefined };

export type CancelInvitationMutationVariables = Exact<{
  id: Scalars['ID'];
}>;


export type CancelInvitationMutation = { __typename?: 'Mutation', cancelInvitation?: boolean | undefined };

export type CurrentTalentInvitationsQueryVariables = Exact<{ [key: string]: never; }>;


export type CurrentTalentInvitationsQuery = { __typename?: 'Query', currentTalentInvitations?: Array<{ __typename?: 'TalentInvitation', id: string, uid: string, email?: string | undefined, is_accepted: boolean, first_name?: string | undefined, last_name?: string | undefined, created_at: any, talent?: { __typename?: 'Talent', id: string, first_name?: string | undefined, last_name?: string | undefined, email?: any | undefined, is_invitation_accepted?: boolean | undefined, avatar?: { __typename?: 'Avatar', avatar: string } | undefined } | undefined, talent_inviter?: { __typename?: 'Talent', join_reason?: Array<JoinReasonEnum | undefined> | undefined, stream_chat_id?: string | undefined, origin?: TalentOriginEnum | undefined, stream_chat_token?: string | undefined, default_company_referral_key?: string | undefined, first_name?: string | undefined, available_date_updated_at?: any | undefined, account_type: AccountTypeEnum, last_name?: string | undefined, address?: string | undefined, location?: string | undefined, recent_position_title?: string | undefined, about?: string | undefined, id: string, created_at?: any | undefined, rate?: number | undefined, rate_min?: number | undefined, rate_max?: number | undefined, salary?: number | undefined, is_invitation_accepted?: boolean | undefined, is_verification_required?: boolean | undefined, is_matcher?: boolean | undefined, email?: any | undefined, available_date?: any | undefined, hours_per_week?: number | undefined, available_now?: boolean | undefined, is_ot_pool?: boolean | undefined, reminded_at?: any | undefined, company_pool_connections?: Array<{ __typename?: 'TalentCompanyPoolConnection', status?: TalentCompanyPoolingStatusEnum | undefined, initiator?: TalentCompanyPoolingInitiatorEnum | undefined, cancel_initiator?: TalentCompanyPoolingInitiatorEnum | undefined, company?: { __typename?: 'Company', id: string, name: string } | undefined } | undefined> | undefined, account_settings?: { __typename?: 'TalentAccountSettings', receive_company_and_product_updates?: boolean | undefined, receive_direct_messages?: boolean | undefined, instant_matches_notifications_min_score?: InstantMatchesNotificationsScoreEnum | undefined } | undefined, companies: Array<{ __typename?: 'Company', id: string, name: string } | undefined>, category?: { __typename?: 'TalentCategory', id: string, name: string, slug: string, created_at?: any | undefined, updated_at?: any | undefined } | undefined, subcategories: Array<{ __typename?: 'Subcategory', id: string, name: string } | undefined>, source?: { __typename?: 'TalentSource', id: string, name: string, logo?: string | undefined, created_at?: any | undefined, updated_at?: any | undefined } | undefined, avatar?: { __typename?: 'Avatar', avatar: string, hash?: string | undefined } | undefined, documents?: Array<{ __typename?: 'TalentDocument', title: string, url: string, size: number, content_type: string, hash: string, created_at?: any | undefined, updated_at?: any | undefined } | undefined> | undefined, talent_data?: { __typename?: 'TalentData', facebook_profile_link?: string | undefined, linkedin_profile_link?: string | undefined, upwork_profile_link?: string | undefined, phone?: string | undefined, vat_number?: string | undefined } | undefined, invited_by?: { __typename?: 'TalentInvitedBy', company_name?: string | undefined, invitation_type?: TalentInvitationTypeEnum | undefined, id?: string | undefined, first_name_abac?: { __typename?: 'ABACString', value?: string | undefined } | undefined, last_name_abac?: { __typename?: 'ABACString', value?: string | undefined } | undefined } | undefined, talent_work_history?: Array<{ __typename?: 'TalentWorkHistory', id: string, position_title: string, company_name: string, worked_from?: any | undefined, worked_to?: any | undefined, created_at?: any | undefined, updated_at?: any | undefined } | undefined> | undefined, skills?: { __typename?: 'SkillPaginator', data: Array<{ __typename?: 'Skill', id: string, name: string, skill_type: SkillTypeEnum }> } | undefined } | undefined, company_user_inviter?: { __typename?: 'Talent', id: string, first_name?: string | undefined, last_name?: string | undefined, email?: any | undefined } | undefined } | undefined> | undefined };

export type ApplyForJobMutationVariables = Exact<{
  job_application_data: JobApplicationInput;
}>;


export type ApplyForJobMutation = { __typename?: 'Mutation', applyForJob: boolean };

export type CreateJobSuggestionMutationVariables = Exact<{
  job_id: Scalars['ID'];
  name: Scalars['String'];
  email: Scalars['String'];
}>;


export type CreateJobSuggestionMutation = { __typename?: 'Mutation', createJobSuggestion: boolean };

export type CreateNewJobMutationVariables = Exact<{
  name: Scalars['String'];
  category_id?: InputMaybe<Scalars['ID']>;
  subcategory_id?: InputMaybe<Scalars['ID']>;
  description: Scalars['String'];
  pitch?: InputMaybe<Scalars['String']>;
  start_date?: InputMaybe<Scalars['DateTimeUtc']>;
  end_date?: InputMaybe<Scalars['DateTimeUtc']>;
  rate_min?: InputMaybe<Scalars['Float']>;
  rate_max?: InputMaybe<Scalars['Float']>;
  is_rate_negotiable?: InputMaybe<Scalars['Boolean']>;
  salary_min?: InputMaybe<Scalars['Float']>;
  salary_max?: InputMaybe<Scalars['Float']>;
  is_salary_negotiable?: InputMaybe<Scalars['Boolean']>;
  max_project_budget?: InputMaybe<Scalars['Float']>;
  finders_fee?: InputMaybe<Scalars['Float']>;
  campaign_owner_id: Scalars['ID'];
  campaign_start_date: Scalars['DateTimeUtc'];
  campaign_end_date: Scalars['DateTimeUtc'];
  campaign_talent_pool?: InputMaybe<Array<InputMaybe<SourceTypeEnum>> | InputMaybe<SourceTypeEnum>>;
  permanent_capacity_type?: InputMaybe<PermanentJobCapacityTypeEnum>;
  is_archived?: InputMaybe<Scalars['Boolean']>;
  skills?: InputMaybe<Array<InputMaybe<Scalars['ID']>> | InputMaybe<Scalars['ID']>>;
  skills_boolean_filter?: InputMaybe<TalentsBooleanSkillsFilterInput>;
  solutions_required?: InputMaybe<Array<InputMaybe<Scalars['ID']>> | InputMaybe<Scalars['ID']>>;
  hard_skills_required?: InputMaybe<Array<InputMaybe<Scalars['ID']>> | InputMaybe<Scalars['ID']>>;
  hours_per_week?: InputMaybe<Scalars['Int']>;
  location_type?: InputMaybe<JobLocationTypeEnum>;
  type?: InputMaybe<JobTypeEnum>;
  location?: InputMaybe<Scalars['String']>;
  country?: InputMaybe<Scalars['String']>;
  city?: InputMaybe<Scalars['String']>;
  client?: InputMaybe<Scalars['String']>;
  is_remote_an_option?: InputMaybe<Scalars['Boolean']>;
  is_draft?: InputMaybe<Scalars['Boolean']>;
  office_hours_per_month?: InputMaybe<Scalars['Float']>;
}>;


export type CreateNewJobMutation = { __typename?: 'Mutation', createJob: string };

export type GetPotentialTalentsCountQueryVariables = Exact<{
  type: JobTypeEnum;
  category_id?: InputMaybe<Scalars['ID']>;
  subcategory_id?: InputMaybe<Scalars['ID']>;
  start_date?: InputMaybe<Scalars['DateTimeUtc']>;
  end_date?: InputMaybe<Scalars['DateTimeUtc']>;
  rate_min?: InputMaybe<Scalars['Float']>;
  rate_max?: InputMaybe<Scalars['Float']>;
  salary_min?: InputMaybe<Scalars['Float']>;
  salary_max?: InputMaybe<Scalars['Float']>;
  campaign_start_date?: InputMaybe<Scalars['DateTimeUtc']>;
  campaign_end_date?: InputMaybe<Scalars['DateTimeUtc']>;
  campaign_talent_pool?: InputMaybe<Array<InputMaybe<SourceTypeEnum>> | InputMaybe<SourceTypeEnum>>;
  skills?: InputMaybe<Array<InputMaybe<Scalars['ID']>> | InputMaybe<Scalars['ID']>>;
  skills_boolean_filter?: InputMaybe<TalentsBooleanSkillsFilterInput>;
  solutions_required?: InputMaybe<Array<InputMaybe<Scalars['ID']>> | InputMaybe<Scalars['ID']>>;
  hard_skills_required?: InputMaybe<Array<InputMaybe<Scalars['ID']>> | InputMaybe<Scalars['ID']>>;
  hours_per_week?: InputMaybe<Scalars['Int']>;
  location_type?: InputMaybe<JobLocationTypeEnum>;
  is_remote_an_option?: InputMaybe<Scalars['Boolean']>;
  country?: InputMaybe<Scalars['String']>;
  office_hours_per_month?: InputMaybe<Scalars['Float']>;
  campaign_owner_id?: InputMaybe<Scalars['ID']>;
}>;


export type GetPotentialTalentsCountQuery = { __typename?: 'Query', potentialTalentsForJobCount: number };

export type MarkNewJobMatchesAsReadMutationVariables = Exact<{
  job_matches_ids?: InputMaybe<Array<InputMaybe<Scalars['ID']>> | InputMaybe<Scalars['ID']>>;
}>;


export type MarkNewJobMatchesAsReadMutation = { __typename?: 'Mutation', markNewJobMatchesAsRead?: boolean | undefined };

export type UpdateJobMutationVariables = Exact<{
  id: Scalars['ID'];
  service_type?: InputMaybe<JobServiceTypeEnum>;
  name?: InputMaybe<Scalars['String']>;
  category_id?: InputMaybe<Scalars['ID']>;
  description?: InputMaybe<Scalars['String']>;
  pitch?: InputMaybe<Scalars['String']>;
  start_date?: InputMaybe<Scalars['DateTimeUtc']>;
  end_date?: InputMaybe<Scalars['DateTimeUtc']>;
  rate_min?: InputMaybe<Scalars['Float']>;
  rate_max?: InputMaybe<Scalars['Float']>;
  is_rate_negotiable?: InputMaybe<Scalars['Boolean']>;
  salary_min?: InputMaybe<Scalars['Float']>;
  salary_max?: InputMaybe<Scalars['Float']>;
  is_salary_negotiable?: InputMaybe<Scalars['Boolean']>;
  max_project_budget?: InputMaybe<Scalars['Float']>;
  finders_fee?: InputMaybe<Scalars['Float']>;
  campaign_owner_id?: InputMaybe<Scalars['ID']>;
  campaign_start_date: Scalars['DateTimeUtc'];
  campaign_end_date: Scalars['DateTimeUtc'];
  campaign_talent_pool?: InputMaybe<Array<InputMaybe<SourceTypeEnum>> | InputMaybe<SourceTypeEnum>>;
  skills_boolean_filter?: InputMaybe<TalentsBooleanSkillsFilterInput>;
  is_archived?: InputMaybe<Scalars['Boolean']>;
  skills?: InputMaybe<Array<InputMaybe<Scalars['ID']>> | InputMaybe<Scalars['ID']>>;
  solutions_required?: InputMaybe<Array<InputMaybe<Scalars['ID']>> | InputMaybe<Scalars['ID']>>;
  hard_skills_required?: InputMaybe<Array<InputMaybe<Scalars['ID']>> | InputMaybe<Scalars['ID']>>;
  hours_per_week?: InputMaybe<Scalars['Int']>;
  location_type?: InputMaybe<JobLocationTypeEnum>;
  type?: InputMaybe<JobTypeEnum>;
  location?: InputMaybe<Scalars['String']>;
  country?: InputMaybe<Scalars['String']>;
  city?: InputMaybe<Scalars['String']>;
  client?: InputMaybe<Scalars['String']>;
  is_remote_an_option?: InputMaybe<Scalars['Boolean']>;
  is_draft?: InputMaybe<Scalars['Boolean']>;
  office_hours_per_month?: InputMaybe<Scalars['Float']>;
}>;


export type UpdateJobMutation = { __typename?: 'Mutation', updateJob: { __typename?: 'Job', id: string } };

export type GetCompaniesLandingDataQueryVariables = Exact<{ [key: string]: never; }>;


export type GetCompaniesLandingDataQuery = { __typename?: 'Query', commonAppInfo?: { __typename?: 'CommonAppInfo', total_ot_freelancers_count: number, total_ot_freelancers_countries_count: number, total_unique_skills_count: number, total_unique_companies_count: number, total_ot_recruiters_count: number, total_ot_approved_freelancers_count: number } | undefined, talentsCountByCategories?: Array<{ __typename?: 'talentsCountItem', name: string, slug: string, talents_count: number } | undefined> | undefined };

export type SearchSkillsQueryVariables = Exact<{
  search: Scalars['String'];
  skill_type: Array<InputMaybe<SkillTypeEnum>> | InputMaybe<SkillTypeEnum>;
}>;


export type SearchSkillsQuery = { __typename?: 'Query', skillSearch: Array<{ __typename?: 'Skill', id: string, skill_type: SkillTypeEnum, name: string, slug: string, created_at?: any | undefined, updated_at?: any | undefined }> };

export type CancelCurrentTalentSubscriptionMutationVariables = Exact<{ [key: string]: never; }>;


export type CancelCurrentTalentSubscriptionMutation = { __typename?: 'Mutation', cancelCurrentTalentSubscription?: { __typename?: 'StripeSubscription', start_date?: any | undefined, current_period_end?: any | undefined, cancel_at_period_end?: boolean | undefined } | undefined };

export type CheckTalentExistsByEmailQueryVariables = Exact<{
  talent_email: Scalars['Email'];
}>;


export type CheckTalentExistsByEmailQuery = { __typename?: 'Query', checkTalentExistsByEmail: boolean };

export type CreateTalentProfileMutationVariables = Exact<{
  email: Scalars['Email'];
  general_info: TalentGeneralInfoInput;
  tags_ids?: InputMaybe<Array<Scalars['ID']> | Scalars['ID']>;
  is_ot_pool?: InputMaybe<Scalars['Boolean']>;
  job_application_data?: InputMaybe<JobApplicationInput>;
}>;


export type CreateTalentProfileMutation = { __typename?: 'Mutation', createTalentProfile?: string | undefined };

export type CurrentTalentSubscriptionQueryVariables = Exact<{ [key: string]: never; }>;


export type CurrentTalentSubscriptionQuery = { __typename?: 'Query', currentTalentSubscription?: { __typename?: 'StripeSubscription', start_date?: any | undefined, current_period_end?: any | undefined, cancel_at_period_end?: boolean | undefined } | undefined };

export type GetPaymentLinkForTalentQueryVariables = Exact<{
  talent_id?: InputMaybe<Scalars['ID']>;
}>;


export type GetPaymentLinkForTalentQuery = { __typename?: 'Query', getPaymentLinkForTalent?: string | undefined };

export type GetPaymentLinkForMatcherQueryVariables = Exact<{
  talent_id?: InputMaybe<Scalars['ID']>;
}>;


export type GetPaymentLinkForMatcherQuery = { __typename?: 'Query', getPaymentLinkForMatcher?: string | undefined };

export type GetTypeformPaymentLinkCodeForMatcherQueryVariables = Exact<{
  talent_id?: InputMaybe<Scalars['ID']>;
}>;


export type GetTypeformPaymentLinkCodeForMatcherQuery = { __typename?: 'Query', getTypeformPaymentLinkCodeForMatcher?: string | undefined };

export type NotifyTalentAboutJobMatchMutationVariables = Exact<{
  match_id: Scalars['ID'];
}>;


export type NotifyTalentAboutJobMatchMutation = { __typename?: 'Mutation', notifyTalentAboutJobMatch?: boolean | undefined };

export type RenewCurrentTalentSubscriptionMutationVariables = Exact<{ [key: string]: never; }>;


export type RenewCurrentTalentSubscriptionMutation = { __typename?: 'Mutation', renewCurrentTalentSubscription?: { __typename?: 'StripeSubscription', start_date?: any | undefined, current_period_end?: any | undefined, cancel_at_period_end?: boolean | undefined } | undefined };

export type SearchTalentsQueryVariables = Exact<{
  search?: InputMaybe<Scalars['String']>;
  category_ids?: InputMaybe<Array<InputMaybe<Scalars['ID']>> | InputMaybe<Scalars['ID']>>;
  skills_ids?: InputMaybe<Array<InputMaybe<Scalars['ID']>> | InputMaybe<Scalars['ID']>>;
  max_rate?: InputMaybe<Scalars['Float']>;
  min_rate?: InputMaybe<Scalars['Float']>;
  source_type?: InputMaybe<SourceTypeEnum>;
  is_active?: InputMaybe<Scalars['Boolean']>;
  is_verification_required?: InputMaybe<Scalars['Boolean']>;
  first?: InputMaybe<Scalars['Int']>;
  page?: InputMaybe<Scalars['Int']>;
  available_now?: InputMaybe<Scalars['Boolean']>;
  skills_boolean_filter?: InputMaybe<TalentsBooleanSkillsFilterInput>;
}>;


export type SearchTalentsQuery = { __typename?: 'Query', talentsSearch?: { __typename?: 'TalentsSearchPaginator', custom_paginator_info: { __typename?: 'customPaginatorInfo', current_page: number, last_page: number, per_page: number, total: number }, data: Array<{ __typename?: 'TalentSearchResult', highlights?: Array<{ __typename?: 'TalentSearchHighlight', source?: string | undefined, text?: string | undefined }> | undefined, talent?: { __typename?: 'Talent', id: string, reminded_at?: any | undefined, created_at?: any | undefined, is_invitation_accepted?: boolean | undefined, is_verification_required?: boolean | undefined, first_name?: string | undefined, last_name?: string | undefined, address?: string | undefined, rate?: number | undefined, recent_position_title?: string | undefined, email?: any | undefined, available_now?: boolean | undefined, location?: string | undefined, is_ot_pool?: boolean | undefined, source?: { __typename?: 'TalentSource', id: string } | undefined, avatar?: { __typename?: 'Avatar', avatar: string } | undefined, talent_data?: { __typename?: 'TalentData', phone?: string | undefined } | undefined } | undefined }> } | undefined };

export type SearchTalentsToSendMessageQueryVariables = Exact<{
  category_ids?: InputMaybe<Array<InputMaybe<Scalars['ID']>> | InputMaybe<Scalars['ID']>>;
  source_type?: InputMaybe<SourceTypeEnum>;
  first?: InputMaybe<Scalars['Int']>;
  page?: InputMaybe<Scalars['Int']>;
  tags_ids?: InputMaybe<Array<InputMaybe<Scalars['ID']>> | InputMaybe<Scalars['ID']>>;
}>;


export type SearchTalentsToSendMessageQuery = { __typename?: 'Query', talentsSearch?: { __typename?: 'TalentsSearchPaginator', data: Array<{ __typename?: 'TalentSearchResult', talent?: { __typename?: 'Talent', id: string, stream_chat_id?: string | undefined, stream_chat_token?: string | undefined, first_name?: string | undefined, last_name?: string | undefined, recent_position_title?: string | undefined, avatar?: { __typename?: 'Avatar', avatar: string } | undefined } | undefined }> } | undefined };

export type TalentCategoriesQueryVariables = Exact<{ [key: string]: never; }>;


export type TalentCategoriesQuery = { __typename?: 'Query', talentCategories?: Array<{ __typename?: 'TalentCategory', id: string, name: string, slug: string, created_at?: any | undefined, updated_at?: any | undefined, subcategories?: Array<{ __typename?: 'Subcategory', id: string, name: string }> | undefined } | undefined> | undefined };

export type TalentsCountByCategoriesQueryVariables = Exact<{
  category_ids?: InputMaybe<Array<InputMaybe<Scalars['ID']>> | InputMaybe<Scalars['ID']>>;
}>;


export type TalentsCountByCategoriesQuery = { __typename?: 'Query', talentsCountByCategories?: Array<{ __typename?: 'talentsCountItem', talents_count: number, name: string, slug: string } | undefined> | undefined };

export type TalentsCountBySubcategoriesQueryVariables = Exact<{
  subcategory_ids?: InputMaybe<Array<InputMaybe<Scalars['ID']>> | InputMaybe<Scalars['ID']>>;
}>;


export type TalentsCountBySubcategoriesQuery = { __typename?: 'Query', talentsCountBySubcategories?: Array<{ __typename?: 'talentsCountItem', talents_count: number, name: string, slug: string } | undefined> | undefined };


export const GetAtsRecordsDocument = gql`
    query GetATSRecords($talent_id: ID, $job_id: ID) {
  ATSRecords(talent_id: $talent_id, job_id: $job_id) {
    id
    type
    actor {
      first_name
      last_name
      avatar {
        avatar
      }
    }
    date
    __typename
    ... on ATSRecordComment {
      text
    }
    ... on ATSRecordJobMatchAction {
      job_match_action {
        id
        job_match_action_type
        job_match_type_before
        job_match_type_after
        job_match {
          job {
            id
            name
          }
        }
      }
    }
  }
}
    `;

/**
 * __useGetAtsRecordsQuery__
 *
 * To run a query within a React component, call `useGetAtsRecordsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAtsRecordsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAtsRecordsQuery({
 *   variables: {
 *      talent_id: // value for 'talent_id'
 *      job_id: // value for 'job_id'
 *   },
 * });
 */
export function useGetAtsRecordsQuery(baseOptions?: Apollo.QueryHookOptions<GetAtsRecordsQuery, GetAtsRecordsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetAtsRecordsQuery, GetAtsRecordsQueryVariables>(GetAtsRecordsDocument, options);
      }
export function useGetAtsRecordsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetAtsRecordsQuery, GetAtsRecordsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetAtsRecordsQuery, GetAtsRecordsQueryVariables>(GetAtsRecordsDocument, options);
        }
export type GetAtsRecordsQueryHookResult = ReturnType<typeof useGetAtsRecordsQuery>;
export type GetAtsRecordsLazyQueryHookResult = ReturnType<typeof useGetAtsRecordsLazyQuery>;
export type GetAtsRecordsQueryResult = Apollo.QueryResult<GetAtsRecordsQuery, GetAtsRecordsQueryVariables>;
export const ApplyCurrentTalentToCompanyPoolDocument = gql`
    mutation ApplyCurrentTalentToCompanyPool($company_id: ID!) {
  applyCurrentTalentToCompanyPool(company_id: $company_id)
}
    `;
export type ApplyCurrentTalentToCompanyPoolMutationFn = Apollo.MutationFunction<ApplyCurrentTalentToCompanyPoolMutation, ApplyCurrentTalentToCompanyPoolMutationVariables>;

/**
 * __useApplyCurrentTalentToCompanyPoolMutation__
 *
 * To run a mutation, you first call `useApplyCurrentTalentToCompanyPoolMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useApplyCurrentTalentToCompanyPoolMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [applyCurrentTalentToCompanyPoolMutation, { data, loading, error }] = useApplyCurrentTalentToCompanyPoolMutation({
 *   variables: {
 *      company_id: // value for 'company_id'
 *   },
 * });
 */
export function useApplyCurrentTalentToCompanyPoolMutation(baseOptions?: Apollo.MutationHookOptions<ApplyCurrentTalentToCompanyPoolMutation, ApplyCurrentTalentToCompanyPoolMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ApplyCurrentTalentToCompanyPoolMutation, ApplyCurrentTalentToCompanyPoolMutationVariables>(ApplyCurrentTalentToCompanyPoolDocument, options);
      }
export type ApplyCurrentTalentToCompanyPoolMutationHookResult = ReturnType<typeof useApplyCurrentTalentToCompanyPoolMutation>;
export type ApplyCurrentTalentToCompanyPoolMutationResult = Apollo.MutationResult<ApplyCurrentTalentToCompanyPoolMutation>;
export type ApplyCurrentTalentToCompanyPoolMutationOptions = Apollo.BaseMutationOptions<ApplyCurrentTalentToCompanyPoolMutation, ApplyCurrentTalentToCompanyPoolMutationVariables>;
export const LeaveCurrentTalentFromCompanyPoolDocument = gql`
    mutation LeaveCurrentTalentFromCompanyPool($company_id: ID!) {
  leaveCurrentTalentFromCompanyPool(company_id: $company_id)
}
    `;
export type LeaveCurrentTalentFromCompanyPoolMutationFn = Apollo.MutationFunction<LeaveCurrentTalentFromCompanyPoolMutation, LeaveCurrentTalentFromCompanyPoolMutationVariables>;

/**
 * __useLeaveCurrentTalentFromCompanyPoolMutation__
 *
 * To run a mutation, you first call `useLeaveCurrentTalentFromCompanyPoolMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLeaveCurrentTalentFromCompanyPoolMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [leaveCurrentTalentFromCompanyPoolMutation, { data, loading, error }] = useLeaveCurrentTalentFromCompanyPoolMutation({
 *   variables: {
 *      company_id: // value for 'company_id'
 *   },
 * });
 */
export function useLeaveCurrentTalentFromCompanyPoolMutation(baseOptions?: Apollo.MutationHookOptions<LeaveCurrentTalentFromCompanyPoolMutation, LeaveCurrentTalentFromCompanyPoolMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<LeaveCurrentTalentFromCompanyPoolMutation, LeaveCurrentTalentFromCompanyPoolMutationVariables>(LeaveCurrentTalentFromCompanyPoolDocument, options);
      }
export type LeaveCurrentTalentFromCompanyPoolMutationHookResult = ReturnType<typeof useLeaveCurrentTalentFromCompanyPoolMutation>;
export type LeaveCurrentTalentFromCompanyPoolMutationResult = Apollo.MutationResult<LeaveCurrentTalentFromCompanyPoolMutation>;
export type LeaveCurrentTalentFromCompanyPoolMutationOptions = Apollo.BaseMutationOptions<LeaveCurrentTalentFromCompanyPoolMutation, LeaveCurrentTalentFromCompanyPoolMutationVariables>;
export const AcceptCompanyPoolInvitationDocument = gql`
    mutation AcceptCompanyPoolInvitation($company_id: ID!) {
  acceptCompanyPoolInvitation(company_id: $company_id)
}
    `;
export type AcceptCompanyPoolInvitationMutationFn = Apollo.MutationFunction<AcceptCompanyPoolInvitationMutation, AcceptCompanyPoolInvitationMutationVariables>;

/**
 * __useAcceptCompanyPoolInvitationMutation__
 *
 * To run a mutation, you first call `useAcceptCompanyPoolInvitationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useAcceptCompanyPoolInvitationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [acceptCompanyPoolInvitationMutation, { data, loading, error }] = useAcceptCompanyPoolInvitationMutation({
 *   variables: {
 *      company_id: // value for 'company_id'
 *   },
 * });
 */
export function useAcceptCompanyPoolInvitationMutation(baseOptions?: Apollo.MutationHookOptions<AcceptCompanyPoolInvitationMutation, AcceptCompanyPoolInvitationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<AcceptCompanyPoolInvitationMutation, AcceptCompanyPoolInvitationMutationVariables>(AcceptCompanyPoolInvitationDocument, options);
      }
export type AcceptCompanyPoolInvitationMutationHookResult = ReturnType<typeof useAcceptCompanyPoolInvitationMutation>;
export type AcceptCompanyPoolInvitationMutationResult = Apollo.MutationResult<AcceptCompanyPoolInvitationMutation>;
export type AcceptCompanyPoolInvitationMutationOptions = Apollo.BaseMutationOptions<AcceptCompanyPoolInvitationMutation, AcceptCompanyPoolInvitationMutationVariables>;
export const RejectCompanyPoolInvitationDocument = gql`
    mutation RejectCompanyPoolInvitation($company_id: ID!) {
  rejectCompanyPoolInvitation(company_id: $company_id)
}
    `;
export type RejectCompanyPoolInvitationMutationFn = Apollo.MutationFunction<RejectCompanyPoolInvitationMutation, RejectCompanyPoolInvitationMutationVariables>;

/**
 * __useRejectCompanyPoolInvitationMutation__
 *
 * To run a mutation, you first call `useRejectCompanyPoolInvitationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRejectCompanyPoolInvitationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [rejectCompanyPoolInvitationMutation, { data, loading, error }] = useRejectCompanyPoolInvitationMutation({
 *   variables: {
 *      company_id: // value for 'company_id'
 *   },
 * });
 */
export function useRejectCompanyPoolInvitationMutation(baseOptions?: Apollo.MutationHookOptions<RejectCompanyPoolInvitationMutation, RejectCompanyPoolInvitationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RejectCompanyPoolInvitationMutation, RejectCompanyPoolInvitationMutationVariables>(RejectCompanyPoolInvitationDocument, options);
      }
export type RejectCompanyPoolInvitationMutationHookResult = ReturnType<typeof useRejectCompanyPoolInvitationMutation>;
export type RejectCompanyPoolInvitationMutationResult = Apollo.MutationResult<RejectCompanyPoolInvitationMutation>;
export type RejectCompanyPoolInvitationMutationOptions = Apollo.BaseMutationOptions<RejectCompanyPoolInvitationMutation, RejectCompanyPoolInvitationMutationVariables>;
export const InviteTalentToCompanyPoolDocument = gql`
    mutation InviteTalentToCompanyPool($talent_id: ID!) {
  inviteTalentToCompanyPool(talent_id: $talent_id)
}
    `;
export type InviteTalentToCompanyPoolMutationFn = Apollo.MutationFunction<InviteTalentToCompanyPoolMutation, InviteTalentToCompanyPoolMutationVariables>;

/**
 * __useInviteTalentToCompanyPoolMutation__
 *
 * To run a mutation, you first call `useInviteTalentToCompanyPoolMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useInviteTalentToCompanyPoolMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [inviteTalentToCompanyPoolMutation, { data, loading, error }] = useInviteTalentToCompanyPoolMutation({
 *   variables: {
 *      talent_id: // value for 'talent_id'
 *   },
 * });
 */
export function useInviteTalentToCompanyPoolMutation(baseOptions?: Apollo.MutationHookOptions<InviteTalentToCompanyPoolMutation, InviteTalentToCompanyPoolMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<InviteTalentToCompanyPoolMutation, InviteTalentToCompanyPoolMutationVariables>(InviteTalentToCompanyPoolDocument, options);
      }
export type InviteTalentToCompanyPoolMutationHookResult = ReturnType<typeof useInviteTalentToCompanyPoolMutation>;
export type InviteTalentToCompanyPoolMutationResult = Apollo.MutationResult<InviteTalentToCompanyPoolMutation>;
export type InviteTalentToCompanyPoolMutationOptions = Apollo.BaseMutationOptions<InviteTalentToCompanyPoolMutation, InviteTalentToCompanyPoolMutationVariables>;
export const RejectTalentCompanyPoolApplicationDocument = gql`
    mutation RejectTalentCompanyPoolApplication($talent_id: ID!) {
  rejectTalentCompanyPoolApplication(talent_id: $talent_id)
}
    `;
export type RejectTalentCompanyPoolApplicationMutationFn = Apollo.MutationFunction<RejectTalentCompanyPoolApplicationMutation, RejectTalentCompanyPoolApplicationMutationVariables>;

/**
 * __useRejectTalentCompanyPoolApplicationMutation__
 *
 * To run a mutation, you first call `useRejectTalentCompanyPoolApplicationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRejectTalentCompanyPoolApplicationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [rejectTalentCompanyPoolApplicationMutation, { data, loading, error }] = useRejectTalentCompanyPoolApplicationMutation({
 *   variables: {
 *      talent_id: // value for 'talent_id'
 *   },
 * });
 */
export function useRejectTalentCompanyPoolApplicationMutation(baseOptions?: Apollo.MutationHookOptions<RejectTalentCompanyPoolApplicationMutation, RejectTalentCompanyPoolApplicationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RejectTalentCompanyPoolApplicationMutation, RejectTalentCompanyPoolApplicationMutationVariables>(RejectTalentCompanyPoolApplicationDocument, options);
      }
export type RejectTalentCompanyPoolApplicationMutationHookResult = ReturnType<typeof useRejectTalentCompanyPoolApplicationMutation>;
export type RejectTalentCompanyPoolApplicationMutationResult = Apollo.MutationResult<RejectTalentCompanyPoolApplicationMutation>;
export type RejectTalentCompanyPoolApplicationMutationOptions = Apollo.BaseMutationOptions<RejectTalentCompanyPoolApplicationMutation, RejectTalentCompanyPoolApplicationMutationVariables>;
export const RemoveTalentFromCompanyPoolDocument = gql`
    mutation RemoveTalentFromCompanyPool($talent_id: ID!, $message: String) {
  removeTalentFromCompanyPool(talent_id: $talent_id, message: $message)
}
    `;
export type RemoveTalentFromCompanyPoolMutationFn = Apollo.MutationFunction<RemoveTalentFromCompanyPoolMutation, RemoveTalentFromCompanyPoolMutationVariables>;

/**
 * __useRemoveTalentFromCompanyPoolMutation__
 *
 * To run a mutation, you first call `useRemoveTalentFromCompanyPoolMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRemoveTalentFromCompanyPoolMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [removeTalentFromCompanyPoolMutation, { data, loading, error }] = useRemoveTalentFromCompanyPoolMutation({
 *   variables: {
 *      talent_id: // value for 'talent_id'
 *      message: // value for 'message'
 *   },
 * });
 */
export function useRemoveTalentFromCompanyPoolMutation(baseOptions?: Apollo.MutationHookOptions<RemoveTalentFromCompanyPoolMutation, RemoveTalentFromCompanyPoolMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RemoveTalentFromCompanyPoolMutation, RemoveTalentFromCompanyPoolMutationVariables>(RemoveTalentFromCompanyPoolDocument, options);
      }
export type RemoveTalentFromCompanyPoolMutationHookResult = ReturnType<typeof useRemoveTalentFromCompanyPoolMutation>;
export type RemoveTalentFromCompanyPoolMutationResult = Apollo.MutationResult<RemoveTalentFromCompanyPoolMutation>;
export type RemoveTalentFromCompanyPoolMutationOptions = Apollo.BaseMutationOptions<RemoveTalentFromCompanyPoolMutation, RemoveTalentFromCompanyPoolMutationVariables>;
export const ApproveTalentCompanyPoolApplicationDocument = gql`
    mutation ApproveTalentCompanyPoolApplication($talent_id: ID!) {
  approveTalentCompanyPoolApplication(talent_id: $talent_id)
}
    `;
export type ApproveTalentCompanyPoolApplicationMutationFn = Apollo.MutationFunction<ApproveTalentCompanyPoolApplicationMutation, ApproveTalentCompanyPoolApplicationMutationVariables>;

/**
 * __useApproveTalentCompanyPoolApplicationMutation__
 *
 * To run a mutation, you first call `useApproveTalentCompanyPoolApplicationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useApproveTalentCompanyPoolApplicationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [approveTalentCompanyPoolApplicationMutation, { data, loading, error }] = useApproveTalentCompanyPoolApplicationMutation({
 *   variables: {
 *      talent_id: // value for 'talent_id'
 *   },
 * });
 */
export function useApproveTalentCompanyPoolApplicationMutation(baseOptions?: Apollo.MutationHookOptions<ApproveTalentCompanyPoolApplicationMutation, ApproveTalentCompanyPoolApplicationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ApproveTalentCompanyPoolApplicationMutation, ApproveTalentCompanyPoolApplicationMutationVariables>(ApproveTalentCompanyPoolApplicationDocument, options);
      }
export type ApproveTalentCompanyPoolApplicationMutationHookResult = ReturnType<typeof useApproveTalentCompanyPoolApplicationMutation>;
export type ApproveTalentCompanyPoolApplicationMutationResult = Apollo.MutationResult<ApproveTalentCompanyPoolApplicationMutation>;
export type ApproveTalentCompanyPoolApplicationMutationOptions = Apollo.BaseMutationOptions<ApproveTalentCompanyPoolApplicationMutation, ApproveTalentCompanyPoolApplicationMutationVariables>;
export const RevokeCompanyPoolInvitationDocument = gql`
    mutation RevokeCompanyPoolInvitation($talent_id: ID!) {
  revokeCompanyPoolInvitation(talent_id: $talent_id)
}
    `;
export type RevokeCompanyPoolInvitationMutationFn = Apollo.MutationFunction<RevokeCompanyPoolInvitationMutation, RevokeCompanyPoolInvitationMutationVariables>;

/**
 * __useRevokeCompanyPoolInvitationMutation__
 *
 * To run a mutation, you first call `useRevokeCompanyPoolInvitationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRevokeCompanyPoolInvitationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [revokeCompanyPoolInvitationMutation, { data, loading, error }] = useRevokeCompanyPoolInvitationMutation({
 *   variables: {
 *      talent_id: // value for 'talent_id'
 *   },
 * });
 */
export function useRevokeCompanyPoolInvitationMutation(baseOptions?: Apollo.MutationHookOptions<RevokeCompanyPoolInvitationMutation, RevokeCompanyPoolInvitationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RevokeCompanyPoolInvitationMutation, RevokeCompanyPoolInvitationMutationVariables>(RevokeCompanyPoolInvitationDocument, options);
      }
export type RevokeCompanyPoolInvitationMutationHookResult = ReturnType<typeof useRevokeCompanyPoolInvitationMutation>;
export type RevokeCompanyPoolInvitationMutationResult = Apollo.MutationResult<RevokeCompanyPoolInvitationMutation>;
export type RevokeCompanyPoolInvitationMutationOptions = Apollo.BaseMutationOptions<RevokeCompanyPoolInvitationMutation, RevokeCompanyPoolInvitationMutationVariables>;
export const GetDashboardInfoDocument = gql`
    query GetDashboardInfo {
  pendingTalents: talents(source_type: OWN, is_verification_required: true) {
    paginatorInfo {
      total
    }
  }
  generalAnalytics {
    key
    value
    slug
  }
  tagsAnalytics {
    key
    value
    slug
  }
  countryAnalytics {
    key
    value
    slug
  }
  categoryAnalytics {
    key
    value
    slug
  }
  lastTalents {
    first_name
    last_name
    id
    location
    avatar {
      avatar
    }
    rate
    is_invitation_accepted
    recent_position_title
  }
}
    `;

/**
 * __useGetDashboardInfoQuery__
 *
 * To run a query within a React component, call `useGetDashboardInfoQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetDashboardInfoQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetDashboardInfoQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetDashboardInfoQuery(baseOptions?: Apollo.QueryHookOptions<GetDashboardInfoQuery, GetDashboardInfoQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetDashboardInfoQuery, GetDashboardInfoQueryVariables>(GetDashboardInfoDocument, options);
      }
export function useGetDashboardInfoLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetDashboardInfoQuery, GetDashboardInfoQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetDashboardInfoQuery, GetDashboardInfoQueryVariables>(GetDashboardInfoDocument, options);
        }
export type GetDashboardInfoQueryHookResult = ReturnType<typeof useGetDashboardInfoQuery>;
export type GetDashboardInfoLazyQueryHookResult = ReturnType<typeof useGetDashboardInfoLazyQuery>;
export type GetDashboardInfoQueryResult = Apollo.QueryResult<GetDashboardInfoQuery, GetDashboardInfoQueryVariables>;
export const GetPendingTalentsCountDocument = gql`
    query GetPendingTalentsCount($source_type: SourceTypeEnum = OWN, $is_verification_required: Boolean = true, $first: Int = 1, $page: Int = 1) {
  talentsSearch(
    source_type: $source_type
    first: $first
    page: $page
    is_verification_required: $is_verification_required
  ) {
    custom_paginator_info {
      total
    }
  }
}
    `;

/**
 * __useGetPendingTalentsCountQuery__
 *
 * To run a query within a React component, call `useGetPendingTalentsCountQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPendingTalentsCountQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPendingTalentsCountQuery({
 *   variables: {
 *      source_type: // value for 'source_type'
 *      is_verification_required: // value for 'is_verification_required'
 *      first: // value for 'first'
 *      page: // value for 'page'
 *   },
 * });
 */
export function useGetPendingTalentsCountQuery(baseOptions?: Apollo.QueryHookOptions<GetPendingTalentsCountQuery, GetPendingTalentsCountQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetPendingTalentsCountQuery, GetPendingTalentsCountQueryVariables>(GetPendingTalentsCountDocument, options);
      }
export function useGetPendingTalentsCountLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetPendingTalentsCountQuery, GetPendingTalentsCountQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetPendingTalentsCountQuery, GetPendingTalentsCountQueryVariables>(GetPendingTalentsCountDocument, options);
        }
export type GetPendingTalentsCountQueryHookResult = ReturnType<typeof useGetPendingTalentsCountQuery>;
export type GetPendingTalentsCountLazyQueryHookResult = ReturnType<typeof useGetPendingTalentsCountLazyQuery>;
export type GetPendingTalentsCountQueryResult = Apollo.QueryResult<GetPendingTalentsCountQuery, GetPendingTalentsCountQueryVariables>;
export const ExpandJobDocument = gql`
    mutation ExpandJob($id: ID!, $campaign_end_date: DateTimeUtc) {
  updateJob(id: $id, campaign_end_date: $campaign_end_date) {
    id
  }
}
    `;
export type ExpandJobMutationFn = Apollo.MutationFunction<ExpandJobMutation, ExpandJobMutationVariables>;

/**
 * __useExpandJobMutation__
 *
 * To run a mutation, you first call `useExpandJobMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useExpandJobMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [expandJobMutation, { data, loading, error }] = useExpandJobMutation({
 *   variables: {
 *      id: // value for 'id'
 *      campaign_end_date: // value for 'campaign_end_date'
 *   },
 * });
 */
export function useExpandJobMutation(baseOptions?: Apollo.MutationHookOptions<ExpandJobMutation, ExpandJobMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ExpandJobMutation, ExpandJobMutationVariables>(ExpandJobDocument, options);
      }
export type ExpandJobMutationHookResult = ReturnType<typeof useExpandJobMutation>;
export type ExpandJobMutationResult = Apollo.MutationResult<ExpandJobMutation>;
export type ExpandJobMutationOptions = Apollo.BaseMutationOptions<ExpandJobMutation, ExpandJobMutationVariables>;
export const UpdateJobFinderFeeDocument = gql`
    mutation UpdateJobFinderFee($id: ID!, $finders_fee: Float) {
  updateJob(id: $id, finders_fee: $finders_fee) {
    id
  }
}
    `;
export type UpdateJobFinderFeeMutationFn = Apollo.MutationFunction<UpdateJobFinderFeeMutation, UpdateJobFinderFeeMutationVariables>;

/**
 * __useUpdateJobFinderFeeMutation__
 *
 * To run a mutation, you first call `useUpdateJobFinderFeeMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateJobFinderFeeMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateJobFinderFeeMutation, { data, loading, error }] = useUpdateJobFinderFeeMutation({
 *   variables: {
 *      id: // value for 'id'
 *      finders_fee: // value for 'finders_fee'
 *   },
 * });
 */
export function useUpdateJobFinderFeeMutation(baseOptions?: Apollo.MutationHookOptions<UpdateJobFinderFeeMutation, UpdateJobFinderFeeMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateJobFinderFeeMutation, UpdateJobFinderFeeMutationVariables>(UpdateJobFinderFeeDocument, options);
      }
export type UpdateJobFinderFeeMutationHookResult = ReturnType<typeof useUpdateJobFinderFeeMutation>;
export type UpdateJobFinderFeeMutationResult = Apollo.MutationResult<UpdateJobFinderFeeMutation>;
export type UpdateJobFinderFeeMutationOptions = Apollo.BaseMutationOptions<UpdateJobFinderFeeMutation, UpdateJobFinderFeeMutationVariables>;
export const GetJobMatchersApplicationsDocument = gql`
    query GetJobMatchersApplications($job_id: ID!, $status: JobMatcherApplicationStatusEnum) {
  jobMatcherApplications(job_id: $job_id, status: $status) {
    id
    job {
      id
    }
    matcher {
      first_name
      last_name
      id
      address
      location
      avatar {
        avatar
      }
    }
    status
  }
  jobMatchers(job_id: $job_id) {
    job {
      id
    }
    talent {
      id
      first_name
      last_name
      id
      address
      location
      avatar {
        avatar
      }
    }
  }
}
    `;

/**
 * __useGetJobMatchersApplicationsQuery__
 *
 * To run a query within a React component, call `useGetJobMatchersApplicationsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetJobMatchersApplicationsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetJobMatchersApplicationsQuery({
 *   variables: {
 *      job_id: // value for 'job_id'
 *      status: // value for 'status'
 *   },
 * });
 */
export function useGetJobMatchersApplicationsQuery(baseOptions: Apollo.QueryHookOptions<GetJobMatchersApplicationsQuery, GetJobMatchersApplicationsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetJobMatchersApplicationsQuery, GetJobMatchersApplicationsQueryVariables>(GetJobMatchersApplicationsDocument, options);
      }
export function useGetJobMatchersApplicationsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetJobMatchersApplicationsQuery, GetJobMatchersApplicationsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetJobMatchersApplicationsQuery, GetJobMatchersApplicationsQueryVariables>(GetJobMatchersApplicationsDocument, options);
        }
export type GetJobMatchersApplicationsQueryHookResult = ReturnType<typeof useGetJobMatchersApplicationsQuery>;
export type GetJobMatchersApplicationsLazyQueryHookResult = ReturnType<typeof useGetJobMatchersApplicationsLazyQuery>;
export type GetJobMatchersApplicationsQueryResult = Apollo.QueryResult<GetJobMatchersApplicationsQuery, GetJobMatchersApplicationsQueryVariables>;
export const ApproveJobMacherApplicationDocument = gql`
    mutation ApproveJobMacherApplication($id: ID!) {
  approveJobMatcherApplication(id: $id) {
    id
  }
}
    `;
export type ApproveJobMacherApplicationMutationFn = Apollo.MutationFunction<ApproveJobMacherApplicationMutation, ApproveJobMacherApplicationMutationVariables>;

/**
 * __useApproveJobMacherApplicationMutation__
 *
 * To run a mutation, you first call `useApproveJobMacherApplicationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useApproveJobMacherApplicationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [approveJobMacherApplicationMutation, { data, loading, error }] = useApproveJobMacherApplicationMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useApproveJobMacherApplicationMutation(baseOptions?: Apollo.MutationHookOptions<ApproveJobMacherApplicationMutation, ApproveJobMacherApplicationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ApproveJobMacherApplicationMutation, ApproveJobMacherApplicationMutationVariables>(ApproveJobMacherApplicationDocument, options);
      }
export type ApproveJobMacherApplicationMutationHookResult = ReturnType<typeof useApproveJobMacherApplicationMutation>;
export type ApproveJobMacherApplicationMutationResult = Apollo.MutationResult<ApproveJobMacherApplicationMutation>;
export type ApproveJobMacherApplicationMutationOptions = Apollo.BaseMutationOptions<ApproveJobMacherApplicationMutation, ApproveJobMacherApplicationMutationVariables>;
export const RejectJobMacherApplicationDocument = gql`
    mutation RejectJobMacherApplication($id: ID!) {
  rejectJobMatcherApplication(id: $id) {
    id
  }
}
    `;
export type RejectJobMacherApplicationMutationFn = Apollo.MutationFunction<RejectJobMacherApplicationMutation, RejectJobMacherApplicationMutationVariables>;

/**
 * __useRejectJobMacherApplicationMutation__
 *
 * To run a mutation, you first call `useRejectJobMacherApplicationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRejectJobMacherApplicationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [rejectJobMacherApplicationMutation, { data, loading, error }] = useRejectJobMacherApplicationMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useRejectJobMacherApplicationMutation(baseOptions?: Apollo.MutationHookOptions<RejectJobMacherApplicationMutation, RejectJobMacherApplicationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RejectJobMacherApplicationMutation, RejectJobMacherApplicationMutationVariables>(RejectJobMacherApplicationDocument, options);
      }
export type RejectJobMacherApplicationMutationHookResult = ReturnType<typeof useRejectJobMacherApplicationMutation>;
export type RejectJobMacherApplicationMutationResult = Apollo.MutationResult<RejectJobMacherApplicationMutation>;
export type RejectJobMacherApplicationMutationOptions = Apollo.BaseMutationOptions<RejectJobMacherApplicationMutation, RejectJobMacherApplicationMutationVariables>;
export const RemoveJobMatcherFromJobDocument = gql`
    mutation RemoveJobMatcherFromJob($matcher_id: ID!, $job_id: ID!) {
  removeJobMatcherFromJob(matcher_id: $matcher_id, job_id: $job_id)
}
    `;
export type RemoveJobMatcherFromJobMutationFn = Apollo.MutationFunction<RemoveJobMatcherFromJobMutation, RemoveJobMatcherFromJobMutationVariables>;

/**
 * __useRemoveJobMatcherFromJobMutation__
 *
 * To run a mutation, you first call `useRemoveJobMatcherFromJobMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRemoveJobMatcherFromJobMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [removeJobMatcherFromJobMutation, { data, loading, error }] = useRemoveJobMatcherFromJobMutation({
 *   variables: {
 *      matcher_id: // value for 'matcher_id'
 *      job_id: // value for 'job_id'
 *   },
 * });
 */
export function useRemoveJobMatcherFromJobMutation(baseOptions?: Apollo.MutationHookOptions<RemoveJobMatcherFromJobMutation, RemoveJobMatcherFromJobMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RemoveJobMatcherFromJobMutation, RemoveJobMatcherFromJobMutationVariables>(RemoveJobMatcherFromJobDocument, options);
      }
export type RemoveJobMatcherFromJobMutationHookResult = ReturnType<typeof useRemoveJobMatcherFromJobMutation>;
export type RemoveJobMatcherFromJobMutationResult = Apollo.MutationResult<RemoveJobMatcherFromJobMutation>;
export type RemoveJobMatcherFromJobMutationOptions = Apollo.BaseMutationOptions<RemoveJobMatcherFromJobMutation, RemoveJobMatcherFromJobMutationVariables>;
export const CreateCompanyAccountAccessRequestDocument = gql`
    mutation CreateCompanyAccountAccessRequest($first_name: String!, $last_name: String!, $email: Email!, $company_id: ID, $new_company_name: String, $referral_key: String) {
  createCompanyAccountAccessRequest(
    first_name: $first_name
    last_name: $last_name
    email: $email
    company_id: $company_id
    new_company_name: $new_company_name
    referral_key: $referral_key
  )
}
    `;
export type CreateCompanyAccountAccessRequestMutationFn = Apollo.MutationFunction<CreateCompanyAccountAccessRequestMutation, CreateCompanyAccountAccessRequestMutationVariables>;

/**
 * __useCreateCompanyAccountAccessRequestMutation__
 *
 * To run a mutation, you first call `useCreateCompanyAccountAccessRequestMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateCompanyAccountAccessRequestMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createCompanyAccountAccessRequestMutation, { data, loading, error }] = useCreateCompanyAccountAccessRequestMutation({
 *   variables: {
 *      first_name: // value for 'first_name'
 *      last_name: // value for 'last_name'
 *      email: // value for 'email'
 *      company_id: // value for 'company_id'
 *      new_company_name: // value for 'new_company_name'
 *      referral_key: // value for 'referral_key'
 *   },
 * });
 */
export function useCreateCompanyAccountAccessRequestMutation(baseOptions?: Apollo.MutationHookOptions<CreateCompanyAccountAccessRequestMutation, CreateCompanyAccountAccessRequestMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateCompanyAccountAccessRequestMutation, CreateCompanyAccountAccessRequestMutationVariables>(CreateCompanyAccountAccessRequestDocument, options);
      }
export type CreateCompanyAccountAccessRequestMutationHookResult = ReturnType<typeof useCreateCompanyAccountAccessRequestMutation>;
export type CreateCompanyAccountAccessRequestMutationResult = Apollo.MutationResult<CreateCompanyAccountAccessRequestMutation>;
export type CreateCompanyAccountAccessRequestMutationOptions = Apollo.BaseMutationOptions<CreateCompanyAccountAccessRequestMutation, CreateCompanyAccountAccessRequestMutationVariables>;
export const GetPendingTalentsDocument = gql`
    query GetPendingTalents($source_type: SourceTypeEnum = OWN, $is_verification_required: Boolean = true, $first: Int = 10, $page: Int = 1) {
  talentsSearch(
    source_type: $source_type
    first: $first
    page: $page
    is_verification_required: $is_verification_required
  ) {
    custom_paginator_info {
      current_page
      last_page
      per_page
      total
    }
    data {
      highlights {
        source
        text
      }
      talent {
        company_pool_connections {
          company {
            id
            name
          }
          status
          initiator
          cancel_initiator
        }
        subcategories {
          id
          name
        }
        id
        reminded_at
        created_at
        is_invitation_accepted
        is_verification_required
        first_name
        first_name_abac {
          value
        }
        last_name
        last_name_abac {
          value
        }
        address
        job_applications {
          id
          job {
            id
            name
          }
        }
        rate
        recent_position_title
        email
        available_now
        location
        stream_chat_id
        is_ot_pool
        source {
          id
        }
        avatar {
          avatar
        }
        talent_data {
          phone
        }
      }
    }
  }
}
    `;

/**
 * __useGetPendingTalentsQuery__
 *
 * To run a query within a React component, call `useGetPendingTalentsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPendingTalentsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPendingTalentsQuery({
 *   variables: {
 *      source_type: // value for 'source_type'
 *      is_verification_required: // value for 'is_verification_required'
 *      first: // value for 'first'
 *      page: // value for 'page'
 *   },
 * });
 */
export function useGetPendingTalentsQuery(baseOptions?: Apollo.QueryHookOptions<GetPendingTalentsQuery, GetPendingTalentsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetPendingTalentsQuery, GetPendingTalentsQueryVariables>(GetPendingTalentsDocument, options);
      }
export function useGetPendingTalentsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetPendingTalentsQuery, GetPendingTalentsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetPendingTalentsQuery, GetPendingTalentsQueryVariables>(GetPendingTalentsDocument, options);
        }
export type GetPendingTalentsQueryHookResult = ReturnType<typeof useGetPendingTalentsQuery>;
export type GetPendingTalentsLazyQueryHookResult = ReturnType<typeof useGetPendingTalentsLazyQuery>;
export type GetPendingTalentsQueryResult = Apollo.QueryResult<GetPendingTalentsQuery, GetPendingTalentsQueryVariables>;
export const GetLeavePageInfoDocument = gql`
    query GetLeavePageInfo($delete_key: String!) {
  selfDeletePageInfo(delete_key: $delete_key) {
    invited_by_full_name
  }
}
    `;

/**
 * __useGetLeavePageInfoQuery__
 *
 * To run a query within a React component, call `useGetLeavePageInfoQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetLeavePageInfoQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetLeavePageInfoQuery({
 *   variables: {
 *      delete_key: // value for 'delete_key'
 *   },
 * });
 */
export function useGetLeavePageInfoQuery(baseOptions: Apollo.QueryHookOptions<GetLeavePageInfoQuery, GetLeavePageInfoQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetLeavePageInfoQuery, GetLeavePageInfoQueryVariables>(GetLeavePageInfoDocument, options);
      }
export function useGetLeavePageInfoLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetLeavePageInfoQuery, GetLeavePageInfoQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetLeavePageInfoQuery, GetLeavePageInfoQueryVariables>(GetLeavePageInfoDocument, options);
        }
export type GetLeavePageInfoQueryHookResult = ReturnType<typeof useGetLeavePageInfoQuery>;
export type GetLeavePageInfoLazyQueryHookResult = ReturnType<typeof useGetLeavePageInfoLazyQuery>;
export type GetLeavePageInfoQueryResult = Apollo.QueryResult<GetLeavePageInfoQuery, GetLeavePageInfoQueryVariables>;
export const RestoreAccountDocument = gql`
    mutation RestoreAccount($delete_key: String!) {
  restoreDeletedAccountByDeleteKey(delete_key: $delete_key) {
    link_to_set_password_page
  }
}
    `;
export type RestoreAccountMutationFn = Apollo.MutationFunction<RestoreAccountMutation, RestoreAccountMutationVariables>;

/**
 * __useRestoreAccountMutation__
 *
 * To run a mutation, you first call `useRestoreAccountMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRestoreAccountMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [restoreAccountMutation, { data, loading, error }] = useRestoreAccountMutation({
 *   variables: {
 *      delete_key: // value for 'delete_key'
 *   },
 * });
 */
export function useRestoreAccountMutation(baseOptions?: Apollo.MutationHookOptions<RestoreAccountMutation, RestoreAccountMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RestoreAccountMutation, RestoreAccountMutationVariables>(RestoreAccountDocument, options);
      }
export type RestoreAccountMutationHookResult = ReturnType<typeof useRestoreAccountMutation>;
export type RestoreAccountMutationResult = Apollo.MutationResult<RestoreAccountMutation>;
export type RestoreAccountMutationOptions = Apollo.BaseMutationOptions<RestoreAccountMutation, RestoreAccountMutationVariables>;
export const DeleteAccountDocument = gql`
    mutation DeleteAccount($delete_key: String!) {
  selfDeleteAccount(delete_key: $delete_key)
}
    `;
export type DeleteAccountMutationFn = Apollo.MutationFunction<DeleteAccountMutation, DeleteAccountMutationVariables>;

/**
 * __useDeleteAccountMutation__
 *
 * To run a mutation, you first call `useDeleteAccountMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteAccountMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteAccountMutation, { data, loading, error }] = useDeleteAccountMutation({
 *   variables: {
 *      delete_key: // value for 'delete_key'
 *   },
 * });
 */
export function useDeleteAccountMutation(baseOptions?: Apollo.MutationHookOptions<DeleteAccountMutation, DeleteAccountMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteAccountMutation, DeleteAccountMutationVariables>(DeleteAccountDocument, options);
      }
export type DeleteAccountMutationHookResult = ReturnType<typeof useDeleteAccountMutation>;
export type DeleteAccountMutationResult = Apollo.MutationResult<DeleteAccountMutation>;
export type DeleteAccountMutationOptions = Apollo.BaseMutationOptions<DeleteAccountMutation, DeleteAccountMutationVariables>;
export const SetTagsToTalentDocument = gql`
    mutation SetTagsToTalent($tags_ids: [ID!]!, $talent_id: ID!) {
  setTagsToTalent(tags_ids: $tags_ids, talent_id: $talent_id)
}
    `;
export type SetTagsToTalentMutationFn = Apollo.MutationFunction<SetTagsToTalentMutation, SetTagsToTalentMutationVariables>;

/**
 * __useSetTagsToTalentMutation__
 *
 * To run a mutation, you first call `useSetTagsToTalentMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSetTagsToTalentMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [setTagsToTalentMutation, { data, loading, error }] = useSetTagsToTalentMutation({
 *   variables: {
 *      tags_ids: // value for 'tags_ids'
 *      talent_id: // value for 'talent_id'
 *   },
 * });
 */
export function useSetTagsToTalentMutation(baseOptions?: Apollo.MutationHookOptions<SetTagsToTalentMutation, SetTagsToTalentMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SetTagsToTalentMutation, SetTagsToTalentMutationVariables>(SetTagsToTalentDocument, options);
      }
export type SetTagsToTalentMutationHookResult = ReturnType<typeof useSetTagsToTalentMutation>;
export type SetTagsToTalentMutationResult = Apollo.MutationResult<SetTagsToTalentMutation>;
export type SetTagsToTalentMutationOptions = Apollo.BaseMutationOptions<SetTagsToTalentMutation, SetTagsToTalentMutationVariables>;
export const CreateJobMatcherApplicationDocument = gql`
    mutation CreateJobMatcherApplication($job_id: ID!, $message: String) {
  createJobMatcherApplication(job_id: $job_id, message: $message) {
    id
  }
}
    `;
export type CreateJobMatcherApplicationMutationFn = Apollo.MutationFunction<CreateJobMatcherApplicationMutation, CreateJobMatcherApplicationMutationVariables>;

/**
 * __useCreateJobMatcherApplicationMutation__
 *
 * To run a mutation, you first call `useCreateJobMatcherApplicationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateJobMatcherApplicationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createJobMatcherApplicationMutation, { data, loading, error }] = useCreateJobMatcherApplicationMutation({
 *   variables: {
 *      job_id: // value for 'job_id'
 *      message: // value for 'message'
 *   },
 * });
 */
export function useCreateJobMatcherApplicationMutation(baseOptions?: Apollo.MutationHookOptions<CreateJobMatcherApplicationMutation, CreateJobMatcherApplicationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateJobMatcherApplicationMutation, CreateJobMatcherApplicationMutationVariables>(CreateJobMatcherApplicationDocument, options);
      }
export type CreateJobMatcherApplicationMutationHookResult = ReturnType<typeof useCreateJobMatcherApplicationMutation>;
export type CreateJobMatcherApplicationMutationResult = Apollo.MutationResult<CreateJobMatcherApplicationMutation>;
export type CreateJobMatcherApplicationMutationOptions = Apollo.BaseMutationOptions<CreateJobMatcherApplicationMutation, CreateJobMatcherApplicationMutationVariables>;
export const GetCompaniesScreenDataDocument = gql`
    query GetCompaniesScreenData {
  poolingCompanies {
    id
    name
    about
    country
    size
    type_of_activity
    logo {
      url
    }
  }
}
    `;

/**
 * __useGetCompaniesScreenDataQuery__
 *
 * To run a query within a React component, call `useGetCompaniesScreenDataQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCompaniesScreenDataQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCompaniesScreenDataQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetCompaniesScreenDataQuery(baseOptions?: Apollo.QueryHookOptions<GetCompaniesScreenDataQuery, GetCompaniesScreenDataQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetCompaniesScreenDataQuery, GetCompaniesScreenDataQueryVariables>(GetCompaniesScreenDataDocument, options);
      }
export function useGetCompaniesScreenDataLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetCompaniesScreenDataQuery, GetCompaniesScreenDataQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetCompaniesScreenDataQuery, GetCompaniesScreenDataQueryVariables>(GetCompaniesScreenDataDocument, options);
        }
export type GetCompaniesScreenDataQueryHookResult = ReturnType<typeof useGetCompaniesScreenDataQuery>;
export type GetCompaniesScreenDataLazyQueryHookResult = ReturnType<typeof useGetCompaniesScreenDataLazyQuery>;
export type GetCompaniesScreenDataQueryResult = Apollo.QueryResult<GetCompaniesScreenDataQuery, GetCompaniesScreenDataQueryVariables>;
export const InviteCompanyToOpentalentDocument = gql`
    mutation InviteCompanyToOpentalent($company_name: String!, $contact_person: String!, $company_email: String!) {
  inviteCompanyToOpentalent(
    company_name: $company_name
    contact_person: $contact_person
    company_email: $company_email
  )
}
    `;
export type InviteCompanyToOpentalentMutationFn = Apollo.MutationFunction<InviteCompanyToOpentalentMutation, InviteCompanyToOpentalentMutationVariables>;

/**
 * __useInviteCompanyToOpentalentMutation__
 *
 * To run a mutation, you first call `useInviteCompanyToOpentalentMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useInviteCompanyToOpentalentMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [inviteCompanyToOpentalentMutation, { data, loading, error }] = useInviteCompanyToOpentalentMutation({
 *   variables: {
 *      company_name: // value for 'company_name'
 *      contact_person: // value for 'contact_person'
 *      company_email: // value for 'company_email'
 *   },
 * });
 */
export function useInviteCompanyToOpentalentMutation(baseOptions?: Apollo.MutationHookOptions<InviteCompanyToOpentalentMutation, InviteCompanyToOpentalentMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<InviteCompanyToOpentalentMutation, InviteCompanyToOpentalentMutationVariables>(InviteCompanyToOpentalentDocument, options);
      }
export type InviteCompanyToOpentalentMutationHookResult = ReturnType<typeof useInviteCompanyToOpentalentMutation>;
export type InviteCompanyToOpentalentMutationResult = Apollo.MutationResult<InviteCompanyToOpentalentMutation>;
export type InviteCompanyToOpentalentMutationOptions = Apollo.BaseMutationOptions<InviteCompanyToOpentalentMutation, InviteCompanyToOpentalentMutationVariables>;
export const GetCompanyViewScreenDataDocument = gql`
    query GetCompanyViewScreenData($id: ID!) {
  company(id: $id) {
    id
    name
    about
    country
    size
    type_of_activity
    address
    website
    logo {
      url
    }
  }
  talentsSearch(
    connection_with_company_id: $id
    first: 3
    is_verification_required: false
  ) {
    custom_paginator_info {
      total
    }
    data {
      talent {
        avatar {
          avatar
        }
      }
    }
  }
  jobs(company_id: $id, first: 1, is_archived: false, is_draft: false) {
    paginatorInfo {
      total
    }
  }
}
    `;

/**
 * __useGetCompanyViewScreenDataQuery__
 *
 * To run a query within a React component, call `useGetCompanyViewScreenDataQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCompanyViewScreenDataQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCompanyViewScreenDataQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetCompanyViewScreenDataQuery(baseOptions: Apollo.QueryHookOptions<GetCompanyViewScreenDataQuery, GetCompanyViewScreenDataQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetCompanyViewScreenDataQuery, GetCompanyViewScreenDataQueryVariables>(GetCompanyViewScreenDataDocument, options);
      }
export function useGetCompanyViewScreenDataLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetCompanyViewScreenDataQuery, GetCompanyViewScreenDataQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetCompanyViewScreenDataQuery, GetCompanyViewScreenDataQueryVariables>(GetCompanyViewScreenDataDocument, options);
        }
export type GetCompanyViewScreenDataQueryHookResult = ReturnType<typeof useGetCompanyViewScreenDataQuery>;
export type GetCompanyViewScreenDataLazyQueryHookResult = ReturnType<typeof useGetCompanyViewScreenDataLazyQuery>;
export type GetCompanyViewScreenDataQueryResult = Apollo.QueryResult<GetCompanyViewScreenDataQuery, GetCompanyViewScreenDataQueryVariables>;
export const PublicPageGetJobDocument = gql`
    query PublicPageGetJob($id: ID!) {
  job(id: $id) {
    id
    name
    country
    city
    is_draft
    is_remote_an_option
    link_to_details
    office_hours_per_month
    skills {
      id
      name
      skill_type
      job_skill_pivot {
        is_required
      }
    }
    skills_boolean_filter {
      not_skills {
        id
        name
        skill_type
      }
      boolean_skills_filter_items {
        one_of_skills {
          id
          name
          skill_type
        }
        all_of_skills {
          id
          name
          skill_type
        }
        boolean_operator
      }
    }
    category {
      id
      name
    }
    subcategory {
      id
      name
    }
    description
    pitch
    start_date
    end_date
    rate_min
    rate_max
    is_rate_negotiable
    campaign_owner {
      id
      first_name
      last_name
      email
      company {
        name
        id
      }
    }
    invitations {
      id
      talent_id
    }
    campaign_start_date
    campaign_end_date
    campaign_talent_pool
    is_archived
    matches_count
    instant_matches_count
    matches_with_application_count
    salary_max
    salary_min
    finders_fee
    location
    hours_per_week
    location_type
    type
    period
    client
    capacity
    posted_at
  }
}
    `;

/**
 * __usePublicPageGetJobQuery__
 *
 * To run a query within a React component, call `usePublicPageGetJobQuery` and pass it any options that fit your needs.
 * When your component renders, `usePublicPageGetJobQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = usePublicPageGetJobQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function usePublicPageGetJobQuery(baseOptions: Apollo.QueryHookOptions<PublicPageGetJobQuery, PublicPageGetJobQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<PublicPageGetJobQuery, PublicPageGetJobQueryVariables>(PublicPageGetJobDocument, options);
      }
export function usePublicPageGetJobLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<PublicPageGetJobQuery, PublicPageGetJobQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<PublicPageGetJobQuery, PublicPageGetJobQueryVariables>(PublicPageGetJobDocument, options);
        }
export type PublicPageGetJobQueryHookResult = ReturnType<typeof usePublicPageGetJobQuery>;
export type PublicPageGetJobLazyQueryHookResult = ReturnType<typeof usePublicPageGetJobLazyQuery>;
export type PublicPageGetJobQueryResult = Apollo.QueryResult<PublicPageGetJobQuery, PublicPageGetJobQueryVariables>;
export const GetTalentSourcesDocument = gql`
    query GetTalentSources {
  talentSources {
    id
    name
    created_at
    updated_at
  }
}
    `;

/**
 * __useGetTalentSourcesQuery__
 *
 * To run a query within a React component, call `useGetTalentSourcesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetTalentSourcesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetTalentSourcesQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetTalentSourcesQuery(baseOptions?: Apollo.QueryHookOptions<GetTalentSourcesQuery, GetTalentSourcesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetTalentSourcesQuery, GetTalentSourcesQueryVariables>(GetTalentSourcesDocument, options);
      }
export function useGetTalentSourcesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetTalentSourcesQuery, GetTalentSourcesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetTalentSourcesQuery, GetTalentSourcesQueryVariables>(GetTalentSourcesDocument, options);
        }
export type GetTalentSourcesQueryHookResult = ReturnType<typeof useGetTalentSourcesQuery>;
export type GetTalentSourcesLazyQueryHookResult = ReturnType<typeof useGetTalentSourcesLazyQuery>;
export type GetTalentSourcesQueryResult = Apollo.QueryResult<GetTalentSourcesQuery, GetTalentSourcesQueryVariables>;
export const ChooseJobServiceDocument = gql`
    query ChooseJobService {
  commonAppInfo {
    total_ot_freelancers_count
    total_ot_approved_freelancers_count
    total_ot_recruiters_count
  }
  talentsSearch(source_type: OWN, is_active: true) {
    custom_paginator_info {
      total
    }
  }
}
    `;

/**
 * __useChooseJobServiceQuery__
 *
 * To run a query within a React component, call `useChooseJobServiceQuery` and pass it any options that fit your needs.
 * When your component renders, `useChooseJobServiceQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useChooseJobServiceQuery({
 *   variables: {
 *   },
 * });
 */
export function useChooseJobServiceQuery(baseOptions?: Apollo.QueryHookOptions<ChooseJobServiceQuery, ChooseJobServiceQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<ChooseJobServiceQuery, ChooseJobServiceQueryVariables>(ChooseJobServiceDocument, options);
      }
export function useChooseJobServiceLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<ChooseJobServiceQuery, ChooseJobServiceQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<ChooseJobServiceQuery, ChooseJobServiceQueryVariables>(ChooseJobServiceDocument, options);
        }
export type ChooseJobServiceQueryHookResult = ReturnType<typeof useChooseJobServiceQuery>;
export type ChooseJobServiceLazyQueryHookResult = ReturnType<typeof useChooseJobServiceLazyQuery>;
export type ChooseJobServiceQueryResult = Apollo.QueryResult<ChooseJobServiceQuery, ChooseJobServiceQueryVariables>;
export const GetTmpDataDocument = gql`
    query GetTmpData($email: Email, $device_id: String!) {
  getTempDataItem(email: $email, device_id: $device_id) {
    success
    result {
      email
      device_id
      data
    }
  }
}
    `;

/**
 * __useGetTmpDataQuery__
 *
 * To run a query within a React component, call `useGetTmpDataQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetTmpDataQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetTmpDataQuery({
 *   variables: {
 *      email: // value for 'email'
 *      device_id: // value for 'device_id'
 *   },
 * });
 */
export function useGetTmpDataQuery(baseOptions: Apollo.QueryHookOptions<GetTmpDataQuery, GetTmpDataQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetTmpDataQuery, GetTmpDataQueryVariables>(GetTmpDataDocument, options);
      }
export function useGetTmpDataLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetTmpDataQuery, GetTmpDataQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetTmpDataQuery, GetTmpDataQueryVariables>(GetTmpDataDocument, options);
        }
export type GetTmpDataQueryHookResult = ReturnType<typeof useGetTmpDataQuery>;
export type GetTmpDataLazyQueryHookResult = ReturnType<typeof useGetTmpDataLazyQuery>;
export type GetTmpDataQueryResult = Apollo.QueryResult<GetTmpDataQuery, GetTmpDataQueryVariables>;
export const SetTmpDataDocument = gql`
    mutation SetTmpData($email: Email, $device_id: String!, $data: String) {
  setTempDataItem(email: $email, device_id: $device_id, data: $data)
}
    `;
export type SetTmpDataMutationFn = Apollo.MutationFunction<SetTmpDataMutation, SetTmpDataMutationVariables>;

/**
 * __useSetTmpDataMutation__
 *
 * To run a mutation, you first call `useSetTmpDataMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSetTmpDataMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [setTmpDataMutation, { data, loading, error }] = useSetTmpDataMutation({
 *   variables: {
 *      email: // value for 'email'
 *      device_id: // value for 'device_id'
 *      data: // value for 'data'
 *   },
 * });
 */
export function useSetTmpDataMutation(baseOptions?: Apollo.MutationHookOptions<SetTmpDataMutation, SetTmpDataMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SetTmpDataMutation, SetTmpDataMutationVariables>(SetTmpDataDocument, options);
      }
export type SetTmpDataMutationHookResult = ReturnType<typeof useSetTmpDataMutation>;
export type SetTmpDataMutationResult = Apollo.MutationResult<SetTmpDataMutation>;
export type SetTmpDataMutationOptions = Apollo.BaseMutationOptions<SetTmpDataMutation, SetTmpDataMutationVariables>;
export const UploadFileDocument = gql`
    mutation UploadFile($file_type: FileTypeEnum!, $owner_id: ID, $file: Upload!) {
  uploadFile(file_type: $file_type, owner_id: $owner_id, file: $file) {
    hash
  }
}
    `;
export type UploadFileMutationFn = Apollo.MutationFunction<UploadFileMutation, UploadFileMutationVariables>;

/**
 * __useUploadFileMutation__
 *
 * To run a mutation, you first call `useUploadFileMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUploadFileMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [uploadFileMutation, { data, loading, error }] = useUploadFileMutation({
 *   variables: {
 *      file_type: // value for 'file_type'
 *      owner_id: // value for 'owner_id'
 *      file: // value for 'file'
 *   },
 * });
 */
export function useUploadFileMutation(baseOptions?: Apollo.MutationHookOptions<UploadFileMutation, UploadFileMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UploadFileMutation, UploadFileMutationVariables>(UploadFileDocument, options);
      }
export type UploadFileMutationHookResult = ReturnType<typeof useUploadFileMutation>;
export type UploadFileMutationResult = Apollo.MutationResult<UploadFileMutation>;
export type UploadFileMutationOptions = Apollo.BaseMutationOptions<UploadFileMutation, UploadFileMutationVariables>;
export const CancelCurrentCompanySubscriptionDocument = gql`
    mutation CancelCurrentCompanySubscription {
  cancelCurrentCompanyUserCompanySubscription {
    start_date
    current_period_end
    cancel_at_period_end
  }
}
    `;
export type CancelCurrentCompanySubscriptionMutationFn = Apollo.MutationFunction<CancelCurrentCompanySubscriptionMutation, CancelCurrentCompanySubscriptionMutationVariables>;

/**
 * __useCancelCurrentCompanySubscriptionMutation__
 *
 * To run a mutation, you first call `useCancelCurrentCompanySubscriptionMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCancelCurrentCompanySubscriptionMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [cancelCurrentCompanySubscriptionMutation, { data, loading, error }] = useCancelCurrentCompanySubscriptionMutation({
 *   variables: {
 *   },
 * });
 */
export function useCancelCurrentCompanySubscriptionMutation(baseOptions?: Apollo.MutationHookOptions<CancelCurrentCompanySubscriptionMutation, CancelCurrentCompanySubscriptionMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CancelCurrentCompanySubscriptionMutation, CancelCurrentCompanySubscriptionMutationVariables>(CancelCurrentCompanySubscriptionDocument, options);
      }
export type CancelCurrentCompanySubscriptionMutationHookResult = ReturnType<typeof useCancelCurrentCompanySubscriptionMutation>;
export type CancelCurrentCompanySubscriptionMutationResult = Apollo.MutationResult<CancelCurrentCompanySubscriptionMutation>;
export type CancelCurrentCompanySubscriptionMutationOptions = Apollo.BaseMutationOptions<CancelCurrentCompanySubscriptionMutation, CancelCurrentCompanySubscriptionMutationVariables>;
export const CheckCompanyUserExistsByEmailDocument = gql`
    query CheckCompanyUserExistsByEmail($email: Email!) {
  checkCompanyUserExistsByEmail(email: $email)
}
    `;

/**
 * __useCheckCompanyUserExistsByEmailQuery__
 *
 * To run a query within a React component, call `useCheckCompanyUserExistsByEmailQuery` and pass it any options that fit your needs.
 * When your component renders, `useCheckCompanyUserExistsByEmailQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCheckCompanyUserExistsByEmailQuery({
 *   variables: {
 *      email: // value for 'email'
 *   },
 * });
 */
export function useCheckCompanyUserExistsByEmailQuery(baseOptions: Apollo.QueryHookOptions<CheckCompanyUserExistsByEmailQuery, CheckCompanyUserExistsByEmailQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CheckCompanyUserExistsByEmailQuery, CheckCompanyUserExistsByEmailQueryVariables>(CheckCompanyUserExistsByEmailDocument, options);
      }
export function useCheckCompanyUserExistsByEmailLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CheckCompanyUserExistsByEmailQuery, CheckCompanyUserExistsByEmailQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CheckCompanyUserExistsByEmailQuery, CheckCompanyUserExistsByEmailQueryVariables>(CheckCompanyUserExistsByEmailDocument, options);
        }
export type CheckCompanyUserExistsByEmailQueryHookResult = ReturnType<typeof useCheckCompanyUserExistsByEmailQuery>;
export type CheckCompanyUserExistsByEmailLazyQueryHookResult = ReturnType<typeof useCheckCompanyUserExistsByEmailLazyQuery>;
export type CheckCompanyUserExistsByEmailQueryResult = Apollo.QueryResult<CheckCompanyUserExistsByEmailQuery, CheckCompanyUserExistsByEmailQueryVariables>;
export const CreateAnnouncementDocument = gql`
    mutation CreateAnnouncement($text: String, $title: String!, $need_response: Boolean!, $talents_ids: [ID]!, $send_email: Boolean) {
  createAnnouncement(
    title: $title
    text: $text
    need_response: $need_response
    talents_ids: $talents_ids
    send_email: $send_email
  )
}
    `;
export type CreateAnnouncementMutationFn = Apollo.MutationFunction<CreateAnnouncementMutation, CreateAnnouncementMutationVariables>;

/**
 * __useCreateAnnouncementMutation__
 *
 * To run a mutation, you first call `useCreateAnnouncementMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateAnnouncementMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createAnnouncementMutation, { data, loading, error }] = useCreateAnnouncementMutation({
 *   variables: {
 *      text: // value for 'text'
 *      title: // value for 'title'
 *      need_response: // value for 'need_response'
 *      talents_ids: // value for 'talents_ids'
 *      send_email: // value for 'send_email'
 *   },
 * });
 */
export function useCreateAnnouncementMutation(baseOptions?: Apollo.MutationHookOptions<CreateAnnouncementMutation, CreateAnnouncementMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateAnnouncementMutation, CreateAnnouncementMutationVariables>(CreateAnnouncementDocument, options);
      }
export type CreateAnnouncementMutationHookResult = ReturnType<typeof useCreateAnnouncementMutation>;
export type CreateAnnouncementMutationResult = Apollo.MutationResult<CreateAnnouncementMutation>;
export type CreateAnnouncementMutationOptions = Apollo.BaseMutationOptions<CreateAnnouncementMutation, CreateAnnouncementMutationVariables>;
export const CreateCompanyForUserDocument = gql`
    mutation CreateCompanyForUser($name: String!, $address: String, $type_of_activity: String, $website: String, $logo: String, $primary_company_user_id: ID) {
  createCompanyForUser(
    name: $name
    address: $address
    type_of_activity: $type_of_activity
    website: $website
    logo: $logo
    primary_company_user_id: $primary_company_user_id
  ) {
    id
  }
}
    `;
export type CreateCompanyForUserMutationFn = Apollo.MutationFunction<CreateCompanyForUserMutation, CreateCompanyForUserMutationVariables>;

/**
 * __useCreateCompanyForUserMutation__
 *
 * To run a mutation, you first call `useCreateCompanyForUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateCompanyForUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createCompanyForUserMutation, { data, loading, error }] = useCreateCompanyForUserMutation({
 *   variables: {
 *      name: // value for 'name'
 *      address: // value for 'address'
 *      type_of_activity: // value for 'type_of_activity'
 *      website: // value for 'website'
 *      logo: // value for 'logo'
 *      primary_company_user_id: // value for 'primary_company_user_id'
 *   },
 * });
 */
export function useCreateCompanyForUserMutation(baseOptions?: Apollo.MutationHookOptions<CreateCompanyForUserMutation, CreateCompanyForUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateCompanyForUserMutation, CreateCompanyForUserMutationVariables>(CreateCompanyForUserDocument, options);
      }
export type CreateCompanyForUserMutationHookResult = ReturnType<typeof useCreateCompanyForUserMutation>;
export type CreateCompanyForUserMutationResult = Apollo.MutationResult<CreateCompanyForUserMutation>;
export type CreateCompanyForUserMutationOptions = Apollo.BaseMutationOptions<CreateCompanyForUserMutation, CreateCompanyForUserMutationVariables>;
export const CreateCompanyUserDocument = gql`
    mutation CreateCompanyUser($first_name: String!, $last_name: String!, $email: Email!, $avatar: String, $position: String, $is_primary: Boolean, $onboarding_type: CompanyUserOnboardingTypeEnum, $referral_key: String) {
  createCompanyUserProfile(
    first_name: $first_name
    last_name: $last_name
    email: $email
    position: $position
    avatar: $avatar
    is_primary: $is_primary
    onboarding_type: $onboarding_type
    referral_key: $referral_key
  ) {
    id
  }
}
    `;
export type CreateCompanyUserMutationFn = Apollo.MutationFunction<CreateCompanyUserMutation, CreateCompanyUserMutationVariables>;

/**
 * __useCreateCompanyUserMutation__
 *
 * To run a mutation, you first call `useCreateCompanyUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateCompanyUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createCompanyUserMutation, { data, loading, error }] = useCreateCompanyUserMutation({
 *   variables: {
 *      first_name: // value for 'first_name'
 *      last_name: // value for 'last_name'
 *      email: // value for 'email'
 *      avatar: // value for 'avatar'
 *      position: // value for 'position'
 *      is_primary: // value for 'is_primary'
 *      onboarding_type: // value for 'onboarding_type'
 *      referral_key: // value for 'referral_key'
 *   },
 * });
 */
export function useCreateCompanyUserMutation(baseOptions?: Apollo.MutationHookOptions<CreateCompanyUserMutation, CreateCompanyUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateCompanyUserMutation, CreateCompanyUserMutationVariables>(CreateCompanyUserDocument, options);
      }
export type CreateCompanyUserMutationHookResult = ReturnType<typeof useCreateCompanyUserMutation>;
export type CreateCompanyUserMutationResult = Apollo.MutationResult<CreateCompanyUserMutation>;
export type CreateCompanyUserMutationOptions = Apollo.BaseMutationOptions<CreateCompanyUserMutation, CreateCompanyUserMutationVariables>;
export const CurrentCompanySubscriptionDocument = gql`
    query CurrentCompanySubscription {
  currentCompanyUserCompanySubscription {
    start_date
    current_period_end
    cancel_at_period_end
  }
}
    `;

/**
 * __useCurrentCompanySubscriptionQuery__
 *
 * To run a query within a React component, call `useCurrentCompanySubscriptionQuery` and pass it any options that fit your needs.
 * When your component renders, `useCurrentCompanySubscriptionQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCurrentCompanySubscriptionQuery({
 *   variables: {
 *   },
 * });
 */
export function useCurrentCompanySubscriptionQuery(baseOptions?: Apollo.QueryHookOptions<CurrentCompanySubscriptionQuery, CurrentCompanySubscriptionQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CurrentCompanySubscriptionQuery, CurrentCompanySubscriptionQueryVariables>(CurrentCompanySubscriptionDocument, options);
      }
export function useCurrentCompanySubscriptionLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CurrentCompanySubscriptionQuery, CurrentCompanySubscriptionQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CurrentCompanySubscriptionQuery, CurrentCompanySubscriptionQueryVariables>(CurrentCompanySubscriptionDocument, options);
        }
export type CurrentCompanySubscriptionQueryHookResult = ReturnType<typeof useCurrentCompanySubscriptionQuery>;
export type CurrentCompanySubscriptionLazyQueryHookResult = ReturnType<typeof useCurrentCompanySubscriptionLazyQuery>;
export type CurrentCompanySubscriptionQueryResult = Apollo.QueryResult<CurrentCompanySubscriptionQuery, CurrentCompanySubscriptionQueryVariables>;
export const GetCurrentUserCompanyTagsDocument = gql`
    query GetCurrentUserCompanyTags {
  currentUserCompanyTags {
    name
    id
  }
}
    `;

/**
 * __useGetCurrentUserCompanyTagsQuery__
 *
 * To run a query within a React component, call `useGetCurrentUserCompanyTagsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCurrentUserCompanyTagsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCurrentUserCompanyTagsQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetCurrentUserCompanyTagsQuery(baseOptions?: Apollo.QueryHookOptions<GetCurrentUserCompanyTagsQuery, GetCurrentUserCompanyTagsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetCurrentUserCompanyTagsQuery, GetCurrentUserCompanyTagsQueryVariables>(GetCurrentUserCompanyTagsDocument, options);
      }
export function useGetCurrentUserCompanyTagsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetCurrentUserCompanyTagsQuery, GetCurrentUserCompanyTagsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetCurrentUserCompanyTagsQuery, GetCurrentUserCompanyTagsQueryVariables>(GetCurrentUserCompanyTagsDocument, options);
        }
export type GetCurrentUserCompanyTagsQueryHookResult = ReturnType<typeof useGetCurrentUserCompanyTagsQuery>;
export type GetCurrentUserCompanyTagsLazyQueryHookResult = ReturnType<typeof useGetCurrentUserCompanyTagsLazyQuery>;
export type GetCurrentUserCompanyTagsQueryResult = Apollo.QueryResult<GetCurrentUserCompanyTagsQuery, GetCurrentUserCompanyTagsQueryVariables>;
export const GetPaymentLinkForCompanyDocument = gql`
    query GetPaymentLinkForCompany($primary_user_id: ID) {
  getPaymentLinkForCompany(primary_user_id: $primary_user_id)
}
    `;

/**
 * __useGetPaymentLinkForCompanyQuery__
 *
 * To run a query within a React component, call `useGetPaymentLinkForCompanyQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPaymentLinkForCompanyQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPaymentLinkForCompanyQuery({
 *   variables: {
 *      primary_user_id: // value for 'primary_user_id'
 *   },
 * });
 */
export function useGetPaymentLinkForCompanyQuery(baseOptions?: Apollo.QueryHookOptions<GetPaymentLinkForCompanyQuery, GetPaymentLinkForCompanyQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetPaymentLinkForCompanyQuery, GetPaymentLinkForCompanyQueryVariables>(GetPaymentLinkForCompanyDocument, options);
      }
export function useGetPaymentLinkForCompanyLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetPaymentLinkForCompanyQuery, GetPaymentLinkForCompanyQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetPaymentLinkForCompanyQuery, GetPaymentLinkForCompanyQueryVariables>(GetPaymentLinkForCompanyDocument, options);
        }
export type GetPaymentLinkForCompanyQueryHookResult = ReturnType<typeof useGetPaymentLinkForCompanyQuery>;
export type GetPaymentLinkForCompanyLazyQueryHookResult = ReturnType<typeof useGetPaymentLinkForCompanyLazyQuery>;
export type GetPaymentLinkForCompanyQueryResult = Apollo.QueryResult<GetPaymentLinkForCompanyQuery, GetPaymentLinkForCompanyQueryVariables>;
export const InviteCompanyUserDocument = gql`
    mutation InviteCompanyUser($email: Email!) {
  inviteCompanyUser(email: $email)
}
    `;
export type InviteCompanyUserMutationFn = Apollo.MutationFunction<InviteCompanyUserMutation, InviteCompanyUserMutationVariables>;

/**
 * __useInviteCompanyUserMutation__
 *
 * To run a mutation, you first call `useInviteCompanyUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useInviteCompanyUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [inviteCompanyUserMutation, { data, loading, error }] = useInviteCompanyUserMutation({
 *   variables: {
 *      email: // value for 'email'
 *   },
 * });
 */
export function useInviteCompanyUserMutation(baseOptions?: Apollo.MutationHookOptions<InviteCompanyUserMutation, InviteCompanyUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<InviteCompanyUserMutation, InviteCompanyUserMutationVariables>(InviteCompanyUserDocument, options);
      }
export type InviteCompanyUserMutationHookResult = ReturnType<typeof useInviteCompanyUserMutation>;
export type InviteCompanyUserMutationResult = Apollo.MutationResult<InviteCompanyUserMutation>;
export type InviteCompanyUserMutationOptions = Apollo.BaseMutationOptions<InviteCompanyUserMutation, InviteCompanyUserMutationVariables>;
export const RenewCurrentCompanySubscriptionDocument = gql`
    mutation RenewCurrentCompanySubscription {
  renewCurrentCompanyUserCompanySubscription {
    start_date
    current_period_end
    cancel_at_period_end
  }
}
    `;
export type RenewCurrentCompanySubscriptionMutationFn = Apollo.MutationFunction<RenewCurrentCompanySubscriptionMutation, RenewCurrentCompanySubscriptionMutationVariables>;

/**
 * __useRenewCurrentCompanySubscriptionMutation__
 *
 * To run a mutation, you first call `useRenewCurrentCompanySubscriptionMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRenewCurrentCompanySubscriptionMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [renewCurrentCompanySubscriptionMutation, { data, loading, error }] = useRenewCurrentCompanySubscriptionMutation({
 *   variables: {
 *   },
 * });
 */
export function useRenewCurrentCompanySubscriptionMutation(baseOptions?: Apollo.MutationHookOptions<RenewCurrentCompanySubscriptionMutation, RenewCurrentCompanySubscriptionMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RenewCurrentCompanySubscriptionMutation, RenewCurrentCompanySubscriptionMutationVariables>(RenewCurrentCompanySubscriptionDocument, options);
      }
export type RenewCurrentCompanySubscriptionMutationHookResult = ReturnType<typeof useRenewCurrentCompanySubscriptionMutation>;
export type RenewCurrentCompanySubscriptionMutationResult = Apollo.MutationResult<RenewCurrentCompanySubscriptionMutation>;
export type RenewCurrentCompanySubscriptionMutationOptions = Apollo.BaseMutationOptions<RenewCurrentCompanySubscriptionMutation, RenewCurrentCompanySubscriptionMutationVariables>;
export const UpdateCompanyUserDocument = gql`
    mutation UpdateCompanyUser($company_user_id: ID!, $first_name: String, $last_name: String, $email: Email, $position: String, $avatar: String) {
  updateCompanyUserProfile(
    company_user_id: $company_user_id
    first_name: $first_name
    last_name: $last_name
    email: $email
    position: $position
    avatar: $avatar
  ) {
    id
  }
}
    `;
export type UpdateCompanyUserMutationFn = Apollo.MutationFunction<UpdateCompanyUserMutation, UpdateCompanyUserMutationVariables>;

/**
 * __useUpdateCompanyUserMutation__
 *
 * To run a mutation, you first call `useUpdateCompanyUserMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateCompanyUserMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateCompanyUserMutation, { data, loading, error }] = useUpdateCompanyUserMutation({
 *   variables: {
 *      company_user_id: // value for 'company_user_id'
 *      first_name: // value for 'first_name'
 *      last_name: // value for 'last_name'
 *      email: // value for 'email'
 *      position: // value for 'position'
 *      avatar: // value for 'avatar'
 *   },
 * });
 */
export function useUpdateCompanyUserMutation(baseOptions?: Apollo.MutationHookOptions<UpdateCompanyUserMutation, UpdateCompanyUserMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateCompanyUserMutation, UpdateCompanyUserMutationVariables>(UpdateCompanyUserDocument, options);
      }
export type UpdateCompanyUserMutationHookResult = ReturnType<typeof useUpdateCompanyUserMutation>;
export type UpdateCompanyUserMutationResult = Apollo.MutationResult<UpdateCompanyUserMutation>;
export type UpdateCompanyUserMutationOptions = Apollo.BaseMutationOptions<UpdateCompanyUserMutation, UpdateCompanyUserMutationVariables>;
export const CreateTalentInvitationDocument = gql`
    mutation CreateTalentInvitation($inviter_talent_id: ID, $inviter_company_user_id: ID, $email: String!, $first_name: String, $last_name: String, $tags_ids: [ID!]) {
  createInvitation(
    inviter_talent_id: $inviter_talent_id
    tags_ids: $tags_ids
    inviter_company_user_id: $inviter_company_user_id
    email: $email
    first_name: $first_name
    last_name: $last_name
  ) {
    id
    uid
    is_accepted
    email
    first_name
    last_name
    uid
    is_accepted
    talent {
      id
      first_name
      last_name
      email
      is_invitation_accepted
      avatar {
        avatar
      }
    }
    talent_inviter {
      id
      first_name
      last_name
      email
      is_invitation_accepted
      avatar {
        avatar
      }
    }
    company_user_inviter {
      id
      first_name
      last_name
      email
    }
    created_at
  }
}
    `;
export type CreateTalentInvitationMutationFn = Apollo.MutationFunction<CreateTalentInvitationMutation, CreateTalentInvitationMutationVariables>;

/**
 * __useCreateTalentInvitationMutation__
 *
 * To run a mutation, you first call `useCreateTalentInvitationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateTalentInvitationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createTalentInvitationMutation, { data, loading, error }] = useCreateTalentInvitationMutation({
 *   variables: {
 *      inviter_talent_id: // value for 'inviter_talent_id'
 *      inviter_company_user_id: // value for 'inviter_company_user_id'
 *      email: // value for 'email'
 *      first_name: // value for 'first_name'
 *      last_name: // value for 'last_name'
 *      tags_ids: // value for 'tags_ids'
 *   },
 * });
 */
export function useCreateTalentInvitationMutation(baseOptions?: Apollo.MutationHookOptions<CreateTalentInvitationMutation, CreateTalentInvitationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateTalentInvitationMutation, CreateTalentInvitationMutationVariables>(CreateTalentInvitationDocument, options);
      }
export type CreateTalentInvitationMutationHookResult = ReturnType<typeof useCreateTalentInvitationMutation>;
export type CreateTalentInvitationMutationResult = Apollo.MutationResult<CreateTalentInvitationMutation>;
export type CreateTalentInvitationMutationOptions = Apollo.BaseMutationOptions<CreateTalentInvitationMutation, CreateTalentInvitationMutationVariables>;
export const ResendInvitationDocument = gql`
    mutation ResendInvitation($id: ID!) {
  resendInvitation(id: $id)
}
    `;
export type ResendInvitationMutationFn = Apollo.MutationFunction<ResendInvitationMutation, ResendInvitationMutationVariables>;

/**
 * __useResendInvitationMutation__
 *
 * To run a mutation, you first call `useResendInvitationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useResendInvitationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [resendInvitationMutation, { data, loading, error }] = useResendInvitationMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useResendInvitationMutation(baseOptions?: Apollo.MutationHookOptions<ResendInvitationMutation, ResendInvitationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ResendInvitationMutation, ResendInvitationMutationVariables>(ResendInvitationDocument, options);
      }
export type ResendInvitationMutationHookResult = ReturnType<typeof useResendInvitationMutation>;
export type ResendInvitationMutationResult = Apollo.MutationResult<ResendInvitationMutation>;
export type ResendInvitationMutationOptions = Apollo.BaseMutationOptions<ResendInvitationMutation, ResendInvitationMutationVariables>;
export const CancelInvitationDocument = gql`
    mutation CancelInvitation($id: ID!) {
  cancelInvitation(id: $id)
}
    `;
export type CancelInvitationMutationFn = Apollo.MutationFunction<CancelInvitationMutation, CancelInvitationMutationVariables>;

/**
 * __useCancelInvitationMutation__
 *
 * To run a mutation, you first call `useCancelInvitationMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCancelInvitationMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [cancelInvitationMutation, { data, loading, error }] = useCancelInvitationMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useCancelInvitationMutation(baseOptions?: Apollo.MutationHookOptions<CancelInvitationMutation, CancelInvitationMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CancelInvitationMutation, CancelInvitationMutationVariables>(CancelInvitationDocument, options);
      }
export type CancelInvitationMutationHookResult = ReturnType<typeof useCancelInvitationMutation>;
export type CancelInvitationMutationResult = Apollo.MutationResult<CancelInvitationMutation>;
export type CancelInvitationMutationOptions = Apollo.BaseMutationOptions<CancelInvitationMutation, CancelInvitationMutationVariables>;
export const CurrentTalentInvitationsDocument = gql`
    query CurrentTalentInvitations {
  currentTalentInvitations {
    id
    uid
    talent {
      id
      first_name
      last_name
      email
      is_invitation_accepted
      avatar {
        avatar
      }
    }
    email
    is_accepted
    first_name
    last_name
    talent_inviter {
      company_pool_connections {
        company {
          id
          name
        }
        status
        initiator
        cancel_initiator
      }
      join_reason
      stream_chat_id
      origin
      stream_chat_token
      default_company_referral_key
      first_name
      available_date_updated_at
      account_type
      last_name
      address
      location
      recent_position_title
      about
      id
      created_at
      rate
      rate_min
      rate_max
      salary
      is_invitation_accepted
      is_verification_required
      is_matcher
      email
      available_date
      hours_per_week
      available_now
      is_ot_pool
      reminded_at
      account_settings {
        receive_company_and_product_updates
        receive_direct_messages
        instant_matches_notifications_min_score
      }
      companies {
        id
        name
      }
      category {
        id
        name
        slug
        created_at
        updated_at
      }
      subcategories {
        id
        name
      }
      source {
        id
        name
        logo
        created_at
        updated_at
      }
      avatar {
        avatar
        hash
      }
      documents {
        title
        url
        size
        content_type
        hash
        created_at
        updated_at
      }
      talent_data {
        facebook_profile_link
        linkedin_profile_link
        upwork_profile_link
        phone
        vat_number
      }
      invited_by {
        company_name
        first_name_abac {
          value
        }
        last_name_abac {
          value
        }
        invitation_type
        id
      }
      talent_work_history {
        id
        position_title
        company_name
        worked_from
        worked_to
        created_at
        updated_at
      }
      documents {
        title
        url
        size
        content_type
        hash
        created_at
        updated_at
      }
      skills(first: 50) {
        data {
          id
          skill_type: skill_type
          name
        }
      }
    }
    company_user_inviter {
      id
      first_name
      last_name
      email
    }
    created_at
  }
}
    `;

/**
 * __useCurrentTalentInvitationsQuery__
 *
 * To run a query within a React component, call `useCurrentTalentInvitationsQuery` and pass it any options that fit your needs.
 * When your component renders, `useCurrentTalentInvitationsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCurrentTalentInvitationsQuery({
 *   variables: {
 *   },
 * });
 */
export function useCurrentTalentInvitationsQuery(baseOptions?: Apollo.QueryHookOptions<CurrentTalentInvitationsQuery, CurrentTalentInvitationsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CurrentTalentInvitationsQuery, CurrentTalentInvitationsQueryVariables>(CurrentTalentInvitationsDocument, options);
      }
export function useCurrentTalentInvitationsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CurrentTalentInvitationsQuery, CurrentTalentInvitationsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CurrentTalentInvitationsQuery, CurrentTalentInvitationsQueryVariables>(CurrentTalentInvitationsDocument, options);
        }
export type CurrentTalentInvitationsQueryHookResult = ReturnType<typeof useCurrentTalentInvitationsQuery>;
export type CurrentTalentInvitationsLazyQueryHookResult = ReturnType<typeof useCurrentTalentInvitationsLazyQuery>;
export type CurrentTalentInvitationsQueryResult = Apollo.QueryResult<CurrentTalentInvitationsQuery, CurrentTalentInvitationsQueryVariables>;
export const ApplyForJobDocument = gql`
    mutation ApplyForJob($job_application_data: JobApplicationInput!) {
  applyForJob(job_application_data: $job_application_data)
}
    `;
export type ApplyForJobMutationFn = Apollo.MutationFunction<ApplyForJobMutation, ApplyForJobMutationVariables>;

/**
 * __useApplyForJobMutation__
 *
 * To run a mutation, you first call `useApplyForJobMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useApplyForJobMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [applyForJobMutation, { data, loading, error }] = useApplyForJobMutation({
 *   variables: {
 *      job_application_data: // value for 'job_application_data'
 *   },
 * });
 */
export function useApplyForJobMutation(baseOptions?: Apollo.MutationHookOptions<ApplyForJobMutation, ApplyForJobMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ApplyForJobMutation, ApplyForJobMutationVariables>(ApplyForJobDocument, options);
      }
export type ApplyForJobMutationHookResult = ReturnType<typeof useApplyForJobMutation>;
export type ApplyForJobMutationResult = Apollo.MutationResult<ApplyForJobMutation>;
export type ApplyForJobMutationOptions = Apollo.BaseMutationOptions<ApplyForJobMutation, ApplyForJobMutationVariables>;
export const CreateJobSuggestionDocument = gql`
    mutation CreateJobSuggestion($job_id: ID!, $name: String!, $email: String!) {
  createJobSuggestion(job_id: $job_id, name: $name, email: $email)
}
    `;
export type CreateJobSuggestionMutationFn = Apollo.MutationFunction<CreateJobSuggestionMutation, CreateJobSuggestionMutationVariables>;

/**
 * __useCreateJobSuggestionMutation__
 *
 * To run a mutation, you first call `useCreateJobSuggestionMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateJobSuggestionMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createJobSuggestionMutation, { data, loading, error }] = useCreateJobSuggestionMutation({
 *   variables: {
 *      job_id: // value for 'job_id'
 *      name: // value for 'name'
 *      email: // value for 'email'
 *   },
 * });
 */
export function useCreateJobSuggestionMutation(baseOptions?: Apollo.MutationHookOptions<CreateJobSuggestionMutation, CreateJobSuggestionMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateJobSuggestionMutation, CreateJobSuggestionMutationVariables>(CreateJobSuggestionDocument, options);
      }
export type CreateJobSuggestionMutationHookResult = ReturnType<typeof useCreateJobSuggestionMutation>;
export type CreateJobSuggestionMutationResult = Apollo.MutationResult<CreateJobSuggestionMutation>;
export type CreateJobSuggestionMutationOptions = Apollo.BaseMutationOptions<CreateJobSuggestionMutation, CreateJobSuggestionMutationVariables>;
export const CreateNewJobDocument = gql`
    mutation CreateNewJob($name: String!, $category_id: ID, $subcategory_id: ID, $description: String!, $pitch: String, $start_date: DateTimeUtc, $end_date: DateTimeUtc, $rate_min: Float, $rate_max: Float, $is_rate_negotiable: Boolean, $salary_min: Float, $salary_max: Float, $is_salary_negotiable: Boolean, $max_project_budget: Float, $finders_fee: Float, $campaign_owner_id: ID!, $campaign_start_date: DateTimeUtc!, $campaign_end_date: DateTimeUtc!, $campaign_talent_pool: [SourceTypeEnum], $permanent_capacity_type: PermanentJobCapacityTypeEnum, $is_archived: Boolean, $skills: [ID], $skills_boolean_filter: TalentsBooleanSkillsFilterInput, $solutions_required: [ID], $hard_skills_required: [ID], $hours_per_week: Int, $location_type: JobLocationTypeEnum, $type: JobTypeEnum, $location: String, $country: String, $city: String, $client: String, $is_remote_an_option: Boolean, $is_draft: Boolean, $office_hours_per_month: Float) {
  createJob(
    permanent_capacity_type: $permanent_capacity_type
    office_hours_per_month: $office_hours_per_month
    is_draft: $is_draft
    name: $name
    category_id: $category_id
    subcategory_id: $subcategory_id
    description: $description
    pitch: $pitch
    start_date: $start_date
    end_date: $end_date
    rate_min: $rate_min
    rate_max: $rate_max
    is_rate_negotiable: $is_rate_negotiable
    salary_min: $salary_min
    salary_max: $salary_max
    is_salary_negotiable: $is_salary_negotiable
    max_project_budget: $max_project_budget
    finders_fee: $finders_fee
    campaign_owner_id: $campaign_owner_id
    campaign_start_date: $campaign_start_date
    campaign_end_date: $campaign_end_date
    campaign_talent_pool: $campaign_talent_pool
    is_archived: $is_archived
    skills: $skills
    skills_boolean_filter: $skills_boolean_filter
    is_remote_an_option: $is_remote_an_option
    solutions_required: $solutions_required
    hard_skills_required: $hard_skills_required
    hours_per_week: $hours_per_week
    location_type: $location_type
    type: $type
    location: $location
    country: $country
    city: $city
    client: $client
  )
}
    `;
export type CreateNewJobMutationFn = Apollo.MutationFunction<CreateNewJobMutation, CreateNewJobMutationVariables>;

/**
 * __useCreateNewJobMutation__
 *
 * To run a mutation, you first call `useCreateNewJobMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateNewJobMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createNewJobMutation, { data, loading, error }] = useCreateNewJobMutation({
 *   variables: {
 *      name: // value for 'name'
 *      category_id: // value for 'category_id'
 *      subcategory_id: // value for 'subcategory_id'
 *      description: // value for 'description'
 *      pitch: // value for 'pitch'
 *      start_date: // value for 'start_date'
 *      end_date: // value for 'end_date'
 *      rate_min: // value for 'rate_min'
 *      rate_max: // value for 'rate_max'
 *      is_rate_negotiable: // value for 'is_rate_negotiable'
 *      salary_min: // value for 'salary_min'
 *      salary_max: // value for 'salary_max'
 *      is_salary_negotiable: // value for 'is_salary_negotiable'
 *      max_project_budget: // value for 'max_project_budget'
 *      finders_fee: // value for 'finders_fee'
 *      campaign_owner_id: // value for 'campaign_owner_id'
 *      campaign_start_date: // value for 'campaign_start_date'
 *      campaign_end_date: // value for 'campaign_end_date'
 *      campaign_talent_pool: // value for 'campaign_talent_pool'
 *      permanent_capacity_type: // value for 'permanent_capacity_type'
 *      is_archived: // value for 'is_archived'
 *      skills: // value for 'skills'
 *      skills_boolean_filter: // value for 'skills_boolean_filter'
 *      solutions_required: // value for 'solutions_required'
 *      hard_skills_required: // value for 'hard_skills_required'
 *      hours_per_week: // value for 'hours_per_week'
 *      location_type: // value for 'location_type'
 *      type: // value for 'type'
 *      location: // value for 'location'
 *      country: // value for 'country'
 *      city: // value for 'city'
 *      client: // value for 'client'
 *      is_remote_an_option: // value for 'is_remote_an_option'
 *      is_draft: // value for 'is_draft'
 *      office_hours_per_month: // value for 'office_hours_per_month'
 *   },
 * });
 */
export function useCreateNewJobMutation(baseOptions?: Apollo.MutationHookOptions<CreateNewJobMutation, CreateNewJobMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateNewJobMutation, CreateNewJobMutationVariables>(CreateNewJobDocument, options);
      }
export type CreateNewJobMutationHookResult = ReturnType<typeof useCreateNewJobMutation>;
export type CreateNewJobMutationResult = Apollo.MutationResult<CreateNewJobMutation>;
export type CreateNewJobMutationOptions = Apollo.BaseMutationOptions<CreateNewJobMutation, CreateNewJobMutationVariables>;
export const GetPotentialTalentsCountDocument = gql`
    query GetPotentialTalentsCount($type: JobTypeEnum!, $category_id: ID, $subcategory_id: ID, $start_date: DateTimeUtc, $end_date: DateTimeUtc, $rate_min: Float, $rate_max: Float, $salary_min: Float, $salary_max: Float, $campaign_start_date: DateTimeUtc, $campaign_end_date: DateTimeUtc, $campaign_talent_pool: [SourceTypeEnum], $skills: [ID], $skills_boolean_filter: TalentsBooleanSkillsFilterInput, $solutions_required: [ID], $hard_skills_required: [ID], $hours_per_week: Int, $location_type: JobLocationTypeEnum, $is_remote_an_option: Boolean, $country: String, $office_hours_per_month: Float, $campaign_owner_id: ID) {
  potentialTalentsForJobCount(
    type: $type
    campaign_owner_id: $campaign_owner_id
    category_id: $category_id
    subcategory_id: $subcategory_id
    start_date: $start_date
    end_date: $end_date
    rate_min: $rate_min
    rate_max: $rate_max
    salary_min: $salary_min
    salary_max: $salary_max
    campaign_start_date: $campaign_start_date
    campaign_end_date: $campaign_end_date
    campaign_talent_pool: $campaign_talent_pool
    skills: $skills
    skills_boolean_filter: $skills_boolean_filter
    solutions_required: $solutions_required
    hard_skills_required: $hard_skills_required
    hours_per_week: $hours_per_week
    location_type: $location_type
    is_remote_an_option: $is_remote_an_option
    country: $country
    office_hours_per_month: $office_hours_per_month
  )
}
    `;

/**
 * __useGetPotentialTalentsCountQuery__
 *
 * To run a query within a React component, call `useGetPotentialTalentsCountQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPotentialTalentsCountQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPotentialTalentsCountQuery({
 *   variables: {
 *      type: // value for 'type'
 *      category_id: // value for 'category_id'
 *      subcategory_id: // value for 'subcategory_id'
 *      start_date: // value for 'start_date'
 *      end_date: // value for 'end_date'
 *      rate_min: // value for 'rate_min'
 *      rate_max: // value for 'rate_max'
 *      salary_min: // value for 'salary_min'
 *      salary_max: // value for 'salary_max'
 *      campaign_start_date: // value for 'campaign_start_date'
 *      campaign_end_date: // value for 'campaign_end_date'
 *      campaign_talent_pool: // value for 'campaign_talent_pool'
 *      skills: // value for 'skills'
 *      skills_boolean_filter: // value for 'skills_boolean_filter'
 *      solutions_required: // value for 'solutions_required'
 *      hard_skills_required: // value for 'hard_skills_required'
 *      hours_per_week: // value for 'hours_per_week'
 *      location_type: // value for 'location_type'
 *      is_remote_an_option: // value for 'is_remote_an_option'
 *      country: // value for 'country'
 *      office_hours_per_month: // value for 'office_hours_per_month'
 *      campaign_owner_id: // value for 'campaign_owner_id'
 *   },
 * });
 */
export function useGetPotentialTalentsCountQuery(baseOptions: Apollo.QueryHookOptions<GetPotentialTalentsCountQuery, GetPotentialTalentsCountQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetPotentialTalentsCountQuery, GetPotentialTalentsCountQueryVariables>(GetPotentialTalentsCountDocument, options);
      }
export function useGetPotentialTalentsCountLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetPotentialTalentsCountQuery, GetPotentialTalentsCountQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetPotentialTalentsCountQuery, GetPotentialTalentsCountQueryVariables>(GetPotentialTalentsCountDocument, options);
        }
export type GetPotentialTalentsCountQueryHookResult = ReturnType<typeof useGetPotentialTalentsCountQuery>;
export type GetPotentialTalentsCountLazyQueryHookResult = ReturnType<typeof useGetPotentialTalentsCountLazyQuery>;
export type GetPotentialTalentsCountQueryResult = Apollo.QueryResult<GetPotentialTalentsCountQuery, GetPotentialTalentsCountQueryVariables>;
export const MarkNewJobMatchesAsReadDocument = gql`
    mutation MarkNewJobMatchesAsRead($job_matches_ids: [ID]) {
  markNewJobMatchesAsRead(job_matches_ids: $job_matches_ids)
}
    `;
export type MarkNewJobMatchesAsReadMutationFn = Apollo.MutationFunction<MarkNewJobMatchesAsReadMutation, MarkNewJobMatchesAsReadMutationVariables>;

/**
 * __useMarkNewJobMatchesAsReadMutation__
 *
 * To run a mutation, you first call `useMarkNewJobMatchesAsReadMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useMarkNewJobMatchesAsReadMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [markNewJobMatchesAsReadMutation, { data, loading, error }] = useMarkNewJobMatchesAsReadMutation({
 *   variables: {
 *      job_matches_ids: // value for 'job_matches_ids'
 *   },
 * });
 */
export function useMarkNewJobMatchesAsReadMutation(baseOptions?: Apollo.MutationHookOptions<MarkNewJobMatchesAsReadMutation, MarkNewJobMatchesAsReadMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<MarkNewJobMatchesAsReadMutation, MarkNewJobMatchesAsReadMutationVariables>(MarkNewJobMatchesAsReadDocument, options);
      }
export type MarkNewJobMatchesAsReadMutationHookResult = ReturnType<typeof useMarkNewJobMatchesAsReadMutation>;
export type MarkNewJobMatchesAsReadMutationResult = Apollo.MutationResult<MarkNewJobMatchesAsReadMutation>;
export type MarkNewJobMatchesAsReadMutationOptions = Apollo.BaseMutationOptions<MarkNewJobMatchesAsReadMutation, MarkNewJobMatchesAsReadMutationVariables>;
export const UpdateJobDocument = gql`
    mutation UpdateJob($id: ID!, $service_type: JobServiceTypeEnum, $name: String, $category_id: ID, $description: String, $pitch: String, $start_date: DateTimeUtc, $end_date: DateTimeUtc, $rate_min: Float, $rate_max: Float, $is_rate_negotiable: Boolean, $salary_min: Float, $salary_max: Float, $is_salary_negotiable: Boolean, $max_project_budget: Float, $finders_fee: Float, $campaign_owner_id: ID, $campaign_start_date: DateTimeUtc!, $campaign_end_date: DateTimeUtc!, $campaign_talent_pool: [SourceTypeEnum], $skills_boolean_filter: TalentsBooleanSkillsFilterInput, $is_archived: Boolean, $skills: [ID], $solutions_required: [ID], $hard_skills_required: [ID], $hours_per_week: Int, $location_type: JobLocationTypeEnum, $type: JobTypeEnum, $location: String, $country: String, $city: String, $client: String, $is_remote_an_option: Boolean, $is_draft: Boolean, $office_hours_per_month: Float) {
  updateJob(
    service_type: $service_type
    skills_boolean_filter: $skills_boolean_filter
    office_hours_per_month: $office_hours_per_month
    is_draft: $is_draft
    id: $id
    name: $name
    category_id: $category_id
    description: $description
    pitch: $pitch
    start_date: $start_date
    end_date: $end_date
    rate_min: $rate_min
    rate_max: $rate_max
    is_remote_an_option: $is_remote_an_option
    is_rate_negotiable: $is_rate_negotiable
    salary_min: $salary_min
    salary_max: $salary_max
    is_salary_negotiable: $is_salary_negotiable
    max_project_budget: $max_project_budget
    finders_fee: $finders_fee
    campaign_owner_id: $campaign_owner_id
    campaign_start_date: $campaign_start_date
    campaign_end_date: $campaign_end_date
    campaign_talent_pool: $campaign_talent_pool
    is_archived: $is_archived
    skills: $skills
    solutions_required: $solutions_required
    hard_skills_required: $hard_skills_required
    hours_per_week: $hours_per_week
    location_type: $location_type
    type: $type
    location: $location
    country: $country
    city: $city
    client: $client
  ) {
    id
  }
}
    `;
export type UpdateJobMutationFn = Apollo.MutationFunction<UpdateJobMutation, UpdateJobMutationVariables>;

/**
 * __useUpdateJobMutation__
 *
 * To run a mutation, you first call `useUpdateJobMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateJobMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateJobMutation, { data, loading, error }] = useUpdateJobMutation({
 *   variables: {
 *      id: // value for 'id'
 *      service_type: // value for 'service_type'
 *      name: // value for 'name'
 *      category_id: // value for 'category_id'
 *      description: // value for 'description'
 *      pitch: // value for 'pitch'
 *      start_date: // value for 'start_date'
 *      end_date: // value for 'end_date'
 *      rate_min: // value for 'rate_min'
 *      rate_max: // value for 'rate_max'
 *      is_rate_negotiable: // value for 'is_rate_negotiable'
 *      salary_min: // value for 'salary_min'
 *      salary_max: // value for 'salary_max'
 *      is_salary_negotiable: // value for 'is_salary_negotiable'
 *      max_project_budget: // value for 'max_project_budget'
 *      finders_fee: // value for 'finders_fee'
 *      campaign_owner_id: // value for 'campaign_owner_id'
 *      campaign_start_date: // value for 'campaign_start_date'
 *      campaign_end_date: // value for 'campaign_end_date'
 *      campaign_talent_pool: // value for 'campaign_talent_pool'
 *      skills_boolean_filter: // value for 'skills_boolean_filter'
 *      is_archived: // value for 'is_archived'
 *      skills: // value for 'skills'
 *      solutions_required: // value for 'solutions_required'
 *      hard_skills_required: // value for 'hard_skills_required'
 *      hours_per_week: // value for 'hours_per_week'
 *      location_type: // value for 'location_type'
 *      type: // value for 'type'
 *      location: // value for 'location'
 *      country: // value for 'country'
 *      city: // value for 'city'
 *      client: // value for 'client'
 *      is_remote_an_option: // value for 'is_remote_an_option'
 *      is_draft: // value for 'is_draft'
 *      office_hours_per_month: // value for 'office_hours_per_month'
 *   },
 * });
 */
export function useUpdateJobMutation(baseOptions?: Apollo.MutationHookOptions<UpdateJobMutation, UpdateJobMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateJobMutation, UpdateJobMutationVariables>(UpdateJobDocument, options);
      }
export type UpdateJobMutationHookResult = ReturnType<typeof useUpdateJobMutation>;
export type UpdateJobMutationResult = Apollo.MutationResult<UpdateJobMutation>;
export type UpdateJobMutationOptions = Apollo.BaseMutationOptions<UpdateJobMutation, UpdateJobMutationVariables>;
export const GetCompaniesLandingDataDocument = gql`
    query GetCompaniesLandingData {
  commonAppInfo {
    total_ot_freelancers_count
    total_ot_freelancers_countries_count
    total_unique_skills_count
    total_unique_companies_count
    total_ot_recruiters_count
    total_ot_approved_freelancers_count
  }
  talentsCountByCategories {
    name
    slug
    talents_count
  }
}
    `;

/**
 * __useGetCompaniesLandingDataQuery__
 *
 * To run a query within a React component, call `useGetCompaniesLandingDataQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCompaniesLandingDataQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCompaniesLandingDataQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetCompaniesLandingDataQuery(baseOptions?: Apollo.QueryHookOptions<GetCompaniesLandingDataQuery, GetCompaniesLandingDataQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetCompaniesLandingDataQuery, GetCompaniesLandingDataQueryVariables>(GetCompaniesLandingDataDocument, options);
      }
export function useGetCompaniesLandingDataLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetCompaniesLandingDataQuery, GetCompaniesLandingDataQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetCompaniesLandingDataQuery, GetCompaniesLandingDataQueryVariables>(GetCompaniesLandingDataDocument, options);
        }
export type GetCompaniesLandingDataQueryHookResult = ReturnType<typeof useGetCompaniesLandingDataQuery>;
export type GetCompaniesLandingDataLazyQueryHookResult = ReturnType<typeof useGetCompaniesLandingDataLazyQuery>;
export type GetCompaniesLandingDataQueryResult = Apollo.QueryResult<GetCompaniesLandingDataQuery, GetCompaniesLandingDataQueryVariables>;
export const SearchSkillsDocument = gql`
    query SearchSkills($search: String!, $skill_type: [SkillTypeEnum]!) {
  skillSearch(search: $search, skill_type: $skill_type) {
    id
    skill_type
    name
    slug
    created_at
    updated_at
  }
}
    `;

/**
 * __useSearchSkillsQuery__
 *
 * To run a query within a React component, call `useSearchSkillsQuery` and pass it any options that fit your needs.
 * When your component renders, `useSearchSkillsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSearchSkillsQuery({
 *   variables: {
 *      search: // value for 'search'
 *      skill_type: // value for 'skill_type'
 *   },
 * });
 */
export function useSearchSkillsQuery(baseOptions: Apollo.QueryHookOptions<SearchSkillsQuery, SearchSkillsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<SearchSkillsQuery, SearchSkillsQueryVariables>(SearchSkillsDocument, options);
      }
export function useSearchSkillsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SearchSkillsQuery, SearchSkillsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<SearchSkillsQuery, SearchSkillsQueryVariables>(SearchSkillsDocument, options);
        }
export type SearchSkillsQueryHookResult = ReturnType<typeof useSearchSkillsQuery>;
export type SearchSkillsLazyQueryHookResult = ReturnType<typeof useSearchSkillsLazyQuery>;
export type SearchSkillsQueryResult = Apollo.QueryResult<SearchSkillsQuery, SearchSkillsQueryVariables>;
export const CancelCurrentTalentSubscriptionDocument = gql`
    mutation CancelCurrentTalentSubscription {
  cancelCurrentTalentSubscription {
    start_date
    current_period_end
    cancel_at_period_end
  }
}
    `;
export type CancelCurrentTalentSubscriptionMutationFn = Apollo.MutationFunction<CancelCurrentTalentSubscriptionMutation, CancelCurrentTalentSubscriptionMutationVariables>;

/**
 * __useCancelCurrentTalentSubscriptionMutation__
 *
 * To run a mutation, you first call `useCancelCurrentTalentSubscriptionMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCancelCurrentTalentSubscriptionMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [cancelCurrentTalentSubscriptionMutation, { data, loading, error }] = useCancelCurrentTalentSubscriptionMutation({
 *   variables: {
 *   },
 * });
 */
export function useCancelCurrentTalentSubscriptionMutation(baseOptions?: Apollo.MutationHookOptions<CancelCurrentTalentSubscriptionMutation, CancelCurrentTalentSubscriptionMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CancelCurrentTalentSubscriptionMutation, CancelCurrentTalentSubscriptionMutationVariables>(CancelCurrentTalentSubscriptionDocument, options);
      }
export type CancelCurrentTalentSubscriptionMutationHookResult = ReturnType<typeof useCancelCurrentTalentSubscriptionMutation>;
export type CancelCurrentTalentSubscriptionMutationResult = Apollo.MutationResult<CancelCurrentTalentSubscriptionMutation>;
export type CancelCurrentTalentSubscriptionMutationOptions = Apollo.BaseMutationOptions<CancelCurrentTalentSubscriptionMutation, CancelCurrentTalentSubscriptionMutationVariables>;
export const CheckTalentExistsByEmailDocument = gql`
    query CheckTalentExistsByEmail($talent_email: Email!) {
  checkTalentExistsByEmail(talent_email: $talent_email)
}
    `;

/**
 * __useCheckTalentExistsByEmailQuery__
 *
 * To run a query within a React component, call `useCheckTalentExistsByEmailQuery` and pass it any options that fit your needs.
 * When your component renders, `useCheckTalentExistsByEmailQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCheckTalentExistsByEmailQuery({
 *   variables: {
 *      talent_email: // value for 'talent_email'
 *   },
 * });
 */
export function useCheckTalentExistsByEmailQuery(baseOptions: Apollo.QueryHookOptions<CheckTalentExistsByEmailQuery, CheckTalentExistsByEmailQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CheckTalentExistsByEmailQuery, CheckTalentExistsByEmailQueryVariables>(CheckTalentExistsByEmailDocument, options);
      }
export function useCheckTalentExistsByEmailLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CheckTalentExistsByEmailQuery, CheckTalentExistsByEmailQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CheckTalentExistsByEmailQuery, CheckTalentExistsByEmailQueryVariables>(CheckTalentExistsByEmailDocument, options);
        }
export type CheckTalentExistsByEmailQueryHookResult = ReturnType<typeof useCheckTalentExistsByEmailQuery>;
export type CheckTalentExistsByEmailLazyQueryHookResult = ReturnType<typeof useCheckTalentExistsByEmailLazyQuery>;
export type CheckTalentExistsByEmailQueryResult = Apollo.QueryResult<CheckTalentExistsByEmailQuery, CheckTalentExistsByEmailQueryVariables>;
export const CreateTalentProfileDocument = gql`
    mutation CreateTalentProfile($email: Email!, $general_info: TalentGeneralInfoInput!, $tags_ids: [ID!], $is_ot_pool: Boolean, $job_application_data: JobApplicationInput) {
  createTalentProfile(
    email: $email
    job_application_data: $job_application_data
    general_info: $general_info
    tags_ids: $tags_ids
    is_ot_pool: $is_ot_pool
  )
}
    `;
export type CreateTalentProfileMutationFn = Apollo.MutationFunction<CreateTalentProfileMutation, CreateTalentProfileMutationVariables>;

/**
 * __useCreateTalentProfileMutation__
 *
 * To run a mutation, you first call `useCreateTalentProfileMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateTalentProfileMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createTalentProfileMutation, { data, loading, error }] = useCreateTalentProfileMutation({
 *   variables: {
 *      email: // value for 'email'
 *      general_info: // value for 'general_info'
 *      tags_ids: // value for 'tags_ids'
 *      is_ot_pool: // value for 'is_ot_pool'
 *      job_application_data: // value for 'job_application_data'
 *   },
 * });
 */
export function useCreateTalentProfileMutation(baseOptions?: Apollo.MutationHookOptions<CreateTalentProfileMutation, CreateTalentProfileMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateTalentProfileMutation, CreateTalentProfileMutationVariables>(CreateTalentProfileDocument, options);
      }
export type CreateTalentProfileMutationHookResult = ReturnType<typeof useCreateTalentProfileMutation>;
export type CreateTalentProfileMutationResult = Apollo.MutationResult<CreateTalentProfileMutation>;
export type CreateTalentProfileMutationOptions = Apollo.BaseMutationOptions<CreateTalentProfileMutation, CreateTalentProfileMutationVariables>;
export const CurrentTalentSubscriptionDocument = gql`
    query CurrentTalentSubscription {
  currentTalentSubscription {
    start_date
    current_period_end
    cancel_at_period_end
  }
}
    `;

/**
 * __useCurrentTalentSubscriptionQuery__
 *
 * To run a query within a React component, call `useCurrentTalentSubscriptionQuery` and pass it any options that fit your needs.
 * When your component renders, `useCurrentTalentSubscriptionQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCurrentTalentSubscriptionQuery({
 *   variables: {
 *   },
 * });
 */
export function useCurrentTalentSubscriptionQuery(baseOptions?: Apollo.QueryHookOptions<CurrentTalentSubscriptionQuery, CurrentTalentSubscriptionQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CurrentTalentSubscriptionQuery, CurrentTalentSubscriptionQueryVariables>(CurrentTalentSubscriptionDocument, options);
      }
export function useCurrentTalentSubscriptionLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CurrentTalentSubscriptionQuery, CurrentTalentSubscriptionQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CurrentTalentSubscriptionQuery, CurrentTalentSubscriptionQueryVariables>(CurrentTalentSubscriptionDocument, options);
        }
export type CurrentTalentSubscriptionQueryHookResult = ReturnType<typeof useCurrentTalentSubscriptionQuery>;
export type CurrentTalentSubscriptionLazyQueryHookResult = ReturnType<typeof useCurrentTalentSubscriptionLazyQuery>;
export type CurrentTalentSubscriptionQueryResult = Apollo.QueryResult<CurrentTalentSubscriptionQuery, CurrentTalentSubscriptionQueryVariables>;
export const GetPaymentLinkForTalentDocument = gql`
    query GetPaymentLinkForTalent($talent_id: ID) {
  getPaymentLinkForTalent(talent_id: $talent_id)
}
    `;

/**
 * __useGetPaymentLinkForTalentQuery__
 *
 * To run a query within a React component, call `useGetPaymentLinkForTalentQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPaymentLinkForTalentQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPaymentLinkForTalentQuery({
 *   variables: {
 *      talent_id: // value for 'talent_id'
 *   },
 * });
 */
export function useGetPaymentLinkForTalentQuery(baseOptions?: Apollo.QueryHookOptions<GetPaymentLinkForTalentQuery, GetPaymentLinkForTalentQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetPaymentLinkForTalentQuery, GetPaymentLinkForTalentQueryVariables>(GetPaymentLinkForTalentDocument, options);
      }
export function useGetPaymentLinkForTalentLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetPaymentLinkForTalentQuery, GetPaymentLinkForTalentQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetPaymentLinkForTalentQuery, GetPaymentLinkForTalentQueryVariables>(GetPaymentLinkForTalentDocument, options);
        }
export type GetPaymentLinkForTalentQueryHookResult = ReturnType<typeof useGetPaymentLinkForTalentQuery>;
export type GetPaymentLinkForTalentLazyQueryHookResult = ReturnType<typeof useGetPaymentLinkForTalentLazyQuery>;
export type GetPaymentLinkForTalentQueryResult = Apollo.QueryResult<GetPaymentLinkForTalentQuery, GetPaymentLinkForTalentQueryVariables>;
export const GetPaymentLinkForMatcherDocument = gql`
    query GetPaymentLinkForMatcher($talent_id: ID) {
  getPaymentLinkForMatcher(talent_id: $talent_id)
}
    `;

/**
 * __useGetPaymentLinkForMatcherQuery__
 *
 * To run a query within a React component, call `useGetPaymentLinkForMatcherQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPaymentLinkForMatcherQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetPaymentLinkForMatcherQuery({
 *   variables: {
 *      talent_id: // value for 'talent_id'
 *   },
 * });
 */
export function useGetPaymentLinkForMatcherQuery(baseOptions?: Apollo.QueryHookOptions<GetPaymentLinkForMatcherQuery, GetPaymentLinkForMatcherQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetPaymentLinkForMatcherQuery, GetPaymentLinkForMatcherQueryVariables>(GetPaymentLinkForMatcherDocument, options);
      }
export function useGetPaymentLinkForMatcherLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetPaymentLinkForMatcherQuery, GetPaymentLinkForMatcherQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetPaymentLinkForMatcherQuery, GetPaymentLinkForMatcherQueryVariables>(GetPaymentLinkForMatcherDocument, options);
        }
export type GetPaymentLinkForMatcherQueryHookResult = ReturnType<typeof useGetPaymentLinkForMatcherQuery>;
export type GetPaymentLinkForMatcherLazyQueryHookResult = ReturnType<typeof useGetPaymentLinkForMatcherLazyQuery>;
export type GetPaymentLinkForMatcherQueryResult = Apollo.QueryResult<GetPaymentLinkForMatcherQuery, GetPaymentLinkForMatcherQueryVariables>;
export const GetTypeformPaymentLinkCodeForMatcherDocument = gql`
    query GetTypeformPaymentLinkCodeForMatcher($talent_id: ID) {
  getTypeformPaymentLinkCodeForMatcher(talent_id: $talent_id)
}
    `;

/**
 * __useGetTypeformPaymentLinkCodeForMatcherQuery__
 *
 * To run a query within a React component, call `useGetTypeformPaymentLinkCodeForMatcherQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetTypeformPaymentLinkCodeForMatcherQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetTypeformPaymentLinkCodeForMatcherQuery({
 *   variables: {
 *      talent_id: // value for 'talent_id'
 *   },
 * });
 */
export function useGetTypeformPaymentLinkCodeForMatcherQuery(baseOptions?: Apollo.QueryHookOptions<GetTypeformPaymentLinkCodeForMatcherQuery, GetTypeformPaymentLinkCodeForMatcherQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetTypeformPaymentLinkCodeForMatcherQuery, GetTypeformPaymentLinkCodeForMatcherQueryVariables>(GetTypeformPaymentLinkCodeForMatcherDocument, options);
      }
export function useGetTypeformPaymentLinkCodeForMatcherLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetTypeformPaymentLinkCodeForMatcherQuery, GetTypeformPaymentLinkCodeForMatcherQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetTypeformPaymentLinkCodeForMatcherQuery, GetTypeformPaymentLinkCodeForMatcherQueryVariables>(GetTypeformPaymentLinkCodeForMatcherDocument, options);
        }
export type GetTypeformPaymentLinkCodeForMatcherQueryHookResult = ReturnType<typeof useGetTypeformPaymentLinkCodeForMatcherQuery>;
export type GetTypeformPaymentLinkCodeForMatcherLazyQueryHookResult = ReturnType<typeof useGetTypeformPaymentLinkCodeForMatcherLazyQuery>;
export type GetTypeformPaymentLinkCodeForMatcherQueryResult = Apollo.QueryResult<GetTypeformPaymentLinkCodeForMatcherQuery, GetTypeformPaymentLinkCodeForMatcherQueryVariables>;
export const NotifyTalentAboutJobMatchDocument = gql`
    mutation NotifyTalentAboutJobMatch($match_id: ID!) {
  notifyTalentAboutJobMatch(match_id: $match_id)
}
    `;
export type NotifyTalentAboutJobMatchMutationFn = Apollo.MutationFunction<NotifyTalentAboutJobMatchMutation, NotifyTalentAboutJobMatchMutationVariables>;

/**
 * __useNotifyTalentAboutJobMatchMutation__
 *
 * To run a mutation, you first call `useNotifyTalentAboutJobMatchMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useNotifyTalentAboutJobMatchMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [notifyTalentAboutJobMatchMutation, { data, loading, error }] = useNotifyTalentAboutJobMatchMutation({
 *   variables: {
 *      match_id: // value for 'match_id'
 *   },
 * });
 */
export function useNotifyTalentAboutJobMatchMutation(baseOptions?: Apollo.MutationHookOptions<NotifyTalentAboutJobMatchMutation, NotifyTalentAboutJobMatchMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<NotifyTalentAboutJobMatchMutation, NotifyTalentAboutJobMatchMutationVariables>(NotifyTalentAboutJobMatchDocument, options);
      }
export type NotifyTalentAboutJobMatchMutationHookResult = ReturnType<typeof useNotifyTalentAboutJobMatchMutation>;
export type NotifyTalentAboutJobMatchMutationResult = Apollo.MutationResult<NotifyTalentAboutJobMatchMutation>;
export type NotifyTalentAboutJobMatchMutationOptions = Apollo.BaseMutationOptions<NotifyTalentAboutJobMatchMutation, NotifyTalentAboutJobMatchMutationVariables>;
export const RenewCurrentTalentSubscriptionDocument = gql`
    mutation RenewCurrentTalentSubscription {
  renewCurrentTalentSubscription {
    start_date
    current_period_end
    cancel_at_period_end
  }
}
    `;
export type RenewCurrentTalentSubscriptionMutationFn = Apollo.MutationFunction<RenewCurrentTalentSubscriptionMutation, RenewCurrentTalentSubscriptionMutationVariables>;

/**
 * __useRenewCurrentTalentSubscriptionMutation__
 *
 * To run a mutation, you first call `useRenewCurrentTalentSubscriptionMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRenewCurrentTalentSubscriptionMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [renewCurrentTalentSubscriptionMutation, { data, loading, error }] = useRenewCurrentTalentSubscriptionMutation({
 *   variables: {
 *   },
 * });
 */
export function useRenewCurrentTalentSubscriptionMutation(baseOptions?: Apollo.MutationHookOptions<RenewCurrentTalentSubscriptionMutation, RenewCurrentTalentSubscriptionMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RenewCurrentTalentSubscriptionMutation, RenewCurrentTalentSubscriptionMutationVariables>(RenewCurrentTalentSubscriptionDocument, options);
      }
export type RenewCurrentTalentSubscriptionMutationHookResult = ReturnType<typeof useRenewCurrentTalentSubscriptionMutation>;
export type RenewCurrentTalentSubscriptionMutationResult = Apollo.MutationResult<RenewCurrentTalentSubscriptionMutation>;
export type RenewCurrentTalentSubscriptionMutationOptions = Apollo.BaseMutationOptions<RenewCurrentTalentSubscriptionMutation, RenewCurrentTalentSubscriptionMutationVariables>;
export const SearchTalentsDocument = gql`
    query SearchTalents($search: String, $category_ids: [ID], $skills_ids: [ID], $max_rate: Float, $min_rate: Float, $source_type: SourceTypeEnum, $is_active: Boolean, $is_verification_required: Boolean, $first: Int = 10, $page: Int, $available_now: Boolean, $skills_boolean_filter: TalentsBooleanSkillsFilterInput) {
  talentsSearch(
    search_string: $search
    category_ids: $category_ids
    skills_ids: $skills_ids
    max_rate: $max_rate
    min_rate: $min_rate
    source_type: $source_type
    is_active: $is_active
    first: $first
    available_now: $available_now
    page: $page
    is_verification_required: $is_verification_required
    skills_boolean_filter: $skills_boolean_filter
  ) {
    custom_paginator_info {
      current_page
      last_page
      per_page
      total
    }
    data {
      highlights {
        source
        text
      }
      talent {
        id
        reminded_at
        created_at
        is_invitation_accepted
        is_verification_required
        first_name
        last_name
        address
        rate
        recent_position_title
        email
        available_now
        location
        is_ot_pool
        source {
          id
        }
        avatar {
          avatar
        }
        talent_data {
          phone
        }
      }
    }
  }
}
    `;

/**
 * __useSearchTalentsQuery__
 *
 * To run a query within a React component, call `useSearchTalentsQuery` and pass it any options that fit your needs.
 * When your component renders, `useSearchTalentsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSearchTalentsQuery({
 *   variables: {
 *      search: // value for 'search'
 *      category_ids: // value for 'category_ids'
 *      skills_ids: // value for 'skills_ids'
 *      max_rate: // value for 'max_rate'
 *      min_rate: // value for 'min_rate'
 *      source_type: // value for 'source_type'
 *      is_active: // value for 'is_active'
 *      is_verification_required: // value for 'is_verification_required'
 *      first: // value for 'first'
 *      page: // value for 'page'
 *      available_now: // value for 'available_now'
 *      skills_boolean_filter: // value for 'skills_boolean_filter'
 *   },
 * });
 */
export function useSearchTalentsQuery(baseOptions?: Apollo.QueryHookOptions<SearchTalentsQuery, SearchTalentsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<SearchTalentsQuery, SearchTalentsQueryVariables>(SearchTalentsDocument, options);
      }
export function useSearchTalentsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SearchTalentsQuery, SearchTalentsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<SearchTalentsQuery, SearchTalentsQueryVariables>(SearchTalentsDocument, options);
        }
export type SearchTalentsQueryHookResult = ReturnType<typeof useSearchTalentsQuery>;
export type SearchTalentsLazyQueryHookResult = ReturnType<typeof useSearchTalentsLazyQuery>;
export type SearchTalentsQueryResult = Apollo.QueryResult<SearchTalentsQuery, SearchTalentsQueryVariables>;
export const SearchTalentsToSendMessageDocument = gql`
    query SearchTalentsToSendMessage($category_ids: [ID], $source_type: SourceTypeEnum = OWN, $first: Int = 1000, $page: Int = 1, $tags_ids: [ID]) {
  talentsSearch(
    category_ids: $category_ids
    source_type: $source_type
    first: $first
    page: $page
    tags_ids: $tags_ids
    is_active: true
    is_verification_required: false
  ) {
    data {
      talent {
        id
        avatar {
          avatar
        }
        stream_chat_id
        stream_chat_token
        first_name
        last_name
        recent_position_title
      }
    }
  }
}
    `;

/**
 * __useSearchTalentsToSendMessageQuery__
 *
 * To run a query within a React component, call `useSearchTalentsToSendMessageQuery` and pass it any options that fit your needs.
 * When your component renders, `useSearchTalentsToSendMessageQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSearchTalentsToSendMessageQuery({
 *   variables: {
 *      category_ids: // value for 'category_ids'
 *      source_type: // value for 'source_type'
 *      first: // value for 'first'
 *      page: // value for 'page'
 *      tags_ids: // value for 'tags_ids'
 *   },
 * });
 */
export function useSearchTalentsToSendMessageQuery(baseOptions?: Apollo.QueryHookOptions<SearchTalentsToSendMessageQuery, SearchTalentsToSendMessageQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<SearchTalentsToSendMessageQuery, SearchTalentsToSendMessageQueryVariables>(SearchTalentsToSendMessageDocument, options);
      }
export function useSearchTalentsToSendMessageLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SearchTalentsToSendMessageQuery, SearchTalentsToSendMessageQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<SearchTalentsToSendMessageQuery, SearchTalentsToSendMessageQueryVariables>(SearchTalentsToSendMessageDocument, options);
        }
export type SearchTalentsToSendMessageQueryHookResult = ReturnType<typeof useSearchTalentsToSendMessageQuery>;
export type SearchTalentsToSendMessageLazyQueryHookResult = ReturnType<typeof useSearchTalentsToSendMessageLazyQuery>;
export type SearchTalentsToSendMessageQueryResult = Apollo.QueryResult<SearchTalentsToSendMessageQuery, SearchTalentsToSendMessageQueryVariables>;
export const TalentCategoriesDocument = gql`
    query TalentCategories {
  talentCategories {
    id
    name
    slug
    created_at
    updated_at
    subcategories {
      id
      name
    }
  }
}
    `;

/**
 * __useTalentCategoriesQuery__
 *
 * To run a query within a React component, call `useTalentCategoriesQuery` and pass it any options that fit your needs.
 * When your component renders, `useTalentCategoriesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useTalentCategoriesQuery({
 *   variables: {
 *   },
 * });
 */
export function useTalentCategoriesQuery(baseOptions?: Apollo.QueryHookOptions<TalentCategoriesQuery, TalentCategoriesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<TalentCategoriesQuery, TalentCategoriesQueryVariables>(TalentCategoriesDocument, options);
      }
export function useTalentCategoriesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<TalentCategoriesQuery, TalentCategoriesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<TalentCategoriesQuery, TalentCategoriesQueryVariables>(TalentCategoriesDocument, options);
        }
export type TalentCategoriesQueryHookResult = ReturnType<typeof useTalentCategoriesQuery>;
export type TalentCategoriesLazyQueryHookResult = ReturnType<typeof useTalentCategoriesLazyQuery>;
export type TalentCategoriesQueryResult = Apollo.QueryResult<TalentCategoriesQuery, TalentCategoriesQueryVariables>;
export const TalentsCountByCategoriesDocument = gql`
    query TalentsCountByCategories($category_ids: [ID]) {
  talentsCountByCategories(category_ids: $category_ids) {
    talents_count
    name
    slug
  }
}
    `;

/**
 * __useTalentsCountByCategoriesQuery__
 *
 * To run a query within a React component, call `useTalentsCountByCategoriesQuery` and pass it any options that fit your needs.
 * When your component renders, `useTalentsCountByCategoriesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useTalentsCountByCategoriesQuery({
 *   variables: {
 *      category_ids: // value for 'category_ids'
 *   },
 * });
 */
export function useTalentsCountByCategoriesQuery(baseOptions?: Apollo.QueryHookOptions<TalentsCountByCategoriesQuery, TalentsCountByCategoriesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<TalentsCountByCategoriesQuery, TalentsCountByCategoriesQueryVariables>(TalentsCountByCategoriesDocument, options);
      }
export function useTalentsCountByCategoriesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<TalentsCountByCategoriesQuery, TalentsCountByCategoriesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<TalentsCountByCategoriesQuery, TalentsCountByCategoriesQueryVariables>(TalentsCountByCategoriesDocument, options);
        }
export type TalentsCountByCategoriesQueryHookResult = ReturnType<typeof useTalentsCountByCategoriesQuery>;
export type TalentsCountByCategoriesLazyQueryHookResult = ReturnType<typeof useTalentsCountByCategoriesLazyQuery>;
export type TalentsCountByCategoriesQueryResult = Apollo.QueryResult<TalentsCountByCategoriesQuery, TalentsCountByCategoriesQueryVariables>;
export const TalentsCountBySubcategoriesDocument = gql`
    query TalentsCountBySubcategories($subcategory_ids: [ID]) {
  talentsCountBySubcategories(subcategory_ids: $subcategory_ids) {
    talents_count
    name
    slug
  }
}
    `;

/**
 * __useTalentsCountBySubcategoriesQuery__
 *
 * To run a query within a React component, call `useTalentsCountBySubcategoriesQuery` and pass it any options that fit your needs.
 * When your component renders, `useTalentsCountBySubcategoriesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useTalentsCountBySubcategoriesQuery({
 *   variables: {
 *      subcategory_ids: // value for 'subcategory_ids'
 *   },
 * });
 */
export function useTalentsCountBySubcategoriesQuery(baseOptions?: Apollo.QueryHookOptions<TalentsCountBySubcategoriesQuery, TalentsCountBySubcategoriesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<TalentsCountBySubcategoriesQuery, TalentsCountBySubcategoriesQueryVariables>(TalentsCountBySubcategoriesDocument, options);
      }
export function useTalentsCountBySubcategoriesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<TalentsCountBySubcategoriesQuery, TalentsCountBySubcategoriesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<TalentsCountBySubcategoriesQuery, TalentsCountBySubcategoriesQueryVariables>(TalentsCountBySubcategoriesDocument, options);
        }
export type TalentsCountBySubcategoriesQueryHookResult = ReturnType<typeof useTalentsCountBySubcategoriesQuery>;
export type TalentsCountBySubcategoriesLazyQueryHookResult = ReturnType<typeof useTalentsCountBySubcategoriesLazyQuery>;
export type TalentsCountBySubcategoriesQueryResult = Apollo.QueryResult<TalentsCountBySubcategoriesQuery, TalentsCountBySubcategoriesQueryVariables>;