export const getPluralPostfix = (
  num: number,
  { postfix }: { postfix: string } = { postfix: 's' },
) => (num > 1 ? postfix : '');
