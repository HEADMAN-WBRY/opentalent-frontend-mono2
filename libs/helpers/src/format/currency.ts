export const formatCurrency = (num: number) =>
  `€${new Intl.NumberFormat('de-DE').format(num)}`;
