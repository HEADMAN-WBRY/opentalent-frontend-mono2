import { checkIsEmailCommon } from '../customMethods';

const TEST_CASES = [
  { input: 'test@gmail.com', output: true },
  { input: 'test@gmail1.com', output: false },
  { input: 'test@hotmail.com', output: true },
  { input: 'hotmail@test.com', output: false },
  { input: 'hotmail@hottmail.com', output: true },
];

describe('checkIsEmailCommon() works right', () => {
  it(`It works`, () => {
    TEST_CASES.forEach((test) => {
      expect(checkIsEmailCommon(test.input)).toBe(test.output);
    });
  });
});
