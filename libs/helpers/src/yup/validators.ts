import * as yup from 'yup';

const baseStringValidator = yup.string().trim().nullable();

export const maxStringValidator = baseStringValidator.max(255 / 2);
export const varCharStringValidator = baseStringValidator.max(128 / 2);
export const string64Validator = baseStringValidator.max(64 / 2);
