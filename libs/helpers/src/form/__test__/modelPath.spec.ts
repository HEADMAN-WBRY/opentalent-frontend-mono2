import { modelPath } from '../modelPath';

export interface SimpleUser {
  user: {
    id: number;
    name: string;
    avatar: {
      url: string;
      hash: string;
    };
    company?: {
      department: {
        license: number;
      };
    };
  };
}

describe('modelPath() works right', () => {
  it(`It works`, () => {
    expect(modelPath<SimpleUser>((s) => s.user.avatar.url)).toBe(
      'user.avatar.url',
    );
    expect(
      modelPath<SimpleUser>((s) => s.user.company?.department.license),
    ).toBe('user.company.department.license');
  });
});
