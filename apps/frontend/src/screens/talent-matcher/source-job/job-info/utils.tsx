import { format, formatDuration, intervalToDuration, parseISO } from 'date-fns';
import { formatName } from 'utils/talent';

import { Grid } from '@mui/material';

import { Job, Skill } from '@libs/graphql-types';
import { isNil } from '@libs/helpers/common';
import { formatCurrency } from '@libs/helpers/format';
import { SkillChip } from '@libs/ui/components/chip';
import { JOB_TYPE_MAP } from '@libs/ui/components/job/utils';
import { formatRate, getJobCapacity } from '@libs/ui/components/job/utils';

import { DataGridItem } from './types';

const getDuration = (job?: Required<Job>) => {
  if (!job?.start_date || !job?.end_date) {
    return '';
  }

  const duration = intervalToDuration({
    start: parseISO(job.start_date),
    end: parseISO(job.end_date),
  });

  return formatDuration(duration, {
    format: ['years', 'months', 'days', 'hours'],
  });
};

export const mapJobToDataGridItems = (job?: Required<Job>): DataGridItem[] => [
  {
    title: 'Client',
    icon: 'client',
    value: job?.client || '-',
  },
  {
    icon: 'location',
    title: 'Country',
    value: job?.country || '-',
  },
  {
    icon: 'location',
    title: 'City',
    value: job?.city || '-',
  },

  {
    title: 'Type',
    value: JOB_TYPE_MAP[job?.location_type as keyof typeof JOB_TYPE_MAP] || '-',
    icon: 'calendar',
  },
  {
    icon: 'euro',
    title: 'Hourly rate range',
    value: formatRate({
      min: job?.rate_min,
      max: job?.rate_max,
      period: 'hour',
      isNegotiable: job?.is_rate_negotiable,
    }),
  },
  {
    icon: 'speak',
    title: "Finder's fee",
    value: !isNil(job?.finders_fee)
      ? formatCurrency(job?.finders_fee || 0)
      : '-',
  },
  {
    icon: 'period',
    title: 'Starting date',
    value: job?.posted_at
      ? format(parseISO(job.start_date), 'MMM d, yyyy')
      : '',
  },
  {
    icon: 'period',
    title: 'Project duration',
    value: getDuration(job),
  },
  {
    title: 'Required Capacity',
    value: getJobCapacity(job as Job),
    icon: 'timelapse',
  },
  {
    title: 'Campaign owner',
    icon: 'owner',
    value:
      formatName({
        firstName: job?.campaign_owner?.first_name,
        lastName: job?.campaign_owner?.last_name,
      }) || '-',
  },
  {
    icon: 'book',
    title: 'Key skills',
    value: (
      <Grid container spacing={2}>
        {job?.skills?.map((skill) => (
          <Grid key={skill?.id} item>
            <SkillChip
              style={{
                maxWidth: 240,
                whiteSpace: 'nowrap',
                textOverflow: 'ellipsis',
              }}
              size="small"
              skill={skill as Skill}
            />
          </Grid>
        ))}
      </Grid>
    ),
  },
];
