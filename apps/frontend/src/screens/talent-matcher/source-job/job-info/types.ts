import { ICONS_MAP } from './consts';

export interface DataGridItem {
  title: string;
  icon: keyof typeof ICONS_MAP;
  value: React.ReactNode;
}
