import { gql } from '@apollo/client';
import FULL_JOB_FRAGMENT from 'graphql/fragments/talent/talentJobFragment';

export const GET_JOB_INFO = gql`
  ${FULL_JOB_FRAGMENT}
  query GetJob($id: ID!) {
    job(id: $id) {
      ...FullJob
      stream_chat_subject_id
      job_matcher_applications {
        id
        matcher {
          id
        }
        status
      }
    }
    #    currentTalentJobApplicationByJobId(job_id: $id) {
    #      id
    #      pitch
    #      rate
    #    }
  }
`;
