import { PopupButton } from '@typeform/embed-react';
import { useCurrentUser } from 'hooks/auth';
import React from 'react';
import { formatName } from 'utils/talent';

import { Box } from '@mui/material';

import { Job } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

interface PresentCandidateProps {
  job: Job;
}

const PresentCandidate = ({ job }: PresentCandidateProps) => {
  const { getData } = useCurrentUser();
  const campaignOwnerName = formatName({
    firstName: job?.campaign_owner?.first_name,
    lastName: job?.campaign_owner?.last_name,
  });
  const matcherData = getData();
  const matcherName = formatName({
    firstName: matcherData.user?.first_name,
    lastName: matcherData.user?.last_name,
  });
  const matcherEmail = matcherData?.user?.email;
  const jobName = job.name;

  return (
    <Box maxWidth={630} margin="0 auto">
      <Box mb={4}>
        <Typography variant="h5" textAlign="center">
          Present candidate
        </Typography>
      </Box>
      <Typography textAlign="center" color="tertiary">
        Talent Matchers program
      </Typography>

      <Box mt={6} mb={4}>
        <Typography color="text.secondary">
          Congratulations! 🎉 Your application for sourcing candidates for this
          role has been accepted. Please submit suitable candidates using the
          link provided, and the assigned talent partner will contact you for
          the next steps.
        </Typography>
      </Box>

      <Box display="flex" justifyContent="center">
        <PopupButton
          buttonProps={{ variant: 'contained' }}
          id="bbSWq8H9"
          as={Button as any}
          hidden={{
            campaign_owner_name: campaignOwnerName,
            talent_matcher_name: matcherName,
            talent_matcher_email: matcherEmail,
            job_name: jobName,
          }}
        >
          Present a candidate
        </PopupButton>
      </Box>
    </Box>
  );
};

export default PresentCandidate;
