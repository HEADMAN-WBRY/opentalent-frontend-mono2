import { PopupButton } from '@typeform/embed-react';
import { useCurrentUser } from 'hooks/auth';
import React from 'react';
import { formatName } from 'utils/talent';

import { Job } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';

interface PresentCandidateProps {
  job: Job;
}

const PresentCandidateButton = ({ job }: PresentCandidateProps) => {
  const { getData } = useCurrentUser();
  const campaignOwnerName = formatName({
    firstName: job?.campaign_owner?.first_name,
    lastName: job?.campaign_owner?.last_name,
  });
  const matcherData = getData();
  const matcherName = formatName({
    firstName: matcherData.user?.first_name,
    lastName: matcherData.user?.last_name,
  });
  const matcherEmail = matcherData?.user?.email;
  const jobName = job.name;

  return (
    <PopupButton
      buttonProps={{ variant: 'contained', color: 'info' }}
      id="bbSWq8H9"
      as={Button as any}
      hidden={{
        campaign_owner_name: campaignOwnerName,
        talent_matcher_name: matcherName,
        talent_matcher_email: matcherEmail,
        job_name: jobName,
      }}
    >
      Present a candidate
    </PopupButton>
  );
};

export default PresentCandidateButton;
