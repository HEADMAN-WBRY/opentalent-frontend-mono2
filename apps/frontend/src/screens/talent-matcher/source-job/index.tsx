import React from 'react';

const SourceJob = React.lazy(() => import('./SourceJob'));

export default SourceJob;
