import { useCurrentTime } from 'hooks/common/useCurrentTime';
import React from 'react';
import { isItExternalJob } from 'screens/talent/job-board-v2/content/Content';
import ExternalJobCardV2 from 'screens/talent/job-board-v2/content/external-job-card-v2';
import JobCard from 'screens/talent/job-board-v2/content/job-card';
import { useCheckPagination } from 'screens/talent/job-board-v2/hooks';

import { Pagination } from '@mui/lab';
import { Box } from '@mui/material';

import { Job } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import ApplyModal from '../apply-modal';
import { useJobList, useJobModals, usePaginationChange } from '../hooks';
import InviteModal from '../invite-modal';
import { ViewTypes } from '../types';
import Controls from './Controls';
import MatcherJobCard from './matcher-job-card';

interface ContentProps {
  openDrawer: VoidFunction;
  currentView: ViewTypes;
  applicationsCount?: number;
  savedCount?: number;
  recommendedCount?: number;
  refetchJobCounts: VoidFunction;
  openCount?: number;
  externalCount?: number;
}

const Content = ({
  openDrawer,
  currentView,
  applicationsCount,
  savedCount,
  recommendedCount,
  refetchJobCounts,
  openCount,
  externalCount,
}: ContentProps) => {
  const currentTime = useCurrentTime();
  const onPaginationChange = usePaginationChange();
  const { jobs, loadingJobList, currentPage, lastPage, loadJobs } =
    useJobList(currentView);
  const { inviteJob, applyJob, handleClose, onJobApply, onInvite } =
    useJobModals();
  const CardComponent =
    currentView === ViewTypes.Matcher ? MatcherJobCard : JobCard;

  useCheckPagination({ lastPage, currentPage });

  return (
    <div>
      <Controls
        currentView={currentView}
        openDrawer={openDrawer}
        applicationsCount={applicationsCount}
        savedCount={savedCount}
        recommendedCount={recommendedCount}
        openCount={openCount}
        externalCount={externalCount}
      />
      <div>
        {loadingJobList && (
          <Typography paragraph variant="body1">
            Loading results, please wait...
          </Typography>
        )}
        {!jobs.length && !loadingJobList && (
          <Typography variant="body1">
            No jobs matching your criteria. Try to change search options.
          </Typography>
        )}
        {jobs.map((job, index) => {
          if (isItExternalJob(job)) {
            return (
              <ExternalJobCardV2
                currentTime={currentTime}
                job={job}
                key={job.id}
                onJobSave={refetchJobCounts}
              />
            );
          }
          return (
            <CardComponent
              currentView={currentView}
              // eslint-disable-next-line react/no-array-index-key
              key={`${job?.posted_at}-${job?.name}-${index}`}
              onInvite={onInvite}
              onJobApply={onJobApply}
              job={job as Job}
              currentTime={currentTime}
              loadJobs={loadJobs}
              onJobSave={refetchJobCounts}
            />
          );
        })}
      </div>
      {!loadingJobList && (
        <Box display="flex" justifyContent="center" mt={4}>
          <Pagination
            page={currentPage}
            showFirstButton
            showLastButton
            count={lastPage}
            variant="outlined"
            shape="rounded"
            onChange={onPaginationChange}
          />
        </Box>
      )}
      {inviteJob && <InviteModal handleClose={handleClose} job={inviteJob} />}
      {applyJob && <ApplyModal handleClose={handleClose} job={applyJob} />}
    </div>
  );
};

export default React.memo(Content);
