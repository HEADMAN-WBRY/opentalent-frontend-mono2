import { JobOrderByColumn } from '@libs/graphql-types';
import { OptionType } from '@libs/ui/components/form/select';

export { MatcherLayoutViewTypes as ViewTypes } from 'components/layout/header/matchers';

export interface FilterForm {
  externalJobEmployment: OptionType[];
  externalJobSeniority: OptionType[];
  country: OptionType;
  externalCompanyName: OptionType;
  tab: JobTabType;

  categories: OptionType[];
  search: string;
  skills: OptionType[];
  rate_min: string;
  rate_max: string;
  order_by_column?: JobOrderByColumn;
  is_saved: boolean;
  page: string;
  is_active: boolean;

  jobSourceInternal: boolean;
  jobSourceExternal: boolean;
}

export enum JobTabType {
  Open = 'open',
  MyApplications = 'myApplications',
  Closed = 'closed',
  Saved = 'saved',
  Recommended = 'recommended',
}
