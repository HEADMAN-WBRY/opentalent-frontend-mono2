import React from 'react';

const JobBoardV2 = React.lazy(() => import('./TalentMatcherJobBoard'));

export default JobBoardV2;
