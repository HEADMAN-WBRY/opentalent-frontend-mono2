import { ConnectedPageLayout } from 'components/layout/page-layout';
import { Formik } from 'formik';
import { useCurrentUser } from 'hooks/auth';
import React from 'react';

import Box from '@mui/material/Box/Box';
import Grid from '@mui/material/Grid';

import { useGetCurrentUserCompanyTagsQuery } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';
import { ConnectedMultipleSelect } from '@libs/ui/components/form/multiple-select';
import { OptionType } from '@libs/ui/components/form/select';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import StepSection from '@libs/ui/components/step-section';
import Typography from '@libs/ui/components/typography';

import { AbnBottomText } from './AbnBottomText';
import { INITIAL_VALUES } from './const';
import EmailControl from './email-control';
import { useSubmitAction } from './hooks';
import useStyles from './styles';
import validator from './validator';

interface CreateProfileProps {}

const CreateProfile = (props: CreateProfileProps) => {
  const classes = useStyles(props);
  const { onSubmit, loading } = useSubmitAction();
  const { getData } = useCurrentUser();
  const { data } = useGetCurrentUserCompanyTagsQuery();
  const tagOptions = (data?.currentUserCompanyTags || []).map((i) => ({
    text: i?.name,
    value: i?.id,
  }));
  const companyName = getData()?.data?.currentCompanyUser?.company?.name;
  const link = (getData()?.data?.currentCompanyUser?.company as any)
    ?.sign_up_landing_link;
  const title = companyName
    ? `Invite people to the ${companyName} Community`
    : 'Invite people to the Community';

  return (
    <ConnectedPageLayout documentTitle="Invite new candidate" drawerProps={{}}>
      <Formik
        onSubmit={onSubmit}
        validationSchema={validator}
        initialValues={INITIAL_VALUES}
        validateOnBlur={false}
        validateOnChange
      >
        {({ handleSubmit }) => {
          return (
            <Box className={classes.container}>
              <Typography variant="h5">{title}</Typography>
              <Typography variant="subtitle1">
                So you can post jobs across your talent pool and hire people
                directly, thereby bypassing recruiter fees.
              </Typography>
              <Box className={classes.form}>
                <StepSection index={1} title="Name and email">
                  <Grid spacing={4} direction="column" container>
                    <Grid wrap="wrap" spacing={4} container item>
                      <Grid xs={12} sm={6} item>
                        <ConnectedTextField
                          name="first_name"
                          fullWidth
                          variant="filled"
                          label="First name"
                        />
                      </Grid>
                      <Grid xs={12} sm={6} item>
                        <ConnectedTextField
                          name="last_name"
                          fullWidth
                          variant="filled"
                          label="Last name"
                        />
                      </Grid>
                    </Grid>
                    <Grid item>
                      <EmailControl
                        fullWidth
                        variant="filled"
                        label="Candidate’s email"
                        name="email"
                        withLoadingIndicator
                      />
                    </Grid>
                  </Grid>
                </StepSection>
                {!!tagOptions.length && (
                  <StepSection index={2} title="Add tags for internal use">
                    <Grid spacing={4} direction="column" container>
                      <Grid item>
                        <ConnectedMultipleSelect
                          fullWidth
                          name="tags"
                          variant="filled"
                          options={tagOptions}
                          label="Pick from list"
                          autoCompleteProps={{
                            filterSelectedOptions: true,
                            isOptionEqualToValue: (
                              opt: OptionType,
                              value: OptionType,
                            ) => opt.value === value.value,
                          }}
                          chipProps={{
                            size: 'small',
                          }}
                          inputProps={{
                            variant: 'filled',
                            label: 'Pick from list',
                            margin: 'dense',
                          }}
                        />
                      </Grid>
                    </Grid>
                  </StepSection>
                )}
                {/* <StepSection index={2} title="General information">
                  <Grid spacing={4} direction="column" container>
                    <Grid item>
                      <ConnectedGraphSelect
                        query={TALENT_CATEGORIES}
                        dataPath="talentCategories"
                        dataMap={{ text: 'name', value: 'id' }}
                        fullWidth
                        name="talent_category_id"
                        variant="filled"
                        label="Category"
                      />
                    </Grid>
                     <Grid item>
                      <ConnectedTextField
                        fullWidth
                        name="recent_position_title"
                        variant="filled"
                        label="Title / role"
                      />
                    </Grid>
                    <Grid item>
                      <ConnectedSelect
                        name="location"
                        fullWidth
                        variant="filled"
                        label="Country"
                        options={COUNTRY_OPTIONS}
                      />
                    </Grid>
                  </Grid>
                </StepSection> */}
                <Box mb={6}>
                  <Button
                    onClick={() => handleSubmit()}
                    variant="contained"
                    color="secondary"
                    disabled={loading}
                    type="submit"
                  >
                    Send invite
                  </Button>
                </Box>

                {link && (
                  <Box>
                    <AbnBottomText link={link} />
                  </Box>
                )}
              </Box>
            </Box>
          );
        }}
      </Formik>
    </ConnectedPageLayout>
  );
};

export default CreateProfile;
