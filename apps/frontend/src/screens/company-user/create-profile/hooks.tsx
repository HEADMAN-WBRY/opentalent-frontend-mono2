import { useSnackbar } from 'notistack';
import { useCallback } from 'react';

import { useCreateTalentInvitationMutation } from '@libs/graphql-types';
import { FormikSubmit } from '@libs/helpers/form';

import { INITIAL_VALUES } from './const';

export const useSubmitAction = () => {
  const { enqueueSnackbar } = useSnackbar();
  const [invite, { loading: isInviting }] = useCreateTalentInvitationMutation();
  const onSubmit: FormikSubmit<typeof INITIAL_VALUES> = useCallback(
    async ({ email, is_email_valid, tags, ...general_info }, formHelpers) => {
      try {
        await invite({
          variables: {
            first_name: general_info.first_name,
            last_name: general_info.last_name,
            email,
            tags_ids: tags.map((i) => i.value! as string),
          },
        });
        enqueueSnackbar('Your invite was sent.', { variant: 'success' });
        formHelpers.resetForm();
      } catch (e) {
        // eslint-disable-next-line no-console
        console.error(e);
      }
    },
    [enqueueSnackbar, invite],
  );

  return { onSubmit, loading: isInviting };
};
