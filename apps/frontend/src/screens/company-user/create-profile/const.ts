import { OptionType } from '@libs/ui/components/form/select';

export const INITIAL_VALUES = {
  first_name: '',
  last_name: '',
  address: '',
  recent_position_title: '',
  talent_category_id: '',
  email: '',
  location: '',
  is_email_valid: false,
  tags: [] as OptionType[],
};
