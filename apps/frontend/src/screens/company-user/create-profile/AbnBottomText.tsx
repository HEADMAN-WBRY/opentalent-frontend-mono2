import { useSnackbar } from 'notistack';
import React from 'react';
import { useCopyToClipboard } from 'react-use';

import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import { Grid, IconButton } from '@mui/material';

import Typography, { OuterLink } from '@libs/ui/components/typography';

interface AbnBottomTextProps {
  link: string;
}

export const AbnBottomText = ({ link }: AbnBottomTextProps) => {
  const [, copy] = useCopyToClipboard();
  const { enqueueSnackbar } = useSnackbar();
  const onCopy = () => {
    enqueueSnackbar('Copied', { variant: 'success' });
    copy(link);
  };

  return (
    <Grid container spacing={2} wrap="nowrap">
      <Grid
        item
        style={{
          display: 'flex',
          alignItems: 'center',
        }}
      >
        <Typography>
          You can also send{' '}
          <OuterLink
            href={link}
            target="_blank"
            color="info.main"
            style={{
              textDecoration: 'underline',
            }}
          >
            this link to your company’s signup page
          </OuterLink>
          .
        </Typography>
      </Grid>

      <Grid item>
        <IconButton color="info" onClick={onCopy} size="small">
          <ContentCopyIcon />
        </IconButton>
      </Grid>
    </Grid>
  );
};
