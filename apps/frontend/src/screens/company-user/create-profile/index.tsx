import React from 'react';

const CreateProfile = React.lazy(() => import('./CreateProfile'));

export default CreateProfile;
