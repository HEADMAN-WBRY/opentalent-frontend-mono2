import { ReactComponent as CheckIcon } from 'assets/icons/check.svg';
import { TalentActions } from 'components/custom/talent/actions-menu';
import { DEFAULT_AVATAR } from 'consts/common';
import useMediaQueries from 'hooks/common/useMediaQueries';
import { useTalentName } from 'hooks/talents';
import { useIsCurrentTalentVerified } from 'hooks/talents/useTalentAccountType';
import React from 'react';
import { Link } from 'react-router-dom';
import { pathManager } from 'routes';
import { stopEvent } from 'utils/common';

import { Avatar, Box, Grid, Paper, Badge, Tooltip } from '@mui/material';

import { Talent, TalentSearchResult } from '@libs/graphql-types';
import { formatCurrency } from '@libs/helpers/format';
import Chip from '@libs/ui/components/chip/Chip';
import Typography from '@libs/ui/components/typography';

import CategoriesList from './CategoriesList';
import Highlights from './Highlights';
import RemindText from './RemindText';
import useStyles from './styles';

export interface TalentCardProps {
  talentSearch: TalentSearchResult;
  refetch: () => void;
}

const POSITION_TITLE_LIMIT = 30;

const TalentCard = (props: TalentCardProps) => {
  const { talentSearch } = props;
  const talent = talentSearch?.talent as Talent;
  const highlights = talentSearch?.highlights || [];
  const { isXS } = useMediaQueries();
  const classes = useStyles(props);
  const name = useTalentName(talent);
  const isTalentVerified = !!useIsCurrentTalentVerified();

  const status = talent?.is_invitation_accepted ? (
    <CheckIcon className={classes.successCheck} />
  ) : (
    <Chip label="pending activation" color="grey" size="small" />
  );

  const positionTitle =
    (talent?.recent_position_title?.length || 0) > POSITION_TITLE_LIMIT
      ? `${talent?.recent_position_title?.slice(0, POSITION_TITLE_LIMIT)}...`
      : talent?.recent_position_title;

  return (
    <Link
      to={pathManager.company.talentProfile.generatePath({
        id: talent?.id || '',
      })}
    >
      <Paper data-test-id="talent-card" className={classes.paper} elevation={0}>
        <Grid container wrap="nowrap">
          <Grid item className={classes.avatarWrapper}>
            <Badge
              overlap="circular"
              classes={{ badge: classes.avatarBadge }}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              variant="standard"
              badgeContent={
                <Box>
                  <Tooltip
                    placement="right-end"
                    open={talent?.available_now ? undefined : false}
                    title="Available now - let’s work together!"
                  >
                    <Box width={22} height={22} />
                  </Tooltip>
                </Box>
              }
            >
              <Avatar
                src={talent?.avatar?.avatar || DEFAULT_AVATAR}
                className={classes.avatar}
                imgProps={{ loading: 'lazy' }}
                alt={name}
              />
            </Badge>
          </Grid>
          <Grid className={classes.contentWrapper} item>
            <Grid container direction="column">
              <Grid
                spacing={2}
                container
                wrap={isXS ? 'wrap' : 'nowrap'}
                classes={{
                  root: classes.firstRow,
                }}
              >
                <Grid item className={classes.nameAndVerification}>
                  <Typography variant="h6" className={classes.nameTitle}>
                    {name}
                  </Typography>
                  {!isTalentVerified && (
                    <Typography
                      variant="subtitle2"
                      className={classes.pendingVerification}
                    >
                      pending verification
                    </Typography>
                  )}
                  {talent?.is_matcher && (
                    <Box ml={2}>
                      <Chip
                        size="small"
                        label="Talent Matcher"
                        className={classes.recruiterChip}
                      />
                    </Box>
                  )}
                </Grid>
                <Grid className={classes.actionsBlock} item>
                  <Box onClick={stopEvent} className={classes.inviteButton}>
                    <TalentActions talent={talent} refetch={props.refetch} />
                  </Box>
                  {!talent?.is_invitation_accepted && (
                    <Box pt={2}>
                      <RemindText talent={talent} />
                    </Box>
                  )}
                </Grid>
              </Grid>
              <Grid container alignItems="center" className={classes.secondRow}>
                <Grid xs={6} item>
                  <Typography variant="body2">{positionTitle}</Typography>
                </Grid>
                <Grid item>
                  <Typography color="textSecondary" variant="body2">
                    {talent?.location}
                  </Typography>
                </Grid>
              </Grid>
              <Grid container alignItems="center">
                <Box display="flex" mr={isXS ? 'auto' : 6}>
                  <Typography
                    className={classes.hRate}
                    color="textSecondary"
                    variant="subtitle2"
                  >
                    Hourly Rate:
                  </Typography>
                  <Typography color="textSecondary" variant="body2">
                    {formatCurrency(talent?.rate || 0)}
                  </Typography>
                </Box>
                <Grid item>{status}</Grid>
              </Grid>
            </Grid>

            <Box mt={2}>
              <CategoriesList talent={talent} />
            </Box>
          </Grid>
        </Grid>
        <Box pt={2}>
          <Highlights items={highlights} />
        </Box>
      </Paper>
    </Link>
  );
};

export default TalentCard;
