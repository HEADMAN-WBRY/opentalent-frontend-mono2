import React from 'react';

import { Grid } from '@mui/material';

import { Subcategory, Talent } from '@libs/graphql-types';
import Chip from '@libs/ui/components/chip';
import Typography from '@libs/ui/components/typography';

interface CategoriesListProps {
  talent: Talent;
}

const CategoriesList = ({ talent }: CategoriesListProps) => {
  const category = talent?.category?.name;
  const subcategories = (talent?.subcategories as Subcategory[]) || [];

  if (!category) {
    return <></>;
  }

  return (
    <div>
      <Typography variant="subtitle2">Category</Typography>
      <Grid spacing={2} container>
        <Grid item>
          <Typography color="text.secondary" variant="body2">
            {category}
          </Typography>
        </Grid>

        {subcategories.map((i) => (
          <Grid key={i.id} item>
            <Chip color="grey" size="small" label={i.name} />
          </Grid>
        ))}
      </Grid>
    </div>
  );
};

export default CategoriesList;
