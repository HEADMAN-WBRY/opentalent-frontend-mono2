import React from 'react';

import { FormControlLabel, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { ConnectedSwitch } from '@libs/ui/components/form/switch';
import Typography from '@libs/ui/components/typography';

export interface NotificationControlProps {
  title: string;
  subtitle: string;
  path: string;
}

const useStyles = makeStyles((theme) => ({
  label: {
    margin: 0,
  },
}));

export const NotificationControl = ({
  title,
  subtitle,
  path,
}: NotificationControlProps) => {
  const classes = useStyles();

  return (
    <div>
      <FormControlLabel
        labelPlacement="start"
        classes={{ root: classes.label }}
        control={<ConnectedSwitch name={path} label="" color="info" />}
        label={
          <Grid container>
            <Grid item>
              <Typography variant="subtitle1">{title}</Typography>
            </Grid>
            <Grid item>
              <Typography variant="body2" color="textSecondary">
                {subtitle}
              </Typography>
            </Grid>
          </Grid>
        }
      />
    </div>
  );
};
