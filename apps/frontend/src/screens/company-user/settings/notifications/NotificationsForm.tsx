import { Formik } from 'formik';
import React from 'react';

import { Box, Grid } from '@mui/material';

import {
  NotificationControl,
  NotificationControlProps,
} from './NotificationControl';
import validator from './validator';

interface CompanyFormProps {
  onSubmit: (data: any) => void;
  initialValues: any;
  loading?: boolean;
  enableReinitialize?: boolean;
}

const NOTIFICATION_ITEMS: NotificationControlProps[] = [
  {
    title: 'Company and product updates',
    subtitle: "We'll keep you in the loop about what's going at OpenTalent.",
    path: 'companyNotifications',
  },
  {
    title: 'Job invitation responses ',
    subtitle:
      'When talents accept or decline your invitations to apply for a job',
    path: 'companyNotifications',
  },
  {
    title: 'Messages (chat)',
    subtitle: 'Receive direct messages from companies / hiring managers.',
    path: 'companyNotifications',
  },
  {
    title: 'Talents joining your community',
    subtitle: 'Receive notifications when someone needs to be verified.',
    path: 'companyNotifications',
  },
  {
    title: 'Talent proposal notification',
    subtitle: 'Receive notifications when talent matchers suggest candidates',
    path: 'companyNotifications',
  },
];

export const NotificationsForm = ({
  onSubmit,
  initialValues,
  loading,
  enableReinitialize = false,
}: CompanyFormProps) => {
  return (
    <Box>
      <Formik
        onSubmit={(values) => onSubmit(values)}
        validationSchema={validator}
        initialValues={initialValues}
        validateOnBlur={false}
        validateOnChange
        enableReinitialize={enableReinitialize}
      >
        {({ handleSubmit }) => (
          <Grid spacing={4} direction="column" container wrap="nowrap">
            {NOTIFICATION_ITEMS.map((item) => (
              <Grid item key={item.path} mb={2}>
                <NotificationControl {...item} />
              </Grid>
            ))}
          </Grid>
        )}
      </Formik>
    </Box>
  );
};
