import React from 'react';

import { Box } from '@mui/material';

import Typography from '@libs/ui/components/typography';

import { NotificationsForm } from './NotificationsForm';
import { useInitialValues, useUpdateUser } from './hooks';

interface ConnectedCompanyFormProps {
  submitText?: string;
  onSuccess?: VoidFunction;
  isCreate?: boolean;
}

const ConnectedNotificationsForm = ({
  onSuccess,
}: ConnectedCompanyFormProps) => {
  const { initialValues } = useInitialValues();
  const { loading, updateRequest } = useUpdateUser({ onSuccess });

  return (
    <Box maxWidth="577px">
      <Box mb={2}>
        <Typography variant="h6" fontWeight={500}>
          What Notifications do you want to receive?
        </Typography>
      </Box>
      <Typography variant="subtitle1" color="textSecondary">
        Only get notified about what’s important to you
      </Typography>

      <Box mt={8}>
        <NotificationsForm
          onSubmit={updateRequest}
          loading={loading}
          initialValues={initialValues}
        />
      </Box>
    </Box>
  );
};

export default ConnectedNotificationsForm;
