import { useMutation } from '@apollo/client';
import {
  GET_CURRENT_COMPANY_USER,
  UPDATE_COMPANY,
  CREATE_COMPANY_FOR_CURRENT_USER,
} from 'graphql/user';
import { useCurrentUser } from 'hooks/auth';
import { useSnackbar } from 'notistack';
import { noop } from 'utils/common';

import { Mutation, MutationUpdateCompanyArgs } from '@libs/graphql-types';

export const useInitialValues = () => {
  const { data: userData } = useCurrentUser();
  const companyUser = userData?.currentCompanyUser;
  const initialValues: MutationUpdateCompanyArgs = {
    company_id: companyUser?.company?.id || '',
    name: companyUser?.company?.name || '',
    logo: companyUser?.company?.logo || '',
    website: companyUser?.company?.website || '',
    type_of_activity: companyUser?.company?.type_of_activity || '',
    country: companyUser?.company?.country || '',
    about: companyUser?.company?.about || '',
  };

  return {
    initialValues,
    initialAvatar: companyUser?.company?.logo,
  };
};

export const useUpdateUser = ({
  onSuccess = noop,
}: {
  onSuccess?: VoidFunction;
}) => {
  const { data } = useCurrentUser();
  const isCreate = !data?.currentCompanyUser?.company;
  const { enqueueSnackbar } = useSnackbar();
  const [request, { loading }] = useMutation<
    Mutation,
    MutationUpdateCompanyArgs
  >(isCreate ? CREATE_COMPANY_FOR_CURRENT_USER : UPDATE_COMPANY, {
    onCompleted: () => {
      enqueueSnackbar(isCreate ? 'Created' : 'Updated', { variant: 'success' });
      onSuccess();
    },
    refetchQueries: [{ query: GET_CURRENT_COMPANY_USER }],
  });
  const updateRequest = (variables: MutationUpdateCompanyArgs) => {
    request({ variables });
  };

  return { updateRequest, loading };
};
