import { CheckThickListItem } from 'components/check-thick-list';
import React from 'react';

import OpenInNewOutlinedIcon from '@mui/icons-material/OpenInNewOutlined';
import { Box, Chip, Grid } from '@mui/material';
import { grey } from '@mui/material/colors';
import { makeStyles } from '@mui/styles';

import Button, { RouterButton } from '@libs/ui/components/button';
import Typography, { OuterLink } from '@libs/ui/components/typography';

import { DIRECT_FEATURES } from '../consts';
import { CANCEL_MODAL } from '../consts';

interface PremiumPlanCardProps {
  showRenewButton?: boolean;
  showCancelButton?: boolean;
  isRenewing: boolean;
  renew: VoidFunction;
}

const useStyles = makeStyles((theme) => ({
  card: {
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    borderRadius: 16,
    padding: theme.spacing(8, 6),
    maxWidth: 400,
  },
  wrapper: {
    width: 360,
  },
  textBlock: {
    backgroundColor: 'white',
    padding: '6px 10px',
    borderRadius: 16,
    fontSize: 13,
    lineHeight: '18px',
    textAlign: 'center',
  },
  chipTernary: {
    fontWeight: 700,
    background: theme.palette.tertiary.main,
    color: 'white',
  },
  chip: {
    color: 'white',
  },
  textBlockDark: {
    backgroundColor: '#EEEEEE',
  },
  chipWhite: {
    backgroundColor: grey[100],
    padding: theme.spacing(1),
    height: 'auto',
    textAlign: 'center',

    '& span': {
      whiteSpace: 'break-spaces !important' as any,
    },
  },
}));

export const PremiumPlanCard = ({
  showRenewButton = false,
  showCancelButton = false,
  renew,
  isRenewing,
}: PremiumPlanCardProps) => {
  const classes = useStyles();

  return (
    <Box className={classes.wrapper}>
      <Box className={classes.card}>
        <Box mb={4}>
          <Typography component="span" variant="h6" fontWeight={400}>
            OpenTalent{' '}
          </Typography>
          <Typography component="span" variant="h6" fontWeight={600}>
            Direct
          </Typography>
        </Box>

        <Box mb={8} alignSelf="center">
          <Grid spacing={2} container>
            <Grid item>
              <Chip
                label="Ideal for: independent recruiters"
                className={classes.chipWhite}
              />
            </Grid>
          </Grid>
        </Box>

        <Box width={335} pb={2} margin="0 auto">
          {DIRECT_FEATURES.map((feature) => (
            <CheckThickListItem key={feature} isChecked>
              {feature}
            </CheckThickListItem>
          ))}
        </Box>

        {showCancelButton && (
          <RouterButton
            to={(location) => ({
              ...location,
              state: {
                [CANCEL_MODAL]: true,
              },
            })}
            variant="text"
            color="info"
          >
            CANCEL SUBSCRIPTION
          </RouterButton>
        )}
        {showRenewButton && (
          <Button
            fullWidth
            size="large"
            rounded
            variant="contained"
            color="info"
            disabled={isRenewing}
            onClick={() => renew()}
          >
            renew subscription
          </Button>
        )}
      </Box>

      <Box mt={8} textAlign="center">
        <OuterLink
          color="info.main"
          target="_blank"
          href="https://opentalent.notion.site/Q-A-about-OpenTalent-OT-Direct-d7e36aaf5a7648379b82e3b330a806ce"
          style={{ textDecoration: 'underline' }}
        >
          Frequently Asked Questions{' '}
          <OpenInNewOutlinedIcon
            fontSize="small"
            style={{ transform: 'translateY(4px)' }}
          />
        </OuterLink>
      </Box>
    </Box>
  );
};
