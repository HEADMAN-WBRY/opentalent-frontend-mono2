import React from 'react';

import { Box, CircularProgress, Grid } from '@mui/material';

import Typography from '@libs/ui/components/typography';

import { CancelSubscriptionModal } from '../CancelSubscriptionModal';
import { EnterprisePlanCard } from '../free/EnterprisePlanCard';
import { PlanData } from './PlanData';
import { PremiumPlanCard } from './PremiumPlanCard';
import { useRenewAction, useSubscriptionData } from './hooks';

interface PremiumPlanContentProps { }

export const PremiumPlanContent = (props: PremiumPlanContentProps) => {
  const {
    showRenewButton,
    showCancelButton,
    isSubscriptionInfoLoading,
    subscription,
  } = useSubscriptionData();

  const { renew, isRenewing } = useRenewAction();

  if (isSubscriptionInfoLoading || isRenewing) {
    return <CircularProgress size={50} color="secondary" />;
  }

  return (
    <>
      <Box mb={4}>
        <Typography component="span" variant="h5" fontWeight={400}>
          Your current plan:{' '}
        </Typography>
        <Typography component="span" variant="h5" fontWeight={700}>
          Direct
        </Typography>
      </Box>

      <PlanData subscription={subscription} />

      <Box mb={6} mt={4}>
        <Grid spacing={6} container>
          <Grid item>
            <PremiumPlanCard
              showRenewButton={showRenewButton}
              showCancelButton={showCancelButton}
              renew={renew}
              isRenewing={isRenewing}
            />
          </Grid>
          <Grid item>
            <EnterprisePlanCard />
          </Grid>
        </Grid>
      </Box>

      <CancelSubscriptionModal />
    </>
  );
};
