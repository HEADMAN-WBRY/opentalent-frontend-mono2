import { useIsFreeCompanyAccount } from 'hooks/company/useCompanyAccountType';
import { useSnackbar } from 'notistack';

import {
  CurrentCompanySubscriptionDocument,
  useCurrentCompanySubscriptionQuery,
  useRenewCurrentCompanySubscriptionMutation,
} from '@libs/graphql-types';

export const useRenewAction = () => {
  const { enqueueSnackbar } = useSnackbar();
  const [renew, { loading: isRenewing }] =
    useRenewCurrentCompanySubscriptionMutation({
      onCompleted: () => {
        enqueueSnackbar('Your subscription has been renewed.', {
          variant: 'success',
        });
      },
      refetchQueries() {
        return [CurrentCompanySubscriptionDocument];
      },
    });

  return { renew, isRenewing };
};

export const useSubscriptionData = () => {
  const isFreeCompanyAccount = useIsFreeCompanyAccount();
  const {
    data: subscription,
    refetch,
    loading: isSubscriptionInfoLoading,
  } = useCurrentCompanySubscriptionQuery({
    skip: isFreeCompanyAccount,
  });

  const unlimitedSubscription =
    subscription?.currentCompanyUserCompanySubscription === null;

  const cancelAtPeriodEnd =
    subscription?.currentCompanyUserCompanySubscription?.cancel_at_period_end;

  const showRenewButton =
    !isFreeCompanyAccount && cancelAtPeriodEnd && !unlimitedSubscription;
  const showCancelButton =
    !isFreeCompanyAccount && !cancelAtPeriodEnd && !unlimitedSubscription;

  return {
    showRenewButton,
    showCancelButton,
    isSubscriptionInfoLoading,
    subscription,
    refetch,
  };
};
