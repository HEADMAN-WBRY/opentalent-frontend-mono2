export const CANCEL_MODAL = 'CANCEL_MODAL';

export const DIRECT_FEATURES = [
  '1 or more job posts',
  'Full candidate profiles',
  'Instant Matches',
  'Invite to apply',
  'Send message to anyone',
  'Download CVs',
  'Hire anyone, free of fees!',
];
