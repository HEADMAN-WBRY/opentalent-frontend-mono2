import { useIsFreeCompanyAccount } from 'hooks/company/useCompanyAccountType';
import React from 'react';

import { Box } from '@mui/material';

import { FreePlanContent } from './free';
import { PremiumPlanContent } from './premium';

interface PlansProps { }

const Plans = (props: PlansProps) => {
  const isFree = useIsFreeCompanyAccount();

  return <Box>{isFree ? <FreePlanContent /> : <PremiumPlanContent />}</Box>;
};

export default Plans;
