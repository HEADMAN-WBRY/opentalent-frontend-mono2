import { useSnackbar } from 'notistack';
import React from 'react';
import { DefaultModalProps, withLocationStateModal } from 'utils/modals';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import {
  CurrentTalentSubscriptionDocument,
  useCancelCurrentCompanySubscriptionMutation,
} from '@libs/graphql-types';
import Button from '@libs/ui/components/button';
import { DefaultModal } from '@libs/ui/components/modals';
import Typography from '@libs/ui/components/typography';

import { CANCEL_MODAL } from './consts';

const useStyles = makeStyles((theme) => ({
  paper: {
    width: 500,
  },
  content: {
    textAlign: 'left',
  },
}));

const useCancelSubscriptionAction = ({
  onSuccess,
}: {
  onSuccess?: VoidFunction;
}) => {
  const { enqueueSnackbar } = useSnackbar();
  const [cancel, { loading }] = useCancelCurrentCompanySubscriptionMutation({
    refetchQueries: [CurrentTalentSubscriptionDocument],
  });

  const onCancel = async () => {
    const res = await cancel();

    if (!res.data?.cancelCurrentCompanyUserCompanySubscription) {
      enqueueSnackbar('Something went wrong', { variant: 'error' });
      return;
    }

    enqueueSnackbar('Subscription cancelled', { variant: 'success' });
    onSuccess?.();
  };
  return {
    onCancel,
    loading,
  };
};

const CancelSubscriptionModalComponent = ({
  isOpen,
  close,
  onSuccess,
}: DefaultModalProps<true> & {
  onSuccess?: (...params: any[]) => Promise<unknown>;
}) => {
  const classes = useStyles();
  const { onCancel, loading } = useCancelSubscriptionAction({
    onSuccess: async () => {
      onSuccess?.();
      close();
    },
  });

  return (
    <DefaultModal
      actions={
        <Grid container spacing={4}>
          <Grid style={{ flexGrow: 1 }} item>
            <Button
              fullWidth
              color="error"
              variant="contained"
              size="large"
              onClick={onCancel}
              disabled={loading}
            >
              Yes, cancel it
            </Button>
          </Grid>
          <Grid style={{ flexGrow: 2 }} item>
            <Button
              disabled={loading}
              size="large"
              fullWidth
              variant="outlined"
              onClick={close}
            >
              No, leave it
            </Button>
          </Grid>
        </Grid>
      }
      handleClose={close}
      open={isOpen}
      title={`You are going to cancel
your subscription plan.`}
      className={classes.paper}
    >
      <Box mt={4}>
        <Typography color="textSecondary" textAlign="center">
          Are you sure?
        </Typography>
      </Box>
    </DefaultModal>
  );
};

export const CancelSubscriptionModal = withLocationStateModal({
  id: CANCEL_MODAL,
})(CancelSubscriptionModalComponent);
