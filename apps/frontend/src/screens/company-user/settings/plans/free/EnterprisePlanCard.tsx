import { CheckThickListItem } from 'components/check-thick-list';
import React from 'react';

import { Box, Chip, Grid } from '@mui/material';
import { grey } from '@mui/material/colors';
import { makeStyles } from '@mui/styles';

import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

interface EnterprisePlanCardProps { }

const useStyles = makeStyles((theme) => ({
  wrapper: {
    width: 360,
  },
  card: {
    backgroundColor: grey[100],
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    borderRadius: 16,
    padding: theme.spacing(8, 6),
  },
  chipWhite: {
    backgroundColor: 'white',
    padding: theme.spacing(1),
    height: 'auto',
    textAlign: 'center',

    '& span': {
      whiteSpace: 'break-spaces !important' as any,
    },
  },
}));

const FEATURES = [
  'Unlimited job posts',
  'Unlimited users accounts',
  'Get Instant Matches & Applications',
  'Send message to anyone',
  'Hire anyone, free of any fees!',
];

export const EnterprisePlanCard = (props: EnterprisePlanCardProps) => {
  const classes = useStyles();

  return (
    <Box className={classes.wrapper}>
      <Box className={classes.card}>
        <Box mb={4} mx={4} alignSelf="center">
          <Typography component="span" variant="h6" fontWeight={400}>
            OpenTalent{' '}
          </Typography>
          <Typography component="span" variant="h6" fontWeight={600}>
            Enterprise
          </Typography>
        </Box>

        <Box mb={5} alignSelf="center">
          <Grid spacing={2} container>
            <Grid item>
              <Chip
                label="Ideal for: companies and agencies"
                className={classes.chipWhite}
              />
            </Grid>
          </Grid>
        </Box>

        <Box mb={4} width="100%">
          <Typography
            variant="subtitle1"
            color="textSecondary"
            whiteSpace="break-spaces"
            textAlign="center"
          >
            {`Custom pricing tailored
to your business`}
          </Typography>
        </Box>

        <Box width="100%" mb={6}>
          <Button
            variant="contained"
            color="info"
            size="large"
            fullWidth
            href="https://calendly.com/d/227-f5k-kcg/discovery-call-opentalent-enterprise"
            {...{ target: '_blank' }}
          >
            request demo
          </Button>
        </Box>

        <Box mb={2}>
          <Typography
            textAlign="left"
            fontWeight={700}
            variant="body2"
            color="textSecondary"
          >
            OpenTalent Direct features
          </Typography>
        </Box>

        <Box mb={1}>
          <Typography textAlign="center" color="info.main" variant="h3">
            +
          </Typography>
        </Box>
        <Box width="100%" margin="0 auto">
          {FEATURES.map((feature) => (
            <CheckThickListItem key={feature} isChecked>
              {feature}
            </CheckThickListItem>
          ))}
        </Box>
      </Box>
    </Box>
  );
};
