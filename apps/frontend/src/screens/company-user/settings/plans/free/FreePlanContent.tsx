import React from 'react';

import { Box, Grid } from '@mui/material';

import Typography from '@libs/ui/components/typography';

import { EnterprisePlanCard } from './EnterprisePlanCard';
import { PremiumPlanCardForFreeAccount } from './PremiumPlanCardForFreeAccount';

interface FreePlanContentProps { }

export const FreePlanContent = (props: FreePlanContentProps) => {
  return (
    <>
      <Box mb={4}>
        <Typography component="span" variant="h5" fontWeight={400}>
          Current plan:{' '}
        </Typography>
        <Typography component="span" variant="h5" fontWeight={700}>
          Free
        </Typography>
      </Box>
      <Box mb={8}>
        <Typography>
          Unlock the full potential with OpenTalent with these available plans:
        </Typography>
      </Box>

      <Box mb={6} mt={4}>
        <Grid spacing={6} container>
          <Grid item>
            <PremiumPlanCardForFreeAccount />
          </Grid>
          <Grid item>
            <EnterprisePlanCard />
          </Grid>
        </Grid>
      </Box>
    </>
  );
};
