import { CheckThickListItem } from 'components/check-thick-list';
import CompanyStripePaymentButton from 'components/custom/company/company-stripe-payment-button';
import { DIRECT_PRICE } from 'consts/plans';
import React from 'react';

import OpenInNewOutlinedIcon from '@mui/icons-material/OpenInNewOutlined';
import { Box, Chip, Grid } from '@mui/material';
import { grey } from '@mui/material/colors';
import { makeStyles } from '@mui/styles';

import { formatCurrency } from '@libs/helpers/format';
import Typography, { OuterLink } from '@libs/ui/components/typography';

import { DIRECT_FEATURES } from '../consts';

interface PremiumPlanCardForFreeAccountProps { }

const useStyles = makeStyles((theme) => ({
  wrapper: {
    width: 360,
  },
  card: {
    backgroundColor: theme.palette.background.paper,
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    borderRadius: 16,
    padding: theme.spacing(8, 6),
  },
  textBlockDark: {
    backgroundColor: '#EEEEEE',
  },
  chipWhite: {
    backgroundColor: grey[100],
    padding: theme.spacing(1),
    height: 'auto',
    textAlign: 'center',

    '& span': {
      whiteSpace: 'break-spaces !important' as any,
    },
  },
}));

export const PremiumPlanCardForFreeAccount = (
  props: PremiumPlanCardForFreeAccountProps,
) => {
  const classes = useStyles();

  return (
    <Box className={classes.wrapper}>
      <Box className={classes.card}>
        <Box mb={4} mx={4} alignSelf="center">
          <Typography component="span" variant="h6" fontWeight={400}>
            OpenTalent{' '}
          </Typography>
          <Typography component="span" variant="h6" fontWeight={600}>
            Direct
          </Typography>
        </Box>

        <Box mb={8} alignSelf="center">
          <Grid spacing={2} container>
            <Grid item>
              <Chip
                label="Ideal for: independent recruiters"
                className={classes.chipWhite}
              />
            </Grid>
          </Grid>
        </Box>

        <Box mb={6} width="100%">
          <Box display="flex" justifyContent="center">
            <Typography variant="h4" color="textSecondary">
              {formatCurrency(DIRECT_PRICE)}
            </Typography>
            <Typography variant="body1" color="textSecondary">
              &nbsp;per month
            </Typography>
          </Box>
        </Box>

        <Box width="100%" mb={6}>
          <CompanyStripePaymentButton>UPGRADE</CompanyStripePaymentButton>
        </Box>

        <Box mb={6}>
          <Typography fontWeight={700} variant="body2" color="textSecondary">
            Includes the following features:
          </Typography>
        </Box>

        <Box width="100%" margin="0 auto">
          {DIRECT_FEATURES.map((feature) => (
            <CheckThickListItem key={feature} isChecked>
              {feature}
            </CheckThickListItem>
          ))}
        </Box>
      </Box>

      <Box mt={8} textAlign="center">
        <OuterLink
          color="info.main"
          target="_blank"
          href="https://opentalent.notion.site/Q-A-about-OpenTalent-OT-Direct-d7e36aaf5a7648379b82e3b330a806ce"
        >
          Frequently Asked Questions{' '}
          <OpenInNewOutlinedIcon
            fontSize="small"
            style={{ transform: 'translateY(4px)' }}
          />
        </OuterLink>
      </Box>
    </Box>
  );
};
