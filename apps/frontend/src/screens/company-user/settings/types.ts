export enum SettingsTabs {
  General = 'general',
  Users = 'users',
  Legal = 'legal',
  Plans = 'plans',
  Notifications = 'notifications',
}
