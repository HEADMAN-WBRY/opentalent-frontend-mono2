import { pathManager } from 'routes';

import { SettingsTabs } from './types';

export const LEGAL_ROUTE = `${pathManager.company.settings.main.generatePath()}?tab=${
  SettingsTabs.Legal
}`;
export const USERS_ROUTE = `${pathManager.company.settings.main.generatePath()}?tab=${
  SettingsTabs.Users
}`;
