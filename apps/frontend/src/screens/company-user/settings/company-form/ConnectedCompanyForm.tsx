import React from 'react';

import { Box } from '@mui/material';

import { CompanyForm } from './CompanyForm';
import { useInitialValues, useUpdateUser } from './hooks';

interface ConnectedCompanyFormProps {
  submitText?: string;
  onSuccess?: VoidFunction;
  isCreate?: boolean;
}

const ConnectedCompanyForm = ({
  submitText = 'Submit',
  onSuccess,
}: ConnectedCompanyFormProps) => {
  const { initialAvatar, initialValues } = useInitialValues();
  const { loading, updateRequest } = useUpdateUser({ onSuccess });

  return (
    <Box maxWidth="500px">
      <CompanyForm
        onSubmit={updateRequest}
        loading={loading}
        initialAvatar={initialAvatar}
        initialValues={initialValues}
      />
    </Box>
  );
};

export default ConnectedCompanyForm;
