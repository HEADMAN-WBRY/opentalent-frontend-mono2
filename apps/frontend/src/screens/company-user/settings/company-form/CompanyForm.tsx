import { Formik } from 'formik';
import React from 'react';

import { Box, Grid } from '@mui/material';

import { FileTypeEnum } from '@libs/graphql-types';
import { COUNTRY_OPTIONS } from '@libs/helpers/consts/countries';
import Button from '@libs/ui/components/button';
import { ConnectedSelect } from '@libs/ui/components/form/select';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';

import AvatarControl from '../avatar-control';
import validator from './validator';

interface CompanyFormProps {
  onSubmit: (data: any) => void;
  initialValues: any;
  initialAvatar?: string;
  submitText?: string;
  loading?: boolean;
  enableReinitialize?: boolean;
}

export const CompanyForm = ({
  submitText = 'Submit',
  onSubmit,
  initialValues,
  initialAvatar,
  loading,
  enableReinitialize = false,
}: CompanyFormProps) => {
  return (
    <Box maxWidth="500px">
      <Formik
        onSubmit={(values) => onSubmit(values)}
        validationSchema={validator}
        initialValues={initialValues}
        validateOnBlur={false}
        validateOnChange
        enableReinitialize={enableReinitialize}
      >
        {({ handleSubmit }) => (
          <Grid spacing={4} direction="column" container wrap="nowrap">
            <Grid item>
              <AvatarControl
                fileType={FileTypeEnum.CompanyLogo}
                name="logo"
                initialAvatar={initialAvatar}
                label="Company logo"
              />
            </Grid>
            <br />
            <Grid item>
              <ConnectedTextField
                size="small"
                name="name"
                fullWidth
                variant="filled"
                label="Company name*"
              />
            </Grid>
            <Grid item>
              <ConnectedTextField
                name="website"
                size="small"
                fullWidth
                variant="filled"
                label="Website"
              />
            </Grid>

            <Grid item>
              <ConnectedSelect
                name="country"
                fullWidth
                variant="filled"
                data-test-id="location"
                label="Country"
                options={COUNTRY_OPTIONS}
                color="primary"
              />
            </Grid>

            <Grid item>
              <ConnectedTextField
                name="type_of_activity"
                size="small"
                fullWidth
                variant="filled"
                label="Type of activity"
              />
            </Grid>

            <Grid item>
              <ConnectedTextField
                name="about"
                size="small"
                fullWidth
                rows={6}
                multiline
                variant="filled"
                label="About"
              />
            </Grid>

            <Grid item>
              <Box pt={4}>
                <Button
                  onClick={() => handleSubmit()}
                  variant="contained"
                  color="primary"
                  disabled={loading}
                >
                  {submitText}
                </Button>
              </Box>
            </Grid>
          </Grid>
        )}
      </Formik>
    </Box>
  );
};
