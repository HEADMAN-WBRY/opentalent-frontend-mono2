import { Formik } from 'formik';
import React from 'react';

import { Box, Grid } from '@mui/material';

import { FileTypeEnum } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';

import AvatarControl from '../avatar-control';
import { getValidator } from './validator';

interface PersonalFormProps {
  submitText?: string;
  onSubmit: (values: any) => void;
  isLoading?: boolean;
  onCancel?: VoidFunction;
  initialValues: any;
  initialAvatar?: string;
  isCreate?: boolean;
}

const PersonalForm = ({
  submitText = 'Submit',
  initialAvatar,
  initialValues,
  isLoading,
  onSubmit,
  isCreate,
  onCancel,
}: PersonalFormProps) => {
  return (
    <Box maxWidth="500px">
      <Formik
        onSubmit={(values) => onSubmit(values)}
        validationSchema={getValidator(!!isCreate)}
        initialValues={initialValues}
        validateOnBlur={false}
        validateOnChange
      >
        {({ handleSubmit }) => (
          <Grid spacing={4} direction="column" container wrap="nowrap">
            {!isCreate && (
              <Grid item>
                <AvatarControl
                  fileType={FileTypeEnum.CompanyUserAvatar}
                  initialAvatar={initialAvatar}
                />
              </Grid>
            )}
            <br />
            <Grid item>
              <Grid spacing={4} container>
                <Grid xs={12} sm={6} item>
                  <ConnectedTextField
                    size="small"
                    name="first_name"
                    fullWidth
                    variant="filled"
                    label="First name*"
                  />
                </Grid>
                <Grid xs={12} sm={6} item>
                  <ConnectedTextField
                    name="last_name"
                    size="small"
                    fullWidth
                    variant="filled"
                    label="Last name*"
                  />
                </Grid>
              </Grid>
            </Grid>
            {isCreate && (
              <Grid item>
                <ConnectedTextField
                  name="email"
                  disabled={!isCreate}
                  fullWidth
                  size="small"
                  variant="filled"
                  label="Email*"
                />
              </Grid>
            )}
            <Grid item>
              <Box pt={4}>
                <Grid spacing={5} container>
                  <Grid xs={6} item>
                    <Button
                      fullWidth
                      onClick={() => handleSubmit()}
                      variant="contained"
                      color="primary"
                      disabled={isLoading}
                    >
                      {submitText}
                    </Button>
                  </Grid>
                  {onCancel && (
                    <Grid xs={6} item>
                      <Button
                        fullWidth
                        onClick={onCancel}
                        variant="outlined"
                        color="secondary"
                        disabled={isLoading}
                      >
                        Cancel
                      </Button>
                    </Grid>
                  )}
                </Grid>
              </Box>
            </Grid>
          </Grid>
        )}
      </Formik>
    </Box>
  );
};

export default PersonalForm;
