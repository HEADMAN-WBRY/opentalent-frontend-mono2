import { useQuery } from '@apollo/client';
import { GET_COLLEAGUES } from 'graphql/user';

import { Query, User } from '@libs/graphql-types';

export const useColleagues = () => {
  const { loading, data } = useQuery<Query>(GET_COLLEAGUES);
  const colleagues = (data?.currentCompanyUserColleagues || []) as User[];
  return { colleagues, isLoading: loading };
};
