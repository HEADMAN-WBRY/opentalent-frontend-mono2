import { useCurrentUser } from 'hooks/auth';
import { useIsEnterpriseCompanyAccount } from 'hooks/company/useCompanyAccountType';
import { useSearchParams, usePushWithQuery } from 'hooks/routing';

import { SettingsTabs } from './types';

export const useSettingsTabs = () => {
  const isEnterprise = useIsEnterpriseCompanyAccount();
  const defaultTab = isEnterprise ? SettingsTabs.Users : SettingsTabs.Plans;
  const { tab = defaultTab } = useSearchParams();
  const push = usePushWithQuery();
  const onTabChange = (_: React.ChangeEvent<unknown>, tab: string) =>
    push({ query: { tab } });

  return { tab, onTabChange };
};

export const useClientType = () => {
  const { data } = useCurrentUser();
  const isPrimary = data?.currentCompanyUser?.is_primary;

  return { isPrimary };
};
