import { useMutation } from '@apollo/client';
import { useFormikContext } from 'formik';
import { UPLOAD_USER_AVATAR } from 'graphql/user';
import { useCurrentUser } from 'hooks/auth';
import { useSnackbar } from 'notistack';
import { useCallback, useEffect } from 'react';

import { Mutation, MutationUploadUserAvatarArgs } from '@libs/graphql-types';

export const useAvatarChange = (name: string) => {
  const { data: userData } = useCurrentUser();
  const userId = userData?.currentCompanyUser?.id as string;
  const { enqueueSnackbar } = useSnackbar();
  const [upload, { data, called }] = useMutation<
    Mutation,
    MutationUploadUserAvatarArgs
  >(UPLOAD_USER_AVATAR);
  const { setFieldValue } = useFormikContext();

  useEffect(() => {
    if (called) {
      setFieldValue(name, data?.uploadUserAvatar?.hash);
    }
  }, [data, name, setFieldValue, called]);

  return useCallback(
    async (files: File[]) => {
      try {
        await upload({ variables: { file: files[0], user_id: userId } });
      } catch (e) {
        enqueueSnackbar((e as any).toString());
      }
    },
    [userId, upload, enqueueSnackbar],
  );
};
