import React from 'react';

import ConnectedImageUpload from '@libs/ui/components/form/image-upload/ConnectedImageUpload';

import { useAvatarChange } from './hooks';

interface AvatarControlProps {
  initialAvatar?: string;
}

const AvatarControl = ({ initialAvatar }: AvatarControlProps) => {
  const onChange = useAvatarChange('avatar');
  return (
    <ConnectedImageUpload
      name="avatar"
      initialImage={initialAvatar}
      onChange={onChange}
      label="Upload avatar"
    />
  );
};

export default AvatarControl;
