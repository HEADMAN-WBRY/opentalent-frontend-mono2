import React from 'react';

import { FileTypeEnum } from '@libs/graphql-types';
import ConnectedImageUpload from '@libs/ui/components/form/image-upload/ConnectedImageUpload';

import { useAvatarChange } from './hooks';

interface AvatarControlProps {
  initialAvatar?: string;
  name?: string;
  fileType: FileTypeEnum;
  label?: string;
}

const AvatarControl = ({
  initialAvatar,
  name = 'avatar',
  fileType,
  label = 'Upload avatar',
}: AvatarControlProps) => {
  const onChange = useAvatarChange({ name, fileType });

  return (
    <ConnectedImageUpload
      name={name}
      initialImage={initialAvatar}
      onChange={onChange}
      label={label}
    />
  );
};

export default AvatarControl;
