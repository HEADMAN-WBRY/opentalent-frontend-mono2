import { ConnectedPageLayout } from 'components/layout/page-layout';
import { useIsEnterpriseCompanyAccount } from 'hooks/company/useCompanyAccountType';
import React from 'react';

import { TabContext, TabPanel } from '@mui/lab';
import { Box, Tab, Tabs } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import General from './general';
import { useClientType, useSettingsTabs } from './hooks';
import Noifications from './notifications';
import Plans from './plans';
import { SettingsTabs } from './types';
import UsersTab from './users-tab';

interface CompanySettingsProps { }

const useStyles = makeStyles((theme) => ({
  tabPanel: {
    padding: `${theme.spacing(6)} 0`,
  },
  legalTabIcon: {
    background: theme.palette.primary.main,
    borderRadius: '100%',
  },
}));

const CompanySettings = (props: CompanySettingsProps) => {
  const classes = useStyles();
  const { tab, onTabChange } = useSettingsTabs();
  const isEnterprise = useIsEnterpriseCompanyAccount();
  const { isPrimary } = useClientType();

  return (
    <ConnectedPageLayout drawerProps={{}} documentTitle="Settings">
      <Box mb={4}>
        <Typography variant="h5" paragraph>
          Account Settings
        </Typography>
      </Box>

      <TabContext value={tab as string}>
        <Tabs
          value={tab as string}
          selectionFollowsFocus
          onChange={onTabChange}
          textColor="secondary"
          indicatorColor="secondary"
        >
          {!isEnterprise && (
            <Tab color="secondary" value={SettingsTabs.Plans} label="Plans" />
          )}
          {isPrimary && (
            <Tab
              color="secondary"
              value={SettingsTabs.General}
              label="Company"
            />
          )}
          <Tab color="secondary" value={SettingsTabs.Users} label="Users" />
          {/* <Tab */}
          {/*   color="secondary" */}
          {/*   value={SettingsTabs.Notifications} */}
          {/*   label="Notifications" */}
          {/* /> */}
        </Tabs>
        {isPrimary && (
          <TabPanel
            classes={{ root: classes.tabPanel }}
            value={SettingsTabs.General}
          >
            <General />
          </TabPanel>
        )}
        {isPrimary && (
          <TabPanel
            classes={{ root: classes.tabPanel }}
            value={SettingsTabs.Notifications}
          >
            <Noifications />
          </TabPanel>
        )}
        <TabPanel
          classes={{ root: classes.tabPanel }}
          value={SettingsTabs.Users}
        >
          <UsersTab />
        </TabPanel>
        <TabPanel
          classes={{ root: classes.tabPanel }}
          value={SettingsTabs.Plans}
        >
          <Plans />
        </TabPanel>
      </TabContext>
    </ConnectedPageLayout>
  );
};

export default CompanySettings;
