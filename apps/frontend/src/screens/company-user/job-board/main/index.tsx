import React from 'react';

const JobBoard = React.lazy(() => import('./JobBoard'));

export default JobBoard;
