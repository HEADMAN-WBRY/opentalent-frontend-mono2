import { ReactComponent as WarningIcon } from 'assets/icons/warning.svg';
import React from 'react';
import { Link } from 'react-router-dom';

import { Box, Grid } from '@mui/material';

import Typography from '@libs/ui/components/typography';

import { LEGAL_ROUTE } from '../../settings/consts';

interface DraftsAlertProps { }

const DraftsAlert = (props: DraftsAlertProps) => {
  return (
    <Box pt={6}>
      <Grid style={{ maxWidth: 500 }} wrap="nowrap" spacing={4} container>
        <Grid item>
          <WarningIcon style={{ marginTop: 8 }} />
        </Grid>
        <Grid item>
          <Typography style={{ marginBottom: 8 }}>
            Your job was saved to drafts.
          </Typography>
          <Typography style={{ marginBottom: 8 }}>
            To get a full access to all OpenTalent features you should sign the{' '}
            <Link to={LEGAL_ROUTE}>
              <Typography color="tertiary" component="span">
                agreement
              </Typography>
            </Link>
            .
          </Typography>
          <Typography>
            You’ll be able to post your gig after after it.
          </Typography>
        </Grid>
      </Grid>
    </Box>
  );
};

export default DraftsAlert;
