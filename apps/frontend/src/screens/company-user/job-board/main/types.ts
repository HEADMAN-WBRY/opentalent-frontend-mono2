export enum JobTypeEnums {
  Active = 'active',
  Archived = 'archived',
  Draft = 'draft',
}
