import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  root: {},
  tab: {
    minWidth: 100,
    '&:not(:last-child)': {
      marginRight: theme.spacing(4),
    },
  },
}));

export default useStyles;
