import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  contentWrapper: {
    background: theme.palette.grey[200],
  },
  contentGrid: {
    [theme.breakpoints.down('md')]: {
      'body &': {
        flexDirection: 'column',
        paddingBottom: theme.spacing(10),
      },
    },
  },
  cardsContainer: {
    flexWrap: 'nowrap',

    [theme.breakpoints.down('md')]: {
      'body &': {
        order: 1,
      },
    },
  },
  overallContainer: {
    flexGrow: 1,
    maxWidth: 325,

    [theme.breakpoints.down('md')]: {
      'body &': {
        maxWidth: '100%',
        order: 0,
      },
    },
  },
  floatingAction: {
    position: 'fixed',
    bottom: theme.spacing(6),
    left: '50%',
    transform: 'translateX(-50%)',
    zIndex: 1,
  },
}));

export default useStyles;
