import { JobPushDirectModal } from 'components/custom/company/modals/direct';
import { ConnectedPageLayout } from 'components/layout/page-layout';
import { useCurrentTime } from 'hooks/common/useCurrentTime';
import useMediaQueries from 'hooks/common/useMediaQueries';
import { useSearchParams } from 'hooks/routing';
import React from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { pathManager } from 'routes/consts';

import AddIcon from '@mui/icons-material/Add';
import { Box, Grid, Hidden, Fab } from '@mui/material';

import { JobsCountByStatus } from '@libs/graphql-types';
import { RouterButton } from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import FreeJobBoard from '../free';
import JobsPagination from './JobsPagination';
import NoJobsContent from './NoJobsContent';
import { JOB_BOARD_TEST_DATA_ATTRS } from './consts';
import { useJobBoardInfo } from './hooks';
import JobCard from './job-card';
import JobTabs from './job-tabs';
import { SuccessPaymentModal } from './modals/SuccessPaymentModal';
import Overall from './overall';
import useStyles from './styles';
import { JobTypeEnums } from './types';

interface JobBoardProps extends RouteComponentProps { }

const JobBoard = (props: JobBoardProps) => {
  const currentTime = useCurrentTime();
  const classes = useStyles();
  const { jobsType = JobTypeEnums.Active, page } = useSearchParams();
  const { isSM } = useMediaQueries();
  const {
    jobs,
    loading,
    currentCompanyUserJobBoardInfo,
    jobsPagination,
    jobsCount,
    refresh,
  } = useJobBoardInfo({
    jobType: jobsType as JobTypeEnums,
    page: Number(page as string),
  });

  const isDraft = jobsType === JobTypeEnums.Draft;
  const hasActiveJobs = !!jobsCount?.find((i) => i?.type === 'ACTIVE')?.count;
  const needShowOverview = !isDraft && hasActiveJobs;
  const hasAnyJobs = jobsCount.some((i) => (i?.count || 0) > 0);
  const needShowFreeEmptyScreen = !hasAnyJobs && !loading;

  if (needShowFreeEmptyScreen) {
    return <FreeJobBoard />;
  }

  return (
    <ConnectedPageLayout
      documentTitle="Job board"
      headerProps={{ accountProps: {} }}
      drawerProps={{}}
      isLoading={loading}
      classes={{ contentWrapper: classes.contentWrapper }}
    >
      <Box display="flex" justifyContent="space-between" pb={4}>
        <Typography variant="h5" paragraph>
          My Jobs
        </Typography>
        {!!jobs.length && (
          <Hidden only={['xs']}>
            <RouterButton
              to={pathManager.company.newJob.service.generatePath()}
              data-test-id={JOB_BOARD_TEST_DATA_ATTRS.addJobButton}
              variant="contained"
              color="primary"
            >
              POST A JOB
            </RouterButton>
          </Hidden>
        )}
        <Hidden only={['sm', 'lg', 'md', 'xl']}>
          <Link to={pathManager.company.newJob.type.generatePath()}>
            <Fab
              className={classes.floatingAction}
              color="primary"
              variant="extended"
            >
              <AddIcon
                data-test-id={JOB_BOARD_TEST_DATA_ATTRS.addJobButton}
                fontSize="small"
              />
              &nbsp;Add new job
            </Fab>
          </Link>
        </Hidden>
      </Box>
      <JobTabs
        jobsCount={jobsCount as JobsCountByStatus[]}
        value={jobsType as JobTypeEnums}
      />
      <Grid
        className={classes.contentGrid}
        spacing={isSM ? 6 : 10}
        pt={isSM ? 6 : 10}
        component={Box}
        wrap="nowrap"
        container
      >
        <Grid
          className={classes.cardsContainer}
          component={Box}
          flexGrow={1}
          item
        >
          {jobs.map((item: any) => (
            <Box key={item?.id} pb={4}>
              <JobCard refresh={refresh} currentTime={currentTime} job={item} />
            </Box>
          ))}
          {!jobs.length && (
            <div>
              <NoJobsContent currentJobType={jobsType as JobTypeEnums} />
            </div>
          )}
        </Grid>
        <Grid className={classes.overallContainer} item>
          {needShowOverview && (
            <Overall
              jobsType={jobsType as JobTypeEnums}
              data={currentCompanyUserJobBoardInfo}
            />
          )}
        </Grid>
      </Grid>
      {!!jobs.length && (
        <Box mb={20}>
          <JobsPagination
            currentPage={jobsPagination?.currentPage}
            lastPage={jobsPagination?.lastPage}
          />
        </Box>
      )}
      <SuccessPaymentModal />
      <JobPushDirectModal />
    </ConnectedPageLayout>
  );
};

export default JobBoard;
