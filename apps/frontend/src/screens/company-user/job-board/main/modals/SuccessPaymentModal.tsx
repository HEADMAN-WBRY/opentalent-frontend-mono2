import { usePushWithQuery, useSearchParams } from 'hooks/routing';
import React from 'react';

import { Box, Grid, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Button from '@libs/ui/components/button';
import { DefaultModal } from '@libs/ui/components/modals';

const useStyles = makeStyles((theme) => ({
  root: {
    width: 340,
    padding: theme.spacing(4, 4),

    '& .MuiDialogContent-root': {
      padding: 0,
    },
  },
}));

export const SuccessPaymentModal = () => {
  const { paymentStatus } = useSearchParams();
  const classes = useStyles();
  const isOpen = paymentStatus === 'success';
  const push = usePushWithQuery();
  const close = () => {
    push({ query: { paymentStatus: '' } });
  };

  return (
    <DefaultModal
      handleClose={close}
      open={isOpen}
      className={classes.root}
      title="Congrats! 🎉"
      actions={
        <Grid container justifyContent="center">
          <Grid xs={8} item>
            <Button fullWidth variant="contained" color="info" onClick={close}>
              Continue
            </Button>
          </Grid>
        </Grid>
      }
    >
      <Box mt={4}>
        <Typography>
          Now you can go to job page and start the campaign.
        </Typography>
      </Box>
    </DefaultModal>
  );
};
