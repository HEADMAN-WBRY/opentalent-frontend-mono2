import React from 'react';
import { pathManager } from 'routes';

import InfoIcon from '@mui/icons-material/Info';
import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { RouterButton } from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import { JobTypeEnums } from './types';

const useStyles = makeStyles((theme) => ({
  buttonCaption: {
    display: 'flex',
    alignItems: 'center',
    color: theme.palette.info.main,
  },
}));

const TITLE_MAP: Record<JobTypeEnums, string> = {
  [JobTypeEnums.Active]: 'There are currently no jobs live.',
  [JobTypeEnums.Archived]: 'There are currently no jobs archived.',
  [JobTypeEnums.Draft]: 'There are currently no draft jobs.',
};

export default function NoJobsContent({
  currentJobType,
}: {
  currentJobType: JobTypeEnums;
}) {
  const classes = useStyles();

  return (
    <Box maxWidth={720}>
      <Box mb={8}>
        <Typography paragraph variant="subtitle1" color="info.main">
          {TITLE_MAP[currentJobType]}
        </Typography>
      </Box>
      <Box mb={8}>
        <Grid container spacing={4}>
          <Grid item>
            <RouterButton
              to={pathManager.company.newJob.service.generatePath()}
              variant="contained"
              color="primary"
            >
              POST A JOB
            </RouterButton>
          </Grid>
          <Grid item className={classes.buttonCaption}>
            <InfoIcon fontSize="small" style={{ marginRight: 8 }} />{' '}
            <Typography color="text.primary" variant="captionSmall">
              We’ll show you instant matches from your community and OpenTalent
            </Typography>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
}
