import { useOpenJobPushDirectModal } from 'components/custom/company/modals/direct';
import { useIsFreeCompanyAccount } from 'hooks/company/useCompanyAccountType';
import { useSnackbar } from 'notistack';
import React from 'react';
import { useHistory } from 'react-router-dom';
import { pathManager } from 'routes';

import { Grid } from '@mui/material';

import { Job, useUpdateJobMutation } from '@libs/graphql-types';
import { stopEvent } from '@libs/helpers/common';
import Button, { RouterButton } from '@libs/ui/components/button';

interface JobCardActionsProps {
  job: Job;
  refresh: VoidFunction;
}

const useGoLiveAction = ({ job }: JobCardActionsProps) => {
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();
  const [mutate, { loading }] = useUpdateJobMutation({
    variables: {
      id: job.id,
      is_draft: false,
      campaign_start_date: job.campaign_start_date,
      campaign_end_date: job.campaign_end_date,
    },
    onCompleted: () => {
      enqueueSnackbar('Job is live now', { variant: 'success' });
      history.push(pathManager.company.job.generatePath({ id: job.id }));
    },
  });

  return { goLive: mutate, loading };
};

export const JobCardActions = (props: JobCardActionsProps) => {
  const { job } = props;
  const isDraft = job.is_draft;
  const isFreeCompanyAccount = useIsFreeCompanyAccount();
  const { goLive, loading } = useGoLiveAction(props);
  const openJobPushDirectModal = useOpenJobPushDirectModal();
  const onGoLiveBtnClick = () => {
    if (isFreeCompanyAccount) {
      openJobPushDirectModal();
      return;
    }
    goLive();
  };

  if (isDraft) {
    return (
      <Grid onClick={stopEvent} container wrap="nowrap" spacing={4}>
        <Grid item>
          <RouterButton
            to={pathManager.company.editJob.generatePath({ id: job.id })}
            variant="outlined"
            color="secondary"
            disabled={loading}
          >
            Edit
          </RouterButton>
        </Grid>
        <Grid item>
          <Button
            color="primary"
            variant="contained"
            onClick={onGoLiveBtnClick}
            disabled={loading}
          >
            go live
          </Button>
        </Grid>
      </Grid>
    );
  }

  return (
    <div onClick={stopEvent}>
      <RouterButton
        to={pathManager.company.job.generatePath({ id: job.id })}
        variant="outlined"
        color="secondary"
      >
        open job page
      </RouterButton>
    </div>
  );
};
