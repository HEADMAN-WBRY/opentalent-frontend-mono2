import React from 'react';

import DateRangeIcon from '@mui/icons-material/DateRange';
import { Tooltip } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { Job } from '@libs/graphql-types';
import {
  checkJobRemainHours,
  getDiffHours,
} from '@libs/ui/components/job/utils';
import Typography from '@libs/ui/components/typography';

interface JobCardTitleProps {
  job: Job;
}

const useStyles = makeStyles((theme) => ({
  titleBadge: {
    background: theme.palette.grey[400],
    fontSize: 12,
    lineHeight: '24px',
    marginLeft: 8,
    position: 'relative',
    top: -3,
    display: 'inline-block',
    padding: '0 8px',
  },
  calendar: {
    color: theme.palette.error.main,
    transform: 'translateY(4px)',
    marginLeft: theme.spacing(2),
    cursor: 'pointer',
  },
}));

const JobCardTitle = ({ job }: JobCardTitleProps) => {
  const classes = useStyles();
  const campaignDiffHours = getDiffHours(job.campaign_end_date);
  const needShowCalendar = checkJobRemainHours(campaignDiffHours);

  return (
    <Typography variant="h6">
      {job.name}
      {job.is_draft && <span className={classes.titleBadge}>Draft</span>}
      {needShowCalendar && (
        <Tooltip
          title={`This campaign will expire in less then ${campaignDiffHours} hours`}
        >
          <DateRangeIcon className={classes.calendar} />
        </Tooltip>
      )}
    </Typography>
  );
};

export default JobCardTitle;
