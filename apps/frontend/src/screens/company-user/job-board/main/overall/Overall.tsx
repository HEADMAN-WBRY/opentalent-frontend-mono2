import useMediaQueries from 'hooks/common/useMediaQueries';
import React, { useMemo } from 'react';

import HowToRegIcon from '@mui/icons-material/HowToReg';
import PeopleIcon from '@mui/icons-material/People';
import { Box, Grid, Paper } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { JobBoardInfo } from '@libs/graphql-types';
import { isNumber } from '@libs/helpers/common';
import { formatNumber } from '@libs/helpers/format';
import Typography from '@libs/ui/components/typography';

import { JobTypeEnums } from '../types';

interface OverallProps {
  data?: JobBoardInfo;
  jobsType: JobTypeEnums;
}

const useStyles = makeStyles((theme) => ({
  icon: {
    color: theme.palette.grey[500],
  },
  container: {
    minWidth: 242,

    [theme.breakpoints.down('md')]: {
      'body &': {
        width: '100%',
      },
    },
  },
  content: {
    flexDirection: 'column',

    [theme.breakpoints.down('md')]: {
      'body &': {
        flexDirection: 'row',
      },
    },
  },
  listItem: {
    [theme.breakpoints.down('md')]: {
      'body &': {
        paddingRight: theme.spacing(3),
      },
    },
  },
}));

const Overall = ({ data, jobsType }: OverallProps) => {
  const classes = useStyles();
  const { isSM } = useMediaQueries();
  const dataList = useMemo(
    () => [
      {
        Icon: PeopleIcon,
        value:
          jobsType === JobTypeEnums.Active
            ? data?.talent_matches_count
            : data?.talent_matches_count_archived || 0,
        label: 'Total leads',
      },
      {
        Icon: HowToRegIcon,
        value:
          jobsType === JobTypeEnums.Active
            ? data?.talent_responses_count
            : data?.talent_responses_count_archived || 0,
        label: 'Applications',
      },
    ],
    [data, jobsType],
  );

  return (
    <Paper elevation={0}>
      <Box className={classes.container} p={isSM ? 4 : 6}>
        <Box pb={isSM ? 1 : 6}>
          <Typography variant="h6">Overview</Typography>
        </Box>

        <Grid className={classes.content} container>
          {dataList.map(({ Icon, value, label }) => (
            <Grid key={label} className={classes.listItem} item>
              <Grid wrap="nowrap" spacing={2} alignItems="center" container>
                <Grid display="flex" component={Box} item>
                  <Icon
                    className={classes.icon}
                    fontSize={isSM ? 'small' : 'inherit'}
                  />
                </Grid>
                <Grid item>
                  <Typography
                    component="span"
                    variant={isSM ? 'body2' : 'h6'}
                    color="tertiary"
                  >
                    {isNumber(value) ? formatNumber(value) : value}
                  </Typography>
                </Grid>
                <Grid item>
                  <Typography
                    component="span"
                    variant={isSM ? 'caption' : 'body2'}
                  >
                    {label}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          ))}
        </Grid>
      </Box>
    </Paper>
  );
};

export default Overall;
