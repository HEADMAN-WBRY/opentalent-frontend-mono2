import { ConnectedPageLayout } from 'components/layout/page-layout';
import React from 'react';
import { pathManager } from 'routes';

import InfoIcon from '@mui/icons-material/Info';
import { Avatar, Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { RouterButton } from '@libs/ui/components/button';
import Typography, { OuterLink } from '@libs/ui/components/typography';

import avatar from '../assets/avatar@2x.png';

const useStyles = makeStyles((theme) => ({
  buttonCaption: {
    display: 'flex',
    alignItems: 'center',
    color: theme.palette.info.main,
  },
}));

export default function FreeJobBoard() {
  const classes = useStyles();

  return (
    <ConnectedPageLayout
      documentTitle="Job board"
      headerProps={{ accountProps: {} }}
      drawerProps={{}}
    >
      <Box maxWidth={720}>
        <Typography paragraph variant="h5" color="info.main">
          Post your first job.
        </Typography>
        <Box mb={6}>
          <Typography>
            Welcome to OpenTalent; the direct sourcing platform for modern
            hiring teams. Post your first job, get leads, and hire anyone
            directly, free of middlemen fees.
          </Typography>
          <Typography>
            Need any help?{' '}
            <OuterLink
              href="https://calendly.com/pieter-meeting/book-a-demo"
              target="_blank"
              color="text.primary"
              hasUnderline
            >
              Contact us.
            </OuterLink>
          </Typography>
        </Box>
        <Box mb={8}>
          <Grid container spacing={4}>
            <Grid item>
              <RouterButton
                to={pathManager.company.newJob.service.generatePath()}
                variant="contained"
                color="primary"
              >
                POST A JOB
              </RouterButton>
            </Grid>
            <Grid item className={classes.buttonCaption}>
              <InfoIcon fontSize="small" style={{ marginRight: 8 }} />{' '}
              <Typography color="text.primary" variant="captionSmall">
                We’ll show you instant matches from your community and
                OpenTalent
              </Typography>
            </Grid>
          </Grid>
        </Box>
        <Box maxWidth={580}>
          <Grid container flexWrap="nowrap" spacing={2}>
            <Grid item>
              <Avatar src={avatar} />
            </Grid>

            <Grid item>
              <Typography
                variant="body2"
                component="div"
                fontSize="13px"
                lineHeight="18px"
                color="textSecondary"
              >
                We've been searching for a rockstar Digital Designer for a long
                time, and found one through OpenTalent in a blink of an eye.
                OpenTalent is easy to use and connects us to a whole new network
                of talent.
              </Typography>
              <Typography variant="captionSmall">
                - Evi van Splunder, code d’azur (media agency)
              </Typography>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </ConnectedPageLayout>
  );
}
