export interface OnboardingV3State {
  general: {
    firstName: string;
    lastName: string;
    email: string;
  };
  company: {
    name?: string;
  };
}
