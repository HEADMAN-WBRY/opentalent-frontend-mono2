import cn from 'classnames';
import { getIn, useFormikContext } from 'formik';
import React from 'react';

import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import { makeStyles } from '@mui/styles';

import Button, { ButtonProps } from '@libs/ui/components/button';

interface NextButtonWithValidationProps extends ButtonProps {
  path: string;
}

export const useStyles = makeStyles((theme) => ({
  button: {
    '&.Mui-disabled': {
      color: 'rgba(255, 255, 255, 0.30)',
      background:
        'var(--dark-action-disabled-background-12-p, rgba(255, 255, 255, 0.12))',
    },
  },
}));

const NextButtonWithValidation = (props: NextButtonWithValidationProps) => {
  const { errors } = useFormikContext();
  const classes = useStyles();
  const currentErrors = getIn(errors, props.path, {});
  const hasErrors = !!Object.values(currentErrors)?.length;
  const endIcon =
    props.endIcon !== undefined ? props.endIcon : <ArrowForwardIcon />;

  return (
    <Button
      endIcon={endIcon}
      color="primary"
      variant="contained"
      fullWidth
      {...props}
      disabled={hasErrors || props.disabled}
      className={cn(props.className, classes.button)}
    >
      {props.children || 'next'}
    </Button>
  );
};

export default NextButtonWithValidation;
