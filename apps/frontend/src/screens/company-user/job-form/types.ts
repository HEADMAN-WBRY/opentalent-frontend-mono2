import { BooleanModalState } from 'components/custom/skills-boolean-search';
import { RouteComponentProps } from 'react-router-dom';

import { MutationUpdateJobArgs, SourceTypeEnum } from '@libs/graphql-types';
import { OptionType } from '@libs/ui/components/form/select';

export type PageProps = RouteComponentProps<{ id: string }>;

export interface CreateJobForm
  extends Omit<
    MutationUpdateJobArgs,
    | 'id'
    | 'skills'
    | 'solutions_required'
    | 'hard_skills_required'
    | 'campaign_talent_pool'
    | 'subcategory_id'
  > {
  skills: OptionType[];
  skills_boolean_v2: BooleanModalState;
  required_skills: OptionType[];
  campaign_talent_pool: SourceTypeEnum[];
  is_boolean_search: boolean;
  is_instant_campaign_start: boolean;
}

export enum JobFormModals {
  Agreement = 'agreement',
}
