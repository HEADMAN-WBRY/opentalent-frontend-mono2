import React from 'react';

const JobForm = React.lazy(() => import('./JobForm'));

export default JobForm;
