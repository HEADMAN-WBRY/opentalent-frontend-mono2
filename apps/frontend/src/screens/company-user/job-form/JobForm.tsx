/* eslint-disable no-nested-ternary */
import { BooleanModal } from 'components/custom/skills-boolean-search';
import ScrollToFirstError from 'components/form/scroll-to-first-error';
import { ConnectedPageLayout } from 'components/layout/page-layout';
import { Formik } from 'formik';
import { useCurrentTime } from 'hooks/common/useCurrentTime';
import useMediaQueries from 'hooks/common/useMediaQueries';
import {
  useIsEnterpriseCompanyAccount,
  useIsFreeCompanyAccount,
} from 'hooks/company/useCompanyAccountType';
import React from 'react';
import { pathManager } from 'routes/consts';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { JobTypeEnum, User } from '@libs/graphql-types';
import { modelPath } from '@libs/helpers/form';
import Button from '@libs/ui/components/button';
import {
  CampaignStatus,
  getCampaignStatus,
} from '@libs/ui/components/job/utils';
import Typography from '@libs/ui/components/typography';

import { DEDICATED_SEARCH_STEPS } from '../choose-new-job-type/consts';
import TopSteps from '../choose-new-job-type/shared/TopSteps';
import InfoBlock from './InfoBlock';
import { EDIT_JOB_TEST_ATTRS } from './consts';
import Details from './details';
import General from './general';
import { useInitialValues, useScreenData, useSubmitHandler } from './hooks';
import Skills from './skills';
import ConnectedTalentsCount from './talents-count';
import { CreateJobForm, PageProps } from './types';
import { getPageTexts } from './utils';
import validators from './validator';

const useStyles = makeStyles((theme) => ({
  form: {
    width: 624,

    [theme.breakpoints.down('md')]: {
      width: '100%',
    },
  },
  contentWrapper: {
    position: 'relative',

    [theme.breakpoints.down('md')]: {
      padding: theme.spacing(4),
    },
  },
  contentRoot: {},

  '@global': {
    '.MuiPopover-root': {
      zIndex: 1500,
    },
  },
}));

const JobForm = ({ match }: PageProps) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const currentTime = useCurrentTime();
  const isEnterprise = useIsEnterpriseCompanyAccount();
  const { isMD } = useMediaQueries();
  const jobId = match.params?.id;
  const isDuplicate =
    !!jobId &&
    match.url === pathManager.company.duplicateJob.generatePath({ id: jobId });
  const { data, loading: dataLoading } = useScreenData({
    jobId,
    isDuplicate,
  });
  const { loading: submitLoading, onSubmit } = useSubmitHandler({
    jobId,
    isDuplicate,
  });
  const isFreeCompanyAccount = useIsFreeCompanyAccount();
  const classes = useStyles();
  const isItDraft = !!data?.job?.is_draft;
  const isCreate = !jobId;
  const campaignStatus =
    isDuplicate || isItDraft
      ? CampaignStatus.NotStarted
      : getCampaignStatus(data?.job);

  const initialValues = useInitialValues({
    job: data?.job,
    isCreate,
    isDuplicate,
    jobType: data?.job?.type || JobTypeEnum.Freelance,
  });

  const jobNotFound = !!jobId && !data?.job;
  const texts = getPageTexts({
    name: initialValues.name,
    isCreate,
    isDuplicate,
    jobType: initialValues.type,
    serviceType: initialValues.service_type,
    isItDraft,
  });

  const jobType = initialValues.type;

  const steps = DEDICATED_SEARCH_STEPS;

  const pageTitle = isItDraft
    ? `Edit draft job: ${data?.job?.name || ''}`
    : 'Post a job > Step 2';

  return (
    <ConnectedPageLayout
      documentTitle="Post a job &gt; Step 2"
      headerProps={{ accountProps: {} }}
      drawerProps={{}}
      isLoading={dataLoading}
      classes={{ contentWrapper: classes.contentWrapper }}
    >
      <Box className={classes.contentRoot}>
        <Typography variant="h5" paragraph>
          {pageTitle}
        </Typography>
        {!jobId && (
          <>
            <TopSteps onlyActiveLabel={isMD} steps={steps} activeIndex={1} />
            <Box mt={4}>
              <InfoBlock jobType={jobType} />
            </Box>
          </>
        )}
        {jobNotFound && (
          <Typography paragraph variant="body1">
            Job not found
          </Typography>
        )}
        {!jobNotFound && (
          <Formik
            validationSchema={validators[jobType]({ isEnterprise })}
            initialValues={initialValues}
            onSubmit={onSubmit}
            validateOnChange={false}
          >
            {({ handleSubmit, values, setFieldValue, errors }) => {
              console.log('LOG: errors', errors);
              return (
                <Box className={classes.form}>
                  <>
                    <BooleanModal
                      onSave={(state) =>
                        setFieldValue(
                          modelPath<CreateJobForm>((m) => m.skills_boolean_v2),
                          state,
                        )
                      }
                    />
                    <ScrollToFirstError />
                    <ConnectedTalentsCount
                      jobType={jobType}
                      sourse={values.campaign_talent_pool[0]}
                    />
                    <General
                      status={campaignStatus}
                      jobType={jobType}
                      remoteIsOption={!!values.is_remote_an_option}
                      locationType={values.location_type}
                      talentPool={values.campaign_talent_pool}
                    />
                    <Skills
                      status={campaignStatus}
                      isBooleanSearch={!!values.is_boolean_search}
                    />
                    <Details
                      isEdit={!isDuplicate && !!jobId}
                      isInstant={values.is_instant_campaign_start}
                      status={campaignStatus}
                      owners={data?.currentCompanyUserColleagues as User[]}
                      isItDraft={isItDraft}
                    />
                    {/*                     <FindersFee */}
                    {/*   currentValue={Number(initialValues.finders_fee) || 0} */}
                    {/*   status={campaignStatus} */}
                    {/*   talentPool={values.campaign_talent_pool} */}
                    {/* /> */}

                    {isFreeCompanyAccount ? (
                      <Grid spacing={4} container>
                        <Grid item>
                          <Button
                            size="large"
                            data-test-id={EDIT_JOB_TEST_ATTRS.saveButton}
                            onClick={() => {
                              setFieldValue('is_draft', true);
                              handleSubmit();
                            }}
                            variant="contained"
                            color="primary"
                            disabled={
                              submitLoading ||
                              campaignStatus === CampaignStatus.Finished
                            }
                          >
                            Continue
                          </Button>
                        </Grid>
                      </Grid>
                    ) : (
                      <Grid spacing={4} container>
                        <Grid item>
                          <Button
                            size="large"
                            data-test-id={EDIT_JOB_TEST_ATTRS.submitButton}
                            onClick={() => {
                              handleSubmit();
                            }}
                            variant="contained"
                            color="primary"
                            disabled={
                              submitLoading ||
                              campaignStatus === CampaignStatus.Finished
                            }
                          >
                            {texts.submit}
                          </Button>
                        </Grid>
                        {(isItDraft || isCreate) && (
                          <Grid item>
                            <Button
                              size="large"
                              data-test-id={EDIT_JOB_TEST_ATTRS.saveButton}
                              onClick={() => {
                                setFieldValue('is_draft', true);
                                handleSubmit();
                              }}
                              variant="outlined"
                              color="secondary"
                              disabled={
                                submitLoading ||
                                campaignStatus === CampaignStatus.Finished
                              }
                            >
                              {texts.save}
                            </Button>
                          </Grid>
                        )}
                      </Grid>
                    )}
                  </>
                </Box>
              );
            }}
          </Formik>
        )}
      </Box>
    </ConnectedPageLayout>
  );
};

export default JobForm;
