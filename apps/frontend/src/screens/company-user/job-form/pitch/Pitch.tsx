import React from 'react';

import { Grid } from '@mui/material';

import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import { CampaignStatus } from '@libs/ui/components/job/utils';
import StepSection from '@libs/ui/components/step-section';
import Typography from '@libs/ui/components/typography';

import { getFieldActivity } from '../utils';

interface PitchProps {
  status: CampaignStatus;
}

const Pitch = ({ status }: PitchProps) => {
  return (
    <StepSection index={4} title="Pitch the job">
      <Grid spacing={4} direction="column" container>
        <Grid item>
          <Typography variant="body2">
            Describe the role in a couple of words (max. 200 characters)
          </Typography>
        </Grid>
        <Grid item>
          <ConnectedTextField
            name="pitch"
            fullWidth
            size="small"
            variant="filled"
            label="Elevator pitch"
            multiline
            rows={4}
            disabled={getFieldActivity({
              status,
              blockStatuses: [CampaignStatus.Finished],
            })}
          />
        </Grid>
      </Grid>
    </StepSection>
  );
};

export default Pitch;
