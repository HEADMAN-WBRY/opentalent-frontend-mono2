import TalentsCount from 'components/custom/onboarding/talents-count';
import { INFINITY_SIGN } from 'consts/common';
import React from 'react';
import { isNil } from 'utils/common';

import { makeStyles } from '@mui/styles';

import { JobTypeEnum, SourceTypeEnum } from '@libs/graphql-types';

import { useJobTalentsCount } from './hooks';

interface ConnectedTalentsCountProps {
  jobType: JobTypeEnum;
  sourse?: SourceTypeEnum;
}

const useStyles = makeStyles((theme) => ({
  card: {
    position: 'fixed',
    top: 94,
    right: 24,

    [theme.breakpoints.down('lg')]: {
      position: 'static',
      marginBottom: theme.spacing(6),
    },
  },
}));

const ConnectedTalentsCount = ({
  jobType,
  sourse,
}: ConnectedTalentsCountProps) => {
  const classes = useStyles();
  const { count, loading } = useJobTalentsCount(jobType);
  const finalCount = loading || isNil(count) ? INFINITY_SIGN : count || 0;

  return (
    <TalentsCount className={classes.card} count={finalCount} source={sourse} />
  );
};

export default ConnectedTalentsCount;
