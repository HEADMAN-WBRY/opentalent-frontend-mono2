import { useFormikContext } from 'formik';
import { useCallback, useEffect, useMemo } from 'react';

import { debounce } from '@mui/material';

import {
  GetPotentialTalentsCountQueryVariables,
  JobTypeEnum,
  useGetPotentialTalentsCountQuery,
} from '@libs/graphql-types';
import { parseNumber } from '@libs/helpers/common';

import { CreateJobForm } from '../types';
import { getSkillsFields } from './../utils';

const mapValuesToCountRequest = (
  values: CreateJobForm,
  jobType: JobTypeEnum,
): GetPotentialTalentsCountQueryVariables | undefined => {
  const ownerId = values.campaign_owner_id;
  const skillsFields = getSkillsFields(values);

  if (!ownerId) {
    return undefined;
  }

  return {
    ...skillsFields,
    type: jobType,
    category_id: values.category_id,
    start_date: values.start_date || undefined,
    end_date: values.end_date || undefined,
    rate_min: parseNumber(values.rate_min),
    rate_max: parseNumber(values.rate_max),
    salary_min: parseNumber(values.salary_min),
    salary_max: parseNumber(values.salary_max),
    campaign_start_date: values.campaign_start_date || undefined,
    campaign_end_date: values.campaign_end_date || undefined,
    campaign_talent_pool: values.campaign_talent_pool,
    skills: values.skills?.map(({ value }) => String(value)),
    hours_per_week: values.hours_per_week,
    location_type: values.location_type,
    is_remote_an_option: values.is_remote_an_option,
    country: values.country,
    office_hours_per_month: parseNumber(values.office_hours_per_month),
    campaign_owner_id: ownerId,
  };
};

export const useJobTalentsCount = (jobType: JobTypeEnum) => {
  const { values } = useFormikContext<CreateJobForm>();
  const { data, loading, refetch } = useGetPotentialTalentsCountQuery({
    variables: {
      type: jobType,
    },
  });
  const onChange = useCallback(
    (values: CreateJobForm) => {
      const variables = mapValuesToCountRequest(values, jobType);
      if (variables) {
        refetch(variables);
      }
    },
    [refetch, jobType],
  );

  const debouncedOnChangeHandler = useMemo(() => {
    return debounce(onChange, 1000);
  }, [onChange]);

  useEffect(() => {
    debouncedOnChangeHandler(values);
  }, [debouncedOnChangeHandler, values]);

  return { count: data?.potentialTalentsForJobCount, loading };
};
