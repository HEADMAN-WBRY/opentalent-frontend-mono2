import { addDays } from 'date-fns';
import React, { useMemo } from 'react';

import { Box, Grid } from '@mui/material';

import { User } from '@libs/graphql-types';
import { modelPath } from '@libs/helpers/form';
import { ConnectedCheckbox } from '@libs/ui/components/form/checkbox';
import { ConnectedDatePicker } from '@libs/ui/components/form/datepicker';
import { ConnectedSelect } from '@libs/ui/components/form/select';
import { ConnectedSwitch } from '@libs/ui/components/form/switch';
import { CampaignStatus } from '@libs/ui/components/job/utils';
import StepSection from '@libs/ui/components/step-section';
import Typography from '@libs/ui/components/typography';

import { CreateJobForm } from '../types';
import { getFieldActivity, getOptionFromUser } from '../utils';

interface DetailsProps {
  owners?: User[];
  status: CampaignStatus;
  isEdit: boolean;
  isInstant: boolean;
  isItDraft: boolean;
}

// const DURATION_OPTIONS = [
//   { text: '2 days', value: 2 },
//   { text: '5 days', value: 5 },
//   { text: '10 days', value: 10 },
//   { text: '30 days', value: 30 },
// ];

const Details = ({
  owners = [],
  status,
  isEdit,
  isItDraft,
  isInstant,
}: DetailsProps) => {
  const ownersOptions = useMemo(() => owners.map(getOptionFromUser), [owners]);

  return (
    <StepSection index={3} title="About this Campaign">
      <Box mb={2}>
        <Grid spacing={4} container>
          {!isInstant && (
            <Grid xs={12} sm={6} item>
              <ConnectedDatePicker
                inputFormat="dd/MM/yyyy"
                name={modelPath<CreateJobForm>((i) => i.campaign_start_date)}
                minDate={addDays(new Date(), 1)}
                TextFieldProps={{
                  variant: 'filled',
                  fullWidth: true,
                  label: 'Starting date',
                }}
                disabled={getFieldActivity({
                  status,
                  blockStatuses: [
                    CampaignStatus.Finished,
                    CampaignStatus.Started,
                  ],
                })}
              />
            </Grid>
          )}
          <Grid xs={12} sm={isInstant ? 12 : 6} item>
            <ConnectedDatePicker
              inputFormat="dd/MM/yyyy"
              name={modelPath<CreateJobForm>((i) => i.campaign_end_date)}
              minDate={addDays(new Date(), 1)}
              TextFieldProps={{
                variant: 'filled',
                fullWidth: true,
                label: 'Ending date',
              }}
              disabled={getFieldActivity({
                status,
                blockStatuses: [
                  CampaignStatus.Finished,
                  CampaignStatus.Started,
                ],
              })}
            />
          </Grid>
        </Grid>
      </Box>
      <Grid spacing={4} direction="column" container>
        <Grid item>
          <ConnectedCheckbox
            name="is_instant_campaign_start"
            label="Start campaign now"
            disabled={
              !isItDraft &&
              (getFieldActivity({
                status,
                blockStatuses: [
                  CampaignStatus.Finished,
                  CampaignStatus.Started,
                ],
              }) ||
                isEdit)
            }
          />
        </Grid>
        <Grid item>
          <Box mb={2}>
            <Typography variant="body1">
              Do you need our help in candidates sourcing?
            </Typography>
          </Box>

          <Box>
            <ConnectedCheckbox
              name="propose_to_matchers"
              label="Send this campaign to Talent matchers."
              disabled={
                !isItDraft &&
                (getFieldActivity({
                  status,
                  blockStatuses: [
                    CampaignStatus.Finished,
                    CampaignStatus.Started,
                  ],
                }) ||
                  isEdit)
              }
            />
          </Box>
        </Grid>
        <Grid item>
          <ConnectedSelect
            name="campaign_owner_id"
            options={ownersOptions}
            fullWidth
            variant="filled"
            label="Campaign owner (hiring manager)"
            formControlProps={{ size: 'small' }}
            disabled={getFieldActivity({
              status,
              blockStatuses: [CampaignStatus.Finished],
            })}
          />
        </Grid>
        {isEdit && (
          <Grid item>
            <Box>
              <ConnectedSwitch
                color="info"
                name="update_auto_matching"
                label="Update candidates matching"
              />
            </Box>

            <Typography variant="body2">
              This will update candidates matches after updating the job details
            </Typography>
          </Grid>
        )}
      </Grid>
    </StepSection>
  );
};

export default Details;
