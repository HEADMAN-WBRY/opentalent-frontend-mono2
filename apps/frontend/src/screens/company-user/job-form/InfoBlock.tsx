import { ReactComponent as LampIcon } from 'assets/icons/lamp.svg';
import React from 'react';

import { Grid, Box } from '@mui/material';

import { JobTypeEnum } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

interface InfoBlockProps {
  jobType: JobTypeEnum;
}

const TEXTS = {
  [JobTypeEnum.Freelance]: (
    <Typography variant="body1">
      <b>Once you post this job,</b> we will show you the best matching
      candidates.
    </Typography>
  ),
  [JobTypeEnum.Permanent]: (
    <Typography variant="body1">
      <b>Once you post this job,</b> we will show you the best matching
      candidates.
    </Typography>
  ),
  [JobTypeEnum.Project]: (
    <Typography variant="body1">
      <b>Once you start this campaign</b>, we will identify potential Talent
      within the OpenTalent Network that can help deliver the Project and we
      will inform you of the best match(es).
    </Typography>
  ),
};

const InfoBlock = ({ jobType }: InfoBlockProps) => {
  return (
    <Box pt={4} pb={8} style={{ maxWidth: 530 }}>
      <Grid spacing={4} container wrap="nowrap">
        <Grid item>
          <LampIcon />
        </Grid>
        <Grid item>{TEXTS[jobType]}</Grid>
      </Grid>
    </Box>
  );
};

export default InfoBlock;
