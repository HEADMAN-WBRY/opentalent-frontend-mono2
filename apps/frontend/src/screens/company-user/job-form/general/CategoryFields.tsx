// import { SubCategorySelector } from 'components/custom/subcategory-selector';
import { TALENT_CATEGORIES } from 'graphql/talents';
import React from 'react';

import { Grid } from '@mui/material';

// import { makeStyles } from '@mui/styles';
// import { Query, TalentCategory } from '@libs/graphql-types';
import { modelPath } from '@libs/helpers/form';
import { ConnectedGraphSelect } from '@libs/ui/components/form/select';
import { CampaignStatus } from '@libs/ui/components/job/utils';

import { CreateJobForm } from '../types';
import { getFieldActivity } from '../utils';

interface CategoryFieldsProps {
  status: CampaignStatus;
}

// const useStyles = makeStyles((theme) => ({
//   subcategory: {
//     '&:empty': {
//       display: 'none',
//     },
//   },
// }));

const CategoryFields = ({ status }: CategoryFieldsProps) => {
  // const classes = useStyles();
  // const [categories, setCategories] = useState<TalentCategory[]>([]);
  const isDisabled = getFieldActivity({
    status,
    blockStatuses: [CampaignStatus.Finished],
  });

  return (
    <>
      <Grid item>
        <ConnectedGraphSelect
          name={modelPath<CreateJobForm>((m) => m.category_id)}
          queryOptions={{
            fetchPolicy: 'network-only',
            // onCompleted: (query: Query) =>
            //   setCategories((query?.talentCategories as any[]) || []),
          }}
          hideNoneValue
          query={TALENT_CATEGORIES}
          dataPath="talentCategories"
          dataMap={{ text: 'name', value: 'id' }}
          variant="filled"
          label="Category"
          fullWidth
          formControlProps={{ size: 'small' }}
          disabled={isDisabled}
        />
      </Grid>
      {/* <Grid item className={classes.subcategory}> */}
      {/*   <SubCategorySelector */}
      {/*     categoryPath={modelPath<CreateJobForm>((m) => m.category_id)} */}
      {/*     name={modelPath<CreateJobForm>((m) => m.subcategory_id)} */}
      {/*     categories={categories} */}
      {/*     chipProps={{ */}
      {/*       color: 'tertiary', */}
      {/*       size: 'small', */}
      {/*     }} */}
      {/*     autoCompleteProps={{ */}
      {/*       filterSelectedOptions: true, */}
      {/*       popupIcon: null, */}
      {/*       multiple: false, */}
      {/*       disabled: isDisabled, */}
      {/*     }} */}
      {/*     inputProps={{ */}
      {/*       variant: 'filled', */}
      {/*       label: 'Select sub-category', */}
      {/*       disabled: isDisabled, */}
      {/*       name: modelPath<CreateJobForm>((m) => m.subcategory_id), */}
      {/*     }} */}
      {/*   /> */}
      {/* </Grid> */}
    </>
  );
};

export default CategoryFields;
