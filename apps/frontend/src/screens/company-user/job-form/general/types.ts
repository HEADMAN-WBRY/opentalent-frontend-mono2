import { JobLocationTypeEnum, SourceTypeEnum } from '@libs/graphql-types';
import { CampaignStatus } from '@libs/ui/components/job/utils';

export interface GeneralProps {
  status: CampaignStatus;
  remoteIsOption?: boolean;
  locationType?: JobLocationTypeEnum;
  onlyOpenTalentPool?: boolean;
  talentPool?: SourceTypeEnum[];
}
