import React from 'react';

import {
  JobLocationTypeEnum,
  JobTypeEnum,
  SourceTypeEnum,
} from '@libs/graphql-types';
import { CampaignStatus } from '@libs/ui/components/job/utils';

import FreelanceJobGeneralFields from './FreelanceJobGeneralFields';
import PermanentJobGeneralFields from './PermanentJobGeneralFields';
import ProjectJobGeneralFields from './ProjectJobGeneralFields';

const FORMS_MAP = {
  [JobTypeEnum.Freelance]: FreelanceJobGeneralFields,
  [JobTypeEnum.Permanent]: PermanentJobGeneralFields,
  [JobTypeEnum.Project]: ProjectJobGeneralFields,
};

const General = ({
  status,
  jobType,
  locationType,
  remoteIsOption,
  talentPool,
}: {
  status: CampaignStatus;
  jobType: JobTypeEnum;
  remoteIsOption: boolean;
  locationType?: JobLocationTypeEnum;
  talentPool?: SourceTypeEnum[];
}) => {
  const Form = FORMS_MAP[jobType];

  return (
    <Form
      status={status}
      locationType={locationType}
      remoteIsOption={remoteIsOption}
      talentPool={talentPool}
    />
  );
};

export default General;
