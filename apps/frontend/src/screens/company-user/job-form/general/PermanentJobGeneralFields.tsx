import { addDays } from 'date-fns';
import React from 'react';

import { Grid } from '@mui/material';

import { JobLocationTypeEnum } from '@libs/graphql-types';
import { COUNTRY_OPTIONS } from '@libs/helpers/consts/countries';
import { modelPath } from '@libs/helpers/form';
import { ConnectedCheckbox } from '@libs/ui/components/form/checkbox';
import { ConnectedDatePicker } from '@libs/ui/components/form/datepicker';
import { ConnectedSelect } from '@libs/ui/components/form/select';
import { ConnectedTextEditor } from '@libs/ui/components/form/text-editor';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import { CampaignStatus } from '@libs/ui/components/job/utils';
import { LOCATION_TYPE_OPTIONS } from '@libs/ui/components/job/utils/consts';
import StepSection from '@libs/ui/components/step-section';

import { CreateJobForm } from '../types';
import { getFieldActivity } from '../utils';
import CategoryFields from './CategoryFields';
import MinDaysPerWeekSelector from './MinDaysPerWeekSelector';
import { GeneralProps } from './types';

const PermanentJobGeneralFields = ({ status, locationType }: GeneralProps) => {
  const isRemote = locationType === JobLocationTypeEnum.Remote;
  const isOnSite = locationType === JobLocationTypeEnum.OnSite;
  const isHybrid = locationType === JobLocationTypeEnum.Hybrid;

  const showCountry = !isRemote;

  return (
    <StepSection index={1} title="General information">
      <Grid spacing={4} direction="column" container>
        <CategoryFields status={status} />
        <Grid item>
          <ConnectedTextField
            name="name"
            fullWidth
            variant="filled"
            label="Title (e.g. UX designer, PHP developer)"
            disabled={getFieldActivity({
              status,
              blockStatuses: [CampaignStatus.Finished],
            })}
          />
        </Grid>
        <Grid item>
          <ConnectedTextEditor
            disabled={getFieldActivity({
              status,
              blockStatuses: [CampaignStatus.Finished],
            })}
            placeholder="Description"
            name="description"
          />
        </Grid>
        <Grid item>
          <ConnectedDatePicker
            inputFormat="dd/MM/yyyy"
            name="start_date"
            minDate={addDays(new Date(), 1)}
            TextFieldProps={{
              variant: 'filled',
              fullWidth: true,
              label: 'Starting date',
            }}
            disabled={getFieldActivity({
              status,
              blockStatuses: [CampaignStatus.Finished, CampaignStatus.Started],
            })}
          />
        </Grid>
        <Grid item>
          <Grid spacing={4} container>
            <Grid xs={12} sm={6} item>
              <ConnectedTextField
                name="salary_min"
                fullWidth
                variant="filled"
                label="Min salary"
                InputProps={{ endAdornment: '€/month' }}
                disabled={getFieldActivity({
                  status,
                  blockStatuses: [
                    CampaignStatus.Finished,
                    CampaignStatus.Started,
                  ],
                })}
              />
            </Grid>
            <Grid xs={12} sm={6} item>
              <ConnectedTextField
                name="salary_max"
                fullWidth
                variant="filled"
                label="Max salary"
                InputProps={{ endAdornment: '€/month' }}
                disabled={getFieldActivity({
                  status,
                  blockStatuses: [CampaignStatus.Finished],
                })}
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <ConnectedCheckbox
            name="is_rate_negotiable"
            label="Salary is negotiable"
            disabled={getFieldActivity({
              status,
              blockStatuses: [CampaignStatus.Finished],
            })}
          />
        </Grid>
        {/* {isEnterprise && ( */}
        {/*   <Grid item> */}
        {/*     <ConnectedSelect */}
        {/*       name="campaign_talent_pool" */}
        {/*       variant="filled" */}
        {/*       hideNoneValue */}
        {/*       label="Select talent pool" */}
        {/*       options={companyPoolOptions} */}
        {/*       fullWidth */}
        {/*     /> */}
        {/*   </Grid> */}
        {/* )} */}
        <Grid item>
          <ConnectedSelect
            name="location_type"
            fullWidth
            hideNoneValue
            variant="filled"
            label="Location"
            options={LOCATION_TYPE_OPTIONS}
            disabled={getFieldActivity({
              status,
              blockStatuses: [CampaignStatus.Finished],
            })}
          />
        </Grid>
        {isOnSite && (
          <Grid item>
            <ConnectedCheckbox
              name="is_remote_an_option"
              label="Remote is an option"
              disabled={getFieldActivity({
                status,
                blockStatuses: [CampaignStatus.Finished],
              })}
            />
          </Grid>
        )}
        {showCountry && (
          <Grid item>
            <Grid spacing={4} container>
              <Grid xs={12} sm={6} item>
                <Grid item>
                  <ConnectedSelect
                    name="country"
                    fullWidth
                    hideNoneValue
                    variant="filled"
                    label="Country"
                    options={COUNTRY_OPTIONS}
                    disabled={getFieldActivity({
                      status,
                      blockStatuses: [CampaignStatus.Finished],
                    })}
                  />
                </Grid>
              </Grid>
              <Grid xs={12} sm={6} item>
                <Grid item>
                  <ConnectedTextField
                    name="city"
                    fullWidth
                    variant="filled"
                    label="City"
                    disabled={getFieldActivity({
                      status,
                      blockStatuses: [CampaignStatus.Finished],
                    })}
                  />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        )}

        {isHybrid && (
          <Grid item>
            <MinDaysPerWeekSelector
              name={modelPath<CreateJobForm>((m) => m.office_hours_per_month)}
              disabled={getFieldActivity({
                status,
                blockStatuses: [CampaignStatus.Finished],
              })}
            />
          </Grid>
        )}

        <Grid item>
          <ConnectedTextField
            name="client"
            fullWidth
            variant="filled"
            label="Client"
            disabled={getFieldActivity({
              status,
              blockStatuses: [CampaignStatus.Finished],
            })}
          />
        </Grid>
      </Grid>
    </StepSection>
  );
};

export default PermanentJobGeneralFields;
