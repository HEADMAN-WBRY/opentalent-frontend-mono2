import {
  CreateSkillModal,
  SuggestSkillAction,
} from 'components/custom/skill-creation';
import {
  useCreationSkillDialog,
  useHandleSkillChange,
} from 'components/custom/skill-creation/hooks';
import { BooleanControl } from 'components/custom/skills-boolean-search';
import { INPUT_SUGGESTION_LENGTH } from 'consts/skills';
import React from 'react';

import { Box, Grid } from '@mui/material';

import { Skill, SkillTypeEnum } from '@libs/graphql-types';
import { modelPath } from '@libs/helpers/form';
import { SkillChip } from '@libs/ui/components/chip';
import { ConnectedMultipleSelect } from '@libs/ui/components/form/multiple-select';
import { OptionType } from '@libs/ui/components/form/select';
import { CampaignStatus } from '@libs/ui/components/job/utils';
import StepSection from '@libs/ui/components/step-section';
import Typography from '@libs/ui/components/typography';

import { CreateJobForm } from '../types';
import { getFieldActivity } from '../utils';
import SearchToggleButton from './SearchToggleButton';
import { useAddSkill } from './hooks';

interface SkillsProps {
  status: CampaignStatus;
  isBooleanSearch: boolean;
}

const SKILLS_PLACEHOLDERS = {
  [SkillTypeEnum.HardSkills]:
    'e.g. Web Development, Data Visualization, 3D Modeling',
  [SkillTypeEnum.Solutions]: 'e.g. HTML5, AWS Cloud, Figma, .NET',
  [SkillTypeEnum.SoftSkills]: 'e.g. Communication, Leadership, Problem-solving',
};

export const renderTags = (value: OptionType[], getTagProps: any) =>
  value.map((option: OptionType, index: number) => (
    <SkillChip
      {...getTagProps({ index })}
      size="small"
      skill={{ ...value[index], name: option.text } as unknown as Skill}
      key={`id-${option.value}-${index + 1}`}
      label={option.text}
    />
  ));

const Skills = ({ status, isBooleanSearch }: SkillsProps) => {
  const {
    onInputBlur,
    onInputFocus,
    onInputChange,
    skillsSuggest,
    onSelectSkill,
    inputValue = '',
    loading,
    activeSkillType,
  } = useAddSkill();
  const showEmptyOptionText =
    inputValue.length > INPUT_SUGGESTION_LENGTH && !skillsSuggest?.length;

  const addSkill = useHandleSkillChange();
  const { isOpen, closeDialog, openDialog, onCreation, initialValues } =
    useCreationSkillDialog();

  const isFieldsDisabled = getFieldActivity({
    status,
    blockStatuses: [CampaignStatus.Finished],
  });

  return (
    <StepSection index={2} title="Key Skills">
      {isBooleanSearch ? (
        <BooleanControl disabled={isFieldsDisabled} />
      ) : (
        <Grid spacing={4} direction="column" container>
          <Grid item>
            <Typography variant="body2">
              Which skills are <b>mandatory</b> for this role?
            </Typography>
          </Grid>
          <Grid item>
            <ConnectedMultipleSelect
              options={skillsSuggest}
              name="required_skills"
              multiple
              noOptionsText={
                showEmptyOptionText ? (
                  <SuggestSkillAction
                    onCreate={() =>
                      openDialog(
                        {
                          name: inputValue,
                          skill_type: activeSkillType?.[0] as SkillTypeEnum,
                        },
                        addSkill('required_skills'),
                      )
                    }
                  />
                ) : (
                  SKILLS_PLACEHOLDERS[SkillTypeEnum.HardSkills]
                )
              }
              disabled={getFieldActivity({
                status,
                blockStatuses: [
                  CampaignStatus.Finished,
                  CampaignStatus.Started,
                ],
              })}
              renderTags={renderTags}
              autoCompleteProps={{
                onChange: onSelectSkill,
                loading,
                popupIcon: null,
                filterSelectedOptions: true,
                disabled: isFieldsDisabled,
              }}
              inputProps={{
                variant: 'filled',
                label: 'Select from list',
                onFocus: onInputFocus([
                  SkillTypeEnum.HardSkills,
                  SkillTypeEnum.Solutions,
                ]),
                onBlur: onInputBlur,
                onChange: onInputChange,
                name: 'required_skills',
              }}
            />
          </Grid>
          <Grid item>
            <Typography variant="body2">
              Which skills are <b>nice-to-have</b> for this role?
            </Typography>
          </Grid>
          <Grid item>
            <ConnectedMultipleSelect
              options={skillsSuggest}
              name="skills"
              loading={loading}
              noOptionsText={
                showEmptyOptionText ? (
                  <SuggestSkillAction
                    onCreate={() =>
                      openDialog(
                        {
                          name: inputValue,
                          skill_type: activeSkillType?.[0] as SkillTypeEnum,
                        },
                        addSkill('skills'),
                      )
                    }
                  />
                ) : (
                  SKILLS_PLACEHOLDERS[SkillTypeEnum.Solutions]
                )
              }
              chipProps={{
                size: 'small',
              }}
              renderTags={renderTags}
              autoCompleteProps={{
                onChange: onSelectSkill,
                loading,
                filterSelectedOptions: true,
                popupIcon: null,
                disabled: isFieldsDisabled,
              }}
              inputProps={{
                variant: 'filled',
                label: 'Select from list',
                onFocus: onInputFocus([
                  SkillTypeEnum.HardSkills,
                  SkillTypeEnum.Solutions,
                  SkillTypeEnum.SoftSkills,
                ]),
                onBlur: onInputBlur,
                onChange: onInputChange,
                name: 'skills',
              }}
            />
          </Grid>
        </Grid>
      )}

      <Box pt={4}>
        <SearchToggleButton
          name={modelPath<CreateJobForm>((m) => m.is_boolean_search)}
          disabled={isFieldsDisabled}
        />
      </Box>
      <CreateSkillModal
        close={closeDialog}
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        initialValues={initialValues!}
        isOpen={isOpen}
        onSubmit={onCreation}
      />
    </StepSection>
  );
};

export default Skills;
