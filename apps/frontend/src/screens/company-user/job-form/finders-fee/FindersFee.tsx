import { useMemo } from 'react';

import { Grid } from '@mui/material';

import { SourceTypeEnum } from '@libs/graphql-types';
import { getRanges } from '@libs/helpers/common';
import { formatCurrency } from '@libs/helpers/format';
import { ConnectedSelect } from '@libs/ui/components/form/select';
import { CampaignStatus } from '@libs/ui/components/job/utils';
import StepSection from '@libs/ui/components/step-section';
import Typography, { OuterLink } from '@libs/ui/components/typography';

import { getFieldActivity } from '../utils';

interface FindersFeeProps {
  index?: number;
  status: CampaignStatus;
  currentValue: number;
  talentPool?: SourceTypeEnum[];
}

const OPTIONS = getRanges(1000, 15000).map((i) => ({
  text: formatCurrency(i),
  value: i,
}));
const NONE_VALUE = {
  text: 'None',
  value: NaN as any,
};

const FindersFee = ({
  index = 4,
  status,
  currentValue = 0,
}: FindersFeeProps) => {
  const finalOptions = useMemo(() => {
    return status === CampaignStatus.Started
      ? [
        { ...NONE_VALUE, disabled: true },
        ...OPTIONS.map((o) => ({
          ...o,
          disabled: o.value < currentValue ? true : false,
        })),
      ]
      : [NONE_VALUE, ...OPTIONS];
  }, [currentValue, status]);

  return (
    <StepSection index={index} title="Add Finder’s Fee - optional">
      <Grid spacing={4} direction="column" container>
        <Grid item>
          <Typography variant="body2">
            How much would you pay someone for recommending the perfect
            candidate for this role? Add a Finder’s Fee.{' '}
            <OuterLink
              variant="body2"
              href="https://www.notion.so/opentalent/Finder-s-Fees-explained-0b24c6d75e9f4e938d07c48b7714217a"
            >
              Learn more &gt;
            </OuterLink>
          </Typography>
        </Grid>
        <Grid item>
          <ConnectedSelect
            name="finders_fee"
            options={finalOptions}
            fullWidth
            hideNoneValue
            size="small"
            variant="filled"
            label="One-time finder’s fee amount"
            disabled={getFieldActivity({
              status,
              blockStatuses: [CampaignStatus.Finished],
            })}
          />
        </Grid>
      </Grid>
    </StepSection>
  );
};

export default FindersFee;
