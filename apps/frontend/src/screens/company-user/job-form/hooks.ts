import { useMutation, useQuery } from '@apollo/client';
import { useCurrentUser } from 'hooks/auth';
import { useIsEnterpriseCompanyAccount } from 'hooks/company/useCompanyAccountType';
import { useSearchParams } from 'hooks/routing';
import { useSnackbar } from 'notistack';
import { useCallback, useMemo } from 'react';
import { useHistory } from 'react-router-dom';
import { pathManager } from 'routes';

import {
  Job,
  JobServiceTypeEnum,
  JobTypeEnum,
  MutationCreateJobArgs,
  MutationUpdateJobArgs,
  Query,
  QueryJobArgs,
  SourceTypeEnum,
} from '@libs/graphql-types';

import {
  CREATE_NEW_JOB,
  GET_NEW_JOB_DATA,
  GET_EDIT_JOB_DATA,
  UPDATE_JOB,
} from './queries';
import { CreateJobForm } from './types';
import { getInitialValues, mapJobToServer } from './utils';

interface HookParams {
  jobId?: string;
  isDuplicate: boolean;
}

export const useSubmitHandler = ({ jobId, isDuplicate }: HookParams) => {
  const { enqueueSnackbar } = useSnackbar();
  const history = useHistory();
  const [action, { loading }] = useMutation<
    any,
    MutationCreateJobArgs | MutationUpdateJobArgs
  >(jobId && !isDuplicate ? UPDATE_JOB : CREATE_NEW_JOB, {
    onCompleted: ({ createJob }) => {
      enqueueSnackbar(
        `Job successfully ${
        // eslint-disable-next-line no-nested-ternary
        isDuplicate ? 'duplicated' : jobId ? 'updated' : 'created'
        }`,
        {
          variant: 'success',
        },
      );

      history.push(
        pathManager.company.job.generatePath({
          id: createJob || jobId,
        }),
      );
    },
  });

  const mapForm = useCallback(
    (jobForm: CreateJobForm) => {
      const mappedValues = mapJobToServer(jobForm);
      return {
        variables: {
          ...mappedValues,
          ...(jobId ? { id: jobId } : {}),
        } as any,
        ...(jobId
          ? {
            refetchQueries: [
              { query: GET_EDIT_JOB_DATA, variables: { id: jobId } },
            ],
          }
          : {}),
      };
    },
    [jobId],
  );

  const onSubmit = useCallback(
    (jobForm: CreateJobForm) => {
      action(mapForm(jobForm));
    },
    [action, mapForm],
  );

  return { onSubmit, loading };
};

export const useScreenData = ({ jobId }: HookParams) => {
  const query = jobId ? GET_EDIT_JOB_DATA : GET_NEW_JOB_DATA;
  const { data, loading } = useQuery<Query, QueryJobArgs>(query, {
    variables: { id: jobId || '' },
  });

  return { data, loading };
};

const useSearchQueryValues = ({ isEnterprise }: { isEnterprise: boolean }) => {
  const { otCommunity, selfCommunity, serviceType, jobType } =
    useSearchParams();
  const pools = Object.entries({
    [SourceTypeEnum.Own]: selfCommunity || isEnterprise,
    [SourceTypeEnum.Opentalent]: otCommunity,
  })
    .filter(([_, value]) => !!value)
    .map(([key]) => key);

  return {
    service_type: serviceType as JobServiceTypeEnum,
    campaign_talent_pool: pools as unknown as SourceTypeEnum[],
    ...(jobType && { type: jobType as JobTypeEnum }),
  };
};

export const useInitialValues = ({
  job,
  isCreate,
  isDuplicate,
  jobType,
}: {
  job?: Job;
  isCreate: boolean;
  isDuplicate: boolean;
  jobType: JobTypeEnum;
}) => {
  const isEnterprise = useIsEnterpriseCompanyAccount();
  const talentPoolOptions = useTalentPoolOptions();
  const { data } = useCurrentUser();
  const searchData = useSearchQueryValues({ isEnterprise });
  const initials = getInitialValues({
    isEnterprise,
    job,
    talentPoolOptions,
    isDuplicate,
  });

  return {
    is_draft: false,
    ...initials,
    type: jobType,
    ...(isCreate && searchData),
    ...(isCreate &&
      data?.currentCompanyUser && {
      client: data?.currentCompanyUser?.company?.name,
      campaign_owner_id:
        data?.currentCompanyUser?.id || initials?.campaign_owner_id,
    }),
  };
};

export const useTalentPoolOptions = () => {
  const { data } = useCurrentUser();
  const companyName = data?.currentCompanyUser?.company?.name;

  return useMemo(
    () => [
      { text: `My Community (${companyName})`, value: SourceTypeEnum.Own },
      { text: 'OpenTalent network', value: SourceTypeEnum.Opentalent },
    ],
    [companyName],
  );
};
