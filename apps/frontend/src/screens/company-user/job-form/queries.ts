import { gql } from '@apollo/client';
import FULL_JOB_FRAGMENT from 'graphql/fragments/companyUser/jobFragment';

export const GET_NEW_JOB_DATA = gql`
  query GetEditJobData {
    currentCompanyUserColleagues {
      id
      first_name
      last_name
    }
  }
`;

export const GET_EDIT_JOB_DATA = gql`
  ${FULL_JOB_FRAGMENT}
  query GetEditJobData($id: ID!) {
    job(id: $id) {
      ...FullJob
    }
    currentCompanyUserColleagues {
      id
      first_name
      last_name
    }
  }
`;

export const CREATE_NEW_JOB = gql`
  mutation CreateNewJob(
    $name: String!
    $category_id: ID
    $description: String!
    $pitch: String
    $start_date: DateTimeUtc
    $end_date: DateTimeUtc
    $rate_min: Float
    $rate_max: Float
    $is_rate_negotiable: Boolean
    $salary_min: Float
    $salary_max: Float
    $is_salary_negotiable: Boolean
    $max_project_budget: Float
    $finders_fee: Float
    $campaign_owner_id: ID!
    $campaign_start_date: DateTimeUtc!
    $campaign_end_date: DateTimeUtc!
    $campaign_talent_pool: [SourceTypeEnum]
    $is_archived: Boolean
    $skills: [ID]
    $skills_boolean_filter: TalentsBooleanSkillsFilterInput
    $solutions_required: [ID]
    $hard_skills_required: [ID]
    $hours_per_week: Int
    $location_type: JobLocationTypeEnum
    $type: JobTypeEnum
    $location: String
    $country: String
    $city: String
    $client: String
    $is_remote_an_option: Boolean
    $is_draft: Boolean
    $office_hours_per_month: Float
    $service_type: JobServiceTypeEnum
    $propose_to_matchers: Boolean
  ) {
    createJob(
      office_hours_per_month: $office_hours_per_month
      propose_to_matchers: $propose_to_matchers
      is_draft: $is_draft
      service_type: $service_type
      name: $name
      category_id: $category_id
      description: $description
      pitch: $pitch
      start_date: $start_date
      end_date: $end_date
      rate_min: $rate_min
      rate_max: $rate_max
      is_rate_negotiable: $is_rate_negotiable
      salary_min: $salary_min
      salary_max: $salary_max
      is_salary_negotiable: $is_salary_negotiable
      max_project_budget: $max_project_budget
      finders_fee: $finders_fee
      campaign_owner_id: $campaign_owner_id
      campaign_start_date: $campaign_start_date
      campaign_end_date: $campaign_end_date
      campaign_talent_pool: $campaign_talent_pool
      is_archived: $is_archived
      skills: $skills
      skills_boolean_filter: $skills_boolean_filter
      is_remote_an_option: $is_remote_an_option
      solutions_required: $solutions_required
      hard_skills_required: $hard_skills_required
      hours_per_week: $hours_per_week
      location_type: $location_type
      type: $type
      location: $location
      country: $country
      city: $city
      client: $client
    )
  }
`;

export const UPDATE_JOB = gql`
  mutation UpdateJob(
    $id: ID!
    $service_type: JobServiceTypeEnum
    $name: String!
    $category_id: ID
    $description: String!
    $pitch: String
    $start_date: DateTimeUtc
    $end_date: DateTimeUtc
    $rate_min: Float
    $rate_max: Float
    $is_rate_negotiable: Boolean
    $salary_min: Float
    $salary_max: Float
    $is_salary_negotiable: Boolean
    $max_project_budget: Float
    $finders_fee: Float
    $campaign_owner_id: ID!
    $campaign_start_date: DateTimeUtc
    $campaign_end_date: DateTimeUtc
    $campaign_talent_pool: [SourceTypeEnum]
    $skills_boolean_filter: TalentsBooleanSkillsFilterInput
    $is_archived: Boolean
    $skills: [ID]
    $solutions_required: [ID]
    $hard_skills_required: [ID]
    $hours_per_week: Int
    $location_type: JobLocationTypeEnum
    $type: JobTypeEnum
    $location: String
    $country: String
    $city: String
    $client: String
    $is_remote_an_option: Boolean
    $is_draft: Boolean
    $office_hours_per_month: Float
    $update_auto_matching: Boolean
  ) {
    updateJob(
      update_auto_matching: $update_auto_matching
      service_type: $service_type
      skills_boolean_filter: $skills_boolean_filter
      office_hours_per_month: $office_hours_per_month
      is_draft: $is_draft
      id: $id
      name: $name
      category_id: $category_id
      description: $description
      pitch: $pitch
      start_date: $start_date
      end_date: $end_date
      rate_min: $rate_min
      rate_max: $rate_max
      is_remote_an_option: $is_remote_an_option
      is_rate_negotiable: $is_rate_negotiable
      salary_min: $salary_min
      salary_max: $salary_max
      is_salary_negotiable: $is_salary_negotiable
      max_project_budget: $max_project_budget
      finders_fee: $finders_fee
      campaign_owner_id: $campaign_owner_id
      campaign_start_date: $campaign_start_date
      campaign_end_date: $campaign_end_date
      campaign_talent_pool: $campaign_talent_pool
      is_archived: $is_archived
      skills: $skills
      solutions_required: $solutions_required
      hard_skills_required: $hard_skills_required
      hours_per_week: $hours_per_week
      location_type: $location_type
      type: $type
      location: $location
      country: $country
      city: $city
      client: $client
    ) {
      id
      is_draft
    }
  }
`;
