import cn from 'classnames';
import React from 'react';

import { Box, Grid, Paper } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { JobTypeEnum } from '@libs/graphql-types';
import Checkbox from '@libs/ui/components/form/checkbox';
import Typography from '@libs/ui/components/typography';

interface OptionBoxProps {
  title: string;
  subtitle: string;
  info: string;
  isActive: boolean;
  type: JobTypeEnum;
  isBeta?: boolean;
  onClick: VoidFunction;
  disabled?: boolean;
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: 300,
    padding: theme.spacing(4),
    cursor: 'pointer',
    position: 'relative',
    height: '100%',
    textAlign: 'center',
    transition: 'border .3s',
    border: ({ isActive }: OptionBoxProps) =>
      isActive
        ? `2px solid ${theme.palette.success.main}`
        : `2px solid transparent`,
  },
  container: {
    padding: theme.spacing(8, 6),
  },
  checkboxChecked: {
    color: `${theme.palette.success.main} !important`,
  },
  checkboxLabel: {
    margin: 0,
  },
  betaBadge: {
    padding: '6px 12px',
    borderRadius: '24px',
    position: 'absolute',
    right: '20px',
    top: '20px',
    color: `${theme.palette.text.secondary}`,
    backgroundColor: `${theme.palette.other.green}`,
    fontWeight: 'bold',
    textTransform: 'uppercase',
  },
  rootDisabled: {
    pointerEvents: 'none',

    '& $container': {
      opacity: 0.4,
    },
  },
}));

const OptionBox = (props: OptionBoxProps) => {
  const classes = useStyles(props);
  const { title, isActive, type, onClick, disabled } = props;

  return (
    <Paper
      onClick={onClick}
      elevation={0}
      classes={{ root: cn(classes.root, { [classes.rootDisabled]: disabled }) }}
    >
      {disabled && <Box className={classes.betaBadge}>Coming soon!</Box>}
      {/* {isBeta && <Box className={classes.betaBadge}>BETA!</Box>} */}
      <Grid container alignItems="center" spacing={2}>
        <Grid item>
          <Checkbox
            checked={isActive}
            formControlLabelProps={{ classes: { root: classes.checkboxLabel } }}
            classes={{ checked: classes.checkboxChecked }}
            name={type}
          />
        </Grid>
        <Grid item>
          <Typography variant="body1">{title}</Typography>
        </Grid>
      </Grid>
    </Paper>
  );
};

export default OptionBox;
