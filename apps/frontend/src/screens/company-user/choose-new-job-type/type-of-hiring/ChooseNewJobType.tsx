import { ConnectedPageLayout } from 'components/layout/page-layout';
import useMediaQueries from 'hooks/common/useMediaQueries';
import React from 'react';

import { Box } from '@mui/material';

import Typography from '@libs/ui/components/typography';

import { DEDICATED_SEARCH_STEPS } from '../consts';
import TopSteps from '../shared/TopSteps';
import OptionsForm from './OptionsForm';
import { PageProps } from './types';

const ChooseNewJobType = (props: PageProps) => {
  const { isMD } = useMediaQueries();

  return (
    <ConnectedPageLayout
      documentTitle="Post a Job > Step 1"
      headerProps={{ accountProps: {} }}
      drawerProps={{}}
    >
      <Box mb={6}>
        <Typography variant="h5">Post a Job &gt; Step 1</Typography>
      </Box>
      <Box mb={10} maxWidth={500}>
        <TopSteps
          onlyActiveLabel={isMD}
          activeIndex={0}
          steps={DEDICATED_SEARCH_STEPS}
        />
      </Box>
      <Box mb={6}>
        <Typography variant="subtitle1">I'm looking for a...</Typography>
      </Box>
      <OptionsForm />
    </ConnectedPageLayout>
  );
};

export default ChooseNewJobType;
