import { usePushWithQuery } from 'hooks/routing';
import React, { useState } from 'react';
import { pathManager } from 'routes';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { JobTypeEnum } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';

import OptionBox from './OptionBox';

interface OptionsFormProps { }

const OPTIONS = {
  [JobTypeEnum.Freelance]: {
    title: 'Freelancer',
    subtitle: 'Hourly rate',
    info: 'Let our community of specialist recruiters fulfil your immediate ‘high- skilled’ freelancer needs.',
  },
  [JobTypeEnum.Permanent]: {
    title: 'Permanent Hire',
    subtitle: 'Salary based',
    info: 'Let our community of specialist recruiters find your next best permanent hire.',
    isBeta: true,
    disabled: false,
  },
  // [JobTypeEnum.Project]: {
  //   title: 'Project',
  //   subtitle: 'Fixed budget',
  //   info: 'Get matched to Talent Collectives and Agencies from OpenTalent’s exclusive European network.',
  // },
};

const useStyles = makeStyles((theme) => ({
  root: {
    textAlign: 'center',

    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
    },
  },
}));

const OptionsForm = (props: OptionsFormProps) => {
  const classes = useStyles();
  const push = usePushWithQuery();
  const [activeOption, setActiveOption] = useState<JobTypeEnum | null>(null);
  const onSubmit = () => {
    push({
      pathname: pathManager.company.newJob.form.generatePath(),
      query: { jobType: activeOption },
    });
  };

  return (
    <>
      <Grid
        flexDirection="column"
        className={classes.root}
        wrap="nowrap"
        container
        spacing={4}
      >
        {[JobTypeEnum.Permanent, JobTypeEnum.Freelance].map((type) => {
          const optionProps = OPTIONS[type];

          return (
            <Grid key={type} item>
              <OptionBox
                {...optionProps}
                type={type as JobTypeEnum}
                isActive={type === activeOption}
                onClick={() => setActiveOption(type as JobTypeEnum)}
              />
            </Grid>
          );
        })}
      </Grid>
      <Box pt={6}>
        <Button
          disabled={!activeOption}
          size="large"
          variant="contained"
          color="primary"
          onClick={onSubmit}
          data-test-id="submit-job-type"
        >
          Continue
        </Button>
      </Box>
    </>
  );
};

export default OptionsForm;
