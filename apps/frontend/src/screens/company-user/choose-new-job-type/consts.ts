import { JobServiceTypeEnum } from '@libs/graphql-types';

export const DEDICATED_SEARCH_STEPS = [
  'Select type of job',
  'Set up job campaign',
];

export const DEFAULT_STEPS = ['Select Type of Service', 'Set up job campaign'];

export const STEPS_ITEMS = {
  [JobServiceTypeEnum.DedicatedSearch]: [
    'Select Type of Service',
    'Select type of hire',
    'Set up job campaign',
  ],
  [JobServiceTypeEnum.SelfService]: [
    'Select Type of Service',
    'Set up job campaign',
  ],
};
