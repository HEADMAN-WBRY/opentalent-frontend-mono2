import { getIn, useFormikContext } from 'formik';
import { useIsFreeCompanyAccount } from 'hooks/company/useCompanyAccountType';
import { usePushWithQuery } from 'hooks/routing';
import { useCallback } from 'react';
import { pathManager } from 'routes/consts';

import { JobServiceTypeEnum } from '@libs/graphql-types';
import { FormikSubmit, modelPath } from '@libs/helpers/form';

import { useCurrentUser } from './../../../../hooks/auth/useCurrentUser';
import { FormState } from './types';

export const useHasOTAccess = () => {
  const hasAccess = !useIsFreeCompanyAccount();

  return hasAccess;
};

export const useBoxClick = () => {
  const hasAccess = useHasOTAccess();
  const { setFieldValue, values, validateField } = useFormikContext();
  const typePath = modelPath<FormState>((m) => m.serviceType);
  const value = getIn(values, typePath);
  const onChange = useCallback(
    (type: JobServiceTypeEnum) => {
      setFieldValue(typePath, type, false);

      const selfCommunityPath = modelPath<FormState>((m) => m.selfCommunity);
      const otCommunityPath = modelPath<FormState>((m) => m.otCommunity);

      if (type === JobServiceTypeEnum.SelfService) {
        setFieldValue(selfCommunityPath, true, true);
        if (hasAccess) {
          setFieldValue(otCommunityPath, true, true);
        }
      } else {
        setFieldValue(selfCommunityPath, false, true);
        setFieldValue(otCommunityPath, false, true);
      }

      setTimeout(() => {
        validateField(typePath);
        validateField(selfCommunityPath);
        validateField(otCommunityPath);
      }, 200);
    },
    [hasAccess, validateField, setFieldValue, typePath],
  );

  return { value, onChange };
};

const ROUTES = {
  [JobServiceTypeEnum.SelfService]:
    pathManager.company.newJob.type.generatePath(),
  // pathManager.company.newJob.form.generatePath(),
  [JobServiceTypeEnum.DedicatedSearch]:
    pathManager.company.newJob.type.generatePath(),
};

export const useSubmitAction = () => {
  const push = usePushWithQuery();
  const onSubmit: FormikSubmit<FormState> = (values) => {
    const { serviceType } = values;
    const pathname = ROUTES[serviceType];

    push({ pathname, query: values });
  };

  return onSubmit;
};
