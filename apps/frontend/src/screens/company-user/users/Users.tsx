import { ConnectedPageLayout } from 'components/layout/page-layout';
import React from 'react';
import { RouteComponentProps, useHistory } from 'react-router-dom';

import {
  MutationCreateCompanyUserProfileArgs,
  User,
} from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import { USERS_ROUTE } from '../settings/consts';
import PersonalForm, { useUpdateOrCreateUser } from '../settings/personal-form';
import { useColleagues } from '../settings/users-tab/hooks';

interface UsersProps extends RouteComponentProps<{ id?: string }> {}

const DEFAULT_VALUES: MutationCreateCompanyUserProfileArgs = {
  first_name: '',
  last_name: '',
  email: '',
};
const getUserInitialValues = (colleagues: User[], id: string) => {
  const user = colleagues.find((i) => i.id === id);

  if (!user) {
    return DEFAULT_VALUES;
  }

  return {
    ...DEFAULT_VALUES,
    first_name: user.first_name || '',
    last_name: user.last_name || '',
    position: user.position || '',
    email: user.email || '',
    company_user_id: user.id,
    avatar: user?.avatar?.avatar,
  };
};

const Users = ({ match }: UsersProps) => {
  const userId = match.params.id;
  const history = useHistory();
  const isCreate = userId === 'create';
  const goToUsers = () => history.push(USERS_ROUTE);
  const { updateRequest, loading: requestLoading } = useUpdateOrCreateUser({
    isCreate,
    onSuccess: goToUsers,
  });
  const { isLoading: colleaguesLoading, colleagues } = useColleagues();
  const initialValues = !userId
    ? DEFAULT_VALUES
    : getUserInitialValues(colleagues, userId);

  return (
    <ConnectedPageLayout
      drawerProps={{}}
      documentTitle="Settings"
      isLoading={colleaguesLoading}
    >
      <Typography variant="h5" paragraph>
        {isCreate ? 'Add new user' : 'Company Account Settings'}
      </Typography>

      <PersonalForm
        isCreate={isCreate}
        onSubmit={updateRequest}
        isLoading={requestLoading}
        initialValues={initialValues}
        onCancel={goToUsers}
        initialAvatar={initialValues?.avatar}
      />
    </ConnectedPageLayout>
  );
};

export default Users;
