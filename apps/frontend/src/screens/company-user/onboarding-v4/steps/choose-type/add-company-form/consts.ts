import { JoinTypeEnum, TypeCardProps } from './types';

export const CARDS_DATA: Pick<TypeCardProps, 'title' | 'text' | 'value'>[] = [
  {
    title: 'Independent',
    text: 'I recruit for myself (independent recruiter)',
    value: JoinTypeEnum.Independent,
  },
  {
    title: 'Agency',
    text: 'I recruit for a recruiting agency / consultancy',
    value: JoinTypeEnum.Agency,
  },
  {
    title: 'Company',
    text: 'I recruit as part of an in-house team within a company',
    value: JoinTypeEnum.Company,
  },
];
