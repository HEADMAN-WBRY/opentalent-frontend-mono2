import { gql } from '@apollo/client';

export const SUGGEST_COMPANY = gql`
  query SuggestCompany($search: String, $first: Int = 10) {
    companies(search: $search, first: $first) {
      data {
        id
        name
      }
    }
  }
`;

export const GET_COMPANY_TALENTS = gql`
  query GetCompanyTalent(
    $worked_in_company_id: ID
    $worked_in_company_str: String
  ) {
    talentsSearch(
      worked_in_company_str: $worked_in_company_str
      worked_in_company_id: $worked_in_company_id
    ) {
      custom_paginator_info {
        total
      }
    }
  }
`;
