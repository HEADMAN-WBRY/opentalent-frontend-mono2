import { OptionType } from '@libs/ui/components/form/select';

export interface OnboardingV4State {
  general: {
    firstName: string;
    lastName: string;
    email: string;
  };
  company: {
    existingCompany?: OptionType;
    newCompanyName?: string;
  };
}
