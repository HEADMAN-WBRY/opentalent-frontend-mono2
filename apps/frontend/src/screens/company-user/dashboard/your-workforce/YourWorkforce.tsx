import { useLazyQuery } from '@apollo/client';
import { SEND_TALENTS_REPORT } from 'graphql/user';
import { useAuth0 } from 'hooks/auth/useAuth0';
import { useConfirm } from 'material-ui-confirm';
import { useSnackbar } from 'notistack';
import React, { useCallback } from 'react';

import { Description } from '@mui/icons-material';
import { Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { DashboardAnalyticsItem, Maybe, Query } from '@libs/graphql-types';
import { isNil } from '@libs/helpers/common';
import { formatCurrency, formatNumber } from '@libs/helpers/format';
import Button from '@libs/ui/components/button';

import DataBlock from '../data-block';

interface YourWorkforceProps {
  data: Maybe<DashboardAnalyticsItem>[];
  pendingTalents: number;
}

const getValue = ({ value, slug }: DashboardAnalyticsItem) => {
  const num = isNil(value) ? 0 : +value?.toFixed(2);

  if (slug === 'average_rate') {
    return formatCurrency(num);
  }
  return formatNumber(num);
};

const useStyles = makeStyles((theme) => ({
  button: {
    color: theme.palette.warning.dark,
    borderColor: theme.palette.warning.dark,
    height: '30px',
    padding: '0 8px',
    marginLeft: 'auto',
  },
}));

const YourWorkforce = ({ data = [], pendingTalents }: YourWorkforceProps) => {
  const confirm = useConfirm();
  const classes = useStyles();
  const {
    user: { email },
  } = useAuth0();
  const { enqueueSnackbar } = useSnackbar();
  const [getReport] = useLazyQuery<Query>(SEND_TALENTS_REPORT, {
    onCompleted: () => {
      enqueueSnackbar(`Report was sent to ${email}`, {
        variant: 'success',
      });
    },
    onError: () => {
      enqueueSnackbar(`Report was not sent`, {
        variant: 'error',
      });
    },
  });
  const onSend = useCallback(async () => {
    await confirm({
      title: `Send report to ${email}`,
      confirmationButtonProps: { autoFocus: true },
      confirmationText: 'Send',
    });
    getReport();
  }, [confirm, email, getReport]);

  return (
    <DataBlock
      fullHeight
      title="My Community"
      button={
        <Button
          variant="outlined"
          onClick={onSend}
          endIcon={<Description />}
          className={classes.button}
        >
          Report
        </Button>
      }
    >
      <Grid spacing={2} container>
        <Grid justifyContent="space-between" container item>
          <Grid item>Pending verification</Grid>
          <Grid item>{pendingTalents}</Grid>
        </Grid>
        {(data as DashboardAnalyticsItem[])
          .filter((i) => i.key !== 'Invites sent')
          .map((item) => (
            <Grid key={item?.key} justifyContent="space-between" container item>
              <Grid item>{item?.key}</Grid>
              <Grid item>{getValue(item)}</Grid>
            </Grid>
          ))}
      </Grid>
    </DataBlock>
  );
};

export default YourWorkforce;
