import { useSnackbar } from 'notistack';

import { useGetDashboardInfoQuery } from '@libs/graphql-types';

export const useDashboardData = () => {
  const { enqueueSnackbar } = useSnackbar();
  const { loading, data } = useGetDashboardInfoQuery({
    fetchPolicy: 'network-only',
    onError: () => enqueueSnackbar('Filed to load data', { variant: 'error' }),
  });

  return { data, loading };
};
