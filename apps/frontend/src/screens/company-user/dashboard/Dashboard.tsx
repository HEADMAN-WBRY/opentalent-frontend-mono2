import { ConnectedPageLayout } from 'components/layout/page-layout';
import useMediaQueries from 'hooks/common/useMediaQueries';
import React from 'react';

import { Grid } from '@mui/material';

import { Talent, useGetPendingTalentsCountQuery } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import Breakdown from './breakdown';
import ByCountry from './by-country';
import { useDashboardData } from './hooks';
import LatestAdditions from './latest-additions';
import YourWorkforce from './your-workforce';

interface DashboardProps { }

const Dashboard = (props: DashboardProps) => {
  const { isSM } = useMediaQueries();
  const { data, loading } = useDashboardData();
  const { data: pendingData } = useGetPendingTalentsCountQuery();

  const pendingTalents =
    pendingData?.talentsSearch?.custom_paginator_info?.total || 0;

  return (
    <ConnectedPageLayout
      documentTitle="Dashboard"
      isLoading={loading}
      drawerProps={{}}
    >
      <Typography variant="h5" paragraph>
        Dashboard
      </Typography>
      <Grid spacing={4} container direction="column">
        <Grid spacing={4} container item>
          <Grid xs={isSM ? 12 : 6} item>
            <YourWorkforce
              data={data?.generalAnalytics || []}
              pendingTalents={pendingTalents}
            />
          </Grid>
          <Grid xs={isSM ? 12 : 6} item>
            <Breakdown
              categories={data?.categoryAnalytics || []}
              tags={data?.tagsAnalytics || []}
            />
          </Grid>
        </Grid>
        <Grid spacing={4} container item>
          <Grid xs={isSM ? 12 : 6} item>
            <ByCountry data={data?.countryAnalytics || []} />
          </Grid>
          <Grid xs={isSM ? 12 : 6} item>
            <LatestAdditions
              lastTalents={(data?.lastTalents as Talent[]) || []}
            />
          </Grid>
        </Grid>
      </Grid>
    </ConnectedPageLayout>
  );
};

export default Dashboard;
