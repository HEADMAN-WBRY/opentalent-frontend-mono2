import React, { useState } from 'react';

import { DashboardAnalyticsItem, Maybe } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import DataBlock from '../data-block';
import BreakdownChart from './BreakdownChart';

interface BreakdownProps {
  categories: Maybe<DashboardAnalyticsItem>[];
  tags: Maybe<DashboardAnalyticsItem>[];
}

const Breakdown = ({ categories, tags }: BreakdownProps) => {
  const [type, setType] = useState<'categories' | 'tags'>('categories');
  const currentData = type === 'categories' ? categories : tags;

  return (
    <DataBlock fullHeight title={`My Community by ${type}`}>
      {type === 'categories' ? (
        <Typography
          pointer
          paragraph
          color="info.main"
          onClick={() => setType('tags')}
        >
          Switch to tags
        </Typography>
      ) : (
        <Typography
          paragraph
          pointer
          color="info.main"
          onClick={() => setType('categories')}
        >
          Switch to categories
        </Typography>
      )}

      <BreakdownChart data={currentData} />
    </DataBlock>
  );
};

export default Breakdown;
