import React, { useMemo } from 'react';
import {
  Bar,
  XAxis,
  YAxis,
  ResponsiveContainer,
  ComposedChart,
  LabelList,
} from 'recharts';

import { Box, colors, useTheme } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { DashboardAnalyticsItem, Maybe } from '@libs/graphql-types';

interface BreakdownChartProps {
  data: Maybe<DashboardAnalyticsItem>[];
}

const useStyles = makeStyles({
  root: {
    '& svg': {
      overflow: 'visible !important',
    },
  },
});

const BreakdownChart = ({ data }: BreakdownChartProps) => {
  const theme = useTheme();
  const height = useMemo(() => data.length * 50, [data]);
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <ResponsiveContainer width="97%" height={height}>
        <ComposedChart
          layout="vertical"
          data={data as DashboardAnalyticsItem[]}
          margin={{ top: 5, right: 5, bottom: 5, left: 5 }}
        >
          <XAxis
            tickLine={false}
            axisLine={false}
            type="number"
            allowDecimals={false}
          />
          <YAxis
            tickLine={false}
            axisLine={false}
            dataKey="key"
            type="category"
            width={200}
            interval={0}
          />
          <Bar
            dataKey="value"
            background={{ fill: colors.grey[100] }}
            barSize={12}
            fill={theme.palette.tertiary.main}
          >
            <LabelList dataKey="value" position="right" />
          </Bar>
        </ComposedChart>
      </ResponsiveContainer>
    </Box>
  );
};

export default BreakdownChart;
