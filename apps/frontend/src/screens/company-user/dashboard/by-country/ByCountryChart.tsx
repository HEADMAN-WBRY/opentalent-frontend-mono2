import React from 'react';
import Chart from 'react-google-charts';

import { useTheme } from '@mui/material';

interface ByCountryChartProps {
  data: (string | number | undefined)[][];
}

const MAP_API_KEY = 'AIzaSyBzllKq0XEZ8KWuZFziX7qJKKkeYzp7wIY';

const ByCountryChart = ({ data }: ByCountryChartProps) => {
  const theme = useTheme();

  return (
    <Chart
      width="100%"
      chartType="GeoChart"
      data={[['Country', 'Count'], ...data]}
      options={{
        region: '150',
        colorAxis: {
          colors: [
            theme.palette.info.dark,
            theme.palette.primary.dark,
            theme.palette.error.light,
          ],
        },
        backgroundColor: '#ededed',
        datalessRegionColor: '#fff',
        defaultColor: '#f5f5f5',
        sizeAxis: {
          maxValue: 10,
          maxSize: 8,
        },
      }}
      // Note: you will need to get a mapsApiKey for your project.
      // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
      mapsApiKey={MAP_API_KEY}
      rootProps={{ 'data-test-id': '4' }}
    />
  );
};

export default ByCountryChart;
