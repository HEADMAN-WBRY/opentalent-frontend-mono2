import { ReactComponent as CheckIcon } from 'assets/icons/check.svg';
import { DEFAULT_AVATAR } from 'consts/common';
import React from 'react';
import { Link } from 'react-router-dom';
import { pathManager } from 'routes/consts';
import { needToShowTalentBlueTick } from 'utils/talent';

import {
  Avatar,
  Box,
  Grid,
  ListItem,
  ListItemAvatar,
  ListItemText,
  useTheme,
} from '@mui/material';

import { Talent } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import useStyles from './styles';

interface TalentCardProps {
  talent: Talent;
}

const TalentCard = ({ talent }: TalentCardProps) => {
  const theme = useTheme();
  const classes = useStyles();
  const rate = talent?.rate;
  const location = talent?.location;
  const position = talent?.recent_position_title;
  const name = [talent?.first_name, talent?.last_name]
    .filter(Boolean)
    .join(' ');
  const showBlueTick = needToShowTalentBlueTick(talent);

  return (
    <Link
      to={pathManager.company.talentProfile.generatePath({ id: talent?.id })}
    >
      <Box className={classes.talentCard}>
        <ListItem alignItems="flex-start">
          <ListItemAvatar className={classes.talentAvatarWrap}>
            <Avatar
              className={classes.talentAvatar}
              alt="Avatar"
              src={talent?.avatar?.avatar || DEFAULT_AVATAR}
            />
          </ListItemAvatar>
          <ListItemText
            primary={
              <Grid spacing={2} className={classes.nameRow} container>
                <Grid item>
                  <Typography
                    component="span"
                    variant="body1"
                    color="textPrimary"
                  >
                    {name}
                  </Typography>
                </Grid>
                <Grid item>
                  {showBlueTick && (
                    <CheckIcon color={theme.palette.info.main} />
                  )}
                </Grid>
              </Grid>
            }
            secondary={
              <Grid spacing={2} container>
                {position && (
                  <Grid item>
                    <Typography
                      component="span"
                      variant="body2"
                      color="textPrimary"
                      style={{ wordBreak: 'break-word' }}
                    >
                      {talent?.recent_position_title}
                    </Typography>
                  </Grid>
                )}
                {location && (
                  <Grid item>
                    <Typography
                      component="span"
                      variant="body2"
                      color="textSecondary"
                    >
                      {talent?.location}
                    </Typography>
                  </Grid>
                )}
                {rate && (
                  <Grid item>
                    <Typography
                      component="span"
                      variant="subtitle2"
                      color="textSecondary"
                    >
                      Hourly Rate
                    </Typography>
                    &nbsp;
                    <Typography
                      component="span"
                      variant="body2"
                      color="textSecondary"
                    >
                      {`€${rate}/h`}
                    </Typography>
                  </Grid>
                )}
              </Grid>
            }
          />
        </ListItem>
      </Box>
    </Link>
  );
};

export default TalentCard;
