import React from 'react';

import { Grid } from '@mui/material';

import { Maybe, Talent } from '@libs/graphql-types';

import DataBlock from '../data-block';
import TalentCard from './TalentCard';

interface LatestAdditionsProps {
  lastTalents: Maybe<Talent>[];
}

const LatestAdditions = ({ lastTalents }: LatestAdditionsProps) => {
  return (
    <DataBlock fullHeight title="Latest Community members">
      <Grid spacing={4} direction="column" container wrap="nowrap">
        {(lastTalents as Talent[]).map((talent) => (
          <Grid key={talent?.id} item>
            <TalentCard talent={talent} />
          </Grid>
        ))}
      </Grid>
    </DataBlock>
  );
};

export default LatestAdditions;
