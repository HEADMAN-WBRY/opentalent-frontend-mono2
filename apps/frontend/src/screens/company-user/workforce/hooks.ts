import { useQuery } from '@apollo/client';
import { useCurrentUserCompanyId } from 'hooks/company';
import { useIsEnterpriseCompanyAccount } from 'hooks/company/useCompanyAccountType';
import { useSearchParams } from 'hooks/routing';
import { useSnackbar } from 'notistack';
import { useCallback, useEffect, useMemo } from 'react';
import * as qs from 'utils/querystring';

import { Query, QueryTalentsSearchArgs } from '@libs/graphql-types';
import { isNumber, noop } from '@libs/helpers/common';

import { SEARCH_TALENTS } from './queries';
import { CIRCLE_OPTIONS } from './search-filter/consts';
import {
  getValuesFromQuerystring,
  mapFormValuesToQueryVariables,
} from './search-filter/utils';

export const useTalentsRequest = (anchorRef: any) => {
  const { enqueueSnackbar } = useSnackbar();
  const searchParams = useSearchParams();
  const companyId = useCurrentUserCompanyId();
  const isEnterprise = useIsEnterpriseCompanyAccount();
  const parsedSearch = useMemo(() => {
    const mapped =
      mapFormValuesToQueryVariables(
        getValuesFromQuerystring(qs.stringify(searchParams)),
      ) || {};

    if (!mapped.source_type) {
      mapped.source_type = isEnterprise
        ? CIRCLE_OPTIONS[0].value
        : CIRCLE_OPTIONS[1].value;
    }
    if (mapped.source_type === 'OWN') {
      mapped.connection_with_company_id = companyId;
    }

    return mapped;
  }, [companyId, isEnterprise, searchParams]) as QueryTalentsSearchArgs;
  let finalRefetch: any = noop;

  const { loading, fetchMore, refetch, data } = useQuery<
    Query,
    QueryTalentsSearchArgs
  >(SEARCH_TALENTS, {
    nextFetchPolicy: 'network-only',
    variables: parsedSearch,
    onCompleted: async (data) => {
      const { current_page, last_page } =
        data?.talentsSearch?.custom_paginator_info || {};

      if (!isNumber(current_page) || !isNumber(last_page)) {
        return;
      }

      if (current_page > last_page) {
        anchorRef.current.scrollIntoView({ behavior: 'smooth' });
        finalRefetch?.({ ...parsedSearch, page: last_page });
      }
    },
    onError: (error) => {
      enqueueSnackbar(error.message, {
        variant: 'error',
        preventDuplicate: true,
      });
    },
  });
  finalRefetch = refetch;
  const onPaginationChange = useCallback(
    async (e: React.ChangeEvent<unknown>, page: number) => {
      anchorRef.current.scrollIntoView({ behavior: 'smooth' });
      await refetch({ ...parsedSearch, page });
    },
    [anchorRef, parsedSearch, refetch],
  );

  useEffect(() => {
    refetch(parsedSearch);
  }, [fetchMore, refetch, parsedSearch]);

  return {
    data,
    loading,
    onPaginationChange,
    parsedSearch,
    refetch,
  };
};
