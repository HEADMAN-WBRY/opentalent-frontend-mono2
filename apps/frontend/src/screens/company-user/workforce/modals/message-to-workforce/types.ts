import { OptionType } from '@libs/ui/components/form/select';

import { MESSAGE_TYPES } from './consts';

export interface FormModel {
  category: string;
  messageType: keyof typeof MESSAGE_TYPES;
  message: string;
  tags: OptionType[];
  sendEmail: boolean;
}
