import React from 'react';
import { pathManager } from 'routes';

import { makeStyles } from '@mui/styles';

import { RouterButton } from '@libs/ui/components/button';

interface PendingTalentsLinkProps {
  count?: number;
}

const useStyles = makeStyles((theme) => ({
  badge: {
    minWidth: 24,
    height: 24,
    padding: 4,
    fontSize: '12px !important',
    backgroundColor: theme.palette.primary.main,
    borderRadius: '50%',
    color: theme.palette.text.primary,
    textAlign: 'center',
    lineHeight: '17px',
  },
}));

const PendingTalentsLink = ({ count = 0 }: PendingTalentsLinkProps) => {
  const classes = useStyles();

  return (
    <RouterButton
      startIcon={<span className={classes.badge}>{count}</span>}
      to={`${pathManager.company.pendingTalents.generatePath()}${
        window.location.search
      }`}
      variant="contained"
      color="tertiary"
    >
      pending verification
    </RouterButton>
  );
};

export default PendingTalentsLink;
