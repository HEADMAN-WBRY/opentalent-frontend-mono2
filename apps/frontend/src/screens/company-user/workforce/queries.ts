import { gql } from '@apollo/client';
import { ABAC_PROPERTIES } from 'graphql/fragments/talent/abacFields';

export const SEARCH_TALENTS = gql`
  ${ABAC_PROPERTIES}
  query GetTalents(
    $search: String
    $country: String
    $category_ids: [ID]
    $skills_ids: [ID]
    $max_rate: Float
    $min_rate: Float
    $source_type: SourceTypeEnum
    $is_active: Boolean
    $is_matcher: Boolean
    $is_verification_required: Boolean
    $first: Int = 10
    $page: Int
    $available_now: Boolean
    $tags_ids: [ID]
    $skills_boolean_filter: TalentsBooleanSkillsFilterInput
    $connected_with_company_id: ID
  ) {
    pendingTalents: talentsSearch(
      source_type: OWN
      is_verification_required: true
      connection_with_company_id: $connected_with_company_id
    ) {
      custom_paginator_info {
        total
      }
    }
    talentsSearch(
      country: $country
      connection_with_company_id: $connected_with_company_id
      search_string: $search
      category_ids: $category_ids
      skills_ids: $skills_ids
      max_rate: $max_rate
      min_rate: $min_rate
      source_type: $source_type
      is_active: $is_active
      first: $first
      available_now: $available_now
      page: $page
      is_verification_required: $is_verification_required
      skills_boolean_filter: $skills_boolean_filter
      is_matcher: $is_matcher
      tags_ids: $tags_ids
    ) {
      custom_paginator_info {
        current_page
        last_page
        per_page
        total
      }
      data {
        highlights {
          source
          text
        }
        talent {
          company_pool_connections {
            company {
              id
              name
            }
            status
            initiator
            cancel_initiator
          }
          origin

          id
          stream_chat_id
          reminded_at
          created_at
          is_invitation_accepted
          is_verification_required
          first_name
          last_name
          address
          rate
          recent_position_title
          email
          available_now
          location
          is_ot_pool
          first_name_abac {
            ...AbacProps
          }
          last_name_abac {
            ...AbacProps
          }
          category {
            id
            name
          }
          subcategories {
            id
            name
          }
          is_matcher
          source {
            id
          }
          avatar {
            avatar
          }
          talent_data {
            phone
          }
        }
      }
    }
  }
`;
