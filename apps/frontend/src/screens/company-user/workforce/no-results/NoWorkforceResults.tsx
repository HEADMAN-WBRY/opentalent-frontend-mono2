import { useFormikContext } from 'formik';
import { useIsEnterpriseCompanyAccount } from 'hooks/company/useCompanyAccountType';
import React, { useCallback } from 'react';
import { pathManager } from 'routes';

import { Box } from '@mui/material';

import Typography, { RouterLink } from '@libs/ui/components/typography';

import { OPENTALENT_CIRCLE_OPTION } from '../search-filter/consts';

interface NoWorkforceResultsProps { }

const useChangeFilter = () => {
  const { setFieldValue } = useFormikContext();
  return useCallback(() => {
    setFieldValue('source_type', OPENTALENT_CIRCLE_OPTION);
  }, [setFieldValue]);
};

export const NoWorkforceResults = (props: NoWorkforceResultsProps) => {
  const onClick = useChangeFilter();
  const isEnterprise = useIsEnterpriseCompanyAccount();

  return (
    <Box maxWidth="570px">
      <Typography paragraph variant="h6">
        There is no one here yet!
      </Typography>
      {isEnterprise && (
        <Typography paragraph variant="body2">
          You can{' '}
          <RouterLink
            to={pathManager.company.createProfile.generatePath()}
            variant="body2"
            style={{ textDecoration: 'none' }}
          >
            invite
          </RouterLink>{' '}
          people to join your community, or you can add people from the{' '}
          <Typography
            style={{ cursor: 'pointer' }}
            onClick={onClick}
            component="span"
            color="info.main"
            variant="body2"
          >
            OpenTalent network
          </Typography>
          .
        </Typography>
      )}
    </Box>
  );
};
