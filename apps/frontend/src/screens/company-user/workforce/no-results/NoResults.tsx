import React from 'react';

import { QueryTalentsSearchArgs, SourceTypeEnum } from '@libs/graphql-types';

import { NoResultsDefault } from './NoResultsDefault';
import { NoWorkforceResults } from './NoWorkforceResults';

interface NoResultsProps {
  search: QueryTalentsSearchArgs;
}

const NoResults = ({ search: { page, ...rest } }: NoResultsProps) => {
  // const noFilters =
  //   Object.values(rest).filter(Boolean).length <= 1 && (!page || page === 1);
  const isWorkforce = rest.source_type === SourceTypeEnum.Own;

  if (isWorkforce) {
    return <NoWorkforceResults />;
  }

  return <NoResultsDefault />;
};

export default NoResults;
