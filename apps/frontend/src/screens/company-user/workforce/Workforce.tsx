import { CompanyDirectCommunityBanner } from 'components/custom/company/direct';
import {
  CreateCampaignModal,
  InviteByCompanyModal,
} from 'components/custom/talent/modals';
import { ConnectedPageLayout } from 'components/layout/page-layout';
import { Formik } from 'formik';
import useMediaQueries from 'hooks/common/useMediaQueries';
import {
  useCompanyAccountTypeCheck,
  useIsFreeCompanyAccount,
} from 'hooks/company/useCompanyAccountType';
import React, { useRef, useState } from 'react';
import { pathManager } from 'routes';

import AddIcon from '@mui/icons-material/Add';
import TuneIcon from '@mui/icons-material/Tune';
import { Pagination } from '@mui/lab';
import {
  Box,
  CircularProgress,
  Grid,
  Hidden,
  SwipeableDrawer,
} from '@mui/material';

import { CompanyAccountTypeEnum, SourceTypeEnum } from '@libs/graphql-types';
import { formatNumber } from '@libs/helpers/format';
import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import TalentCard from '../shared/talent-card-v2';
import { useTalentsRequest } from './hooks';
import { MessageToWorkforceModal } from './modals';
import NoResults from './no-results';
import PendingTalentsLink from './pending-talents-link';
import SearchFilter from './search-filter';
import { useFilterSubmit, useInitialFilterState } from './search-filter/hooks';
import validator from './search-filter/validator';
import SendAnnouncement from './send-announcemnt';
import useStyles from './styles';

export interface WorkforceProps { }

export interface StyledWorkforceProps extends WorkforceProps {
  isEmpty: boolean;
}

const Workforce = (props: WorkforceProps) => {
  const anchorRef = useRef<HTMLDivElement>(null);
  const [isDrawerOpen, setIsDrawerOpen] = useState(false);
  const { isSM } = useMediaQueries();
  const { initialValues } = useInitialFilterState();
  const isEnterpriseCompany = useCompanyAccountTypeCheck([
    CompanyAccountTypeEnum.Enterprise,
  ]);
  const isFreeCompanyAccount = useIsFreeCompanyAccount();

  const { data, loading, parsedSearch, refetch } = useTalentsRequest(anchorRef);

  const allTalentsCount =
    data?.talentsSearch?.custom_paginator_info?.total || 0;
  const lastPage = data?.talentsSearch?.custom_paginator_info?.last_page;
  const currentPage = data?.talentsSearch?.custom_paginator_info?.current_page;
  const talents = data?.talentsSearch?.data || [];
  const isEmpty = talents.length === 0;
  const pendingTalentsCount =
    (data as any)?.pendingTalents?.custom_paginator_info?.total || 0;
  const pagesIsRight = true;
  //   isNumber(currentPage) && isNumber(lastPage) && currentPage <= lastPage;

  const classes = useStyles({ ...props, isEmpty });

  const onSubmit = useFilterSubmit();

  return (
    <Formik
      onSubmit={onSubmit}
      initialValues={initialValues}
      validationSchema={validator}
    >
      {({ setFieldValue, values, submitForm }) => {
        const isOpentalentSource =
          values?.source_type?.value === SourceTypeEnum.Opentalent;

        return (
          <ConnectedPageLayout
            headerProps={{ accountProps: {} }}
            drawerProps={{}}
            documentTitle="Search for Talent"
          >
            <div ref={anchorRef} />
            <Box className={classes.container}>
              <Grid
                className={classes.fullHeight}
                container
                direction="column"
                justifyContent="space-between"
              >
                <Grid item>
                  <Grid
                    container
                    justifyContent="space-between"
                    alignItems="center"
                  >
                    <Grid item>
                      <Typography variant="h5">
                        {isOpentalentSource
                          ? '🔎 Search the OpenTalent community'
                          : '🔎 Search My Community'}
                      </Typography>
                    </Grid>
                    <Grid className={classes.titleButtons} item>
                      {!isOpentalentSource && isEnterpriseCompany && (
                        <Grid spacing={4} container>
                          {!!pendingTalentsCount && (
                            <Grid item>
                              <PendingTalentsLink count={pendingTalentsCount} />
                            </Grid>
                          )}
                          <Grid item>
                            <SendAnnouncement />
                          </Grid>
                        </Grid>
                      )}
                      <Hidden mdUp>
                        <Box display="flex" alignItems="center">
                          <Button
                            onClick={() => setIsDrawerOpen(true)}
                            color="inherit"
                            endIcon={<TuneIcon />}
                          >
                            Search
                          </Button>
                        </Box>
                      </Hidden>
                    </Grid>
                  </Grid>
                  <Box mt={6}>
                    {isOpentalentSource ? (
                      <Typography>
                        Find vetted, high-skilled professionals and add them to
                        your community. 👇
                      </Typography>
                    ) : (
                      <Typography>
                        Let’s find the right person within your talent
                        community.
                      </Typography>
                    )}
                  </Box>
                  <Box className={classes.form}>
                    <Grid container spacing={4}>
                      <Grid className={classes.filterContainer} item md={4}>
                        <SwipeableDrawer
                          classes={{
                            paper: classes.settingsDrawer,
                          }}
                          anchor="right"
                          variant={isSM ? 'temporary' : 'permanent'}
                          open={isDrawerOpen}
                          onClose={() => setIsDrawerOpen(false)}
                          onOpen={() => setIsDrawerOpen(true)}
                        >
                          <SearchFilter
                            closeDrawer={() => setIsDrawerOpen(false)}
                            isOpentalentSource={isOpentalentSource}
                          />
                        </SwipeableDrawer>
                      </Grid>
                      <Grid item md={8} sm={12}>
                        {!!allTalentsCount && (
                          <Typography paragraph>
                            <Typography
                              variant="h6"
                              component="span"
                              color="info.main"
                            >
                              {formatNumber(allTalentsCount)}
                            </Typography>{' '}
                            Candidates found
                          </Typography>
                        )}

                        {isFreeCompanyAccount && (
                          <Box mb={4}>
                            <CompanyDirectCommunityBanner />
                          </Box>
                        )}

                        {talents?.map((item: any) => (
                          <Box key={item.talent.id} mb={4}>
                            <TalentCard talentSearch={item} refetch={refetch} />
                          </Box>
                        ))}
                        {(loading || !pagesIsRight) && (
                          <Box
                            display="flex"
                            alignItems="center"
                            justifyContent="center"
                          >
                            <CircularProgress color="secondary" />
                          </Box>
                        )}
                        {isEmpty && !loading && pagesIsRight && (
                          <NoResults search={parsedSearch} />
                        )}
                        {!isEmpty && !loading && (
                          <Box display="flex" justifyContent="center" mt={4}>
                            <Pagination
                              page={currentPage}
                              showFirstButton
                              showLastButton
                              count={lastPage}
                              variant="outlined"
                              shape="rounded"
                              onChange={(_, page) => {
                                setFieldValue('page', page);
                                submitForm();
                                if (anchorRef?.current) {
                                  anchorRef?.current?.scrollIntoView({
                                    behavior: 'smooth',
                                  });
                                }
                              }}
                            />
                          </Box>
                        )}
                      </Grid>
                    </Grid>
                  </Box>
                </Grid>
              </Grid>
              <Hidden smUp>
                <Button
                  size="large"
                  variant="contained"
                  color="primary"
                  href={pathManager.company.createProfile.generatePath()}
                  classes={{
                    root: classes.mbInviteButton,
                  }}
                  startIcon={<AddIcon />}
                >
                  <Typography>Invite</Typography>
                </Button>
              </Hidden>
            </Box>
            <InviteByCompanyModal />
            <MessageToWorkforceModal />
            <CreateCampaignModal />
          </ConnectedPageLayout>
        );
      }}
    </Formik>
  );
};

export default Workforce;
