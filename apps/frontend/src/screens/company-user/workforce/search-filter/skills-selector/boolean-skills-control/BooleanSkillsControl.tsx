import {
  BooleanModalState,
  isBooleanSearchEmpty,
} from 'components/custom/skills-boolean-search';
import BooleanSearchFormula from 'components/custom/skills-boolean-search/boolean-modal/BooleanSearchFormula';
import React from 'react';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

interface BooleanSkillsControlProps {
  onToggle: VoidFunction;
  openModal: VoidFunction;
  booleanSearch: BooleanModalState;
  clearBooleanSearch: VoidFunction;
}

const useStyles = makeStyles((theme) => ({
  formula: {
    textAlign: 'left',
  },
}));

const BooleanSkillsControl = ({
  onToggle,
  booleanSearch,
  openModal,
  clearBooleanSearch,
}: BooleanSkillsControlProps) => {
  const classes = useStyles();
  const isEmpty = isBooleanSearchEmpty(booleanSearch);

  return (
    <>
      <Box pb={2}>
        {isEmpty ? (
          <Typography variant="body2" paragraph>
            Your search query is empty.
          </Typography>
        ) : (
          <BooleanSearchFormula
            className={classes.formula}
            state={booleanSearch}
            grey
          />
        )}
      </Box>

      <Grid spacing={2} container>
        <Grid item>
          <Button onClick={openModal} variant="outlined" color="info">
            {isEmpty ? 'create' : 'edit'} boolean
          </Button>
        </Grid>
        <Grid item>
          <Button onClick={onToggle} variant="outlined" color="info">
            standard search
          </Button>
        </Grid>
        <Grid item>
          <Button onClick={clearBooleanSearch} variant="outlined" color="error">
            clean
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

export default BooleanSkillsControl;
