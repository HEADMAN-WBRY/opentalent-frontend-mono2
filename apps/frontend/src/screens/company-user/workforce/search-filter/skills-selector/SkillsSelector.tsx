import {
  BooleanModal,
  useOpenBooleanModal,
} from 'components/custom/skills-boolean-search';
import React from 'react';

import { Box } from '@mui/material';

import Button from '@libs/ui/components/button';

import { DEFAULT_SKILLS } from '../consts';
import BooleanSkillsControl from './boolean-skills-control';
import {
  useToggleBooleanSearch,
  useToggleBooleanSearchState,
} from './boolean-skills-control/hooks';
import TalentsCountWidget from './boolean-skills-control/talents-count-widget/TalentsCountWidget';
import { useTalentCountRequest } from './boolean-skills-control/talents-count-widget/hooks';
import { DefaultSkillsControl } from './default-skills-control';

interface SkillsSelectorProps {}

export const SkillsSelector = (props: SkillsSelectorProps) => {
  const { isBooleanSkills, onToggle } = useToggleBooleanSearch();
  const { setBooleanState, booleanSearch, clearBooleanSearch } =
    useToggleBooleanSearchState();
  const openBooleanModalFunc = useOpenBooleanModal();
  const { getTalents, talentsCount } = useTalentCountRequest();
  const currentBooleanState = booleanSearch || DEFAULT_SKILLS;
  const openBooleanModal = () =>
    openBooleanModalFunc({
      initialValues: currentBooleanState,
    });

  return (
    <>
      {isBooleanSkills ? (
        <BooleanSkillsControl
          booleanSearch={currentBooleanState}
          onToggle={onToggle}
          openModal={openBooleanModal}
          clearBooleanSearch={clearBooleanSearch}
        />
      ) : (
        <>
          <DefaultSkillsControl />
          <Box pt={4}>
            <Button onClick={openBooleanModal} variant="outlined" color="info">
              use Boolean search
            </Button>
          </Box>
        </>
      )}

      <BooleanModal onSave={setBooleanState} onChange={getTalents}>
        <TalentsCountWidget talentsCount={talentsCount} />
      </BooleanModal>
    </>
  );
};
