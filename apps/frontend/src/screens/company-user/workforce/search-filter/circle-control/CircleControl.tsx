import letterO from 'assets/icons/letter-o-black.svg';
import { useFormikContext } from 'formik';
import { useCurrentUser } from 'hooks/auth';
import React from 'react';

import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { SourceTypeEnum } from '@libs/graphql-types';
import { ConnectedCircleSelect } from '@libs/ui/components/form/double-circle-select';

import { FilterValues, getCircleOptions } from '../consts';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',

    '& .circles > div': {
      '&:nth-child(2)': {
        background: `${theme.palette.grey[300]} !important`,

        '&.active': {
          background: `${theme.palette.primary.main} !important`,
        },
      },
    },
  },
  imageWrapper: {
    width: 40,
    overflow: 'hidden',
    borderRadius: '50%',
    height: 40,
    position: 'absolute',
    zIndex: 20,
    top: 24,
    left: 24,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    maxWidth: '100%',
    maxHeight: '100%',
  },
}));

const CircleControl = () => {
  const { values } = useFormikContext<FilterValues>();
  const isOwn = values?.source_type?.value === SourceTypeEnum.Own;
  const classes = useStyles();
  const { data } = useCurrentUser();
  const logo = isOwn ? data?.currentCompanyUser?.company?.logo : letterO;
  const name = data?.currentCompanyUser?.company?.name || 'Your';

  return (
    <Box className={classes.root}>
      {logo && (
        <Box className={classes.imageWrapper}>
          <img className={classes.logo} src={logo} alt="logo" />
        </Box>
      )}
      <ConnectedCircleSelect
        name="source_type"
        options={getCircleOptions(name)}
      />
    </Box>
  );
};

export default CircleControl;
