import { useCurrentUserCompanyId } from 'hooks/company';
import { useIsEnterpriseCompanyAccount } from 'hooks/company/useCompanyAccountType';
import { usePushWithQuery } from 'hooks/routing';
import { useMemo } from 'react';
import { useLocation } from 'react-router-dom';

import { debounce } from '@mui/material';
import { deepmerge } from '@mui/utils';

import { CIRCLE_OPTIONS, INITIAL_VALUES } from './consts';
import { getValuesFromQuerystring, mapValuesToQuery } from './utils';

export const useFilterSubmit = () => {
  const push = usePushWithQuery();
  const onSubmit = useMemo(() => {
    const handler = (values: typeof INITIAL_VALUES) => {
      const search = mapValuesToQuery(values);

      push({ query: search, replace: true });
    };
    return debounce(handler);
  }, [push]);

  return onSubmit;
};

export const useInitialFilterState = () => {
  const location = useLocation();
  const isEnterprise = useIsEnterpriseCompanyAccount();
  const companyId = useCurrentUserCompanyId();
  const queryFilter = useMemo(() => {
    return getValuesFromQuerystring(location.search);
  }, [location.search]);
  const initialValues = useMemo(
    () =>
      deepmerge(
        {
          ...INITIAL_VALUES,
          source_type: isEnterprise ? CIRCLE_OPTIONS[0] : CIRCLE_OPTIONS[1],
          company_id: companyId!,
        },
        queryFilter || {},
        { clone: true },
      ),
    [companyId, isEnterprise, queryFilter],
  );

  return { initialValues };
};
