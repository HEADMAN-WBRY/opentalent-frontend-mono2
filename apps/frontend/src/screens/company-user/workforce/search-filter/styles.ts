import { makeStyles } from '@mui/styles';

import { PALETTE } from '@libs/ui/themes/default/palette';
import { TYPOGRAPHY_THEME_OPTIONS } from '@libs/ui/themes/default/typography';

const useStyles = makeStyles(() => ({
  paper: {
    width: '100%',
    borderRadius: 0,
    padding: '24px 24px',
  },
  box: {
    width: '100%',
  },
  badge: {
    width: 24,
    height: 24,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 100,
    backgroundColor: (PALETTE.primary as any).main ?? '#FFFF00',
    ...TYPOGRAPHY_THEME_OPTIONS.body2,
  },
  searchByInput: {
    overflow: 'hidden',

    '& .MuiInputLabel-root': {
      whiteSpace: 'nowrap',
    },
  },
}));

export default useStyles;
