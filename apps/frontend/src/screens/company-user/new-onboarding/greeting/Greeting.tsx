import FixedFooter from 'components/custom/onboarding/fixed-footer';
import React, { useState } from 'react';
import { pathManager } from 'routes';

import { Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { RouterButton } from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import Container from '../shared/Container';
import ChooseServiceBlock from './ChooseServiceBlock';

interface GreetingProps {}

const useStyles = makeStyles((theme) => ({
  textWrap: {
    maxWidth: 540,
    margin: '0 auto 60px',
  },
  choicesWrap: {
    display: 'flex',
    flexWrap: 'nowrap',
    justifyContent: 'center',
    alignItems: 'top',

    '& > div:not(:last-child)': {
      marginRight: theme.spacing(8),
    },
  },
}));

const CHOICES = [
  {
    type: 'self_service' as const,
    title: 'Self-service',
    subtitle:
      'Post a FREE job and let the OpenTalent network and marketplace do the heavy lifting for you. Results within minutes.',
    forWho:
      'Companies that have open remote roles and want to find someone quickly, hassle-free.',
  },
  {
    type: 'outsourced' as const,
    title: 'Outsourced / RPO',
    forWho:
      'Companies that seek full-service Recruitment Process Outsourcing (RPO).',
    subtitle:
      'Let our Talent Matching Specialists help you to attract, source and hire the talent you need to drive your business forward.',
  },
];

const Greeting = (props: GreetingProps) => {
  const classes = useStyles();
  const [currentType, setType] = useState<'self_service' | 'outsourced'>();
  const link =
    currentType === 'self_service'
      ? pathManager.company.newOnboarding.builder.main.getRoute()
      : pathManager.company.newOnboarding.bookCall.getRoute();

  return (
    <Container>
      <div>
        <div className={classes.textWrap}>
          <Typography style={{ fontStyle: 'italic' }} paragraph variant="h4">
            Hey there! 🎉
          </Typography>
          <Typography paragraph variant="body2">
            Welcome to the <b>OpenTalent Marketplace</b>; Europe’s
            ‘community-powered’ recruitment system for hiring managers to close
            ‘hard-to-fill’ roles quickly, hassle-free at fixed competitive
            rates.
          </Typography>

          <Typography fontWeight={500} variant="h6">
            Start by telling us what service you are looking for?
          </Typography>
        </div>
        <div className={classes.choicesWrap}>
          {CHOICES.map(({ title, subtitle, type, forWho }) => (
            <ChooseServiceBlock
              key={type}
              title={title}
              subtitle={subtitle}
              name={type}
              onClick={() => setType(type)}
              isActive={currentType === type}
              forWho={forWho}
            />
          ))}
        </div>

        <FixedFooter>
          <Grid justifyContent="center" container>
            <Grid item>
              <RouterButton
                to={link}
                color="primary"
                variant="contained"
                style={{ width: 300 }}
                disabled={!currentType}
              >
                next
              </RouterButton>
            </Grid>
          </Grid>
        </FixedFooter>
      </div>
    </Container>
  );
};

export default Greeting;
