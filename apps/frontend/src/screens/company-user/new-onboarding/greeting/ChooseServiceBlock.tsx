import React from 'react';

import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import ChoiceBox from '../shared/ChoiceBox';
import { InfoBox } from '../shared/InfoBox';

interface ChooseServiceBlockProps {
  name: string;
  title: string;
  subtitle: string;
  forWho: string;
  isActive: boolean;
  onClick: VoidFunction;
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    maxWidth: 450,
  },
  choiceRoot: {
    cursor: 'pointer',
    position: 'relative',
    height: 242,
    textAlign: 'center',
    borderRadius: 16,
    transition: 'border .3s',
    background: theme.palette.grey[800],
    border: ({ isActive }: ChooseServiceBlockProps) =>
      isActive
        ? `2px solid ${theme.palette.primary.main}`
        : `2px solid transparent`,
  },
  choiceBox: {
    height: 242,
  },
  checkboxChecked: {
    color: `${theme.palette.primary.main} !important`,
  },
  checkboxLabel: {
    margin: 0,
  },
  choiceDetails: {
    marginTop: theme.spacing(4),
  },
}));

const ChooseServiceBlock = (props: ChooseServiceBlockProps) => {
  const classes = useStyles(props);
  const { forWho, ...rest } = props;

  return (
    <div className={classes.root}>
      <ChoiceBox {...rest} className={classes.choiceBox} />

      <InfoBox className={classes.choiceDetails}>
        <Typography variant="subtitle1">
          <Typography component="span" color="primary.main">
            For Who:
          </Typography>{' '}
          {forWho}
        </Typography>
      </InfoBox>
    </div>
  );
};

export default ChooseServiceBlock;
