import FixedFooter from 'components/custom/onboarding/fixed-footer';
import { INFINITY_SIGN } from 'consts/common';
import React from 'react';
import { EXTERNAL_RESOURCES, pathManager } from 'routes';

import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import { Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { isNil } from '@libs/helpers/common';
import { formatNumber } from '@libs/helpers/format';
import Button, { RouterButton } from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import Container from '../shared/Container';
import { useAppInfo } from './hooks';

interface WelcomeProps {}

const useStyles = makeStyles((theme) => ({
  textWrap: {
    maxWidth: 570,
    margin: '0 auto 60px',
  },
}));

const Welcome = (props: WelcomeProps) => {
  const classes = useStyles();
  const { talentsCount } = useAppInfo();
  const talents = isNil(talentsCount)
    ? INFINITY_SIGN
    : formatNumber(talentsCount);
  // const recruiters = isNil(recruitersCount)
  //   ? INFINITY_SIGN
  //   : formatNumber(recruitersCount);

  return (
    <Container>
      <div>
        <div className={classes.textWrap}>
          <Typography
            style={{ fontStyle: 'italic', marginBottom: 24 }}
            paragraph
            variant="h4"
            fontWeight={600}
          >
            Welcome! 👋
          </Typography>

          <Typography variant="body1" paragraph>
            Posting a job is FREE and takes 1-2 minutes.
          </Typography>

          <Typography variant="body1" paragraph>
            Afterwards, we’ll show you the best matching candidates from our
            fast-growing community of{' '}
            <Typography component="span" variant="body1" color="primary.main">
              {talents} professionals
            </Typography>{' '}
            across Europe.
          </Typography>

          <Typography variant="body1" paragraph>
            We’ll also ask our community members to recommend people from their
            personal networks.
          </Typography>

          <Typography variant="body1" paragraph>
            And we’ll have one of our Talent Matchers review the best
            candidate(s) for you to interview.
          </Typography>

          <Typography variant="body1" paragraph>
            Now, let’s get your first job live!
          </Typography>
        </div>

        <FixedFooter>
          <Grid spacing={4} justifyContent="center" container>
            <Grid xs={6} item>
              <Button
                href={EXTERNAL_RESOURCES.companiesLanding}
                color="primary"
                fullWidth
                variant="outlined"
                startIcon={<ArrowBackIcon />}
              >
                BACK
              </Button>
            </Grid>
            <Grid xs={6} item>
              <RouterButton
                to={pathManager.company.newOnboarding.builder.main.generatePath()}
                color="primary"
                variant="contained"
                fullWidth
                endIcon={<ArrowForwardIcon />}
              >
                START
              </RouterButton>
            </Grid>
          </Grid>
        </FixedFooter>
      </div>
    </Container>
  );
};

export default Welcome;
