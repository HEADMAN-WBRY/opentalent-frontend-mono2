import { PermanentJobCapacityTypeEnum } from '@libs/graphql-types';

export interface JobDurationInfo {
  startDate: string;
  endDate: string;
  capacity?: number;
  permanentCapacityType?: PermanentJobCapacityTypeEnum;
}
