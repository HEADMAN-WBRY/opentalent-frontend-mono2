import FixedFooter from 'components/custom/onboarding/fixed-footer';
import { Formik } from 'formik';
import React from 'react';
import { pathManager } from 'routes';
import * as yup from 'yup';

import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { modelPath } from '@libs/helpers/form';
import { formatCurrency } from '@libs/helpers/format';
import Button, { RouterButton } from '@libs/ui/components/button';
import ConnectedSlider from '@libs/ui/components/form/slider';
import { JOB_FINDERS_FEE } from '@libs/ui/components/job/utils/consts';
import Typography from '@libs/ui/components/typography';

import StepTemplate from '../shared/StepTemplate';
import { useSubmitHandler } from './hooks';
import { FindersFeeInfo } from './types';

interface FindersFeeProps {}

const INITIAL_VALUES: FindersFeeInfo = {
  findersFee: '',
};

const useStyles = makeStyles((theme) => ({
  info: {
    maxWidth: 800,
    margin: '-20px auto 0',
  },
  form: {
    maxWidth: 400,
    margin: '64px auto 0',
    color: 'white',
  },
  markLabel: {
    color: 'white',
    marginTop: theme.spacing(4),
  },
}));

const getValidator = () =>
  yup.object().shape({
    findersFee: yup.string().trim().required('Location is required'),
  });

const MARKS = [JOB_FINDERS_FEE.min, JOB_FINDERS_FEE.max];
const MARKS_ITEMS = MARKS.map((value) => ({
  label: formatCurrency(value),
  value,
}));

const FindersFee = (props: FindersFeeProps) => {
  const classes = useStyles();
  const { onSubmit, loading } = useSubmitHandler();

  return (
    <Formik<FindersFeeInfo>
      validationSchema={getValidator()}
      initialValues={INITIAL_VALUES}
      onSubmit={onSubmit}
    >
      {({ handleSubmit, errors }) => (
        <StepTemplate
          title="Finders Fee"
          subtitle="From where will work be conducted?"
          currentStep={7}
          currentStepLabel="Finders Fee"
        >
          <div className={classes.info}>
            <Typography variant="body2" paragraph>
              How much is it worth to you if we help you find your next best
              hire? That’s what we call the Finder’s Fee; an amount you pay only
              when we help you find the right person for the job. Finder Fees
              are split with our community, who can recommend people. The higher
              a Finder’s Fee, the more likely our commmunity will run for you,
              and help you find the ideal candidate.
            </Typography>

            <Typography variant="h6" paragraph>
              Go on and set the Finder’s Fee for this job.
            </Typography>
          </div>

          <Box className={classes.form}>
            <ConnectedSlider
              name={modelPath<FindersFeeInfo>((m) => m.findersFee)}
              step={500}
              valueLabelFormat={formatCurrency}
              valueLabelDisplay="auto"
              defaultValue={JOB_FINDERS_FEE.min}
              min={JOB_FINDERS_FEE.min}
              max={JOB_FINDERS_FEE.max}
              marks={MARKS_ITEMS}
              visibleMin={JOB_FINDERS_FEE.min}
              classes={{ markLabel: classes.markLabel }}
            />
          </Box>

          <FixedFooter>
            <Grid spacing={4} justifyContent="center" container>
              <Grid xs={6} item>
                <RouterButton
                  startIcon={<ArrowBackIcon />}
                  to={pathManager.company.newOnboarding.greeting.generatePath()}
                  color="primary"
                  variant="outlined"
                  style={{ width: 200 }}
                >
                  Back
                </RouterButton>
              </Grid>
              <Grid xs={6} item>
                <Button
                  endIcon={<ArrowForwardIcon />}
                  color="primary"
                  variant="contained"
                  style={{ width: 200 }}
                  disabled={loading || !!Object.keys(errors).length}
                  onClick={handleSubmit}
                >
                  next
                </Button>
              </Grid>
            </Grid>
          </FixedFooter>
        </StepTemplate>
      )}
    </Formik>
  );
};

export default FindersFee;
