export interface JobLocationInfo {
  location: string;
  country: string;
  city: string;
}
