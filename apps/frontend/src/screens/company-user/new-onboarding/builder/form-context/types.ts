import { CreationInfo } from '../account-creation/types';
import { FindersFeeInfo } from '../finders-fee/types';
import { JobCategoryInfo } from '../job-category/types';
import { JobDurationInfo } from '../job-duration/types';
import { RateInfo } from '../job-rate/types';
import { JobSkillsInfo } from '../job-skills/types';
import { JobTypeState } from '../job-type-choosing/types';
import { JobLocationInfo } from '../workspace/types';

export interface BuilderFormState {
  companyAccount: CreationInfo;
  jobType: JobTypeState;
  jobCategory: JobCategoryInfo;
  jobSkills: JobSkillsInfo;
  jobDuration: JobDurationInfo;
  workspace: JobLocationInfo;
  findersFee: FindersFeeInfo;
  rate: RateInfo;
  _internal: {
    deviceId: string;
  };
}
