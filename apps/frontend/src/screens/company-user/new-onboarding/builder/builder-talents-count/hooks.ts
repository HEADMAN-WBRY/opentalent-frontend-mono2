import { useFormikContext } from 'formik';
import { useCallback, useEffect, useMemo } from 'react';

import { debounce } from '@mui/material';

import {
  GetPotentialTalentsCountQueryVariables,
  useGetPotentialTalentsCountQuery,
} from '@libs/graphql-types';

import { mapFormDataToJobCreation } from '../form-context/mappers';
import { BuilderFormState } from '../form-context/types';

const mapValuesToCountRequest = (
  values: BuilderFormState,
): GetPotentialTalentsCountQueryVariables => {
  const finalValues = mapFormDataToJobCreation(values);

  return {
    type: finalValues.type!,
    category_id: finalValues.category_id,
    skills: finalValues.skills,
    solutions_required: finalValues.solutions_required,
    hard_skills_required: finalValues.hard_skills_required,
    campaign_end_date: finalValues.campaign_end_date,
    campaign_start_date: finalValues.campaign_start_date,
  };
};

export const useJobTalentsCount = () => {
  const { values } = useFormikContext<BuilderFormState>();

  const { data, loading, refetch } = useGetPotentialTalentsCountQuery({
    variables: {
      type: values.jobType.type,
    },
  });

  const onChange = useCallback(
    (values: BuilderFormState) => {
      const request = mapValuesToCountRequest(values);

      refetch(request);
    },
    [refetch],
  );

  const debouncedOnChangeHandler = useMemo(() => {
    return debounce(onChange, 1000);
  }, [onChange]);

  useEffect(() => {
    debouncedOnChangeHandler(values);
  }, [debouncedOnChangeHandler, values]);

  return { count: data?.potentialTalentsForJobCount, loading };
};
