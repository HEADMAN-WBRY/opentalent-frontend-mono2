import TalentsCount from 'components/custom/onboarding/talents-count';
import React from 'react';

import { makeStyles } from '@mui/styles';

import { useJobTalentsCount } from './hooks';

interface BuilderTalentsCountProps {}

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'fixed',
    bottom: 104,
    right: 24,
    width: 306,
    color: 'white',
    background:
      'linear-gradient(180deg, rgba(255, 255, 255, 0.05) 0%, rgba(255, 255, 255, 0.05) 100%), #121212',

    [theme.breakpoints.down('sm')]: {
      position: 'static',
      maxWidth: 400,
      width: '100%',
      margin: `${theme.spacing(6)} auto 0`,
    },

    '& p': {
      color: 'rgba(255, 255, 255, 0.7) !important',
    },
  },
}));

const BuilderTalentsCount = (props: BuilderTalentsCountProps) => {
  const classes = useStyles();
  const { count, loading } = useJobTalentsCount();
  const finalCount = count || 0;

  return (
    <TalentsCount
      className={classes.root}
      isLoading={loading}
      count={finalCount}
    />
  );
};

export default BuilderTalentsCount;
