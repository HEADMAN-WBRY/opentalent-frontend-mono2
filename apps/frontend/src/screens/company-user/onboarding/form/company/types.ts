import { ImageFormikState } from 'components/form/image-upload-v2/upload-image-control';

export interface CreateCompanyState {
  _logo: ImageFormikState;
  name: string;
  website: string;
}
