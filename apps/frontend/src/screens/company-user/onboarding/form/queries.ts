import { gql } from '@apollo/client';

export const SEND_INVITATION_LINK = gql`
  mutation SendInvitationLink($invitation_link_id: ID!, $email: Email!) {
    sendInvitationLinkByEmail(
      invitation_link_id: $invitation_link_id
      email: $email
    )
  }
`;

export const GET_SCREEN_DATA = gql`
  query GetScreenData {
    currentTalentInvitationLinks {
      id
      url
      name
      is_used
    }
  }
`;
