import { ImageFormikState } from 'components/form/image-upload-v2/upload-image-control';

import { CreateCompanyUserMutationVariables } from '@libs/graphql-types';

export interface CreateUserState extends CreateCompanyUserMutationVariables {
  _avatar: ImageFormikState;
}
