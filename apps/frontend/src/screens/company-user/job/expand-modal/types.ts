export interface FormModel {
  days: number;
}

export interface ExpandCampaignModalData {
  jobId: string;
  campaignEndDate: Date | string;
}
