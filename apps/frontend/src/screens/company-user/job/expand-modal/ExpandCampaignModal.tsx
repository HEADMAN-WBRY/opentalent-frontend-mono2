import { Formik } from 'formik';
import React from 'react';
import { DefaultModalProps, withLocationStateModal } from 'utils/modals';
import * as yup from 'yup';

import { Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { modelPath } from '@libs/helpers/form';
import Button from '@libs/ui/components/button';
import { ConnectedSelect } from '@libs/ui/components/form/select';
import { DefaultModal } from '@libs/ui/components/modals';

import { MODAL_TYPES } from '../consts';
import { useSubmitHandler } from './hooks';
import { FormModel, ExpandCampaignModalData } from './types';

const useStyles = makeStyles((theme) => ({
  paper: {
    width: 620,
  },
  content: {
    textAlign: 'left',
    marginTop: theme.spacing(4),
  },
}));

const validation = yup.object().shape({
  days: yup.number().nullable().required(),
});

const DURATION_OPTIONS = [
  { text: '2 days', value: 2 },
  { text: '5 days', value: 5 },
  { text: '10 days', value: 10 },
  { text: '30 days', value: 30 },
];

const ExpandCampaignModalComponent = ({
  modalData,
  isOpen,
  close,
}: DefaultModalProps<ExpandCampaignModalData>) => {
  const classes = useStyles();
  const { onSubmit, loading } = useSubmitHandler({
    handleClose: close,
    jobId: modalData?.jobId || '',
    campaignEndDate: modalData?.campaignEndDate || '',
  });

  return (
    <Formik<FormModel>
      initialValues={{ days: 2 }}
      onSubmit={onSubmit}
      validationSchema={validation}
    >
      {({ handleSubmit }) => (
        <DefaultModal
          actions={
            <Grid container spacing={4}>
              <Grid xs={6} item>
                <Button
                  fullWidth
                  color="primary"
                  variant="contained"
                  size="large"
                  onClick={handleSubmit}
                  disabled={loading}
                >
                  Extend campaign
                </Button>
              </Grid>
              <Grid xs={6} item>
                <Button
                  size="large"
                  fullWidth
                  variant="outlined"
                  onClick={close}
                  disabled={loading}
                >
                  Cancel
                </Button>
              </Grid>
            </Grid>
          }
          handleClose={close}
          open={isOpen}
          title="Extend campaign"
          className={classes.paper}
        >
          <Grid
            className={classes.content}
            container
            spacing={4}
            direction="column"
          >
            <Grid item>
              <ConnectedSelect
                formControlProps={{ size: 'small' }}
                options={DURATION_OPTIONS}
                fullWidth
                hideNoneValue
                name={modelPath<FormModel>((m) => m.days)}
                variant="filled"
                label="Select number of days to extend"
              />
            </Grid>
          </Grid>
        </DefaultModal>
      )}
    </Formik>
  );
};

export const ExpandCampaignModal =
  withLocationStateModal<ExpandCampaignModalData>({
    id: MODAL_TYPES.EXPAND,
  })(ExpandCampaignModalComponent);
