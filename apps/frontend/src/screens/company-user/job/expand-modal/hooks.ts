import { addDays } from 'date-fns';
import { useSnackbar } from 'notistack';

import { useExpandJobMutation } from '@libs/graphql-types';

import { FormModel } from './types';

export const useSubmitHandler = ({
  jobId,
  handleClose,
  campaignEndDate,
}: {
  campaignEndDate: string | Date;
  jobId: string;
  handleClose: VoidFunction;
}) => {
  const { enqueueSnackbar } = useSnackbar();
  const [extend, { loading }] = useExpandJobMutation({
    onCompleted: () => {
      enqueueSnackbar('Campaign successfully extended!', {
        variant: 'success',
      });
      handleClose();
    },
  });
  const onSubmit = (values: FormModel) => {
    if (!jobId) {
      enqueueSnackbar('No job id provided', { variant: 'error' });
      return;
    }
    const newEndDate = addDays(new Date(campaignEndDate), values.days);
    extend({
      variables: { id: jobId, campaign_end_date: newEndDate },
    });
  };
  return { loading, onSubmit };
};
