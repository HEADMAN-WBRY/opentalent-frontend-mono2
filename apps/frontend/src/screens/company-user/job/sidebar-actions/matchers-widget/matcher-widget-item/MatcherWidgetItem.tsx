import { ChatTypes } from 'components/chat';
import { StartChat } from 'components/chat/custom/start-chat';
import { useSnackbar } from 'notistack';
import React from 'react';
import { useHistory } from 'react-router-dom';
import { pathManager } from 'routes';
import { formatName } from 'utils/talent';

import {
  ListItem,
  ListItemAvatar,
  ListItemText,
  Avatar,
  Grid,
} from '@mui/material';
import { makeStyles } from '@mui/styles';

import {
  GetJobMatchersApplicationsDocument,
  JobMatcherApplicationStatusEnum,
  Talent,
  useApproveJobMacherApplicationMutation,
  useRejectJobMacherApplicationMutation,
  useRemoveJobMatcherFromJobMutation,
} from '@libs/graphql-types';
import Button from '@libs/ui/components/button';

interface MatcherWidgetItemProps {
  refresh: VoidFunction;
  status: JobMatcherApplicationStatusEnum;
  matcher: Talent;
  applicationId?: string;
  jobId: string;
}

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(1, 2),
    border: `1px solid ${theme.palette.divider}`,
    borderRadius: theme.spacing(2),
  },

  listItem: {
    paddingLeft: 0,
    cursor: 'pointer',
  },
}));

export const MatcherWidgetItem = ({
  status,
  matcher,
  applicationId,
  jobId,
}: MatcherWidgetItemProps) => {
  const history = useHistory();
  const { enqueueSnackbar } = useSnackbar();
  const [approve, { loading: approveInProgress }] =
    useApproveJobMacherApplicationMutation({
      onCompleted: () => {
        enqueueSnackbar('Application approved', { variant: 'success' });
      },
      refetchQueries: [GetJobMatchersApplicationsDocument],
    });
  const [reject, { loading: rejectInProgress }] =
    useRejectJobMacherApplicationMutation({
      onCompleted: () => {
        enqueueSnackbar('Application rejected', { variant: 'success' });
      },
      refetchQueries: [GetJobMatchersApplicationsDocument],
    });
  const [remove, { loading: removeInProgress }] =
    useRemoveJobMatcherFromJobMutation({
      onCompleted: () => {
        enqueueSnackbar('Application removed', { variant: 'success' });
      },
      refetchQueries: [GetJobMatchersApplicationsDocument],
    });

  const isLoadingFinal =
    removeInProgress || approveInProgress || rejectInProgress;
  const classes = useStyles();
  const isApproved = status === JobMatcherApplicationStatusEnum.Approved;
  const matcherId = matcher?.id;

  return (
    <div className={classes.root}>
      <Grid container wrap="nowrap">
        <Grid item flexGrow={1}>
          <ListItem
            alignItems="flex-start"
            key={matcher?.id}
            classes={{ root: classes.listItem }}
            onClick={() => {
              if (!matcherId) {
                return;
              }
              history.push(
                pathManager.company.talentProfile.generatePath({
                  id: matcherId,
                }),
              );
            }}
          >
            <ListItemAvatar>
              <Avatar alt="Remy Sharp" src={matcher?.avatar?.avatar} />
            </ListItemAvatar>
            <ListItemText
              primary={formatName({
                firstName: matcher?.first_name,
                lastName: matcher?.last_name,
              })}
              secondary={matcher?.location}
            />
          </ListItem>
        </Grid>
        <Grid item style={{ display: 'flex', alignItems: 'center' }}>
          <Grid container direction="column" alignItems="center" spacing={2}>
            {isApproved ? (
              <Grid item>
                <StartChat
                  fullWidth
                  variant="contained"
                  color="primary"
                  size="small"
                  chatType={ChatTypes.UserToTalent}
                  person={matcher}
                  disabled={isLoadingFinal}
                >
                  Message
                </StartChat>
              </Grid>
            ) : (
              <Grid item>
                <Button
                  fullWidth
                  disabled={isLoadingFinal}
                  variant="contained"
                  color="primary"
                  size="small"
                  onClick={() => approve({ variables: { id: applicationId! } })}
                >
                  Approve
                </Button>
              </Grid>
            )}

            {isApproved ? (
              <Grid item flexGrow={1} style={{ width: '100%' }}>
                <Button
                  fullWidth
                  variant="text"
                  color="info"
                  size="small"
                  disabled={isLoadingFinal}
                  onClick={() =>
                    remove({
                      variables: {
                        job_id: jobId,
                        matcher_id: matcher.id,
                      },
                    })
                  }
                >
                  Remove
                </Button>
              </Grid>
            ) : (
              <Grid item flexGrow={1} style={{ width: '100%' }}>
                <Button
                  fullWidth
                  disabled={isLoadingFinal}
                  variant="text"
                  color="info"
                  size="small"
                  onClick={() => reject({ variables: { id: applicationId! } })}
                >
                  Reject
                </Button>
              </Grid>
            )}
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};
