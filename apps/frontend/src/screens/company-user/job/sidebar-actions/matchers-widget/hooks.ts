import {
  Job,
  JobMatcherApplication,
  JobMatcherApplicationStatusEnum,
  useGetJobMatchersApplicationsQuery,
} from '@libs/graphql-types';

export const useGetMatchersApplicatioins = (job: Job) => {
  const { data, refetch } = useGetJobMatchersApplicationsQuery({
    variables: { job_id: job.id },
  });
  const applications = data?.jobMatcherApplications || [];
  const assignedMatchers = data?.jobMatchers || [];
  const hasNoApplication = !applications.length;
  const approvedApplications = applications.filter(
    (i) => i!.status === JobMatcherApplicationStatusEnum.Approved,
  ) as JobMatcherApplication[];
  const requestedApplications = applications.filter(
    (i) => i!.status === JobMatcherApplicationStatusEnum.Requested,
  ) as JobMatcherApplication[];

  return {
    approvedApplications,
    assignedMatchers,
    requestedApplications,
    refetch,
    hasNoApplication,
  };
};
