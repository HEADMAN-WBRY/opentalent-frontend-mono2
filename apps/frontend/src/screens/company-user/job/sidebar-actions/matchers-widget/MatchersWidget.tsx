import React from 'react';

import { Alert, Box, Card } from '@mui/material';
import { makeStyles } from '@mui/styles';

import {
  Job,
  JobMatcherApplicationStatusEnum,
  Talent,
} from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import { useGetMatchersApplicatioins } from './hooks';
import { MatcherWidgetItem } from './matcher-widget-item';

interface FindersFeeWidgetProps {
  job: Job;
}

const useStyles = makeStyles(() => ({
  wrap: {
    maxHeight: 420,
    overflowY: 'auto',
  },
}));

export const MatchersWidget = ({ job }: FindersFeeWidgetProps) => {
  const classes = useStyles();
  const {
    requestedApplications,
    approvedApplications,
    refetch,
    hasNoApplication,
    assignedMatchers,
  } = useGetMatchersApplicatioins(job);

  if (hasNoApplication) {
    return <></>;
  }

  return (
    <Card elevation={0}>
      <Box m={4} className={classes.wrap}>
        <Box mb={4}>
          <Typography variant="h6">Assign Talent Matchers</Typography>
        </Box>

        <Box mb={2}>
          <Typography variant="subtitle1">Assigned Talent Matchers</Typography>
        </Box>

        <Box mb={4}>
          {assignedMatchers.map((i) => (
            <Box mb={2} key={i?.talent?.id}>
              <MatcherWidgetItem
                refresh={refetch}
                status={JobMatcherApplicationStatusEnum.Approved}
                matcher={i!.talent as Talent}
                jobId={job.id}
              />
            </Box>
          ))}

          {!approvedApplications.length && (
            <Alert color="info">
              {`You don’t have any assigned Talent Matchers yet.`}
            </Alert>
          )}
        </Box>

        <Box mb={2}>
          <Typography variant="subtitle1">
            Talent Matcher aplications
          </Typography>
        </Box>

        <Box mb={2}>
          {requestedApplications.map((i) => (
            <Box mb={2}>
              <MatcherWidgetItem
                key={i.id}
                refresh={refetch}
                status={i.status}
                matcher={i.matcher as Talent}
                jobId={job.id}
                applicationId={i.id}
              />
            </Box>
          ))}
          {!requestedApplications.length && (
            <Alert color="info">
              {`You don’t have any applications from Talent Matchers`}
            </Alert>
          )}
        </Box>
      </Box>
    </Card>
  );
};
