import { useSnackbar } from 'notistack';
import React from 'react';
import { useState } from 'react';

import ExpandLessRoundedIcon from '@mui/icons-material/ExpandLessRounded';
import ExpandMoreRoundedIcon from '@mui/icons-material/ExpandMoreRounded';
import NavigateNextOutlinedIcon from '@mui/icons-material/NavigateNextOutlined';
import {
  Avatar,
  Box,
  Card,
  Collapse,
  Grid,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from '@mui/material';
import { makeStyles } from '@mui/styles';

import {
  JobTypeEnum,
  useUpdateJobFinderFeeMutation,
} from '@libs/graphql-types';
import { getRanges } from '@libs/helpers/common';
import { formatCurrency } from '@libs/helpers/format';
import Button from '@libs/ui/components/button';
import Select from '@libs/ui/components/form/select';
import Typography from '@libs/ui/components/typography';

import { GET_JOB_INFO } from '../../queries';
import moneyIcon from './money.svg';

interface FindersFeeWidgetProps {
  currentFee?: number;
  jobId: string;
  jobType: JobTypeEnum;
}

const useStyles = makeStyles(() => ({
  primaryText: {
    transform: ({ isCollapsed }: { isCollapsed: boolean }) =>
      isCollapsed ? 'translateY(10px)' : '',
    transition: 'all .4s',
    fontSize: 20,
  },
}));

const PERMANENT_OPTIONS = getRanges(1000, 15000).map((i) => ({
  text: formatCurrency(i),
  value: i,
}));

export const FindersFeeWidget = ({
  currentFee,
  jobId,
}: FindersFeeWidgetProps) => {
  const [value, setValue] = useState<number | null | undefined>(currentFee);
  const { enqueueSnackbar } = useSnackbar();
  const [isCollapsed, setIsCollapsed] = useState(false);
  const classes = useStyles({ isCollapsed });
  const [updateFee, { loading }] = useUpdateJobFinderFeeMutation({
    variables: {
      id: jobId,
    },
    refetchQueries: [{ query: GET_JOB_INFO, variables: { id: jobId } }],
    onCompleted: () => {
      enqueueSnackbar('Finders fee updated', { variant: 'success' });
    },
  });

  return (
    <Card elevation={0}>
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar alt="Remy Sharp" src={moneyIcon} />
        </ListItemAvatar>
        <ListItemText
          classes={{ primary: classes.primaryText }}
          primary={'Add Finder’s Fee'}
          secondary={
            <Collapse in={!isCollapsed}>
              <React.Fragment>
                <Typography
                  lineHeight="20px"
                  variant="body2"
                  color="text.primary"
                >
                  Ask the community to refer new candidates.
                </Typography>
              </React.Fragment>
            </Collapse>
          }
        />
      </ListItem>

      <Collapse in={!isCollapsed}>
        <Box px={4}>
          <Select
            name="finders_fee"
            options={PERMANENT_OPTIONS}
            fullWidth
            size="small"
            variant="filled"
            label="Pick a Finders fee"
            value={value}
            disabled={loading}
            helperText="No upfront fees apply"
            onChange={(e) => {
              const value = (e.target?.value as number) || null;
              setValue(value);
            }}
          />
        </Box>
      </Collapse>

      <Box p={4}>
        <Grid container spacing={4}>
          <Grid xs={6} item>
            <Button
              endIcon={<NavigateNextOutlinedIcon fontSize="small" />}
              fullWidth
              variant="contained"
              color="info"
              size="medium"
              onClick={() => {
                updateFee({
                  variables: {
                    id: jobId,
                    finders_fee: value as any,
                  },
                });
              }}
            >
              CONTINUE
            </Button>
          </Grid>
          <Grid xs={6} item>
            <Button
              fullWidth
              variant="outlined"
              color="info"
              endIcon={
                isCollapsed ? (
                  <ExpandMoreRoundedIcon />
                ) : (
                  <ExpandLessRoundedIcon />
                )
              }
              onClick={() => setIsCollapsed((i) => !i)}
            >
              {isCollapsed ? 'Learn more' : 'Show less'}
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Card>
  );
};
