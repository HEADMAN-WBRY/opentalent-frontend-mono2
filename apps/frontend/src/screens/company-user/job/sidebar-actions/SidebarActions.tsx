import React from 'react';

import { Box } from '@mui/material';

import { Job } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import { FindersFeeWidget } from './finders-fee-widget';
import { MatchersWidget } from './matchers-widget';
import { OpentalentProWidget } from './opentalen-pro-widget';

interface SidebarActionsProps {
  job: Job;
}

export const SidebarActions = ({ job }: SidebarActionsProps) => {
  return (
    <Box mt={6}>
      <Box mb={4}>
        <Typography color="info.main" variant="h6">
          Get more candidates 🔥
        </Typography>
        <Typography variant="body2" paragraph whiteSpace="break-spaces">
          {`Add these ‘no cure - no pay’ services
to get more inbound candidate leads.`}
        </Typography>
      </Box>

      <Box mt={4}>
        <FindersFeeWidget
          currentFee={job.finders_fee}
          jobId={job.id}
          jobType={job.type || 'FREELANCE'}
        />
      </Box>
      <Box mt={4} mb={4}>
        <OpentalentProWidget job={job} />
      </Box>

      <Box>
        <MatchersWidget job={job} />
      </Box>
    </Box>
  );
};
