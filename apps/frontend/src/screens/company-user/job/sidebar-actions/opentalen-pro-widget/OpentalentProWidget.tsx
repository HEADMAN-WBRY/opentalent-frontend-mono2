import React from 'react';
import { useState } from 'react';

import ExpandLessRoundedIcon from '@mui/icons-material/ExpandLessRounded';
import ExpandMoreRoundedIcon from '@mui/icons-material/ExpandMoreRounded';
import {
  Avatar,
  Box,
  Card,
  Collapse,
  Grid,
  ListItem,
  ListItemAvatar,
  ListItemText,
  TextField,
} from '@mui/material';
import { makeStyles } from '@mui/styles';

import { Job, JobTypeEnum } from '@libs/graphql-types';
import { EXTERNAL_LINKS } from '@libs/helpers/consts';
import Button from '@libs/ui/components/button';

import searchIcon from './search.svg';

interface OpentalentProWidgetProps {
  job: Job;
}

const useStyles = makeStyles(() => ({
  primaryText: {
    transform: ({ isCollapsed }: { isCollapsed: boolean }) =>
      isCollapsed ? 'translateY(10px)' : '',
    transition: 'all .4s',
    fontSize: 20,
  },
}));

const FIELD_TEXTS: Record<JobTypeEnum, { label: string; placeholder: string }> =
{
  FREELANCE: {
    label: "Commission (on top of candidate's rate)",
    placeholder: '€10/hour',
  },
  PERMANENT: {
    label: 'Commission (based on Annual Gross Salary)',
    placeholder: '15%',
  },
  PROJECT: {
    label: 'Commission (based on Annual Gross Salary)',
    placeholder: '15%',
  },
};

export const OpentalentProWidget = ({ job }: OpentalentProWidgetProps) => {
  const [isCollapsed, setIsCollapsed] = useState(false);
  const classes = useStyles({ isCollapsed });
  const jobType = job.type || 'FREELANCE';
  const texts = FIELD_TEXTS[jobType];

  return (
    <Card elevation={0}>
      <ListItem alignItems="flex-start">
        <ListItemAvatar>
          <Avatar alt="Remy Sharp" src={searchIcon} />
        </ListItemAvatar>
        <ListItemText
          classes={{ primary: classes.primaryText }}
          primary="OpenTalent RPO"
          secondary={
            <Collapse in={!isCollapsed}>
              <React.Fragment>
                Let Europe’s #1 recruiter network find you perfect candidates in
                record time.
              </React.Fragment>
            </Collapse>
          }
        />
      </ListItem>

      <Collapse in={!isCollapsed}>
        <Box px={4}>
          <TextField
            fullWidth
            size="small"
            variant="filled"
            label={texts.label}
            disabled
            value={texts.placeholder}
            helperText="No upfront fees apply"
          />
        </Box>
      </Collapse>

      <Box p={4}>
        <Grid container spacing={4}>
          <Grid xs={6} item>
            <Button
              href={EXTERNAL_LINKS.opentalentProMeeting}
              {...{ target: '_blank' }}
              fullWidth
              variant="contained"
              color="info"
              size="medium"
            >
              Book MEETING
            </Button>
          </Grid>
          <Grid xs={6} item>
            <Button
              fullWidth
              variant="outlined"
              color="info"
              endIcon={
                isCollapsed ? (
                  <ExpandMoreRoundedIcon />
                ) : (
                  <ExpandLessRoundedIcon />
                )
              }
              onClick={() => setIsCollapsed((i) => !i)}
            >
              {isCollapsed ? 'Learn more' : 'Show less'}
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Card>
  );
};
