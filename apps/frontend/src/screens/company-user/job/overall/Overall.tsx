import useMediaQueries from 'hooks/common/useMediaQueries';
import React, { useMemo } from 'react';
import { formatName } from 'utils/talent';

import DateRangeIcon from '@mui/icons-material/DateRange';
import HowToRegIcon from '@mui/icons-material/HowToReg';
import PeopleIcon from '@mui/icons-material/People';
import { Box, Grid, Paper, Tooltip } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { Job } from '@libs/graphql-types';
import { RouterButton } from '@libs/ui/components/button';
import {
  CampaignStatus,
  checkJobRemainHours,
  getDiffHours,
} from '@libs/ui/components/job/utils';
import Typography from '@libs/ui/components/typography';

import { MODAL_TYPES } from '../consts';
import { ExpandCampaignModalData } from '../expand-modal';
import Timer from './Timer';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    minWidth: 240,

    [theme.breakpoints.down('md')]: {
      'body &': {
        width: '100%',
      },
    },
  },
  icon: {
    color: theme.palette.grey[500],
  },
  calendar: {
    color: theme.palette.error.main,
    transform: 'translateY(6px)',
    marginLeft: theme.spacing(1),
    cursor: 'pointer',
  },
}));

interface OverallProps {
  job?: Job;
  campaignStatus?: CampaignStatus;
  currentTime: Date;
  hideTimer?: boolean;
}

const Overall = ({
  hideTimer = false,
  job,
  campaignStatus,
  currentTime,
}: OverallProps) => {
  const classes = useStyles();
  const { isSM } = useMediaQueries();
  const campaignDiffHours = getDiffHours(job?.campaign_end_date);
  const isAboutExpire = checkJobRemainHours(campaignDiffHours);
  const extendModalData: ExpandCampaignModalData = {
    jobId: job?.id || '',
    campaignEndDate: job?.campaign_end_date,
  };
  const DATA = useMemo(
    () => [
      {
        Icon: PeopleIcon,
        value: job?.matches_count,
        label: 'Total leads',
      },
      {
        Icon: HowToRegIcon,
        value: job?.matches_with_application_count,
        label: 'Applications',
      },
    ],
    [job],
  );

  return (
    <Paper elevation={0}>
      <Box p={isSM ? 4 : 6} className={classes.root}>
        <Typography variant="h6">Campaign summary</Typography>
        <Box>
          {!hideTimer && (
            <Box pt={isSM ? 4 : 6} mb={2}>
              <Typography variant="subtitle2">
                Time remained{' '}
                {isAboutExpire && (
                  <Tooltip
                    title={`This campaign will expire in less then ${campaignDiffHours} hours`}
                  >
                    <DateRangeIcon className={classes.calendar} />
                  </Tooltip>
                )}
              </Typography>
              <Box pt={2}>
                <Timer
                  campaignStatus={campaignStatus || CampaignStatus.NotStarted}
                  startDate={job?.campaign_start_date}
                  endDate={job?.campaign_end_date}
                  currentTime={currentTime}
                  expired={isAboutExpire}
                />
              </Box>
            </Box>
          )}

          {isAboutExpire && (
            <Box>
              <RouterButton
                to={{
                  state: { [MODAL_TYPES.EXPAND]: extendModalData },
                }}
                variant="outlined"
              >
                Extend campaign
              </RouterButton>
            </Box>
          )}
        </Box>
        <Box pt={isSM ? 4 : 6}>
          <Typography variant="subtitle2">Results</Typography>
          <Box pt={2}>
            {DATA.map(({ Icon, value, label }) => (
              <Grid
                key={label}
                wrap="nowrap"
                spacing={2}
                alignItems="center"
                container
              >
                <Grid display="flex" component={Box} item>
                  <Icon
                    className={classes.icon}
                    fontSize={isSM ? 'small' : 'inherit'}
                  />
                </Grid>
                <Grid item>
                  <Typography
                    component="span"
                    variant={isSM ? 'body2' : 'h6'}
                    color="tertiary"
                  >
                    {value}
                  </Typography>
                </Grid>
                <Grid item>
                  <Typography
                    component="span"
                    variant={isSM ? 'caption' : 'body2'}
                  >
                    {label}
                  </Typography>
                </Grid>
              </Grid>
            ))}
          </Box>
        </Box>
        <Box pt={isSM ? 4 : 6}>
          <Typography variant="subtitle2">Campaign owner</Typography>
          <Box pt={isSM ? 1 : 2}>
            <Typography component="span" color="tertiary" variant="body2">
              {formatName({
                firstName: job?.campaign_owner?.first_name,
                lastName: job?.campaign_owner?.last_name,
              })}
            </Typography>
            {/* &nbsp;
            <Typography component="span" variant="body2">
              (you)
            </Typography> */}
          </Box>
        </Box>
      </Box>
    </Paper>
  );
};

export default Overall;
