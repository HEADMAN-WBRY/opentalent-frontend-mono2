import cn from 'classnames';
import React from 'react';
import { useIntervalTimer } from 'screens/talent/job-apply/job-info/hooks';

import { Box, Grid, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { CampaignStatus } from '@libs/ui/components/job/utils';

interface TimerProps {
  startDate: string;
  endDate: string;
  campaignStatus: CampaignStatus;
  currentTime: Date;
  expired?: boolean;
}

const useStyles = makeStyles((theme) => ({
  timeText: {
    width: 42,
    display: 'inline-flex',
    color: ({ expired }: { expired: boolean }) =>
      expired ? theme.palette.error.main : theme.palette.text.secondary,
  },
  divider: {
    lineHeight: '30px',
    color: ({ expired }: { expired: boolean }) =>
      expired ? theme.palette.error.main : theme.palette.text.secondary,
  },
  dividerLeft: {
    paddingRight: 4,
  },
  dividerRight: {
    paddingLeft: 4,
  },
  labels: {
    opacity: 0.6,
  },
  minutes: {
    width: 36,
    position: 'relative',
    left: -4,
    display: 'inline-flex',
  },
}));

const formatTimeValue = (val = 0, status: CampaignStatus) =>
  status === CampaignStatus.Finished ? '00' : val.toString().padStart(2, '0');

const Timer = ({
  endDate,
  startDate,
  campaignStatus,
  currentTime,
  expired = false,
}: TimerProps) => {
  const classes = useStyles({ expired });
  const { duration, daysDiff } = useIntervalTimer({
    start: startDate,
    end: endDate,
    currentTime,
  });

  return (
    <Grid spacing={1} container>
      <Grid item>
        <Grid alignItems="center" container direction="column">
          <Grid item>
            <Typography
              className={classes.timeText}
              variant="h4"
              color="textSecondary"
            >
              {formatTimeValue(daysDiff, campaignStatus)}
            </Typography>
          </Grid>
          <Grid item>
            <Typography
              className={classes.labels}
              variant="caption"
              color="textSecondary"
            >
              Days
            </Typography>
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Grid alignItems="center" container direction="column">
          <Grid display="flex" component={Box} item>
            <Typography
              className={cn(classes.divider, classes.dividerLeft)}
              component="span"
              variant="h4"
              color="textSecondary"
            >
              :
            </Typography>
            <Typography
              className={classes.timeText}
              component="span"
              variant="h4"
              color="textSecondary"
            >
              {formatTimeValue(duration?.hours, campaignStatus)}
            </Typography>
            <Typography
              className={cn(classes.divider, classes.dividerRight)}
              component="span"
              variant="h4"
              color="textSecondary"
            >
              :
            </Typography>
          </Grid>
          <Grid item>
            <Typography
              className={classes.labels}
              variant="caption"
              color="textSecondary"
            >
              Hours
            </Typography>
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Grid alignItems="center" container direction="column">
          <Grid item>
            <Typography variant="h4" className={classes.timeText}>
              {formatTimeValue(duration?.minutes, campaignStatus)}
            </Typography>
          </Grid>
          <Grid item>
            <Typography
              className={cn(classes.minutes, classes.labels)}
              variant="caption"
              color="textSecondary"
            >
              Minutes
            </Typography>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default React.memo(Timer);
