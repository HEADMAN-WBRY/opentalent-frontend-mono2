import { useQuery } from '@apollo/client';

import {
  Query,
  QueryJobMatchesArgs,
  SourceTypeEnum,
} from '@libs/graphql-types';

import { GET_JOB_MATCHES_INFO } from './queries';

interface GetMatchesDataParams extends QueryJobMatchesArgs { }

export const useGetMatchesData = ({
  job_id,
  location,
  match_type,
  source_type,
}: GetMatchesDataParams) => {
  const { data, loading, refetch } = useQuery<Query, QueryJobMatchesArgs>(
    GET_JOB_MATCHES_INFO,
    {
      variables: {
        job_id,
        location: location || undefined,
        match_type,
        source_type,
      },
      nextFetchPolicy: 'network-only',
      pollInterval: 5000,
    },
  );

  const ownCommunityMatches = (data as any)?.ownCommunityMatches?.length || 0;
  const opentalentCommunityMatches =
    (data as any)?.opentalentCommunityMatches?.length || 0;

  const COMMUNITY_OPTIONS = [
    {
      text: `My Community (${ownCommunityMatches})`,
      value: SourceTypeEnum.Own,
    },
    {
      text: `OpenTalent (${opentalentCommunityMatches})`,
      value: SourceTypeEnum.Opentalent,
    },
  ];

  return {
    data,
    loading,
    ownCommunityMatches,
    opentalentCommunityMatches,
    communityOptions: COMMUNITY_OPTIONS,
    reloadMatchesData: refetch,
  };
};
