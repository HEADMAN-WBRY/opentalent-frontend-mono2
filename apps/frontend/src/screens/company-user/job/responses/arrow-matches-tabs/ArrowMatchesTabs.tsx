import { usePushWithQuery } from 'hooks/routing';
import React, { useCallback, useMemo } from 'react';

import { JobMatchTypeEnum } from '@libs/graphql-types';
import ArrowTabs from '@libs/ui/components/arrow-tabs';
import { TabItemsType } from '@libs/ui/components/arrow-tabs/types';

import { MATCHES_TABS } from '../../consts';

interface ArrowMatchesTabsProps {
  value: JobMatchTypeEnum;
  count: Partial<Record<JobMatchTypeEnum | string, number>>;
  className?: string;
}

const ArrowMatchesTabs = ({
  value,
  count,
  className,
}: ArrowMatchesTabsProps) => {
  const push = usePushWithQuery();
  const onChange = useCallback(
    (match: string) => push({ query: { match } }),
    [push],
  );
  const items = useMemo<TabItemsType[]>(
    () => [
      {
        children: `Instant matches\n(${count[MATCHES_TABS.instant] || 0})`,
        id: MATCHES_TABS.instant,
      },
      {
        children: `Applied\n(${count[MATCHES_TABS.talent] || 0})`,
        id: MATCHES_TABS.talent,
      },
      {
        children: `Invited\n(${count[MATCHES_TABS.invited] || 0})`,
        id: MATCHES_TABS.invited,
      },
      {
        children: `Intake\n(${count[MATCHES_TABS.intake] || 0})`,
        id: MATCHES_TABS.intake,
      },
      {
        children: `Rejected\n(${count[MATCHES_TABS.rejected] || 0})`,
        id: MATCHES_TABS.rejected,
      },
      {
        children: `Withdrawn\n(${count[MATCHES_TABS.withdrawn] || 0})`,
        id: MATCHES_TABS.withdrawn,
      },
      {
        children: `Hired\n(${count[MATCHES_TABS.hired] || 0})`,
        id: MATCHES_TABS.hired,
        bg: 'primary',
      },
    ],
    [count],
  );

  return (
    <ArrowTabs
      className={className}
      currentActive={value}
      items={items}
      onChange={onChange}
    />
  );
};

export default ArrowMatchesTabs;
