import { LOCAL_STORAGE_KEYS } from 'consts/ls-keys';
import { useCurrentUserCompanyId } from 'hooks/company';
import { useIsFreeCompanyAccount } from 'hooks/company/useCompanyAccountType';
import React, { useMemo } from 'react';
import { useState } from 'react';
import { useLocation } from 'react-router-dom';
import { parse } from 'utils/querystring';

import { Box, Collapse } from '@mui/material';
import { makeStyles } from '@mui/styles';

import {
  Job,
  JobMatch,
  JobMatchTypeEnum,
  SourceTypeEnum,
} from '@libs/graphql-types';
import Typography, { RouterLink } from '@libs/ui/components/typography';

import { MATCHES_TABS } from '../consts';
import { CandidateCardsDirectBanner } from '../direct-banners';
import { useEuropeFilterSearch } from '../hooks';
import { InProgressCard } from './InProgressCard';
import NoResponses from './NoResponses';
import ArrowMatchesTabs from './arrow-matches-tabs';
import { useGetMatchesData } from './hooks';
import MatchesFilter from './matches-filter';
import Response from './response';
import { useCountryOptionsFromMatches } from './response/utils';

interface ResponsesProps {
  job?: Job;
  reloadJobData?: () => void;
  location?: string;
  sourceType?: SourceTypeEnum;
}

type TabsValues = typeof MATCHES_TABS[keyof typeof MATCHES_TABS];

const sortMatches = (matches: JobMatch[] = []) =>
  matches.reduce<Record<TabsValues, JobMatch[]>>(
    (acc, match) => {
      if (match.match_type === JobMatchTypeEnum.InstantMatch) {
        acc[MATCHES_TABS.instant].push(match);
      }
      if (match.match_type === JobMatchTypeEnum.Invited) {
        acc[MATCHES_TABS.invited].push(match);
      }
      if (match.match_type === JobMatchTypeEnum.TalentApplication) {
        acc[MATCHES_TABS.talent].push(match);
      }
      if (match.is_shortlist) {
        acc[MATCHES_TABS.saved].push(match);
      }
      if (match.match_type === JobMatchTypeEnum.Rejected) {
        acc[MATCHES_TABS.rejected].push(match);
      }
      if (match.match_type === JobMatchTypeEnum.Hired) {
        acc[MATCHES_TABS.hired].push(match);
      }
      if (match.match_type === JobMatchTypeEnum.Withdrawn) {
        acc[MATCHES_TABS.withdrawn].push(match);
      }
      if (match.match_type === JobMatchTypeEnum.Intake) {
        acc[MATCHES_TABS.intake].push(match);
      }

      return acc;
    },
    {
      [MATCHES_TABS.instant]: [],
      [MATCHES_TABS.talent]: [],
      [MATCHES_TABS.saved]: [],
      [MATCHES_TABS.invited]: [],
      [MATCHES_TABS.hired]: [],
      [MATCHES_TABS.withdrawn]: [],
      [MATCHES_TABS.rejected]: [],
      [MATCHES_TABS.intake]: [],
    },
  );

const useStyles = makeStyles((theme) => ({
  tabs: {
    display: 'flex',
    flexWrap: 'wrap',
    paddingRight: theme.spacing(2),

    '& > button:first-child': {
      flexGrow: 1,
    },
  },
}));

const Responses = ({
  job,
  location,
  reloadJobData,
  sourceType = SourceTypeEnum.Own,
}: ResponsesProps) => {
  const { search } = useLocation();
  const companyId = useCurrentUserCompanyId();
  const classes = useStyles();
  const isFreeCompanyAccount = useIsFreeCompanyAccount();
  const [showBanner, setShowBanner] = useState(
    isFreeCompanyAccount &&
    !localStorage.getItem(LOCAL_STORAGE_KEYS.companyDirectJobPagePopup),
  );
  const europeSearch = useEuropeFilterSearch();
  const isInProgress = !!job?.is_instant_matching_in_progress;
  const { match = MATCHES_TABS.instant } = parse(search.slice(1));
  const currentMatchType = match as TabsValues;
  const allSorted: Record<TabsValues, JobMatch[]> = useMemo(
    () => sortMatches((job?.matches as JobMatch[]) || []),
    [job],
  );
  const { options: countryOptions, allMatchesCount } =
    useCountryOptionsFromMatches(job?.matches as JobMatch[]);
  const currentMatches = allSorted[currentMatchType];
  const isInstantMatch = currentMatchType === 'INSTANT_MATCH';

  const { communityOptions, reloadMatchesData } = useGetMatchesData({
    job_id: job?.id,
    location,
  });

  const refresh = () => {
    reloadJobData?.();
    reloadMatchesData?.();
  };

  const currentFilteredMatches = useMemo(
    () =>
      isInstantMatch
        ? currentMatches.filter((i) => {
          const fromLocation = !!location
            ? i.talent?.location === location
            : true;
          const isInPool = i.talent?.company_pool_connections?.some(
            (c) => c?.company?.id === companyId && c?.status === 'APPROVED',
          );

          if (sourceType === SourceTypeEnum.Own) {
            return fromLocation && isInPool;
          }

          return fromLocation && !isInPool;
        })
        : currentMatches,
    [companyId, currentMatches, isInstantMatch, location, sourceType],
  );
  const noResponsesInThisCountry =
    !!location &&
    isInstantMatch &&
    !!currentFilteredMatches.length &&
    currentFilteredMatches.length < 5;

  return (
    <Box>
      <Box mb={3}>
        <ArrowMatchesTabs
          className={classes.tabs}
          count={{
            [MATCHES_TABS.saved]: allSorted[MATCHES_TABS.saved].length,
            [MATCHES_TABS.instant]: allSorted[MATCHES_TABS.instant].length,
            [MATCHES_TABS.talent]: allSorted[MATCHES_TABS.talent].length,
            [MATCHES_TABS.invited]: allSorted[MATCHES_TABS.invited].length,
            [MATCHES_TABS.rejected]: allSorted[MATCHES_TABS.rejected].length,
            [MATCHES_TABS.withdrawn]: allSorted[MATCHES_TABS.withdrawn].length,
            [MATCHES_TABS.hired]: allSorted[MATCHES_TABS.hired].length,
            [MATCHES_TABS.intake]: allSorted[MATCHES_TABS.intake].length,
          }}
          value={match as JobMatchTypeEnum}
        />
      </Box>

      {isInstantMatch && (
        <Box mb={6}>
          <MatchesFilter
            allMatchesCount={allMatchesCount}
            countryOptions={countryOptions}
            communityOptions={communityOptions}
            location={location}
          />
        </Box>
      )}

      {isInProgress && <InProgressCard />}

      {noResponsesInThisCountry && (
        <Box>
          <Typography variant="body1" color="textSecondary">
            Want to see more candidates? Filter by{' '}
            <RouterLink color="info.main" to={{ search: europeSearch }}>
              Europe
            </RouterLink>
            .
          </Typography>
        </Box>
      )}

      {currentFilteredMatches.map((jobMatch, index) => (
        <>
          <Box mt={6} key={jobMatch.id}>
            <Response
              refresh={refresh}
              job={job as Job}
              jobMatch={jobMatch}
              isSaved={allSorted[MATCHES_TABS.saved].some(
                (i) => jobMatch.talent?.id === i.talent?.id,
              )}
            />
            {!index && (
              <Collapse in={showBanner}>
                <Box mt={6}>
                  <CandidateCardsDirectBanner
                    candidatesCount={allMatchesCount}
                    onHide={() => {
                      localStorage.setItem(
                        LOCAL_STORAGE_KEYS.companyDirectJobPagePopup,
                        'true',
                      );
                      setShowBanner((i) => !i);
                    }}
                  />
                </Box>
              </Collapse>
            )}
          </Box>
        </>
      ))}
      {!currentFilteredMatches.length && (
        <Box mt={6}>
          <NoResponses matchType={match as JobMatchTypeEnum | string} />
        </Box>
      )}
    </Box>
  );
};

export default Responses;
