import FormikAutoSaving from 'components/form/formik/FormikAutoSave';
import { Formik } from 'formik';
import { usePushWithQuery } from 'hooks/routing';
import React, { useEffect, useState } from 'react';

import { Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { SourceTypeEnum } from '@libs/graphql-types';
import { ConnectedSelect, OptionType } from '@libs/ui/components/form/select';

interface MatchesFilterProps {
  location?: string;
  countryOptions?: OptionType[];
  communityOptions?: OptionType[];
  allMatchesCount?: number;
}

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 600,

    '& .MuiInputBase-root': {
      background: 'transparent',
    },
  },
}));

const MatchesFilter = ({
  location = '',
  countryOptions = [],
  allMatchesCount = 0,
  communityOptions = [],
}: MatchesFilterProps) => {
  const [finalLocation, setFinalLocation] = useState(location);
  const classes = useStyles();
  const push = usePushWithQuery();

  useEffect(() => {
    const noLocation =
      !!finalLocation &&
      countryOptions?.every((i) => i.value !== finalLocation);

    if (noLocation) {
      setFinalLocation('');
    }
  }, [countryOptions, finalLocation]);

  return (
    <Formik
      enableReinitialize
      initialValues={{
        location: finalLocation,
        source_type: SourceTypeEnum.Own,
      }}
      onSubmit={(query) => push({ query, replace: true })}
    >
      {({ submitForm }) => (
        <div className={classes.root}>
          <FormikAutoSaving debounceMs={1000} onValuesChange={submitForm} />
          <Grid container spacing={4}>
            <Grid item>
              <ConnectedSelect
                name="location"
                options={countryOptions}
                fullWidth
                variant="standard"
                label="Location"
                displayEmpty
                noneItemText={`Europe (${allMatchesCount})`}
                labelShrink
              />
            </Grid>
            <Grid item>
              <ConnectedSelect
                name="source_type"
                options={communityOptions}
                fullWidth
                hideNoneValue
                variant="standard"
                label="Community"
                labelShrink
              />
            </Grid>
          </Grid>
        </div>
      )}
    </Formik>
  );
};

export default MatchesFilter;
