import { Formik } from 'formik';
import React from 'react';
import {
  DefaultModalProps,
  useOpenModal,
  withLocationStateModal,
} from 'utils/modals';

import { Box, Button, Grid } from '@mui/material';

import { ConnectedSelect } from '@libs/ui/components/form/select';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import { JOB_MATCH_DECLINE_REASONS } from '@libs/ui/components/job/utils';
import { DefaultModal } from '@libs/ui/components/modals';
import Typography from '@libs/ui/components/typography';

import { MODAL_TYPES } from '../../consts';
import { useDeclineAction } from './hooks';
import { DeclineModalData, FormState } from './types';

interface DeclineModalProps extends DefaultModalProps<DeclineModalData> {
  jobId: string;
}

const REASONS_OPTIONS = Object.entries(JOB_MATCH_DECLINE_REASONS).map(
  ([value, text]) => ({ value, text }),
);

const DeclineModalComponent = ({
  jobId,
  close,
  isOpen,
  modalData,
}: DeclineModalProps) => {
  const matchId = modalData?.matchId || '';
  const firstName = modalData?.firstName || 'a candidate';
  const { onSubmit, loading } = useDeclineAction({
    jobId,
    matchId,
    onClose: close,
  });

  return (
    <Formik<FormState> initialValues={{}} onSubmit={onSubmit}>
      {({ handleSubmit }) => (
        <DefaultModal
          actions={
            <Grid spacing={4} container>
              <Grid xs={6} item>
                <Button
                  fullWidth
                  color="primary"
                  variant="contained"
                  onClick={() => handleSubmit()}
                  disabled={loading}
                >
                  Decline
                </Button>
              </Grid>
              <Grid xs={6} item>
                <Button
                  fullWidth
                  variant="outlined"
                  color="secondary"
                  onClick={close}
                  disabled={loading}
                >
                  Cancel
                </Button>
              </Grid>
            </Grid>
          }
          handleClose={close}
          open={isOpen}
          title={`You are going to decline ${firstName}?`}
        >
          <Box>
            <Typography paragraph>
              Please confirm. Select the reason why are you declining. You can
              also write a message
            </Typography>
          </Box>

          <Box pb={4}>
            <ConnectedSelect
              name="reason"
              options={REASONS_OPTIONS}
              fullWidth
              variant="filled"
              label="Reason"
              formControlProps={{ size: 'small' }}
              style={{ textAlign: 'left' }}
            />
          </Box>

          <Box>
            <ConnectedTextField
              name="message"
              fullWidth
              variant="filled"
              label="Message"
              multiline
              rows={4}
            />
          </Box>
        </DefaultModal>
      )}
    </Formik>
  );
};

export const DeclineModal = withLocationStateModal<DeclineModalData>({
  id: MODAL_TYPES.DECLINE,
})(DeclineModalComponent);

export const useOpenDeclineModal = () =>
  useOpenModal<DeclineModalData>(MODAL_TYPES.DECLINE);
