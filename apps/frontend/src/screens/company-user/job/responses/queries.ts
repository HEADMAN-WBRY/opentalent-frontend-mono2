import { gql } from '@apollo/client';

export const GET_JOB_MATCHES_INFO = gql`
  query GetJobMatchesInfo($job_id: ID) {
    ownCommunityMatches: jobMatches(
      job_id: $job_id
      source_type: OWN
      match_type: INSTANT_MATCH
    ) {
      id
    }
    opentalentCommunityMatches: jobMatches(
      job_id: $job_id
      source_type: OPENTALENT
      match_type: INSTANT_MATCH
    ) {
      id
    }
  }
`;
