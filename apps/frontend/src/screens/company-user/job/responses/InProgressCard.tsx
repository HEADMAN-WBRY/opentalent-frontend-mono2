import React from 'react';

import { Box, LinearProgress } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

interface InProgressCardProps {}

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(10),
    backgroundColor: 'white',
    borderRadius: 4,
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  text: {
    marginBottom: theme.spacing(5),
  },
  progressContainer: {
    flexGrow: 1,
    width: '100%',
    maxWidth: 300,
  },
}));

export const InProgressCard = (props: InProgressCardProps) => {
  const classes = useStyles();

  return (
    <Box p={10} className={classes.root}>
      <Typography
        className={classes.text}
        variant="body1"
        color="textSecondary"
      >
        Hold on as we’re finding the best candidates for this role.
      </Typography>
      <Box className={classes.progressContainer}>
        <LinearProgress color="info" />
      </Box>
    </Box>
  );
};
