import { TalentPoolActions } from 'components/custom/company/talent-pool-actions';
import { useGetCompanyPoolConnectionToTalent } from 'hooks/company';
import React from 'react';
import { useHistory } from 'react-router-dom';
import { useToggle } from 'react-use';
import { pathManager } from 'routes';
import { MatchLabel } from 'screens/talent/job-board-v2/content/job-card/MatchLabel';

import { Box, Grid, Paper } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { Job, JobMatch, Talent } from '@libs/graphql-types';
import { stopEvent } from '@libs/helpers/common';

import MainInfo from './MainInfo';
import UserBlock from './UserBlock';
import { useOnViewAction } from './hooks';
import NewActionsBlock from './new-actions-block';

interface ResponseProps {
  refresh?: () => void;
  jobMatch: JobMatch;
  isSaved: boolean;
  job: Job;
}

const useStyles = makeStyles((theme) => ({
  mainInfo: {
    paddingLeft: '112px',

    [theme.breakpoints.down('md')]: {
      'body &': {
        paddingLeft: 0,
      },
    },
  },
  userInfo: {
    [theme.breakpoints.down('md')]: {
      'body &': {
        flexDirection: 'column',
      },
    },
  },
  paper: {
    position: 'relative',
    cursor: 'pointer',
  },
  actionsWrapper: {
    position: 'absolute',
    top: 16,
    right: 16,
  },
}));

const Response = ({ jobMatch, isSaved, job, refresh }: ResponseProps) => {
  const classes = useStyles();
  const history = useHistory();
  const [isPitchShown, togglePitch] = useToggle(false);
  const { talent } = jobMatch;
  const isInPool = !!useGetCompanyPoolConnectionToTalent({ talent });
  const isDraft = !!job?.is_draft;
  const isNewMatch = jobMatch.is_new;
  const { ref } = useOnViewAction({ isNewMatch, matchId: jobMatch.id });
  const matchQuality = jobMatch?.match_quality;

  return (
    <div
      onClick={() =>
        history.push(
          pathManager.company.talentProfile.generatePath({
            id: talent?.id || '',
          }),
        )
      }
    >
      <Paper
        data-test-id="response-card"
        className={classes.paper}
        elevation={0}
        ref={ref}
      >
        <Box p={4}>
          <Grid
            className={classes.userInfo}
            wrap="nowrap"
            justifyContent="space-between"
            container
          >
            <Grid item>
              <UserBlock talent={talent} isNewMatch={isNewMatch} />
            </Grid>
            <Grid className={classes.actionsWrapper} item onClick={stopEvent}>
              {isInPool ? (
                <NewActionsBlock
                  disabled={isDraft}
                  jobId={job.id}
                  jobMatch={jobMatch}
                  isSaved={isSaved}
                  togglePitch={togglePitch}
                />
              ) : (
                <TalentPoolActions
                  talent={talent as Talent}
                  refetch={refresh}
                />
              )}
            </Grid>
            {!!matchQuality && (
              <Grid
                style={{
                  marginTop: 48,
                  marginRight: 8,
                }}
                item
              >
                <MatchLabel matchQuality={matchQuality} />
              </Grid>
            )}
          </Grid>
          <Box className={classes.mainInfo} mt={2} pl={10}>
            <MainInfo isPitchShown={isPitchShown} jobMatch={jobMatch} />
          </Box>
        </Box>
      </Paper>
    </div>
  );
};

export default Response;
