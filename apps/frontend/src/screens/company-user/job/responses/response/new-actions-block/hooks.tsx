import { useMutation } from '@apollo/client';
import { useSnackbar } from 'notistack';
import React, { useCallback } from 'react';

import IntakeIcon from '@mui/icons-material/AssignmentOutlined';
import BlockIcon from '@mui/icons-material/BlockOutlined';
import RemoveIcon from '@mui/icons-material/HighlightOffOutlined';
import MailIcon from '@mui/icons-material/Mail';
import PersonAddIcon from '@mui/icons-material/PersonAddOutlined';
import HireIcon from '@mui/icons-material/WorkOutlineOutlined';

import {
  JobMatchTypeEnum,
  Mutation,
  MutationHireJobMatchArgs,
  useNotifyTalentAboutJobMatchMutation,
} from '@libs/graphql-types';

import { GET_JOB_INFO, HIRE_TO_JOB, INTAKE_TO_JOB } from '../../../queries';
import { useOpenDeclineModal } from '../../decline-modal';
import { useOpenInviteModal } from '../../invite-modal';
import { useOnRemoveModal } from '../../remove-response-modal';
import { ActionOptionType, NewActionsBlockProps } from './types';

export const useChildMenuAction = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const open = Boolean(anchorEl);

  const onClick = (event: any) => {
    if (!anchorEl) {
      setAnchorEl(event.currentTarget);
    } else {
      setAnchorEl(null);
    }
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return {
    open,
    handleClose,
    anchorEl,
    onClick,
  };
};

export const useOnInvite = (talentId: string, firstName: string) => {
  const openInviteModal = useOpenInviteModal();
  const onInvite = useCallback(
    () => openInviteModal({ talentId, firstName }),
    [openInviteModal, talentId, firstName],
  );

  return onInvite;
};

export const useOnDecline = (matchId: string, firstName: string) => {
  const openDeclineModal = useOpenDeclineModal();
  const onDecline = useCallback(
    () => openDeclineModal({ matchId, firstName }),
    [matchId, openDeclineModal, firstName],
  );

  return onDecline;
};

export const useOnRemove = (matchId: string) => {
  const openRemoveModal = useOnRemoveModal();
  const onRemove = useCallback(
    () => openRemoveModal({ matchId }),
    [matchId, openRemoveModal],
  );

  return onRemove;
};

const useOnHire = (matchId: string, jobId: string) => {
  const { enqueueSnackbar } = useSnackbar();
  const [hire] = useMutation<Mutation, MutationHireJobMatchArgs>(HIRE_TO_JOB, {
    onCompleted: () => {
      enqueueSnackbar('Success!', { variant: 'success' });
    },
    refetchQueries: [{ query: GET_JOB_INFO, variables: { id: jobId } }],
  });

  const hireWithoutContactHandler = useCallback(
    () => hire({ variables: { job_match_id: matchId } }),
    [hire, matchId],
  );
  const hireWithContactHandler = useCallback(
    () => hire({ variables: { job_match_id: matchId, with_contract: true } }),
    [hire, matchId],
  );

  return { hireWithoutContactHandler, hireWithContactHandler };
};

const useOnIntake = (matchId: string, jobId: string) => {
  const { enqueueSnackbar } = useSnackbar();
  const [mutate] = useMutation<Mutation, MutationHireJobMatchArgs>(
    INTAKE_TO_JOB,
    {
      onCompleted: () => {
        enqueueSnackbar('Success!', { variant: 'success' });
      },
      refetchQueries: [{ query: GET_JOB_INFO, variables: { id: jobId } }],
    },
  );

  return useCallback(
    () => mutate({ variables: { job_match_id: matchId } }),
    [mutate, matchId],
  );
};

const useNotifyTalentAboutMatch = (matchId: string, jobId: string) => {
  const { enqueueSnackbar } = useSnackbar();
  const [send] = useNotifyTalentAboutJobMatchMutation({
    variables: {
      match_id: matchId,
    },
    onCompleted: () => {
      enqueueSnackbar('Notification sent', { variant: 'success' });
    },
    refetchQueries: [{ query: GET_JOB_INFO, variables: { id: jobId } }],
  });

  return send;
};

export const useMenuActions = ({
  jobId,
  jobMatch,
  isSaved,
}: NewActionsBlockProps): ActionOptionType[] => {
  const jobMatchId = jobMatch.id;
  const { hireWithContactHandler, hireWithoutContactHandler } = useOnHire(
    jobMatchId,
    jobId,
  );
  const notifyTalent = useNotifyTalentAboutMatch(jobMatchId, jobId);
  const onIntake = useOnIntake(jobMatchId, jobId);
  const firstName = jobMatch.talent?.first_name || '';
  const onInvite = useOnInvite(jobMatch.talent?.id || '', firstName);
  // const { action: saveAction, isLoading } = useSaveAction({ id: jobId });
  // const saveOption = {
  //   text: !isSaved ? 'Add to shortlist' : 'Remove from shortlist',
  //   Icon: !isSaved ? StarBorderIcon : StarIcon,
  //   onClick: () => saveAction({ matchId: jobMatch.id, isSaved }),
  //   isLoading,
  // };
  const inviteOption = {
    text: 'Invite to Apply',
    Icon: PersonAddIcon,
    onClick: onInvite,
  };

  const onRemove = useOnRemove(jobMatch.id);
  const removeOption = {
    text: 'Remove',
    Icon: RemoveIcon,
    onClick: onRemove,
  };

  const onDecline = useOnDecline(jobMatch.id, firstName);
  const declineOption = {
    text: 'Decline',
    Icon: BlockIcon,
    onClick: onDecline,
  };

  const hireWithoutContact = {
    text: 'Contract by myself',
    onClick: hireWithoutContactHandler,
  };

  const hireWithContact = {
    text: 'Contract through OpenTalent',
    onClick: hireWithContactHandler,
  };

  const hireOption = {
    text: 'Hire',
    Icon: HireIcon,
    childActions: [hireWithContact, hireWithoutContact],
  };

  const intakeOption = {
    text: 'Add to intake',
    Icon: IntakeIcon,
    onClick: onIntake,
  };

  const isNotified = jobMatch?.is_talent_notified;

  const notifyTalentOption = {
    text: isNotified ? 'Notified' : 'Notify about match',
    Icon: MailIcon,
    onClick: notifyTalent,
    disabled: isNotified,
  };

  if (jobMatch.match_type === JobMatchTypeEnum.InstantMatch) {
    return [
      inviteOption,
      removeOption,
      intakeOption,
      hireOption,
      notifyTalentOption,
    ];
  }

  if (jobMatch.match_type === JobMatchTypeEnum.TalentApplication) {
    return [intakeOption, hireOption, declineOption];
  }

  if (jobMatch.match_type === JobMatchTypeEnum.Invited) {
    return [intakeOption, hireOption, declineOption];
  }

  if (jobMatch.match_type === JobMatchTypeEnum.Intake) {
    return [hireOption, declineOption];
  }

  return [];
};
