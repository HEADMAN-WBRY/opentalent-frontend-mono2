import cn from 'classnames';
import { useMenuAction } from 'hooks/common/useMenuAction';
import React from 'react';
import { noop } from 'utils/common';

import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import {
  Badge,
  Grid,
  IconButton,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
} from '@mui/material';

import Button from '@libs/ui/components/button';

import { useChildMenuAction, useMenuActions } from './hooks';
import { useStyles } from './styles';
import { ActionOptionType, NewActionsBlockProps } from './types';
import { getMenuTitle, isDisabled } from './utils';

const NewActionsBlock = (props: NewActionsBlockProps) => {
  const { jobMatch, togglePitch, disabled } = props;
  const { open, handleClick, handleClose, anchorEl } = useMenuAction();
  const {
    open: openCh,
    handleClose: handleCloseCh,
    anchorEl: anchorElCh,
    onClick: handleClickCh,
  } = useChildMenuAction();
  const actions = useMenuActions(props);
  const pitch = jobMatch.job_application?.pitch;
  const classes = useStyles();

  const renderDefaultItem = ({
    text,
    Icon,
    onClick = noop,
    isLoading,
    childActions,
    disabled,
  }: ActionOptionType) => {
    return (
      <MenuItem
        key={text}
        disabled={isLoading || disabled}
        onClick={(event) => {
          onClick();

          if (childActions) {
            handleClickCh(event);
          } else {
            handleClose();
          }
        }}
        style={{ paddingTop: '2px', paddingBottom: '2px' }}
      >
        {Icon && (
          <ListItemIcon>
            <Icon fontSize="inherit" />
          </ListItemIcon>
        )}
        <ListItemText primary={text} />
        {childActions && (
          <ArrowRightIcon
            fontSize="small"
            className={cn(classes.arrow, { [classes.downArrow]: openCh })}
          />
        )}
        {childActions && (
          <Menu
            id="child-menu"
            anchorEl={anchorElCh}
            keepMounted
            open={openCh}
            onClose={handleCloseCh}
            anchorOrigin={{
              vertical: 'top',
              horizontal: 'right',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'left',
            }}
          >
            {childActions.map((act) => renderDefaultItem(act))}
          </Menu>
        )}
      </MenuItem>
    );
  };

  return (
    <Grid container direction="column">
      <Grid item>
        <Grid alignItems="center" spacing={4} container>
          <Grid item>
            {pitch && (
              <IconButton disabled={!pitch} onClick={togglePitch} size="large">
                <Badge invisible={!pitch} badgeContent="!" color="primary">
                  <MailOutlineIcon />
                </Badge>
              </IconButton>
            )}
          </Grid>
          <Grid item>
            <Button
              endIcon={<MoreVertIcon />}
              variant="outlined"
              onClick={handleClick as any}
              disabled={isDisabled(jobMatch.match_type) || disabled}
            >
              {getMenuTitle(jobMatch.match_type)}
            </Button>
            <Menu
              id="long-menu"
              anchorEl={anchorEl}
              keepMounted
              open={open}
              onClose={handleClose}
              anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
              transformOrigin={{ vertical: 'top', horizontal: 'right' }}
            >
              {actions.map((action) => renderDefaultItem(action))}
            </Menu>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default NewActionsBlock;
