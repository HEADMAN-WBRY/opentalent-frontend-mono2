import IntakeIcon from '@mui/icons-material/AssignmentOutlined';

import { JobMatch } from '@libs/graphql-types';

export interface NewActionsBlockProps {
  disabled: boolean;
  jobMatch: JobMatch;
  isSaved: boolean;
  jobId: string;
  togglePitch: VoidFunction;
}

export interface PureActionType {
  text: string;
  Icon?: typeof IntakeIcon;
  onClick?: VoidFunction;
  isLoading?: boolean;
  disabled?: boolean;
}

export interface ActionOptionType extends PureActionType {
  childActions?: PureActionType[];
}
