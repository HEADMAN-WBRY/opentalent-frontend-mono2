import { useSnackbar } from 'notistack';
import React, { useCallback } from 'react';
import useCopyToClipboard from 'react-use/lib/useCopyToClipboard';
import CategoriesList from 'screens/company-user/shared/talent-card/CategoriesList';

import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import { Box, Collapse, Grid, IconButton, Tooltip } from '@mui/material';

import { JobMatch, Skill } from '@libs/graphql-types';
import { stopEvent } from '@libs/helpers/common';
import SkillChip from '@libs/ui/components/chip/SkillChip';
import Typography from '@libs/ui/components/typography';

interface MainInfoProps {
  jobMatch: JobMatch;
  isPitchShown: boolean;
}

const MainInfo = ({ jobMatch, isPitchShown }: MainInfoProps) => {
  const { enqueueSnackbar } = useSnackbar();
  const pitch = jobMatch.job_application?.pitch || '';
  const skills = jobMatch.matched_skills;
  const keywords = jobMatch.cv_skills_keywords || [];
  const isInstantMatch = jobMatch.match_type === 'INSTANT_MATCH';
  const [, copyToClipboard] = useCopyToClipboard();
  const onCopy = useCallback(() => {
    copyToClipboard(pitch);
    enqueueSnackbar('Application text copied', { variant: 'success' });
  }, [copyToClipboard, enqueueSnackbar, pitch]);

  return (
    <Box>
      {!!skills?.length && (
        <Box mb={2}>
          <Typography variant="subtitle2">Top matches</Typography>
          <Grid component={Box} spacing={2} container>
            {jobMatch.matched_skills?.map((skill) => (
              <Grid key={skill?.id} item>
                <SkillChip size="small" skill={skill as Skill} />
              </Grid>
            ))}
          </Grid>
        </Box>
      )}

      {!!keywords.length && isInstantMatch && (
        <Box pb={3}>
          <Typography variant="subtitle2">
            Found in metadata search (CV)
          </Typography>
          <Grid component={Box} spacing={2} container>
            {keywords.map((skill) => (
              <Grid key={skill?.id} item>
                <SkillChip
                  size="small"
                  variant="outlined"
                  skill={skill as Skill}
                />
              </Grid>
            ))}
          </Grid>
        </Box>
      )}

      {jobMatch.talent && (
        <Box mb={2}>
          <CategoriesList talent={jobMatch.talent} />
        </Box>
      )}

      {pitch && (
        <Collapse in={isPitchShown}>
          <Box onClick={stopEvent} pt={6}>
            <Typography fontWeight={500} variant="subtitle2">
              Candidate’s application&nbsp;
              <Tooltip title="Copy text">
                <IconButton onClick={onCopy}>
                  <ContentCopyIcon fontSize="small" />
                </IconButton>
              </Tooltip>
            </Typography>
            <Box pt={2}>
              <Typography variant="body2" color="text.secondary">
                {pitch}
              </Typography>
            </Box>
          </Box>
        </Collapse>
      )}
    </Box>
  );
};

export default MainInfo;
