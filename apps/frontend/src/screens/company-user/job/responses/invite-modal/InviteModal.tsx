import { Box, Button, Grid } from '@mui/material';
import { Formik } from 'formik';
import React from 'react';
import {
  DefaultModalProps,
  useOpenModal,
  withLocationStateModal,
} from 'utils/modals';

import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import { DefaultModal } from '@libs/ui/components/modals';
import Typography from '@libs/ui/components/typography';

import { MODAL_TYPES } from '../../consts';
import { useInviteHandler } from './hooks';
import { FormState, InviteModalData } from './types';

interface DeclineModalProps extends DefaultModalProps<InviteModalData> {
  jobId: string;
}

const InviteModalComponent = ({
  jobId,
  isOpen,
  close,
  modalData,
}: DeclineModalProps) => {
  const talentId = modalData?.talentId || '';
  const firstName = modalData?.firstName || '';
  const { onSubmit, loading } = useInviteHandler({
    jobId,
    onClose: close,
    talentId,
  });

  return (
    <Formik<FormState> initialValues={{}} onSubmit={onSubmit}>
      {({ handleSubmit }) => (
        <DefaultModal
          actions={
            <Grid spacing={4} container>
              <Grid xs={6} item>
                <Button
                  fullWidth
                  variant="outlined"
                  color="secondary"
                  onClick={close}
                  disabled={loading}
                >
                  Cancel
                </Button>
              </Grid>
              <Grid xs={6} item>
                <Button
                  fullWidth
                  color="primary"
                  variant="contained"
                  onClick={() => handleSubmit()}
                  disabled={loading}
                >
                  Invite
                </Button>
              </Grid>
            </Grid>
          }
          handleClose={close}
          open={isOpen}
          title={`Invite ${firstName} to Apply`}
        >
          <Box>
            <Typography paragraph>
              You can write a customized message below.
            </Typography>
          </Box>

          <Box>
            <ConnectedTextField
              name="message"
              fullWidth
              variant="filled"
              label="Write a Message"
              multiline
              rows={4}
            />
          </Box>
        </DefaultModal>
      )}
    </Formik>
  );
};

export const InviteModal = withLocationStateModal<InviteModalData>({
  id: MODAL_TYPES.INVITE,
})(InviteModalComponent);

export const useOpenInviteModal = () =>
  useOpenModal<InviteModalData>(MODAL_TYPES.INVITE);
