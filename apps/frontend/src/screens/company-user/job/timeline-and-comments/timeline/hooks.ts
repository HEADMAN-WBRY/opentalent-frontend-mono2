import { useGetAtsRecordsQuery } from '@libs/graphql-types';

export const useJobATSRecords = (jobId: string) => {
  const { data, loading } = useGetAtsRecordsQuery({
    variables: { job_id: jobId },
  });
  const records = [...(data?.ATSRecords || [])].reverse();

  return { records, loading };
};
