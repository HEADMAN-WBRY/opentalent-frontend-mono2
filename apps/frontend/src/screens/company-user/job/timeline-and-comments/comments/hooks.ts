import { useStreamChatContext } from 'components/chat/common/provider';
import { useCallback, useEffect, useState } from 'react';
import { Channel } from 'stream-chat';

export const useChannelById = (channelId?: string) => {
  const { client } = useStreamChatContext();
  const [channel, setChannel] = useState<Channel>();
  const getChannel = useCallback(async () => {
    if (channelId) {
      const resChannel = await client.channel('messaging', channelId);

      setChannel(resChannel);
    }
  }, [channelId, client]);

  useEffect(() => {
    getChannel();
  }, [getChannel]);

  return channel;
};

export const useMentionClickHandler = () => {};
