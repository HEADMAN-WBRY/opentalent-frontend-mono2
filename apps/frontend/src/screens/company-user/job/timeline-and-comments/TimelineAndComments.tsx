import React, { useState } from 'react';

import { TabContext, TabList, TabPanel } from '@mui/lab';
import { Box, Paper, Tab } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { Job } from '@libs/graphql-types';

import { Comments } from './comments';
import { Timeline } from './timeline';
import { TabTypes } from './types';

interface TimelineAndCommentsProps {
  job: Job;
}

const useStyles = makeStyles((theme) => ({
  wrap: {},
  content: {
    height: 400,
  },
  tab: {
    flexGrow: 1,
  },
  tabsWrap: {
    padding: theme.spacing(2, 4, 0),
  },
  tabPanel: {
    padding: 0,
    height: '100%',
  },
  flexContainer: {
    display: 'flex',
    justifyContent: 'center',
  },
}));

const TimelineAndComments = ({ job }: TimelineAndCommentsProps) => {
  const [activeTab, setActiveTab] = useState<TabTypes>(TabTypes.Comments);
  const classes = useStyles();

  return (
    <Paper elevation={0}>
      <Box className={classes.wrap}>
        <TabContext value={activeTab}>
          <Box className={classes.tabsWrap}>
            <TabList
              classes={{ flexContainer: classes.flexContainer }}
              onChange={(_, nextTab: TabTypes) => setActiveTab(nextTab)}
              textColor="secondary"
              indicatorColor="secondary"
            >
              <Tab
                className={classes.tab}
                label="Comments"
                value={TabTypes.Comments}
              />
              <Tab
                className={classes.tab}
                label="Timeline"
                value={TabTypes.Timeline}
              />
            </TabList>
          </Box>
          <div className={classes.content}>
            <TabPanel className={classes.tabPanel} value={TabTypes.Comments}>
              <Comments job={job} />
            </TabPanel>
            <TabPanel className={classes.tabPanel} value={TabTypes.Timeline}>
              <Timeline job={job} />
            </TabPanel>
          </div>
        </TabContext>
      </Box>
    </Paper>
  );
};

export default TimelineAndComments;
