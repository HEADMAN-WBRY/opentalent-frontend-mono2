import React from 'react';
import useToggle from 'react-use/lib/useToggle';

import { Box, Collapse } from '@mui/material';

import { Job } from '@libs/graphql-types';
import BooleanSkills from '@libs/ui/components/job/boolean-skills';
import Description from '@libs/ui/components/job/description';
import GeneralInfo from '@libs/ui/components/job/general-info';
import RequiredSkills from '@libs/ui/components/job/required-skills';
import { CampaignStatus } from '@libs/ui/components/job/utils';

interface JobInfoProps {
  job: Job;
  campaignStatus: CampaignStatus;
}

const JobInfo = ({ job, campaignStatus }: JobInfoProps) => {
  const [withDetails, setWithDetails] = useToggle(false);

  return (
    <>
      <GeneralInfo
        setWithDetails={setWithDetails}
        campaignStatus={campaignStatus}
        withDetails={withDetails}
        job={job}
      />
      <Collapse in={withDetails}>
        <Box style={{ marginTop: '3px' }}>
          {job.skills_boolean_filter ? (
            <BooleanSkills job={job} />
          ) : (
            <RequiredSkills job={job} />
          )}
        </Box>
        <Box style={{ marginTop: '3px' }}>
          <Description job={job} />
        </Box>
      </Collapse>
    </>
  );
};

export default JobInfo;
