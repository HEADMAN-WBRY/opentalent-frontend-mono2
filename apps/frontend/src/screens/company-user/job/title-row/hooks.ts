import { useSnackbar } from 'notistack';
import { useCallback } from 'react';
import { useCopyToClipboard } from 'react-use';
import { EXTERNAL_RESOURCES } from 'routes';

import { Job } from '@libs/graphql-types';

export const useCopyAction = (job?: Job) => {
  const { enqueueSnackbar } = useSnackbar();
  const [, copy] = useCopyToClipboard();
  const jobId = job?.id;
  const onCopy = useCallback(() => {
    if (!jobId) {
      enqueueSnackbar(`No job data provided`, { variant: 'error' });
      return;
    }

    const link = EXTERNAL_RESOURCES.jobPage({ id: btoa(job?.id) });

    copy(link.toString());
    enqueueSnackbar(`Link copied to clipboard`, { variant: 'success' });
  }, [copy, enqueueSnackbar, job?.id, jobId]);

  return onCopy;
};
