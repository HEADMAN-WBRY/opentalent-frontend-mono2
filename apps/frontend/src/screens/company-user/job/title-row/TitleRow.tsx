import useMediaQueries from 'hooks/common/useMediaQueries';
import { useIsFreeCompanyAccount } from 'hooks/company/useCompanyAccountType';
import React from 'react';
import { pathManager } from 'routes/consts';
import { useOpenModal } from 'utils/modals';

import ArchiveOutlinedIcon from '@mui/icons-material/ArchiveOutlined';
import EditIcon from '@mui/icons-material/EditOutlined';
import FileCopyIcon from '@mui/icons-material/FileCopyOutlined';
import PlayArrowOutlinedIcon from '@mui/icons-material/PlayArrowOutlined';
import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { Job } from '@libs/graphql-types';
import Button, { RouterButton } from '@libs/ui/components/button';
import { CampaignStatus } from '@libs/ui/components/job/utils';
import Typography from '@libs/ui/components/typography';

import { MODAL_TYPES } from '../consts';
import ShareButton from './ShareButton';

interface TitleRowProps {
  job?: Job;
  campaignStatus: CampaignStatus;
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: theme.spacing(4),

    [theme.breakpoints.down('xs')]: {
      flexDirection: 'column',
    },

    '& .MuiButton-endIcon': {
      [theme.breakpoints.down('md')]: {
        margin: 0,
      },
    },

    '& .MuiButton-root': {
      [theme.breakpoints.down('md')]: {
        paddingLeft: 0,
        paddingRight: 0,
        minWidth: 40,
      },
    },
  },
  link: {
    background: theme.palette.primary.main,
    padding: `0px ${theme.spacing(2)}`,

    [theme.breakpoints.down('sm')]: {
      'body &': {
        position: 'absolute',
        top: 0,
        whiteSpace: 'nowrap',
        right: 0,
      },
    },
  },
  titleBlock: {
    position: 'relative',
  },
}));

const TitleRow = ({ job }: TitleRowProps) => {
  const isFreeCompanyAccount = useIsFreeCompanyAccount();
  const openArchiveModal = useOpenModal(MODAL_TYPES.ARCHIVE);
  const classes = useStyles();
  const { isSM } = useMediaQueries();
  const isDraft = !!job?.is_draft;
  const id = job?.id;

  return (
    <Grid
      container
      spacing={isSM ? 6 : 4}
      wrap="nowrap"
      className={classes.root}
    >
      <Grid style={{ flexGrow: 1 }} item>
        <Typography variant="h5">{job?.name}</Typography>
      </Grid>
      <Grid style={{ flexGrow: 1 }} item>
        <Grid spacing={4} wrap="nowrap" justifyContent="flex-end" container>
          <Grid component={Box} item>
            <RouterButton
              to={pathManager.company.editJob.generatePath({
                id: job?.id || '',
              })}
              endIcon={<EditIcon />}
              variant="outlined"
              color="secondary"
            >
              {!isSM && 'Edit'}
            </RouterButton>
          </Grid>
          {!isFreeCompanyAccount && (
            <Grid component={Box} item>
              <RouterButton
                to={pathManager.company.duplicateJob.generatePath({
                  id: job?.id || '',
                })}
                data-test-id="duplicate"
                endIcon={<FileCopyIcon />}
                variant="outlined"
                color="secondary"
              >
                {!isSM && 'Duplicate'}
              </RouterButton>
            </Grid>
          )}
          <Grid component={Box} item>
            <Button
              onClick={() => openArchiveModal()}
              endIcon={<ArchiveOutlinedIcon />}
              variant="outlined"
              color="secondary"
              disabled={job?.is_archived}
            >
              {!isSM && 'Archive'}
            </Button>
          </Grid>
          {!!id && !isFreeCompanyAccount && (
            <Grid component={Box} item>
              <ShareButton job={job} />
            </Grid>
          )}
          {isDraft && (
            <Grid component={Box} item>
              <RouterButton
                to={pathManager.company.editJob.generatePath({
                  id: job?.id || '',
                })}
                endIcon={<PlayArrowOutlinedIcon />}
                variant="contained"
                color="primary"
                disabled={job?.is_archived || isFreeCompanyAccount}
              >
                {!isSM && 'Start campaign'}
              </RouterButton>
            </Grid>
          )}
        </Grid>
      </Grid>
    </Grid>
  );
};

export default TitleRow;
