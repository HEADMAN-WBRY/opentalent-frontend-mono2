import { CheckThickListItem } from 'components/check-thick-list';
import CompanyStripePaymentButton from 'components/custom/company/company-stripe-payment-button';
import { DIRECT_FEATURES } from 'consts/plans';
import React from 'react';
import { useState } from 'react';

import ExpandLessRoundedIcon from '@mui/icons-material/ExpandLessRounded';
import ExpandMoreRoundedIcon from '@mui/icons-material/ExpandMoreRounded';
import { Box, Button, Card, CardContent, Collapse, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

interface SidebarDirectBannerProps { }

const useStyles = makeStyles((theme) => ({
  root: {
    color: 'white',
    background: theme.palette.primary.contrastText,
  },
}));

export const SidebarDirectBanner = (props: SidebarDirectBannerProps) => {
  const classes = useStyles();
  const [isCollapsed, setIsCollapsed] = useState(true);

  return (
    <Card classes={{ root: classes.root }} elevation={0}>
      <CardContent>
        <Box mb={4}>
          <Typography variant="body2">
            Job is in{' '}
            <Typography variant="body2" color="primary.main" component="span">
              draft mode
            </Typography>
            . Upgrade to push live, see candidate details and get more leads.
          </Typography>
        </Box>

        <Collapse in={!isCollapsed}>
          <Box mb={4}>
            <Box mb={4}>
              <Typography textAlign="center">What will you get:</Typography>
            </Box>

            <Box maxWidth={284} margin="0 auto">
              {DIRECT_FEATURES.map((feature) => (
                <CheckThickListItem key={feature} isChecked>
                  {feature}
                </CheckThickListItem>
              ))}
            </Box>
          </Box>
        </Collapse>

        <Grid container spacing={4}>
          <Grid xs={6} item>
            <CompanyStripePaymentButton
              fullWidth
              variant="contained"
              color="primary"
              size="medium"
            >
              Upgrade
            </CompanyStripePaymentButton>
          </Grid>
          <Grid xs={6} item>
            <Button
              fullWidth
              variant="outlined"
              color="info"
              endIcon={
                isCollapsed ? (
                  <ExpandMoreRoundedIcon />
                ) : (
                  <ExpandLessRoundedIcon />
                )
              }
              onClick={() => setIsCollapsed((i) => !i)}
            >
              {isCollapsed ? 'Learn more' : 'Show less'}
            </Button>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};
