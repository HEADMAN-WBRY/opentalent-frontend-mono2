import CompanyStripePaymentButton from 'components/custom/company/company-stripe-payment-button';
import React from 'react';

import CloseOutlinedIcon from '@mui/icons-material/CloseOutlined';
import {
  Box,
  Card,
  CardActions,
  CardContent,
  Grid,
  IconButton,
} from '@mui/material';
import { makeStyles } from '@mui/styles';

import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

interface CandidateCardsDirectBannerProps {
  candidatesCount: number;
  onHide: VoidFunction;
}

const useStyles = makeStyles((theme) => ({
  root: {
    color: 'white',
    background: theme.palette.primary.contrastText,
    textAlign: 'center',
    position: 'relative',
  },
  closeButton: {
    position: 'absolute',
    top: 4,
    color: 'whtie',
    right: 4,
  },
}));

export const CandidateCardsDirectBanner = ({
  candidatesCount,
  onHide,
}: CandidateCardsDirectBannerProps) => {
  const classes = useStyles();

  return (
    <Card classes={{ root: classes.root }}>
      <CardContent>
        <IconButton
          color="inherit"
          className={classes.closeButton}
          onClick={onHide}
        >
          <CloseOutlinedIcon />
        </IconButton>
        <Typography variant="h6" paragraph>
          Candidate leads 🔥🔥🔥
        </Typography>

        <Box>
          <Typography>
            Great, we already found{' '}
            <Typography component="span" color="primary">
              <b>{candidatesCount}</b> leads
            </Typography>
            .
          </Typography>
          <Typography>
            Upgrade to see full candidate details and push this job live to get
            more leads.
          </Typography>
        </Box>
      </CardContent>
      <CardActions>
        <Grid
          spacing={4}
          component={Box}
          maxWidth={440}
          container
          mb={4}
          mx="auto"
        >
          <Grid xs={6} item>
            <CompanyStripePaymentButton
              variant="contained"
              color="primary"
              fullWidth
            >
              Upgrade
            </CompanyStripePaymentButton>
          </Grid>
          <Grid xs={6} item>
            <Button fullWidth variant="text" color="info" onClick={onHide}>
              Skip for now
            </Button>
          </Grid>
        </Grid>
      </CardActions>
    </Card>
  );
};
