import { useSnackbar } from 'notistack';
import { useCallback, useState } from 'react';
import { useCreateChat } from 'screens/profile/modals/chat-with-talent/hooks';

import {
  Talent,
  useSearchTalentsToSendMessageLazyQuery,
} from '@libs/graphql-types';
import { sleep } from '@libs/helpers/common';
import { FormikSubmit } from '@libs/helpers/form';

import { FormModel } from './types';

const formatMessage = ({ message, messageType }: FormModel) => {
  let resultMessage = message;

  if (messageType === 'NO_REPLY') {
    resultMessage = `🎉 ANNOUNCEMENT 🎉\n\n

${message}\n\n
[No need to reply]`;
  } else {
    resultMessage = `💚 INPUT REQUEST 💚\n\n

${message}`;
  }

  return resultMessage;
};

export const useSubmitAction = ({ onSuccess }: { onSuccess: VoidFunction }) => {
  const { createChannel } = useCreateChat();
  const { enqueueSnackbar } = useSnackbar();
  const [isLoading, setIsLoading] = useState(false);
  const [getTalents, { loading }] = useSearchTalentsToSendMessageLazyQuery();
  const onSubmit: FormikSubmit<FormModel> = useCallback(
    async (values) => {
      setIsLoading(true);

      const { category } = values;
      const finalMessage = formatMessage(values);
      const res = await getTalents({
        variables: {
          category_ids: category ? [category as string] : undefined,
        },
      });
      const talents = (res.data?.talentsSearch?.data?.map((i) => i.talent) ||
        []) as Talent[];

      if (!talents.length) {
        enqueueSnackbar('No talents to send message', { variant: 'warning' });
      }

      let count = 0;

      for (let talent of talents) {
        try {
          const channel = await createChannel(talent);

          if (!channel) {
            return;
          }

          await channel?.create();
          await channel?.sendMessage({
            id: `${talent.id}_${Date.now()}`,
            text: finalMessage,
          });
          count++;

          await sleep(1000);
        } catch (error: any) {
          enqueueSnackbar(error?.message, { variant: 'error' });
        }
      }

      if (count) {
        enqueueSnackbar(`Message was sended to ${count} talents`, {
          variant: 'success',
        });
      }

      setIsLoading(false);
      onSuccess();
    },
    [createChannel, enqueueSnackbar, getTalents, onSuccess],
  );

  return { onSubmit, loading: loading || isLoading };
};
