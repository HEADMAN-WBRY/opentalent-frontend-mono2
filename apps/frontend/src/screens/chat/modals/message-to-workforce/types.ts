import { MESSAGE_TYPES } from './consts';

export interface FormModel {
  category: string;
  messageType: keyof typeof MESSAGE_TYPES;
  message: string;
}
