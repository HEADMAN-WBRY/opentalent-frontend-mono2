export const MIN_RATE = 25;
export const MAX_SALARY = 99999;
export const MAX_COMPANIES_COUNT = 6;
export const MAX_DESCRIBE_CHARS = 300;
