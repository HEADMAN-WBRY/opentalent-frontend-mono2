import { useMutation } from '@apollo/client';
import { getIn, useFormikContext } from 'formik';
import { UPLOAD_TALENT_AVATAR, UPLOAD_TALENT_DOCUMENT } from 'graphql/talents';
import { useSnackbar } from 'notistack';
import { useCallback, useMemo } from 'react';

import {
  Mutation,
  MutationUploadTalentAvatarArgs,
  MutationUploadTalentDocumentArgs,
  TalentDocument,
} from '@libs/graphql-types';

import { CreatingFormState } from './types';
import { getTalentInitialState } from './utils';

export const useInitialTalentState = () =>
  useMemo(() => {
    const initial = getTalentInitialState();
    return initial;
  }, []);

export const useAvatarChange = ({
  name,
  avatarPath,
  talentId = '1',
}: {
  name: string;
  avatarPath: string;
  talentId?: string;
}) => {
  const { enqueueSnackbar } = useSnackbar();
  const { setFieldValue } = useFormikContext();
  const [upload, { loading }] = useMutation<
    Mutation,
    MutationUploadTalentAvatarArgs
  >(UPLOAD_TALENT_AVATAR, {
    onCompleted: (result) => {
      const hash = result?.uploadTalentAvatar?.hash;
      const avatar = result?.uploadTalentAvatar?.avatar;

      if (hash) {
        setFieldValue(name, hash);
        setFieldValue(avatarPath, avatar);
      } else {
        enqueueSnackbar('Image upload failed!', { variant: 'error' });
      }
    },
  });
  const onAvatarChange = useCallback(
    (files: File[]) => {
      upload({ variables: { file: files[0], talent_id: talentId } });
    },
    [talentId, upload],
  );

  return { onAvatarChange, isLoading: loading };
};

export const useDocumentsChange = ({
  name,
  talentId = '1',
}: {
  name: string;
  talentId?: string;
}) => {
  const { enqueueSnackbar } = useSnackbar();
  const [upload, { loading }] = useMutation<
    Mutation,
    MutationUploadTalentDocumentArgs
  >(UPLOAD_TALENT_DOCUMENT);
  const { setFieldValue, values } = useFormikContext<CreatingFormState>();

  const onDocumentsChange = useCallback(
    async (files: File[]) => {
      const filesToUpload = files.filter((i) => !(i as any).hash);
      const currentFiles: TalentDocument[] = getIn(values, name);
      try {
        if (!filesToUpload.length) {
          return;
        }

        const res = await upload({
          variables: { files: filesToUpload, talent_id: talentId },
        });
        const uploadedFiles = (res.data?.uploadTalentDocument ||
          []) as TalentDocument[];
        const allFiles = currentFiles.concat(uploadedFiles);

        setFieldValue(name, allFiles);
      } catch (e) {
        enqueueSnackbar((e as any).toString());
      }
    },
    [values, name, upload, talentId, setFieldValue, enqueueSnackbar],
  );
  return { onDocumentsChange, loading };
};
