import { SOLUTIONS_WITH_HARD } from 'consts/skills';
import React from 'react';
import { AvailabilityType } from 'screens/talent/edit-profile/types';

import { Skill, SkillTypeEnum, TalentDocument } from '@libs/graphql-types';
import { OptionType } from '@libs/ui/components/form/select';

import { JoinReasonEnum } from './../../../../../../../libs/graphql-types/src/index';

export interface StepDataType {
  title: string;
  subTitle: React.ReactNode;
  step: number;
  Content: React.ElementType;
  nextLink: string;
  backLink?: string;
  statePath: string;
}

export interface ProfileFormState {
  firstName: string;
  lastName: string;
  email: string;
  location: string;
  category: string;
  position: string;
  linkedLink: string;
  subcategories: OptionType[];
  phone: string;
  vat?: string;
}

export interface PictureFormState {
  avatar: string;
  hash: string;
  files?: FileList;
}

export interface AvailabilityFormState {
  availableNow: AvailabilityType;
  availableDate: string;
  hoursPerWeek: string;
}

export interface RateFormState {
  min: number;
}

export interface SalaryFormState {
  salary?: number;
}

type SkillOption = Skill & OptionType;

export interface SkillsFormState {
  [SkillTypeEnum.HardSkills]: SkillOption[];
  [SkillTypeEnum.SoftSkills]: SkillOption[];
  [SkillTypeEnum.Solutions]: SkillOption[];
  [SOLUTIONS_WITH_HARD]: SkillOption[];
}

export interface CompaniesFormState {
  companies: OptionType[];
}

export interface CVFormState {
  documents: TalentDocument[];
}

export interface DescribeFormState {
  about: string;
}

export interface CustomWorkHistory {
  id: number | string;
  positionTitle: string;
  companyName: string;
  worked: [Date | null, Date | null];
  workNow: boolean;
}

export interface JobApplicationData {
  pitch?: string;
}

export interface HelpState {
  knowRecruiters: boolean;
  knowTalentPooling: boolean;
  knowCEO: boolean;
  knowCompaniesThatBenefits: boolean;
}

export interface CreatingFormState {
  reasons?: JoinReasonEnum[];
  talentData?: {
    id: string;
  };
  profile: ProfileFormState;
  picture: PictureFormState;
  availability: AvailabilityFormState;
  rate: RateFormState;
  salary: SalaryFormState;
  skills: SkillsFormState;
  companies: CompaniesFormState;
  cv: CVFormState;
  describe: DescribeFormState;
  workHistory: CustomWorkHistory[];
  application?: JobApplicationData;
  help?: HelpState;
}
