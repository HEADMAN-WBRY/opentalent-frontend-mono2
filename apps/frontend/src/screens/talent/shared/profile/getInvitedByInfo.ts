import { pathManager } from 'routes';
import { formatName } from 'utils/talent';

import { Talent, TalentInvitationTypeEnum } from '@libs/graphql-types';

interface InvitedByInfo {
  image?: string;
  title: string;
  inviter: string;
  link?: string;
  tooltip?: string;
}

export const getInvitedByInfo = (talent: Talent): InvitedByInfo => {
  if (
    talent.invited_by?.invitation_type ===
    TalentInvitationTypeEnum.InvitationTypeByTalent
  ) {
    const link = talent?.invited_by?.id!!
      ? pathManager.company.talentProfile.generatePath({
        id: talent?.invited_by.id,
      })
      : '';
    const inviter = formatName({
      firstName: talent?.invited_by?.first_name_abac?.value || '[hidden]',
      lastName: talent?.invited_by?.last_name_abac?.value,
    });

    return {
      image: talent.source?.logo,
      title: 'Invited by',
      inviter,
      link,
    };
  }

  const companyName = talent.invited_by?.company_name;
  const isCompanyInvitation =
    talent.invited_by?.invitation_type ===
    TalentInvitationTypeEnum.InvitationTypeByCompany;

  if (isCompanyInvitation) {
    return {
      image: talent.source?.logo,
      title: 'Applied directly',
      inviter: `${companyName || 'Company'}`,
      tooltip: `This person applied to the ${companyName} community and got accepted`,
    };
  }

  return {
    image: talent.source?.logo,
    title: 'Applied directly',
    inviter: `${companyName || 'OpenTalent'}`,
    tooltip: `This person applied to the ${companyName} community and got accepted`,
  };
};
