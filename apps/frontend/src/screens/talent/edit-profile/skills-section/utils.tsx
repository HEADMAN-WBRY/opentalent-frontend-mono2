import React from 'react';

import { SkillTypeEnum } from '@libs/graphql-types';
import Chip from '@libs/ui/components/chip/Chip';
import { OptionType } from '@libs/ui/components/form/select';

export const renderTags = (value: OptionType[], getTagProps: any) =>
  value.map((option: OptionType, index: number) => (
    <Chip
      {...{
        color:
          option.skill_type === SkillTypeEnum.HardSkills
            ? 'success'
            : 'tertiary',
        size: 'small',
      }}
      key={`id-${option.value}-${index + 1}`}
      label={option.text}
      {...getTagProps({ index })}
    />
  ));
