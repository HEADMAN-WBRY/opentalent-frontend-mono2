import { INPUT_SUGGESTION_LENGTH } from 'consts/skills';
import React from 'react';

import { Box } from '@mui/material';
import Grid from '@mui/material/Grid';

import { modelPath } from '@libs/helpers/form';
import { ConnectedMultipleSelect } from '@libs/ui/components/form/multiple-select';
import Typography from '@libs/ui/components/typography';

import { ProfileFormState } from '../../types';
import { useAddSkill } from './hooks';

const Companies = () => {
  const path = modelPath<ProfileFormState>((m) => m.companies.companies);
  const {
    onInputChange,
    skillsSuggest,
    onSelectSkill,
    inputValue = '',
  } = useAddSkill(path);
  const showNoOptionsText =
    inputValue.length > INPUT_SUGGESTION_LENGTH && !skillsSuggest?.length;

  return (
    <Grid spacing={4} direction="column" container>
      <Grid item>
        <Box pt={4}>
          <Typography variant="body2">Top companies</Typography>
        </Box>
      </Grid>
      <Grid item>
        <ConnectedMultipleSelect
          options={skillsSuggest}
          name={path}
          noOptionsText={
            showNoOptionsText
              ? `press enter to create ${inputValue}`
              : 'e.g. ABN, Microsoft, Google'
          }
          chipProps={{
            color: 'tertiary',
            size: 'small',
          }}
          autoCompleteProps={{
            onChange: onSelectSkill,
            filterSelectedOptions: true,
          }}
          inputProps={{
            variant: 'filled',
            label: (
              <span style={{ whiteSpace: 'nowrap' }}>
                What top companies have you worked for or served as clients?
              </span>
            ),
            onChange: onInputChange,
            value: inputValue,
          }}
        />
      </Grid>
    </Grid>
  );
};

export default React.memo(Companies);
