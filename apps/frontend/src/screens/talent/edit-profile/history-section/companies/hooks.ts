import { useLazyQuery, useMutation } from '@apollo/client';
import { debounce } from '@mui/material';
import { INPUT_SUGGESTION_LENGTH } from 'consts/skills';
import { useFormikContext } from 'formik';
import { useConfirm } from 'material-ui-confirm';
import { useSnackbar } from 'notistack';
import { useCallback, useMemo, useState } from 'react';
import useKeyPressEvent from 'react-use/lib/useKeyPressEvent';

import {
  Company,
  Mutation,
  MutationCreateCompanyArgs,
  Query,
  QueryCompaniesArgs,
} from '@libs/graphql-types';
import { OptionType } from '@libs/ui/components/form/select';

import { CREATE_COMPANY, SEARCH_COMPANIES } from './queries';

const useHandleFieldChange = (fieldPath: string) => {
  const { setFieldValue, getFieldProps } = useFormikContext();
  return useCallback(
    ({ company }: { company: Company }) => {
      const { value = [] } = getFieldProps<OptionType[]>(fieldPath);
      const isDuplicate = value.some((opt) => opt.value === company.id);

      if (isDuplicate) {
        return;
      }

      setFieldValue(
        fieldPath,
        (value as any).concat({
          ...company,
          text: company?.name,
          value: company?.id,
        }),
      );
    },
    [fieldPath, getFieldProps, setFieldValue],
  );
};

const useSkillsSuggestion = () => {
  const [getSkills, { data }] = useLazyQuery<Query, QueryCompaniesArgs>(
    SEARCH_COMPANIES,
    { fetchPolicy: 'network-only' },
  );
  const getSkillsSuggest = useMemo(() => debounce(getSkills, 300), [getSkills]);
  const skillsSuggest: OptionType[] = useMemo(() => {
    const currentSuggest = (data?.companies?.data ?? [])?.map(
      ({ id, name }) => ({
        id,
        name,
        value: id,
        text: name,
      }),
    );
    return currentSuggest;
  }, [data]);

  return { getSkillsSuggest, skillsSuggest };
};

const useCreateSkillAction = () => {
  const confirm = useConfirm();
  const [createSkill] = useMutation<Mutation, MutationCreateCompanyArgs>(
    CREATE_COMPANY,
  );

  return useCallback(
    async (name: string) => {
      await confirm({
        title: `Create company "${name}"?`,
        confirmationButtonProps: { autoFocus: true },
      });
      const data = await createSkill({
        variables: { name },
      });
      return data?.data?.createCompany;
    },
    [confirm, createSkill],
  );
};

export const useAddSkill = (fieldPath: string) => {
  const { enqueueSnackbar } = useSnackbar();

  const [inputValue, setInputValue] = useState<string>();

  const changeField = useHandleFieldChange(fieldPath);
  const createSkill = useCreateSkillAction();
  const [checkSkillExists] = useLazyQuery<Query, QueryCompaniesArgs>(
    SEARCH_COMPANIES,
    {
      variables: { search: '' },
      nextFetchPolicy: 'cache-only',
      onCompleted: async (data: Query) => {
        let company = data?.companies?.data?.find((i) => i.name === inputValue);
        if (!company) {
          company = await createSkill(inputValue || '');
          enqueueSnackbar(`Company "${inputValue}" created successfully!`, {
            variant: 'success',
          });
        }
        changeField({
          company: company as Company,
        });
        setInputValue('');
      },
    },
  );

  const { getSkillsSuggest, skillsSuggest } = useSkillsSuggestion();

  const handleAddSkill = useCallback(async () => {
    if (inputValue) {
      checkSkillExists({
        variables: { search: inputValue },
      });
    }
  }, [inputValue, checkSkillExists]);

  const onInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const val = e?.target?.value;
    setInputValue(val);
    if (val?.length > INPUT_SUGGESTION_LENGTH) {
      getSkillsSuggest({
        variables: {
          search: e?.target?.value || '',
        },
      });
    }
  };

  useKeyPressEvent('Enter', handleAddSkill);

  const onSelectSkill = () => {
    setInputValue('');
  };

  return {
    onInputChange,
    skillsSuggest,
    onSelectSkill,
    inputValue,
  };
};
