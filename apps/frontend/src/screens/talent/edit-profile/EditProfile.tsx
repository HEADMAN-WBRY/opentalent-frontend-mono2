import ScrollToFirstError from 'components/form/scroll-to-first-error';
import { ConnectedPageLayout } from 'components/layout/page-layout';
import { Form, Formik } from 'formik';
import { useIsCurrentTalentVerified } from 'hooks/talents/useTalentAccountType';
import React from 'react';

import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

import Button from '@libs/ui/components/button';

import AvailabilitySection from './availability-section';
import GeneralSection from './general-section';
import HistorySection from './history-section';
import { useLoadTalent, useTalentUpdate } from './hooks';
import SkillsSection from './skills-section';
import useStyles from './styles';
import { ProfileFormState } from './types';
import { validator } from './validator';
import VerificationSection from './verification-section';

interface EditProfileProps { }

const EditProfile = (props: EditProfileProps) => {
  const classes = useStyles(props);
  const { initialValues, isLoading, talent } = useLoadTalent();
  const { loading, onSubmit } = useTalentUpdate();
  const isTalentVerified = useIsCurrentTalentVerified();

  return (
    <ConnectedPageLayout
      isLoading={isLoading}
      drawerProps={isTalentVerified ? {} : undefined}
      headerProps={{ accountProps: {} }}
      documentTitle="Edit profile"
    >
      <Box className={classes.container}>
        <Typography variant="h5">
          {talent?.is_invitation_accepted
            ? 'Edit profile'
            : 'Set up your profile'}
        </Typography>

        {initialValues && (
          <Formik<ProfileFormState>
            validationSchema={validator}
            initialValues={initialValues}
            onSubmit={onSubmit}
            validateOnChange={false}
          >
            {({ values }) => {
              return (
                <Box component={Form} className={classes.form}>
                  <>
                    <ScrollToFirstError />
                    {console.log(values)}
                    <GeneralSection values={values} />
                    <AvailabilitySection
                      availableNow={values.availability.availableNow}
                    />
                    <HistorySection />
                    <SkillsSection />
                    <VerificationSection
                      talentId={values.talentData?.id || ''}
                    />
                    <div>
                      <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                        disabled={loading}
                      >
                        {talent?.is_invitation_accepted
                          ? 'Update profile'
                          : 'Save profile'}
                      </Button>
                    </div>
                  </>
                </Box>
              );
            }}
          </Formik>
        )}
      </Box>
      {/* <TalentWelcomeModal
        talentId={talentId}
        isInvitationAccepted={isInvitationAccepted || false}
      /> */}
    </ConnectedPageLayout>
  );
};

export default EditProfile;
