import { CreatingFormState } from '../shared/profile/types';

export interface RoutePropsType {
  talentId: string;
}

export enum AvailabilityType {
  Now = 'now',
  Later = 'later',
}

export interface ProfileFormState extends CreatingFormState {}
