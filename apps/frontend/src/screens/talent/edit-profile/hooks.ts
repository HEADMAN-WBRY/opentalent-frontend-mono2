import { useMutation, useQuery } from '@apollo/client';
import { GET_CURRENT_TALENT, UPDATE_TALENT_PROFILE } from 'graphql/talents';
import { useSnackbar } from 'notistack';
import { useCallback, useMemo } from 'react';
import { useHistory } from 'react-router-dom';
import { pathManager } from 'routes';

import {
  Mutation,
  MutationUpdateTalentProfileArgs,
  Query,
} from '@libs/graphql-types';
import { FormikSubmit } from '@libs/helpers/form';

import {
  mapFormToTalentUpdate,
  mapTalentToClient,
} from '../shared/profile/mappers';
import { ProfileFormState } from './types';

export const useLoadTalent = () => {
  const { loading, data } = useQuery<Query>(GET_CURRENT_TALENT);

  const talent = data?.currentTalent;

  const initialValues: ProfileFormState = useMemo(
    () => ({
      talentData: {
        id: talent?.id || '',
      },
      ...mapTalentToClient(talent),
    }),
    [talent],
  );

  return {
    talent,
    initialValues: talent ? initialValues : null,
    isLoading: loading,
  };
};

export const useTalentUpdate = () => {
  const history = useHistory();
  const { enqueueSnackbar } = useSnackbar();
  const [submitFrom, { loading }] = useMutation<
    Mutation,
    MutationUpdateTalentProfileArgs
  >(UPDATE_TALENT_PROFILE, {
    onCompleted: () => {
      enqueueSnackbar(`Success!`, { variant: 'success' });
      history.push(pathManager.talent.profile.generatePath());
    },
  });
  const onSubmit: FormikSubmit<ProfileFormState> = useCallback(
    async (formData) => {
      try {
        await submitFrom({ variables: mapFormToTalentUpdate(formData) });
      } catch (e) {
        enqueueSnackbar((e as any).toString(), { variant: 'error' });
      }
    },
    [submitFrom, enqueueSnackbar],
  );

  return { onSubmit, loading };
};
