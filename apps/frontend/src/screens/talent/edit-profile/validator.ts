import { MAX_DATE } from 'consts/date';
import { SKILLS_LIMITS, SOLUTIONS_WITH_HARD } from 'consts/skills';
import errors from 'consts/validationErrors';
import { isBefore } from 'date-fns';
import { AvailabilityType } from 'screens/talent/edit-profile/types';
import {
  MAX_COMPANIES_COUNT,
  MAX_DESCRIBE_CHARS,
  MAX_SALARY,
} from 'screens/talent/shared/profile/consts';
import * as yup from 'yup';

import { SkillTypeEnum } from '@libs/graphql-types';
import {
  maxStringValidator,
  string64Validator,
  varCharStringValidator,
} from '@libs/helpers/yup';
import {
  MAX_JOB_RATE,
  MIN_JOB_RATE,
} from '@libs/ui/components/job/utils/consts';

export const validator = yup.object().shape({
  profile: yup.object().shape({
    firstName: varCharStringValidator.required(),
    lastName: varCharStringValidator.required(),
    position: maxStringValidator.required(),
    location: varCharStringValidator.required(),
    category: yup.string().required(),
    linkedLink: maxStringValidator.customUrl(),
    phone: string64Validator.phone(),
    vat: maxStringValidator,
    // TODO: make this field mandatory
    subcategories: yup.array().max(3),
  }),
  cv: yup.object().shape({
    documents: yup.array().of(yup.object()).min(1),
  }),
  availability: yup.object().shape({
    availableNow: yup.string().required(),
    availableDate: yup
      .date()
      .nullable()
      .when('availableNow', {
        is: (availableNow: AvailabilityType) =>
          availableNow === AvailabilityType.Later,
        then: yup
          .date()
          .typeError(errors.invalidDate)
          .min(new Date())
          .max(MAX_DATE)
          .nullable()
          .required(),
        otherwise: yup.date().nullable(),
      }),
    hoursPerWeek: yup.number().required(),
  }),
  rate: yup.object().shape({
    min: yup
      .number()
      .typeError(errors.invalidNumber)
      .required()
      .max(MAX_JOB_RATE)
      .min(MIN_JOB_RATE),
  }),
  salary: yup.object().shape({
    salary: yup.number().required().max(MAX_SALARY, `Maximum ${MAX_SALARY}`),
  }),
  companies: yup.object().shape({
    companies: yup
      .array()
      .max(MAX_COMPANIES_COUNT, `Maximum ${MAX_COMPANIES_COUNT} companies`),
  }),
  describe: yup.object().shape({
    about: yup.string().trim().max(MAX_DESCRIBE_CHARS),
  }),
  skills: yup.object().shape({
    [SOLUTIONS_WITH_HARD]: yup
      .array()
      .max(
        SKILLS_LIMITS[SOLUTIONS_WITH_HARD].max,
        errors.maxOptionsLength(SKILLS_LIMITS[SOLUTIONS_WITH_HARD].max),
      )
      .min(
        SKILLS_LIMITS[SOLUTIONS_WITH_HARD].min,
        errors.minOptionsLength(SKILLS_LIMITS[SOLUTIONS_WITH_HARD].min),
      ),
    [SkillTypeEnum.SoftSkills]: yup
      .array()
      .max(
        SKILLS_LIMITS[SkillTypeEnum.SoftSkills].max,
        errors.maxOptionsLength(SKILLS_LIMITS[SkillTypeEnum.SoftSkills].max),
      )
      .min(
        SKILLS_LIMITS[SkillTypeEnum.SoftSkills].min,
        errors.minOptionsLength(SKILLS_LIMITS[SkillTypeEnum.SoftSkills].min),
      ),
  }),
  workHistory: yup.array().of(
    yup.object().shape(
      {
        positionTitle: yup.string().when(['worked', 'companyName'], {
          is: (worked: string[], companyName: string) =>
            worked?.some(Boolean) || !!companyName,
          then: maxStringValidator.required(errors.required),
          otherwise: maxStringValidator,
        }),
        companyName: yup.string().when(['worked', 'positionTitle'], {
          is: (worked: string[], positionTitle: string) =>
            worked?.some(Boolean) || !!positionTitle,
          then: maxStringValidator.required(errors.required),
          otherwise: maxStringValidator,
        }),
        worked: yup.array().when(['companyName', 'positionTitle'], {
          is: (companyName: string, positionTitle: string) =>
            !!companyName || !!positionTitle,
          then: yup.array().of(
            yup
              .date()
              .typeError(errors.invalidDate)
              .nullable()
              .max(MAX_DATE)
              .test(
                'Date range test',
                errors.required,
                function rangeTest(val) {
                  const isFromField = this.path.includes('.worked[0]');
                  return !(!val && isFromField);
                },
              )
              .test(
                'minMaxValidator',
                errors.invalidIntervalDate,
                function rangeTest(val) {
                  const [startDate, endDate] = this.parent;

                  if (!startDate || !endDate) {
                    return true;
                  }

                  return isBefore(startDate, endDate);
                },
              ),
          ),
          otherwise: yup.array().nullable(),
        }),
      },
      [
        ['companyName', 'worked'],
        ['worked', 'positionTitle'],
        ['companyName', 'positionTitle'],
      ] as any,
    ),
  ),
});
