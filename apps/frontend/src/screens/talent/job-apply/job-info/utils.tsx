import { format, formatDuration, intervalToDuration, parseISO } from 'date-fns';

import { Job, JobTypeEnum } from '@libs/graphql-types';
import { getJobCapacity } from '@libs/ui/components/job/utils';

import { DataGridItem } from './types';

const getDuration = (job?: Required<Job>) => {
  if (!job?.start_date || !job?.end_date) {
    return '';
  }

  const duration = intervalToDuration({
    start: parseISO(job.start_date),
    end: parseISO(job.end_date),
  });

  return formatDuration(duration, {
    format: ['years', 'months', 'days', 'hours'],
  });
};

export const mapJobToDataGridItems = (job?: Required<Job>): DataGridItem[] => {
  const isFreelanceJob = job?.type === JobTypeEnum.Freelance;
  const periodItem: DataGridItem = {
    icon: 'period',
    title: 'Project duration',
    value: getDuration(job),
  };

  return [
    {
      icon: 'period',
      title: 'Starting date',
      value: job?.posted_at
        ? format(parseISO(job.start_date), 'MMM d, yyyy')
        : '',
    },
    ...(isFreelanceJob ? [periodItem] : []),
    {
      title: 'Required Capacity',
      value: getJobCapacity(job as Job),
      icon: 'timelapse',
    },
    {
      title: 'Client',
      icon: 'client',
      value: job?.client,
    },
    {
      icon: 'location',
      title: 'Country',
      value: job?.country,
    },
    {
      icon: 'location',
      title: 'City',
      value: job?.city,
    },
  ];
};
