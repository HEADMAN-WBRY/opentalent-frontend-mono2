import { useCurrentTime } from 'hooks/common/useCurrentTime';
import React, { useState } from 'react';
import ChatWithClientButton from 'screens/talent/shared/chat-with-client-button';

import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import { Box, Collapse, Grid, Paper } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { Job, JobTypeEnum, Skill } from '@libs/graphql-types';
import SkillChip from '@libs/ui/components/chip/SkillChip';
import {
  JOB_TYPE_MAP,
  formatRate,
  getDiffHours,
  checkJobRemainHours,
  isUnprocessableJob,
} from '@libs/ui/components/job/utils';
import Typography from '@libs/ui/components/typography';

import DataGrid from './DataGrid';
import JobTimer from './JobTimer';
import { DataGridItem } from './types';
import { mapJobToDataGridItems } from './utils';

interface JobInfoProps {
  job: Required<Job>;
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    marginBottom: theme.spacing(4),

    'html body div&': {
      marginTop: `${theme.spacing(10)} !important`,
    },

    '&:before': {
      display: 'none',
    },
  },
  title: {
    marginBottom: theme.spacing(6),
    marginTop: theme.spacing(4),
  },
  titleDataGrid: {
    paddingBottom: theme.spacing(2),
  },
  summaryRoot: {
    alignItems: 'flex-start',
    padding: `${theme.spacing(4)} ${theme.spacing(4)} 0 ${theme.spacing(4)}`,
  },
  summaryContent: {
    margin: '0 -36px 0 0 !important',
  },
  detailsRoot: {
    padding: `0 ${theme.spacing(4)}`,
  },
  expandIcon: {},
  chip: {
    maxWidth: 240,
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
  },
  toggler: {
    cursor: 'pointer',
    display: 'inline-flex',
    alignItems: 'center',
  },
  titleRow: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  description: {
    color: theme.palette.text.secondary,
    '& > p': {
      margin: 0,
    },
  },
}));

const JobInfo = ({ job }: JobInfoProps) => {
  const [isExpand, setIsExpand] = useState(false);
  const classes = useStyles();
  const toggle = () => setIsExpand((state) => !state);
  const currentTime = useCurrentTime();
  const campaignDiffHours = getDiffHours(job?.campaign_end_date);
  const isAboutExpire = checkJobRemainHours(campaignDiffHours);
  const isPermanentRole = job.type === JobTypeEnum.Permanent;
  const isActionsDisabled = isUnprocessableJob(job);

  const rateRangeItem: DataGridItem = isPermanentRole
    ? {
      icon: 'euro',
      title: 'Salary range',
      value: formatRate({
        min: job.salary_min,
        max: job.salary_max,
        period: 'month',
        isNegotiable: job.is_rate_negotiable,
      }),
    }
    : {
      icon: 'euro',
      title: 'Hourly rate range',
      value: formatRate({
        min: job.rate_min,
        max: job.rate_max,
        period: 'hour',
        isNegotiable: job.is_rate_negotiable,
      }),
    };

  return (
    <Paper elevation={0} square classes={{ root: classes.root }}>
      <Box padding={4}>
        <Box className={classes.titleDataGrid} width="100%">
          <Box className={classes.titleRow}>
            <Typography variant="h6">Job details</Typography>
            <JobTimer
              isAboutExpire={isAboutExpire}
              currentTime={currentTime}
              job={job}
            />
          </Box>
          <Box pt={7} pb={6}>
            <Typography>{job.name}</Typography>
          </Box>

          <DataGrid
            items={[
              {
                icon: 'book',
                title: 'Key skills',
                value: (
                  <Grid container spacing={2}>
                    {job.skills.map((skill) => (
                      <Grid key={skill?.id} item>
                        <SkillChip
                          className={classes.chip}
                          size="small"
                          skill={skill as Skill}
                        />
                      </Grid>
                    ))}
                  </Grid>
                ),
              },
              rateRangeItem,
              {
                title: 'Type',
                value:
                  JOB_TYPE_MAP[
                  job?.location_type as keyof typeof JOB_TYPE_MAP
                  ] || '-',
                icon: 'calendar',
              },
            ]}
          />
        </Box>
        <Collapse in={isExpand}>
          <Box pb={4} width="100%">
            <DataGrid items={mapJobToDataGridItems(job)} />
            <br />
            <div
              className={classes.description}
              // eslint-disable-next-line react/no-danger
              dangerouslySetInnerHTML={{ __html: job?.description || '' }}
            />
          </Box>
        </Collapse>
        <Grid container justifyContent="space-between">
          <Grid item>
            <Box>
              <Typography
                className={classes.toggler}
                component="span"
                color="tertiary"
                variant="body2"
                onClick={toggle}
              >
                {isExpand ? 'Show less' : 'Find out more'}
                {isExpand ? (
                  <KeyboardArrowUpIcon fontSize="small" />
                ) : (
                  <KeyboardArrowDownIcon fontSize="small" />
                )}
              </Typography>
            </Box>
          </Grid>
          <Grid item>
            <ChatWithClientButton
              job={job}
              disabled={isActionsDisabled}
              color="info"
            />
          </Grid>
        </Grid>
      </Box>
    </Paper>
  );
};

export default JobInfo;
