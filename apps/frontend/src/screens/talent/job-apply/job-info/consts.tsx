import { ReactComponent as EuroIcon } from 'assets/icons/euro.svg';

import DateRangeIcon from '@mui/icons-material/DateRange';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import MenuBookIcon from '@mui/icons-material/MenuBook';
import PermContactCalendarIcon from '@mui/icons-material/PermContactCalendar';
import TimelapseIcon from '@mui/icons-material/Timelapse';
import WatchLaterIcon from '@mui/icons-material/WatchLater';
import WorkIcon from '@mui/icons-material/Work';

import { JobTypeEnum } from '@libs/graphql-types';

export const ICONS_MAP = {
  book: MenuBookIcon,
  location: LocationOnIcon,
  clock: WatchLaterIcon,
  calendar: PermContactCalendarIcon,
  period: DateRangeIcon,
  client: WorkIcon,
  timelapse: TimelapseIcon,
  euro: EuroIcon,
};

export const COMPENSATION_TYPES: Record<JobTypeEnum, string> = {
  [JobTypeEnum.Freelance]: '€/h',
  [JobTypeEnum.Permanent]: '€/month',
  [JobTypeEnum.Project]: '€/h',
};
