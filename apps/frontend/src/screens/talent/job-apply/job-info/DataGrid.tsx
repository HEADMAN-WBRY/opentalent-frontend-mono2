import React from 'react';

import { Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { ICONS_MAP } from './consts';
import { DataGridItem } from './types';

interface DataGridProps {
  items: DataGridItem[];
}

const useStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(4),
    paddingBottom: theme.spacing(2),

    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
      paddingBottom: theme.spacing(4),
      paddingLeft: 0,
    },
  },
  leftCol: {
    color: theme.palette.grey[500],
    width: 200,

    [theme.breakpoints.down('sm')]: {
      width: 'auto',
    },
  },
}));

const DataGrid = ({ items }: DataGridProps) => {
  const classes = useStyles();
  return (
    <>
      {items.map(({ title, icon, value }) => {
        const Icon = ICONS_MAP[icon];
        return (
          <Grid
            wrap="nowrap"
            className={classes.root}
            spacing={2}
            container
            key={title}
          >
            <Grid item xs={12} sm={4}>
              <Grid
                wrap="nowrap"
                className={classes.leftCol}
                spacing={2}
                container
              >
                <Grid item>
                  <Icon />
                </Grid>
                <Grid item>{title}</Grid>
              </Grid>
            </Grid>
            <Grid item>{value}</Grid>
          </Grid>
        );
      })}
    </>
  );
};

export default DataGrid;
