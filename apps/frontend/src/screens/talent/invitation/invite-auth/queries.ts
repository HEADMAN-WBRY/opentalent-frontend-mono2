import { gql } from '@apollo/client';

export const INVITE_FORM_SUBMIT = gql`
  mutation InviteFormSubmit($key: String!, $email: Email!) {
    createTalentAccountFromInvitationLink(key: $key, email: $email)
  }
`;

export const GET_SCREEN_DATA = gql`
  query GetScreenData($key: String!) {
    invitationLinkInfo(key: $key) {
      name
      inviting_person_name
      is_used
    }
  }
`;
