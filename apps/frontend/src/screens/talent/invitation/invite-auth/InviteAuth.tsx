import { useQuery } from '@apollo/client';
import { Formik } from 'formik';
import { usePushWithQuery } from 'hooks/routing';
import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { pathManager } from 'routes';

import { Box, Grow } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { Query, QueryInvitationLinkInfoArgs } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import Form from './Form';
import { useInviteFormSubmit, useValidator } from './hooks';
import { GET_SCREEN_DATA } from './queries';
import { InviteFormModel } from './types';

interface InviteAuthProps {}

const useStyles = makeStyles(() => ({
  wrapper: {
    maxWidth: 424,
    position: 'relative',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
  },
}));

const INITIAL_STATE: InviteFormModel = {
  password_repeat: '',
  password: '',
  email: '',
};

const InviteAuth = (props: InviteAuthProps) => {
  const push = usePushWithQuery();
  const { key } = useParams<{ key: string }>();
  const { data, loading: dataLoading } = useQuery<
    Query,
    QueryInvitationLinkInfoArgs
  >(GET_SCREEN_DATA, {
    variables: { key },
  });
  const classes = useStyles();
  const { onSubmit, loading } = useInviteFormSubmit({ key });
  const validator = useValidator();

  useEffect(() => {
    if (data?.invitationLinkInfo?.is_used) {
      push({
        pathname: pathManager.talent.invitationInUse.generatePath(),
        query: { name: data?.invitationLinkInfo?.inviting_person_name },
      });
    }
  }, [data, push]);

  return (
    <Box className={classes.wrapper}>
      <Grow in={!dataLoading} timeout={500}>
        <div>
          <Typography variant="h4" paragraph>
            Hi {data?.invitationLinkInfo?.name || 'User'},
          </Typography>

          <Typography variant="body1" paragraph>
            Congratulations! {data?.invitationLinkInfo?.inviting_person_name}{' '}
            invited you to join OpenTalent. To complete your profile put your
            email address and click ""Activate Account"" button below, and you
            will received an activation link on your email ID to create your
            profile.
          </Typography>

          <Typography variant="body2" color="textSecondary" paragraph>
            Meet OpenTalent: the online platform that leverages the networks of
            vetted professionals and recruiters across the European Union to
            quickly find the best of the best people in the industry.
          </Typography>

          <Formik
            validationSchema={validator}
            initialValues={INITIAL_STATE}
            onSubmit={onSubmit}
          >
            {({ handleSubmit }) => (
              <Form loading={loading} onSubmit={handleSubmit} />
            )}
          </Formik>
        </div>
      </Grow>
    </Box>
  );
};

export default InviteAuth;
