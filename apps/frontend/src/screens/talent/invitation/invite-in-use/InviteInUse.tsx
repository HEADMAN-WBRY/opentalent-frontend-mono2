import { Box, Grow } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { useSearchParams } from 'hooks/routing';
import React from 'react';
import { Link } from 'react-router-dom';
import { pathManager } from 'routes';

import Typography from '@libs/ui/components/typography';

interface InviteAuthProps {}

const useStyles = makeStyles(() => ({
  wrapper: {
    maxWidth: 424,
    position: 'relative',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
  },
}));

const InviteInUse = (props: InviteAuthProps) => {
  const classes = useStyles();
  const { name } = useSearchParams();

  return (
    <Box className={classes.wrapper}>
      <Grow in timeout={500}>
        <div>
          <Typography variant="h4" paragraph>
            Hi! 😟
          </Typography>

          <Box pt={4} display="flex">
            <Typography variant="body1" paragraph>
              Sorry, but this invite link has been already used.
            </Typography>
          </Box>

          <Typography color="textSecondary" variant="body2" paragraph>
            <Typography component="span" variant="body2" color="secondary">
              {name}
            </Typography>{' '}
            invited you to join OpenTalent. <br />
            But this invite link has been already used. <br />
            Maybe it was you. Please contact {name} or try to apply again at{' '}
            <Typography color="tertiary" component="span">
              <Link to={pathManager.talent.jobBoard.generatePath()}>
                opentalent.co
              </Link>
            </Typography>
          </Typography>
        </div>
      </Grow>
    </Box>
  );
};

export default InviteInUse;
