import { makeStyles } from '@mui/styles';
import PageLayout from 'components/layout/page-layout';
import React from 'react';
import { Route, Switch } from 'react-router-dom';

import InviteAuth from './invite-auth';
import InviteInUse from './invite-in-use';
import InviteSuccess from './invite-success';
import { InvitationProps } from './types';

const useStyles = makeStyles(() => ({
  contentWrapper: {
    overflow: 'hidden',
    margin: 'auto',
  },
}));

const Invitation = ({ match }: InvitationProps) => {
  const classes = useStyles();
  return (
    <PageLayout
      classes={{ contentWrapper: classes.contentWrapper }}
      documentTitle="Invitation"
      headerProps={{
        accountProps: null,
      }}
    >
      <Switch>
        <Route path={`${match.path}/success`} exact component={InviteSuccess} />
        <Route path={`${match.path}/in-use`} exact component={InviteInUse} />
        <Route path={`${match.path}/:key`} exact component={InviteAuth} />
      </Switch>
    </PageLayout>
  );
};

export default Invitation;
