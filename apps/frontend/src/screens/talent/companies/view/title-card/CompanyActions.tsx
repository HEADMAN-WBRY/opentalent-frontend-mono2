import { GET_CURRENT_TALENT } from 'graphql/talents';
import { useSnackbar } from 'notistack';
import React from 'react';

import LogoutIcon from '@mui/icons-material/Logout';
import PersonAddAltOutlinedIcon from '@mui/icons-material/PersonAddAltOutlined';
import { Box, Menu, MenuItem } from '@mui/material';
import { makeStyles } from '@mui/styles';

import {
  GetCompanyViewScreenDataQuery,
  TalentCompanyPoolConnection,
  TalentCompanyPoolingStatusEnum,
  useApplyCurrentTalentToCompanyPoolMutation,
  useLeaveCurrentTalentFromCompanyPoolMutation,
} from '@libs/graphql-types';
import { noop } from '@libs/helpers/common';
import Button from '@libs/ui/components/button';
import { NotificationWithAction } from '@libs/ui/components/snackbar';

interface CompanyActionsProps {
  company: GetCompanyViewScreenDataQuery['company'];
  poolConnection?: TalentCompanyPoolConnection;
}

const useStyles = makeStyles(() => ({
  actionsWidth: {
    width: 175,
  },
}));

export const CompanyActions = ({
  company,
  poolConnection,
}: CompanyActionsProps) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const handleClose = () => setAnchorEl(null);
  const handleOpen = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const { enqueueSnackbar } = useSnackbar();
  let leaveAction: VoidFunction = noop;
  const [apply, { loading: isAppling }] =
    useApplyCurrentTalentToCompanyPoolMutation({
      variables: { company_id: company!.id },
      refetchQueries: [
        {
          query: GET_CURRENT_TALENT,
        },
      ],
      onCompleted: () => {
        enqueueSnackbar(
          <NotificationWithAction
            handleAction={() => leaveAction()}
            actionText="Undo"
          >
            Your request has been sent to the company.
          </NotificationWithAction>,
          {
            variant: 'success',
          },
        );
      },
    });
  const [leave, { loading: isLeaving }] =
    useLeaveCurrentTalentFromCompanyPoolMutation({
      variables: { company_id: company!.id },
      refetchQueries: [
        {
          query: GET_CURRENT_TALENT,
        },
      ],
      onCompleted: () => {
        enqueueSnackbar(
          <NotificationWithAction
            handleAction={() => apply()}
            actionText="Undo"
          >
            You have left to the company community.
          </NotificationWithAction>,
          {
            variant: 'warning',
            persist: true,
          },
        );
      },
    });
  const finalIsLoading = isAppling || isLeaving;
  leaveAction = leave;

  if (!company) {
    return null;
  }

  const menu = (
    <Menu
      id="basic-menu"
      anchorEl={anchorEl}
      open={!!anchorEl}
      onClose={handleClose}
      classes={{ list: classes.actionsWidth }}
      MenuListProps={{
        'aria-labelledby': 'basic-button',
      }}
    >
      <MenuItem
        onClick={() => {
          handleClose();
          leave();
        }}
      >
        <LogoutIcon style={{ marginRight: 8 }} />
        Leave
      </MenuItem>
    </Menu>
  );

  if (TalentCompanyPoolingStatusEnum.Approved === poolConnection?.status) {
    return (
      <Box className={classes.actionsWidth}>
        <Button
          fullWidth
          onClick={handleOpen as any}
          variant="contained"
          disabled={finalIsLoading}
          color="success"
          endIcon={<PersonAddAltOutlinedIcon />}
        >
          CONNECTED
        </Button>
        {menu}
      </Box>
    );
  }

  const isPending = [TalentCompanyPoolingStatusEnum.Pending].includes(
    poolConnection?.status as any,
  );

  if (isPending) {
    return (
      <Box className={classes.actionsWidth}>
        <Button
          variant="outlined"
          color="warning"
          onClick={handleOpen as any}
          fullWidth
        >
          Pending
        </Button>
        {menu}
      </Box>
    );
  }

  return (
    <Box className={classes.actionsWidth}>
      <Button
        fullWidth
        variant="contained"
        disabled={finalIsLoading}
        color="info"
        endIcon={<PersonAddAltOutlinedIcon />}
        onClick={() => apply({ variables: { company_id: company?.id } })}
      >
        apply to join
      </Button>
    </Box>
  );
};
