import { Formik } from 'formik';
import React from 'react';
import * as yup from 'yup';

import { Box, Card, CardContent, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { InviteCompanyToOpentalentMutationVariables } from '@libs/graphql-types';
import { modelPath } from '@libs/helpers/form';
import Button from '@libs/ui/components/button';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import Typography from '@libs/ui/components/typography';

import {
  InviteCompanyModal,
  useOpenJobPushDirectModal,
} from './InviteCompanyModal';
import { InviteFormSchema } from './types';

interface InviteProps { }

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(6),
  },
}));

const validation = yup.object().shape({
  company_name: yup.string().required('Required').trim().min(1),
});

export const Invite = (props: InviteProps) => {
  const classes = useStyles();
  const openModal = useOpenJobPushDirectModal();

  return (
    <Box>
      <Formik<InviteCompanyToOpentalentMutationVariables>
        initialValues={{ company_name: '' } as any}
        onSubmit={(values) => {
          const vars: InviteFormSchema = {
            company_name: values.company_name,
          };

          openModal(vars);
        }}
        validationSchema={validation}
      >
        {({ resetForm, handleSubmit }) => {
          return (
            <div>
              <Card elevation={0}>
                <CardContent className={classes.root}>
                  <Typography variant="h6" color="info.main" paragraph>
                    Invite someone to start a community.
                  </Typography>

                  <Box mb={4}>
                    <Typography variant="body2" color="text.secondary">
                      Do you know any companies or recruiters that want to build
                      a talent community? Invite them today. And oh... when they
                      end up as a customer, you’ll get 10% of all year 1
                      earnings.
                    </Typography>
                  </Box>

                  <Grid container spacing={4}>
                    <Grid flexGrow={1} item>
                      <ConnectedTextField
                        fullWidth
                        size="small"
                        variant="standard"
                        label="Enter Company Name"
                        name={modelPath<InviteCompanyToOpentalentMutationVariables>(
                          (m) => m.company_name,
                        )}
                      />
                    </Grid>
                    <Grid item>
                      <Button
                        color="info"
                        variant="contained"
                        size="large"
                        onClick={handleSubmit}
                      >
                        Continue
                      </Button>
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
              <InviteCompanyModal onSuccess={resetForm} />
            </div>
          );
        }}
      </Formik>
    </Box>
  );
};
