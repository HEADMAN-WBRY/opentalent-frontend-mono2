import { Formik } from 'formik';
import { useSnackbar } from 'notistack';
import React from 'react';
import {
  DefaultModalProps,
  useOpenModal,
  withLocationStateModal,
} from 'utils/modals';
import * as yup from 'yup';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { useInviteCompanyToOpentalentMutation } from '@libs/graphql-types';
import { FormikSubmit } from '@libs/helpers/form';
import { modelPath } from '@libs/helpers/form';
import Button from '@libs/ui/components/button';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import { DefaultModal } from '@libs/ui/components/modals';
import Typography from '@libs/ui/components/typography';

import { InviteFormSchema } from './types';

interface InviteCompanyModalProps extends DefaultModalProps<InviteFormSchema> {
  onSuccess?: VoidFunction;
}

const INVITE_COMPANY_MODAL = 'INVITE_COMPANY_MODAL';

const useStyles = makeStyles((theme) => ({
  root: {
    borderRadius: 8,
    width: 500,

    [theme.breakpoints.down('md')]: {
      width: 'auto',
    },
  },
}));

const InviteCompanyModalComponent = ({
  isOpen,
  close,
  modalData = {} as any,
  onSuccess,
}: InviteCompanyModalProps) => {
  const validation = yup.object().shape({
    company_name: yup.string().trim().min(1),
    contact_person: yup.string(),
    company_email: yup.string().email().onlyBusinessEmail().required().trim(),
  });

  const { enqueueSnackbar } = useSnackbar();
  const [invite, { loading }] = useInviteCompanyToOpentalentMutation({
    onCompleted: () => {
      onSuccess?.();
      enqueueSnackbar(
        `Thank you! We received your invite. We'll contact the company representative soon.`,
        {
          variant: 'success',
        },
      );
      close();
    },
  });
  const classes = useStyles();
  const onSubmit: FormikSubmit<InviteFormSchema> = (values) => {
    return invite({
      variables: {
        contact_person: values?.contact_person || '',
        company_email: values?.company_email || '',
        company_name: values?.company_name || '',
      },
    });
  };

  return (
    <Formik<InviteFormSchema>
      initialValues={modalData}
      onSubmit={onSubmit}
      validationSchema={validation}
      enableReinitialize
    >
      {({ handleSubmit }) => {
        return (
          <DefaultModal
            className={classes.root}
            handleClose={close}
            open={isOpen}
            title={'Invite a Company'}
            actions={
              <Grid container spacing={4}>
                <Grid flexGrow={2} item>
                  <Button
                    color="info"
                    size="large"
                    fullWidth
                    variant="outlined"
                    onClick={close}
                    disabled={loading}
                  >
                    BACK
                  </Button>
                </Grid>
                <Grid flexGrow={1} item>
                  <Button
                    fullWidth
                    color="info"
                    variant="contained"
                    size="large"
                    disabled={loading}
                    onClick={handleSubmit}
                  >
                    SEND Invite
                  </Button>
                </Grid>
              </Grid>
            }
          >
            <Box>
              <Box mt={6} mb={6}>
                <Typography>
                  We’ll send an email to this person to start a community on
                  OpenTalent.
                </Typography>
              </Box>
              <Box mt={4}>
                <ConnectedTextField
                  fullWidth
                  name={modelPath<InviteFormSchema>((m) => m.company_name)}
                  size="small"
                  variant="standard"
                  label="Company name"
                />
              </Box>

              <Box mt={4}>
                <ConnectedTextField
                  fullWidth
                  name={modelPath<InviteFormSchema>((m) => m.company_email)}
                  size="small"
                  variant="standard"
                  label="Work Email"
                />
              </Box>
            </Box>
          </DefaultModal>
        );
      }}
    </Formik>
  );
};

export const InviteCompanyModal = withLocationStateModal<InviteFormSchema>({
  id: INVITE_COMPANY_MODAL,
})(InviteCompanyModalComponent);

export const useOpenJobPushDirectModal = () =>
  useOpenModal(INVITE_COMPANY_MODAL);
