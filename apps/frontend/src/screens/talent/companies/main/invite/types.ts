export interface InviteFormSchema {
  company_name?: string;
  contact_person?: string;
  company_email?: string;
}
