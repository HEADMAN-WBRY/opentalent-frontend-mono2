import { ConnectedPageLayout } from 'components/layout/page-layout';
import React from 'react';

import { Box } from '@mui/material';

import { useGetCompaniesScreenDataQuery } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import { Invite } from './invite';
import { CompaniesList } from './list';

export const CompaniesScreen = () => {
  const { loading, data, refetch } = useGetCompaniesScreenDataQuery();

  return (
    <ConnectedPageLayout
      documentTitle="Companies"
      headerProps={{ accountProps: {} }}
      drawerProps={{}}
      isLoading={loading}
    >
      <Box>
        <Typography variant="h5" paragraph>
          Active Communities
        </Typography>
        <Typography variant="body1">
          Companies and recruiters use OpenTalent to build private talent
          communities.
          <br /> Request to join your favourite communities for exclusive access
          to jobs and opportunities.
        </Typography>

        <Box mt={6} mb={6}>
          <CompaniesList refetch={refetch} companies={data?.poolingCompanies} />
        </Box>

        <Box>
          <Invite />
        </Box>
      </Box>
    </ConnectedPageLayout>
  );
};
