import { useSearchParams, usePushWithQuery } from 'hooks/routing';

import { SettingsTabs } from './types';

export const useSettingsTabs = () => {
  const { tab = SettingsTabs.Plans } = useSearchParams();
  const push = usePushWithQuery();
  const onTabChange = (e: React.ChangeEvent<unknown>, tab: string) =>
    push({ query: { tab } });

  return { tab, onTabChange };
};
