import React from 'react';

import { Box } from '@mui/material';

import {
  AccountTypeEnum,
  CurrentTalentSubscriptionQuery,
} from '@libs/graphql-types';
import { formatDateDefault } from '@libs/helpers/format';
import Typography from '@libs/ui/components/typography';

interface PlanDataProps {
  plan: AccountTypeEnum;
  subscription?: CurrentTalentSubscriptionQuery;
}

export const PlanData = ({ plan, subscription }: PlanDataProps) => {
  const startDate = subscription?.currentTalentSubscription?.start_date;
  const endDate = subscription?.currentTalentSubscription?.current_period_end;
  const isCanceled =
    subscription?.currentTalentSubscription?.cancel_at_period_end;

  if (!subscription) {
    return <></>;
  }

  if (isCanceled) {
    return (
      <Box>
        <Typography color="warning.main">
          This plan has been cancelled.
        </Typography>
        <Typography color="textSecondary">
          The current plan will expire on:{' '}
          <Typography component="span" color="warning.main">
            {formatDateDefault(endDate)}
          </Typography>
        </Typography>
      </Box>
    );
  }

  return (
    <Box>
      {startDate && (
        <Typography color="textSecondary">
          Active since: {formatDateDefault(startDate)}
        </Typography>
      )}
      {endDate && (
        <Typography color="textSecondary">
          Subscription renewal date: {formatDateDefault(endDate)}
        </Typography>
      )}
    </Box>
  );
};
