import { useSnackbar } from 'notistack';

import {
  AccountTypeEnum,
  CurrentTalentSubscriptionDocument,
  useCurrentTalentSubscriptionQuery,
  useRenewCurrentTalentSubscriptionMutation,
} from '@libs/graphql-types';

export const useRenewAction = () => {
  const { enqueueSnackbar } = useSnackbar();
  const [renew, { loading: isRenewing }] =
    useRenewCurrentTalentSubscriptionMutation({
      onCompleted: () => {
        enqueueSnackbar('Your subscription has been renewed.', {
          variant: 'success',
        });
      },
      refetchQueries() {
        return [CurrentTalentSubscriptionDocument];
      },
    });

  return { renew, isRenewing };
};

export const useSubscriptionData = ({ plan }: { plan: AccountTypeEnum }) => {
  const isFreePlan = plan === AccountTypeEnum.Free;
  const {
    data: subscription,
    refetch,
    loading: isSubscriptionInfoLoading,
  } = useCurrentTalentSubscriptionQuery({
    skip: isFreePlan,
  });

  const unlimitedSubscription =
    subscription?.currentTalentSubscription === null;

  const cancelAtPeriodEnd =
    subscription?.currentTalentSubscription?.cancel_at_period_end;

  const showRenewButton =
    !isFreePlan && cancelAtPeriodEnd && !unlimitedSubscription;
  const showCancelButton =
    !isFreePlan && !cancelAtPeriodEnd && !unlimitedSubscription;

  return {
    showRenewButton,
    showCancelButton,
    isSubscriptionInfoLoading,
    subscription,
    refetch,
  };
};
