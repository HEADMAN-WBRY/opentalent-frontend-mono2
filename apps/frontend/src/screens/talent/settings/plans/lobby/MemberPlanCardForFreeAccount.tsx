import { CheckThickListItem } from 'components/check-thick-list';
import StripePaymentButton from 'components/custom/talent/stripe-payment-button';
import React from 'react';

import InfoRoundedIcon from '@mui/icons-material/InfoRounded';
import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { formatCurrency } from '@libs/helpers/format';
import Typography from '@libs/ui/components/typography';

interface MemberPlanCardForFreeAccountProps { }

const useStyles = makeStyles((theme) => ({
  card: {
    backgroundColor: '#F7F5F5',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    borderRadius: 16,
    padding: theme.spacing(8, 6),
    maxWidth: 364,
  },
  info: {
    marginRight: theme.spacing(2),
    marginLeft: theme.spacing(5),
    color: theme.palette.info.main,
  },

  textBlock: {
    backgroundColor: 'white',
    padding: '6px 10px',
    borderRadius: 16,
    fontSize: 13,
    lineHeight: '18px',
    textAlign: 'center',
  },
  chip: {
    color: 'white',
  },
  textBlockDark: {
    backgroundColor: '#EEEEEE',
  },
}));

const MEMBER_FEATURES = [
  'Job Matching',
  'Community',
  'Refer & Earn',
  'Perks & Benefits',
];

export const MemberPlanCardForFreeAccount = (
  props: MemberPlanCardForFreeAccountProps,
) => {
  const classes = useStyles();

  return (
    <Box>
      <Box className={classes.card}>
        <Box mb={6} className={classes.textBlock}>
          <Typography
            whiteSpace="break-spaces"
            textAlign="center"
            variant="caption"
          >
            <b>Europe’s #1 talent-centric membership.</b>
          </Typography>
        </Box>

        <Box mb={6}>
          <Typography component="span" variant="h5" fontWeight={400}>
            OpenTalent{' '}
          </Typography>
          <Typography component="span" variant="h5" fontWeight={700}>
            Member
          </Typography>
        </Box>

        <Box width={174} margin="0 auto">
          {MEMBER_FEATURES.map((feature) => (
            <CheckThickListItem key={feature} isChecked>
              {feature}
            </CheckThickListItem>
          ))}

          <Box mb={4} mt={2}>
            <Typography variant="h4" textAlign="center" fontWeight={500}>
              +
            </Typography>
          </Box>
          <Box mb={4} mt={2}>
            <Typography variant="subtitle1" textAlign="center">
              Future Features
            </Typography>
          </Box>
        </Box>

        <Box mb={6} mt={6}>
          <Box display="flex" justifyContent="center">
            <Typography variant="h4" color="textSecondary">
              {formatCurrency(4)}
            </Typography>
            <Typography variant="body1" color="textSecondary">
              .99 per month
            </Typography>
          </Box>
        </Box>

        <Box mb={6} width="100%">
          <StripePaymentButton>Select</StripePaymentButton>
        </Box>
      </Box>

      <Box mt={4} mb={6} display="flex" alignItems="center">
        <InfoRoundedIcon className={classes.info} />{' '}
        <Typography variant="subtitle2">
          Start or stop your membership anytime.
        </Typography>
      </Box>
    </Box>
  );
};
