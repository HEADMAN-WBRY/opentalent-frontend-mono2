import React from 'react';

import { Box } from '@mui/material';

import Typography from '@libs/ui/components/typography';

import { MemberPlanCardForFreeAccount } from './MemberPlanCardForFreeAccount';

interface LobbyPlanContentProps { }

export const LobbyPlanContent = (props: LobbyPlanContentProps) => {
  return (
    <>
      <Box mb={3}>
        <Typography component="span" variant="h5" fontWeight={400}>
          Your current plan:{' '}
        </Typography>
        <Typography component="span" variant="h5" fontWeight={700}>
          Visitor
        </Typography>
      </Box>

      <Box mb={8}>
        <Typography color="textSecondary">
          Here are the plans that we currently offer:
        </Typography>
      </Box>

      <Box mb={6}>
        <MemberPlanCardForFreeAccount />
      </Box>
    </>
  );
};
