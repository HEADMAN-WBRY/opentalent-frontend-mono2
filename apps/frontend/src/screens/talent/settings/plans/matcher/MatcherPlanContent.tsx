import { useIsComplementaryPaidTalentAccount } from 'hooks/talents/useTalentAccountType';
import React from 'react';

import { Box, Chip, CircularProgress, Grid, Tooltip } from '@mui/material';

import { AccountTypeEnum } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import { CancelSubscriptionModal } from '../CancelSubscriptionModal';
import { InvoiceLink } from '../InvoiceLink';
import { MatcherPlanCard } from './MatcherPlanCard';
import { PlanData } from './PlanData';
import { useRenewAction, useSubscriptionData } from './hooks';

interface PremiumPlanContentProps {
  plan: AccountTypeEnum;
}

export const MatcherPlanContent = ({ plan }: PremiumPlanContentProps) => {
  const {
    showRenewButton,
    showCancelButton,
    isSubscriptionInfoLoading,
    subscription,
  } = useSubscriptionData({ plan });
  const isComplemetaryPremium = useIsComplementaryPaidTalentAccount();

  const { renew, isRenewing } = useRenewAction();

  if (isSubscriptionInfoLoading || isRenewing) {
    return <CircularProgress size={50} color="secondary" />;
  }

  return (
    <>
      <Box mb={2}>
        <Typography component="span" variant="h5" fontWeight={400}>
          Your current plan:{' '}
        </Typography>
        <Typography component="span" variant="h5" fontWeight={700}>
          Matcher
        </Typography>
      </Box>

      <Box mb={4}>
        <InvoiceLink />
      </Box>

      <PlanData plan={plan} subscription={subscription} />

      <Box>
        <Box mb={6} mt={4}>
          <Grid spacing={4} container>
            {/* <Grid item> */}
            {/*   <PremiumPlanCard /> */}
            {/* </Grid> */}
            <Grid item>
              <MatcherPlanCard
                showRenewButton={showRenewButton}
                showCancelButton={showCancelButton}
                renew={renew}
                isRenewing={isRenewing}
              />

              <Box mt={6} display="flex" width="100%" justifyContent="center">
                {isComplemetaryPremium && (
                  <Tooltip title="You were given a complementary upgrade">
                    <Chip
                      color="success"
                      label={
                        <>
                          Complementary <b>(FREE)</b> upgrade
                        </>
                      }
                    />
                  </Tooltip>
                )}
              </Box>
            </Grid>
          </Grid>
        </Box>
      </Box>

      <CancelSubscriptionModal />
    </>
  );
};
