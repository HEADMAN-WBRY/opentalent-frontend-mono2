import { CheckThickListItem } from 'components/check-thick-list';
import React from 'react';

import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { formatCurrency } from '@libs/helpers/format';
import Button, { RouterButton } from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import { CANCEL_MODAL } from '../consts';

interface PremiumPlanCardProps {
  showRenewButton?: boolean;
  showCancelButton?: boolean;
  isRenewing: boolean;
  renew: VoidFunction;
}

const useStyles = makeStyles((theme) => ({
  card: {
    backgroundColor: '#F7F5F5',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    borderRadius: 16,
    padding: theme.spacing(8, 6),
    width: 364,
  },
  textBlock: {
    backgroundColor: 'white',
    padding: '6px 10px',
    borderRadius: 16,
    fontSize: 13,
    lineHeight: '18px',
    textAlign: 'center',
  },
  chip: {
    color: 'white',
  },
  textBlockDark: {
    backgroundColor: '#EEEEEE',
  },
}));

const FEATURES = [
  'Get Paid To Recruit',
  'Direct Community Search',
  'Become Talent Partner',
];

export const MatcherPlanCard = ({
  showRenewButton = false,
  showCancelButton = false,
  renew,
  isRenewing,
}: PremiumPlanCardProps) => {
  const classes = useStyles();

  return (
    <Box className={classes.card}>
      <Box mb={6} className={classes.textBlock}>
        <Typography
          whiteSpace="break-spaces"
          textAlign="center"
          variant="caption"
          fontWeight="600"
        >
          Europe’s #1 talent-centric membership.
        </Typography>
      </Box>
      <Box mb={6}>
        <Typography component="span" variant="h5" fontWeight={700}>
          Matcher
        </Typography>
      </Box>

      <Box width={250} margin="0 auto">
        {FEATURES.map((feature) => (
          <CheckThickListItem key={feature} isChecked>
            {feature}
          </CheckThickListItem>
        ))}

        <Box mb={4} mt={2}>
          <Typography variant="h4" textAlign="center" fontWeight={500}>
            +
          </Typography>
        </Box>
        <Box mb={4} mt={2}>
          <Typography variant="subtitle1" textAlign="center">
            Future Features
          </Typography>
        </Box>
      </Box>

      <Box mb={6} mt={2}>
        <Box display="flex" justifyContent="center">
          <Typography variant="h4" color="textSecondary">
            {formatCurrency(9)}
          </Typography>
          <Typography variant="body1" color="textSecondary">
            .99 per month
          </Typography>
        </Box>
      </Box>

      {/* <Box mb={4}> */}
      {/*   <Typography variant="caption" textAlign="center" color="text.secondary"> */}
      {/*     Get 2 months FREE with annual billing */}
      {/*   </Typography> */}
      {/* </Box> */}

      {showCancelButton && (
        <Box>
          <RouterButton
            to={(location) => ({
              ...location,
              state: {
                [CANCEL_MODAL]: true,
              },
            })}
            variant="text"
            color="info"
          >
            CANCEL SUBSCRIPTION
          </RouterButton>
        </Box>
      )}
      {showRenewButton && (
        <Box>
          <Button
            fullWidth
            size="large"
            rounded
            variant="contained"
            color="info"
            disabled={isRenewing}
            onClick={() => renew()}
          >
            renew subscription
          </Button>
        </Box>
      )}
    </Box>
  );
};
