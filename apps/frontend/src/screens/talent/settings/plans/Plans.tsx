import {
  useIsPaidMatcherAccount,
  useIsPaidMemberAccount,
  useTalentAccountType,
} from 'hooks/talents/useTalentAccountType';
import React from 'react';

import { Box } from '@mui/material';

import { FreePlanContent } from './free';
import { MatcherPlanContent } from './matcher';
import { PremiumPlanContent } from './premium';

interface PlansProps { }

const Plans = (props: PlansProps) => {
  const plan = useTalentAccountType();
  const isMember = useIsPaidMemberAccount();
  const isMatcher = useIsPaidMatcherAccount();

  if (isMatcher) {
    return <MatcherPlanContent plan={plan} />;
  }

  if (isMember) {
    return <PremiumPlanContent plan={plan} />;
  }

  return (
    <Box>
      <FreePlanContent />
    </Box>
  );
};

export default Plans;
