import { CheckThickListItem } from 'components/check-thick-list';
import StripePaymentButton from 'components/custom/talent/stripe-payment-button';
import React from 'react';

import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { formatCurrency } from '@libs/helpers/format';
import Typography from '@libs/ui/components/typography';

interface PremiumPlanCardForFreeAccountProps { }

const useStyles = makeStyles((theme) => ({
  card: {
    backgroundColor: '#F7F5F5',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    borderRadius: 16,
    padding: theme.spacing(8, 6),
    width: 364,
  },
  textBlock: {
    backgroundColor: 'white',
    padding: '6px 10px',
    borderRadius: 16,
    fontSize: 13,
    lineHeight: '18px',
    textAlign: 'center',
  },
  chip: {
    color: 'white',
  },
  textBlockDark: {
    backgroundColor: '#EEEEEE',
  },
}));

export const MEMBER_FEATURES = [
  'Curated Job Search',
  'Top Job Alerts',
  'Network with Alumni',
];

export const PremiumPlanCardForFreeAccount = (
  props: PremiumPlanCardForFreeAccountProps,
) => {
  const classes = useStyles();

  return (
    <Box className={classes.card}>
      <Box mb={6} className={classes.textBlock}>
        <Typography
          whiteSpace="break-spaces"
          textAlign="center"
          variant="caption"
        >
          <b>Europe’s #1 talent-centric membership.</b>
        </Typography>
      </Box>

      <Box mb={6}>
        <Typography component="span" variant="h5" fontWeight={700}>
          Member
        </Typography>
      </Box>

      <Box width={250} margin="0 auto">
        {MEMBER_FEATURES.map((feature) => (
          <CheckThickListItem key={feature} isChecked>
            {feature}
          </CheckThickListItem>
        ))}
        <Box mb={4} mt={2}>
          <Typography variant="h4" textAlign="center" fontWeight={500}>
            +
          </Typography>
        </Box>
        <Box mb={4} mt={2}>
          <Typography variant="subtitle1" textAlign="center">
            Future Features
          </Typography>
        </Box>
      </Box>

      <Box mb={6} mt={2}>
        <Box display="flex" justifyContent="center">
          <Typography variant="h4" color="textSecondary">
            {formatCurrency(4)}
          </Typography>
          <Typography variant="body1" color="textSecondary">
            .99 per month
          </Typography>
        </Box>
      </Box>

      {/* <Box mb={4}> */}
      {/*   <Typography variant="caption" textAlign="center" color="text.secondary"> */}
      {/*     Get 2 months FREE with annual billing */}
      {/*   </Typography> */}
      {/* </Box> */}

      <Box width="100%">
        <StripePaymentButton>Select</StripePaymentButton>
      </Box>
    </Box>
  );
};
