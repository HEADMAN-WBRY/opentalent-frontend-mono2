import React from 'react';

import { Box, Grid } from '@mui/material';

import Typography from '@libs/ui/components/typography';

import { MatcherPlanCard } from '../premium/MatcherPlanCard';
import { PremiumPlanCardForFreeAccount } from './PremiumPlanCardForFreeAccount';

interface FreePlanContentProps { }

export const FreePlanContent = (props: FreePlanContentProps) => {
  return (
    <>
      <Box mb={3}>
        <Typography component="span" variant="h5" fontWeight={400}>
          Your current plan:{' '}
        </Typography>
        <Typography component="span" variant="h5" fontWeight={700}>
          Visitor
        </Typography>
      </Box>

      <Box mb={8}>
        <Typography color="textSecondary">
          Here are the plans that we currently offer:
        </Typography>
      </Box>

      <Box mb={6}>
        <Grid container spacing={4}>
          <Grid item>
            <PremiumPlanCardForFreeAccount />
          </Grid>
          <Grid item>
            <MatcherPlanCard />
          </Grid>
        </Grid>
      </Box>
    </>
  );
};
