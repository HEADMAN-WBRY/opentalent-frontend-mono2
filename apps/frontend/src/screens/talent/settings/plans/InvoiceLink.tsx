import React from 'react';

import OpenInNewIcon from '@mui/icons-material/OpenInNew';

import { OuterLink } from '@libs/ui/components/typography';

interface InvoiceLinkProps { }

export const InvoiceLink = (props: InvoiceLinkProps) => {
  return (
    <div>
      You can get an invoice{' '}
      <OuterLink
        href="https://billing.stripe.com/p/login/8wM0173rA8GKeBy5kk"
        color="info.main"
        target="_blank"
      >
        here{' '}
        <OpenInNewIcon
          color="inherit"
          fontSize="small"
          style={{ marginBottom: -4 }}
        />
      </OuterLink>
    </div>
  );
};
