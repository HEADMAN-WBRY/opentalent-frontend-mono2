import { useMutation } from '@apollo/client';
import { GET_CURRENT_TALENT, UPDATE_TALENT_PROFILE } from 'graphql/talents';
import { useCurrentUser } from 'hooks/auth';
import { useSnackbar } from 'notistack';
import { noop } from 'utils/common';

import {
  Mutation,
  MutationUpdateTalentProfileArgs,
  TalentAccountSettings,
  TalentAccountSettingsInput,
} from '@libs/graphql-types';
import { FormikSubmit } from '@libs/helpers/form';

export const useInitialValues = (): TalentAccountSettingsInput => {
  const { data: userData } = useCurrentUser();
  const initialValues = userData?.currentTalent
    ?.account_settings as TalentAccountSettings;
  return {
    instant_matches_notifications_min_score:
      initialValues.instant_matches_notifications_min_score,
    receive_company_and_product_updates:
      initialValues.receive_company_and_product_updates,
    receive_direct_messages: initialValues.receive_direct_messages,
  };
};

export const useUpdateUser = ({
  onSuccess = noop,
}: {
  onSuccess?: VoidFunction;
}) => {
  const { data: userData } = useCurrentUser();
  const { enqueueSnackbar } = useSnackbar();
  const [request, { loading }] = useMutation<
    Mutation,
    MutationUpdateTalentProfileArgs
  >(UPDATE_TALENT_PROFILE, {
    onCompleted: () => {
      enqueueSnackbar('Updated', { variant: 'success' });
      onSuccess();
    },
    refetchQueries: [{ query: GET_CURRENT_TALENT }],
  });
  const userId = userData?.currentTalent?.id;
  const updateRequest: FormikSubmit<TalentAccountSettingsInput> = (
    account_settings,
  ) => {
    if (!userId) {
      return;
    }
    request({
      variables: {
        talent_id: userId,
        account_settings,
      },
    });
  };

  return { updateRequest, loading };
};
