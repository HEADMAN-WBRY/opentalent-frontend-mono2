export enum SettingsTabs {
  General = 'general',
  Notifications = 'notifications',
  Plans = 'plans',
}
