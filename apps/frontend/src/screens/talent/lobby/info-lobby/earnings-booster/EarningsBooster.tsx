import React from 'react';

import Button from '@libs/ui/components/button';

import CardsPointBlicks, {
  ItemType,
} from '../../accounts-block/CardsPointBlicks';
import booster from './earnings-booster@2x.png';
import recruiter from './recruter@2x.png';

interface EarningsBoosterProps { }

export const ITEMS: Array<ItemType> = [
  {
    id: 1,
    title: 'Monthly draw',
    subtitle: `Boost your earnings`,
    text: 'Every month, one of our members wins €500 in extra earnings.Will it be you ? ',
    points: ['1x draw every month', 'Members automatically play'],
    imgComponent: <img srcSet={`${booster} 2x`} alt="Earnings booster" />,
    hideIndex: true,
    revert: false,
    additionalContent: (
      <Button
        variant="outlined"
        color="info"
        href="https://opentalent.notion.site/Earnings-Booster-82d4365ed9204cd99dcdac02ed238d55"
        {...{ target: '_blank' }}
      >
        Learn more
      </Button>
    ),
  },
  {
    id: 2,
    title: 'Talent matcher',
    subtitle: `Get paid to recruit`,
    text: 'Start recruiting with us. Join the Talent Matcher program(for recruiters only) ',
    points: [
      'Earn on your terms',
      'Min. €2.000 Finder Fees.',
      'Direct clients only',
    ],
    imgComponent: <img srcSet={`${recruiter} 2x`} alt="Earnings booster" />,
    hideIndex: true,
    revert: true,
  },
];

export const EarningsBooster = (props: EarningsBoosterProps) => {
  return (
    <div>
      <CardsPointBlicks items={ITEMS} />
    </div>
  );
};
