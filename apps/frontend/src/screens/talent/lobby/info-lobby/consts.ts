import { AccordionProps } from '../shared/AccordionItem';

export const SOON_ITEMS: Pick<AccordionProps, 'title' | 'text'>[] = [
  {
    title: 'How much does the membership cost?',
    text: 'Membership to OpenTalent costs €4.99 per month. All members have the same all-access plan. The Talent Matcher program costs €9.99 per month. There are no extra costs, or contracts.',
  },

  {
    title: 'How do I cancel?',
    text: 'OpenTalent is flexible. You can cancel your membership online (in Settings). There are no cancellation fees – start or stop your account anytime.',
  },
  {
    title: 'What happens if I do not pay?',
    text: `You will remain a Visitor. We will verify your account within 5 days. Afterwards, Members can search for you in the OpenTalent directory and reach out by chat, and you can reply. Members can also invite you to apply for jobs.`,
  },
  {
    title: 'What happens after I become a full member?',
    text: `You can access the platform and start using all features.`,
  },
];
