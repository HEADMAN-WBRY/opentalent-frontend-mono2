import StripePaymentButton from 'components/custom/talent/stripe-payment-button';
import { ConnectedPageLayout } from 'components/layout/page-layout';
import React from 'react';
import { pathManager } from 'routes';

import AddRoundedIcon from '@mui/icons-material/AddRounded';
import ArrowBackIosNewRoundedIcon from '@mui/icons-material/ArrowBackIosNewRounded';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { RouterButton } from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import CardsPointBlicks from '../accounts-block/CardsPointBlicks';
import { TALENT_CARDS_ITEMS } from '../accounts-block/tabs-content/TalentTabContent';
import { CommingSoon } from '../comming-soon';
import { EarningsBooster } from './earnings-booster';

interface TalentLobbyProps { }

const useStyles = makeStyles((theme) => ({
  contentWrapper: {
    margin: '0 auto',
    width: '100%',

    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(4, 4),
    },
  },
  content: {
    maxWidth: 800,
    margin: '0 auto',
  },
  info: {
    marginRight: theme.spacing(2),
    color: theme.palette.info.main,
  },
}));

export const TalentLobbyInfo = (props: TalentLobbyProps) => {
  const classes = useStyles();

  return (
    <ConnectedPageLayout
      classes={{ contentWrapper: classes.contentWrapper }}
      drawerProps={{}}
      documentTitle="Lobby"
    >
      <Box>
        <RouterButton
          color="info"
          to={pathManager.talent.lobby.main.generatePath()}
          startIcon={<ArrowBackIosNewRoundedIcon />}
        >
          BACK
        </RouterButton>
      </Box>

      <Box className={classes.content}>
        <Box pb={6}>
          <Typography variant="body1" color="info.main" textAlign="center">
            MEMBERSHIP BENEFITS
          </Typography>
        </Box>
        <Box mx="auto" mb={6} maxWidth={670}>
          <Typography variant="h4" textAlign="center" paragraph>
            What do members get?
          </Typography>

          <Typography textAlign="center">
            Expect purpose-built tooling and new avenues to help boost your
            income together with direct access to an exclusive network of
            like-minded peers - all under one roof.
          </Typography>
        </Box>

        <Box mb={6} width="100%" display="flex" justifyContent="center">
          <Box width={280}>
            <StripePaymentButton>ACTIVATE my membership</StripePaymentButton>
          </Box>
        </Box>

        <Box
          mt={4}
          mb={10}
          display="flex"
          alignItems="center"
          justifyContent="center"
        >
          <InfoOutlinedIcon className={classes.info} />{' '}
          <Typography variant="subtitle2">Start or stop anytime.</Typography>
        </Box>

        <Box mt={8} mb={12}>
          <CardsPointBlicks items={TALENT_CARDS_ITEMS} />
        </Box>

        <Box style={{ fontSize: 40 }} display="flex" justifyContent="center">
          <AddRoundedIcon fontSize="inherit" />
        </Box>

        <Box mt={8} mb={12}>
          <EarningsBooster />
        </Box>

        <Box mb={14}>
          <CommingSoon />
        </Box>

        {/* <Box mb={8}> */}
        {/*   <Rules /> */}
        {/* </Box> */}
      </Box>
    </ConnectedPageLayout>
  );
};

export default TalentLobbyInfo;
