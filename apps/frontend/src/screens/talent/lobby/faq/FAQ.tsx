import React, { useState } from 'react';

import { Box, Typography } from '@mui/material';

import { OuterLink } from '@libs/ui/components/typography';

import { AccordionItem, AccordionProps } from '../shared/AccordionItem';
import { SOON_ITEMS } from './consts';

interface FAQProps {
  items?: Pick<AccordionProps, 'title' | 'text'>[];
  title?: string;
}

export const FAQ = ({
  items = SOON_ITEMS,
  title = 'Frequently Asked Questions:',
}: FAQProps) => {
  const [activeIndex, setActiveIndex] = useState<number>();

  return (
    <div>
      <Box mt={8} mb={8}>
        <Typography variant="h6" textAlign="center">
          {title}
        </Typography>
      </Box>
      <Box maxWidth={600} style={{ margin: '0 auto' }}>
        {items.map((i, index) => (
          <AccordionItem
            {...i}
            isActive={index === activeIndex}
            onChange={() =>
              setActiveIndex(index === activeIndex ? undefined : index)
            }
          />
        ))}
      </Box>

      <Box mt={6} mb={12}>
        <Typography textAlign="center">
          Do you have other questions? Write an email to{' '}
          <OuterLink color="info.main" href={`mailto:hello@opentalent.co`}>
            hello@opentalent.co
          </OuterLink>
        </Typography>
      </Box>
    </div>
  );
};
