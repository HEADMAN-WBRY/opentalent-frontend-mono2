import { AccordionProps } from '../shared/AccordionItem';

export const SOON_ITEMS: Pick<AccordionProps, 'title' | 'text'>[] = [
  {
    title: 'Who is OpenTalent for?',
    text: 'OpenTalent is designed for high-skilled professionals in Europe seeking job opportunities, networking, and career growth, as well as companies and recruiters looking to hire top talent.',
  },
  // {
  //   title: 'How much does the membership cost?',
  //   text: 'Membership to OpenTalent costs €4.99 per month. All members have the same all-access plan. The Talent Matcher program costs €9.99 per month. There are no extra costs, or contracts.',
  // },
  // {
  //   title: 'How do I cancel?',
  //   text: 'OpenTalent is flexible. You can cancel your membership online (in Settings). There are no cancellation fees – start or stop your account anytime.',
  // },
  // {
  //   title: 'What happens if I do not pay?',
  //   text: 'You will remain In The Lobby. We will verify your account within 5 days. Afterwards, members can search for you in the OpenTalent directory and reach out by chat, and you can reply. Members can also invite you to apply for jobs. You can still access the Perks page and invite up to 5 friends/colleagues to the community',
  // },
  {
    title: 'What happens after I become a member?',
    text: `You can access the platform and start using all features.`,
  },
  {
    title: 'How long until I get verified?',
    text: `For members who are pending verification, we will verify your account within 1 working day. For non-members, verification can take up to 5 working days.`,
  },
];
