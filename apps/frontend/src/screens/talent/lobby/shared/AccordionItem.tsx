import React from 'react';

import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import {
  AccordionSummary,
  Typography,
  AccordionDetails,
  Accordion,
  Grid,
} from '@mui/material';
import { makeStyles } from '@mui/styles';

export interface AccordionProps {
  title: string;
  text: React.ReactNode;
  sideContent?: React.ReactNode;

  isActive: boolean;
  onChange: VoidFunction;
}

const useStyles = makeStyles((theme) => ({
  root: {
    borderRadius: `${theme.spacing(4)} !important`,
    overflow: 'hidden',
    marginBottom: `${theme.spacing(2)} !important`,
  },

  details: {
    background: theme.palette.grey[100],
    padding: theme.spacing(4),
  },

  summaryContent: {
    margin: `${theme.spacing(5, 2)} !important`,
  },
}));

export const AccordionItem = ({
  title,
  text,
  isActive,
  onChange,
  sideContent,
}: AccordionProps) => {
  const classes = useStyles();

  return (
    <Accordion
      classes={{ root: classes.root }}
      elevation={0}
      expanded={isActive}
      onChange={onChange}
    >
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        classes={{ content: classes.summaryContent }}
      >
        <Grid container>
          <Grid item flexGrow={1}>
            <Typography>{title}</Typography>
          </Grid>
          <Grid item>{sideContent}</Grid>
        </Grid>
      </AccordionSummary>
      <AccordionDetails classes={{ root: classes.details }}>
        <Typography variant="body2">{text}</Typography>
      </AccordionDetails>
    </Accordion>
  );
};
