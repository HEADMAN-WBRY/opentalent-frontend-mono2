import React from 'react';

import connectImg from './assets/connect@2x.png';
import matchesImg from './assets/matches@2x.png';
import perksImg from './assets/perks@2x.png';
import referImg from './assets/refer@2x.png';

export interface ItemType {
  id: number;
  title: string;
  subtitle: string;
  text: string;
  points: Array<string>;
  imgComponent: JSX.Element;
  revert: boolean;
}

export const WHAT_TO_EXPECT_ITEMS: Array<ItemType> = [
  {
    id: 1,
    title: 'JOB BOARD',
    subtitle: `ChatGPT Job Matching`,
    text: 'Leverage the power of ChatGPT to find jobs that perfectly match your skills and needs.',
    points: [
      '10K+ high-skilled jobs across Europe',
      'Find top jobs in seconds + instant notifications ',
      'New jobs added every day',
    ],
    imgComponent: <img srcSet={`${matchesImg} 2x`} alt="matches" />,
    revert: true,
  },
  {
    id: 2,
    title: `COMMUNITY`,
    subtitle: `Find The Skills You Need`,
    text: `Meet new advisors, learn new skills, or find new colleagues on OpenTalent.`,
    points: [
      '27.000 verified member profiles',
      '3.000 skills across 29 countries',
      'Chat / connect with anyone',
    ],
    imgComponent: <img srcSet={`${connectImg} 2x`} alt="matches" />,
    revert: false,
  },
  {
    id: 3,
    title: `INVITE & EARN`,
    subtitle: `Refer & Earn programs`,
    text: `Know talented people? Refer candidates to open jobs and get paid when they get hired.`,
    points: [
      '€1.000 - €5.000 in Finder Fees, guaranteed',
      'Refer up to 5 candidates per month',
      'Dedicated program for recruiters',
    ],
    imgComponent: <img srcSet={`${referImg} 2x`} alt="matches" />,
    revert: true,
  },
  {
    id: 4,
    title: `PERKS`,
    subtitle: `Serious Perks`,
    text: `The best deals in town for you. €2.500+ in savings on exclusive offers. New perks added every week.`,
    points: [
      'Co-working, insurances, car rentals...',
      'Discounts from 15% to 50%',
      'Request your favourite perks & promos',
    ],
    imgComponent: <img srcSet={`${perksImg} 2x`} alt="matches" />,
    revert: false,
  },
];
