import cn from 'classnames';
import React from 'react';

import { Box, Grid, Container } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { useMediaQueries } from '@libs/helpers/hooks/media-queries';
import Typography from '@libs/ui/components/typography';

import { WHAT_TO_EXPECT_ITEMS, ItemType } from './consts';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    maxWidth: '810px',
    margin: '0 auto',
  },
  container: {
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
      justifyContent: 'center',
    },

    '&:not(:last-child)': {
      marginBottom: theme.spacing(30),
    },

    [theme.breakpoints.down('md')]: {
      '&:not(:last-child)': {
        marginBottom: theme.spacing(20),
      },
    },
  },
  miniTitle: {
    fontSize: '12px',
    lineHeight: '20px',
    color: theme.palette.info.main,
  },
  title: {
    marginBottom: theme.spacing(2),
    whiteSpace: 'pre-line',

    [theme.breakpoints.down('sm')]: {
      fontSize: '20px',
      lineHeight: '28px',
    },
  },
  text: {
    lineHeight: '28px',
    maxWidth: '360px',
  },
  ul: {
    listStyleType: 'square',
    paddingLeft: theme.spacing(5),

    '& > li::marker': {
      color: theme.palette.info.main,
    },
  },
  point: {
    marginBottom: theme.spacing(1),
  },
  textSide: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
  },
  img: {
    display: 'flex',
    alignItems: 'center',

    [theme.breakpoints.down('md')]: {
      justifyContent: 'center',
      marginBottom: theme.spacing(10),

      '& > img': {
        maxWidth: '100%',
      },
    },
  },
}));

const WhatToExpect: React.FC = () => {
  const classes = useStyles();
  const { isSM } = useMediaQueries();

  const renderTextSide = (item: ItemType) => (
    <Box className={classes.textSide}>
      <Box>
        <Typography className={classes.miniTitle} variant="subtitle2">
          {item.title}
        </Typography>
        <Typography className={classes.title} variant="h6" fontWeight={500}>
          {item.id}. {item.subtitle}
        </Typography>
        <Typography className={classes.text} variant="body2">
          {item.text}
        </Typography>
        <ul className={classes.ul}>
          {item.points.map((point, idx) => {
            const key = point + idx;

            return (
              <li key={key}>
                <Typography className={classes.point} variant="body2">
                  {point}
                </Typography>
              </li>
            );
          })}
        </ul>
      </Box>
    </Box>
  );

  return (
    <Box className={classes.wrapper}>
      <Box mb={8}>
        <Typography variant="h6" textAlign="center" fontWeight={500}>
          What to expect?
        </Typography>
        <Typography variant="subtitle1" color="info.main" textAlign="center">
          Membership benefits:
        </Typography>
      </Box>

      <div>
        {WHAT_TO_EXPECT_ITEMS.map((item) => (
          <Grid
            container
            key={item.id}
            className={classes.container}
            direction={isSM && !item.revert ? 'column-reverse' : 'row'}
            spacing={4}
          >
            <Grid
              item
              xs={isSM ? 12 : 6}
              className={cn({ [classes.img]: item.revert })}
            >
              {item.revert ? item.imgComponent : renderTextSide(item)}
            </Grid>
            <Grid
              item
              xs={isSM ? 12 : 6}
              className={cn({ [classes.img]: !item.revert })}
            >
              {item.revert ? renderTextSide(item) : item.imgComponent}
            </Grid>
          </Grid>
        ))}
      </div>
    </Box>
  );
};

export default WhatToExpect;
