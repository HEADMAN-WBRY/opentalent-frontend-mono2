import React from 'react';

import { Box, Typography } from '@mui/material';

import { EXTERNAL_LINKS } from '@libs/helpers/consts';
import { OuterLink } from '@libs/ui/components/typography';

interface RulesProps { }

export const Rules = (props: RulesProps) => {
  return (
    <div>
      <Box mb={8}>
        <Typography variant="h5" textAlign="center">
          Rules Apply:
        </Typography>
      </Box>

      <Box maxWidth={800} style={{ margin: '0 auto' }}>
        <Typography textAlign="center">
          As a member of OpenTalent, you're expected to follow our guidelines to
          maintain a trustworthy and collaborative environment. This includes
          adhering to messaging rules. We reserve the right to suspend any
          membership that violates these principles.
        </Typography>
      </Box>

      <Box mb={4} mt={4}>
        <Typography textAlign="center">
          For details, please visit{' '}
          <OuterLink
            color="info.main"
            href={EXTERNAL_LINKS.communityGuidelines}
            target="_blank"
            style={{ textDecoration: 'underline' }}
          >
            Community Guidelines
          </OuterLink>
        </Typography>
      </Box>
    </div>
  );
};
