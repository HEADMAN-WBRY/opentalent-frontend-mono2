import { ReactComponent as InviteIcon } from 'assets/icons/invite.svg';
import { ReactComponent as PerksIcon } from 'assets/icons/perks.svg';
import React from 'react';
import { pathManager } from 'routes';

import ChatOutlinedIcon from '@mui/icons-material/ChatOutlined';
import WorkOutlineIcon from '@mui/icons-material/WorkOutline';
import { Box, Button, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

interface PagesCardProps { }

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 900,
    margin: '0 auto',
    padding: theme.spacing(8, 8),
    background: 'white',
    borderRadius: theme.spacing(4),
  },
  text: {
    maxWidth: 135,

    [theme.breakpoints.down('sm')]: {
      maxWidth: 240,
    },
  },
}));

const CARDS = [
  {
    text: 'Search for and apply to jobs posted on OpenTalent.',
    title: 'Jobs',
    Icon: WorkOutlineIcon,
    link: pathManager.talent.jobBoard.generatePath(),
  },
  {
    text: 'Access to Perks & Benefits exclusive to our community.',
    title: 'Perks',
    Icon: PerksIcon,
    link: pathManager.perks.generatePath(),
  },
  {
    text: 'Refer someone to a job and earn €1.000 if they get hired.',
    title: 'Invite & Earn',
    Icon: InviteIcon,
    link: pathManager.talent.invite.generatePath(),
  },
  {
    text: 'Receive and reply to incoming messages from members.',
    title: 'Messenger',
    Icon: ChatOutlinedIcon,
    link: pathManager.chat.generatePath(),
  },
];

export const PagesCard = (props: PagesCardProps) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container justifyContent="space-around" spacing={6}>
        {CARDS.map((i) => (
          <Grid item key={i.title}>
            <Box
              display="flex"
              flexDirection="column"
              alignItems="center"
              height={30}
              mb={3}
            >
              <Typography color="info.main">
                <i.Icon color="inherit" />
              </Typography>
            </Box>

            <Box mb={4}>
              <Typography variant="subtitle1" textAlign="center">
                {i.title}
              </Typography>
            </Box>
            <Box className={classes.text} mb={4}>
              <Typography
                variant="body2"
                textAlign="center"
                color="text.secondary"
              >
                {i.text}
              </Typography>
            </Box>

            <Box display="flex" justifyContent="center">
              <Button href={i.link} variant="outlined" color="info">
                VISIT NOW
              </Button>
            </Box>
          </Grid>
        ))}
      </Grid>
    </div>
  );
};
