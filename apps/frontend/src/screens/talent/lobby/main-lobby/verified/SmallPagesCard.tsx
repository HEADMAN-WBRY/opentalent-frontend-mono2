import React from 'react';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import booster from './assets/booster@2x.png';
import features from './assets/features@2x.png';
import networking from './assets/networking@2x.png';

interface PagesCardProps { }

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 600,
    margin: '0 auto',
    padding: theme.spacing(8, 8),
    background: 'white',
    borderRadius: theme.spacing(4),
  },
  text: {
    maxWidth: 150,

    [theme.breakpoints.down('sm')]: {
      maxWidth: 240,
    },
  },
}));

const CARDS = [
  {
    title: 'Earnings Booster',
    Icon: booster,
  },
  {
    title: 'Networking',
    Icon: networking,
  },
  {
    title: 'New Features',
    Icon: features,
  },
];

export const SmallPagesCard = (props: PagesCardProps) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container justifyContent="space-between">
        {CARDS.map((i) => (
          <Grid item key={i.title} style={{ width: 140 }}>
            <Box
              display="flex"
              flexDirection="column"
              alignItems="center"
              mb={3}
            >
              <Typography color="info.main">
                <img srcSet={`${i.Icon} 2x`} alt="icon" />
              </Typography>
            </Box>

            <Box mb={4}>
              <Typography variant="subtitle1" textAlign="center">
                {i.title}
              </Typography>
            </Box>
          </Grid>
        ))}
      </Grid>

      <Box mt={2}>
        <Grid container justifyContent="center">
          <Grid item>
            <Button variant="outlined" color="info">
              LEARN MORE
            </Button>
          </Grid>
        </Grid>
      </Box>
    </div>
  );
};
