import React from 'react';
import { pathManager } from 'routes';

import { Grid } from '@mui/material';
import { green } from '@mui/material/colors';
import { makeStyles } from '@mui/styles';
import { Box } from '@mui/system';

import { RouterButton } from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

interface ProgramCardsProps { }

const useStyles = makeStyles((theme) => ({
  content: {
    maxWidth: 800,
    margin: '0 auto',
  },
  card: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8),
    borderRadius: theme.spacing(4),
    maxWidth: 386,
  },

  point: {
    '&:before': {
      content: '""',
      display: 'inline-block',
      marginRight: theme.spacing(2),
      width: 6,
      height: 6,
      backgroundColor: green[600],
      transform: 'translateY(-2px)',
    },
  },
}));

const CARDS_DATA = [
  {
    subtitle: 'FULL MEMBERSHIP',
    title: 'Jobs & Community Access',
    text: 'For €4,99 per month you’ll get access to thousands of curated jobs, AI-powered job matching and our vetted community.',
    points: [
      'Get notified when new top jobs open',
      'Learn why jobs are a match for you',
    ],
    url: pathManager.talent.lobby.member.generatePath(),
  },
  {
    subtitle: 'TALENT MATCHER PROGRAM',
    title: 'Recruit With Us',
    text: 'For €9,99 per month you can join our Talent Matcher program and work with our team to place candidates, and earn extra money.',
    points: [
      'Earn up to €5.000 per placement',
      'For mid / senior recruiters in Europe',
    ],
    url: pathManager.talent.lobby.matcher.generatePath(),
  },
];

export const ProgramCards = (props: ProgramCardsProps) => {
  const classes = useStyles();

  return (
    <div className={classes.content}>
      <Grid container spacing={6}>
        {CARDS_DATA.map((item) => (
          <Grid item key={item.text}>
            <Box className={classes.card}>
              <Typography
                variant="subtitle2"
                paragraph
                textAlign="center"
                color="info.main"
              >
                {item.subtitle}
              </Typography>
              <Box mb={8}>
                <Typography variant="h6" textAlign="center" fontWeight={500}>
                  {item.title}
                </Typography>
              </Box>
              <Typography
                variant="body2"
                paragraph
                style={{ letterSpacing: '0px' }}
              >
                {item.text}
              </Typography>
              <Box mt={2} mb={8}>
                {item.points.map((point) => (
                  <Typography
                    className={classes.point}
                    variant="body2"
                    key={point}
                  >
                    {point}
                  </Typography>
                ))}
              </Box>

              <Box display="flex" justifyContent="center">
                <RouterButton to={item.url} variant="contained" color="info">
                  LEARN MORE
                </RouterButton>
              </Box>
            </Box>
          </Grid>
        ))}
      </Grid>
    </div>
  );
};
