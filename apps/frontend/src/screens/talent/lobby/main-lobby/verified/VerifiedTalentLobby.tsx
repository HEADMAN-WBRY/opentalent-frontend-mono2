import { ConnectedPageLayout } from 'components/layout/page-layout';
import React from 'react';
import { pathManager } from 'routes';

import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography, {
  OuterLink,
  RouterLink,
} from '@libs/ui/components/typography';

import { PagesCard } from './PagesCard';
import { ProgramCards } from './ProgramCards';

interface TalentLobbyProps { }

const useStyles = makeStyles((theme) => ({
  contentWrapper: {
    margin: '0 auto',
    width: '100%',

    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(4, 4),
    },
  },
  content: {
    maxWidth: 800,
    margin: '0 auto',
  },
}));

const MainTalentLobby = (props: TalentLobbyProps) => {
  const classes = useStyles();

  return (
    <ConnectedPageLayout
      classes={{ contentWrapper: classes.contentWrapper }}
      drawerProps={{}}
      documentTitle="Lobby"
    >
      <Box className={classes.content}>
        <Box mx="auto" mt={5} mb={6} maxWidth={670}>
          <Box mb={8}>
            <Typography variant="h4" textAlign="center">
              Welcome to the club 🎉
            </Typography>
          </Box>

          <Typography textAlign="center">
            It’s official - you are now a verified ‘Visiting’ member of
            OpenTalent, which gives you access to a few of the benefits of our
            platform. Make sure to keep{' '}
            <RouterLink
              to={pathManager.talent.profile.generatePath()}
              color="info.main"
            >
              your profile
            </RouterLink>{' '}
            up-to-date so members can connect with you for opportunities.
          </Typography>
        </Box>

        <Box mb={12}>
          <PagesCard />
        </Box>

        <Box maxWidth={660} mx="auto" mb={12}>
          <Typography variant="body2" textAlign="center">
            To unlock the full benefits of OpenTalent, and enable our team to
            continue to build the platform and drive value for the community, we
            offer two paid memberships.
          </Typography>
        </Box>

        <Box mb={12}>
          <ProgramCards />
        </Box>

        <Box maxWidth={660} mx="auto" mb={12}>
          <Typography variant="h5" textAlign="center" paragraph>
            Rules apply:
          </Typography>

          <Typography variant="body2" textAlign="center" paragraph>
            As a member of OpenTalent, you're expected to follow our guidelines
            to maintain a trustworthy and collaborative environment. This
            includes adhering to messaging rules. We reserve the right to
            suspend any membership that violates these principles.
          </Typography>

          <Typography variant="body2" textAlign="center">
            For details, please visit{' '}
            <OuterLink href="#" color="info.main">
              Community Guidelines
            </OuterLink>
          </Typography>
        </Box>
      </Box>
    </ConnectedPageLayout>
  );
};

export default MainTalentLobby;
