import React from 'react';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import poster from './poster@2x.png';

interface TextBlock1Props { }

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 720,
    margin: '0 auto',
  },
  image: {
    transform: 'translate(-10px, -10px)',
  },

  imageWrapper: {
    [theme.breakpoints.down('md')]: {
      display: 'flex',
      justifyContent: 'center',
    },
  },
  textBlock: {
    [theme.breakpoints.down('md')]: {
      flexDirection: 'column',
    },
  },
  texts: {
    '& *': {
      letterSpacing: 0,
    },
  },
}));

export const TextBlock1 = (props: TextBlock1Props) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Typography variant="h6" textAlign="center" fontWeight={500} paragraph>
        Life of a professional can be tough…
      </Typography>

      <Grid className={classes.textBlock} container wrap="nowrap" spacing={4}>
        <Grid item className={classes.imageWrapper}>
          <img className={classes.image} srcSet={`${poster} 2x`} alt="poster" />
        </Grid>
        <Grid item className={classes.texts}>
          <Typography paragraph variant="body2">
            Scouring the job market for the perfect fit often feels like
            searching for a needle in a haystack— you invest time, energy, and
            hope, only to come up short or settle for less.{' '}
          </Typography>
          <Typography paragraph variant="body2">
            And let's face it, when you're going solo in your job hunt, it can
            feel like it's you against the world. Networking events can be hit
            or miss, and online platforms rarely give you what you're truly
            looking for.
          </Typography>
          <Typography paragraph variant="body2">
            To top it off, time is of the essence. Limited time means limited
            chances to tap into new avenues for income, side-lining potential
            growth opportunities that could boost your career.
          </Typography>
        </Grid>
      </Grid>
    </Box>
  );
};
