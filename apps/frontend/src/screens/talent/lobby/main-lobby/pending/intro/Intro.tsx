import { ReactComponent as CrownIcon } from 'assets/icons/crown.svg';
import StripePaymentButton from 'components/custom/talent/stripe-payment-button';
import React from 'react';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import logo from './title-logo.svg';

interface IntroProps { }

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 710,
    margin: '0 auto',
  },
  icon: {
    '& path': {
      fill: theme.palette.info.dark,
      fillOpacity: 1,
    },
  },

  title: {
    textAlign: 'center',
    lineHeight: '48px',

    '& span': {
      backgroundImage: `url(${logo})`,
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      backgroundSize: 'contain',
    },
  },

  infoIcon: {
    color: theme.palette.info.main,
    transform: 'translateY(5px)',
  },
}));

export const Intro = (props: IntroProps) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Box pb={8}>
        <Grid justifyContent="center" container spacing={2}>
          <Grid item>
            <CrownIcon className={classes.icon} />
          </Grid>
          <Grid item>
            <Typography variant="body1" color="info.main" textAlign="center">
              Welcome to Europe’s #1 talent-centric platform.
            </Typography>
          </Grid>
        </Grid>
      </Box>
      <Box mb={8}>
        <Typography variant="h4" className={classes.title}>
          We help <span>you</span> thrive.
        </Typography>
      </Box>
      <Box mb={8}>
        <Typography textAlign="center" variant="body2">
          Since 2021, we've been obsessing about what talent across Europe
          wants. OpenTalent is our attempt to bring to market something
          refreshingly new; an online membership that unlocks new opportunities
          for its members, far beyond just jobs.
        </Typography>
      </Box>

      <Grid container justifyContent="center" spacing={4}>
        <Grid item>
          <StripePaymentButton variant="contained" color="info">
            ACTIVATE my membership
          </StripePaymentButton>
        </Grid>
        {/* <Grid item> */}
        {/*   <RouterButton */}
        {/*     variant="outlined" */}
        {/*     color="info" */}
        {/*     style={{ minWidth: 280 }} */}
        {/*     size="large" */}
        {/*     to={pathManager.talent.lobby.info.generatePath()} */}
        {/*   > */}
        {/*     Learn More */}
        {/*   </RouterButton> */}
        {/* </Grid> */}
      </Grid>
    </div>
  );
};
