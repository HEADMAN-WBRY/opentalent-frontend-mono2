import React from 'react';
import { pathManager } from 'routes';

import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { RouterButton } from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

interface AboutMembershipProps {
  scrollToTop: () => void;
}

const ITEMS = [
  'Job Matching',
  'Job Alerts',
  'Networking',
  'Earnings Booster',
  'Talent Matcher',
  'New Features',
];

const useStyles = makeStyles((theme) => ({
  item: {
    whiteSpace: 'nowrap',
    maxWidth: 200,
    marginBottom: theme.spacing(2),

    '& svg': {
      color: theme.palette.success.dark,
    },
  },
  card: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 2, 6),
    maxWidth: 530,
    borderRadius: theme.spacing(4),
    margin: `${theme.spacing(6)} auto`,
  },
  wrap: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    marginBottom: theme.spacing(4),
  },
}));

export const AboutMembership = ({ scrollToTop }: AboutMembershipProps) => {
  const classes = useStyles();

  return (
    <div>
      <Box mb={4}>
        <Typography fontWeight={500} variant="h5" textAlign="center">
          About the Membership
        </Typography>
      </Box>

      <Box maxWidth={800} style={{ margin: '0 auto' }}>
        <Typography textAlign="center" variant="body2">
          As a member, you gain access to a vetted community and a powerful
          platform designed to provide tailored job matches, foster meaningful
          new connections, and unlock diverse income-boosting opportunities
          combined with continuous new feature drops.
        </Typography>
      </Box>

      <Box className={classes.card}>
        <Box className={classes.wrap}>
          {ITEMS.map((item, index) => (
            <Grid container className={classes.item} spacing={2} wrap="nowrap">
              <Grid item>
                <CheckCircleOutlineIcon color="inherit" />
              </Grid>
              <Grid item xs={6}>
                <Typography variant="subtitle1" fontWeight={500}>
                  {item}
                </Typography>
              </Grid>
            </Grid>
          ))}
        </Box>
        <Grid container justifyContent="center">
          <Grid item>
            <RouterButton
              to={pathManager.talent.lobby.info.generatePath()}
              onClick={scrollToTop}
              variant="outlined"
              color="info"
            >
              LEARN MORE
            </RouterButton>
          </Grid>
        </Grid>
      </Box>
    </div>
  );
};
