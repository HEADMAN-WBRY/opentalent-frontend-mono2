import useMediaQueries from 'hooks/common/useMediaQueries';
import React from 'react';

import CloseIcon from '@mui/icons-material/Close';
import { Grid, IconButton } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Box } from '@mui/system';

import Typography, { OuterLink } from '@libs/ui/components/typography';

import img from './booster@2x.png';
import { useCheckBoosterBannerState } from './hooks';

interface BoosterBannerProps { }

const useStyles = makeStyles((theme) => ({
  root: {
    border: `3px solid ${theme.palette.primary.main}`,
    borderRadius: theme.spacing(4),
    background: theme.palette.secondary.main,
    color: 'white',
    padding: theme.spacing(4),
    maxWidth: 1190,
    margin: '0 auto 18px',
    position: 'relative',

    '& img': {
      height: 90,
      width: 160,
    },
  },

  closeBtn: {
    position: 'absolute',
    top: 0,
    right: 0,
  },
}));

export const BoosterBanner = (props: BoosterBannerProps) => {
  const classes = useStyles();
  const { isOpen, changeVisibility } = useCheckBoosterBannerState();
  const { isMD } = useMediaQueries();

  if (!isOpen) {
    return null;
  }

  return (
    <div className={classes.root}>
      <Grid
        container
        spacing={4}
        wrap={isMD ? 'wrap' : 'nowrap'}
        justifyContent="center"
      >
        <Grid item>
          <img srcSet={`${img} 2x`} alt="booster" />
        </Grid>
        <Grid
          item
          flexGrow={1}
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
          }}
        >
          <Box>
            <Box mb={1}>
              <Typography variant="h6" color="white" fontWeight={500}>
                Activate your membership to automatically play the ‘Earnings
                Booster’.
              </Typography>
            </Box>
            <Typography variant="body2" color="white" fontWeight={500}>
              On the 1st of every month, we give away{' '}
              <Typography color="primary" component="span">
                €500 in extra earnings.
              </Typography>{' '}
              <OuterLink
                target="_blank"
                color="white"
                href="https://opentalent.notion.site/Earnings-Booster-82d4365ed9204cd99dcdac02ed238d55"
                style={{ textDecoration: 'underline' }}
              >
                Click here
              </OuterLink>{' '}
              to learn more.
            </Typography>
          </Box>
        </Grid>
      </Grid>

      <IconButton
        onClick={() => changeVisibility()}
        color="inherit"
        className={classes.closeBtn}
      >
        <CloseIcon />
      </IconButton>
    </div>
  );
};
