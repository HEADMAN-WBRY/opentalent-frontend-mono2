import { LOCAL_STORAGE_KEYS } from 'consts/ls-keys';
import { useCurrentUser } from 'hooks/auth';
import { useState } from 'react';

export const useCheckBoosterBannerState = () => {
  const { user } = useCurrentUser();
  const key = LOCAL_STORAGE_KEYS.boosterBanner(user?.id || '');
  const isShown = !!localStorage.getItem(key);
  const [isOpen, setIsOpen] = useState(!isShown);

  const changeVisibility = () => {
    setIsOpen(false);
    localStorage.setItem(key, 'true');
  };

  return { isOpen, changeVisibility };
};
