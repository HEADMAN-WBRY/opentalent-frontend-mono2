import { ConnectedPageLayout } from 'components/layout/page-layout';
import React from 'react';
import { useRef } from 'react';

import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import { Carousel } from '../../carousel-block/carousel';
import { CARDS_DATA } from '../../carousel-block/carousel/consts';
import { FAQ } from '../../faq';
import { AccordionProps } from '../../shared/AccordionItem';
import { PagesCard } from '../verified/PagesCard';
import { SmallPagesCard } from '../verified/SmallPagesCard';
import { BoosterBanner } from './booster-banner';
import { Intro } from './intro';
import { TextBlock1 } from './text-block-1';

interface TalentLobbyProps { }

const useStyles = makeStyles((theme) => ({
  contentWrapper: {
    margin: '0 auto',
    width: '100%',

    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(4, 4),
    },
  },

  textBlock: {
    maxWidth: 720,
    margin: '0 auto',
  },
}));

export const FAQ_ITEMS: Pick<AccordionProps, 'title' | 'text'>[] = [
  {
    title: 'Who is OpenTalent for?',
    text: 'OpenTalent is members-only platform and community for ‘high-skilled’ professionals across Europe to gain disproportionate advantages in building a career and/or business-of-one.',
  },
  {
    title: 'Why do I need to get verified?',
    text: 'Account verification is a crucial step that helps OpenTalent to maintain the quality and security of its platform and community. You can continue to use the platform while our team verifies your account.',
  },
  {
    title: 'How much does OpenTalent cost?',
    text: 'A membership to OpenTalent costs €4,99 per month. As a member, you get full access to the platform and its features as well as the community.',
  },
  {
    title: 'What happens if I do not pay?',
    text: 'You will not be able to access ‘member-only’ features, like the Job Board or the Community.',
  },
  {
    title: 'How do I cancel?',
    text: 'OpenTalent is flexible. You can cancel your membership anytime. There are no cancellation fees – start or stop your account whenever you want.',
  },
  {
    title: 'What happens after I activate my membership?',
    text: 'After activating your membership you will be able to access the platform and start using all its features as well as contact anyone in the community.',
  },
];

const PendingTalentLobby = (props: TalentLobbyProps) => {
  const classes = useStyles();
  const ref = useRef<HTMLDivElement>(null);
  // const scrollToTop = () => {
  //   ref?.current?.scrollIntoView({ behavior: 'smooth', block: 'start' });
  // };

  return (
    <ConnectedPageLayout
      classes={{ contentWrapper: classes.contentWrapper }}
      drawerProps={{}}
      documentTitle="Lobby"
    >
      <div ref={ref} />
      <BoosterBanner />

      <Box mb={10} mt={4}>
        <Intro />
      </Box>
      <Box mb={12}>
        <TextBlock1 />
      </Box>

      <Box className={classes.textBlock} pb={4}>
        <Typography variant="h6" textAlign="center" fontWeight={500} paragraph>
          And professional's welcome support…
        </Typography>

        <Typography textAlign="center" variant="body2">
          That's why we created OpenTalent: the members-only platform for
          Europe’s professionals that seeks to help you thrive. Expect job
          matches that truly resonate, connections with relevant new peers, and
          multiple avenues to boost income - all under one roof.
        </Typography>
      </Box>

      <Box mb={10}>
        <Carousel slides={CARDS_DATA} />
      </Box>

      {/* <Box mb={8}> */}
      {/*   <AboutMembership scrollToTop={scrollToTop} /> */}
      {/* </Box> */}

      <Box className={classes.textBlock} pb={10}>
        <Typography variant="h6" textAlign="center" paragraph fontWeight={500}>
          As a member, what do I get?
        </Typography>

        <Typography textAlign="center" variant="body2">
          As a member, you gain access to a vetted community and a powerful
          platform designed to provide tailored job matches, foster meaningful
          new connections, and unlock diverse income-boosting opportunities
          combined with continuous new feature drops.
        </Typography>
      </Box>

      <Box mb={10}>
        <SmallPagesCard />
      </Box>

      <Box className={classes.textBlock} pb={10}>
        <Typography variant="h6" textAlign="center" paragraph fontWeight={500}>
          Can I use OpenTalent for FREE?
        </Typography>

        <Typography textAlign="center" variant="body2">
          Yes, you can access select features for free, like the ‘Perks’ and
          ‘Invite & Earn’ programs. You can also reply to incoming messages from
          other community members.
        </Typography>
      </Box>

      <Box mb={2}>
        <PagesCard />
      </Box>

      <Box mb={2}>
        <FAQ items={FAQ_ITEMS} title="Frequently Asked Questions" />
      </Box>

      {/* <Box> */}
      {/*   <Rules /> */}
      {/* </Box> */}
    </ConnectedPageLayout>
  );
};

export default PendingTalentLobby;
