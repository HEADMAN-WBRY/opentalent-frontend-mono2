import { useIsPaidTalentAccount } from 'hooks/talents/useTalentAccountType';
import React from 'react';
import { Redirect } from 'react-router-dom';
import { pathManager } from 'routes';

import PendingTalentLobby from './pending/PendingTalentLobby';

interface TalentLobbyProps { }

const MainTalentLobby = (props: TalentLobbyProps) => {
  const isPaidTalent = useIsPaidTalentAccount();

  if (!isPaidTalent) {
    return <PendingTalentLobby />;
  }

  return <Redirect to={pathManager.talent.jobBoard.generatePath()} />;

  // return <VerifiedTalentLobby />;
};

export default MainTalentLobby;
