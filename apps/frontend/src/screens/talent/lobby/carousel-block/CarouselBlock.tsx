import React from 'react';

import { Box, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { Carousel } from './carousel/Carousel';
import { CARDS_DATA } from './carousel/consts';
import { CarouselItemData } from './carousel/types';

interface CarouselBlockProps {
  slides?: CarouselItemData[];
}

const useStyles = makeStyles((theme) => ({
  text: {
    maxWidth: 624,
    margin: '0 auto',
  },
}));

export const CarouselBlock = ({ slides = CARDS_DATA }: CarouselBlockProps) => {
  const classes = useStyles();

  return (
    <div>
      <Box mb={8}>
        <Typography textAlign="center" variant="h6">
          And professional's welcome support…
        </Typography>
      </Box>
      <Box className={classes.text} mb={8}>
        <Typography variant="body2" textAlign="center">
          That's why we created OpenTalent — to be the platform that seeks to
          help you out. We seek to bring our members job matches that truly
          resonate, connections with relevant new peers, and multiple avenues to
          boost income, all under one roof.
        </Typography>
      </Box>

      <Box>
        <Carousel slides={slides} />
      </Box>
    </div>
  );
};
