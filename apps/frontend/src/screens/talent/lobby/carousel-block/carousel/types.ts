export interface CarouselItemData {
  text: string;
  name: string;
  role: string;
  img: string;
}
