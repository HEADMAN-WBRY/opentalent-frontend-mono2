import cn from 'classnames';
import React, { useState } from 'react';
import { getPreviousIndex } from 'utils/common';

import ArrowBackRoundedIcon from '@mui/icons-material/ArrowBackRounded';
import ArrowForwardRoundedIcon from '@mui/icons-material/ArrowForwardRounded';
import { Grid, IconButton } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { getNextIndex } from '@libs/helpers/common';

import { CardItem } from './CardItem';
import { CarouselItemData } from './types';

interface CarouselProps {
  slides: CarouselItemData[];
}

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 900,
    margin: '0 auto',
  },

  arrowButton: {
    border: `1px solid ${theme.palette.text.secondary}`,

    [theme.breakpoints.down('sm')]: {
      height: 24,
      width: 24,
    },
  },

  wrapper: {
    padding: theme.spacing(14, 0, 8),

    '& > div': {
      display: 'flex',
      alignItems: 'center',
    },
  },

  stepper: {
    height: 32,

    '& i': {
      position: 'relative',
      display: 'block',
      border: `2px solid ${theme.palette.secondary.main}`,
      width: 12,
      height: 12,
      borderRadius: '100%',

      '&::before': {
        position: 'absolute',
        content: "''",
        background: theme.palette.secondary.main,
        top: '50%',
        left: '50%',
        width: 0,
        height: 0,
        display: 'block',
        transform: 'translate(-50%, -50%)',
        transition: 'all .3s',
        borderRadius: '100%',
      },

      '&$activeStep': {
        '&::before': {
          width: 12,
          height: 12,
        },
      },
    },
  },
  activeStep: {},
}));

export const Carousel = ({ slides }: CarouselProps) => {
  const classes = useStyles();
  const [activeIndex, setActiveIndex] = useState(0);
  const totalPages = slides.length / 2;
  const clickNext = () => setActiveIndex(getNextIndex(activeIndex, totalPages));
  const clickPrev = () =>
    setActiveIndex(getPreviousIndex(activeIndex, totalPages));
  const currentCardsToShow = slides.slice(activeIndex * 2, activeIndex * 2 + 2);

  return (
    <div className={classes.root}>
      <Grid
        container
        wrap="nowrap"
        className={classes.wrapper}
        spacing={4}
        justifyContent="center"
      >
        <Grid item>
          <IconButton
            onClick={clickPrev}
            size="large"
            className={classes.arrowButton}
          >
            <ArrowBackRoundedIcon />
          </IconButton>
        </Grid>
        <Grid item>
          <Grid container spacing={6} justifyContent="center">
            {currentCardsToShow.map((p) => (
              <Grid item key={p.name}>
                <CardItem {...p} />
              </Grid>
            ))}
          </Grid>
        </Grid>
        <Grid item>
          <IconButton
            size="large"
            className={classes.arrowButton}
            onClick={() => clickNext()}
          >
            <ArrowForwardRoundedIcon />
          </IconButton>
        </Grid>
      </Grid>

      <Grid
        className={classes.stepper}
        spacing={4}
        container
        justifyContent="center"
      >
        {Array.from({ length: totalPages }).map((_, index) => (
          <Grid key={index} item>
            <i
              className={cn({ [classes.activeStep]: index === activeIndex })}
            />
          </Grid>
        ))}
      </Grid>
    </div>
  );
};
