import adam1 from './assets/adam1@2x.png';
import adam from './assets/adam@2x.png';
import darko from './assets/darko@2x.png';
import mariusz from './assets/mariusz@2x.png';
import monica from './assets/monica@2x.png';
import nadia from './assets/nadia@2x.png';
import { CarouselItemData } from './types';

export const CARDS_DATA: CarouselItemData[] = [
  {
    text: 'Just two weeks after setting up my profile, I was contacted for a role that was a perfect fit for my skill set. I got the job soon after.',
    name: 'Adam',
    img: adam,
    role: 'Data Scientist',
  },
  {
    text: 'The job matches I get from OpenTalent are spot-on. Saves me hours in job searching.',
    name: 'Monica',
    img: monica,
    role: 'QA Tester',
  },
  {
    text: "OpenTalent's Talent Matcher program transformed my recruiting game. Two placements in one month, and the commissions are great!",
    name: 'Ádám',
    img: adam1,
    role: 'Recruiter',
  },
  {
    text: 'OpenTalent got me a great discount at a premium co-working space, which became a hub for valuable professional connections.',
    name: 'Nadia',
    img: nadia,
    role: 'Designer',
  },
  {
    text: "Found a stellar remote role in a top Dutch company thanks to OpenTalent. Couldn't be happier.",
    name: 'Mariusz',
    img: mariusz,
    role: 'Developer',
  },
  {
    text: "Working with OpenTalent led to an effortless, large-scale sourcing deal in Europe. It's been a rewarding partnership.",
    name: 'Darko',
    img: darko,
    role: 'Recruiter',
  },
];
