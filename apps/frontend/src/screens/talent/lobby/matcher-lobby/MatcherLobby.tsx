import StripePaymentButton from 'components/custom/talent/stripe-payment-button';
import { ConnectedPageLayout } from 'components/layout/page-layout';
import React from 'react';
import { pathManager } from 'routes';

import ArrowBackIosNewRoundedIcon from '@mui/icons-material/ArrowBackIosNewRounded';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { RouterButton } from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import CardsPointBlicks from '../accounts-block/CardsPointBlicks';
import {
  MATCHER_CARDS_ITEMS,
  MATCHER_CAROUSEL_ITEMS,
} from '../accounts-block/tabs-content/TalentMatcherTabContent';
import { Carousel } from '../carousel-block/carousel';
import { FAQ } from '../faq';
import { CommingSoon } from './comming-soon';
import { FAQ_ITEMS } from './consts';

interface TalentLobbyProps { }

const useStyles = makeStyles((theme) => ({
  contentWrapper: {
    margin: '0 auto',
    width: '100%',

    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(4, 4),
    },
  },
  content: {
    maxWidth: 800,
    margin: '0 auto',
  },
  info: {
    marginRight: theme.spacing(2),
    color: theme.palette.info.main,
  },
}));

export const MatcherLobby = (props: TalentLobbyProps) => {
  const classes = useStyles();

  return (
    <ConnectedPageLayout
      classes={{ contentWrapper: classes.contentWrapper }}
      drawerProps={{}}
      documentTitle="Lobby"
    >
      <Box>
        <RouterButton
          color="info"
          to={pathManager.talent.lobby.main.generatePath()}
          startIcon={<ArrowBackIosNewRoundedIcon />}
        >
          BACK
        </RouterButton>
      </Box>

      <Box className={classes.content}>
        <Box mx="auto" mb={6} maxWidth={670}>
          <Typography variant="h4" textAlign="center" paragraph>
            Talent Matcher Program
          </Typography>

          <Typography textAlign="center">
            You’ll join one of Europe’s largest recruiter networks and get to
            partner with our team to place candidates, and earn some extra
            money. The best Talent Matchers get invited to join our exclusive
            Talent Partner program.
          </Typography>
        </Box>

        <Box mb={6} width="100%" display="flex" justifyContent="center">
          <Box width={280}>
            <StripePaymentButton isTalentMatcher>
              JOIN THE PROGRAM
            </StripePaymentButton>
          </Box>
        </Box>

        <Box
          mt={4}
          mb={10}
          display="flex"
          alignItems="center"
          justifyContent="center"
        >
          <InfoOutlinedIcon className={classes.info} />{' '}
          <Typography variant="subtitle2">
            special 2023 prices - start or stop anytime
          </Typography>
        </Box>

        <Box mx="auto" mb={6} maxWidth={670}>
          <Typography variant="h5" textAlign="center" paragraph>
            “Turn Your Candidates Into Cash”
          </Typography>

          <Typography
            textAlign="center"
            variant="overline"
            component="div"
            color="info.main"
          >
            WHAT CAN YOU EXPECT?
          </Typography>
        </Box>

        <Box mb={18}>
          <Carousel slides={MATCHER_CAROUSEL_ITEMS} />
        </Box>

        <Box mt={8} mb={12}>
          <CardsPointBlicks items={MATCHER_CARDS_ITEMS} />
        </Box>

        <Box mb={12}>
          <CommingSoon />
        </Box>

        <Box mb={12} width="100%" display="flex" justifyContent="center">
          <Box width={280}>
            <StripePaymentButton isTalentMatcher>
              JOIN THE PROGRAM
            </StripePaymentButton>
          </Box>
        </Box>

        <Box mb={8}>
          <FAQ items={FAQ_ITEMS} />
        </Box>
      </Box>
    </ConnectedPageLayout>
  );
};

export default MatcherLobby;
