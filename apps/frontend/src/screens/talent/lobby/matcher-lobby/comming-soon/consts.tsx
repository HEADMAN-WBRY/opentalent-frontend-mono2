import Typography from '@libs/ui/components/typography';

import { AccordionProps } from '../../shared/AccordionItem';

export const SOON_ITEMS: Pick<AccordionProps, 'title' | 'text'>[] = [
  {
    title: 'Curated Job Search',
    text: (
      <Typography>
        We leverage the power of ChatGPT to find jobs that perfectly match your
        skills and needs.
      </Typography>
    ),
  },
  {
    title: 'Job Match Alerts',
    text: (
      <Typography>
        We leverage ChatGPT to match you with new top jobs and send you instant
        notifications.
      </Typography>
    ),
  },
  {
    title: 'Trusted Networking',
    text: (
      <Typography>
        Connect with a trusted community of ‘high-skilled’ talent across Europe.
      </Typography>
    ),
  },
  {
    title: 'Future Features',
    text: <Typography>Get access to all future platform features.</Typography>,
  },
];
