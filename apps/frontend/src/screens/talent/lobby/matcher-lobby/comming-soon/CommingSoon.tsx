import React, { useState } from 'react';
import { pathManager } from 'routes';

import AddRoundedIcon from '@mui/icons-material/AddRounded';
import { Box, Grid, Typography } from '@mui/material';

import { RouterLink } from '@libs/ui/components/typography';

import { AccordionItem } from '../../shared/AccordionItem';
import { SOON_ITEMS } from './consts';

interface CommingSoonProps { }

export const CommingSoon = (props: CommingSoonProps) => {
  const [activeIndex, setActiveIndex] = useState<number>();

  return (
    <div>
      <Grid container justifyContent="center">
        <Grid item style={{ fontSize: 40 }}>
          <AddRoundedIcon fontSize="inherit" />
        </Grid>
      </Grid>
      <Box mt={8} mb={8}>
        <Typography variant="h6" textAlign="center" paragraph>
          Full Membership
        </Typography>
        <Typography color="info.main" variant="subtitle1" textAlign="center">
          ALL BENEFITS OF THE FULL MEMBERSHIP
        </Typography>
      </Box>
      <Box maxWidth={600} style={{ margin: '0 auto' }}>
        {SOON_ITEMS.map((i, index) => (
          <AccordionItem
            {...i}
            isActive={index === activeIndex}
            onChange={() =>
              setActiveIndex(index === activeIndex ? undefined : index)
            }
          />
        ))}
      </Box>

      <Box mt={6}>
        <Typography textAlign="center">
          <RouterLink
            color="info.main"
            style={{ textDecoration: 'underline' }}
            to={pathManager.talent.lobby.member.generatePath()}
          >
            Learn more
          </RouterLink>{' '}
          about the OpenTalent full membership.
        </Typography>
      </Box>
    </div>
  );
};
