import React from 'react';

const CompanySettings = React.lazy(() => import('./TalentLobby'));

export default CompanySettings;
