import { Route, Switch } from 'react-router-dom';
import { pathManager } from 'routes';

import { TalentLobbyInfo } from './info-lobby';
import MainTalentLobby from './main-lobby/MainTalentLobby';
import { MatcherLobby } from './matcher-lobby';
import { MembershipTalentLobby } from './membership-lobby';

interface TalentLobbyProps { }

const TalentLobby = (props: TalentLobbyProps) => {
  return (
    <Switch>
      <Route
        exact
        path={pathManager.talent.lobby.main.getRoute()}
        component={MainTalentLobby}
      />
      <Route
        exact
        path={pathManager.talent.lobby.member.getRoute()}
        component={MembershipTalentLobby}
      />
      <Route
        exact
        path={pathManager.talent.lobby.matcher.getRoute()}
        component={MatcherLobby}
      />
      <Route
        exact
        path={pathManager.talent.lobby.info.getRoute()}
        component={TalentLobbyInfo}
      />
    </Switch>
  );
};

export default TalentLobby;
