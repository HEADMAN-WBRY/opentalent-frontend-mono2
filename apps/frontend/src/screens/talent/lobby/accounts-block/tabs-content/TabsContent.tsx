import React from 'react';

import { makeStyles } from '@mui/styles';

import { AccountTabsType } from '../types';
import { TalentMatcherTabContent } from './TalentMatcherTabContent';
import { TalentTabContent } from './TalentTabContent';
import { VisitorTabContent } from './VisitorTabContent';

interface TabsContentProps {
  activeTab: AccountTabsType;
}

const TAB_CONTENT_MAP = {
  [AccountTabsType.Visitor]: VisitorTabContent,
  [AccountTabsType.Talent]: TalentTabContent,
  [AccountTabsType.TalentMatcher]: TalentMatcherTabContent,
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
}));

export const TabsContent = ({ activeTab }: TabsContentProps) => {
  const classes = useStyles();
  const ContentComponent = TAB_CONTENT_MAP[activeTab];

  return (
    <div className={classes.root}>
      <ContentComponent />
    </div>
  );
};
