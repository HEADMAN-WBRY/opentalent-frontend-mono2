import React from 'react';

import { grey } from '@mui/material/colors';
import { makeStyles } from '@mui/styles';

interface TabContentLayoutProps extends React.PropsWithChildren<unknown> { }

const useStyles = makeStyles((theme) => ({
  root: {
    background: '#F6F6F6',
    border: `1px solid ${grey[500]}`,
    borderTop: 'none',
    padding: theme.spacing(8, 6),
    borderBottomLeftRadius: theme.spacing(8),
    borderBottomRightRadius: theme.spacing(8),

    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(4, 4),
    },
  },
}));

export const TabContentLayout = ({ children }: TabContentLayoutProps) => {
  const classes = useStyles();

  return <div className={classes.root}>{children}</div>;
};
