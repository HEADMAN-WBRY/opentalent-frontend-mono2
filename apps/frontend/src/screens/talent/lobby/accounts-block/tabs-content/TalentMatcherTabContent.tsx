import StripePaymentButton from 'components/custom/talent/stripe-payment-button';
import React from 'react';

import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import { Box, Grid, Hidden } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import { Carousel } from '../../carousel-block/carousel';
import { CarouselItemData } from '../../carousel-block/carousel/types';
import CardsPointBlicks, { ItemType } from '../CardsPointBlicks';
import { TabContentLayout } from './TabContentLayout';
import adam1 from './assets/avatar/adam1@2x.png';
import aleksandra from './assets/avatar/aleksandra@2x.png';
import darko from './assets/avatar/darko@2x.png';
import mariel from './assets/avatar/mariel@2x.png';
import imgItem1 from './assets/matcher-1@2x.png';
import imgItem2 from './assets/matcher-2@2x.png';
import imgItem3 from './assets/matcher-3@2x.png';

interface TalentMatcherTabContentProps { }

export const MATCHER_CARDS_ITEMS: Array<ItemType> = [
  {
    id: 1,
    title: 'INVITE & EARN',
    subtitle: `Get Paid To Recruit`,
    text: 'Book your seat to source for OpenTalent jobs and work  directly with our team to close jobs faster.',
    points: [
      'Earn up to €5.000 per successful placement',
      'Invite up to 50 candidates per month',
      'Limited Talent Matcher spots per job',
    ],
    imgComponent: <img srcSet={`${imgItem1} 2x`} alt="matches" />,
    revert: false,
  },
  {
    id: 2,
    title: `COMMUNITY`,
    subtitle: `Advance Talent Search`,
    text: `Use OpenTalent to find candidates for any job. Benefit from advanced AI-powered search. `,
    points: [
      'Search 27.500+ vetted candidates',
      'Advanced Filters incl. 3K skills',
    ],
    imgComponent: <img srcSet={`${imgItem2} 2x`} alt="matches" />,
    revert: true,
  },
  {
    id: 3,
    title: `COMMUNITY`,
    subtitle: `Become Talent Partner`,
    text: `Evolve into a Talent Partner: our premium partnership for independent recruiters.`,
    points: [
      'Supercharge your recruiting business',
      'Get leads, tooling and team work',
    ],
    imgComponent: <img srcSet={`${imgItem3} 2x`} alt="matches" />,
    revert: false,
  },
];

export const MATCHER_CAROUSEL_ITEMS: CarouselItemData[] = [
  {
    text: "OpenTalent's Talent Matcher program transformed my recruiting game. Two placements in one month, and the commissions are great!",
    name: 'Ádám',
    img: adam1,
    role: 'Recruiter',
  },
  {
    text: "Working with OpenTalent led to an effortless, large-scale sourcing deal in Europe. It's been a rewarding partnership.",
    name: 'Darko',
    img: darko,
    role: 'Recruiter',
  },
  {
    text: 'I wish to express my deep appreciation for the innovative Open Talent platform, which has significantly enriched my recruitment endeavors over the past several months. It has emerged as an invaluable resource for identifying and engaging top-tier talent',
    name: 'Mariel',
    img: mariel,
    role: 'Recruiter',
  },
  {
    text: 'Flexible working hours are my top priority - they always have and will be. This is what Open Talent provides, as I work as much as I want, whenever and wherever I want. Thanks to this precious possibility and openness I am able to deliver my best performance.',
    name: 'Aleksandra',
    img: aleksandra,
    role: 'Recruiter',
  },
];

const useStyles = makeStyles((theme) => ({
  infoIcon: {
    color: theme.palette.info.main,
    transform: 'translateY(5px)',
  },
}));

export const TalentMatcherTabContent = (
  props: TalentMatcherTabContentProps,
) => {
  const classes = useStyles();

  return (
    <TabContentLayout>
      <Hidden smUp>
        <Box mb={6} mt={2}>
          <Typography variant="h6" textAlign="center" paragraph>
            Talent Matcher
          </Typography>
          <Typography variant="h6" color="info.main" textAlign="center">
            €9,99
          </Typography>
        </Box>
      </Hidden>

      <Typography variant="subtitle1" textAlign="center" color="text.secondary">
        Apply to the Talent Matcher program
      </Typography>
      <Typography variant="h5" textAlign="center" color="info.main">
        “Turn Your Candidates Into Cash”
      </Typography>

      <Box mt={4}>
        <Carousel slides={MATCHER_CAROUSEL_ITEMS} />
      </Box>

      <Box mt={6} mb={2}>
        <Grid container justifyContent="center">
          <Grid item>
            <StripePaymentButton isTalentMatcher>
              Apply to the program
            </StripePaymentButton>
          </Grid>
        </Grid>
      </Box>

      <Box>
        <Grid container justifyContent="center" alignItems="center" spacing={2}>
          <Grid item>
            <InfoOutlinedIcon className={classes.infoIcon} />
          </Grid>
          <Grid item>
            <Typography variant="caption" component="span">
              Designed for recruiters across Europe
            </Typography>
          </Grid>
        </Grid>
      </Box>

      <Box maxWidth={628} style={{ margin: '40px auto 0' }}>
        <Typography variant="h6" textAlign="center" paragraph>
          How It Works?
        </Typography>
        <Typography variant="body2" textAlign="center">
          You’ll join one of Europe’s largest recruiter networks and get to
          partner with our team to place candidates, and earn some extra money.
          The best Talent Matchers get invited to join our exclusive Talent
          Partner program.
        </Typography>
      </Box>

      <Box mt={8}>
        <CardsPointBlicks items={MATCHER_CARDS_ITEMS} />
      </Box>
    </TabContentLayout>
  );
};
