import React from 'react';
import { pathManager } from 'routes';

import { Box } from '@mui/material';

import { RouterButton } from '@libs/ui/components/button';

import { CommingSoon } from '../../comming-soon';
import CardsPointBlicks, { ItemType } from '../CardsPointBlicks';
import { TabContentLayout } from './TabContentLayout';
import imgItem1 from './assets/visitor-1@2x.png';
import imgItem2 from './assets/visitor-2@2x.png';
import imgItem3 from './assets/visitor-3@2x.png';
import imgItem4 from './assets/visitor-4@2x.png';
import imgItem5 from './assets/visitor-5@2x.png';

interface VisitorTabContentProps { }

export const CARDS_ITEMS: Array<ItemType> = [
  {
    id: 1,
    title: 'JOB BOARD',
    subtitle: `Search jobs`,
    text: 'We bring together one of Europe’s most curated ‘high-skilled’ job boards. Search jobs, connect with recruiters, and get hired for top jobs.',
    points: ['Thousands of jobs across Europe', 'New jobs added daily'],
    imgComponent: <img srcSet={`${imgItem1} 2x`} alt="matches" />,
    revert: true,
    additionalContent: (
      <RouterButton
        color="info"
        to={pathManager.talent.jobBoard.generatePath()}
        variant="contained"
      >
        VISIT NOW
      </RouterButton>
    ),
  },
  {
    id: 2,
    title: `JOB BOARD`,
    subtitle: `Job Alerts`,
    text: `Get notified of job openings that match what you want. We use ChatGPT/AI to match you with top jobs, so you never miss a dream job again.`,
    points: [
      'Be the first to know when jobs open',
      'Configure when to receive alerts  ',
    ],
    imgComponent: <img srcSet={`${imgItem2} 2x`} alt="matches" />,
    revert: false,
    additionalContent: (
      <RouterButton
        color="info"
        to={`${pathManager.talent.settings.main.generatePath()}?tab=notifications`}
        variant="outlined"
      >
        Set your preferences
      </RouterButton>
    ),
  },
  {
    id: 3,
    title: `INVITE & EARN`,
    subtitle: `Refer & Earn`,
    text: `Know people? Refer candidates to open jobs and we’ll pay you when they get hired.`,
    points: [
      'Earn €1.000 for every hire on OpenTalent',
      'Refer up to 5 candidates',
    ],
    imgComponent: <img srcSet={`${imgItem3} 2x`} alt="matches" />,
    revert: true,
    additionalContent: (
      <RouterButton
        color="info"
        to={pathManager.talent.invite.generatePath()}
        variant="contained"
      >
        visit now
      </RouterButton>
    ),
  },
  {
    id: 4,
    title: `COMMUNITY`,
    subtitle: `Network With Alumni`,
    text: `Connect with people at your favourite companies and start building network.`,
    points: [
      '27.500+ vetted member profiles',
      '35.000+ registered employers',
      'Chat with anyone / request an intro',
    ],
    imgComponent: <img srcSet={`${imgItem4} 2x`} alt="matches" />,
    revert: false,
    additionalContent: (
      <RouterButton
        color="info"
        to={pathManager.talent.community.generatePath()}
        variant="contained"
      >
        visit now
      </RouterButton>
    ),
  },
  {
    id: 5,
    title: `PERKS`,
    subtitle: `Perks & Benefits`,
    text: `Save €3.000+ through our perks program; co-working, car rentals, and more benefits.`,
    points: ['Get discounts up 50%', 'New perks added every week'],
    imgComponent: <img srcSet={`${imgItem5} 2x`} alt="matches" />,
    revert: true,
    additionalContent: (
      <RouterButton
        color="info"
        to={pathManager.perks.generatePath()}
        variant="contained"
      >
        visit now
      </RouterButton>
    ),
  },
];

export const VisitorTabContent = (props: VisitorTabContentProps) => {
  return (
    <TabContentLayout>
      <Box mt={4}>
        <CardsPointBlicks items={CARDS_ITEMS} />
      </Box>

      <Box mb={4} mt={10}>
        <CommingSoon />
      </Box>
    </TabContentLayout>
  );
};
