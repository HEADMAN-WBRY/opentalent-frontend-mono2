import StripePaymentButton from 'components/custom/talent/stripe-payment-button';
import React from 'react';

import { Box, Grid, Hidden } from '@mui/material';

import Typography from '@libs/ui/components/typography';

import { Carousel } from '../../carousel-block/carousel';
import { CarouselItemData } from '../../carousel-block/carousel/types';
import { CommingSoon } from '../../comming-soon';
import CardsPointBlicks, { ItemType } from '../CardsPointBlicks';
import { TabContentLayout } from './TabContentLayout';
import adam from './assets/avatar/adam@2x.png';
import mariusz from './assets/avatar/mariusz@2x.png';
import monica from './assets/avatar/monica@2x.png';
import nadia from './assets/avatar/nadia@2x.png';
import imgItem1 from './assets/talent-1@2x.png';
import imgItem2 from './assets/talent-2@2x.png';
import imgItem3 from './assets/talent-3@2x.png';

interface TalentTabContentProps { }

export const TALENT_CARDS_ITEMS: Array<ItemType> = [
  {
    id: 1,
    title: 'Job BOARD',
    subtitle: `Job Matching`,
    text: 'See the best matching jobs from thousands of curated ‘high-skilled’ jobs across Europe.',
    points: [
      <>10.000+ jobs from top job boards</>,
      'Advanced ChatGPT-powered matching',
      'New jobs added daily',
    ],
    imgComponent: <img srcSet={`${imgItem1} 2x`} alt="matches" />,
    revert: true,
  },
  {
    id: 2,
    title: `Job BOARD`,
    subtitle: `Job Alerts`,
    text: `Be the first to know when your dream job opens. Get real-time notifications and alerts.`,
    points: [
      'Instant email alerts for new job matches',
      'ChatGPT-powered job matching',
      'Learn why jobs are a match',
    ],
    imgComponent: <img srcSet={`${imgItem2} 2x`} alt="matches" />,
    revert: false,
  },
  {
    id: 3,
    title: `COmmunity`,
    subtitle: `Elite networking`,
    text: `Connect with one of Europe’s most trusted networks of ‘high-skilled’ professionals.`,
    points: [
      <>29.000+ verified profiles</>,
      <>Alumni at 35.000+ companies</>,
      'Send messages / request intros',
    ],
    imgComponent: <img srcSet={`${imgItem3} 2x`} alt="matches" />,
    revert: true,
  },
];

export const TALENT_CAROUSEL_ITEMS: CarouselItemData[] = [
  {
    text: 'Just two weeks after setting up my profile, I was contacted for a role that was a perfect fit for my skill set. I got the job soon after.',
    name: 'Adam',
    img: adam,
    role: 'Data Scientist',
  },
  {
    text: 'The job matches I get from OpenTalent are spot-on. Saves me hours in job searching.',
    name: 'Monica',
    img: monica,
    role: 'QA Tester',
  },
  {
    text: 'OpenTalent got me a great discount at a premium co-working space, which became a hub for valuable professional connections.',
    name: 'Nadia',
    img: nadia,
    role: 'Designer',
  },
  {
    text: "Found a stellar remote role in a top Dutch company thanks to OpenTalent. Couldn't be happier.",
    name: 'Mariusz',
    img: mariusz,
    role: 'Developer',
  },
];

export const TalentTabContent = (props: TalentTabContentProps) => {
  return (
    <TabContentLayout>
      <Hidden smUp>
        <Box mb={6} mt={2}>
          <Typography variant="h6" textAlign="center" paragraph>
            Member
          </Typography>
          <Typography variant="h6" color="info.main" textAlign="center">
            €4,99
          </Typography>
        </Box>
      </Hidden>

      <Typography variant="subtitle1" textAlign="center" color="text.secondary">
        Upgrade to Member
      </Typography>
      <Typography variant="h5" textAlign="center" color="info.main">
        “Fast-Track Your Path To A Dream Job”
      </Typography>

      <Box mb={12} mt={6}>
        <Grid container justifyContent="center">
          <Grid item>
            <StripePaymentButton>start your membership</StripePaymentButton>
          </Grid>
        </Grid>
      </Box>

      <Box maxWidth={628} style={{ margin: '40px auto 0' }}>
        <Typography variant="h6" textAlign="center" paragraph>
          What Can You Expect?
        </Typography>
        <Typography variant="body2" textAlign="center">
          As a ‘Member’, you get full access to the OpenTalent platform and
          community. Join Europe’s #1 talent-centric platform and community
          today - and we’ll help you in discovering your dream job, and much
          more.
        </Typography>
      </Box>

      <Box mt={8}>
        <CardsPointBlicks items={TALENT_CARDS_ITEMS} />
      </Box>

      <Box mt={16}>
        <Box>
          <Typography variant="h6" textAlign="center">
            What Members have to say:
          </Typography>
        </Box>
        <Carousel slides={TALENT_CAROUSEL_ITEMS} />
      </Box>

      <Box my={6}>
        <Grid container justifyContent="center">
          <Grid item>
            <StripePaymentButton>start your membership</StripePaymentButton>
          </Grid>
        </Grid>
      </Box>

      <Box mt={7}>
        <CommingSoon />
      </Box>
    </TabContentLayout>
  );
};
