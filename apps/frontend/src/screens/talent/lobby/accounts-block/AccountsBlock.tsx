import React, { useState } from 'react';

import { Box, Chip, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';
import InfoLinkContainer from '@libs/ui/components/typography/InfoLinkContainer';

import { TabControls } from './TabControls';
import { TabsContent } from './tabs-content';
import { AccountTabsType } from './types';

interface AccountsBlockProps { }

const useStyles = makeStyles(() => ({
  root: {
    maxWidth: 870,
    margin: '0 auto',
  },
}));

export const AccountsBlock = (props: AccountsBlockProps) => {
  const classes = useStyles();
  const [activeTab, setActiveTab] = useState(AccountTabsType.Visitor);

  return (
    <div className={classes.root}>
      <Box mb={8}>
        <Typography variant="h4" textAlign="center">
          So what is next?
        </Typography>
      </Box>
      <Box mb={7}>
        <Typography variant="body2" textAlign="center">
          After being verified, your profile gets added to our community and you
          can start to apply for jobs. You can also reach out to members of
          OpenTalent, and they can reach out to you. You can also invite people
          for a chance to earn referral fees, and you can enjoy our perks
          program.
        </Typography>
      </Box>

      <Box mb={4}>
        <Grid container justifyContent="center">
          <Grid item>
            <Chip
              label="Your account is pending verification"
              color="success"
              variant="outlined"
            />
          </Grid>
        </Grid>
      </Box>

      <Box mb={14}>
        <Grid container justifyContent="center">
          <Grid item>
            <InfoLinkContainer>
              <Typography variant="body2" textAlign="center">
                Verification can take up to 5 days
              </Typography>
            </InfoLinkContainer>
          </Grid>
        </Grid>
      </Box>

      <Box pt={4}>
        <TabControls onChange={setActiveTab} activeTab={activeTab} />
        <TabsContent activeTab={activeTab} />
      </Box>
    </div>
  );
};
