import cn from 'classnames';
import React from 'react';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { useMediaQueries } from '@libs/helpers/hooks/media-queries';
import Typography from '@libs/ui/components/typography';

export interface ItemType {
  id: number;
  hideIndex?: boolean;
  title: string;
  subtitle: string;
  text: string;
  points: Array<React.ReactNode>;
  imgComponent: JSX.Element;
  revert: boolean;
  additionalContent?: React.ReactNode;
}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    maxWidth: '810px',
    margin: '0 auto',
  },
  container: {
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
      justifyContent: 'center',
    },

    '&:not(:last-child)': {
      marginBottom: theme.spacing(10),
    },

    [theme.breakpoints.down('md')]: {
      '&:not(:last-child)': {
        marginBottom: theme.spacing(10),
      },
    },
  },
  miniTitle: {
    textTransform: 'uppercase',
    fontSize: '12px',
    lineHeight: '20px',
    marginBottom: theme.spacing(2),
    color: theme.palette.info.main,
  },
  title: {
    marginBottom: theme.spacing(2),
    whiteSpace: 'pre-line',

    [theme.breakpoints.down('sm')]: {
      fontSize: '20px',
      lineHeight: '28px',
    },
  },
  text: {
    lineHeight: '28px',
    maxWidth: '370px',
  },
  ul: {
    listStyleType: 'square',
    paddingLeft: theme.spacing(5),

    '& > li::marker': {
      color: theme.palette.info.main,
    },
  },
  point: {
    marginBottom: theme.spacing(1),
  },
  slideWrapper: {
    display: 'flex',
    alignItems: 'center',

    '&:nth-child(2)': {
      justifyContent: 'flex-end',
    },
  },
  textSide: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
  },
  img: {
    display: 'flex',
    alignItems: 'center',

    '& > img': {
      maxWidth: '100%',
    },

    [theme.breakpoints.down('md')]: {
      justifyContent: 'center',
      marginBottom: theme.spacing(10),
    },
  },
}));

const WhatToExpect: React.FC<{ items: ItemType[] }> = ({ items }) => {
  const classes = useStyles();
  const { isSM } = useMediaQueries();

  const renderTextSide = (item: ItemType) => (
    <Box className={classes.textSide}>
      <Box>
        <Typography className={classes.miniTitle} variant="subtitle2">
          {item.title}
        </Typography>
        <Typography className={classes.title} variant="h6" fontWeight={500}>
          {item.hideIndex ? '' : `${item.id}. `}
          {item.subtitle}
        </Typography>
        <Typography className={classes.text} variant="body2">
          {item.text}
        </Typography>
        <ul className={classes.ul}>
          {item.points.map((point, idx) => {
            const key = item.title + idx;

            return (
              <li key={key}>
                <Typography className={classes.point} variant="body2">
                  {point}
                </Typography>
              </li>
            );
          })}
        </ul>
        {!!item.additionalContent && <div>{item.additionalContent}</div>}
      </Box>
    </Box>
  );

  return (
    <Box className={classes.wrapper}>
      <div>
        {items.map((item) => {
          const revertFinal = item.revert && !isSM;

          return (
            <Grid
              container
              key={item.id}
              className={classes.container}
              direction={isSM && revertFinal ? 'column-reverse' : 'row'}
              spacing={4}
            >
              <Grid
                item
                xs={isSM ? 12 : 6}
                className={cn(classes.slideWrapper, {
                  [classes.img]: revertFinal,
                })}
              >
                {revertFinal ? item.imgComponent : renderTextSide(item)}
              </Grid>
              <Grid
                item
                xs={isSM ? 12 : 6}
                className={cn(classes.slideWrapper, {
                  [classes.img]: !revertFinal,
                })}
              >
                {revertFinal ? renderTextSide(item) : item.imgComponent}
              </Grid>
            </Grid>
          );
        })}
      </div>
    </Box>
  );
};

export default WhatToExpect;
