export enum AccountTabsType {
  Visitor = 'Visitor',
  Talent = 'Talent',
  TalentMatcher = 'TalentMatcher',
}
