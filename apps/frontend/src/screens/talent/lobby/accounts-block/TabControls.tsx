import cn from 'classnames';
import React from 'react';

import { Box, Grid } from '@mui/material';
import { grey } from '@mui/material/colors';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import { AccountTabsType } from './types';

interface TabControlsProps {
  activeTab: AccountTabsType;
  onChange: (e: AccountTabsType) => void;
}

interface TabItemData {
  title: string;
  content: React.ReactNode;
  mark?: string;
  key: AccountTabsType;
}

const TAB_ITEMS: TabItemData[] = [
  {
    key: AccountTabsType.Visitor,
    title: 'Visitor',
    content: (
      <Box>
        <Typography variant="h5" color="info.main" textAlign="center">
          FREE
        </Typography>
      </Box>
    ),
  },
  // {
  //   key: AccountTabsType.Talent,
  //   title: 'Member',
  //   mark: 'popular',
  //   content: (
  //     <Box>
  //       <Typography textAlign="center" variant="body2" color="text.secondary">
  //         Now{' '}
  //         <Typography variant="h5" color="info.main" component="span">
  //           €4,99
  //         </Typography>{' '}
  //         p/m
  //       </Typography>
  //       <Typography
  //         variant="caption"
  //         textAlign="center"
  //         component="p"
  //         color="text.secondary"
  //       >
  //         (special 2023 pricing)
  //       </Typography>
  //     </Box>
  //   ),
  // },
  {
    key: AccountTabsType.TalentMatcher,
    title: 'Talent Matcher',
    mark: 'For recruiters',
    content: (
      <Box>
        <Typography textAlign="center" variant="body2" color="text.secondary">
          Now{' '}
          <Typography variant="h5" color="info.main" component="span">
            €9,99
          </Typography>{' '}
          p/m
        </Typography>
      </Box>
    ),
  },
];

const useStyles = makeStyles((theme) => ({
  root: {},
  title: {
    [theme.breakpoints.down('sm')]: {
      fontSize: 14,
      lineHeight: '18px',
    },
  },

  tab: {
    padding: theme.spacing(4),
    borderTopLeftRadius: theme.spacing(4),
    borderTopRightRadius: theme.spacing(4),
    background: '#F3F3F3',
    border: `1px solid ${grey[500]}`,
    cursor: 'pointer',
    transition: 'background .3s',
    position: 'relative',
    overflow: 'hidden',
  },

  tabActive: {
    background: '#F6F6F6',
    borderBottomColor: '#F6F6F6',
  },
  content: {
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  mark: {
    position: 'absolute',
    transform: 'rotate(45deg)',
    color: 'white',
    background: theme.palette.info.main,
    top: 26,
    right: -26,
    padding: theme.spacing(0, 4),

    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
}));

export const TabControls = ({ activeTab, onChange }: TabControlsProps) => {
  const classes = useStyles();

  return (
    <Grid className={classes.root} container>
      {TAB_ITEMS.map((item) => (
        <Grid
          item
          xs={6}
          onClick={() => onChange(item.key)}
          className={cn(classes.tab, {
            [classes.tabActive]: activeTab === item.key,
          })}
        >
          <Typography className={classes.title} variant="h6" textAlign="center">
            {item.title}
          </Typography>
          <Box mt={2} className={classes.content}>
            {item.content}
          </Box>

          {!!item.mark && (
            <Typography
              variant="caption"
              textAlign="center"
              component="div"
              className={classes.mark}
            >
              {item.mark}
            </Typography>
          )}
        </Grid>
      ))}
    </Grid>
  );
};
