import { Grid } from '@mui/material';

import Typography from '@libs/ui/components/typography';

import { AccordionProps } from '../shared/AccordionItem';
import gptIcon from './gpt@2x.png';

export const SOON_ITEMS: Pick<
  AccordionProps,
  'title' | 'text' | 'sideContent'
>[] = [
    {
      title: 'Opportunity Board 🌟',
      text: (
        <Typography>
          <Typography fontWeight={600} component="span">
            Are you on the lookout for new opportunities?
          </Typography>{' '}
          Our Opportunity Board operates like an open 'community forum' where
          anyone can share interesting opportunities with the community.
        </Typography>
      ),
    },
    {
      title: 'Project Collaboration Board 🤝',
      text: (
        <Typography>
          <Typography fontWeight={600} component="span">
            Need help?
          </Typography>{' '}
          Post what you're working on for other members to see and offer their
          help—voluntarily or paid, depending on your project.
        </Typography>
      ),
    },
    {
      title: 'Job Match Calculator 📟',
      sideContent: (
        <Grid container spacing={2} style={{ marginBottom: -6 }}>
          <Grid item>
            <Typography variant="body2" color="text.secondary">
              Powered by
            </Typography>
          </Grid>
          <Grid item>
            <img srcSet={`${gptIcon} 2x`} alt="gpt" />
          </Grid>
        </Grid>
      ),
      text: (
        <Typography>
          <Typography fontWeight={600} component="span">
            Want to know if a job suits you?
          </Typography>{' '}
          Copy and paste the URL of any job post to get an instant recommendation
          on its match with your profile. If there's a fit, we can also draft a
          cover letter for you.
        </Typography>
      ),
    },
    {
      title: 'Introduce Me 👋',
      text: (
        <Typography>
          <Typography fontWeight={600} component="span">
            Looking to connect with someone new?
          </Typography>{' '}
          Allow us to introduce you to any of our members. We'll identify the
          synergies between you two and facilitate a warm and substantial
          introduction.
        </Typography>
      ),
    },
    {
      title: 'Book Me 📅',
      text: (
        <Typography>
          <Typography fontWeight={600} component="span">
            Selling a service or course?
          </Typography>{' '}
          Promote it on OpenTalent and enable other members to book you directly.
        </Typography>
      ),
    },
    {
      title: 'Mentorship program 🙋🏽',
      text: (
        <Typography>
          <Typography fontWeight={600} component="span">
            Eager to help others thrive?
          </Typography>{' '}
          Join our mentorship program and we will begin matching you with
          individuals eager to learn from you and develop their skills.
        </Typography>
      ),
    },
    {
      title: 'Integrations 🔗',
      text: (
        <Typography>
          <Typography fontWeight={600} component="span">
            Looking to integrate with OpenTalent?
          </Typography>{' '}
          Soon we'll be adding functionalities to sync with ATS, HR & Pay rolling
          systems, and your favourite calendar scheduling and video conferencing
          apps.
        </Typography>
      ),
    },
    {
      title: 'Events 🎪 (virtual, skill-based events…)',
      text: (
        <Typography>
          <Typography fontWeight={600} component="span">
            Ready for real-life meetups?
          </Typography>{' '}
          Stay tuned! We are in the process of planning our inaugural events for
          2024.
        </Typography>
      ),
    },
  ];
