import React, { useState } from 'react';

import AddRoundedIcon from '@mui/icons-material/AddRounded';
import { Box, Grid, Typography } from '@mui/material';

import { EXTERNAL_LINKS } from '@libs/helpers/consts';
import { OuterLink } from '@libs/ui/components/typography';

import { AccordionItem } from '../shared/AccordionItem';
import { SOON_ITEMS } from './consts';

interface CommingSoonProps { }

export const CommingSoon = (props: CommingSoonProps) => {
  const [activeIndex, setActiveIndex] = useState<number>();

  return (
    <div>
      <Grid container justifyContent="center">
        <Grid item style={{ fontSize: 40 }}>
          <AddRoundedIcon fontSize="inherit" />
        </Grid>
      </Grid>
      <Box mt={8} mb={8}>
        <Typography variant="h6" textAlign="center" paragraph>
          Coming Soon!
        </Typography>
        <Typography color="info.main" variant="subtitle1" textAlign="center">
          NEW FEATURE DROPS
        </Typography>
      </Box>
      <Box maxWidth={600} style={{ margin: '0 auto' }}>
        {SOON_ITEMS.map((i, index) => (
          <AccordionItem
            {...i}
            isActive={index === activeIndex}
            onChange={() =>
              setActiveIndex(index === activeIndex ? undefined : index)
            }
          />
        ))}
      </Box>

      <Box mt={6}>
        <Typography textAlign="center">
          Visit{' '}
          <OuterLink
            color="info.main"
            href={EXTERNAL_LINKS.whatsNext}
            target="_blank"
          >
            ‘What’s Next?’
          </OuterLink>{' '}
          to see what we’re working on right now
        </Typography>
      </Box>
    </div>
  );
};
