import StripePaymentButton from 'components/custom/talent/stripe-payment-button';
import { ConnectedPageLayout } from 'components/layout/page-layout';
import React from 'react';
import { pathManager } from 'routes';

import ArrowBackIosNewRoundedIcon from '@mui/icons-material/ArrowBackIosNewRounded';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { RouterButton } from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import CardsPointBlicks from '../accounts-block/CardsPointBlicks';
import {
  TALENT_CARDS_ITEMS,
  TALENT_CAROUSEL_ITEMS,
} from '../accounts-block/tabs-content/TalentTabContent';
import { Carousel } from '../carousel-block/carousel';
import { CommingSoon } from '../comming-soon';
import { FAQ } from '../faq';
import { SOON_ITEMS } from './consts';

interface TalentLobbyProps { }

const useStyles = makeStyles((theme) => ({
  contentWrapper: {
    margin: '0 auto',
    width: '100%',

    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(4, 4),
    },
  },
  content: {
    maxWidth: 800,
    margin: '0 auto',
  },
  info: {
    marginRight: theme.spacing(2),
    color: theme.palette.info.main,
  },
}));

export const MembershipTalentLobby = (props: TalentLobbyProps) => {
  const classes = useStyles();

  return (
    <ConnectedPageLayout
      classes={{ contentWrapper: classes.contentWrapper }}
      drawerProps={{}}
      documentTitle="Lobby"
    >
      <Box>
        <RouterButton
          color="info"
          to={pathManager.talent.lobby.main.generatePath()}
          startIcon={<ArrowBackIosNewRoundedIcon />}
        >
          BACK
        </RouterButton>
      </Box>

      <Box className={classes.content}>
        <Box mx="auto" mb={6} maxWidth={670}>
          <Typography variant="h4" textAlign="center" paragraph>
            Full Membership
          </Typography>

          <Typography textAlign="center">
            OpenTalent’s full membership is designed to help professionals find
            jobs that truly align with what you are looking for. We leverage the
            power of ChatGPT and AI to search thousands of open jobs and match
            them with your needs and skills. Full membership also grants you
            access to our trusted pan-European community of professionals - all
            under one roof.
          </Typography>
        </Box>

        <Box mb={6} width="100%" display="flex" justifyContent="center">
          <Box width={280}>
            <StripePaymentButton>START FULL MEMBERSHIP</StripePaymentButton>
          </Box>
        </Box>

        <Box
          mt={4}
          mb={10}
          display="flex"
          alignItems="center"
          justifyContent="center"
        >
          <InfoOutlinedIcon className={classes.info} />{' '}
          <Typography variant="subtitle2">
            Start or stop your membership anytime.
          </Typography>
        </Box>

        <Box mx="auto" mb={6} maxWidth={670}>
          <Typography variant="h5" textAlign="center" paragraph>
            “Fast-Track You Path To A Dream Job”
          </Typography>

          <Typography
            textAlign="center"
            variant="overline"
            component="div"
            color="info.main"
          >
            WHAT CAN YOU EXPECT?
          </Typography>
        </Box>

        <Box mb={18}>
          <Carousel slides={TALENT_CAROUSEL_ITEMS} />
        </Box>

        <Box mt={8} mb={12}>
          <CardsPointBlicks items={TALENT_CARDS_ITEMS} />
        </Box>

        <Box mb={12}>
          <CommingSoon />
        </Box>

        <Box mb={12} width="100%" display="flex" justifyContent="center">
          <Box width={280}>
            <StripePaymentButton>START FULL MEMBERSHIP</StripePaymentButton>
          </Box>
        </Box>

        <Box mb={8}>
          <FAQ items={SOON_ITEMS} />
        </Box>
      </Box>
    </ConnectedPageLayout>
  );
};

export default MembershipTalentLobby;
