import React from 'react';

const MarkedJobs = React.lazy(() => import('./MarkedJobs'));

export default MarkedJobs;
