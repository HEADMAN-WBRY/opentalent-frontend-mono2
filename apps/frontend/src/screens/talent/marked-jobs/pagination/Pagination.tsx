import { Box } from '@mui/material';
import { usePushWithQuery } from 'hooks/routing';
import React from 'react';

import Pagination, {
  PaginationProps,
} from '@libs/ui/components/pagination/Pagination';

interface JobsPaginationProps {
  currentPage: number;
  lastPage: number;
}

const JobsPagination = ({ currentPage, lastPage }: JobsPaginationProps) => {
  const push = usePushWithQuery();
  const onPaginationChange: PaginationProps['onChange'] = (_, page) =>
    push({ query: { page } });

  return (
    <Box display="flex" justifyContent="center" mt={4}>
      <Pagination
        page={currentPage}
        showFirstButton
        showLastButton
        count={lastPage}
        variant="outlined"
        shape="rounded"
        onChange={onPaginationChange}
      />
    </Box>
  );
};

export default JobsPagination;
