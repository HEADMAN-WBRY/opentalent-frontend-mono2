import { ConnectedPageLayout } from 'components/layout/page-layout';
import React from 'react';
import { JobMarkType } from 'types';

import { Box } from '@mui/material';

import { Job } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import { PAGE_DATA } from './consts';
import Filter from './filter';
import { useScreenData } from './hooks';
import JobList from './job-list';
import JobsPagination from './pagination';
import { getJobsQuery } from './queries';
import { PageProps } from './types';

const MarkedJobs = ({ match }: PageProps) => {
  const markType = match.params.type as JobMarkType;
  const pageData = PAGE_DATA[markType];
  const query = pageData.query || getJobsQuery(pageData?.queryPatch);
  const { data, isLoading, refetch } = useScreenData({
    query,
  });
  const jobs = (data?.currentTalentJobBoardSearch?.data || []) as Job[];
  const { currentPage = 1, lastPage = 1 } =
    data?.currentTalentJobBoardSearch?.paginatorInfo ||
    data?.currentTalentJobRecommedations?.paginatorInfo ||
    {};

  return (
    <ConnectedPageLayout
      documentTitle="Job board"
      drawerProps={{}}
      headerProps={{ accountProps: {} }}
      isLoading={isLoading}
    >
      <Box maxWidth="768px">
        <Typography variant="h5">{pageData.title}</Typography>
      </Box>
      <Box>
        <Filter />
      </Box>
      <Box pt={8}>
        <JobList
          noItemsText={pageData.noItemsText}
          refetch={refetch}
          jobs={jobs}
        />
      </Box>
      {jobs.length > 0 && (
        <JobsPagination currentPage={currentPage} lastPage={lastPage} />
      )}
    </ConnectedPageLayout>
  );
};

export default MarkedJobs;
