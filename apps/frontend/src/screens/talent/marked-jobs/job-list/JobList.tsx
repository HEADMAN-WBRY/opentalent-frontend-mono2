import { useCurrentTime } from 'hooks/common/useCurrentTime';
import React from 'react';
import { isItExternalJob } from 'screens/talent/job-board-v2/content/Content';
import ExternalJobCardV2 from 'screens/talent/job-board-v2/content/external-job-card-v2';

import { Job } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import JobCard from '../../job-board-v2/content/job-card';
import { useJobModals } from '../../job-board-v2/hooks';
import InviteModal from '../../job-board-v2/invite-modal';

interface JobListProps {
  jobs: Job[];
  refetch: any;
  noItemsText: string;
}

const JobRecommendationsList = ({
  jobs,
  refetch,
  noItemsText = "You don't have any saved jobs. Try to look for relevant jobs from the main job board.",
}: JobListProps) => {
  const currentTime = useCurrentTime();
  const { inviteJob, handleClose, onJobApply, onInvite } = useJobModals();

  return (
    <div>
      <div>
        {!jobs.length && <Typography variant="body1">{noItemsText}</Typography>}
        {jobs.map((job, index) => {
          if (isItExternalJob(job)) {
            return (
              <ExternalJobCardV2
                key={`${job?.posted_at}-${index}`}
                job={job}
                currentTime={currentTime}
              />
            );
          }

          return (
            <JobCard
              key={job.id}
              onInvite={onInvite}
              onJobApply={onJobApply}
              job={job as Job}
              currentTime={currentTime}
              onJobSave={refetch}
            />
          );
        })}
      </div>
      {inviteJob && <InviteModal handleClose={handleClose} job={inviteJob} />}
    </div>
  );
};

export default React.memo(JobRecommendationsList);
