import { gql } from '@apollo/client';
import EXTERNAL_JOB_FRAGMENT from 'graphql/fragments/talent/externalJobFragment';
import FULL_JOB_FRAGMENT from 'graphql/fragments/talent/talentJobFragment';

export const getJobsQuery = (queryPatch = '') => {
  return gql`
    ${FULL_JOB_FRAGMENT}
    query GetAppliedJobs(
      $order_by: [QueryCurrentTalentJobBoardSearchOrderByOrderByClause!]
      $first: Int
      $page: Int
    ) {
      currentTalentJobBoardSearch(
        order_by: $order_by
        first: $first
        page: $page
        ${queryPatch}
      ) {
        paginatorInfo {
          count
          currentPage
          firstItem
          hasMorePages
          lastItem
          lastPage
          perPage
          total
        }
        data {
          ...FullJob
        }
      }
    }
  `;
};

export const GET_SAVED_EXTERNAL_JOBS = gql`
  ${EXTERNAL_JOB_FRAGMENT}
  query GetExternalJobs($first: Int = 20, $page: Int) {
    currentTalentExternalJobBoardSearch(
      first: $first
      page: $page
      is_saved: true
    ) {
      paginatorInfo {
        count
        currentPage
        firstItem
        hasMorePages
        lastItem
        lastPage
        perPage
        total
      }
      data {
        ...ExternalJobBoardJobFragment
      }
    }
  }
`;

export const GET_APPLIED_JOBS = gql`
  ${FULL_JOB_FRAGMENT}
  query GetAppliedJobs(
    $order_by: [QueryCurrentTalentJobBoardSearchOrderByOrderByClause!]
    $first: Int
    $page: Int
  ) {
    currentTalentJobBoardSearch(
      order_by: $order_by
      first: $first
      page: $page
      is_applied: true
    ) {
      paginatorInfo {
        count
        currentPage
        firstItem
        hasMorePages
        lastItem
        lastPage
        perPage
        total
      }
      data {
        ...FullJob
      }
    }
  }
`;

export const GET_RECOMMENDED_JOBS = gql`
  ${FULL_JOB_FRAGMENT}
  query GetExternalRecommendedJobs($first: Int = 20, $page: Int) {
    currentTalentJobRecommedations(page: $page, first: $first) {
      paginatorInfo {
        count
        currentPage
        firstItem
        hasMorePages
        lastItem
        lastPage
        perPage
        total
      }
      data {
        id
        job {
          ...FullJob
        }
        match_quality
      }
    }
  }
`;

export const SAVE_JOB = gql`
  mutation SaveJobToFavorites($job_id: ID!) {
    saveJobToFavorites(job_id: $job_id)
  }
`;

export const DELETE_JOB = gql`
  mutation DeleteJobFromFavorites($job_id: ID!) {
    deleteJobFromFavorites(job_id: $job_id)
  }
`;
