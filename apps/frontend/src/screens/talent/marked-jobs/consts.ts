import { JobMarkType } from 'types';

import { GET_RECOMMENDED_JOBS } from './queries';

export const PAGE_DATA = {
  [JobMarkType.Applied]: {
    title: 'My applications',
    queryPatch: 'is_applied: true',
    query: null,
    noItemsText:
      'You have no active job applications. Try to look for relevant jobs from the main job board.',
  },
  [JobMarkType.Saved]: {
    title: 'Saved jobs',
    queryPatch: 'is_saved: true',
    query: null,
    noItemsText:
      "You don't have any saved jobs. Try to look for relevant jobs from the main job board.",
  },
  [JobMarkType.Recommended]: {
    title: 'Recommended jobs',
    query: GET_RECOMMENDED_JOBS,
    noItemsText:
      "You don't have any recommended jobs. Try to look for relevant jobs from the main job board.",
  },
};
