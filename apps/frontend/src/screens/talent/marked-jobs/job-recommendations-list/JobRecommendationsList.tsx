import { useCurrentTime } from 'hooks/common/useCurrentTime';
import React from 'react';
import { isItExternalJob } from 'screens/talent/job-board-v2/content/Content';
import ExternalJobCardV2 from 'screens/talent/job-board-v2/content/external-job-card-v2';

import { Job, JobRecommendation } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import JobCard from '../../job-board-v2/content/job-card';
import { useJobModals } from '../../job-board-v2/hooks';
import InviteModal from '../../job-board-v2/invite-modal';

interface JobListProps {
  recommendations: JobRecommendation[];
  refetch: any;
  noItemsText: string;
}

const JobList = ({ recommendations, refetch }: JobListProps) => {
  const currentTime = useCurrentTime();
  const { inviteJob, handleClose, onJobApply, onInvite } = useJobModals();

  return (
    <div>
      <div>
        {!recommendations.length && (
          <Typography variant="body1">
            You don't have any saved jobs. Try to look for relevant jobs from
            the main job board.
          </Typography>
        )}
        {recommendations.map((rec, index) => {
          if (!rec.job) {
            return null;
          }

          if (isItExternalJob(rec.job)) {
            return (
              <ExternalJobCardV2
                key={`${rec?.job?.posted_at}-${index}`}
                job={rec.job}
                currentTime={currentTime}
                matchQuality={rec.match_quality}
              />
            );
          }

          return (
            <JobCard
              key={rec.job.id}
              onInvite={onInvite}
              onJobApply={onJobApply}
              job={rec.job as Job}
              currentTime={currentTime}
              onJobSave={refetch}
              matchQuality={rec.match_quality}
            />
          );
        })}
      </div>
      {inviteJob && <InviteModal handleClose={handleClose} job={inviteJob} />}
    </div>
  );
};

export default React.memo(JobList);
