import React from 'react';
import { useLocation } from 'react-router-dom';
import { pathManager } from 'routes';

import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { Box, Grid } from '@mui/material';

import { RouterButton } from '@libs/ui/components/button';

interface FilterProps { }

const Filter = (props: FilterProps) => {
  const location = useLocation();

  return (
    <Box mt={4}>
      <Grid
        container
        justifyContent="space-between"
        wrap="nowrap"
        alignItems="center"
      >
        <Grid item>
          <RouterButton
            to={{
              pathname: pathManager.talent.jobBoard.generatePath(),
              search: (location.state as any)?.jobBoardSearch || '',
            }}
            variant="outlined"
            color="info"
            startIcon={<ArrowBackIcon />}
          >
            back to all jobs
          </RouterButton>
        </Grid>
      </Grid>
    </Box>
  );
};

export default Filter;
