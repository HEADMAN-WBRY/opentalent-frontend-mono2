import { RouteComponentProps } from 'react-router-dom';

import { JobMarkType } from 'types';

export type PageProps = RouteComponentProps<{ type: JobMarkType }>;
