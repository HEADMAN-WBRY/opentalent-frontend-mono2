import { useMutation } from '@apollo/client';
import { useCurrentUser } from 'hooks/auth';
import useMixPanel from 'hooks/common/useMixPanel';
import { useSnackbar } from 'notistack';
import { useCallback, useMemo } from 'react';
import useCopyToClipboard from 'react-use/lib/useCopyToClipboard';

import {
  Mutation,
  MutationCreateInvitationLinkArgs,
} from '@libs/graphql-types';

import { CREATE_INVITATION_LINK, GET_SCREEN_DATA } from './queries';

export const useCopyToClipboardAction = () => {
  const { enqueueSnackbar } = useSnackbar();
  const [state, copyToClipboard] = useCopyToClipboard();

  return useCallback(
    ({ name, link }: { name: string; link: string }) => {
      copyToClipboard(
        `Hey ${name}! I want to invite you to OpenTalent, here’s your link:\n ${link}`,
      );

      if (!state.error) {
        enqueueSnackbar(
          `Your invite link for ${name} has been copied. \nYou now can send it with any messenger.`,
          { variant: 'success' },
        );
        return;
      }

      enqueueSnackbar('Something went wrong', { variant: 'error' });
    },
    [copyToClipboard, enqueueSnackbar, state],
  );
};

export const useOnCreateHandler = () => {
  const { enqueueSnackbar } = useSnackbar();
  const { data: userData } = useCurrentUser();
  const mixPanel = useMixPanel();
  const defaultVariables = useMemo(
    () => ({
      inviting_talent_id: userData?.currentTalent?.id || '',
      name: '',
    }),
    [userData?.currentTalent?.id],
  );
  const [create, { loading }] = useMutation<
    Mutation,
    MutationCreateInvitationLinkArgs
  >(CREATE_INVITATION_LINK, {
    variables: defaultVariables,
    refetchQueries: [{ query: GET_SCREEN_DATA }],
    onCompleted: (data) => {
      enqueueSnackbar(
        `Your invite link for ${data.createInvitationLink?.name} has been created`,
        {
          variant: 'success',
        },
      );
      mixPanel.track('User invited other user during the onboarding');
    },
    // eslint-disable-next-line no-console
    onError: console.error,
  });

  const onCreate = useCallback(
    (params: { name: string }) => {
      create({ variables: { ...defaultVariables, ...params } });
    },
    [create, defaultVariables],
  );

  return { onCreate, loading };
};
