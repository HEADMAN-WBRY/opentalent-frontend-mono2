import FixedFooter from 'components/custom/onboarding/fixed-footer';
import React from 'react';

import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { Grid } from '@mui/material';

import { RouterButton } from '@libs/ui/components/button';

import { useSaveAction } from '../saveStepHooks';
import NextButton from './NextButton';

interface FooterProps {
  currentStep: number;
  backLink?: string;
  nextLink: string;
  disabled: boolean;
  isLastStep?: boolean;
}

const Footer = ({
  currentStep,
  backLink,
  nextLink,
  disabled,
  isLastStep,
}: FooterProps) => {
  const { isSaving, save } = useSaveAction();

  return (
    <FixedFooter>
      <Grid spacing={6} container>
        {backLink && (
          <Grid xs={6} item>
            <RouterButton
              to={backLink}
              variant="outlined"
              size="large"
              color="primary"
              fullWidth
              startIcon={<ArrowBackIcon />}
            >
              Back
            </RouterButton>
          </Grid>
        )}
        <Grid xs={!!backLink ? 6 : 12} item>
          <NextButton
            currentStep={currentStep}
            nextLink={nextLink}
            disabled={disabled || isSaving}
            onClick={save}
            isLastStep={isLastStep}
          />
        </Grid>
      </Grid>
    </FixedFooter>
  );
};

export default Footer;
