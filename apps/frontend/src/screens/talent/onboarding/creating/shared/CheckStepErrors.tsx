import { FormikErrors, getIn } from 'formik';
import { useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import { pathManager } from 'routes';

import { CreatingFormState } from '../../../shared/profile/types';
import { STEPS_DATA } from '../consts';

interface CheckStepErrorsProps {
  errors: FormikErrors<CreatingFormState>;
  currentStep: number;
}

const CheckStepErrors = ({ errors, currentStep }: CheckStepErrorsProps) => {
  const history = useHistory();

  useEffect(() => {
    const step = STEPS_DATA.find((data) => {
      const { step, statePath } = data;
      const hasErrors = !!Object.keys(getIn(errors, statePath) || {}).length;

      if (hasErrors && step < currentStep) {
        return true;
      }
      return false;
    });

    if (step) {
      history.push(
        pathManager.talent.onboarding.creating.generatePath({
          step: step.step,
        }),
      );
    }
  }, [currentStep, errors, history]);

  return null;
};

export default CheckStepErrors;
