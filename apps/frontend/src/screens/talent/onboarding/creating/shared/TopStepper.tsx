import cn from 'classnames';
import React from 'react';
import { useHistory } from 'react-router-dom';
import { pathManager } from 'routes';
import { StepDataType } from 'screens/talent/shared/profile/types';

import { Step, StepLabel, Stepper } from '@mui/material';
import { makeStyles } from '@mui/styles';

interface StepperProps {
  current: number;
  stepsData: StepDataType[];
}

export const useStyles = makeStyles((theme) => ({
  stepper: {
    background: 'none',
    padding: 0,
    marginBottom: theme.spacing(18),

    '& .MuiStepConnector-root': {
      [theme.breakpoints.down('sm')]: {
        visibility: 'hidden',
      },
    },
  },
  root: {
    [theme.breakpoints.down('sm')]: {
      padding: 0,
    },

    '&:last-child .MuiStepLabel-iconContainer': {
      padding: 0,
    },

    '& span > svg': {
      '& > circle': {
        color: theme.palette.grey[700],
      },
      '& > text': {
        color: theme.palette.primary.contrastText,
      },
    },
  },
  iconContainer: {
    padding: 0,
  },
  current: {
    '& span > svg': {
      fontSize: 48,
      '& > circle': {
        color: theme.palette.primary.main,
      },
    },
  },
  completed: {
    '& svg': {
      color: `${theme.palette.grey[500]} !important`,
    },
  },
  clickable: {
    cursor: 'pointer',
  },
}));

const TopStepper = ({ current, stepsData }: StepperProps) => {
  const history = useHistory();
  const classes = useStyles();

  return (
    <Stepper activeStep={current - 1} className={classes.stepper}>
      {stepsData.map(({ step }) => {
        const isClickable = current > step;
        const isCurrent = step === current;

        return (
          <Step
            key={step}
            onClick={() => {
              if (!isClickable) {
                return;
              }
              history.push(
                pathManager.talent.onboarding.creating.generatePath({
                  step,
                }),
              );
            }}
            classes={{
              root: cn(classes.root, {
                [classes.current]: isCurrent,
                [classes.clickable]: isClickable,
                [classes.completed]: isClickable,
              }),
            }}
          >
            <StepLabel classes={{ iconContainer: classes.iconContainer }} />
          </Step>
        );
      })}
    </Stepper>
  );
};

export default TopStepper;
