import { useFormikContext } from 'formik';
import React from 'react';
import { useHistory } from 'react-router-dom';

import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import SaveIcon from '@mui/icons-material/Save';
import { Button } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { CreatingFormState } from '../../../shared/profile/types';

interface IProps {
  currentStep: number;
  nextLink: string;
  disabled: boolean;
  isLastStep?: boolean;
  onClick: () => Promise<any>;
}

const useStyles = makeStyles((theme) => ({
  disabled: {
    pointerEvents: 'none',
  },

  link: {
    width: '100%',
  },

  button: {
    width: '100%',
    background: `${theme.palette.primary.main} !important`,
  },
}));

const NextButton: React.FC<IProps> = ({
  currentStep,
  nextLink,
  disabled,
  onClick,
  isLastStep,
}) => {
  const classes = useStyles();
  const history = useHistory();
  const { submitForm } = useFormikContext<CreatingFormState>();
  const goToNext = async () =>
    onClick().finally(() => {
      history.push(nextLink);
    });

  if (isLastStep) {
    return (
      <Button
        variant="contained"
        size="large"
        data-test-id="saveButton"
        color="primary"
        endIcon={<SaveIcon />}
        onClick={submitForm}
        className={classes.button}
        disabled={disabled}
        fullWidth
      >
        Save profile
      </Button>
    );
  }

  return (
    <Button
      data-test-id="nextButton"
      variant="contained"
      color="primary"
      endIcon={<ArrowForwardIcon />}
      className={classes.button}
      disabled={disabled}
      fullWidth
      size="large"
      onClick={goToNext}
    >
      Next
    </Button>
  );
};

export default NextButton;
