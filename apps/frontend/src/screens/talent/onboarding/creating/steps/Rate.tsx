import React from 'react';

import InfoIcon from '@mui/icons-material/Info';
import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { modelPath } from '@libs/helpers/form';
import ConnectedSlider from '@libs/ui/components/form/slider';
import {
  MAX_JOB_RATE,
  MIN_JOB_RATE,
} from '@libs/ui/components/job/utils/consts';
import Typography from '@libs/ui/components/typography';

import { CreatingFormState } from '../../../shared/profile/types';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    maxWidth: 500,
    margin: '0 auto',
  },
  info: {
    color: theme.palette.tertiary.main,
    marginTop: theme.spacing(12),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',

    '& .MuiTypography-root': {
      marginLeft: theme.spacing(2),
      fontStyle: 'italic',
      lineHeight: '28px',
    },
  },
}));

const Rate: React.FC = () => {
  const classes = useStyles();

  return (
    <Box className={classes.wrapper}>
      <ConnectedSlider
        name={modelPath<CreatingFormState>((m) => m.rate.min)}
        min={MIN_JOB_RATE}
        max={MAX_JOB_RATE}
        visibleMin={MIN_JOB_RATE}
        helperText={`Min. €${MIN_JOB_RATE} /hour`}
      />
      <Box className={classes.info}>
        <InfoIcon fontSize="small" />
        <a
          target="_blank"
          rel="noreferrer"
          href="https://www.notion.so/Setting-your-hourly-rate-bb251dffab694c979f80e869cc5c0c56"
        >
          <Typography variant="caption">
            How to determine your min. hourly rate?
          </Typography>
        </a>
      </Box>
    </Box>
  );
};

export default Rate;
