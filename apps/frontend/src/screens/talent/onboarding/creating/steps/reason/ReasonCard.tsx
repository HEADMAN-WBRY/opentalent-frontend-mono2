import React from 'react';

import { Box } from '@mui/material';
import Typography from '@mui/material/Typography';
import { makeStyles } from '@mui/styles';

import Checkbox from '@libs/ui/components/form/checkbox';

import { ReasonCardProps } from './types';

const useStyles = makeStyles((theme) => ({
  root: {
    borderRadius: 3,
    padding: 24,
    background:
      'linear-gradient(180deg, rgba(255, 255, 255, 0.07) 0%, rgba(255, 255, 255, 0.07) 100%), #121212',
    width: 374,
    cursor: 'pointer',
    outline: ({ selected }: { selected: boolean }) =>
      selected
        ? `2px solid ${theme.palette.success.main}`
        : '2px solid transparent',
    transition: 'outline 0.3s ease',

    '& .MuiCheckbox-root': {
      color: 'rgba(255, 255, 255, 0.7)',

      '&.Mui-checked': {
        color: theme.palette.success.main,
      },
    },
  },
}));

export const ReasonCard = ({
  onChange,
  title,
  text,
  selected = false,
}: ReasonCardProps) => {
  const classes = useStyles({ selected });

  return (
    <Box onClick={onChange} className={classes.root}>
      <Checkbox
        color="primary"
        formControlLabelProps={{
          componentsProps: { typography: { variant: 'h6' } },
        }}
        checked={selected}
        label={title}
      />

      <Typography component={Box} variant="body2">
        {text}
      </Typography>
    </Box>
  );
};
