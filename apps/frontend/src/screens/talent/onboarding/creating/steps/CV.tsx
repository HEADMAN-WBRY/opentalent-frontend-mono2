import { useMutation } from '@apollo/client';
import { ConnectedDropzone } from 'components/form/dropzone';
import { getIn, useFormikContext } from 'formik';
import { UPLOAD_TALENT_DOCUMENT } from 'graphql/talents';
import { useSnackbar } from 'notistack';
import React, { useCallback } from 'react';
import ServerFiles from 'screens/talent/edit-profile/verification-section/ServerFiles';

import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import { Box, CircularProgress, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import {
  Mutation,
  MutationUploadTalentDocumentArgs,
  TalentDocument,
} from '@libs/graphql-types';
import { modelPath } from '@libs/helpers/form';
import { OuterLink } from '@libs/ui/components/typography';
import InfoLinkContainer from '@libs/ui/components/typography/InfoLinkContainer';

import { CreatingFormState } from '../../../shared/profile/types';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    maxWidth: 600,
    margin: '0 auto',
  },
  dropzone: {
    '& > div > div': {
      borderColor: theme.palette.grey[300],
      color: theme.palette.secondary.contrastText,
      height: 242,
      width: 194,
      marginLeft: 'auto',
      marginRight: 'auto',
      position: 'relative',

      '&::before': {
        position: 'absolute',
        content: '""',
        height: 50,
        width: 50,
        top: 0,
        right: 0,
        borderColor: theme.palette.grey[300],
        borderBottom: '1px dashed',
        borderLeft: '1px dashed',
      },

      '&::after': {
        position: 'absolute',
        content: '""',
        height: 60,
        width: 70,
        top: '-27px',
        right: '-32px',
        borderColor: '#0F0F0D',
        borderBottom: '1px dashed',
        transform: 'rotate(45deg)',
        backgroundColor: '#0F0F0D',
        pointerEvents: 'none',
      },

      '& .MuiSvgIcon-root': {
        fontSize: '48px',
      },

      '& .MuiTypography-root': {
        color: theme.palette.secondary.contrastText,
        fontSize: '16px',
      },
    },
  },
  info: {
    color: theme.palette.tertiary.main,
    marginTop: theme.spacing(10),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',

    '& .MuiTypography-root': {
      marginLeft: theme.spacing(2),
      fontStyle: 'italic',
      lineHeight: '28px',
    },
  },
}));

export const useDocumentsChange = ({
  name,
  talentId = '1',
}: {
  name: string;
  talentId?: string;
}) => {
  const { enqueueSnackbar } = useSnackbar();
  const [upload, { loading }] = useMutation<
    Mutation,
    MutationUploadTalentDocumentArgs
  >(UPLOAD_TALENT_DOCUMENT);
  const { setFieldValue, values } = useFormikContext<CreatingFormState>();

  const onDocumentsChange = useCallback(
    async (files: File[]) => {
      const filesToUpload = files.filter((i) => !(i as any).hash);
      const currentFiles: TalentDocument[] = getIn(values, name);
      try {
        if (!filesToUpload.length) {
          return;
        }

        const res = await upload({
          variables: { files: filesToUpload, talent_id: talentId },
        });
        const uploadedFiles = (res.data?.uploadTalentDocument ||
          []) as TalentDocument[];
        const allFiles = currentFiles.concat(uploadedFiles);

        setFieldValue(name, allFiles);
      } catch (e) {
        enqueueSnackbar((e as any).toString());
      }
    },
    [values, name, upload, talentId, setFieldValue, enqueueSnackbar],
  );
  return { onDocumentsChange, loading };
};

const CV: React.FC = () => {
  const classes = useStyles();
  const { values } = useFormikContext<CreatingFormState>();
  const talentId = values.talentData?.id;
  const path = modelPath<CreatingFormState>((m) => m.cv.documents);
  const { onDocumentsChange, loading } = useDocumentsChange({
    name: path,
    talentId,
  });

  return (
    <Box className={classes.wrapper}>
      <Grid item className={classes.dropzone}>
        <ConnectedDropzone
          name="test"
          dropzoneOptions={{
            accept: 'application/pdf',
          }}
          onChange={onDocumentsChange}
          Icon={CloudUploadIcon}
          label="Upload your CV"
          withoutList
        />
      </Grid>
      {loading ? (
        <Box pt={2} justifyContent="center" display="flex">
          <CircularProgress color="primary" />
        </Box>
      ) : (
        <ServerFiles name={path} />
      )}
      <Box pt={4}>
        <InfoLinkContainer centered>
          <OuterLink
            target="_blank"
            rel="noreferrer"
            href="https://opentalent.notion.site/How-to-download-your-CV-from-LinkedIn-fc3c09607d54433291c8eb72290592c1"
            variant="caption"
          >
            Click here to learn how to download your CV from your LinkedIn
            profile.
          </OuterLink>
        </InfoLinkContainer>
      </Box>
    </Box>
  );
};

export default CV;
