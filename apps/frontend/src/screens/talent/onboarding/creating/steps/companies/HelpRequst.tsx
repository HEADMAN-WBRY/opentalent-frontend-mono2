import React from 'react';
import { CreatingFormState } from 'screens/talent/shared/profile/types';

import { Box } from '@mui/material';
import { grey } from '@mui/material/colors';
import { makeStyles } from '@mui/styles';

import { modelPath } from '@libs/helpers/form';
import { ConnectedCheckbox } from '@libs/ui/components/form/checkbox';
import Typography from '@libs/ui/components/typography';

interface HelpRequstProps { }

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 700,
    margin: '0 auto',
    padding: theme.spacing(8, 12, 10),
    background: grey[900],
    borderRadius: theme.spacing(4),

    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(8, 4, 5),
    },
  },
  checkboxes: {
    display: 'flex',
    justifyContent: 'center',
    padding: theme.spacing(8, 6),
    background: grey[800],
    borderRadius: theme.spacing(4),
    maxWidth: 740,
    margin: '0 auto',

    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(4, 3),
    },
  },
}));

const CHECKBOXES = [
  {
    text: 'Yes, tell me more.',
    path: modelPath<CreatingFormState>(
      (m) => m.help?.knowCompaniesThatBenefits,
    ),
  },
];

export const HelpRequst = (props: HelpRequstProps) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Box mb={2}>
        <Typography textAlign="center" variant="h6">
          Join our referral program 💰
        </Typography>
      </Box>
      <Box mb={6}>
        <Typography textAlign="center" variant="body2">
          Do you know companies that can benefit from modern recruiting
          solutions? If so, you might want to learn more about the OpenTalent
          referral program.
        </Typography>
      </Box>

      <Box className={classes.checkboxes}>
        {CHECKBOXES.map(({ text, path }) => (
          <Box>
            <ConnectedCheckbox name={path} label={text} color="info" />
          </Box>
        ))}
      </Box>
    </Box>
  );
};
