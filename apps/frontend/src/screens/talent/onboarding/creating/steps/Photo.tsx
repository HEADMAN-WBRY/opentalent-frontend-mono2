import { useFormikContext } from 'formik';
import React from 'react';
import { useAvatarChange } from 'screens/talent/shared/profile/hooks';
import { isObjectEmpty } from 'utils/common';

import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { modelPath } from '@libs/helpers/form';
import ConnectedImageUpload from '@libs/ui/components/form/image-upload/ConnectedImageUpload';

import { CreatingFormState } from '../../../shared/profile/types';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    '& > div': {
      margin: '0 auto',
      width: 242,
      height: 242,
      background: 'none',
      borderColor: theme.palette.grey[300],

      '& > div': {
        color: theme.palette.secondary.contrastText,

        '& svg': {
          width: 48,
          height: 48,
        },
      },
    },
  },
}));

const Photo: React.FC = () => {
  const classes = useStyles();
  const { values } = useFormikContext<CreatingFormState>();
  const hashPath = modelPath<CreatingFormState>((m) => m.picture.hash);
  const talentId = values.talentData?.id;
  const chosenFile = values.picture.files?.[0] || {};
  const noChosenFile = isObjectEmpty(chosenFile);
  const initialAvatar =
    (!noChosenFile && URL.createObjectURL(chosenFile as MediaSource)) ||
    values.picture.avatar;
  const { onAvatarChange } = useAvatarChange({
    name: hashPath,
    talentId,
    avatarPath: modelPath<CreatingFormState>((m) => m.picture.avatar),
  });

  return (
    <Box className={classes.wrapper}>
      <ConnectedImageUpload
        name={modelPath<CreatingFormState>((m) => m.picture.files)}
        initialImage={initialAvatar}
        onChange={onAvatarChange}
      />
    </Box>
  );
};

export default Photo;
