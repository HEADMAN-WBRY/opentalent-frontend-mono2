import { useFormikContext } from 'formik';
import useMediaQueries from 'hooks/common/useMediaQueries';
import React from 'react';
import { MAX_DESCRIBE_CHARS } from 'screens/talent/shared/profile/consts';

import { Box } from '@mui/material';

import { modelPath } from '@libs/helpers/form';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import Typography from '@libs/ui/components/typography';

import { CreatingFormState } from '../../../shared/profile/types';

const ApplicationPitch: React.FC = () => {
  const { values } = useFormikContext<CreatingFormState>();
  const { isXS } = useMediaQueries();
  const aboutLength = values.application?.pitch?.length || 0;

  return (
    <>
      <Box>
        <ConnectedTextField
          name={modelPath<CreatingFormState>((m) => m.application?.pitch)}
          fullWidth
          size="small"
          variant="filled"
          label="Pitch"
          multiline
          rows={isXS ? 8 : 4}
          placeholder={`Free text (max ${MAX_DESCRIBE_CHARS} characters)`}
        />
        <Box textAlign="right">
          <Typography variant="caption">
            {`${aboutLength} of ${MAX_DESCRIBE_CHARS}`}
          </Typography>
        </Box>
      </Box>
    </>
  );
};

export default ApplicationPitch;
