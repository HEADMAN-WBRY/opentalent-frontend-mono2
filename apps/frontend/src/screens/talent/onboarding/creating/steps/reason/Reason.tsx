import { getIn, useFormikContext } from 'formik';
import React from 'react';
import { CreatingFormState } from 'screens/talent/shared/profile/types';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { JoinReasonEnum } from '@libs/graphql-types';
import { modelPath } from '@libs/helpers/form';

import { ReasonCard } from './ReasonCard';
import { CARDS_DATA } from './consts';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    maxWidth: 375 * 2 + 16,
    margin: '0 auto',
  },
}));

const Reason: React.FC = () => {
  const classes = useStyles();
  const { setFieldValue, values } = useFormikContext<CreatingFormState>();
  const currentReasons: JoinReasonEnum[] =
    getIn(
      values,
      modelPath<CreatingFormState>((m) => m.reasons),
    ) || [];

  return (
    <Box className={classes.wrapper}>
      <Grid container spacing={4}>
        {CARDS_DATA.map((m) => {
          const selected = currentReasons.includes(m.value);

          return (
            <Grid key={m.title} item>
              <ReasonCard
                {...m}
                selected={selected}
                onChange={() => {
                  const newArr = selected
                    ? currentReasons.filter((r) => r !== m.value)
                    : currentReasons.concat(m.value);
                  setFieldValue(
                    modelPath<CreatingFormState>((m) => m.reasons),
                    newArr,
                  );
                }}
              />
            </Grid>
          );
        })}
      </Grid>
    </Box>
  );
};

export default Reason;
