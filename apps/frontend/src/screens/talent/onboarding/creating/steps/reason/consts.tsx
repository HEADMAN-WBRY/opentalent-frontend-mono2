import { JoinReasonEnum } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import { ReasonCardProps } from './types';

export const CARDS_DATA: Pick<ReasonCardProps, 'title' | 'text' | 'value'>[] = [
  {
    title: 'Permanent work',
    text: "I'm (actively) looking for a new full-time job",
    value: JoinReasonEnum.PermanentWork,
  },
  {
    title: 'Freelancing work',
    text: 'I want new freelancing projects/gigs',
    value: JoinReasonEnum.Freelance,
  },
  {
    title: 'Networking',
    text: 'I want to meet new interesting people',
    value: JoinReasonEnum.Networking,
  },
  {
    title: 'Perks & Benefits',
    text: 'I want access to special perks & benefits',
    value: JoinReasonEnum.PerksAndBenefits,
  },
  {
    title: 'Candidates',
    text: (
      <Typography variant="body2">
        I want to find candidates for open roles{' '}
        <Typography variant="body2" color="primary">
          👉 for Companies
        </Typography>
      </Typography>
    ),
    value: JoinReasonEnum.Candidates,
  },
  {
    title: 'Venture building',
    text: (
      <Typography variant="body2">
        I want to find people to build my startup{' '}
        <Typography variant="body2" color="primary">
          👉 for Founders
        </Typography>
      </Typography>
    ),
    value: JoinReasonEnum.VentureBuilding,
  },
];
