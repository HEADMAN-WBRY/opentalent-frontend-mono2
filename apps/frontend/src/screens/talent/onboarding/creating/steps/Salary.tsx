import React from 'react';

import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { modelPath } from '@libs/helpers/form';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import Typography from '@libs/ui/components/typography';

import { CreatingFormState } from '../../../shared/profile/types';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    maxWidth: 500,
    margin: '0 auto',
  },
  info: {
    color: theme.palette.tertiary.main,
    marginTop: theme.spacing(12),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: 'pointer',

    '& .MuiTypography-root': {
      marginLeft: theme.spacing(2),
      fontStyle: 'italic',
      lineHeight: '28px',
    },
  },
}));

const Salary: React.FC = () => {
  const classes = useStyles();

  return (
    <Box className={classes.wrapper}>
      <ConnectedTextField
        name={modelPath<CreatingFormState>((m) => m.salary.salary)}
        fullWidth
        variant="filled"
        label="Min. amount"
        type="number"
        InputProps={{
          endAdornment: (
            <Typography variant="body2" color="text.secondary">
              €/m
            </Typography>
          ),
        }}
      />
    </Box>
  );
};

export default Salary;
