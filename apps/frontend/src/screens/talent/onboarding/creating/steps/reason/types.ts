import { JoinReasonEnum } from '@libs/graphql-types';

export interface ReasonCardProps {
  onChange: VoidFunction;
  title: string;
  text: React.ReactNode;
  value: JoinReasonEnum;
  selected?: boolean;
}
