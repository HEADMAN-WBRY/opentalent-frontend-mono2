import { SubCategorySelector } from 'components/custom/subcategory-selector';
import { TALENT_CATEGORIES } from 'graphql/talents';
import React, { useState } from 'react';

import { Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { Query, TalentCategory } from '@libs/graphql-types';
import { COUNTRY_OPTIONS } from '@libs/helpers/consts/countries';
import { modelPath } from '@libs/helpers/form';
import { ConnectedPhoneField } from '@libs/ui/components/form/phone-field';
import {
  ConnectedGraphSelect,
  ConnectedSelect,
} from '@libs/ui/components/form/select';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import { OuterLink } from '@libs/ui/components/typography';
import InfoLinkContainer from '@libs/ui/components/typography/InfoLinkContainer';

import { CreatingFormState } from '../../../shared/profile/types';

interface ProfileProps { }

const useStyles = makeStyles((theme) => ({
  wrapper: {
    maxWidth: 496,
    margin: '0 auto',
    width: '100%',

    '& > div': {
      paddingLeft: '0px !important',
    },

    '& .MuiFormHelperText-root': {
      color: theme.palette.grey[500],
    },
  },
  subcategory: {
    '&:empty': {
      display: 'none',
    },
  },
}));

export const Profile = (props: ProfileProps) => {
  const classes = useStyles();
  const [categories, setCategories] = useState<TalentCategory[]>([]);

  return (
    <Grid spacing={4} direction="column" container className={classes.wrapper}>
      <Grid item>
        <ConnectedTextField
          name={modelPath<CreatingFormState>((m) => m.profile.email)}
          fullWidth
          variant="filled"
          label="Enter your email"
        />
      </Grid>
      <Grid item>
        <Grid spacing={4} container>
          <Grid xs={12} sm={6} item>
            <ConnectedTextField
              name={modelPath<CreatingFormState>((m) => m.profile.firstName)}
              data-test-id="first_name"
              fullWidth
              variant="filled"
              label="First name"
            />
          </Grid>
          <Grid xs={12} sm={6} item>
            <ConnectedTextField
              name={modelPath<CreatingFormState>((m) => m.profile.lastName)}
              data-test-id="last_name"
              fullWidth
              variant="filled"
              label="Last name"
            />
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <ConnectedSelect
          name={modelPath<CreatingFormState>((m) => m.profile.location)}
          fullWidth
          variant="filled"
          data-test-id="location"
          label="Where are you based"
          options={COUNTRY_OPTIONS}
          color="primary"
          helperText="Currently only accepting people based in the European Union"
        />
      </Grid>
      <Grid item>
        <ConnectedGraphSelect
          query={TALENT_CATEGORIES}
          queryOptions={{
            fetchPolicy: 'network-only',
            onCompleted: (query: Query) =>
              setCategories((query?.talentCategories as any[]) || []),
          }}
          dataPath="talentCategories"
          data-test-id="talentCategories"
          dataMap={{ text: 'name', value: 'id' }}
          fullWidth
          name={modelPath<CreatingFormState>((m) => m.profile.category)}
          variant="filled"
          label="Category"
        />
      </Grid>
      <Grid className={classes.subcategory} item>
        <SubCategorySelector
          categoryPath={modelPath<CreatingFormState>((m) => m.profile.category)}
          name={modelPath<CreatingFormState>((m) => m.profile.subcategories)}
          categories={categories}
          chipProps={{
            color: 'tertiary',
            size: 'small',
          }}
          multiple
          autoCompleteProps={{
            filterSelectedOptions: true,
            popupIcon: null,
          }}
          inputProps={{
            variant: 'filled',
            label: 'Select sub-category',
          }}
        />
      </Grid>
      <Grid item>
        <ConnectedTextField
          name={modelPath<CreatingFormState>((m) => m.profile.position)}
          fullWidth
          data-test-id="recent_position_title"
          variant="filled"
          label="Personal Tagline"
          helperText="Describe yourself in max. 40 characters"
        />
      </Grid>

      <Grid item>
        <ConnectedTextField
          name={modelPath<CreatingFormState>((m) => m.profile.linkedLink)}
          fullWidth
          variant="filled"
          label="Paste your LinkedIn profile URL"
          helperText={
            <InfoLinkContainer>
              <OuterLink
                href="https://opentalent.notion.site/How-to-find-my-LinkedIn-Profile-URL-3bdad4015cf949878a73494352836730"
                target="_blank"
                rel="noreferrer"
                variant="caption"
              >
                <i>Click here to learn how to copy your LinkedIn profile URL</i>
              </OuterLink>
            </InfoLinkContainer>
          }
        />
      </Grid>
      <Grid item>
        <ConnectedPhoneField
          name={modelPath<CreatingFormState>((m) => m.profile.phone)}
          fullWidth
          defaultCountry="nl"
          variant="filled"
          label="Phone"
        />
      </Grid>
    </Grid>
  );
};
