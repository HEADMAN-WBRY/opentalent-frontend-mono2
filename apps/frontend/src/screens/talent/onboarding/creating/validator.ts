import { useApolloClient } from '@apollo/client';
import { MAX_DATE } from 'consts/date';
import { SKILLS_LIMITS, SOLUTIONS_WITH_HARD } from 'consts/skills';
import errors from 'consts/validationErrors';
import { AvailabilityType } from 'screens/talent/edit-profile/types';
import {
  MAX_COMPANIES_COUNT,
  MAX_DESCRIBE_CHARS,
  MAX_SALARY,
} from 'screens/talent/shared/profile/consts';
import * as yup from 'yup';

import {
  CheckTalentExistsByEmailDocument,
  SkillTypeEnum,
} from '@libs/graphql-types';
import {
  maxStringValidator,
  string64Validator,
  varCharStringValidator,
} from '@libs/helpers/yup';

export const useValidator = () => {
  const client = useApolloClient();

  return yup.object().shape({
    profile: yup.object().shape({
      firstName: varCharStringValidator.required(),
      lastName: varCharStringValidator.required(),
      email: yup
        .string()
        .trim()
        .email('Invalid email')
        .required("Email can't be empty")
        .test(
          'email-exist-check',
          'A talent account with this email already exists',
          async function test(email) {
            const isValid = await yup
              .string()
              .email()
              .required()
              .validate(email);
            if (isValid) {
              const res = await client.query({
                query: CheckTalentExistsByEmailDocument,
                variables: { talent_email: email },
              });
              return !res.data.checkTalentExistsByEmail;
            }
            return this.schema.validate(email);
          },
        ),

      position: maxStringValidator.required(),
      location: varCharStringValidator.required(),
      category: yup.string().required(),
      linkedLink: maxStringValidator.customUrl(),
      subcategories: yup.array().max(3).min(1),
      phone: string64Validator.phone(),
    }),
    cv: yup.object().shape({
      documents: yup.array().of(yup.object()).min(1),
    }),

    reasons: yup.array().of(yup.string()).min(1),

    availability: yup.object().shape({
      availableNow: yup.string().required(),
      availableDate: yup
        .date()
        .nullable()
        .when('availableNow', {
          is: (availableNow: AvailabilityType) =>
            availableNow === AvailabilityType.Later,
          then: yup
            .date()
            .typeError(errors.invalidDate)
            .nullable()
            .min(new Date())
            .required()
            .max(MAX_DATE),
          otherwise: yup.date().nullable(),
        }),
      hoursPerWeek: yup.number().required(),
    }),
    rate: yup.object().shape({
      min: yup.string().required(),
    }),
    salary: yup.object().shape({
      salary: yup.number().required().max(MAX_SALARY, `Maximum ${MAX_SALARY}`),
    }),
    companies: yup.object().shape({
      companies: yup
        .array()
        .max(MAX_COMPANIES_COUNT, `Maximum ${MAX_COMPANIES_COUNT} companies`)
        .min(1, `Minimum 1 company`),
    }),
    describe: yup.object().shape({
      about: yup.string().trim().max(MAX_DESCRIBE_CHARS),
    }),
    skills: yup.object().shape({
      [SOLUTIONS_WITH_HARD]: yup
        .array()
        .max(
          SKILLS_LIMITS[SOLUTIONS_WITH_HARD].max,
          errors.maxOptionsLength(SKILLS_LIMITS[SOLUTIONS_WITH_HARD].max),
        )
        .min(
          SKILLS_LIMITS[SOLUTIONS_WITH_HARD].min,
          errors.minOptionsLength(SKILLS_LIMITS[SOLUTIONS_WITH_HARD].min),
        ),
      [SkillTypeEnum.SoftSkills]: yup
        .array()
        .max(
          SKILLS_LIMITS[SkillTypeEnum.SoftSkills].max,
          errors.maxOptionsLength(SKILLS_LIMITS[SkillTypeEnum.SoftSkills].max),
        )
        .min(
          SKILLS_LIMITS[SkillTypeEnum.SoftSkills].min,
          errors.minOptionsLength(SKILLS_LIMITS[SkillTypeEnum.SoftSkills].min),
        ),
    }),
  });
};
