import { pathManager } from 'routes';

import { modelPath } from '@libs/helpers/form';
import { OuterLink } from '@libs/ui/components/typography';

import { CreatingFormState, StepDataType } from '../../shared/profile/types';
import { Profile } from './steps';
import ApplicationPitch from './steps/ApplicationPitch';
import Availability from './steps/Availability';
import CV from './steps/CV';
import Describe from './steps/Describe';
import Photo from './steps/Photo';
import Rate from './steps/Rate';
import Salary from './steps/Salary';
import Skills from './steps/Skills';
import Companies from './steps/companies';
import Reason from './steps/reason';

export const STEPS_DATA: StepDataType[] = [
  {
    title: `Start your application`,
    subTitle: 'Expected completion time: <2 minutes ',
    step: 1,
    Content: Profile,
    nextLink: pathManager.talent.onboarding.creating.generatePath({ step: 2 }),
    backLink: pathManager.talent.onboarding.intro.generatePath({ step: 4 }),
    statePath: modelPath<CreatingFormState>((m) => m.profile),
  },
  {
    title: 'Tell us what you are looking for',
    subTitle: 'Select the things that interest you.',
    step: 2,
    Content: Reason,
    nextLink: pathManager.talent.onboarding.creating.generatePath({ step: 3 }),
    backLink: pathManager.talent.onboarding.creating.generatePath({ step: 1 }),
    statePath: modelPath<CreatingFormState>((m) => m.reasons),
  },
  {
    title: 'Upload your profile picture',
    subTitle: 'Remember that a picture speaks a thousand words ;)',
    step: 3,
    Content: Photo,
    backLink: pathManager.talent.onboarding.creating.generatePath({ step: 2 }),
    nextLink: pathManager.talent.onboarding.creating.generatePath({ step: 4 }),
    statePath: modelPath<CreatingFormState>((m) => m.picture),
  },
  {
    title: 'Availability',
    subTitle: 'Tell clients when you’re open for new roles.',
    step: 4,
    Content: Availability,
    backLink: pathManager.talent.onboarding.creating.generatePath({ step: 3 }),
    nextLink: pathManager.talent.onboarding.creating.generatePath({ step: 5 }),
    statePath: modelPath<CreatingFormState>((m) => m.availability),
  },
  {
    title: 'Minimum hourly rate (€)',
    subTitle: 'What’s the minimum rate you charge per hour?',
    step: 5,
    Content: Rate,
    backLink: pathManager.talent.onboarding.creating.generatePath({ step: 4 }),
    nextLink: pathManager.talent.onboarding.creating.generatePath({ step: 6 }),
    statePath: modelPath<CreatingFormState>((m) => m.rate),
  },
  {
    title: 'Monthly salary (€)',
    subTitle: 'What’s your minimum monthly salary expectation?',
    step: 6,
    Content: Salary,
    backLink: pathManager.talent.onboarding.creating.generatePath({ step: 5 }),
    nextLink: pathManager.talent.onboarding.creating.generatePath({ step: 7 }),
    statePath: modelPath<CreatingFormState>((m) => m.salary),
  },
  {
    title: 'Skills passport',
    subTitle: 'List your top skills so we can match you to jobs.',
    step: 7,
    Content: Skills,
    backLink: pathManager.talent.onboarding.creating.generatePath({ step: 6 }),
    nextLink: pathManager.talent.onboarding.creating.generatePath({ step: 8 }),
    statePath: modelPath<CreatingFormState>((m) => m.skills),
  },
  {
    title: 'Companies you worked at',
    subTitle: 'What companies have you worked for / served as a supplier?',
    step: 8,
    Content: Companies,
    backLink: pathManager.talent.onboarding.creating.generatePath({ step: 7 }),
    nextLink: pathManager.talent.onboarding.creating.generatePath({ step: 9 }),
    statePath: modelPath<CreatingFormState>((m) => m.companies),
  },
  {
    title: 'Upload your CV',
    subTitle:
      'So we can best match you with new roles based on information we might have missed.',
    Content: CV,
    step: 9,
    backLink: pathManager.talent.onboarding.creating.generatePath({ step: 8 }),
    nextLink: pathManager.talent.onboarding.creating.generatePath({ step: 10 }),
    statePath: modelPath<CreatingFormState>((m) => m.cv),
  },
];

export const getDescribeStep = ({ isLast }: { isLast: boolean }) => {
  return {
    title: 'Describe your ideal role',
    subTitle:
      'What you are looking for now? Any companies you want to work for? Anything clients need to know?',
    Content: Describe,
    step: 10,
    backLink: pathManager.talent.onboarding.creating.generatePath({ step: 9 }),
    nextLink: isLast
      ? pathManager.talent.onboarding.finish.generatePath()
      : pathManager.talent.onboarding.creating.generatePath({ step: 11 }),
    statePath: modelPath<CreatingFormState>((m) => m.describe),
  };
};

export const getPitchStep = ({
  jobName,
  jobLink,
}: {
  jobName: string;
  jobLink: string;
}): StepDataType => ({
  title: 'A Brief Motivational Message',
  subTitle: (
    <>
      Tell us why you are the perfect candidate for this role?{' '}
      <OuterLink href={jobLink} target="_blank">
        ({jobName})
      </OuterLink>
    </>
  ),
  Content: ApplicationPitch,
  step: 11,
  backLink: pathManager.talent.onboarding.creating.generatePath({ step: 10 }),
  nextLink: pathManager.talent.onboarding.finish.generatePath(),
  statePath: modelPath<CreatingFormState>((m) => m.application),
});
