import { LOCAL_STORAGE_KEYS } from 'consts/ls-keys';

import { QueryGetTempDataItemArgs } from '@libs/graphql-types';

export const getTmpDataArgs = (): QueryGetTempDataItemArgs => ({
  device_id:
    localStorage.getItem(LOCAL_STORAGE_KEYS.talentOnboardingDeviceId) || '',
  email: localStorage.getItem(LOCAL_STORAGE_KEYS.talentOnboardingEmail),
});
