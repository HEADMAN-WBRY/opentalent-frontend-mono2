import FixedFooter from 'components/custom/onboarding/fixed-footer';
import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { EXTERNAL_RESOURCES, pathManager } from 'routes';

import { Box, Grow, Hidden } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Button from '@libs/ui/components/button';
import Typography, { OuterLink } from '@libs/ui/components/typography';

import { LocationState } from '../shared/types';
import { useJobData } from './hooks';

interface GreetingProps {
  link: string;
}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    maxWidth: 335,
    position: 'relative',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',

    '& .MuiTypography-paragraph': {
      letterSpacing: 'normal',
    },
  },
  title: {
    fontStyle: 'italic',
    lineHeight: '42px',
    marginBottom: theme.spacing(6),
  },
  titleEmoji: {
    fontStyle: 'normal',
  },
  link: {
    display: 'block',
    width: '100%',
  },
}));

const Greeting = ({ link }: GreetingProps) => {
  const classes = useStyles();
  const { data } = useJobData();
  const jobName = data?.job?.name;
  const jobId = data?.job?.id;
  const location = useLocation<LocationState>();
  const isFromLanding = !!location.state?.fromLanding;

  // NOTE: If talent comes from landing - skip some steps
  const finalRedirectPath = isFromLanding
    ? pathManager.talent.onboarding.creating.generatePath({ step: 1 })
    : link;

  const linkButton = (
    <Link className={classes.link} to={finalRedirectPath}>
      <Button
        data-test-id="greetingScreenSubmit"
        fullWidth
        color="primary"
        variant="contained"
      >
        START MY APPLICATION
      </Button>
    </Link>
  );

  return (
    <>
      <Box className={classes.wrapper}>
        <Grow in timeout={500}>
          <div>
            <Typography variant="h4" className={classes.title} fontWeight={600}>
              Welcome!
            </Typography>
            <Typography variant="body2" paragraph>
              We're thrilled to have you apply for the role of{' '}
              {jobId ? (
                <OuterLink
                  href={EXTERNAL_RESOURCES.jobPage({ id: jobId })}
                  {...{ target: '_blank' }}
                  whiteSpace="nowrap"
                  variant="body2"
                  color="primary"
                >
                  {jobName}
                </OuterLink>
              ) : (
                <Typography
                  component="span"
                  whiteSpace="nowrap"
                  variant="body2"
                  color="primary"
                >
                  {jobName}
                </Typography>
              )}
              . To apply you need to create an account on OpenTalent; Europe’s
              #1 talent-centric platform.
            </Typography>
            <Typography variant="body2" paragraph>
              On OpenTalent you can find new jobs, earn commissions, grow your
              network, and unlock amazing perks & benefits!
            </Typography>
            <Typography variant="body2" paragraph>
              Ready to make your mark?
            </Typography>

            <Hidden mdDown>
              <Box pt={4}>{linkButton}</Box>
            </Hidden>
          </div>
        </Grow>
      </Box>
      <Hidden mdUp>
        <FixedFooter>
          <Box width="100%" px={4}>
            {linkButton}
          </Box>
        </FixedFooter>
      </Hidden>
    </>
  );
};

export default Greeting;
