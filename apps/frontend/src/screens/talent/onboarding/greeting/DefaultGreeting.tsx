import FixedFooter from 'components/custom/onboarding/fixed-footer';
import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { pathManager } from 'routes';

import { Box, Grow, Hidden } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import { LocationState } from '../shared/types';

interface GreetingProps {
  link: string;
}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    maxWidth: 335,
    position: 'relative',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',

    '& .MuiTypography-paragraph': {
      letterSpacing: 'normal',
    },
  },
  title: {
    fontStyle: 'italic',
    lineHeight: '42px',
    marginBottom: theme.spacing(6),
  },
  titleEmoji: {
    fontStyle: 'normal',
  },
  link: {
    display: 'block',
    width: '100%',
  },
}));

const Greeting = ({ link }: GreetingProps) => {
  const classes = useStyles();
  const location = useLocation<LocationState>();
  const isFromLanding = !!location.state?.fromLanding;

  // NOTE: If talent comes from landing - skip some steps
  const finalRedirectPath = isFromLanding
    ? pathManager.talent.onboarding.creating.generatePath({ step: 1 })
    : link;

  const linkButton = (
    <Link className={classes.link} to={finalRedirectPath}>
      <Button
        data-test-id="greetingScreenSubmit"
        fullWidth
        color="primary"
        variant="contained"
        size="large"
      >
        START MY APPLICATION
      </Button>
    </Link>
  );

  return (
    <>
      <Box className={classes.wrapper}>
        <Grow in timeout={500}>
          <div>
            <Typography variant="h4" className={classes.title} fontWeight={600}>
              Welcome!
            </Typography>

            <Typography variant="body2" paragraph>
              We're excited to have you apply to Europe's premier talent-centric
              membership.
            </Typography>
            <Typography variant="body2" paragraph>
              At OpenTalent, we understand the challenges professionals face,
              and that's why we're here, dedicated to providing you with an edge
              as you build your business-of-one and career.
            </Typography>
            <Typography variant="body2" paragraph>
              To kickstart this journey, all you need to do is craft a strong
              profile. By doing so, we can verify your account and start
              curating opportunities just for you.
            </Typography>

            <Hidden mdDown>
              <Box pt={4}>{linkButton}</Box>
            </Hidden>
          </div>
        </Grow>
      </Box>
      <Hidden mdUp>
        <FixedFooter>
          <Box width="100%" px={4}>
            {linkButton}
          </Box>
        </FixedFooter>
      </Hidden>
    </>
  );
};

export default Greeting;
