import { LOCAL_STORAGE_KEYS } from 'consts/ls-keys';
import React from 'react';

import ApplicationGreeting from './ApplicationGreeting';
import DefaultGreeting from './DefaultGreeting';

interface GreetingProps {
  link: string;
}

const Greeting = ({ link }: GreetingProps) => {
  const isApplication = !!localStorage.getItem(
    LOCAL_STORAGE_KEYS.talentOnboardingAppliedJobId,
  );

  if (isApplication) {
    return <ApplicationGreeting link={link} />;
  }

  return <DefaultGreeting link={link} />;
};

export default Greeting;
