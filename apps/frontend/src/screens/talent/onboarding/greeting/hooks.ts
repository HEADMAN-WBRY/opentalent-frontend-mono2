import { gql, useQuery } from '@apollo/client';
import { LOCAL_STORAGE_KEYS } from 'consts/ls-keys';

import { Query } from '@libs/graphql-types';

export const GET_JOB_INFO = gql`
  query GetJob($id: ID!) {
    job(id: $id) {
      name
      id
    }
  }
`;

export const useJobData = () => {
  const jobId = localStorage.getItem(
    LOCAL_STORAGE_KEYS.talentOnboardingAppliedJobId,
  );
  return useQuery<Query>(GET_JOB_INFO, {
    variables: {
      id: jobId,
    },
    skip: !jobId,
  });
};
