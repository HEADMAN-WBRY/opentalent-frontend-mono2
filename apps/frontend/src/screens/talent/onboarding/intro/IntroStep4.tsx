import React from 'react';

import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import perksImg from './assets/feat4@2x.png';

const useStyles = makeStyles((theme) => ({
  title: {
    fontSize: '28px',
    lineHeight: '44px',
    fontStyle: 'italic',
  },
  image: {
    margin: '0 auto',

    [theme.breakpoints.down('md')]: {
      width: '100%',
    },

    '& img': {
      maxWidth: '100%',
    },
  },
}));

const IntroStep3 = () => {
  const classes = useStyles();

  return (
    <Box mt={4}>
      <Box pb={4}>
        <Typography variant="h4" className={classes.title} fontWeight={600}>
          4. Perks & Benefits
        </Typography>
      </Box>
      <Box pb={6} maxWidth={380} marginX="auto">
        <Typography variant="body2">
          The best deals in town for you. Save thousands with exclusive deals.
          New perks added weekly.
        </Typography>
      </Box>
      <Box className={classes.image}>
        {/* <Lottie animationData={(animationData as any).default} loop /> */}
        <img srcSet={`${perksImg} 2x`} alt="matches" />
      </Box>
    </Box>
  );
};

export default IntroStep3;
