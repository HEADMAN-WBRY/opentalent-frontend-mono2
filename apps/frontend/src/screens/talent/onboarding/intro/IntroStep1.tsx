import React from 'react';

import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import matchesImg from './assets/feat1@2x.png';

const useStyles = makeStyles((theme) => ({
  title: {
    fontSize: '28px',
    lineHeight: '44px',
    fontStyle: 'italic',
  },
  text: {},
  image: {
    textAlign: 'center',

    [theme.breakpoints.down('md')]: {
      width: '100%',
    },

    '& img': {
      maxWidth: '100%',
    },
  },
}));

const IntroStep1 = () => {
  const classes = useStyles();

  return (
    <div>
      <Box mt={4}>
        <Box pb={3}>
          <Typography variant="h4" fontWeight={600} className={classes.title}>
            1. Curated Job Search
          </Typography>
        </Box>
        <Box pb={6} maxWidth={350} marginX="auto">
          <Typography variant="body2">
            Leverage the power of ChatGPT to find jobs that perfectly match your
            skills and needs.
          </Typography>
        </Box>
        <div className={classes.image}>
          <img srcSet={`${matchesImg} 2x`} alt="matches" />
        </div>
      </Box>
    </div>
  );
};

export default IntroStep1;
