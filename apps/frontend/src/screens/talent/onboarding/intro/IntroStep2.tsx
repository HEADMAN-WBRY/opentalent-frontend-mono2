import React from 'react';

import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import image from './assets/feat2@2x.png';

const useStyles = makeStyles((theme) => ({
  title: {
    fontSize: '28px',
    lineHeight: '44px',
    fontStyle: 'italic',
    fontWeight: 600,
  },
  image: {
    margin: '0 auto',

    [theme.breakpoints.down('md')]: {
      width: '100%',
    },

    '& img': {
      maxWidth: '100%',
    },
  },
}));

const IntroStep4 = () => {
  const classes = useStyles();

  return (
    <Box mt={4}>
      <Box pb={4}>
        <Typography variant="h4" className={classes.title}>
          2. Elite Networking
        </Typography>
      </Box>
      <Box pb={6} maxWidth={310} marginX="auto">
        <Typography variant="body2">
          Meet new advisors, learn new skills, or find new colleagues on
          OpenTalent.
        </Typography>
      </Box>

      <Box>
        <img
          className={classes.image}
          srcSet={`${image} 2x`}
          alt="Skilled people"
        />
      </Box>
    </Box>
  );
};

export default IntroStep4;
