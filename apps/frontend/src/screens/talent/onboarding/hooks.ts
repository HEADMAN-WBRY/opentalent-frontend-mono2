import { LOCAL_STORAGE_KEYS } from 'consts/ls-keys';
import { useSearchParams } from 'hooks/routing';
import { useEffect, useState } from 'react';
import { mayBeAddDeviceId } from 'utils/common/mayBeAddDeviceId';

import { useCheckTalentExistsByEmailQuery } from '@libs/graphql-types';
import { isNil } from '@libs/helpers/common';

export const useTmpVarsFromQuery = () => {
  const { email, companyId, appliedJobId, origin, uid } = useSearchParams();

  const [finalEmail, setFinalEmail] = useState(
    localStorage.getItem(LOCAL_STORAGE_KEYS.talentOnboardingEmail) ||
    email ||
    '',
  );
  const [finalAppliedJobId, setAppliedJobId] = useState(
    localStorage.getItem(LOCAL_STORAGE_KEYS.talentOnboardingAppliedJobId) ||
    appliedJobId ||
    '',
  );
  const [finalOrigin, setFinalOrigin] = useState(
    localStorage.getItem(LOCAL_STORAGE_KEYS.talentOnboardingOrigin) || origin,
  );

  useEffect(() => {
    if (companyId && companyId !== undefined) {
      localStorage.setItem(
        LOCAL_STORAGE_KEYS.talentOnboardingCompanyId,
        companyId.toString(),
      );
    }

    if (!isNil(uid) && typeof uid === 'string') {
      localStorage.setItem(LOCAL_STORAGE_KEYS.talentInvitationUid, uid);
    }

    if (email && typeof email === 'string') {
      localStorage.setItem(LOCAL_STORAGE_KEYS.talentOnboardingEmail, email);
      setFinalEmail(email);
    }

    if (!!appliedJobId) {
      localStorage.setItem(
        LOCAL_STORAGE_KEYS.talentOnboardingAppliedJobId,
        appliedJobId as string,
      );
      setAppliedJobId(appliedJobId);
    }

    if (!!origin) {
      localStorage.setItem(
        LOCAL_STORAGE_KEYS.talentOnboardingOrigin,
        origin as string,
      );
      setFinalOrigin(origin);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return {
    origin: finalOrigin,
    email: finalEmail,
    appliedJobId: finalAppliedJobId,
  };
};

export const useDeviceId = () => {
  useEffect(() => {
    mayBeAddDeviceId(LOCAL_STORAGE_KEYS.talentOnboardingDeviceId);
  }, []);
};

export const useCheckTalentEmail = (talent_email: string) => {
  const { data, loading } = useCheckTalentExistsByEmailQuery({
    variables: { talent_email },
    skip: !talent_email,
  });

  return { loading, isTalentExist: data?.checkTalentExistsByEmail };
};
