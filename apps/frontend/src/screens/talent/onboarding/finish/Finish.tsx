import { ReactComponent as CheckIcon } from 'assets/icons/check.svg';
import { LOCAL_STORAGE_KEYS } from 'consts/ls-keys';
import React, { useEffect } from 'react';
import { RouteComponentProps } from 'react-router-dom';

import { Box, Grid, Grow } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    maxWidth: 870,
    position: 'relative',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    textAlign: 'center',
  },
  title: {
    fontStyle: 'italic',

    marginBottom: theme.spacing(4),

    [theme.breakpoints.down('sm')]: {
      lineHeight: '36px',
    },
  },
  titleText: {
    fontStyle: 'italic',
    fontWeight: 500,

    [theme.breakpoints.down('sm')]: {
      fontSize: 32,
    },
  },
  subTitle: {
    lineHeight: '24px',
    marginBottom: theme.spacing(12),
  },
  bottomSubTitle: {
    fontStyle: 'italic',
    marginTop: theme.spacing(4),
    fontWeight: 400,
    marginBottom: theme.spacing(15),
    whiteSpace: 'pre-line',
    maxWidth: 500,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  button: {
    maxWidth: 252,
    width: '100%',
    fontSize: '15px',
  },
  checkIcon: {
    width: 32,
    height: 32,
  },
}));

interface IProps extends RouteComponentProps { }

const Finish: React.FC<IProps> = () => {
  const classes = useStyles();
  useEffect(() => {
    localStorage.removeItem(LOCAL_STORAGE_KEYS.talentOnboardingEmail);
    localStorage.removeItem(LOCAL_STORAGE_KEYS.talentOnboardingCompanyId);
    localStorage.removeItem(LOCAL_STORAGE_KEYS.talentOnboardingAppliedJobId);
    localStorage.removeItem(LOCAL_STORAGE_KEYS.talentOnboardingOrigin);
  }, []);

  return (
    <Box className={classes.wrapper}>
      <Grow timeout={500} in>
        <div>
          <Grid container spacing={4} justifyContent="center">
            <Grid item>
              <Typography
                className={classes.title}
                variant="h4"
                fontWeight={600}
              >
                Verification required
              </Typography>
            </Grid>
            <Grid item>
              <CheckIcon className={classes.checkIcon} />
            </Grid>
          </Grid>

          <Typography variant="h6" className={classes.bottomSubTitle}>
            We sent you an email. Visit your inbox to verify your account and
            access OpenTalent.
          </Typography>
        </div>
      </Grow>
    </Box>
  );
};

export default Finish;
