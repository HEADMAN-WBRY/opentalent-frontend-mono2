/* eslint-disable react/no-danger */
import { Box, Paper } from '@mui/material';
import React from 'react';

import Typography from '@libs/ui/components/typography';

interface ClientMessageProps {
  message?: string;
}

const ClientMessage = ({ message = '' }: ClientMessageProps) => {
  return (
    <Paper elevation={0} square>
      <Box padding={4}>
        <Box pb={4}>
          <Typography variant="h6">Message from client</Typography>
        </Box>
        <Typography variant="body2" color="textSecondary">
          <span dangerouslySetInnerHTML={{ __html: message }} />
        </Typography>
      </Box>
    </Paper>
  );
};

export default ClientMessage;
