import { usePushWithQuery } from 'hooks/routing';
import { useMemo } from 'react';
import { useLocation } from 'react-router-dom';

import { debounce } from '@mui/material';
import { deepmerge } from '@mui/utils';

import { INITIAL_VALUES } from './consts';
import { getValuesFromQuerystring, mapValuesToQuery } from './utils';

export const useFilterSubmit = () => {
  const push = usePushWithQuery();
  const onSubmit = useMemo(() => {
    const handler = (values: typeof INITIAL_VALUES) => {
      const search = mapValuesToQuery(values);

      push({ query: search, replace: true });
    };
    return debounce(handler);
  }, [push]);

  return onSubmit;
};

export const useInitialFilterState = () => {
  const location = useLocation();
  const queryFilter = useMemo(() => {
    return getValuesFromQuerystring(location.search);
  }, [location.search]);
  const initialValues = useMemo(
    () => deepmerge(INITIAL_VALUES, queryFilter || {}, { clone: true }),
    [queryFilter],
  );

  return { initialValues };
};
