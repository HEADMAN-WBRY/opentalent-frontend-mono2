import { mergeDeep } from '@apollo/client/utilities';
import { BooleanModalState } from 'components/custom/skills-boolean-search';
import { useFormikContext } from 'formik';
import { useCallback } from 'react';

import { modelPath } from '@libs/helpers/form';

import { FilterValues } from '../../consts';

export const useToggleBooleanSearch = () => {
  const { values, setFieldValue } = useFormikContext<FilterValues>();
  const isBooleanSkills = values.isBooleanSkills || false;
  const onToggle = useCallback(() => {
    setFieldValue(
      modelPath<FilterValues>((m) => m.isBooleanSkills),
      !isBooleanSkills,
    );
  }, [isBooleanSkills, setFieldValue]);

  return { onToggle, isBooleanSkills };
};

export const useToggleBooleanSearchState = () => {
  const { values, setValues } = useFormikContext<FilterValues>();
  const booleanSearch = values.booleanSearch;

  const setBooleanState = useCallback(
    (state: BooleanModalState) => {
      const { booleanSearch, ...restValues } = values;
      const newState: FilterValues = mergeDeep(restValues, {
        booleanSearch: state,
        isBooleanSkills: true,
      });

      setValues(newState);
    },
    [setValues, values],
  );

  const clearBooleanSearch = useCallback(() => {
    const newState: FilterValues = mergeDeep(values, {
      booleanSearch: undefined,
      isBooleanSkills: true,
    });

    setValues(newState);
  }, [setValues, values]);

  return { setBooleanState, booleanSearch, clearBooleanSearch };
};
