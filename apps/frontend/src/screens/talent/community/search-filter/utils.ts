import {
  BooleanModalState,
  isBooleanSearchEmpty,
  mapBooleanToServer,
} from 'components/custom/skills-boolean-search';
import * as qs from 'utils/querystring';

import { QueryTalentsSearchArgs, SourceTypeEnum } from '@libs/graphql-types';
import { isNil } from '@libs/helpers/common';
import { OptionType } from '@libs/ui/components/form/select';

import { EUROPE_COUNTRY, FilterValues } from './consts';

export const stringifyBoolean = (booleanSearch?: BooleanModalState) => {
  return isBooleanSearchEmpty(booleanSearch)
    ? ''
    : JSON.stringify(booleanSearch);
};
const parseBoolean = (boolString: string) => {
  if (!boolString) {
    return undefined;
  }
  const parsed = JSON.parse(boolString);

  if (isBooleanSearchEmpty(parsed)) {
    return undefined;
  }

  return parsed;
};

export const mapFormValuesToQueryVariables = (
  values: Partial<FilterValues>,
): QueryTalentsSearchArgs => {
  const {
    search = '',
    category_ids = [],
    skills_ids = [],
    available_now,
    max_rate,
    min_rate,
    page,
    is_verification_required,
    booleanSearch,
    country,
    isBooleanSkills,

    worked_in_company_str,
    tags_ids,
  } = values;
  const categories = category_ids.map(({ value }) => String(value));
  const skills = skills_ids.map(({ value }) => String(value));
  const tags = tags_ids?.map(({ value }) => String(value)) || [];

  const skillsVariables =
    isBooleanSkills && booleanSearch
      ? {
        skills_boolean_filter: mapBooleanToServer(booleanSearch),
      }
      : (skills.length && { skills_ids: skills }) || {};

  return {
    country: country === EUROPE_COUNTRY ? undefined : country,

    is_active: true,
    worked_in_company_str,
    ...(search.length >= 3 && { search }),
    ...(!!categories.length && { category_ids: categories }),
    ...(!!page && { page }),
    ...(!!tags.length && { tags_ids: tags }),
    ...(available_now && { available_now: true }),
    ...(isNil(is_verification_required)
      ? {}
      : { is_verification_required: !!is_verification_required }),
    ...skillsVariables,
    source_type: SourceTypeEnum.Community,
    max_rate,
    min_rate,
  };
};

const mapOptionsToQuery = (opts: OptionType[]) =>
  opts.map(({ value, text, skill_type }) => {
    const arr = [value, text];
    return skill_type ? arr.concat(skill_type) : arr;
  });
export const mapValuesToQuery = (values: FilterValues) => {
  const { category_ids, skills_ids, isBooleanSkills, booleanSearch, tags_ids } =
    values;

  const params = {
    ...mapFormValuesToQueryVariables(values),
    ...(!!category_ids?.length && {
      category_ids: mapOptionsToQuery(category_ids),
    }),
    ...(!!tags_ids?.length && {
      tags_ids: mapOptionsToQuery(tags_ids),
    }),
    ...(!!skills_ids?.length && { skills_ids: mapOptionsToQuery(skills_ids) }),

    isBooleanSkills: isBooleanSkills ? '1' : '',
    booleanSearch: stringifyBoolean(booleanSearch),
  };
  return params;
};

const mapOptionsFromQuery = (
  str: string,
  { typeName }: { typeName?: string } = {},
) =>
  str?.split(';').map((s) => {
    const [value, text, type] = s?.split(',');
    return { value, text, ...(type && typeName && { [typeName]: type }) };
  });

export const getValuesFromQuerystring = (s: string): Partial<FilterValues> => {
  const {
    skills_ids,
    category_ids,
    is_active,
    search,
    min_rate,
    max_rate,
    available_now,
    page,
    is_verification_required,
    booleanSearch,
    isBooleanSkills,
    country,
    is_matcher,
    tags_ids,
    worked_in_company_str,
  } = qs.parse(s);

  return {
    worked_in_company_str: worked_in_company_str as string,
    search: search as string,
    min_rate: min_rate as number,
    max_rate: max_rate as number,
    country: (country as string) || EUROPE_COUNTRY,
    is_verification_required: !!is_verification_required,
    booleanSearch: parseBoolean(booleanSearch as string),
    isBooleanSkills: !!isBooleanSkills,

    available_now: !!available_now,
    page: page as number,

    is_matcher: !!is_matcher,

    ...(is_active === undefined
      ? { is_active: true, is_inactive: false }
      : { is_active: !!is_active, is_inactive: !is_active }),
    ...(!!category_ids && {
      category_ids: mapOptionsFromQuery(category_ids as string),
    }),
    ...(!!tags_ids && {
      tags_ids: mapOptionsFromQuery(tags_ids as string),
    }),
    ...(!!skills_ids && {
      skills_ids: mapOptionsFromQuery(skills_ids as string, {
        typeName: 'skill_type',
      }),
    }),
  };
};
