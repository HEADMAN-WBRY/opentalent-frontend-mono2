import { useCurrentUser } from 'hooks/auth';
import { useTalentAccountType } from 'hooks/talents/useTalentAccountType';
import React from 'react';
import { Link } from 'react-router-dom';
import { pathManager } from 'routes';
import { formatName } from 'utils/talent';

import { Box, Grid, Paper } from '@mui/material';

import {
  AccountTypeEnum,
  Talent,
  TalentSearchResult,
} from '@libs/graphql-types';

import CategoriesList from './CategoriesList';
import Highlights from './Highlights';
import LogoBlock from './LogoBlock';
import MainInfo from './MainInfo';
import TitleSection from './TitleSection';

interface TalentCardProps {
  talentSearch: TalentSearchResult;
  refetch: VoidFunction;
}

const TalentCard = (props: TalentCardProps) => {
  const { talentSearch, refetch } = props;
  const plan = useTalentAccountType();
  const { isTalent } = useCurrentUser();
  const talent = talentSearch?.talent as Talent;
  const highlights = talentSearch?.highlights || [];
  const name = formatName({
    firstName: talent.first_name_abac.value,
    lastName: talent.last_name_abac.value,
  });
  const hideHighlights = isTalent && plan === AccountTypeEnum.Free;
  const talentLink = pathManager.talent.viewOtherTalent.generatePath({
    id: talent.id,
  });

  return (
    <Link to={talentLink}>
      <Paper component={Box} p={6} data-test-id="talent-card" elevation={0}>
        <Grid wrap="nowrap" spacing={6} container>
          <Grid item>
            <LogoBlock name={name} talent={talent} />
          </Grid>
          <Grid flexGrow={1} item>
            <TitleSection refetch={refetch} talent={talent} name={name} />

            <MainInfo talent={talent} />

            <CategoriesList talent={talent} />
          </Grid>
        </Grid>

        {!!highlights.length && !hideHighlights && (
          <Box mt={3}>
            <Highlights items={highlights} />
          </Box>
        )}
      </Paper>
    </Link>
  );
};

export default TalentCard;
