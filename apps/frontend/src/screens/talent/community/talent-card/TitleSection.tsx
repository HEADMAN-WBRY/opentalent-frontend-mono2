import { ReactComponent as CheckIcon } from 'assets/icons/check.svg';
import cn from 'classnames';
import { NEW_TALENTS_FLAG_DAYS_COUNT } from 'consts/talents';
import { useIsCurrentTalentVerified } from 'hooks/talents/useTalentAccountType';
import React from 'react';
import { isNotOlderThan } from 'utils/common';

import { Grid, Box, Tooltip } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { Talent } from '@libs/graphql-types';
import Chip from '@libs/ui/components/chip';
import Typography from '@libs/ui/components/typography';

interface TitleSectionProps {
  name: string;
  talent: Talent;
  refetch: VoidFunction;
}

const useStyles = makeStyles((theme) => ({
  checkIcon: {
    transform: 'translateY(5px)',
    display: 'inline-block',
  },
  titleChip: {
    transform: 'translateY(-2px)',
  },
  greenChip: {
    backgroundColor: theme.palette.success.dark,
    color: 'white',
  },
  inviteButton: {
    position: 'relative',
    display: 'flex',
    justifyContent: 'flex-end',
  },
  pendingVerification: {
    backgroundColor: theme.palette.warning.dark,
    color: theme.palette.secondary.contrastText,
    borderRadius: '16px',
    padding: '0 6px',
    lineHeight: '20px',
    height: '20px',
    fontSize: '12px',
    marginLeft: theme.spacing(4),
  },
}));

const TitleSection = ({ name, talent }: TitleSectionProps) => {
  const classes = useStyles();
  const isTalentVerified = !!useIsCurrentTalentVerified();
  const isNewUser = talent?.created_at
    ? isNotOlderThan(talent?.created_at, NEW_TALENTS_FLAG_DAYS_COUNT)
    : false;

  const status = talent?.is_invitation_accepted ? (
    <CheckIcon className={classes.checkIcon} />
  ) : (
    <Chip
      className={classes.titleChip}
      label="pending activation"
      color="grey"
      size="small"
    />
  );

  return (
    <Grid container wrap="nowrap">
      <Grid item flexGrow={1}>
        <Box mb={1}>
          <Typography component="span" variant="h6">
            {name}
          </Typography>

          <Box display="inline-block" ml={2}>
            {status}
          </Box>

          {talent?.is_matcher && (
            <Box display="inline-block" ml={2}>
              <Chip
                size="small"
                label="Talent Matcher"
                className={classes.titleChip}
              />
            </Box>
          )}

          {isNewUser && (
            <Tooltip
              title={`This person joined in the last two days, say hi!`}
              followCursor
            >
              <Box display="inline-block" ml={2}>
                <Chip
                  size="small"
                  label="New member"
                  color="success"
                  className={cn(classes.titleChip, classes.greenChip)}
                />
              </Box>
            </Tooltip>
          )}

          {!isTalentVerified && (
            <Box display="inline-block" ml={2}>
              <Chip
                size="small"
                label="pending verification"
                color="tertiary"
                className={classes.titleChip}
              />
            </Box>
          )}
        </Box>
        <Box mb={1}>
          <Typography style={{ wordBreak: 'break-word' }} variant="body2">
            {talent.recent_position_title}
          </Typography>
        </Box>
      </Grid>
    </Grid>
  );
};

export default TitleSection;
