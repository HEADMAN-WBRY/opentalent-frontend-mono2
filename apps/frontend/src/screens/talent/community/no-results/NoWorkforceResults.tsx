import React from 'react';

import { Box } from '@mui/material';

import Typography from '@libs/ui/components/typography';

interface NoWorkforceResultsProps { }

export const NoWorkforceResults = (props: NoWorkforceResultsProps) => {
  return (
    <Box maxWidth="570px">
      <Typography paragraph variant="h6">
        There' no one here yet!
      </Typography>
    </Box>
  );
};
