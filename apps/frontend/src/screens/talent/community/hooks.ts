import { useQuery } from '@apollo/client';
import { useSearchParams } from 'hooks/routing';
import { useSnackbar } from 'notistack';
import { useCallback, useEffect, useMemo } from 'react';
import * as qs from 'utils/querystring';

import { Query, QueryTalentsSearchArgs } from '@libs/graphql-types';
import { isNumber } from '@libs/helpers/common';

import { SEARCH_TALENTS } from './queries';
import {
  getValuesFromQuerystring,
  mapFormValuesToQueryVariables,
} from './search-filter/utils';

export const useTalentsRequest = (anchorRef: any) => {
  const { enqueueSnackbar } = useSnackbar();
  const searchParams = useSearchParams();
  const parsedSearch = useMemo(
    () =>
      mapFormValuesToQueryVariables(
        getValuesFromQuerystring(qs.stringify(searchParams)),
      ) || {},
    [searchParams],
  ) as QueryTalentsSearchArgs;

  const { loading, fetchMore, refetch, data } = useQuery<
    Query,
    QueryTalentsSearchArgs
  >(SEARCH_TALENTS, {
    nextFetchPolicy: 'network-only',
    variables: parsedSearch,
    onCompleted: async (data) => {
      const { current_page, last_page } =
        data?.talentsSearch?.custom_paginator_info || {};

      if (!isNumber(current_page) || !isNumber(last_page)) {
        return;
      }

      if (current_page > last_page) {
        anchorRef.current.scrollIntoView({ behavior: 'smooth' });
        refetch?.({ ...parsedSearch, page: last_page });
      }
    },
    onError: (error) => {
      enqueueSnackbar(error.message, {
        variant: 'error',
        preventDuplicate: true,
      });
    },
  });
  const onPaginationChange = useCallback(
    async (e: React.ChangeEvent<unknown>, page: number) => {
      anchorRef.current.scrollIntoView({ behavior: 'smooth' });
      await refetch({ ...parsedSearch, page });
    },
    [anchorRef, parsedSearch, refetch],
  );

  useEffect(() => {
    refetch(parsedSearch);
  }, [fetchMore, refetch, parsedSearch]);

  return {
    data,
    loading,
    onPaginationChange,
    parsedSearch,
    refetch,
  };
};
