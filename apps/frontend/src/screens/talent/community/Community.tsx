import { ReactComponent as InvitesIcon } from 'assets/icons/invites.svg';
import {
  CreateCampaignModal,
  InviteByCompanyModal,
} from 'components/custom/talent/modals';
import { TalentPremiumBanner } from 'components/custom/talent/premium';
import { ConnectedPageLayout } from 'components/layout/page-layout';
import { INFINITY_SIGN } from 'consts/common';
import { Formik } from 'formik';
import useMediaQueries from 'hooks/common/useMediaQueries';
import { useIsPaidTalentAccount } from 'hooks/talents/useTalentAccountType';
import React, { useRef, useState } from 'react';
import { pathManager } from 'routes';
import { isNumber } from 'utils/common';

import AddIcon from '@mui/icons-material/Add';
import TuneIcon from '@mui/icons-material/Tune';
import { Pagination } from '@mui/lab';
import {
  Box,
  CircularProgress,
  Grid,
  Hidden,
  SwipeableDrawer,
} from '@mui/material';

import { formatNumberSafe } from '@libs/helpers/format';
import Button, { RouterButton } from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import { useTalentsRequest } from './hooks';
import NoResults from './no-results';
import SearchFilter from './search-filter';
import { useFilterSubmit, useInitialFilterState } from './search-filter/hooks';
import validator from './search-filter/validator';
import useStyles from './styles';
import TalentCard from './talent-card';

export interface WorkforceProps { }

export interface StyledWorkforceProps extends WorkforceProps {
  isEmpty: boolean;
}

const Workforce = (props: WorkforceProps) => {
  const anchorRef = useRef<HTMLDivElement>(null);
  const [isDrawerOpen, setIsDrawerOpen] = useState(false);
  const { isSM } = useMediaQueries();
  const { data, loading, parsedSearch, refetch } = useTalentsRequest(anchorRef);

  const lastPage = data?.talentsSearch?.custom_paginator_info?.last_page;
  const currentPage = data?.talentsSearch?.custom_paginator_info?.current_page;
  const totalTalentsCount = data?.talentsSearch?.custom_paginator_info?.total;
  const talents = data?.talentsSearch?.data || [];
  const isEmpty = talents.length === 0;
  const pagesIsRight =
    isNumber(currentPage) && isNumber(lastPage) && currentPage <= lastPage;

  const isPremium = useIsPaidTalentAccount();
  const classes = useStyles({ ...props, isEmpty });

  const { initialValues } = useInitialFilterState();
  const onSubmit = useFilterSubmit();

  return (
    <Formik
      onSubmit={onSubmit}
      initialValues={initialValues}
      validationSchema={validator}
    >
      {({ setFieldValue, submitForm, values }) => {
        return (
          <ConnectedPageLayout
            headerProps={{ accountProps: {} }}
            drawerProps={{}}
            documentTitle="Search for Talent"
          >
            <div ref={anchorRef} />
            <Box className={classes.container}>
              <Grid
                className={classes.fullHeight}
                container
                direction="column"
                justifyContent="space-between"
              >
                <Grid item>
                  <Grid
                    container
                    justifyContent="space-between"
                    alignItems="center"
                  >
                    <Grid item>
                      <Box mb={2}>
                        <Typography variant="h5">
                          Connect with the OpenTalent Community 🎉
                        </Typography>
                      </Box>
                    </Grid>
                    <Grid className={classes.titleButtons} item>
                      <RouterButton
                        to={pathManager.talent.invite.generatePath()}
                        color="info"
                        variant="outlined"
                        startIcon={<InvitesIcon />}
                      >
                        Invite someone
                      </RouterButton>
                      <Hidden mdUp>
                        <Box display="flex" alignItems="center">
                          <Button
                            onClick={() => setIsDrawerOpen(true)}
                            color="inherit"
                            endIcon={<TuneIcon />}
                          >
                            Search
                          </Button>
                        </Box>
                      </Hidden>
                    </Grid>
                  </Grid>
                  <Box className={classes.form}>
                    <Grid container spacing={4}>
                      <Grid className={classes.filterContainer} item md={5}>
                        <SwipeableDrawer
                          classes={{
                            paper: classes.settingsDrawer,
                          }}
                          anchor="right"
                          variant={isSM ? 'temporary' : 'permanent'}
                          open={isDrawerOpen}
                          onClose={() => setIsDrawerOpen(false)}
                          onOpen={() => setIsDrawerOpen(true)}
                        >
                          <SearchFilter
                            closeDrawer={() => setIsDrawerOpen(false)}
                          />
                        </SwipeableDrawer>
                      </Grid>
                      <Grid item md={7} sm={12}>
                        <Typography variant="subtitle1" paragraph>
                          <Typography
                            variant="subtitle1"
                            color="info.main"
                            component="span"
                          >
                            {formatNumberSafe(totalTalentsCount, {
                              fallback: INFINITY_SIGN,
                            })}
                          </Typography>{' '}
                          active professionals
                        </Typography>

                        {!isPremium && (
                          <Box mb={4}>
                            <TalentPremiumBanner />
                          </Box>
                        )}

                        {talents?.map((item: any) => (
                          <Box key={item.talent.id} mb={4}>
                            <TalentCard talentSearch={item} refetch={refetch} />
                          </Box>
                        ))}
                        {(loading || !pagesIsRight) && (
                          <Box
                            display="flex"
                            alignItems="center"
                            justifyContent="center"
                          >
                            <CircularProgress color="secondary" />
                          </Box>
                        )}
                        {isEmpty && !loading && pagesIsRight && (
                          <NoResults search={parsedSearch} />
                        )}
                        {!isEmpty && !loading && (
                          <Box display="flex" justifyContent="center" mt={4}>
                            <Pagination
                              page={currentPage}
                              showFirstButton
                              showLastButton
                              count={lastPage}
                              variant="outlined"
                              shape="rounded"
                              onChange={(e, page) => {
                                setFieldValue('page', page);
                                submitForm();
                                if (anchorRef?.current) {
                                  anchorRef?.current?.scrollIntoView({
                                    behavior: 'smooth',
                                  });
                                }
                              }}
                            />
                          </Box>
                        )}
                      </Grid>
                    </Grid>
                  </Box>
                </Grid>
              </Grid>
              <Hidden smUp>
                <Button
                  size="large"
                  variant="contained"
                  color="primary"
                  href={pathManager.company.createProfile.generatePath()}
                  classes={{
                    root: classes.mbInviteButton,
                  }}
                  startIcon={<AddIcon />}
                >
                  <Typography>Invite</Typography>
                </Button>
              </Hidden>
            </Box>
            <InviteByCompanyModal />
            <CreateCampaignModal />
          </ConnectedPageLayout>
        );
      }}
    </Formik>
  );
};

export default Workforce;
