import { gql } from '@apollo/client';
import { ABAC_PROPERTIES } from 'graphql/fragments/talent/abacFields';

export const SEARCH_TALENTS = gql`
  ${ABAC_PROPERTIES}
  query GetTalents(
    $search: String
    $country: String
    $category_ids: [ID]
    $skills_ids: [ID]
    $max_rate: Float
    $min_rate: Float
    $source_type: SourceTypeEnum
    $is_active: Boolean
    $is_matcher: Boolean
    $is_verification_required: Boolean
    $worked_in_company_str: String
    $first: Int = 10
    $page: Int
    $available_now: Boolean
    $tags_ids: [ID]
    $skills_boolean_filter: TalentsBooleanSkillsFilterInput
  ) {
    talentsSearch(
      country: $country
      worked_in_company_str: $worked_in_company_str
      search_string: $search
      category_ids: $category_ids
      skills_ids: $skills_ids
      max_rate: $max_rate
      min_rate: $min_rate
      source_type: $source_type
      is_active: $is_active
      first: $first
      available_now: $available_now
      page: $page
      is_verification_required: $is_verification_required
      skills_boolean_filter: $skills_boolean_filter
      is_matcher: $is_matcher
      tags_ids: $tags_ids
    ) {
      custom_paginator_info {
        current_page
        last_page
        per_page
        total
      }
      data {
        highlights {
          source
          text
        }
        talent {
          id
          reminded_at
          created_at
          is_invitation_accepted
          is_verification_required
          created_at
          first_name_abac {
            ...AbacProps
          }
          last_name_abac {
            ...AbacProps
          }
          address
          rate
          recent_position_title
          email_abac {
            ...AbacProps
          }
          available_now
          location
          is_ot_pool
          category {
            id
            name
          }
          subcategories {
            id
            name
          }
          is_matcher
          source {
            id
          }
          avatar {
            avatar
          }
          talent_data {
            phone
          }
        }
      }
    }
  }
`;
