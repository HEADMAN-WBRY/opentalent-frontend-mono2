import { makeStyles } from '@mui/styles';

import { StyledWorkforceProps } from './Community';

const useStyles = makeStyles((theme) => ({
  fullHeight: {
    height: '100%',
  },
  container: {
    height: '100%',
    [theme.breakpoints.down('md')]: {
      marginBottom: ({ isEmpty }: StyledWorkforceProps) =>
        isEmpty ? 0 : theme.spacing(22),
    },
  },
  titleButtons: {
    display: 'flex',
    justifyContent: 'space-between',
    flexWrap: 'nowrap',

    [theme.breakpoints.down('md')]: {
      width: '100%',
      maxWidth: 744,
      paddingTop: theme.spacing(2),
    },
  },
  form: {
    paddingTop: 32,
  },
  inviteButton: {
    color: theme.palette.text.primary ?? 'rgba(0, 0, 0, 0.87)',
  },
  mbInviteButton: {
    color: theme.palette.text.primary ?? 'rgba(0, 0, 0, 0.87)',
    borderRadius: 24,
    position: 'fixed',
    zIndex: theme.zIndex.speedDial,
    bottom: 10,
    left: '50%',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
  filterContainer: {
    position: 'relative',
  },
  settingsDrawer: {
    position: 'static',
    borderLeft: 'none',

    [theme.breakpoints.down('md')]: {
      position: 'fixed',
      width: '40%',
      top: 40,
    },
    [theme.breakpoints.down('sm')]: {
      width: '90%',
    },
  },
}));

export default useStyles;
