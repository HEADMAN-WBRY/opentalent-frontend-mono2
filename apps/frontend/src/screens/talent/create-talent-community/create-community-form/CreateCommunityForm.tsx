import { Formik } from 'formik';
import React from 'react';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { modelPath } from '@libs/helpers/form';
import Button from '@libs/ui/components/button';
import { ConnectedCheckbox } from '@libs/ui/components/form/checkbox';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import Typography from '@libs/ui/components/typography';

import { FormState } from './types';

interface PendingStaticPopupProps { }

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    transform: 'translateY(-36px)',

    [theme.breakpoints.down('sm')]: {
      transform: 'none',
    },
  },
  card: {
    overflow: 'hidden',
    width: 604,
    borderRadius: 24,
    boxShadow:
      '0px 8px 10px -5px rgba(0, 0, 0, 0.12), 0px 16px 24px 2px rgba(0, 0, 0, 0.12), 0px 6px 30px 5px rgba(0, 0, 0, 0.08)',

    [theme.breakpoints.down('sm')]: {
      width: '100%',
      borderRadius: 0,
    },
  },
  content: {
    padding: theme.spacing(10, 14),
    background: 'white',

    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing(6, 4),
    },
  },
  label: {
    fontSize: 14,
    lineHeight: '20px',
  },
}));

export const CreateCommunityForm = (props: PendingStaticPopupProps) => {
  const classes = useStyles();

  return (
    <Formik<FormState>
      initialValues={{ companyName: '', url: '', consent: false }}
      onSubmit={console.log}
    >
      <div className={classes.root}>
        <div className={classes.card}>
          <div className={classes.content}>
            <Typography variant="h5" textAlign="center" paragraph>
              Let's set up your Community
            </Typography>
            <Box mt={4} mb={6}>
              <ConnectedTextField
                fullWidth
                size="small"
                variant="filled"
                label="Company name"
                name={modelPath<FormState>((m) => m.companyName)}
              />
            </Box>
            <Box mb={6}>
              <Typography variant="subtitle1">OpenTalent public URL</Typography>
              <Grid container spacing={2}>
                <Grid item display="flex" alignItems="center">
                  <Typography variant="body2">
                    opentalent.co/community/
                  </Typography>
                </Grid>
                <Grid item flexGrow={1}>
                  <ConnectedTextField
                    fullWidth
                    size="small"
                    variant="outlined"
                    name={modelPath<FormState>((m) => m.url)}
                  />
                </Grid>
              </Grid>
            </Box>
            <Box mb={8}>
              <ConnectedCheckbox
                formControlLabelProps={{
                  classes: { label: classes.label },
                }}
                label="I verify that I am the official representative of this company and have the right to act on behalf of the company in the creation of this page."
                name={modelPath<FormState>((m) => m.consent)}
              />
            </Box>

            <Box display="flex" justifyContent="center">
              <Button color="info" variant="contained">
                REQUEST ACCESS
              </Button>
            </Box>
          </div>
        </div>
      </div>
    </Formik>
  );
};
