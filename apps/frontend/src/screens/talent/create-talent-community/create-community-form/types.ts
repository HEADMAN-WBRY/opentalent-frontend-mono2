export interface FormState {
  companyName: string;
  url: string;
  consent: boolean;
}
