import React from 'react';

import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import image from './pending-popup@2x.png';

interface PendingStaticPopupProps { }

const useStyles = makeStyles((theme) => ({
  root: {
    overflow: 'hidden',
    maxWidth: 490,
    borderRadius: 24,
    boxShadow:
      '0px 8px 10px -5px rgba(0, 0, 0, 0.12), 0px 16px 24px 2px rgba(0, 0, 0, 0.12), 0px 6px 30px 5px rgba(0, 0, 0, 0.08)',
  },
  imageWrap: {
    height: 230,
    overflow: 'hidden',

    '& img': {
      width: '100%',
      objectFit: 'contain',
    },
  },
  content: {
    padding: theme.spacing(8, 6),
    background: 'white',
  },
}));

export const PendingStaticPopup = (props: PendingStaticPopupProps) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.imageWrap}>
        <img srcSet={`${image} 2x`} alt="Banner" />
      </div>
      <div className={classes.content}>
        <Typography variant="h5" textAlign="center" paragraph>
          Your account is pending verification
        </Typography>
        <Typography textAlign="center">
          Please hold on as we verify your account. Account verification is a
          crucial step to maintain the quality and security of our community.
        </Typography>
      </div>
    </div>
  );
};
