import { ConnectedPageLayout } from 'components/layout/page-layout';
import { useIsCurrentTalentVerified } from 'hooks/talents/useTalentAccountType';
import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { pathManager } from 'routes';

import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { PendingStaticPopup } from './PendingStaticPopup';

const useStyles = makeStyles(() => ({
  contentWrapper: {
    overflow: 'hidden',
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
    maxWidth: '100%',
  },
  popup: {},
}));

const PendingVerification = () => {
  const classes = useStyles();
  const history = useHistory();
  const isTalentVerified = useIsCurrentTalentVerified();

  useEffect(() => {
    if (isTalentVerified) {
      history.push(pathManager.auth.generatePath());
    }
  }, [history, isTalentVerified]);

  return (
    <ConnectedPageLayout
      classes={{ contentWrapper: classes.contentWrapper }}
      documentTitle="Pending verification"
      headerProps={{
        accountProps: {},
      }}
    >
      <Box>
        <PendingStaticPopup />
      </Box>
    </ConnectedPageLayout>
  );
};

export default PendingVerification;
