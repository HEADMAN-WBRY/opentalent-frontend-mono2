import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import React from 'react';

import Spoiler from '@libs/ui/components/spoiler';
import Typography from '@libs/ui/components/typography';

import useClasses from '../styles';

interface AboutProps {
  children?: string;
}

const About = (props: AboutProps) => {
  const classes = useClasses(props);
  const { children } = props;
  return (
    <Card elevation={0} variant="outlined">
      <CardContent>
        <Typography className={classes.title} variant="h6">
          About
        </Typography>
        <Spoiler
          typographyProps={{
            className: classes.aboutText,
            variant: 'body2',
            color: 'textSecondary',
          }}
          wordsCount={30}
        >
          {children}
        </Spoiler>
      </CardContent>
    </Card>
  );
};

export default About;
