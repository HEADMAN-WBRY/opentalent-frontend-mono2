import { useSnackbar } from 'notistack';
import React, { useCallback } from 'react';
import { useCopyToClipboard } from 'react-use';

import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import { Grid, IconButton, Tooltip, Typography } from '@mui/material';
import { grey } from '@mui/material/colors';

import { stopEvent } from '@libs/helpers/common';

interface ContactRowProps {
  onRowClick: VoidFunction;
  icon: React.ReactNode;
  text?: string;
  content?: React.ReactNode;
}

const ContactRow = ({ onRowClick, icon, text, content }: ContactRowProps) => {
  const { enqueueSnackbar } = useSnackbar();
  const [, copyToClipboard] = useCopyToClipboard();
  const onCopy = useCallback(
    (text: string) => {
      copyToClipboard(text);
      enqueueSnackbar('Contact copied', { variant: 'success' });
    },
    [copyToClipboard, enqueueSnackbar],
  );

  return (
    <Grid
      style={{
        cursor: 'pointer',
      }}
      onClick={onRowClick}
      spacing={2}
      container
      wrap="nowrap"
    >
      <Grid
        style={{
          minWidth: 30,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          color: grey[700],
        }}
        item
      >
        {icon}
      </Grid>
      <Grid style={{ width: 'calc(100% - 68px)' }} item>
        {!!text ? (
          <Typography
            style={{
              textOverflow: 'ellipsis',
              overflow: 'hidden',
              width: '100%',
              whiteSpace: 'nowrap',
            }}
            variant="subtitle1"
            color="textSecondary"
          >
            {text}
          </Typography>
        ) : (
          content
        )}
      </Grid>
      {!!text && (
        <Grid onClick={stopEvent} item>
          <Tooltip title="Copy">
            <IconButton onClick={() => onCopy(text)} size="small">
              <ContentCopyIcon fontSize="small" />
            </IconButton>
          </Tooltip>
        </Grid>
      )}
    </Grid>
  );
};

export default ContactRow;
