/* eslint-disable react/no-array-index-key */
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Grid from '@mui/material/Grid';
import { ReactComponent as FacebookIcon } from 'assets/icons/facebook.svg';
import { ReactComponent as LinkedInIcon } from 'assets/icons/linked-in.svg';
import { ReactComponent as PhoneIcon } from 'assets/icons/phone.svg';
import React from 'react';

import Typography from '@libs/ui/components/typography';

import useClasses from '../styles';

interface VerificationProps {}

const Verification = (props: VerificationProps) => {
  const classes = useClasses(props);

  return (
    <Card elevation={0} variant="outlined">
      <CardContent>
        <Typography className={classes.title} variant="h5">
          Verification
        </Typography>
        <Grid spacing={2} alignContent="flex-end" direction="column" container>
          <Grid alignItems="center" item container>
            <Typography variant="captionSmall" color="textPrimary">
              Verified by
            </Typography>
            <FacebookIcon className={classes.grayIcon} />
            &nbsp;
            <LinkedInIcon className={classes.grayIcon} />
            &nbsp;
            <PhoneIcon className={classes.grayIcon} />
          </Grid>
          <Grid item>
            <Typography variant="body2" color="tertiary">
              Request the legal ID
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant="body2" color="tertiary">
              Request the NDA
            </Typography>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

export default Verification;
