/* eslint-disable react/no-array-index-key */
import { ReactComponent as LinkedInIcon } from 'assets/icons/linked-in.svg';
import { useOpenTalentPremiumModal } from 'components/custom/talent/modals/limited-profile-access';
import { useIsPaidTalentAccount } from 'hooks/talents/useTalentAccountType';
import React, { useCallback } from 'react';

// import MailIcon from '@mui/icons-material/Mail';
// import PhoneAndroidIcon from '@mui/icons-material/PhoneAndroid';
import { Card, CardContent, Box } from '@mui/material';

import { Talent } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import useClasses from '../../styles';
import ContactRow from './ContactRow';

interface SocialProps {
  talent: Partial<Talent> | null;
}

const useLinkClick = () => {
  return useCallback((link: string) => {
    window.open(link, '__blank');
  }, []);
};

const Social = (props: SocialProps) => {
  const classes = useClasses(props);
  const { talent } = props;
  const canShowContacts = useIsPaidTalentAccount();
  const openLimitedProfileAccessModal = useOpenTalentPremiumModal();
  // const openRecruiterSubscriptionModal = useOpenRecruiterSubscriptionModal();

  const onLinkClick = useLinkClick();

  const linkedinAbac = talent?.talent_data?.linkedin_profile_link_abac;
  // const phoneAbac = talent?.talent_data?.phone;
  // const emailAbac = talent?.email_abac;

  return (
    <Card elevation={0} variant="outlined">
      <CardContent>
        <Typography className={classes.title} noWrap variant="h6">
          Social links and contacts
        </Typography>

        {canShowContacts && (
          <Box
            style={{
              display: canShowContacts ? 'block' : 'flex',
            }}
          >
            {!!linkedinAbac?.value && (
              <ContactRow
                onRowClick={() => onLinkClick(linkedinAbac.value!)}
                icon={<LinkedInIcon style={{ marginBottom: 4 }} />}
                text={linkedinAbac?.value}
              />
            )}
            {/* {!!emailAbac?.is_presented && (
              <ContactRow
                onRowClick={noop}
                icon={<MailIcon />}
                content={
                  <Button
                    onClick={() => openRecruiterSubscriptionModal()}
                    variant="text"
                    color="info"
                  >
                    show email
                  </Button>
                }
              />
            )}
            {!!phoneAbac && (
              <ContactRow
                onRowClick={noop}
                icon={<PhoneAndroidIcon />}
                content={
                  <Button
                    onClick={() => openRecruiterSubscriptionModal()}
                    variant="text"
                    color="info"
                  >
                    show phone number
                  </Button>
                }
              />
            )} */}
          </Box>
        )}
        {!canShowContacts && (
          <Button
            onClick={() => openLimitedProfileAccessModal()}
            color="secondary"
            variant="outlined"
          >
            Contact info
          </Button>
        )}
      </CardContent>
    </Card>
  );
};

export default Social;
