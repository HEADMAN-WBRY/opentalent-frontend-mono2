/* eslint-disable react/no-array-index-key */
import { useOpenTalentPremiumModal } from 'components/custom/talent/modals/limited-profile-access';
import { useIsPaidTalentAccount } from 'hooks/talents/useTalentAccountType';
import React from 'react';

import { Button } from '@mui/material';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Grid from '@mui/material/Grid';

import { Talent } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import useClasses from '../styles';

interface DocumentsProps {
  talent: Partial<Talent>;
}

const Documents = (props: DocumentsProps) => {
  const { talent } = props;
  const classes = useClasses(props);
  const openLimitedAccessModal = useOpenTalentPremiumModal();
  const canShowContacts = useIsPaidTalentAccount();

  return (
    <Card elevation={0} variant="outlined">
      <CardContent>
        <Typography className={classes.title} variant="h6">
          Resume
        </Typography>

        {canShowContacts && (
          <Grid spacing={2} direction="column" container>
            {talent?.documents?.map((doc) => (
              <Grid
                title={doc?.title}
                className={classes.ellipsisContainer}
                key={doc?.hash}
                item
              >
                <a href={doc?.url} target="_blank" rel="noreferrer" download>
                  <Typography
                    className={classes.ellipsis}
                    variant="body2"
                    color="tertiary"
                  >
                    {doc?.title}
                  </Typography>
                </a>
              </Grid>
            ))}
          </Grid>
        )}
        {!canShowContacts && (
          <Button
            onClick={() => openLimitedAccessModal()}
            color="secondary"
            variant="outlined"
          >
            Show cv
          </Button>
        )}
      </CardContent>
    </Card>
  );
};

export default Documents;
