import { makeStyles } from '@mui/styles';

import { Theme } from '@libs/ui/themes/default';

const useStyles = makeStyles((theme: Theme) => ({
  title: {
    paddingBottom: theme.spacing(3),
  },
  availabilityInfoIcon: {
    transform: 'translateY(3px)',
  },
  grayIcon: {
    color: theme.palette.text.secondary,
  },
  aboutText: {
    wordBreak: 'break-word',
  },

  ellipsisContainer: {
    width: '100%',
  },

  ellipsis: {
    maxWidth: '100%',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
}));

export default useStyles;
