import { makeStyles } from '@mui/styles';

import { Theme } from '@libs/ui/themes/default';

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    position: 'relative',
    [theme.breakpoints.down('sm')]: {
      paddingTop: theme.spacing(17),
      background: `linear-gradient(180deg, ${theme.palette.grey[200]} 130px, ${theme.palette.grey[50]} 0%)`,
    },
  },
}));

export default useStyles;
