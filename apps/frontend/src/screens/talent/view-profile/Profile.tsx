import {
  CreateCampaignModal,
  InviteByCompanyModal,
} from 'components/custom/talent/modals';
import { ConnectedPageLayout } from 'components/layout/page-layout';
import useMediaQueries from 'hooks/common/useMediaQueries';
import React from 'react';
import { ChatWithTalentModal } from 'screens/profile/modals/chat-with-talent';

import Grid from '@mui/material/Grid';

import { Tag } from '@libs/graphql-types';

import { useProfileInfo } from './hooks';
import ProfileContent from './profile-content';
import ProfileHeader from './profile-header';
import ProfileSidebar from './profile-sidebar';
import useStyles from './styles';
import { ProfileScreenProps } from './types';

const Profile = (props: ProfileScreenProps) => {
  const talentId = props.match?.params?.id;

  const { isSM } = useMediaQueries();
  const classes = useStyles(props);
  const {
    loading,
    talent: talentData,
    fetchTalent,
    data,
  } = useProfileInfo(talentId);
  const companyTags = (data?.currentUserCompanyTags as Tag[]) || [];

  return (
    <ConnectedPageLayout
      isLoading={loading}
      headerProps={{ accountProps: {} }}
      documentTitle="Profile"
      drawerProps={{}}
    >
      {!!talentData && (
        <Grid direction="column" container className={classes.container}>
          <ProfileHeader
            talent={talentData as any}
            refetchTalent={fetchTalent}
          />
          <Grid item>
            <Grid
              container
              direction={isSM ? 'column-reverse' : 'row'}
              spacing={4}
            >
              <Grid md={4} item>
                <ProfileSidebar talent={talentData as any} />
              </Grid>
              <Grid md={8} item>
                <ProfileContent
                  talent={talentData as any}
                  companyTags={companyTags}
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      )}

      <InviteByCompanyModal />
      <CreateCampaignModal />
      <ChatWithTalentModal />
    </ConnectedPageLayout>
  );
};

export default Profile;
