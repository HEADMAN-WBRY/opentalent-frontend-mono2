import { useMutation, useQuery } from '@apollo/client';
import { CREATE_CHAT_MESSAGES } from 'graphql/chat';
import { useCallback } from 'react';

import {
  Mutation,
  MutationCreateChatMessageArgs,
  Query,
  QueryAtsRecordsArgs,
  Talent,
} from '@libs/graphql-types';

import { GET_ATS_RECORDS } from './queries';

export const useATSRecords = (talentID: string) => {
  const { loading, data } = useQuery<Query, QueryAtsRecordsArgs>(
    GET_ATS_RECORDS,
    { variables: { talent_id: talentID }, fetchPolicy: 'network-only' },
  );
  const records = [...(data?.ATSRecords || [])].reverse();

  return {
    records,
    isLoading: loading,
  };
};

export const useCreateComment = (talent: Talent) => {
  const talentId = talent?.id;
  const [createCommentAction, { loading }] = useMutation<
    Mutation,
    MutationCreateChatMessageArgs
  >(CREATE_CHAT_MESSAGES, {
    refetchQueries: [
      { query: GET_ATS_RECORDS, variables: { talent_id: talentId } },
    ],
  });

  const createComment = useCallback(
    (message: string) =>
      createCommentAction({
        variables: {
          talent_id: talentId,
          message,
        },
      }),
    [createCommentAction, talentId],
  );

  return { createComment, isLoading: loading };
};
