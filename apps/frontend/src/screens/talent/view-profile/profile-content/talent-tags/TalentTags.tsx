import { Form, Formik } from 'formik';
import React, { useMemo } from 'react';

import InfoIcon from '@mui/icons-material/Info';
import { Box, Paper, Tooltip } from '@mui/material';

import { Tag, Talent } from '@libs/graphql-types';
import FormikAutoSave from '@libs/ui/components/form/formik/FormikAutoSave';
import { ConnectedMultipleSelect } from '@libs/ui/components/form/multiple-select';
import { OptionType } from '@libs/ui/components/form/select';
import Typography from '@libs/ui/components/typography';

import { useSubmitHandler } from './hooks';
import { IFormState } from './types';

interface TalentTagsProps {
  companyTags: Tag[];
  talent?: Partial<Talent> | null;
}

const mapTagToOption = (tag: Tag): OptionType => ({
  text: tag.name,
  value: tag.id,
});

const TalentTags = ({ companyTags, talent }: TalentTagsProps) => {
  const { onSubmit, loading } = useSubmitHandler({ talentId: talent?.id });
  const allTagOptions = useMemo(
    () => companyTags.map(mapTagToOption),
    [companyTags],
  );
  const initialValues = useMemo(
    () =>
      allTagOptions.filter((allTagsItem) =>
        talent?.tags?.some((i) => i?.id === allTagsItem.value),
      ),
    [allTagOptions, talent?.tags],
  );

  return (
    <Formik<IFormState>
      onSubmit={onSubmit}
      initialValues={{ tags: initialValues }}
    >
      {({ values }) => (
        <Form>
          <FormikAutoSave />
          <Paper p={4} component={Box} elevation={0} variant="outlined">
            <Box mb={4}>
              <Typography component="span" variant="h6">
                Tags
              </Typography>{' '}
              <Tooltip
                title="Assign tags to candidate profiles for internal filtering"
                placement="right"
              >
                <InfoIcon
                  style={{ margin: '0 auto -4px' }}
                  fontSize="small"
                  color="disabled"
                />
              </Tooltip>
            </Box>

            <ConnectedMultipleSelect
              name="tags"
              size="small"
              options={allTagOptions}
              autoCompleteProps={{
                loading,
                filterSelectedOptions: true,
              }}
              inputProps={{
                label: 'Pick from list',
              }}
            />
          </Paper>
        </Form>
      )}
    </Formik>
  );
};

export default TalentTags;
