import { Box, CircularProgress, ImageList, ImageListItem } from '@mui/material';
import React from 'react';

import { ChatMessage } from '@libs/graphql-types';

import Comment from '../comment';
import useStyles from '../styles';
import { useLoadComments, useOnScrollListener } from './hooks';

interface CommentsListProps {
  newComments: ChatMessage[];
  listRef: any;
  talentId: string;
  scrollToBottom: VoidFunction;
}

const CommentsList = ({
  newComments,
  listRef,
  talentId,
  scrollToBottom,
}: CommentsListProps) => {
  const classes = useStyles();
  const { comments, isLoading, loadMoreComments } = useLoadComments({
    talentId,
    scrollToBottom,
  });

  const onScroll = useOnScrollListener({ ref: listRef, loadMoreComments });

  return (
    <ImageList
      classes={{ root: classes.commentsWrapper }}
      cols={1}
      ref={listRef}
      onScroll={onScroll}
      style={{ flexDirection: 'column', flexWrap: 'nowrap' }}
    >
      <Box
        padding={2}
        display="flex"
        justifyContent="center"
        width={30}
        height={30}
        style={{ height: 0 }}
      >
        {isLoading && <CircularProgress color="secondary" size={30} />}
      </Box>
      {comments
        .slice()
        .reverse()
        .map((comment) => (
          <ImageListItem className={classes.commentWrapper} key={comment?.id}>
            <Comment data={comment} />
          </ImageListItem>
        ))}
      {newComments.map((comment) => (
        <ImageListItem className={classes.commentWrapper} key={comment?.id}>
          <Comment data={comment} />
        </ImageListItem>
      ))}
    </ImageList>
  );
};

export default CommentsList;
