import { OperationVariables, useLazyQuery } from '@apollo/client';
import { useIsItCurrentTalent } from 'hooks/common/useIsItCurrentTalent';
import { useSnackbar } from 'notistack';
import { useCallback, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { pathManager } from 'routes';

import { Query } from '@libs/graphql-types';

import { TALENT_PROFILE_DATA } from './queries';

const useTalentData = (talentId: string) => {
  const history = useHistory();
  const { enqueueSnackbar } = useSnackbar();

  const [request, { loading, data }] = useLazyQuery<Query, OperationVariables>(
    TALENT_PROFILE_DATA,
    {
      variables: {
        talent_id: talentId,
      },
      fetchPolicy: 'network-only',
      onCompleted: ({ talent }) => {
        if (!talent) {
          history.push(pathManager.company.workforce.generatePath());
        }
      },
      onError: (error) => {
        enqueueSnackbar(error.message, {
          variant: 'error',
          preventDuplicate: true,
        });
      },
    },
  );

  const fetchTalent = useCallback(() => {
    if (!talentId) {
      return;
    }

    request({
      variables: {
        talent_id: talentId,
      },
    });
  }, [request, talentId]);

  return { loading, talent: data?.talent, fetchTalent, data };
};

export const useProfileInfo = (talentId?: string) => {
  const history = useHistory();
  const isItSameProfile = useIsItCurrentTalent(talentId);
  const result = useTalentData(talentId as string);
  const { fetchTalent } = result;

  useEffect(() => {
    if (isItSameProfile) {
      history.push(pathManager.talent.profile.generatePath());

      return;
    }
    fetchTalent();
  }, [fetchTalent, history, isItSameProfile]);

  return result;
};
