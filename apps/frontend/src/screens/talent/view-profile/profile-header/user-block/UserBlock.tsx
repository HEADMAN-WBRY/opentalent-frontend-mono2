import { ReactComponent as CheckIcon } from 'assets/icons/check.svg';
import { ReactComponent as EuroIcon } from 'assets/icons/euro.svg';
import { ReactComponent as MapPinIcon } from 'assets/icons/map-pin.svg';
import { DEFAULT_AVATAR } from 'consts/common';
import { JOIN_REASON_LABELS } from 'consts/talents';
import { useIsPaidTalentAccount } from 'hooks/talents/useTalentAccountType';
import React, { useMemo } from 'react';
import { useOpenPremiumSubscriptionModal } from 'screens/profile/LimitedProfileAccessModal';
import { formatName, isVerifiedTalent } from 'utils/talent';

import { Avatar, Grid, Tooltip, Badge, Chip } from '@mui/material';
import Box from '@mui/material/Box';

import { Talent } from '@libs/graphql-types';
import { formatRate } from '@libs/ui/components/job/utils';
import Typography from '@libs/ui/components/typography';

import useStyles from './styles';

export interface UserBlockProps {
  talent: Partial<Talent> | null;
  isXS: boolean;
}

const UserBlock = (props: UserBlockProps) => {
  const classes = useStyles(props);
  const { talent, isXS } = props;
  const rate = formatRate({ min: talent?.rate_min, period: 'hour' });
  const rateMonth = formatRate({ min: talent?.salary, period: 'month' });

  const canSeeData = useIsPaidTalentAccount();
  const openLimitedAccessModal = useOpenPremiumSubscriptionModal();
  const isTalentVerified = isVerifiedTalent(talent as Talent);

  const talentName = formatName({
    firstName: talent?.first_name_abac?.value,
    lastName: talent?.last_name_abac?.value,
  });

  const isVerificationCheckShown = useMemo(
    () => talent?.is_invitation_accepted && isTalentVerified,
    [talent?.is_invitation_accepted, isTalentVerified],
  );

  return (
    <Grid
      className={classes.root}
      direction={isXS ? 'column' : 'row'}
      spacing={6}
      container
      wrap="nowrap"
    >
      <Grid item justifyContent={isXS ? 'center' : undefined}>
        <Badge
          overlap="circular"
          classes={{ badge: classes.avatarBadge }}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          variant="standard"
          badgeContent={
            <Box>
              <Tooltip
                placement="right-end"
                open={talent?.available_now ? undefined : false}
                title={
                  talent?.hours_per_week
                    ? `Available now for ${talent.hours_per_week} hours per week`
                    : "Available now - let's get to work!"
                }
              >
                <Box width={22} height={22} />
              </Tooltip>
            </Box>
          }
        >
          <Avatar
            className={classes.avatar}
            src={talent?.avatar?.avatar || DEFAULT_AVATAR}
          />
        </Badge>
      </Grid>
      <Grid item>
        <Grid wrap="nowrap" direction="column" container>
          <Grid
            justifyContent={isXS ? 'center' : undefined}
            spacing={2}
            item
            container
            style={{ flexWrap: 'nowrap', alignItems: 'center' }}
          >
            <Grid item>
              <Typography
                onClick={() => {
                  if (!canSeeData) {
                    openLimitedAccessModal();
                  }
                }}
                pointer={!canSeeData}
                className={classes.name}
                noWrap
                variant="h5"
              >
                {talentName}
              </Typography>
            </Grid>
            {isVerificationCheckShown && (
              <Grid item>
                <Tooltip title="This person was verified by Opentalent through interviews, social media and/or legal document checks.">
                  <CheckIcon className={classes.checkIcon} />
                </Tooltip>
              </Grid>
            )}
            {isTalentVerified && (
              <Typography
                variant="subtitle2"
                className={classes.pendingVerification}
              >
                pending verification
              </Typography>
            )}
            {talent?.is_matcher && (
              <Grid item>
                <Chip
                  size="small"
                  label="Talent Matcher"
                  className={classes.recruiterChip}
                />
              </Grid>
            )}
          </Grid>
          <Grid item>
            <Typography
              align={isXS ? 'center' : undefined}
              variant="body2"
              color="textSecondary"
              className={classes.positionTitle}
            >
              {talent?.recent_position_title}
            </Typography>
          </Grid>
          {!!talent?.join_reason?.length && (
            <Grid item>
              <Box mt={1}>
                <Grid container spacing={2}>
                  <Grid item>
                    <Typography variant="body2" color="textSecondary">
                      Looking for:
                    </Typography>
                  </Grid>
                  {talent.join_reason.map((i) => (
                    <Grid item key={i}>
                      <Chip
                        label={JOIN_REASON_LABELS[i!]}
                        size="small"
                        color="success"
                        style={{ color: 'black' }}
                      />
                    </Grid>
                  ))}
                </Grid>
              </Box>
            </Grid>
          )}
          <Grid className={classes.info} item>
            <Grid
              justifyContent={isXS ? 'center' : undefined}
              alignItems="stretch"
              spacing={2}
              container
            >
              {!!rate && (
                <>
                  <Grid item>
                    <EuroIcon />
                  </Grid>
                  <Grid item>
                    <Typography variant="body2" color="textSecondary">
                      {rate}
                    </Typography>
                  </Grid>
                </>
              )}
              {!!rateMonth && (
                <>
                  <Grid item>
                    <EuroIcon />
                  </Grid>
                  <Grid item>
                    <Typography variant="body2" color="textSecondary">
                      {rateMonth}
                    </Typography>
                  </Grid>
                </>
              )}
              <Grid item>&nbsp;</Grid>
              {talent?.location && (
                <>
                  <Grid item>
                    <MapPinIcon color="disabled" />
                  </Grid>
                  <Grid item>
                    <Typography
                      className={classes.location}
                      variant="body2"
                      color="textSecondary"
                    >
                      {talent?.location}
                    </Typography>
                  </Grid>
                </>
              )}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default UserBlock;
