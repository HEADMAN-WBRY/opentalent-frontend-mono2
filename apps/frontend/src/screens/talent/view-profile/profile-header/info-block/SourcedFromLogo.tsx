import { DEFAULT_AVATAR } from 'consts/common';
import React from 'react';

import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Tooltip from '@mui/material/Tooltip';

import useStyles from './styles';

interface SourcedFromLogoProps {
  tooltip?: string;
  image?: string;
}

const SourcedFromLogo = (props: SourcedFromLogoProps) => {
  const { tooltip, image = DEFAULT_AVATAR } = props;
  const classes = useStyles();

  return (
    <Box position="relative">
      <Tooltip
        open={!!tooltip ? undefined : false}
        classes={{
          tooltip: classes.sourcedTooltip,
          tooltipPlacementLeft: classes.sourcedTooltipPlacementLeft,
          tooltipPlacementRight: classes.sourcedTooltipPlacementRight,
        }}
        placement="right-end"
        title={tooltip!}
      >
        <Avatar className={classes.sourcedAvatar} src={image} />
      </Tooltip>
    </Box>
  );
};

export default SourcedFromLogo;
