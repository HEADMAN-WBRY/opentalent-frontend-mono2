import React from 'react';
import { Link } from 'react-router-dom';

import Typography from '@libs/ui/components/typography';

interface InvitedByLinkProps {
  text: string;
  link?: string;
}

const InvitedByLink = ({ text, link }: InvitedByLinkProps) => {
  return (
    <Typography
      variant="subtitle1"
      color={!!link ? 'tertiary' : 'textPrimary'}
      style={{ textDecoration: !!link ? 'underline' : undefined }}
    >
      {!!link ? <Link to={link}>{text}</Link> : text}
    </Typography>
  );
};

export default InvitedByLink;
