import React from 'react';
import { getInvitedByInfo } from 'screens/talent/shared/profile/getInvitedByInfo';

import { Grid } from '@mui/material';

import { Talent } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import InvitedByLink from './InvitedByLink';
import SourcedFromLogo from './SourcedFromLogo';
import useStyles from './styles';

interface InfoBlockProps {
  isSM?: boolean;
  isXS?: boolean;
  talent: Partial<Talent> | null;
}

const InfoBlock = ({ isXS, talent }: InfoBlockProps) => {
  const classes = useStyles();
  const info = getInvitedByInfo(talent as Talent);

  return (
    <Grid
      className={classes.infoBlock}
      container
      justifyContent={isXS ? 'center' : undefined}
    >
      <Grid item className={classes.sourcedInfo}>
        <SourcedFromLogo tooltip={info.tooltip} image={info.image} />
      </Grid>
      <Grid item>
        <Grid
          direction="column"
          className={classes.invitedBlock}
          justifyContent="center"
          container
        >
          <Grid item>
            <Grid container>
              <Grid item>
                <Typography
                  transform="uppercase"
                  variant="overline"
                  color="textSecondary"
                  style={{ whiteSpace: 'nowrap' }}
                >
                  {info.title}
                </Typography>
              </Grid>
              <Grid item>
                {
                  // invitedByCompany && (
                  //   <Tooltip
                  //     TransitionComponent={Zoom}
                  //     title="This user has been invited by a company"
                  //   >
                  //     <InfoIcon color="disabled" />
                  //   </Tooltip>
                  // )
                }
              </Grid>
            </Grid>
          </Grid>
          <Grid item>
            <InvitedByLink text={info.inviter} link={info.link} />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default InfoBlock;
