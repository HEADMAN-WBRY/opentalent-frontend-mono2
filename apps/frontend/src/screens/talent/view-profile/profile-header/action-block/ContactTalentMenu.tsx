import { usePopupState } from 'material-ui-popup-state/hooks';
import React from 'react';

import { Talent } from '@libs/graphql-types';

import MessengerMenuItem from './MessengerMenuItem';

interface ContactTalentMenuProps {
  talent: Partial<Talent>;
}

const ContactTalentMenu = ({ talent }: ContactTalentMenuProps) => {
  const popupState = usePopupState({
    variant: 'popper',
    popupId: 'demoPopper',
  });

  return (
    <MessengerMenuItem
      talent={talent as Talent}
      closePopup={popupState.close}
    />
  );
};

export default ContactTalentMenu;
