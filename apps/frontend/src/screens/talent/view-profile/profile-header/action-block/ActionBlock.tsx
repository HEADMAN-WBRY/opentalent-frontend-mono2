import { useIsItCurrentTalent } from 'hooks/common/useIsItCurrentTalent';
import React from 'react';

import { Box, Grid } from '@mui/material';

import { Talent } from '@libs/graphql-types';

import ContactTalentMenu from './ContactTalentMenu';
import { IntroduceMe } from './IntroduceMe';
import useStyles from './styles';

interface ActionBlockProps {
  talent: Partial<Talent>;
  refetchTalent: VoidFunction;
}

const ActionBlock = ({ talent, refetchTalent }: ActionBlockProps) => {
  const classes = useStyles();
  const isItSameProfile = useIsItCurrentTalent(talent.id);

  return (
    <Box className={classes.container}>
      <Grid container spacing={4} direction="column">
        <Grid item>
          <ContactTalentMenu talent={talent} />
        </Grid>
        {!isItSameProfile && (
          <Grid item>
            <IntroduceMe talent={talent} />
          </Grid>
        )}
      </Grid>
    </Box>
  );
};

export default ActionBlock;
