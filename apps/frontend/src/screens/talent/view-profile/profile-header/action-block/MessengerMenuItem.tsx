import { ChatTypes } from 'components/chat';
import { useMessengerStatus } from 'components/chat/hooks';
import { useOpenTalentPremiumModal } from 'components/custom/talent/modals/limited-profile-access';
import { useIsPaidTalentAccount } from 'hooks/talents/useTalentAccountType';
import { useHistory } from 'react-router-dom';
import { pathManager } from 'routes';

import SendIcon from '@mui/icons-material/Send';

import { Talent } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';

interface MessengerMenuItemProps {
  closePopup: VoidFunction;
  talent: Talent;
}

const MessengerMenuItem = ({ closePopup, talent }: MessengerMenuItemProps) => {
  const hasAccess = useIsPaidTalentAccount();
  const history = useHistory();
  const isEnabled = useMessengerStatus(talent.stream_chat_id || '');
  const openModal = useOpenTalentPremiumModal();

  return (
    <Button
      variant="contained"
      color="primary"
      style={{ minWidth: '150px' }}
      size="medium"
      fullWidth
      disabled={!isEnabled}
      endIcon={<SendIcon />}
      onClick={() => {
        if (hasAccess) {
          history.push({
            pathname: pathManager.chat.generatePath(),
            state: {
              strategy: {
                type: ChatTypes.TalentToTalent,
                data: talent,
              },
            },
          });
          closePopup();
        } else {
          openModal();
        }
      }}
    >
      Message
    </Button>
  );
};

export default MessengerMenuItem;
