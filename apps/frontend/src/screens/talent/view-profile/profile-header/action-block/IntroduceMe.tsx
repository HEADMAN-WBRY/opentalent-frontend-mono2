import { useCurrentUser } from 'hooks/auth';
import { useIsPaidTalentAccount } from 'hooks/talents/useTalentAccountType';
import React from 'react';
import { formatName } from 'utils/talent';

import { Tooltip } from '@mui/material';

import { Talent } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';

interface IntroduceMeProps {
  talent: Partial<Talent>;
}

export const IntroduceMe = ({ talent }: IntroduceMeProps) => {
  const isCurrentTalentPaid = useIsPaidTalentAccount();
  const { getData } = useCurrentUser();
  const currentTalentData = getData().data?.currentTalent;
  const currentTalentName = formatName({
    firstName: currentTalentData?.first_name,
    lastName: currentTalentData?.last_name,
  });
  const talentName = formatName({
    firstName: talent.first_name_abac?.value,
    lastName: talent.last_name_abac?.value,
  });

  const link = `https://form.typeform.com/to/mFKw0IhB#\
requester_name=${currentTalentName}\
&requester_email=${currentTalentData?.email}\
&target_name=${talentName}\
&target_email=${talent.email_abac?.value}`;

  return (
    <Tooltip
      title="Membership required"
      followCursor
      open={isCurrentTalentPaid ? false : undefined}
    >
      <div>
        <Button
          disabled={!isCurrentTalentPaid}
          href={link}
          color="info"
          variant="outlined"
        >
          Introduce me
        </Button>
      </div>
    </Tooltip>
  );
};
