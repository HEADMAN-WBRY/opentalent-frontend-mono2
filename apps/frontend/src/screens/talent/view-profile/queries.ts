import { gql } from '@apollo/client';
import { ABAC_PROPERTIES } from 'graphql/fragments/talent/abacFields';

export const TALENT_PROFILE_DATA = gql`
  ${ABAC_PROPERTIES}
  query GetTalentData($talent_id: ID!) {
    talent(id: $talent_id) {
      stream_chat_id
      stream_chat_token
      default_company_referral_key
      join_reason
      first_name_abac {
        ...AbacProps
      }
      last_name_abac {
        ...AbacProps
      }
      available_date_updated_at
      address
      location
      recent_position_title
      about
      id
      rate_min
      rate_max
      salary
      is_invitation_accepted
      is_verification_required
      is_matcher
      email_abac {
        ...AbacProps
      }
      available_date
      hours_per_week
      available_now
      is_ot_pool
      reminded_at
      account_settings {
        receive_company_and_product_updates
        receive_direct_messages
        instant_matches_notifications_min_score
      }
      companies {
        id
        name
      }
      category {
        id
        name
        slug
        created_at
        updated_at
      }
      subcategories {
        id
        name
      }
      source {
        id
        name
        logo
        created_at
        updated_at
      }
      avatar {
        avatar
        hash
      }
      documents {
        title
        url
        size
        content_type
        hash
        created_at
        updated_at
      }
      talent_data {
        linkedin_profile_link_abac {
          ...AbacProps
        }
      }
      invited_by {
        company_name
        first_name_abac {
          value
        }
        last_name_abac {
          value
        }
        invitation_type
        id
      }
      talent_work_history {
        id
        position_title
        company_name
        worked_from
        worked_to
        created_at
        updated_at
      }
      skills(first: 50) {
        data {
          id
          skill_type: skill_type
          name
        }
      }
    }
  }
`;
