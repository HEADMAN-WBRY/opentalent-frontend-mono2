import { useCurrentUser } from 'hooks/auth';
import React from 'react';

import ShareIcon from '@mui/icons-material/Share';
import { Box, Card, CardContent } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import { useCopyAction } from './hooks';

interface RefLinkBlockProps {}

const useStyles = makeStyles((theme) => ({
  card: {
    maxWidth: 450,
    margin: '0 auto',
  },
  cardContent: {
    padding: theme.spacing(8, 12),
  },
}));

const RefLinkBlock = (props: RefLinkBlockProps) => {
  const classes = useStyles();
  const { getData } = useCurrentUser();
  const refKey = getData()?.data?.currentTalent?.default_company_referral_key;
  const onCopy = useCopyAction(refKey);

  return (
    <>
      <Card raised className={classes.card}>
        <CardContent className={classes.cardContent}>
          <Box mb={6}>
            <Typography fontWeight={500} textAlign="center" variant="h6">
              Share your personal invite link
            </Typography>
          </Box>

          <Button
            variant="contained"
            size="large"
            fullWidth
            color="primary"
            startIcon={<ShareIcon />}
            onClick={onCopy}
            disabled={!refKey}
          >
            Copy invite link
          </Button>
          {!refKey && (
            <Box mt={4}>
              <Typography
                color="error.main"
                fontWeight={500}
                textAlign="center"
              >
                Couldn't get referral key
              </Typography>
            </Box>
          )}
        </CardContent>
        <i />
      </Card>
    </>
  );
};

export default RefLinkBlock;
