import { useSnackbar } from 'notistack';
import { useCallback } from 'react';
import { useCopyToClipboard } from 'react-use';

export const useCopyAction = (refKey?: string) => {
  const { enqueueSnackbar } = useSnackbar();
  const [, copy] = useCopyToClipboard();

  const onCopy = useCallback(() => {
    if (!refKey) {
      enqueueSnackbar("Couldn't get referral key", { variant: 'error' });

      return null;
    }
    copy(`${window.location.origin}/invite/${refKey}`);
    enqueueSnackbar('Referral link is copied', { variant: 'success' });
  }, [copy, enqueueSnackbar, refKey]);

  return onCopy;
};
