import { DEFAULT_AVATAR } from 'consts/common';
import React from 'react';

import { Avatar, Grid } from '@mui/material';

import Typography from '@libs/ui/components/typography';

import useStyles from './styles';

interface TalentCardProps {
  name?: string;
  logo?: string;
}

const TalentCard = (props: TalentCardProps) => {
  const { name = 'Company', logo = DEFAULT_AVATAR } = props;
  const classes = useStyles(props);

  return (
    <Grid className={classes.container} container spacing={5}>
      <Grid item>
        <Avatar className={classes.avatar} src={logo} />
      </Grid>
      <Grid item>
        <Grid>
          <Typography variant="h6">{name}</Typography>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default TalentCard;
