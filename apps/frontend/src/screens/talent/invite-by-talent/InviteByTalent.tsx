import { ConnectedPageLayout } from 'components/layout/page-layout';
import { usePushWithQuery, useSearchParams } from 'hooks/routing';
import React from 'react';
import { RouteComponentProps } from 'react-router-dom';

import { TabContext, TabPanel } from '@mui/lab';

import { Company, useCurrentTalentInvitationsQuery } from '@libs/graphql-types';

import InvitationTabs from './InvitationTabs';
import InviteClients from './invite-clients';
import InviteTalents from './invite-talents/InviteTalents';
import { useRequestInviteData } from './invite-talents/hooks';
import { InviteTabs } from './types';

interface ProfileProps extends RouteComponentProps { }

const getTab = (tab: string) =>
  new Set([InviteTabs.Talent, InviteTabs.Company]).has(tab as InviteTabs)
    ? tab
    : InviteTabs.Talent;

const InviteByTalent = (props: ProfileProps) => {
  const { tab } = useSearchParams();
  const currentTab = getTab(tab as string);
  const pushWithQuery = usePushWithQuery();
  const onChange = (tab: string) => pushWithQuery({ query: { tab } });
  const { data, id, isLoading, getInvitationRequest } = useRequestInviteData();
  const left = data?.talentInvitationsInfo?.talent_invitations_left ?? 0;

  const {
    loading: newInvitationsLoading,
    data: newInvitationsData,
    refetch: reloadTalentInvitations,
  } = useCurrentTalentInvitationsQuery();
  const isLoadingFinal = newInvitationsLoading || isLoading;
  const max =
    left + (newInvitationsData?.currentTalentInvitations?.length || 0);

  const referch = () => {
    reloadTalentInvitations();
    getInvitationRequest();
  };

  return (
    <ConnectedPageLayout
      headerProps={{ accountProps: {} }}
      documentTitle="InviteByTalent"
      drawerProps={{}}
      isLoading={isLoadingFinal}
    >
      <TabContext value={currentTab}>
        <InvitationTabs onChange={onChange} />

        <TabPanel value={InviteTabs.Talent}>
          <InviteTalents
            talentInvitationsInfo={data?.talentInvitationsInfo}
            invites={newInvitationsData?.currentTalentInvitations}
            left={left}
            max={max}
            getInvitationRequest={getInvitationRequest}
            id={id}
            refetch={referch}
          />
        </TabPanel>
        <TabPanel value={InviteTabs.Company}>
          <InviteClients
            companies={
              data?.talentInvitationsInfo?.referred_companies as Company[]
            }
          />
        </TabPanel>
      </TabContext>
    </ConnectedPageLayout>
  );
};

export default InviteByTalent;
