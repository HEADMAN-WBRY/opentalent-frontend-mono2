import React from 'react';

import { TabList } from '@mui/lab';
import { Tab } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { InviteTabs } from './types';

interface InvitationTabsProps {
  onChange: (tab: string) => void;
}

const useStyles = makeStyles((theme) => ({
  container: {
    justifyContent: 'center',
  },
  tabSelected: {
    color: `${theme.palette.info.main} !important`,
  },
  indicator: {
    backgroundColor: `${theme.palette.info.main} !important`,
  },
}));

const InvitationTabs = ({ onChange }: InvitationTabsProps) => {
  const classes = useStyles();

  return (
    <TabList
      classes={{
        flexContainer: classes.container,
        indicator: classes.indicator,
      }}
      color="info"
      onChange={(e, val) => onChange(val)}
    >
      <Tab
        classes={{ selected: classes.tabSelected }}
        label="Invite talent"
        value={InviteTabs.Talent}
      />
      {/* <Tab */}
      {/*   classes={{ selected: classes.tabSelected }} */}
      {/*   label="Refer a client" */}
      {/*   value={InviteTabs.Company} */}
      {/* /> */}
    </TabList>
  );
};

export default InvitationTabs;
