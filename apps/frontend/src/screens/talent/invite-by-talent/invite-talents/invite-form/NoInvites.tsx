import { Box, Grid } from '@mui/material';
import { ReactComponent as InviteImage } from 'assets/images/invite-talent.svg';
import React from 'react';

import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

interface NoInvitesProps {
  openModal: VoidFunction;
}

const NoInvites = ({ openModal }: NoInvitesProps) => {
  return (
    <Box padding="20px 20px 0">
      <Grid container direction="column">
        <Grid item>
          <Typography paragraph align="center" variant="h6">
            Do you want to invite more than 5 people?
          </Typography>
        </Grid>
        <Grid item>
          <Box margin="0 auto" maxWidth="370px">
            <Typography paragraph align="center" variant="body2">
              Join our Chief&apos;s programme to invite up to 50 people and
              build your freelance collective.
            </Typography>
          </Box>
        </Grid>
        <Grid item>
          <Box pt={4} textAlign="center">
            <Button onClick={openModal} variant="contained" color="primary">
              Join the programme
            </Button>
          </Box>
        </Grid>
        <Grid item>
          <Box mt={-11} textAlign="center">
            <InviteImage />
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default NoInvites;
