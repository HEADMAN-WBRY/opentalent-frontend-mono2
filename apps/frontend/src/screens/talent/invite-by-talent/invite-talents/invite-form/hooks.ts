import { useMutation } from '@apollo/client';
import { CREATE_INVITATION_LINK } from 'graphql/talents';
import useMixPanel from 'hooks/common/useMixPanel';
import { useSnackbar } from 'notistack';
import { useCallback } from 'react';
import { useCopyToClipboard } from 'react-use';

import {
  Mutation,
  MutationCreateInvitationLinkArgs,
  useCreateTalentInvitationMutation,
} from '@libs/graphql-types';
import { FormikSubmit } from '@libs/helpers/form';

import { INITIAL_VALUES } from './consts';

interface HookParams {
  id: string;
  getInvitationRequest: VoidFunction;
  getInvites: VoidFunction;
}

export const useSubmitEmailForm = ({
  id,
  getInvitationRequest,
  getInvites,
}: HookParams) => {
  const { enqueueSnackbar } = useSnackbar();
  const mixPanel = useMixPanel();
  const [invite, { loading: isInviting, error }] =
    useCreateTalentInvitationMutation({
      onCompleted: () => {
        enqueueSnackbar('Your invite was sent', {
          variant: 'success',
        });
        mixPanel.track('User was invited through the invite page');
        getInvites();
      },
    });
  const onSubmit: FormikSubmit<typeof INITIAL_VALUES> = useCallback(
    async (values, utils) => {
      try {
        await invite({
          variables: {
            first_name: values.name,
            email: values.email,
            inviter_talent_id: id,
          },
        });
      } catch (e) {
        if (
          (e as any).graphQLErrors[0].extensions?.validation?.email?.includes(
            'The email has already been taken.',
          )
        ) {
          enqueueSnackbar('This person already has OpenTalent account', {
            variant: 'warning',
          });
        } else {
          enqueueSnackbar('Something went wrong', {
            variant: 'error',
          });
        }
        return;
      }
      utils.resetForm({ values: INITIAL_VALUES });
    },
    [enqueueSnackbar, id, invite],
  );

  return { onSubmit, loading: isInviting, error };
};

export const useSubmitLinkForm = ({ id, getInvitationRequest }: HookParams) => {
  const { enqueueSnackbar } = useSnackbar();
  const [, copyToClipboard] = useCopyToClipboard();
  const [getInviteLink, { loading }] = useMutation<
    Mutation,
    MutationCreateInvitationLinkArgs
  >(CREATE_INVITATION_LINK, {
    onCompleted: (values) => {
      const link = values.createInvitationLink?.url;

      if (link) {
        copyToClipboard(link);
        getInvitationRequest();
        enqueueSnackbar('Link copied successfully!', { variant: 'success' });
      }
    },
    // eslint-disable-next-line no-console
    onError: console.error,
  });

  const onSubmit: FormikSubmit<{ name: string }> = useCallback(
    async ({ name }) => {
      getInviteLink({ variables: { inviting_talent_id: id, name } });
    },
    [getInviteLink, id],
  );

  return { loading, onSubmit };
};
