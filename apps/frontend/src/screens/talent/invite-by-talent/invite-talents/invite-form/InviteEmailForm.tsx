import { useApolloClient } from '@apollo/client';
import errors from 'consts/validationErrors';
import { Form, Formik } from 'formik';
import React from 'react';
import EmailControl from 'screens/company-user/create-profile/email-control';
import * as yup from 'yup';

import MailOutlineIcon from '@mui/icons-material/MailOutline';
import PermIdentityIcon from '@mui/icons-material/PermIdentity';
import { Grid } from '@mui/material';

import { CheckTalentExistsByEmailDocument } from '@libs/graphql-types';
import { maxStringValidator } from '@libs/helpers/yup';
import Button from '@libs/ui/components/button';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import Typography from '@libs/ui/components/typography';

import { INITIAL_VALUES } from './consts';
import { useSubmitEmailForm } from './hooks';

interface InviteEmailFormProps {
  disabled: boolean;
  id: string;
  getInvitationRequest: VoidFunction;
  getInvites: VoidFunction;
}

const useValidator = () => {
  const client = useApolloClient();
  const validator = yup.object().shape({
    name: maxStringValidator.required(errors.required),
    email: maxStringValidator
      .email(errors.invalidEmail)
      .required(errors.required)
      .test(
        'email-exist-check',
        'A talent account with this email already exists',
        async function test(email) {
          const isValid = await yup.string().email().required().validate(email);
          if (isValid) {
            const res = await client.query({
              query: CheckTalentExistsByEmailDocument,
              variables: { talent_email: email },
            });
            return !res.data.checkTalentExistsByEmail;
          }
          return this.schema.validate(email);
        },
      ),
  });

  return validator;
};

const InviteEmailForm = ({
  getInvitationRequest,
  disabled,
  id,
  getInvites,
}: InviteEmailFormProps) => {
  const validator = useValidator();
  const { onSubmit, loading } = useSubmitEmailForm({
    id,
    getInvitationRequest,
    getInvites,
  });

  return (
    <Formik
      validationSchema={validator}
      initialValues={INITIAL_VALUES}
      onSubmit={onSubmit}
    >
      {() => (
        <Form>
          <Grid spacing={8} container direction="column">
            <Grid item>
              <Typography variant="h6" align="center">
                Whom do you want to invite?
              </Typography>
            </Grid>
            <Grid item>
              <Typography variant="body2" align="center">
                send invite by email
              </Typography>
            </Grid>
            <Grid item>
              <ConnectedTextField
                label="First Name"
                fullWidth
                name="name"
                variant="outlined"
                InputProps={{
                  startAdornment: <PermIdentityIcon color="disabled" />,
                }}
              />
            </Grid>
            <Grid item>
              <EmailControl
                label="Email"
                name="email"
                fullWidth
                variant="outlined"
                InputProps={{
                  startAdornment: <MailOutlineIcon color="disabled" />,
                }}
              />
            </Grid>
            <Grid item>
              <Button
                type="submit"
                variant="contained"
                color="primary"
                fullWidth
                disabled={disabled || !!loading}
              >
                Send invite
              </Button>
            </Grid>

            {/* <Grid item> */}
            {/*   <Typography */}
            {/*     style={{ */}
            {/*       display: 'flex', */}
            {/*       justifyContent: 'center', */}
            {/*     }} */}
            {/*     variant="body2" */}
            {/*   > */}
            {/*     or&nbsp; */}
            {/*     <RouterLink to={{ search: `?form=${FORM_TYPES.link}` }}> */}
            {/*       share your personal invite link */}
            {/*     </RouterLink> */}
            {/*   </Typography> */}
            {/* </Grid> */}
          </Grid>
        </Form>
      )}
    </Formik>
  );
};

export default InviteEmailForm;
