// import { useSearchParams } from 'hooks/routing';
import React from 'react';

import { Box, Grid, Paper } from '@mui/material';

import Typography from '@libs/ui/components/typography';

import InviteEmailForm from './InviteEmailForm';
// import InviteLinkForm from './InviteLinkForm';
import NoInvites from './NoInvites';
// import { FORM_TYPES } from './consts';
import useStyles from './styles';

interface InviteFormProps {
  max: number;
  left: number;
  id: string;
  getInvitationRequest: VoidFunction;
  openModal: VoidFunction;
  getInvites: VoidFunction;
}

const InviteForm = (props: InviteFormProps) => {
  const { max, id, getInvitationRequest, openModal, left, getInvites } = props;
  const classes = useStyles(props);
  // const { form = FORM_TYPES.email } = useSearchParams();
  // const isLinkForm = form === FORM_TYPES.link;
  const noInvites = left <= 0;

  return (
    <Grid className={classes.container} container direction="column">
      <Grid item>
        <Box margin="0 auto">
          <Typography
            className={classes.title}
            paragraph
            variant="h5"
            align="center"
          >
            Invite talent and get paid
          </Typography>
        </Box>
      </Grid>
      <Grid item>
        <Typography paragraph variant="body1" align="center">
          {noInvites ? 'No more invites' : `(${left}/${max} invites left)`}
        </Typography>
      </Grid>
      {noInvites && <NoInvites openModal={openModal} />}
      {!noInvites && (
        <Grid item>
          <Paper elevation={10} className={classes.form}>
            {/* {isLinkForm ? ( */}
            {/*   <InviteLinkForm */}
            {/*     id={id} */}
            {/*     getInvitationRequest={getInvitationRequest} */}
            {/*     disabled={noInvites} */}
            {/*   /> */}
            {/* ) : ( */}
            <InviteEmailForm
              id={id}
              getInvitationRequest={getInvitationRequest}
              disabled={noInvites}
              getInvites={getInvites}
            />
            {/* )} */}
          </Paper>
        </Grid>
      )}
    </Grid>
  );
};

export default InviteForm;
