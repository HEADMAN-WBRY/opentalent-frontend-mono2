import React from 'react';

import { Box, Grid } from '@mui/material';

import { CurrentTalentInvitationsQuery } from '@libs/graphql-types';
import Typography, { OuterLink } from '@libs/ui/components/typography';

import useStyles from './styles';
import TalentCard from './talent-card';

interface HowItWorksProps {
  max: number;
  left: number;
  invites: CurrentTalentInvitationsQuery['currentTalentInvitations'];
  openModal: VoidFunction;
  refetch: VoidFunction;
}

const HowItWorks = (props: HowItWorksProps) => {
  const { max, invites, left, refetch } = props;
  const classes = useStyles(props);

  return (
    <Grid
      className={classes.container}
      spacing={4}
      container
      direction="column"
    >
      <Grid item>
        <Typography variant="h6" align="center">
          How it works?
        </Typography>
      </Grid>
      <Grid item>
        <Box className={classes.paragraph}>
          <Typography variant="body2" align="center">
            We believe that talented individuals like yourself can be mobilised
            to act as the best talent spotters. So we built a system that
            enables you to source talent from your personal network and get paid
            for doing so. For every person you invite, and who gets hired
            through OpenTalent, we will pay you €1.000. You‘ll start with 5
            invites only, so spend them wisely. For more information about the
            referral scheme please check{' '}
            <OuterLink href="https://www.notion.so/opentalent/Get-Paid-830a2d2bd1c34f51a06a0c2637564c18?pvs=4">
              here
            </OuterLink>
          </Typography>
        </Box>
      </Grid>
      <Grid item>
        <Typography
          className={classes.talentsTitle}
          variant="h6"
          align="center"
        >
          People you invited
        </Typography>
      </Grid>
      <Grid item>
        <Grid container spacing={6} direction="column">
          {invites?.map((inv) => (
            <Grid key={inv?.id} item>
              <TalentCard invitation={inv} refetch={refetch} />
            </Grid>
          ))}
          {!!left && (
            <Grid item>
              <TalentCard count={{ max, left }} />
            </Grid>
          )}
        </Grid>
      </Grid>
    </Grid>
  );
};

export default HowItWorks;
