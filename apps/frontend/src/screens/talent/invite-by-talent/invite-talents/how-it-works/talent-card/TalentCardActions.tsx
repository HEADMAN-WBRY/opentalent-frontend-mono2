import { useMenuAction } from 'hooks/common/useMenuAction';
import React from 'react';

import MoreVertIcon from '@mui/icons-material/MoreVert';
import { ListItemIcon, ListItemText, Menu, MenuItem } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { CurrentTalentInvitationsQuery } from '@libs/graphql-types';
import { stopEvent } from '@libs/helpers/common';
import Button from '@libs/ui/components/button';

import { useInvitationActions } from './hooks';

interface TalentActionsProps {
  invitation: NonNullable<
    CurrentTalentInvitationsQuery['currentTalentInvitations']
  >[number];
  onSuccess: VoidFunction;
}

const useStyles = makeStyles((theme) => ({
  menu: {
    minWidth: `150px !important`,
  },
  error: {
    color: theme.palette.error.main,

    '& svg': {
      color: theme.palette.error.main,
    },
  },
  warinig: {
    color: theme.palette.warning.main,

    '& svg': {
      color: theme.palette.warning.main,
    },
  },
  success: {
    color: theme.palette.success.main,

    '& svg': {
      color: theme.palette.success.main,
    },
  },
}));

export const TalentCardActions = ({
  invitation,
  onSuccess,
}: TalentActionsProps) => {
  const { open, handleClose, handleClick, anchorEl } = useMenuAction();
  const classes = useStyles();
  const { loading, actions } = useInvitationActions({
    invitation,
    onSuccess: () => {
      handleClose();
      onSuccess();
    },
  });

  return (
    <>
      <Button
        onClick={handleClick as VoidFunction}
        variant="outlined"
        size="medium"
        data-test-id="talentActions"
        endIcon={<MoreVertIcon />}
        disabled={loading}
      >
        Actions
      </Button>
      <Menu
        id="long-menu"
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        onBackdropClick={stopEvent}
        classes={{ list: classes.menu }}
      >
        {actions.map(({ text, onClick, Icon, disabled = false, color }) => {
          return (
            <MenuItem
              key={text}
              onClick={(e) => {
                stopEvent(e);
                onClick();
                handleClose();
              }}
              style={{ paddingTop: '2px', paddingBottom: '2px' }}
              classes={{
                // @ts-ignore
                root: disabled ? '' : classes[color as any],
              }}
              disabled={disabled}
            >
              {Icon && (
                <ListItemIcon>
                  <Icon fontSize="inherit" />
                </ListItemIcon>
              )}
              <ListItemText primary={text} />
            </MenuItem>
          );
        })}
      </Menu>
    </>
  );
};
