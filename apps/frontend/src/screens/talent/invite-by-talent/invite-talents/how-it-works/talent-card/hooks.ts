import { ActionItem } from 'components/custom/talent/types';
import { useSnackbar } from 'notistack';

import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import NotInterestedIcon from '@mui/icons-material/NotInterested';

import {
  CurrentTalentInvitationsQuery,
  useCancelInvitationMutation,
  useResendInvitationMutation,
} from '@libs/graphql-types';

export const useInvitationActions = ({
  invitation,
  onSuccess,
}: {
  invitation: NonNullable<
    CurrentTalentInvitationsQuery['currentTalentInvitations']
  >[number];
  onSuccess: VoidFunction;
}): { loading: boolean; actions: ActionItem[] } => {
  const { enqueueSnackbar } = useSnackbar();
  const [resendInvitation, { loading: isResending }] =
    useResendInvitationMutation({
      variables: { id: invitation!?.id },
      onCompleted: () => {
        enqueueSnackbar('Invitation has been resended', { variant: 'success' });
        onSuccess();
      },
    });
  const [cancelInvitation, { loading: isCancelling }] =
    useCancelInvitationMutation({
      variables: { id: invitation!?.id },
      onCompleted: () => {
        enqueueSnackbar('Invitation has been cancelled', {
          variant: 'success',
        });
        onSuccess();
      },
    });

  const loading = isResending || isCancelling;

  return {
    loading,
    actions: [
      {
        text: 'Resend invitation',
        onClick: resendInvitation,
        Icon: CheckCircleOutlineIcon,
      },
      {
        text: 'Cancel invitation',
        onClick: cancelInvitation,
        Icon: NotInterestedIcon,
      },
    ] as ActionItem[],
  };
};
