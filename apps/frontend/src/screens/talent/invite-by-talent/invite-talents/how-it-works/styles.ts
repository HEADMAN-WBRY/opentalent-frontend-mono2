import { makeStyles } from '@mui/styles';

import { Theme } from '@libs/ui/themes/default';

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    paddingTop: theme.spacing(6),
    paddingBottom: theme.spacing(10),
  },
  talentsTitle: {
    padding: `${theme.spacing(4)} 0`,
  },
  footer: {
    paddingTop: `${theme.spacing(10)} !important`,
  },
  paragraph: {
    maxWidth: 670,
    padding: `0 ${theme.spacing(4)}`,
    margin: `${theme.spacing(6)} auto 0px`,
  },
}));

export default useStyles;
