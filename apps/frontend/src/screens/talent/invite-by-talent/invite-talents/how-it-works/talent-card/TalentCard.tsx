import { ReactComponent as CheckIcon } from 'assets/icons/check.svg';
import { ReactComponent as LetterIcon } from 'assets/icons/letter.svg';
import { DEFAULT_AVATAR } from 'consts/common';
import React from 'react';
import { useHistory } from 'react-router-dom';
import { pathManager } from 'routes';
import { formatName } from 'utils/talent';

import { Avatar, Grid } from '@mui/material';

import { CurrentTalentInvitationsQuery } from '@libs/graphql-types';
import { noop } from '@libs/helpers/common';
import Chip from '@libs/ui/components/chip/Chip';
import Typography from '@libs/ui/components/typography';

import { TalentCardActions } from './TalentCardActions';
import useStyles from './styles';

interface TalentCardProps {
  refetch?: VoidFunction;
  count?: {
    max: number;
    left: number;
  };
  invitation?: NonNullable<
    CurrentTalentInvitationsQuery['currentTalentInvitations']
  >[number];
}

const TalentCard = (props: TalentCardProps) => {
  const { count, invitation, refetch = noop } = props;
  const isTalentCard = !count;
  const classes = useStyles(props);
  const talent = invitation?.talent;
  const avatar = DEFAULT_AVATAR;
  const name = formatName({
    firstName: talent?.first_name || invitation?.first_name,
    lastName: talent?.last_name,
  });
  const email = talent?.email || invitation?.email;
  const isAccepted = invitation?.is_accepted;
  const needShowActions = isTalentCard && !isAccepted;
  const history = useHistory();
  const onTalentClick = () => {
    if (invitation?.talent?.id) {
      history.push(
        pathManager.talent.viewOtherTalent.generatePath({
          id: invitation.talent.id,
        }),
      );
    }
  };

  return (
    <div onClick={onTalentClick} className={classes.container}>
      <Grid container spacing={5} wrap="nowrap">
        <Grid item>
          {!isTalentCard ? (
            <LetterIcon />
          ) : (
            <Avatar className={classes.avatar} src={avatar} />
          )}
        </Grid>
        <Grid item flexGrow={1} className={classes.mainInfo}>
          <Grid direction="column" container className={classes.fullWidth}>
            <Grid item className={classes.fullWidth}>
              <Typography variant="h6" className={classes.name} title={name}>
                {!isTalentCard ? `Spread the love ❤️` : name}
              </Typography>
            </Grid>
            <Grid item className={classes.fullWidth}>
              <Typography
                className={classes.email}
                variant="body2"
                color="textSecondary"
                title={email}
              >
                {count ? `(${count?.left}/${count?.max} invites left)` : email}
              </Typography>
            </Grid>
            {isTalentCard && (
              <Grid item className={classes.status}>
                {isAccepted ? (
                  <CheckIcon className={classes.check} />
                ) : (
                  <Chip label="pending activation" color="grey" size="small" />
                )}
              </Grid>
            )}
          </Grid>
        </Grid>
        {needShowActions && (
          <Grid item>
            <TalentCardActions invitation={invitation} onSuccess={refetch} />
          </Grid>
        )}
      </Grid>
    </div>
  );
};

export default TalentCard;
