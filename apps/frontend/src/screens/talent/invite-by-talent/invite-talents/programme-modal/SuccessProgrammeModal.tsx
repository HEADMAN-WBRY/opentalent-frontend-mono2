import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
} from '@mui/material';
import React from 'react';

import Button from '@libs/ui/components/button';

import useStyles from './styles';

interface ProgrammeModalProps {
  handleClose: VoidFunction;
  open: boolean;
}

const ProgrammeModal = (props: ProgrammeModalProps) => {
  const { open, handleClose } = props;
  const classes = useStyles();

  return (
    <Dialog
      classes={{ paper: classes.modal }}
      open={open}
      onClose={handleClose}
    >
      <DialogTitle className={classes.title}>Thank you!</DialogTitle>
      <DialogContent>
        <Grid spacing={4} direction="column" container>
          <Grid item>We received your request</Grid>
        </Grid>
      </DialogContent>
      <DialogActions className={classes.actions}>
        <Button
          fullWidth
          variant="contained"
          color="primary"
          autoFocus
          onClick={handleClose}
        >
          Ok
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ProgrammeModal;
