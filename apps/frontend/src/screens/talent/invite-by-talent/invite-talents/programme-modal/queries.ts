import { gql } from '@apollo/client';

export const CREATE_CHIEFS_PROGRAM = gql`
  mutation CreateChiefsProgramApplication($talent_id: ID!) {
    createChiefsProgramApplication(talent_id: $talent_id)
  }
`;
