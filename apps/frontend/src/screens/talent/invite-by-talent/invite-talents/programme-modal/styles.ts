import { makeStyles } from '@mui/styles';

import { Theme } from '@libs/ui/themes/default';

const useStyles = makeStyles((theme: Theme) => ({
  modal: {
    padding: theme.spacing(10),
    textAlign: 'center',
  },
  title: {
    padding: 0,
  },
  actions: {
    paddingTop: theme.spacing(6),
  },
}));

export default useStyles;
