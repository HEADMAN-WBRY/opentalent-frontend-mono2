import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
} from '@mui/material';
import React from 'react';

import Button from '@libs/ui/components/button';

import { useJoinProgrammeModalSubmit } from './hooks';
import useStyles from './styles';

interface ProgrammeModalProps {
  handleClose: VoidFunction;
  open: boolean;
  id: string;
  onSuccess: VoidFunction;
}

const ProgrammeModal = (props: ProgrammeModalProps) => {
  const { open, handleClose, id, onSuccess } = props;
  const classes = useStyles();
  const { onSubmit, loading } = useJoinProgrammeModalSubmit({
    handleClose,
    onSuccess,
    id,
  });

  return (
    <Dialog
      classes={{ paper: classes.modal }}
      open={open}
      onClose={handleClose}
    >
      <DialogTitle className={classes.title}>
        Join our Chief&apos;s programme
      </DialogTitle>
      <DialogContent>
        <Grid spacing={4} direction="column" container>
          <Grid item>
            to build your freelance collective of up to 50 individuals.
          </Grid>
          <Grid item>
            Work together as a team. Make more money as a collective. Become
            known as a talent whisperer.{' '}
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions className={classes.actions}>
        <Button
          disabled={loading}
          fullWidth
          variant="contained"
          color="primary"
          autoFocus
          onClick={onSubmit}
        >
          Join
        </Button>
        <Button
          onClick={handleClose}
          fullWidth
          variant="outlined"
          color="secondary"
        >
          Cancel
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default ProgrammeModal;
