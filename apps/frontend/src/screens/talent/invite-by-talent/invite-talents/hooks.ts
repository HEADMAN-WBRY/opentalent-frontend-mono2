import { useQuery } from '@apollo/client';
import { useCurrentUser } from 'hooks/auth/useCurrentUser';
import qs from 'query-string';
import { useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { pathManager } from 'routes/consts';

import { Query, QueryTalentInvitationsInfoArgs } from '@libs/graphql-types';

import { GET_INVITATION_INFO } from '../queries';

export const useRequestInviteData = () => {
  const { data: userData, isTalent } = useCurrentUser();

  const id = userData?.currentTalent?.id as string;
  const { data, loading, refetch } = useQuery<
    Query,
    QueryTalentInvitationsInfoArgs
  >(GET_INVITATION_INFO, {
    fetchPolicy: 'network-only',
    variables: { inviting_talent_id: id },
    skip: !id || !isTalent,
  });

  return {
    data,
    isInvitationAccepted: userData?.currentTalent?.is_invitation_accepted,
    isLoading: loading,
    id,
    getInvitationRequest: refetch,
  };
};

export const useModalState = (modalId: string) => {
  const history = useHistory();
  const { modal } = qs.parse(history.location.search);
  const handleClose = useCallback(() => {
    history.push(pathManager.talent.invite.generatePath());
  }, [history]);
  const handleOpen = useCallback(() => {
    history.push({
      pathname: pathManager.talent.invite.generatePath(),
      search: qs.stringify({ modal: modalId }),
    });
  }, [history, modalId]);

  return {
    isOpen: !!(modal && modal === modalId),
    handleClose,
    handleOpen,
  };
};
