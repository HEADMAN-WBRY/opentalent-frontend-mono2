import React from 'react';

import {
  CurrentTalentInvitationsQuery,
  TalentInvitationsInfo,
} from '@libs/graphql-types';

import { useModalState } from './hooks';
import HowItWorks from './how-it-works';
import InviteForm from './invite-form';
import ProgrammeModal, { SuccessProgrammeModal } from './programme-modal';

interface ProfileProps {
  talentInvitationsInfo?: TalentInvitationsInfo;
  invites?: CurrentTalentInvitationsQuery['currentTalentInvitations'];
  getInvitationRequest?: any;
  left?: number;
  max?: number;
  id: string;
  refetch: VoidFunction;
}

const InviteByTalent = ({
  getInvitationRequest,
  invites,
  id,
  max = 5,
  left = 5,
  refetch,
}: ProfileProps) => {
  const { isOpen, handleClose, handleOpen } = useModalState('joinProgramme');
  const {
    isOpen: isSuccessOpen,
    handleClose: handleCloseSuccess,
    handleOpen: handleOpenSuccess,
  } = useModalState('joinSuccess');

  return (
    <>
      <InviteForm
        openModal={handleOpen}
        getInvitationRequest={getInvitationRequest}
        getInvites={refetch}
        id={id}
        max={max}
        left={left}
      />
      <HowItWorks
        openModal={handleOpen}
        max={max}
        left={left}
        invites={invites}
        refetch={refetch}
      />
      <ProgrammeModal
        id={id}
        open={isOpen}
        onSuccess={handleOpenSuccess}
        handleClose={handleClose}
      />
      <SuccessProgrammeModal
        open={isSuccessOpen}
        handleClose={handleCloseSuccess}
      />
    </>
  );
};

export default InviteByTalent;
