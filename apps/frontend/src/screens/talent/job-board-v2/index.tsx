import React from 'react';

const JobBoardV2 = React.lazy(() => import('./JobBoardV2'));

export default JobBoardV2;
