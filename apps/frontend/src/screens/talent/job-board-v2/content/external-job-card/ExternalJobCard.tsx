import cn from 'classnames';
import React, { useState } from 'react';

import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import { Box, Paper } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { ExternalJob, MatchQualityEnum } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';

import ExternalJobCardFooter from './JobCardFooter';
import ExternalJobCardTitle from './JobCardTitle';
import JobDescription from './JobDescription';
import JobDetails from './JobDetails';

interface ExternalJobCardProps {
  onJobSave?: VoidFunction;
  job: ExternalJob;
  matchQuality?: MatchQualityEnum;
}

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: theme.spacing(4),
  },
  togglerIcon: {
    transform: ({ isOpen }: { isOpen: boolean }) =>
      `rotate(${isOpen ? 0 : 180}deg)`,
  },
  expired: {
    background: '#FFF9',
  },
}));

const ExternalJobCard = ({
  job,
  onJobSave,
  matchQuality,
}: ExternalJobCardProps) => {
  const [isOpen, setIsOpen] = useState(false);
  const classes = useStyles({ isOpen });
  const handleShowMore = () => {
    setIsOpen((s) => !s);
  };

  const isJobExpired = !job?.application_active || job?.deleted;

  return (
    <Paper
      className={cn(classes.root, {
        [classes.expired]: isJobExpired,
      })}
      elevation={0}
    >
      <Box p={6}>
        <ExternalJobCardTitle
          job={job}
          onJobSave={onJobSave}
          isSaved={job.is_saved}
          matchQuality={matchQuality}
        />
        <JobDetails job={job} isOpen={isOpen} />
        <JobDescription job={job} isOpen={isOpen} />
        <Box display="flex" justifyContent="flex-end">
          <Button
            onClick={handleShowMore}
            color="info"
            variant="text"
            endIcon={
              <KeyboardArrowUpIcon
                className={classes.togglerIcon}
                color="inherit"
              />
            }
          >
            Show more
          </Button>
        </Box>

        <ExternalJobCardFooter
          job={job}
          isOpen={isOpen}
          isJobExpired={isJobExpired}
        />
      </Box>
    </Paper>
  );
};

export default ExternalJobCard;
