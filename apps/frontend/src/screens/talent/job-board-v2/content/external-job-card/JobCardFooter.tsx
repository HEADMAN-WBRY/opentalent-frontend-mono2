import { Box, Grid } from '@mui/material';

import { ExternalJob } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

interface JobCardFooterProps {
  job: ExternalJob;
  isOpen?: boolean;
  isJobExpired?: boolean;
}

const JobCardFooter = ({ job, isJobExpired = false }: JobCardFooterProps) => {
  const link = job?.external_url || job?.company_url;

  return (
    <Grid spacing={2} component={Box} container>
      <Grid item>
        <Button
          disabled={!link || isJobExpired}
          variant="contained"
          {...{ target: '_blank' }}
          color="info"
          href={link}
        >
          Apply to job
        </Button>
      </Grid>
      {isJobExpired && (
        <Grid item display="flex" alignItems="center">
          <Typography color="error">
            This jos is not awailable to apply
          </Typography>
        </Grid>
      )}
    </Grid>
  );
};

export default JobCardFooter;
