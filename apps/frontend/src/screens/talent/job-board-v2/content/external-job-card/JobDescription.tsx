import React from 'react';

import { Box, Collapse } from '@mui/material';

import { ExternalJob } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

interface JobDescriptionProps {
  job: ExternalJob;
  isOpen: boolean;
}

const JobDescription = ({ job, isOpen }: JobDescriptionProps) => {
  return (
    <Collapse in={isOpen}>
      <Box mb={4}>
        <Typography variant="subtitle2">Description</Typography>
        <div
          style={{ paddingTop: 8, whiteSpace: 'break-spaces' }}
          // eslint-disable-next-line react/no-danger
          dangerouslySetInnerHTML={{ __html: job?.description || '' }}
        />
      </Box>
    </Collapse>
  );
};

export default JobDescription;
