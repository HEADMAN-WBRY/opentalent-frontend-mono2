import useMediaQueries from 'hooks/common/useMediaQueries';
import React from 'react';
import { SENIORITY_LABELS } from 'utils/external-jobs';

import { Box, Grid } from '@mui/material';

import { ExternalJob } from '@libs/graphql-types';
import { isNil, isNumber } from '@libs/helpers/common';
import { formatCurrency } from '@libs/helpers/format';
import { formatDate } from '@libs/ui/components/job/utils';
import Typography from '@libs/ui/components/typography';

interface JobDetailsProps {
  job: ExternalJob;
  isOpen: boolean;
}

const getExternalJobFieldsForTalents = (
  job: ExternalJob,
): { label: string; value: string }[] => {
  return [
    {
      label: 'Salary:',
      value: isNumber(job?.salary) ? formatCurrency(job.salary) : '-',
    },
    {
      label: 'Seniority:',
      value: !isNil(job.seniority) ? SENIORITY_LABELS[job.seniority] : '-',
    },
    {
      label: 'Created:',
      value: !isNil(job?.created) ? formatDate(job.created) : '-',
    },
    {
      label: 'Client:',
      value: job?.company_name || '-',
    },
    {
      label: 'City:',
      value: job?.location || '-',
    },
  ];
};

const JobDetails = ({ job, isOpen }: JobDetailsProps) => {
  const items = getExternalJobFieldsForTalents(job);
  const { isSM } = useMediaQueries();

  return (
    <Grid
      component={Box}
      pt={4}
      container
      spacing={2}
      direction={isSM ? 'column' : 'row'}
    >
      {items.map((i) => (
        <Grid xs={12} sm={6} md={4} key={i.label} item>
          <Typography component="span" variant="body2">
            {i.label}
          </Typography>
          {` `}
          <Typography color="textSecondary" component="span" variant="body2">
            {i.value}
          </Typography>
        </Grid>
      ))}
    </Grid>
  );
};

export default JobDetails;
