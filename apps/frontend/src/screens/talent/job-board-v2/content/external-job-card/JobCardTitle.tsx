import useMediaQueries from 'hooks/common/useMediaQueries';
import React from 'react';

import { Box, Grid } from '@mui/material';

import { ExternalJob, MatchQualityEnum } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import { MatchLabel } from './MatchLabel';
import SaveToFavoritesButton from './SaveToFavoritesButton';

interface JobCardTitleProps {
  job: ExternalJob;
  isSaved?: boolean;
  onJobSave?: VoidFunction;
  matchQuality?: MatchQualityEnum;
}

const JobCardTitle = ({
  job,
  isSaved,
  onJobSave,
  matchQuality,
}: JobCardTitleProps) => {
  const { isXS } = useMediaQueries();

  return (
    <Grid
      justifyContent="space-between"
      container
      wrap="nowrap"
      direction={isXS ? 'column' : 'row'}
    >
      <Grid item>
        <Grid
          wrap="nowrap"
          spacing={4}
          style={{ alignItems: 'center' }}
          container
        >
          <Grid item>
            <Typography variant="h6">{job?.title}</Typography>
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <Grid
          wrap="nowrap"
          component={Box}
          pt={isXS ? 3 : 0}
          spacing={4}
          container
        >
          {!!matchQuality && (
            <Grid style={{ display: 'flex', alignItems: 'center' }} item>
              <MatchLabel matchQuality={matchQuality} />
            </Grid>
          )}
          <Grid item>
            <SaveToFavoritesButton
              onJobSave={onJobSave}
              isSaved={isSaved}
              jobId={job.id}
            />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default React.memo(JobCardTitle);
