import { gql } from '@apollo/client';

export const GET_EXTERNAL_COMPANIES_NAMES = gql`
  query GetExternalCompaniesNames($search: String) {
    externalJobCompanies(search: $search)
  }
`;
