import React, { useState } from 'react';

import { Box } from '@mui/material';

import {
  ExternalJobEmploymentTypeEnum,
  ExternalJobSeniorityLevelEnum,
} from '@libs/graphql-types';
import { COUNTRY_OPTIONS } from '@libs/helpers/consts/countries';
import { modelPath } from '@libs/helpers/form';
import Accordion from '@libs/ui/components/accordion/Accordion';
import {
  ConnectedMultipleSelect,
  GraphConnectedMultipleSelect,
} from '@libs/ui/components/form/multiple-select';
import Typography from '@libs/ui/components/typography';

import { FilterForm } from '../types';
import { GET_EXTERNAL_COMPANIES_NAMES } from './query';

interface ExternalJobsFilterProps { }

const EMPLOYMENT_OPTIONS = [
  {
    text: 'Full Time',
    value: ExternalJobEmploymentTypeEnum.FullTime,
  },
  {
    text: 'Part Time',
    value: ExternalJobEmploymentTypeEnum.PartTime,
  },
  {
    text: 'Contract',
    value: ExternalJobEmploymentTypeEnum.Contract,
  },
  {
    text: 'Internship',
    value: ExternalJobEmploymentTypeEnum.Internship,
  },
  {
    text: 'Temporary',
    value: ExternalJobEmploymentTypeEnum.Temporary,
  },
  {
    text: 'Volunteer',
    value: ExternalJobEmploymentTypeEnum.Volunteer,
  },
  {
    text: 'Other',
    value: ExternalJobEmploymentTypeEnum.Other,
  },
];

const SENIORITY_OPTIONS = [
  {
    text: 'Intern',
    value: ExternalJobSeniorityLevelEnum.Internship,
  },
  {
    text: 'Entry Level',
    value: ExternalJobSeniorityLevelEnum.EntryLevel,
  },
  {
    text: 'Associate',
    value: ExternalJobSeniorityLevelEnum.Associate,
  },
  {
    text: 'Mid-Senior Level',
    value: ExternalJobSeniorityLevelEnum.MidSeniorLevel,
  },
  {
    text: 'Executive',
    value: ExternalJobSeniorityLevelEnum.Executive,
  },
  {
    text: 'Director',
    value: ExternalJobSeniorityLevelEnum.Director,
  },
];

export const ExternalJobsFilter = (props: ExternalJobsFilterProps) => {
  const [companySearch, setCompanySearch] = useState('');

  return (
    <div>
      <Accordion
        defaultExpanded
        summary={
          <Typography variant="overline">FILTER BY TYPE OF ROLE</Typography>
        }
        details={
          <Box flexGrow={1} mb={2}>
            <ConnectedMultipleSelect
              name={modelPath<FilterForm>((m) => m.externalJobEmployment)}
              options={EMPLOYMENT_OPTIONS}
              chipProps={{
                color: 'tertiary',
                size: 'small',
              }}
              inputProps={{
                variant: 'filled',
                label: 'Pick from list',
                placeholder: '',
                margin: 'dense',
                fullWidth: true,
              }}
            />
          </Box>
        }
      />
      <Accordion
        defaultExpanded
        summary={
          <Typography variant="overline">FILTER BY SENIORITY</Typography>
        }
        details={
          <Box flexGrow={1} mb={2}>
            <ConnectedMultipleSelect
              name={modelPath<FilterForm>((m) => m.externalJobSeniority)}
              options={SENIORITY_OPTIONS}
              chipProps={{
                color: 'tertiary',
                size: 'small',
              }}
              inputProps={{
                variant: 'filled',
                label: 'Pick from list',
                placeholder: '',
                margin: 'dense',
                fullWidth: true,
              }}
            />
          </Box>
        }
      />
      <Accordion
        defaultExpanded
        summary={<Typography variant="overline">FILTER BY COUNTRY</Typography>}
        details={
          <Box flexGrow={1} mb={2}>
            <ConnectedMultipleSelect
              name={modelPath<FilterForm>((m) => m.country)}
              options={COUNTRY_OPTIONS}
              chipProps={{
                color: 'tertiary',
                size: 'small',
              }}
              autoCompleteProps={{
                multiple: false,
              }}
              inputProps={{
                variant: 'filled',
                label: 'Pick from list',
                placeholder: '',
                margin: 'dense',
                fullWidth: true,
              }}
            />
          </Box>
        }
      />
      <Accordion
        defaultExpanded
        summary={<Typography variant="overline">FILTER BY COMPANY</Typography>}
        details={
          <Box flexGrow={1} mb={2}>
            <GraphConnectedMultipleSelect
              name={modelPath<FilterForm>((m) => m.externalCompanyName)}
              query={GET_EXTERNAL_COMPANIES_NAMES}
              dataPath="externalJobCompanies"
              dataMap={(name) => ({ text: name, value: name })}
              autoCompleteProps={{
                multiple: false,
              }}
              queryOptions={{
                variables: {
                  search: companySearch,
                },
              }}
              chipProps={{
                color: 'tertiary',
                size: 'small',
              }}
              inputProps={{
                variant: 'filled',
                label: 'Pick from list',
                placeholder: '',
                margin: 'dense',
                onChange: (e: any) => setCompanySearch(e?.target?.value || ''),
              }}
            />
          </Box>
        }
      />
    </div>
  );
};
