import { useQuery } from '@apollo/client';

import { GET_JOBS_COUNT } from '../queries';

export const useJobCountsForFilter = () => {
  const { data } = useQuery(GET_JOBS_COUNT);

  const directJobsCount = (data as any)?.direct?.paginatorInfo?.total;
  const externalJobsCount = (data as any)?.external?.paginatorInfo?.total;

  return {
    directJobsCount,
    externalJobsCount,
  };
};
