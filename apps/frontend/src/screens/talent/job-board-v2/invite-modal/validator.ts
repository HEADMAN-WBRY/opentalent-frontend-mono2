import * as yup from 'yup';

import errors from 'consts/validationErrors';

export default yup.object().shape({
  name: yup.string().required(errors.required),
  email: yup.string().email(errors.invalidEmail).required(errors.required),
});
