import React from 'react';

import { Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

interface TitleLineProps { }

const useStyles = makeStyles((theme) => ({
  title: {
    fontWeight: 500,
    fontSize: '24px',
    lineHeight: '32px',
  },
}));

const TitleLine = (props: TitleLineProps) => {
  const classes = useStyles();

  return (
    <Grid container>
      <Grid flexGrow={1} item>
        <Typography className={classes.title}>Job Board</Typography>
      </Grid>
    </Grid>
  );
};

export default TitleLine;
