import React from 'react';

const Auth = React.lazy(() => import('./Auth'));

export default Auth;
