import { gql } from '@apollo/client';
import { ABAC_PROPERTIES } from 'graphql/fragments/talent/abacFields';
import FULL_TALENT_FRAGMENT from 'graphql/fragments/talent/fullTalentFragment';

export const GET_JOBS_TOTAL = gql`
  query GetJobsTotal($id: ID!) {
    jobs {
      paginatorInfo {
        total
      }
    }
  }
`;

export const GET_JOBS = gql`
  query GetJobs($talent_id: ID!) {
    availableJobsForTalent(talent_id: $talent_id) {
      job {
        id
        name
      }
      is_applied
      is_invited
    }
  }
`;

export const INVITE_TO_JOB = gql`
  mutation InviteToJob($job_id: ID!, $talent_id: ID!, $message: String) {
    inviteToJob(talent_id: $talent_id, job_id: $job_id, message: $message)
  }
`;

export const COMPANY_FLOW_PROFILE_DATA = gql`
  ${FULL_TALENT_FRAGMENT}
  ${ABAC_PROPERTIES}
  query GetCompanyFlowProfileData($company_id: ID!, $talent_id: ID!) {
    talent(id: $talent_id) {
      ...FullTalent
      first_name_abac {
        ...AbacProps
      }
      last_name_abac {
        ...AbacProps
      }
      is_matcher
      category {
        id
        name
      }
      subcategories {
        id
        name
      }
      tags {
        id
        name
      }
    }
    currentUserCompanyTags {
      id
      name
    }
    jobs(
      company_id: $company_id
      is_archived: false
      is_draft: false
      first: 500
      page: 1
    ) {
      data {
        id
        name
      }
      paginatorInfo {
        total
      }
    }
  }
`;
