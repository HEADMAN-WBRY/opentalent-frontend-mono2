import useMediaQueries from 'hooks/common/useMediaQueries';
import React from 'react';

import { Box, Grid, Paper } from '@mui/material';

import { Talent, TalentCompanyPoolConnection } from '@libs/graphql-types';

import ActionBlock from './action-block';
import InfoBlock from './info-block';
import useStyles from './styles';
import UserBlock from './user-block';

interface ProfileHeaderProps {
  isTalentFlow: boolean;
  talent?: Partial<Talent>;
  refetchTalent: VoidFunction;
  connection?: TalentCompanyPoolConnection;
}

const ProfileHeader = ({
  isTalentFlow,
  talent = {},
  refetchTalent,
  connection,
}: ProfileHeaderProps) => {
  const { isSM, isXS } = useMediaQueries();
  const classes = useStyles();

  return (
    <Box mb={4}>
      <Paper elevation={0} component={Box} p={4}>
        <Grid
          direction={isSM ? 'column' : 'row'}
          justifyContent="space-between"
          container
          spacing={4}
          wrap="nowrap"
        >
          <Grid item component={Box}>
            <UserBlock isXS={isXS} talent={talent} />
          </Grid>
          <Grid item component={Box} display="flex" alignItems="center">
            <InfoBlock
              isXS={isXS}
              isSM={isSM}
              isTalentFlow={isTalentFlow}
              talent={talent}
            />
          </Grid>
          <Grid className={classes.actionContainer} sm="auto" item>
            <ActionBlock
              refetchTalent={refetchTalent}
              talent={talent}
              isTalentFlow={isTalentFlow}
              connection={connection}
            />
          </Grid>
        </Grid>
      </Paper>
    </Box>
  );
};

export default ProfileHeader;
