import { ReactComponent as CheckIcon } from 'assets/icons/check.svg';
import { ReactComponent as EuroIcon } from 'assets/icons/euro.svg';
import { ReactComponent as MapPinIcon } from 'assets/icons/map-pin.svg';
import cn from 'classnames';
import { useOpenDirectSubscriptionModal } from 'components/custom/company/modals/direct';
import { DEFAULT_AVATAR } from 'consts/common';
import { JOIN_REASON_LABELS } from 'consts/talents';
import { useCurrentUser } from 'hooks/auth';
import React, { useMemo } from 'react';
import { isVerifiedTalent } from 'utils/talent';
import { formatName } from 'utils/talent';

import { Avatar, Grid, Tooltip, Badge, Chip } from '@mui/material';
import Box from '@mui/material/Box';

import { Talent } from '@libs/graphql-types';
import { formatRate } from '@libs/ui/components/job/utils';
import Typography from '@libs/ui/components/typography';

import useStyles from './styles';

export interface UserBlockProps {
  talent: Partial<Talent> | null;
  isXS: boolean;
}

const UserBlock = (props: UserBlockProps) => {
  const classes = useStyles(props);
  const openLimitedAccessModal = useOpenDirectSubscriptionModal();
  const { talent, isXS } = props;
  const { isCompany } = useCurrentUser();
  const rateHour = formatRate({ min: talent?.rate_min, period: 'hour' });
  const rateMonth = formatRate({ min: talent?.salary, period: 'month' });
  const talentName = formatName({
    firstName: isCompany ? talent?.first_name_abac?.value : talent?.first_name,
    lastName: isCompany ? talent?.last_name_abac?.value : talent?.last_name,
  });
  const isNameHidden = talentName.includes('...');
  const showTalentMatcherBadge = !!talent?.is_matcher;
  const isTalentVerified = isVerifiedTalent((talent as Talent) ?? undefined);

  const isVerificationCheckShown = useMemo(
    () => talent?.is_invitation_accepted && isTalentVerified,
    [talent?.is_invitation_accepted, isTalentVerified],
  );

  const onNameClick = () => {
    if (isNameHidden) {
      openLimitedAccessModal();
    }
  };

  return (
    <Grid
      className={classes.root}
      direction={isXS ? 'column' : 'row'}
      spacing={6}
      container
      wrap="nowrap"
    >
      <Grid item justifyContent={isXS ? 'center' : undefined}>
        <Badge
          overlap="circular"
          classes={{ badge: classes.avatarBadge }}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          variant="standard"
          badgeContent={
            <Box>
              <Tooltip
                placement="right-end"
                open={talent?.available_now ? undefined : false}
                title={
                  talent?.hours_per_week
                    ? `Available now for ${talent.hours_per_week} hours per week`
                    : "Available now - let's get to work!"
                }
              >
                <Box width={22} height={22} />
              </Tooltip>
            </Box>
          }
        >
          <Avatar
            className={classes.avatar}
            src={talent?.avatar?.avatar || DEFAULT_AVATAR}
          />
        </Badge>
      </Grid>
      <Grid item>
        <Grid wrap="nowrap" direction="column" container>
          <Grid
            justifyContent={isXS ? 'center' : undefined}
            spacing={2}
            item
            container
            style={{ flexWrap: 'nowrap', alignItems: 'center' }}
          >
            <Grid item>
              <Typography
                onClick={onNameClick}
                className={cn(classes.name, {
                  [classes.pointer]: isNameHidden,
                })}
                noWrap
                variant="h5"
              >
                {talentName}
              </Typography>
            </Grid>
            {isVerificationCheckShown && (
              <Grid item>
                <Tooltip title="This person was verified by Opentalent through interviews, social media and/or legal document checks.">
                  <CheckIcon className={classes.checkIcon} />
                </Tooltip>
              </Grid>
            )}
            {!isTalentVerified && (
              <Typography
                variant="subtitle2"
                className={classes.pendingVerification}
              >
                pending verification
              </Typography>
            )}
            {showTalentMatcherBadge && (
              <Grid item>
                <Chip
                  size="small"
                  label="Talent Matcher"
                  className={classes.recruiterChip}
                />
              </Grid>
            )}
          </Grid>
          <Grid item>
            <Typography
              align={isXS ? 'center' : undefined}
              variant="body2"
              color="textSecondary"
              className={classes.positionTitle}
            >
              {talent?.recent_position_title}
            </Typography>
          </Grid>
          {!!talent?.join_reason?.length && (
            <Grid item>
              <Box mt={1}>
                <Grid container spacing={2}>
                  <Grid item>
                    <Typography variant="body2" color="textSecondary">
                      Looking for:
                    </Typography>
                  </Grid>
                  {talent.join_reason.map((i) => (
                    <Grid item key={i}>
                      <Chip
                        label={JOIN_REASON_LABELS[i!]}
                        size="small"
                        color="success"
                        style={{ color: 'black' }}
                      />
                    </Grid>
                  ))}
                </Grid>
              </Box>
            </Grid>
          )}
          <Grid className={classes.info} item>
            <Grid
              justifyContent={isXS ? 'center' : undefined}
              alignItems="stretch"
              spacing={2}
              container
            >
              {!!rateHour && (
                <>
                  <Grid item>
                    <EuroIcon />
                  </Grid>
                  <Grid item>
                    <Typography variant="body2" color="textSecondary">
                      {rateHour}
                    </Typography>
                  </Grid>
                </>
              )}
              <Grid item>&nbsp;</Grid>
              {!!rateMonth && (
                <>
                  <Grid item>
                    <EuroIcon />
                  </Grid>
                  <Grid item>
                    <Typography variant="body2" color="textSecondary">
                      {rateMonth}
                    </Typography>
                  </Grid>
                </>
              )}
              <Grid item>&nbsp;</Grid>
              {talent?.location && (
                <>
                  <Grid item>
                    <MapPinIcon color="disabled" />
                  </Grid>
                  <Grid item>
                    <Typography
                      className={classes.location}
                      variant="body2"
                      color="textSecondary"
                    >
                      {talent?.location}
                    </Typography>
                  </Grid>
                </>
              )}
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default UserBlock;
