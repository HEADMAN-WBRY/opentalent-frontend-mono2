import { makeStyles } from '@mui/styles';

import { Theme } from '@libs/ui/themes/default';

import { UserBlockProps } from './UserBlock';

const useStyles = makeStyles((theme: Theme) => ({
  root: {},
  pointer: {
    cursor: 'pointer',
  },
  avatar: {
    width: theme.spacing(24),
    height: theme.spacing(24),
    border: `2px solid ${theme.palette.secondary.contrastText}`,
    [theme.breakpoints.down('sm')]: {
      marginTop: theme.spacing(4),
    },
  },
  recruiterChip: {
    background: theme.palette.primary.light,
  },
  avatarBadge: {
    background: ({ talent }: UserBlockProps) =>
      talent?.available_now
        ? theme.palette.primary.light
        : theme.palette.grey[500],
    border: `2px solid ${theme.palette.grey[200]}`,
    width: 22,
    height: 22,
  },
  positionTitle: {
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
    margin: '0 auto',

    [theme.breakpoints.down('sm')]: {
      whiteSpace: 'normal',
    },
  },
  info: {
    paddingTop: theme.spacing(2),
  },
  name: {
    flexShrink: 0,
  },
  location: {
    letterSpacing: 0.15,
  },
  checkIcon: {
    marginBottom: -6,
    color: theme.palette.info.main,
    cursor: 'pointer',
  },
  pendingVerification: {
    backgroundColor: theme.palette.warning.dark,
    color: theme.palette.secondary.contrastText,
    borderRadius: '16px',
    padding: '0 6px',
    lineHeight: '20px',
    height: '20px',
    fontSize: '12px',
    marginLeft: theme.spacing(4),
    whiteSpace: 'nowrap',
  },
}));

export default useStyles;
