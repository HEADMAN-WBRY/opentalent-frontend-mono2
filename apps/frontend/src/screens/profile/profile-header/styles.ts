import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
  actionContainer: {
    display: 'flex',
    alignItems: 'center',

    [theme.breakpoints.down('md')]: {
      padding: '0 !important',
    },
  },
}));

export default useStyles;
