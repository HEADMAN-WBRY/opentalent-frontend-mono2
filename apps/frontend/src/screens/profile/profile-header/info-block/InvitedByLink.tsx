import React from 'react';
import { Link } from 'react-router-dom';
import { pathManager } from 'routes/consts';
import { formatName } from 'utils/talent';

import { TalentInvitationTypeEnum, TalentInvitedBy } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

interface InvitedByLinkProps {
  invitedBy: TalentInvitedBy;
  isTalentFlow: boolean;
}

const InvitedByLink = ({ invitedBy, isTalentFlow }: InvitedByLinkProps) => {
  const needLink =
    invitedBy.invitation_type ===
    TalentInvitationTypeEnum.InvitationTypeByTalent &&
    !isTalentFlow &&
    !!invitedBy.id;
  const link = pathManager.company.talentProfile.generatePath({
    id: invitedBy.id || '',
  });
  const name = formatName({
    firstName: invitedBy.first_name_abac?.value || '[hidden]',
    lastName: invitedBy.last_name_abac?.value,
  });

  return (
    <Typography
      variant="subtitle1"
      color={needLink ? 'tertiary' : 'textPrimary'}
      style={{ textDecoration: needLink ? 'underline' : undefined }}
    >
      {needLink ? <Link to={link}>{name}</Link> : name}
    </Typography>
  );
};

export default InvitedByLink;
