import { makeStyles } from '@mui/styles';

import { Theme } from '@libs/ui/themes/default';

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    padding: theme.spacing(4),
  },
  mbInviteButton: {
    marginBottom: theme.spacing(4),
  },
  sourcedInfo: {
    marginRight: theme.spacing(8),
  },
  sourcedAvatar: {
    width: 80,
    height: 80,
    background: 'white',
    border: '3px solid #EEEEEE',

    '& img': {
      objectFit: 'contain',
    },
  },
  sourcedTooltip: {
    position: 'relative',
    margin: 0,
    maxWidth: 500,
    bottom: -10,
  },
  sourcedTooltipPlacementLeft: {
    left: 10,
  },
  sourcedTooltipPlacementRight: {
    left: -10,
  },
  infoBlock: {
    flexWrap: 'nowrap',
  },
  invitedBlock: {
    height: '100%',
  },
}));

export default useStyles;
