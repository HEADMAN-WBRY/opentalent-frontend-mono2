import React from 'react';
import { Link } from 'react-router-dom';
import { pathManager } from 'routes/consts';

import EditIcon from '@mui/icons-material/Edit';
import { Box, Button } from '@mui/material';

import { Talent, TalentCompanyPoolConnection } from '@libs/graphql-types';

import ContactTalentMenu from './ContactTalentMenu';
import useStyles from './styles';

interface ActionBlockProps {
  isTalentFlow: boolean;
  talent: Partial<Talent>;
  refetchTalent: VoidFunction;
  connection?: TalentCompanyPoolConnection;
}

const ActionBlock = ({
  isTalentFlow,
  connection,
  talent,
  refetchTalent,
}: ActionBlockProps) => {
  const classes = useStyles();

  return (
    <Box className={classes.container}>
      {isTalentFlow ? (
        <Link to={pathManager.talent.editProfile.generatePath()}>
          <Button
            endIcon={<EditIcon fontSize="inherit" />}
            variant="outlined"
            color="secondary"
            size="small"
          >
            Edit profile
          </Button>
        </Link>
      ) : (
        <ContactTalentMenu
          refetchUser={refetchTalent}
          talent={talent}
          connection={connection}
        />
      )}
    </Box>
  );
};

export default ActionBlock;
