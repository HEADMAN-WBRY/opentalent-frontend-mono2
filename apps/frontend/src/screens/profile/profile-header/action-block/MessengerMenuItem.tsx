import { ReactComponent as SendIcon } from 'assets/icons/message.svg';
import { useStreamChatContext } from 'components/chat/common/provider';
import { useOpenDirectSubscriptionModal } from 'components/custom/company/modals/direct';
import { useHaveAccessToTalent } from 'hooks/talents';
import React, { useCallback, useEffect, useState } from 'react';
import { useOpenChatModal } from 'screens/profile/modals/chat-with-talent';

import { Talent } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';

interface MessengerMenuItemProps {
  closePopup: VoidFunction;
  talent: Talent;
  disabled?: boolean;
}

const useMessengerStatus = (streamChatId: string) => {
  const [isEnabled, setIsEnabled] = useState(false);
  const { client } = useStreamChatContext();

  const queryTalent = useCallback(async () => {
    if (!streamChatId) {
      return;
    }

    const res = await client.queryUsers({
      id: {
        $eq: streamChatId,
      },
    });
    setIsEnabled(!!res.users.length);
  }, [client, streamChatId]);

  useEffect(() => {
    queryTalent();
  }, [queryTalent]);

  return isEnabled;
};

const MessengerMenuItem = ({
  closePopup,
  talent,
  disabled,
}: MessengerMenuItemProps) => {
  const hasAccess = useHaveAccessToTalent(talent as Talent);
  const openStartChatting = useOpenChatModal();
  const isEnabled = useMessengerStatus(talent.stream_chat_id || '');
  const openModal = useOpenDirectSubscriptionModal();

  return (
    <Button
      variant="contained"
      color="primary"
      style={{ minWidth: '150px' }}
      size="medium"
      fullWidth
      disabled={!isEnabled || disabled}
      endIcon={<SendIcon />}
      onClick={() => {
        if (hasAccess) {
          openStartChatting({ talent });
          closePopup();
        } else {
          openModal();
        }
      }}
    >
      Message
    </Button>
  );
};

export default MessengerMenuItem;
