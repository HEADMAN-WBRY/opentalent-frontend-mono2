import { TalentPoolActions } from 'components/custom/company/talent-pool-actions';
import { ActionItem } from 'components/custom/company/types';
import { TalentActions } from 'components/custom/talent/actions-menu';
import { usePopupState } from 'material-ui-popup-state/hooks';
import React from 'react';
import { COMPANY_RESTRICTION_TOOLTIP_TEXT } from 'screens/profile/consts';
import { isVerifiedTalent } from 'utils/talent';

import { Grid, Tooltip } from '@mui/material';

import { Talent, TalentCompanyPoolConnection } from '@libs/graphql-types';

import MessengerMenuItem from './MessengerMenuItem';

interface ContactTalentMenuProps {
  talent: Partial<Talent>;
  refetchUser?: VoidFunction;
  connection?: TalentCompanyPoolConnection;
}

const ContactTalentMenu = ({
  talent,
  refetchUser,
  connection,
}: ContactTalentMenuProps) => {
  const isTalentVerified = isVerifiedTalent(talent as Talent);
  const canMessage = !!connection;
  const popupState = usePopupState({
    variant: 'popper',
    popupId: 'demoPopper',
  });

  const filterActions = (actions: ActionItem[]): ActionItem[] => {
    if (!isTalentVerified) {
      return actions.filter(
        (action) => !['Message', 'Invite to apply'].includes(action.text),
      );
    }
    return actions.filter((action) => !['Message'].includes(action.text));
  };

  return (
    <Grid container spacing={4} direction="column" wrap="nowrap">
      <Grid item>
        <Tooltip
          title={COMPANY_RESTRICTION_TOOLTIP_TEXT}
          open={canMessage ? false : undefined}
          followCursor
        >
          <div>
            <MessengerMenuItem
              talent={talent as Talent}
              disabled={!canMessage}
              closePopup={popupState.close}
            />
          </div>
        </Tooltip>
      </Grid>
      <Grid style={{ display: 'grid' }} item>
        {!!connection ? (
          <TalentActions
            talent={talent as Talent}
            refetch={refetchUser}
            filterActions={filterActions}
          />
        ) : (
          <Tooltip title="Connect this person to My Community" followCursor>
            <div style={{ width: '100%' }}>
              <TalentPoolActions
                talent={talent as Talent}
                refetch={refetchUser}
              />
            </div>
          </Tooltip>
        )}
      </Grid>
    </Grid>
  );
};

export default ContactTalentMenu;
