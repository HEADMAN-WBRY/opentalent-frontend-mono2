import cn from 'classnames';
import { ChatTypes } from 'components/chat';
import { ChannelHeader } from 'components/chat/common/channel-header';
import { DEFAULT_MASSAGE_ACTIONS_LIST } from 'components/chat/common/message';
import { useStreamChatContext } from 'components/chat/common/provider';
import { CHAT_STYLE_VARS, streamI18nInstance } from 'components/chat/consts';
import { useChatStyles } from 'components/chat/styles';
import { TalentModals } from 'components/custom/talent/types';
import React from 'react';
import {
  Channel,
  Thread,
  Chat,
  Window,
  MessageList,
  MessageInput,
} from 'stream-chat-react';
import {
  DefaultModalProps,
  useOpenModal,
  withLocationStateModal,
} from 'utils/modals';

import { Dialog, Alert } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { Talent } from '@libs/graphql-types';

import { useCreateChatWithTalent } from './hooks';

const useStyles = makeStyles(() => ({
  root: {
    zIndex: 1500,
  },
  paper: {
    width: 620,

    '& .str-chat__main-panel': {
      padding: '0 !important',
    },
  },
  container: {
    justifyContent: 'flex-end',
  },
  content: {
    textAlign: 'left',
  },
}));

interface ModalData {
  talent: Talent;
  chatType?: ChatTypes;
}

interface ChatWithTalentProps extends DefaultModalProps<ModalData> { }

export const ChatWithTalentModalComponent = ({
  modalData,
  isOpen,
  close,
}: ChatWithTalentProps) => {
  const talent = modalData?.talent as Talent;
  const chatType = modalData?.chatType;
  const classes = useStyles();
  const chatClasses = useChatStyles();
  const { client } = useStreamChatContext();
  const { channel } = useCreateChatWithTalent({ talent, chatType });

  return (
    <Dialog
      classes={{
        root: classes.root,
        paper: cn(classes.paper, chatClasses.chat),
        container: classes.container,
      }}
      fullScreen
      open={isOpen}
      onClose={close}
    >
      {!channel && <Alert color="info">Connecting to the channel</Alert>}
      {channel && (
        <Chat
          client={client}
          defaultLanguage="en"
          i18nInstance={streamI18nInstance}
          customStyles={CHAT_STYLE_VARS}
        >
          <Channel channel={channel}>
            <Window>
              <ChannelHeader toggleMobileView={close} />
              <MessageList messageActions={DEFAULT_MASSAGE_ACTIONS_LIST} />
              <MessageInput />
            </Window>
            <Thread />
          </Channel>
        </Chat>
      )}
    </Dialog>
  );
};

export const ChatWithTalentModal = withLocationStateModal<ModalData>({
  id: TalentModals.Chat,
})(ChatWithTalentModalComponent);

export const useOpenChatModal = () =>
  useOpenModal<ModalData>(TalentModals.Chat);
