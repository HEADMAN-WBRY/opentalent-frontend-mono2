import { TalentModals } from 'components/custom/talent/types';
import { Formik, FormikConfig } from 'formik';
import { usePushWithQuery } from 'hooks/routing';
import React from 'react';
import {
  DefaultModalProps,
  useOpenModal,
  withLocationStateModal,
} from 'utils/modals';
import * as yup from 'yup';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { modelPath } from '@libs/helpers/form';
import Button from '@libs/ui/components/button';
import { ConnectedGraphSelect } from '@libs/ui/components/form/select';
import { DefaultModal } from '@libs/ui/components/modals';
import Typography from '@libs/ui/components/typography';

import { GET_JOBS } from '../queries';
import { mapJobsToOptions } from './utils';

const useStyles = makeStyles((theme) => ({
  paper: {
    width: 620,
  },
  content: {
    textAlign: 'left',
  },
}));

const validation = yup.object().shape({
  jobId: yup.string().nullable().trim().required(),
});

interface FormModel {
  jobId: string;
}

interface ModalData {
  talentId: string;
}

interface StartChattingModalProps extends DefaultModalProps<ModalData> {}

const StartChattingModalComponent = ({
  modalData,
  close,
  isOpen,
}: StartChattingModalProps) => {
  const talentId = modalData?.talentId || '';
  const classes = useStyles();
  const push = usePushWithQuery();
  const onCreate: FormikConfig<FormModel>['onSubmit'] = ({ jobId }) =>
    push({
      query: { jobId, modal: TalentModals.Chat },
    });

  return (
    <Formik<FormModel>
      initialValues={{ jobId: '' }}
      onSubmit={onCreate}
      validationSchema={validation}
      key={TalentModals.StartChatting}
    >
      {({ submitForm }) => (
        <DefaultModal
          actions={
            <Grid container spacing={4}>
              <Grid style={{ flexGrow: 1 }} item>
                <Button
                  fullWidth
                  color="primary"
                  variant="contained"
                  size="large"
                  onClick={submitForm}
                >
                  Start the Dialog
                </Button>
              </Grid>
              <Grid style={{ flexGrow: 2 }} item>
                <Button
                  size="large"
                  fullWidth
                  variant="outlined"
                  onClick={close}
                >
                  Cancel
                </Button>
              </Grid>
            </Grid>
          }
          handleClose={close}
          open={isOpen}
          title="Dialog with Talent"
          className={classes.paper}
        >
          <div className={classes.content}>
            <Box pb={4} pt={6}>
              <Typography>
                Choose an active Job to discuss with this person.
              </Typography>
            </Box>
            <ConnectedGraphSelect
              query={GET_JOBS}
              dataPath="availableJobsForTalent"
              dataMap={mapJobsToOptions}
              formControlProps={{ size: 'small' }}
              fullWidth
              name={modelPath<FormModel>((m) => m.jobId)}
              variant="filled"
              label="Job name"
              queryOptions={{
                variables: {
                  talent_id: talentId,
                },
              }}
            />
          </div>
        </DefaultModal>
      )}
    </Formik>
  );
};

export const StartChattingModal = withLocationStateModal<ModalData>({
  id: TalentModals.StartChatting,
})(StartChattingModalComponent);

export const useOpenStartChattingModal = () =>
  useOpenModal<ModalData>(TalentModals.StartChatting);
