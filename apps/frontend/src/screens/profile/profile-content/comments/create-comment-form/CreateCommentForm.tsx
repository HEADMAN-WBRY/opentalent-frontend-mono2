import { ReactComponent as CommentsIcon } from 'assets/icons/comments.svg';
import validationErrors from 'consts/validationErrors';
import { Formik, Form } from 'formik';
import useMediaQueries from 'hooks/common/useMediaQueries';
import React from 'react';
import * as yup from 'yup';

import { Grid } from '@mui/material';

import Button from '@libs/ui/components/button';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import Typography from '@libs/ui/components/typography';

import useStyles from '../styles';

interface CreateCommentFormProps {
  createMessage: any;
  isLoading: boolean;
  noConnection?: boolean;
}

const validator = yup.object().shape({
  message: yup.string().required(validationErrors.required),
});

const CreateCommentForm = ({
  createMessage,
  isLoading,
  noConnection = true,
}: CreateCommentFormProps) => {
  const { isSM } = useMediaQueries();
  const classes = useStyles();

  return (
    <Formik
      validateOnBlur={false}
      initialValues={{ message: '' }}
      validationSchema={validator}
      onSubmit={createMessage}
    >
      {({ handleSubmit }) => (
        <Form>
          <Grid
            container
            direction={isSM ? 'column' : 'row'}
            alignItems={isSM ? undefined : 'center'}
            spacing={4}
            className={classes.postCommentWrapper}
          >
            <Grid md={8} item>
              <ConnectedTextField
                name="message"
                fullWidth
                disabled={noConnection}
                label={
                  <Grid container>
                    <CommentsIcon className={classes.commentsIcon} />
                    <Typography variant="body1" color="textSecondary">
                      Post a comment...
                    </Typography>
                  </Grid>
                }
                variant="filled"
              />
            </Grid>
            <Grid xs={8} sm={4} md={4} item>
              <Button
                disabled={isLoading || noConnection}
                onClick={() => handleSubmit()}
                fullWidth
                size="large"
                variant="contained"
                color="secondary"
                type="submit"
              >
                POST A COMMENT
              </Button>
            </Grid>
          </Grid>
        </Form>
      )}
    </Formik>
  );
};

export default CreateCommentForm;
