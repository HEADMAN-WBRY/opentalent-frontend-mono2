import { ReactComponent as CommentsIcon } from 'assets/icons/comments.svg';
import { useGetCompanyPoolConnectionToTalent } from 'hooks/company';
import React, { useRef } from 'react';

import InfoIcon from '@mui/icons-material/Info';
import { Grid, Paper, Tooltip, Zoom } from '@mui/material';

import { Talent } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import CommentsList from './comments-list';
import CreateCommentForm from './create-comment-form';
import { useAddChatMessage, useScrollToListBottom } from './hooks';
import useStyles from './styles';

export interface CommentsProps {
  talent?: Partial<Talent> | null;
}

const Comments = ({ talent }: CommentsProps) => {
  const classes = useStyles();
  const listRef = useRef();
  const connection = useGetCompanyPoolConnectionToTalent({ talent });
  const scrollToListBottom = useScrollToListBottom(listRef);
  const talentId = talent?.id || '';

  const {
    createMessage,
    loading: addMessageLoading,
    newComments,
  } = useAddChatMessage({
    talentId,
    scrollToListBottom,
  });

  return (
    <Paper className={classes.paperRoot} elevation={0}>
      <Grid className={classes.titleRoot} container alignItems="center">
        <Grid item className={classes.commentsIconRoot}>
          <CommentsIcon className={classes.commentsIcon} />
        </Grid>
        <Grid item>
          <Typography variant="h6">Notes</Typography>
        </Grid>
        <Grid item>
          <Grid container alignItems="center">
            <Tooltip
              style={{ marginLeft: '5px' }}
              TransitionComponent={Zoom}
              title="Take some notes - for inter-company use"
              placement="right"
            >
              <InfoIcon fontSize="small" color="disabled" />
            </Tooltip>
          </Grid>
        </Grid>
      </Grid>
      <CommentsList
        talentId={talentId}
        listRef={listRef}
        newComments={newComments}
        scrollToBottom={scrollToListBottom}
      />
      <CreateCommentForm
        isLoading={addMessageLoading}
        createMessage={createMessage}
        noConnection={!connection}
      />
    </Paper>
  );
};

export default Comments;
