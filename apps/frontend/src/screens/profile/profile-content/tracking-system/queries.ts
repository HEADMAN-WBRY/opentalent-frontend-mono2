import { gql } from '@apollo/client';

export const GET_ATS_RECORDS = gql`
  query GetATSRecords($talent_id: ID!) {
    ATSRecords(talent_id: $talent_id) {
      id
      type
      actor {
        first_name
        last_name
        avatar {
          avatar
        }
      }
      date
      __typename
      ... on ATSRecordComment {
        text
      }

      ... on ATSRecordJobMatchAction {
        job_match_action {
          id
          job_match_action_type
          job_match_type_before
          job_match_type_after
          job_match {
            job {
              id
              name
            }
          }
        }
      }
    }
  }
`;
