import validationErrors from 'consts/validationErrors';
import { Formik, Form } from 'formik';
import { useGetCompanyPoolConnectionToTalent } from 'hooks/company';
import React from 'react';
import { COMPANY_RESTRICTION_TOOLTIP_TEXT } from 'screens/profile/consts';
import * as yup from 'yup';

import { Grid, Tooltip } from '@mui/material';

import { Talent } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import Typography from '@libs/ui/components/typography';

import { useCreateComment } from '../hooks';

interface CreateCommentFormProps {
  talent: Talent;
  onSubmit: VoidFunction;
}

const validator = yup.object().shape({
  message: yup.string().required(validationErrors.required),
});

const CreateCommentForm = ({ talent, onSubmit }: CreateCommentFormProps) => {
  const { createComment, isLoading } = useCreateComment(talent);
  const connection = useGetCompanyPoolConnectionToTalent({ talent });

  return (
    <Formik
      validateOnBlur={false}
      initialValues={{ message: '' }}
      validationSchema={validator}
      onSubmit={async ({ message }, helpers) => {
        await createComment(message);
        helpers.resetForm();
        onSubmit();
      }}
    >
      {({ handleSubmit }) => (
        <Form>
          <Grid container spacing={4}>
            <Grid style={{ flexGrow: 1 }} item>
              <ConnectedTextField
                name="message"
                size="small"
                fullWidth
                disabled={!connection}
                label={
                  <Grid container>
                    <Typography variant="body1" color="textSecondary">
                      Post a comment...
                    </Typography>
                  </Grid>
                }
                variant="filled"
              />
            </Grid>
            <Grid item>
              <Tooltip
                title={COMPANY_RESTRICTION_TOOLTIP_TEXT}
                followCursor
                open={!!connection ? false : undefined}
              >
                <div>
                  <Button
                    onClick={() => handleSubmit()}
                    fullWidth
                    size="large"
                    variant="outlined"
                    color="secondary"
                    disabled={!connection || isLoading}
                    type="submit"
                  >
                    Add comment
                  </Button>
                </div>
              </Tooltip>
            </Grid>
          </Grid>
        </Form>
      )}
    </Formik>
  );
};

export default CreateCommentForm;
