import { RecordList } from 'components/custom/tracking-system';
import React, { useRef } from 'react';

import { Box, CircularProgress, Divider, Paper } from '@mui/material';

import { Talent } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import CreateRecordForm from './create-record-form';
import { useATSRecords } from './hooks';

interface TrackingSystemProps {
  talent?: Partial<Talent> | null;
}

const TrackingSystem = ({ talent }: TrackingSystemProps) => {
  const listRef = useRef<HTMLDivElement>(null);
  const { isLoading, records } = useATSRecords(talent?.id || '');
  const scrollToListTop = () =>
    listRef.current?.scrollTo({ top: 0, behavior: 'smooth' });

  return (
    <Paper elevation={0}>
      <Box p={4}>
        <Box pb={4}>
          <Typography variant="h6">Timeline</Typography>
        </Box>
        <CreateRecordForm
          onSubmit={scrollToListTop}
          talent={talent as Talent}
        />
      </Box>
      <Divider />
      <Box p={4}>
        <Box maxHeight={500} overflow="scroll">
          <RecordList ref={listRef} records={records} />
        </Box>
      </Box>
      {isLoading && (
        <Box
          p={4}
          style={{ width: '100%' }}
          display="flex"
          justifyContent="center"
        >
          <CircularProgress color="secondary" />
        </Box>
      )}
    </Paper>
  );
};

export default TrackingSystem;
