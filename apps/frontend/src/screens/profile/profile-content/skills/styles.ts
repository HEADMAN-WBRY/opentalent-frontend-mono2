import { makeStyles } from '@mui/styles';

import { Theme } from '@libs/ui/themes/default';

const useStyles = makeStyles((theme: Theme) => ({
  skillsSectionWrapper: {
    marginBottom: theme.spacing(3),
  },
}));

export default useStyles;
