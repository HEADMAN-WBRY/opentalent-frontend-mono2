import Box from '@mui/material/Box';
import React from 'react';

import SkillsSection from './SkillsSection';

export default {
  title: 'Components/SkillsSection',
  component: SkillsSection,
};

const mockSkills = [
  ...Array(5).fill({
    id: 1,
    name: 'October CMS',
    active: true,
  }),
  ...Array(10).fill({
    id: 1,
    name: 'October CMS',
    active: false,
  }),
];

export const Default = () => {
  return (
    <>
      <Box mb={4}>
        <SkillsSection skillsData={mockSkills} title="Technologies" />
      </Box>
    </>
  );
};
