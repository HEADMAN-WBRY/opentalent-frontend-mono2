import { AuthUtils } from 'auth0/utils';
import { useAuth0 } from 'hooks/auth/useAuth0';
import React from 'react';
import CategoriesList from 'screens/company-user/shared/talent-card/CategoriesList';

import { Box, Grid, Paper } from '@mui/material';

import { Tag, Talent } from '@libs/graphql-types';

import Skills from './skills';
import useStyles from './styles';
import TalentTags from './talent-tags';
import TrackingSystem from './tracking-system';

export interface ProfileContentProps {
  talent?: Partial<Talent> | null;
  companyTags: Tag[];
}

const ProfileContent = (props: ProfileContentProps) => {
  const { talent, companyTags } = props;
  const classes = useStyles(props);
  const { user } = useAuth0();
  const isCompany = AuthUtils.isCompany(user);

  return (
    <Grid container spacing={4}>
      {isCompany && (
        <Grid xs={12} item>
          <TrackingSystem talent={talent} />
        </Grid>
      )}
      <Grid
        classes={{
          root: classes.skills,
        }}
        item
      >
        <Paper p={4} component={Box} elevation={0}>
          <Skills skills={talent?.skills?.data} />
          {talent && <CategoriesList talent={talent as Talent} />}
        </Paper>
      </Grid>
      {!!companyTags.length && (
        <Grid flexGrow={1} item>
          <TalentTags talent={talent} companyTags={companyTags} />
        </Grid>
      )}
    </Grid>
  );
};

export default ProfileContent;
