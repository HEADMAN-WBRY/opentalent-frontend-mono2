import { OptionType } from '@libs/ui/components/form/select';

export interface IFormState {
  tags: OptionType[];
}
