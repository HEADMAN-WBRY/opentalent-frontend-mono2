import { TalentModals } from 'components/custom/talent/types';
import React from 'react';
import {
  DefaultModalProps,
  useOpenModal,
  withLocationStateModal,
} from 'utils/modals';

import { Box, Grid } from '@mui/material';

import { EXTERNAL_LINKS } from '@libs/helpers/consts';
import Button from '@libs/ui/components/button';
import { DefaultModal } from '@libs/ui/components/modals';
import Typography from '@libs/ui/components/typography';

// import { useOnInviteAction } from './profile-header/action-block/hooks';

interface LimitedProfileAccessModalProps extends DefaultModalProps<true> {
  talentId: string;
}

const LimitedProfileAccessModalComponent = ({
  talentId,
  isOpen,
  close,
}: LimitedProfileAccessModalProps) => {
  // const onInvite = useOnInviteAction(talentId || '');

  return (
    <DefaultModal
      handleClose={close}
      open={isOpen}
      title="Limited profile access"
      actions={
        <Grid spacing={4} container>
          <Grid xs={6} item>
            <Button
              fullWidth
              size="large"
              color="primary"
              variant="contained"
              // onClick={onInvite}
              href={EXTERNAL_LINKS.pieterLink}
              {...{ target: '_blank' }}
            >
              learn more
            </Button>
          </Grid>
          <Grid xs={6} item>
            <Button
              fullWidth
              size="large"
              variant="outlined"
              color="secondary"
              onClick={close}
            >
              Cancel
            </Button>
          </Grid>
        </Grid>
      }
    >
      <Box>
        <Typography paragraph variant="body2">
          To view personal details, invite this person to a job, or communicate
          with someone directly, you need to Subscribe to the OpenTalent
          network.
        </Typography>
      </Box>
    </DefaultModal>
  );
};

export const LimitedProfileAccessModal = withLocationStateModal({
  id: TalentModals.LimitedAccess,
})(LimitedProfileAccessModalComponent);

export const useOpenPremiumSubscriptionModal = () =>
  useOpenModal(TalentModals.LimitedAccess);
