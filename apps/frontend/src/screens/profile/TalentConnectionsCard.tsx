import { useCurrentUser } from 'hooks/auth';
import React from 'react';
import { Link } from 'react-router-dom';
import { pathManager } from 'routes';

import GppGoodOutlinedIcon from '@mui/icons-material/GppGoodOutlined';
import { Box, Card, CardContent, Chip, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { Talent, TalentCompanyPoolingStatusEnum } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

interface TalentConnectionsCardProps {
  talent: Talent;
}

const useStyles = makeStyles((theme) => ({
  shieldIcon: {
    display: 'inherit',
    color: theme.palette.success.main,
  },
}));

export const TalentConnectionsCard = ({
  talent,
}: TalentConnectionsCardProps) => {
  const { isCompany } = useCurrentUser();
  const classes = useStyles();
  const connections = talent?.company_pool_connections?.filter(
    (i) =>
      i?.status === TalentCompanyPoolingStatusEnum.Approved &&
      i.company?.name !== 'OpenTalent',
  );

  if (!connections?.length) {
    return null;
  }

  const moreCount = (connections?.length ?? 0) - 3;

  return (
    <Box mb={4}>
      <Card elevation={0}>
        <CardContent style={{ paddingBottom: 16 }}>
          <Grid container spacing={2}>
            <Grid item>
              <GppGoodOutlinedIcon className={classes.shieldIcon} />
            </Grid>
            <Grid item>
              <Typography variant="h6">Verified member of:</Typography>
            </Grid>
            <Grid item />
            {connections?.slice(0, 3).map((i) => (
              <Grid item key={i?.company?.id!}>
                {isCompany ? (
                  <Chip
                    label={i?.company?.name}
                    color="success"
                    size="small"
                    variant="outlined"
                  />
                ) : (
                  <Link
                    to={pathManager.talent.companies.viewCompany.generatePath({
                      id: i?.company?.id!,
                    })}
                  >
                    <Chip
                      style={{ cursor: 'pointer' }}
                      label={i?.company?.name}
                      color="success"
                      size="small"
                      variant="outlined"
                    />
                  </Link>
                )}
              </Grid>
            ))}
            {moreCount > 0 && (
              <Grid item>
                <Typography>and {moreCount} more...</Typography>
              </Grid>
            )}
          </Grid>
        </CardContent>
      </Card>
    </Box>
  );
};
