import React from 'react';

import { Grid } from '@mui/material';

import { Talent } from '@libs/graphql-types';

import About from './cards/About';
import Availability from './cards/Availability';
import Documents from './cards/Documents';
import Social from './cards/Social';
import WorkHistory from './cards/WorkHistory';

interface ProfileSidebarProps {
  talent: Partial<Talent>;
  isTalentFlow: boolean;
}

const ProfileSidebar = (props: ProfileSidebarProps) => {
  const { talent } = props;
  return (
    <Grid spacing={4} direction="column" container wrap="nowrap">
      <Grid item>
        <Availability talent={talent} />
      </Grid>
      <Grid item>
        <Social talent={talent} />
      </Grid>
      <Grid item>
        <Documents talent={talent} />
      </Grid>
      <Grid item>
        <About>{talent?.about ?? ''}</About>
      </Grid>
      <Grid item>
        <WorkHistory talent={talent} />
      </Grid>
    </Grid>
  );
};

export default ProfileSidebar;
