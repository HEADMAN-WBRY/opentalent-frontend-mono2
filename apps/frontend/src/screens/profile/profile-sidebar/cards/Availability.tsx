/* eslint-disable react/no-array-index-key */
import { format, formatDistanceToNow } from 'date-fns';
import React from 'react';

import AccessTimeIcon from '@mui/icons-material/AccessTime';
import InfoIcon from '@mui/icons-material/Info';
import { Tooltip } from '@mui/material';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Grid from '@mui/material/Grid';

import { Talent } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import useClasses from '../styles';

interface AvailabilityProps {
  talent: Partial<Talent> | null;
}

const Availability = (props: AvailabilityProps) => {
  const classes = useClasses(props);
  const availDay = props?.talent?.available_date;
  const isAvailNow = props?.talent?.available_now;
  const hoursPerWeek = props?.talent?.hours_per_week;
  const lastUpdate = props?.talent?.available_date_updated_at;
  const formattedLastUpdate = lastUpdate
    ? formatDistanceToNow(new Date(lastUpdate))
    : null;

  return (
    <Card elevation={0}>
      <CardContent>
        <Typography className={classes.title} variant="h6">
          Availability{' '}
          {formattedLastUpdate && (
            <Tooltip
              className={classes.availabilityInfoIcon}
              title={`Last updated ${formattedLastUpdate} ago`}
            >
              <InfoIcon fontSize="small" color="disabled" />
            </Tooltip>
          )}
        </Typography>
        <Grid direction="column" spacing={2} container>
          {isAvailNow && (
            <Grid item>
              <Typography variant="body1" color="textSecondary">
                Available now
              </Typography>
            </Grid>
          )}
        </Grid>
        {isAvailNow && hoursPerWeek && (
          <Grid item>
            <Grid spacing={1} container>
              <Grid style={{ color: 'rgba(0, 0, 0, 0.54)' }} item>
                <AccessTimeIcon color="inherit" fontSize="small" />
              </Grid>
              <Grid item>
                <Typography variant="body1" color="textSecondary">
                  {`${hoursPerWeek} hrs/week`}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        )}
        {availDay && !isAvailNow && (
          <Grid item>
            <Typography variant="body1" color="textSecondary">
              {`Available per ${format(new Date(availDay), 'dd MMM yyyy')}`}
            </Typography>
          </Grid>
        )}
        {!availDay && !isAvailNow && (
          <Grid item>
            <Typography variant="body1" color="textSecondary">
              Not available now
            </Typography>
          </Grid>
        )}
      </CardContent>
    </Card>
  );
};

export default Availability;
