/* eslint-disable react/no-array-index-key */
import { useOpenDirectSubscriptionModal } from 'components/custom/company/modals/direct';
import React from 'react';
import { COMPANY_RESTRICTION_TOOLTIP_TEXT } from 'screens/profile/consts';

import { Box, Tooltip } from '@mui/material';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Grid from '@mui/material/Grid';

import { Talent } from '@libs/graphql-types';
import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import { useNeedShowContacts } from '../../profile-header/action-block/hooks';
import useClasses from '../styles';

interface DocumentsProps {
  talent: Partial<Talent>;
}

const Documents = (props: DocumentsProps) => {
  const { talent } = props;
  const classes = useClasses(props);
  const needShowContacts = useNeedShowContacts(talent);
  const openLimitedAccessModal = useOpenDirectSubscriptionModal();

  return (
    <Card elevation={0}>
      <CardContent>
        <Typography className={classes.title} variant="h6">
          Resume
        </Typography>

        {needShowContacts ? (
          <Grid spacing={2} direction="column" container>
            {talent?.documents?.map((doc) => (
              <Grid
                title={doc?.title}
                className={classes.ellipsisContainer}
                key={doc?.hash}
                item
              >
                {needShowContacts ? (
                  <a href={doc?.url} target="_blank" rel="noreferrer" download>
                    <Typography
                      className={classes.ellipsis}
                      variant="body2"
                      color="tertiary"
                    >
                      {doc?.title}
                    </Typography>
                  </a>
                ) : (
                  <Typography
                    style={{ cursor: 'pointer' }}
                    className={classes.ellipsis}
                    variant="body2"
                    color="tertiary"
                    onClick={() => openLimitedAccessModal()}
                  >
                    {doc?.title}
                  </Typography>
                )}
              </Grid>
            ))}
          </Grid>
        ) : (
          <Tooltip title={COMPANY_RESTRICTION_TOOLTIP_TEXT} followCursor>
            <Box>
              <Button disabled variant="outlined">
                Show resume
              </Button>
            </Box>
          </Tooltip>
        )}
      </CardContent>
    </Card>
  );
};

export default Documents;
