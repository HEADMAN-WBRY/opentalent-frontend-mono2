/* eslint-disable react/no-array-index-key */
import { ReactComponent as FacebookIcon } from 'assets/icons/facebook.svg';
import { ReactComponent as LinkedInIcon } from 'assets/icons/linked-in.svg';
import { ReactComponent as UpworkInIcon } from 'assets/icons/upwork.svg';
import { useOpenDirectSubscriptionModal } from 'components/custom/company/modals/direct';
import { useCurrentUser } from 'hooks/auth';
import { useHaveAccessToTalent } from 'hooks/talents';
import { useSnackbar } from 'notistack';
import React, { useCallback, useMemo, useState } from 'react';
import useCopyToClipboard from 'react-use/lib/useCopyToClipboard';

import ContentCopyIcon from '@mui/icons-material/ContentCopy';
import MailIcon from '@mui/icons-material/Mail';
import PhoneAndroidIcon from '@mui/icons-material/PhoneAndroid';
import {
  Card,
  CardContent,
  Grid,
  useTheme,
  Box,
  IconButton,
  Tooltip,
} from '@mui/material';

import { Talent } from '@libs/graphql-types';
import { stopEvent, hasNumbersInString } from '@libs/helpers/common';
import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import useClasses from '../styles';

interface SocialProps {
  talent: Partial<Talent> | null;
}

const ensureLinkProtocol = (link?: string) => {
  if (!link) {
    return link;
  }

  if (
    link.startsWith('mailto:') ||
    link.startsWith('tel:') ||
    link.startsWith('http')
  ) {
    return link;
  }

  return `https://${link}`;
};

const useLinkClick = ({ canShowContacts }: { canShowContacts: boolean }) => {
  const openLimitedAccessModal = useOpenDirectSubscriptionModal();

  return useCallback(
    (link: string) => {
      if (canShowContacts) {
        const finalLink = ensureLinkProtocol(link);
        window.open(finalLink, '__blank');
      } else {
        openLimitedAccessModal();
      }
    },
    [canShowContacts, openLimitedAccessModal],
  );
};

const Social = (props: SocialProps) => {
  const openLimitedAccessModal = useOpenDirectSubscriptionModal();
  const [isShown, showContacts] = useState(false);
  const classes = useClasses(props);
  const { enqueueSnackbar } = useSnackbar();
  const { talent } = props;

  const theme = useTheme();
  const { isTalent: isCurrentUserTalent } = useCurrentUser();
  const hasCompanyAccess = useHaveAccessToTalent(talent as Talent);
  const canShowContacts = isCurrentUserTalent || hasCompanyAccess;

  const [, copyToClipboard] = useCopyToClipboard();
  const onCopy = useCallback(
    (text: string) => {
      copyToClipboard(text);
      enqueueSnackbar('Contact copied', { variant: 'success' });
    },
    [copyToClipboard, enqueueSnackbar],
  );
  const onContactBtnClick = () => {
    if (canShowContacts) {
      showContacts(true);
    } else {
      openLimitedAccessModal();
    }
  };

  const onLinkClick = useLinkClick({ canShowContacts });
  const links = useMemo(() => {
    const facebook = talent?.talent_data?.facebook_profile_link!;
    const linkedin = talent?.talent_data?.linkedin_profile_link!;
    const upwork = talent?.talent_data?.upwork_profile_link!;
    const phone = talent?.talent_data?.phone!;
    const email = talent?.email!;

    return [
      { link: facebook, Icon: FacebookIcon, text: facebook },
      { link: linkedin, Icon: LinkedInIcon, text: linkedin },
      { link: upwork, Icon: UpworkInIcon, text: upwork },
      { link: email ? `mailto:${email}` : email, Icon: MailIcon, text: email },
      {
        link: hasNumbersInString(phone) ? `tel:${phone}` : '',
        Icon: PhoneAndroidIcon,
        text: phone,
      },
    ].filter(({ link }) => Boolean(link));
  }, [talent]);

  return (
    <Card elevation={0}>
      <CardContent>
        <Typography className={classes.title} variant="h6">
          Social links and contacts
        </Typography>

        {!isShown && (
          <Tooltip
            followCursor
            title="Connect to view"
            open={!hasCompanyAccess ? undefined : false}
          >
            <Box>
              <Button
                disabled={!canShowContacts}
                onClick={onContactBtnClick}
                variant="outlined"
              >
                CONTACT INFO
              </Button>
            </Box>
          </Tooltip>
        )}

        {isShown && (
          <Box
            style={{
              display: canShowContacts ? 'block' : 'flex',
            }}
          >
            {links.map(({ link, Icon, text }) => (
              <Grid
                style={{
                  cursor: 'pointer',
                  maxWidth: canShowContacts ? '100%' : '40px',
                }}
                onClick={() => onLinkClick(link)}
                spacing={2}
                container
                wrap="nowrap"
              >
                <Grid
                  style={{
                    minWidth: 30,
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                  key={link}
                  item
                >
                  <Icon style={{ color: theme.palette.grey[600] }} />
                </Grid>
                {canShowContacts && (
                  <>
                    <Grid
                      style={{ width: 'calc(100% - 68px)' }}
                      key={link}
                      item
                    >
                      <Typography
                        style={{
                          textOverflow: 'ellipsis',
                          overflow: 'hidden',
                          width: '100%',
                        }}
                        variant="subtitle1"
                        color="textSecondary"
                      >
                        {text?.replace(/^https?:\/\//, '')}
                      </Typography>
                    </Grid>
                    <Grid onClick={stopEvent} item>
                      <Tooltip title="Copy">
                        <IconButton onClick={() => onCopy(text)} size="small">
                          <ContentCopyIcon fontSize="small" />
                        </IconButton>
                      </Tooltip>
                    </Grid>
                  </>
                )}
              </Grid>
            ))}
          </Box>
        )}
      </CardContent>
    </Card>
  );
};

export default Social;
