/* eslint-disable react/no-array-index-key */
import { format, parseISO } from 'date-fns';
import React from 'react';
import SkillsSection from 'screens/profile/profile-content/skills/skills-section/SkillsSection';

import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Grid from '@mui/material/Grid';

import { Talent } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import useClasses from '../styles';

interface WorkHistoryProps {
  talent?: Partial<Talent> | null;
}

const WorkHistory = (props: WorkHistoryProps) => {
  const classes = useClasses(props);
  const { talent } = props;

  return (
    <Card elevation={0}>
      <CardContent>
        <Typography className={classes.title} variant="h6">
          Work Experience
        </Typography>
        <Box pb={4}>
          <SkillsSection
            title="Top companies"
            skillsData={talent?.companies as any}
          />
        </Box>
        {talent?.talent_work_history?.map((historyItem, index, arr) => (
          <Box
            mb={index + 1 === arr.length ? 0 : '16px'}
            key={`${historyItem?.company_name}_${historyItem?.position_title}`}
          >
            <Grid direction="column" container>
              <Typography variant="subtitle2">
                {historyItem?.position_title}
              </Typography>
              <Typography variant="caption" color="textSecondary">
                {historyItem?.company_name}
              </Typography>
              <Typography variant="body2" color="textSecondary">{`${historyItem?.worked_from &&
                format(parseISO(historyItem?.worked_from), 'yyyy MMM')
                } - ${historyItem?.worked_to
                  ? format(parseISO(historyItem?.worked_to), 'yyyy MMM')
                  : 'Currently working here'
                }`}</Typography>
            </Grid>
          </Box>
        ))}
      </CardContent>
    </Card>
  );
};

export default WorkHistory;
