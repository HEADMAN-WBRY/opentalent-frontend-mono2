import { CompanyDirectBannerForProfile } from 'components/custom/company/direct';
import {
  CreateCampaignModal,
  InviteByCompanyModal,
} from 'components/custom/talent/modals';
import { ConnectedPageLayout } from 'components/layout/page-layout';
import useMediaQueries from 'hooks/common/useMediaQueries';
import { useGetCompanyPoolConnectionToTalent } from 'hooks/company';
import { useIsFreeCompanyAccount } from 'hooks/company/useCompanyAccountType';
import React from 'react';
import { useRouteMatch } from 'react-router-dom';
import { pathManager } from 'routes';

import { Box } from '@mui/material';
import Grid from '@mui/material/Grid';

import { Tag } from '@libs/graphql-types';

import { LimitedProfileAccessModal } from './LimitedProfileAccessModal';
import { TalentConnectionsCard } from './TalentConnectionsCard';
import { useProfileInfo } from './hooks';
import { ChatWithTalentModal } from './modals/chat-with-talent';
import ProfileContent from './profile-content';
import ProfileHeader from './profile-header';
import ProfileSidebar from './profile-sidebar';
import useStyles from './styles';
import { ProfileScreenProps } from './types';

const Profile = (props: ProfileScreenProps) => {
  const talentId = props.match?.params?.id;
  const { isSM } = useMediaQueries();
  const classes = useStyles(props);
  const isFreeCompanyAccount = useIsFreeCompanyAccount();
  const isTalentFlow = useRouteMatch({
    path: pathManager.talent.profile.generatePath(),
    exact: true,
  });
  const {
    loading,
    talent: talentData,
    fetchTalent,
    data,
  } = useProfileInfo(talentId);
  const companyTags = (data?.currentUserCompanyTags as Tag[]) || [];

  const connection = useGetCompanyPoolConnectionToTalent({
    talent: talentData,
  });

  return (
    <ConnectedPageLayout
      isLoading={loading}
      headerProps={{ accountProps: {} }}
      documentTitle="Profile"
      drawerProps={{}}
    >
      {isFreeCompanyAccount && !isTalentFlow && (
        <Box mb={4}>
          <CompanyDirectBannerForProfile />
        </Box>
      )}
      {!!talentData && (
        <Grid direction="column" container className={classes.container}>
          <ProfileHeader
            talent={talentData as any}
            isTalentFlow={!!isTalentFlow}
            refetchTalent={fetchTalent}
            connection={connection}
          />
          <Box>
            <TalentConnectionsCard talent={talentData} />
          </Box>
          <Grid item>
            <Grid
              container
              direction={isSM ? 'column-reverse' : 'row'}
              spacing={4}
            >
              <Grid md={4} item>
                <ProfileSidebar
                  talent={talentData as any}
                  isTalentFlow={!!isTalentFlow}
                />
              </Grid>
              <Grid md={8} item>
                <ProfileContent
                  talent={talentData as any}
                  companyTags={companyTags}
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      )}

      <InviteByCompanyModal />
      <CreateCampaignModal />
      <LimitedProfileAccessModal talentId={talentId || ''} />
      <ChatWithTalentModal />
    </ConnectedPageLayout>
  );
};

export default Profile;
