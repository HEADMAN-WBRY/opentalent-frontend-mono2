import {
  ExternalJobEmploymentTypeEnum,
  ExternalJobSeniorityLevelEnum,
} from '@libs/graphql-types';

export const SENIORITY_LABELS: Record<ExternalJobSeniorityLevelEnum, string> = {
  [ExternalJobSeniorityLevelEnum.MidSeniorLevel]: 'Mid-Senior level',
  [ExternalJobSeniorityLevelEnum.NotApplicable]: 'Not Applicable',
  [ExternalJobSeniorityLevelEnum.EntryLevel]: 'Entry Level',
  [ExternalJobSeniorityLevelEnum.Executive]: 'Executive',
  [ExternalJobSeniorityLevelEnum.Associate]: 'Associate',
  [ExternalJobSeniorityLevelEnum.Director]: 'Director',
  [ExternalJobSeniorityLevelEnum.Internship]: 'Internship',
};

export const EMPLOYMENT_LABELS: Record<ExternalJobEmploymentTypeEnum, string> =
{
  [ExternalJobEmploymentTypeEnum.Internship]: 'Internship',
  [ExternalJobEmploymentTypeEnum.Other]: 'Other',
  [ExternalJobEmploymentTypeEnum.Volunteer]: 'Volunteer',
  [ExternalJobEmploymentTypeEnum.Temporary]: 'Temporary',
  [ExternalJobEmploymentTypeEnum.Contract]: 'Contract',
  [ExternalJobEmploymentTypeEnum.FullTime]: 'Full Time',
  [ExternalJobEmploymentTypeEnum.PartTime]: 'Part Time',
};
