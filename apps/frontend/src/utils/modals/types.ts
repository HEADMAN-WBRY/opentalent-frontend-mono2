interface RequiresModalProps {
  isOpen: boolean;
  close: VoidFunction;
}

export type DefaultModalProps<S> = RequiresModalProps & {
  modalData?: S;
  modalKey?: string;
};
