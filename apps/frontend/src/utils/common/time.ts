import { differenceInDays } from 'date-fns';

export const isNotOlderThan = (date: Date, days: number) => {
  const diff = differenceInDays(new Date(), date);
  return diff <= days;
};
