import qs from 'query-string';

export const parse = (s: string) =>
  qs.parse(s, { parseBooleans: true, parseNumbers: true });
export const stringify = (s: Record<string, any>) =>
  qs.stringify(s, {
    arrayFormat: 'separator',
    arrayFormatSeparator: ';',
  });
