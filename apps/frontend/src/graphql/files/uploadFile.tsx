import { gql } from '@apollo/client';

export default gql`
  mutation UploadFile(
    $file_type: FileTypeEnum!
    $owner_id: ID
    $file: Upload!
  ) {
    uploadFile(file_type: $file_type, owner_id: $owner_id, file: $file) {
      hash
    }
  }
`;
