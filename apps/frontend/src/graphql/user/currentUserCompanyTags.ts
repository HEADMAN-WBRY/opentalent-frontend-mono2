import { gql } from '@apollo/client';

export default gql`
  query GetCurrentUserCompanyTags {
    currentUserCompanyTags {
      name
      id
    }
  }
`;
