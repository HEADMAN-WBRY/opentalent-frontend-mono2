import { gql } from '@apollo/client';

export default gql`
  mutation UploadUserAvatar($user_id: ID!, $file: Upload!) {
    uploadUserAvatar(user_id: $user_id, file: $file) {
      hash
    }
  }
`;
