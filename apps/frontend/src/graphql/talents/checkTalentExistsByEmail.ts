import { gql } from '@apollo/client';

export default gql`
  query CheckTalentExistsByEmail($talent_email: Email!) {
    checkTalentExistsByEmail(talent_email: $talent_email)
  }
`;
