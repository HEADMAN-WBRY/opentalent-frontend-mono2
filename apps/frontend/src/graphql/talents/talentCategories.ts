import { gql } from '@apollo/client';

export default gql`
  query TalentCategories {
    talentCategories {
      id
      name
      slug
      created_at
      updated_at
      subcategories {
        id
        name
      }
    }
  }
`;
