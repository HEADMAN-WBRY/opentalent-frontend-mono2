import { gql } from '@apollo/client';

export default gql`
  mutation RemindTalentAccount($talent_id: ID!) {
    remindToActivateTalentAccount(talent_id: $talent_id)
  }
`;
