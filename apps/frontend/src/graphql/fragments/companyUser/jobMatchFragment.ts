import { gql } from '@apollo/client';

export default gql`
  fragment FullJobMatch on JobMatch {
    job_invitation {
      is_declined
    }
    job_application {
      pitch
    }
    cv_skills_keywords {
      id
      skill_type
      name
    }
    id
    is_talent_notified
    talent {
      first_name
      first_name_abac {
        value
      }
      last_name
      last_name_abac {
        value
      }
      company_pool_connections {
        company {
          id
          name
        }
        status
      }
      id
      email
      address
      recent_position_title
      location
      rate_max
      rate_min
      is_invitation_accepted
      available_now
      avatar {
        avatar
      }
      category {
        id
        name
      }
      subcategories {
        id
        name
      }
    }
    matched_skills {
      id
      name
      skill_type
    }
    match_percent
    match_type
    is_instant_match
    is_applied
    is_invited
    is_shortlist
    match_quality
    is_new
  }
`;
