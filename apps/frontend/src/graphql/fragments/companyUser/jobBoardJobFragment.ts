import { gql } from '@apollo/client';

export default gql`
  fragment JobBoardJobFragment on Job {
    id
    name
    country
    city
    is_draft
    is_remote_an_option
    link_to_details
    skills {
      id
      name
      skill_type
      job_skill_pivot {
        is_required
      }
    }
    skills_boolean_filter {
      not_skills {
        id
        name
        skill_type
      }
      boolean_skills_filter_items {
        one_of_skills {
          id
          name
          skill_type
        }
        all_of_skills {
          id
          name
          skill_type
        }
        boolean_operator
      }
    }
    category {
      id
      name
    }
    description
    pitch
    start_date
    end_date
    rate_min
    rate_max
    is_rate_negotiable
    campaign_owner {
      id
      first_name
      last_name
    }
    campaign_start_date
    campaign_end_date
    campaign_talent_pool
    is_archived
    matches_count
    instant_matches_count
    matches_with_application_count
    salary_max
    salary_min
    finders_fee
    location
    hours_per_week
    location_type
    type
    period
    client
    capacity
    posted_at
  }
`;
