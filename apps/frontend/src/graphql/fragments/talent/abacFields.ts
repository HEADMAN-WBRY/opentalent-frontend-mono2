import { gql } from '@apollo/client';

export const ABAC_PROPERTIES = gql`
  fragment AbacProps on ABACString {
    is_accessible
    is_presented
    value
  }
`;
