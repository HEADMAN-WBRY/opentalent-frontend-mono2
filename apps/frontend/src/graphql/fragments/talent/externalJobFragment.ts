import { gql } from '@apollo/client';

export default gql`
  fragment ExternalJobBoardJobFragment on ExternalJob {
    id
    match_quality
    category {
      id
      name
    }
    is_saved
    title
    country
    title
    url
    external_url
    description
    seniority
    employment_type
    location
    company_name
    company_url
    salary
    linkedin_job_id
    created
    last_updated
    deleted
    application_active
  }
`;
