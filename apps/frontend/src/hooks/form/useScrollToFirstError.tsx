import { useFormikContext } from 'formik';
import { useEffect, useState } from 'react';

import { debounce } from '@mui/material';

const getNameOfFirstErrorFiled = (errors: Record<string, any>): string => {
  let res = '';
  const getPath = (err: Record<string, any>) => {
    const [key, value] = Object.entries(err)[0];
    res = res ? `${res}.${key}` : key;
    if (typeof value === 'object') {
      getPath(value);
    }
  };

  getPath(errors);

  return res;
};

export const useScrollToFirstError = ({ delay = 500 }: { delay?: number }) => {
  const { errors, submitCount, isValid } = useFormikContext();
  const [prevSubmitCount, setPrevSubmitCount] = useState(submitCount);
  useEffect(() => {
    if (prevSubmitCount !== submitCount && !isValid) {
      const name = getNameOfFirstErrorFiled(errors);
      const node = window.document.getElementsByName(name)[0];
      if (node) {
        const rect = node.getBoundingClientRect();
        const top = window.pageYOffset + rect.top - 100;
        debounce(
          window.scrollTo,
          delay,
        )({
          top,
          behavior: 'smooth',
        });
      }
      setPrevSubmitCount(submitCount);
    }
  }, [submitCount, isValid, errors, prevSubmitCount, delay]);
};
