import { useContext } from 'react';
// @ts-expect-error
import MixpanelContext from 'react-mixpanel';

const useMixPanel = () => {
  return useContext(MixpanelContext) as any;
};

export default useMixPanel;
