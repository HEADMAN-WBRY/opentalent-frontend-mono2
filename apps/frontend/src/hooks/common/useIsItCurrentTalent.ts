import { useCurrentUser } from 'hooks/auth';

export const useIsItCurrentTalent = (talentId?: string) => {
  const { user } = useCurrentUser();
  const isItSameProfile = talentId === user?.id;
  return isItSameProfile;
};
