import useTitle from 'react-use/lib/useTitle';

import { PROJECT_NAME } from 'consts/config';

export const usePageTitle = (page?: string) => {
  useTitle(page ? `${PROJECT_NAME} | ${page}` : PROJECT_NAME);
};
