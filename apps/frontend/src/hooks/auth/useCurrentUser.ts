import { useApolloClient } from '@apollo/client';
import { AuthUtils } from 'auth0/utils';
import { GET_CURRENT_TALENT } from 'graphql/talents';
import { GET_CURRENT_COMPANY_USER } from 'graphql/user';
import { useMemo, useCallback } from 'react';

import { Query, Talent, User } from '@libs/graphql-types';

import { useAuth0 } from './useAuth0';

interface CurrentUserResult {
  data: Query | null;
  user?: Talent | User;
  isAuthorized: boolean;
  isTalent: boolean;
  isCompany: boolean;
}

export const useCurrentUser = (): CurrentUserResult & {
  getData: () => CurrentUserResult;
} => {
  const apolloClient = useApolloClient();
  const { user: auth0User, isAuthenticated } = useAuth0();
  const isTalent = AuthUtils.isTalent(auth0User);
  const isCompany = AuthUtils.isCompany(auth0User);
  const userData = apolloClient.readQuery<Query>({
    query: isTalent ? GET_CURRENT_TALENT : GET_CURRENT_COMPANY_USER,
  });
  const result = useMemo(() => {
    return {
      data: userData,
      user:
        userData && isTalent
          ? userData?.currentTalent
          : userData?.currentCompanyUser,
      isAuthorized: isAuthenticated,
      isTalent,
      isCompany,
    };
  }, [isAuthenticated, isCompany, isTalent, userData]);

  const getData = useCallback(() => {
    const userData = apolloClient.readQuery<Query>({
      query: isTalent ? GET_CURRENT_TALENT : GET_CURRENT_COMPANY_USER,
    });

    return {
      data: userData,
      user:
        userData && isTalent
          ? userData?.currentTalent
          : userData?.currentCompanyUser,
      isAuthorized: isAuthenticated,
      isTalent,
      isCompany,
    };
  }, [apolloClient, isAuthenticated, isCompany, isTalent]);

  return { ...result, getData };
};
