import { useCurrentUser } from 'hooks/auth';

export const useTalentPoolConnections = () => {
  const { getData } = useCurrentUser();
  const connections =
    getData()?.data?.currentTalent?.company_pool_connections || [];

  return connections;
};
