export enum JobMarkType {
  Saved = 'saved',
  Applied = 'applied',
  Recommended = 'recommended',
}
