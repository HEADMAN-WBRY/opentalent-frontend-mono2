import { UserInterface, UserRoles } from 'auth0/types';

const isTalent = (user: UserInterface) =>
  user?.['https://opentalent.co']?.user_roles?.includes(UserRoles.Talent);
const isCompany = (user: UserInterface) =>
  user?.['https://opentalent.co']?.user_roles?.includes(UserRoles.Company);

export const AuthUtils = { isTalent, isCompany };
