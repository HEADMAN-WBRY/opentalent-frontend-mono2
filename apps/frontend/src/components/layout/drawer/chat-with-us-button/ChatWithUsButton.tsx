import { useQuery } from '@apollo/client';
import { ChatTypes } from 'components/chat';
import React from 'react';
import { pathManager } from 'routes';

import QuestionAnswerIcon from '@mui/icons-material/QuestionAnswer';

import { Query } from '@libs/graphql-types';
import { RouterButton } from '@libs/ui/components/button';

import { LOAD_TALENT_QUERY } from './queries';

interface ChatWithUsButtonProps {
  className?: string;
}

export const ChatWithUsButton = ({ className }: ChatWithUsButtonProps) => {
  const { data } = useQuery<Query>(LOAD_TALENT_QUERY, {
    variables: { talent_id: 68 },
  });
  const talent = data?.talent;

  return (
    <RouterButton
      startIcon={<QuestionAnswerIcon />}
      className={className}
      disabled={!talent}
      to={{
        pathname: pathManager.chat.generatePath(),
        state: {
          strategy: {
            type: ChatTypes.TalentToTalent,
            data: talent,
          },
        },
      }}
    >
      Chat with us
    </RouterButton>
  );
};
