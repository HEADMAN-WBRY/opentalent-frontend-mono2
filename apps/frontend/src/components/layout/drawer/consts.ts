import InboxIcon from '@mui/icons-material/MoveToInbox';

export const FAKE_ITEMS = [
  { text: 'Default', Icon: InboxIcon },
  {
    text: 'With link',
    link: '/',
    Icon: InboxIcon,
    isActive: true,
  },
  {
    text: 'With badge',
    link: '/alo',
    Icon: InboxIcon,
    badge: 2,
  },
  {
    text: 'With sub list',
    Icon: InboxIcon,
    items: [{ text: 'Subitem', Icon: InboxIcon }],
  },
];
