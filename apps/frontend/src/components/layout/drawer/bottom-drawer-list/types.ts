import * as H from 'history';
import React from 'react';

export type BottomDrawerListItemProps = {
  onClick?: VoidFunction;
  to?: H.LocationDescriptor<any>;
  href?: string;
  text: string;
  Icon: React.ComponentType<any>;
  disabled?: boolean;
  htmlTitle?: string;
};
