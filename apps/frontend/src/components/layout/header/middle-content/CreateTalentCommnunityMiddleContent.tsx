import React from 'react';
import { useHistory } from 'react-router-dom';
import { pathManager } from 'routes';

import { Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

interface CreateTalentCommnunityMiddleContentProps { }

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(2, 3),
    background: 'rgba(242, 98, 236, 1)',
    color: 'white',
    maxWidth: 400,
    borderRadius: 4,
    cursor: 'pointer',
  },
}));

export const CreateTalentCommnunityMiddleContent = (
  props: CreateTalentCommnunityMiddleContentProps,
) => {
  const classes = useStyles();
  const history = useHistory();
  const onClick = () =>
    history.push(pathManager.talent.createCommunity.generatePath());

  return (
    <div className={classes.root} onClick={onClick}>
      <Grid container>
        <Grid item flexGrow={1}>
          <Typography variant="body1">
            <span style={{ color: 'black' }}>NEW:</span> Create your Freelancer
            Community
          </Typography>
        </Grid>
      </Grid>
    </div>
  );
};
