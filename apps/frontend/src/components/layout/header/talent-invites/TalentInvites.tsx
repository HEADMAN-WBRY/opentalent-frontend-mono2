import { ReactComponent as InvitesIcon } from 'assets/icons/invites.svg';
import { AuthUtils } from 'auth0/utils';
import { useAuth0 } from 'hooks/auth/useAuth0';
import React from 'react';
import { Link } from 'react-router-dom';
import { pathManager } from 'routes/consts';
import { useRequestInviteData } from 'screens/talent/invite-by-talent/invite-talents/hooks';

import { Skeleton } from '@mui/lab';
import { Grid, IconButton } from '@mui/material';

import useStyles from './styles';

interface TalentInvitesProps {
  isLoading: boolean;
  inventionCount?: number;
}

const TalentInvites = (props: TalentInvitesProps) => {
  const { isLoading } = props;
  const classes = useStyles(props);
  const { user } = useAuth0();
  const isTalent = AuthUtils.isTalent(user);
  const { isInvitationAccepted } = useRequestInviteData();

  if (!isTalent || !isInvitationAccepted) {
    return null;
  }

  if (isLoading) {
    return (
      <Grid spacing={2} alignItems="center" container>
        <Grid item>
          <Skeleton
            className={classes.skeleton}
            variant="circular"
            width={30}
            height={30}
          />
        </Grid>
        <Grid item>
          <Skeleton
            className={classes.skeleton}
            width="40px"
            height="20px"
            variant="text"
          />
        </Grid>
      </Grid>
    );
  }

  return (
    <Link to={pathManager.talent.invite.generatePath()}>
      <IconButton className={classes.root}>
        <InvitesIcon />
      </IconButton>
    </Link>
  );
};

export default TalentInvites;
