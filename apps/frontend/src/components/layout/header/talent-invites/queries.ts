import { gql } from '@apollo/client';

export const GET_INVITATION_INFO = gql`
  query GetTalentInvitationsInfo($inviting_talent_id: ID!) {
    talentInvitationsInfo(inviting_talent_id: $inviting_talent_id) {
      invitations_left
    }
  }
`;
