import React from 'react';
import { Link } from 'react-router-dom';
import replaceString from 'react-string-replace';

import {
  NotificationActionInterface,
  NotificationMessage as NotificationMessageType,
} from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import { getActionLink } from './utils';

interface NotificationMessageProps {
  message: NotificationMessageType;
  handleChangeStatus: (status: boolean) => void;
}

const NotificationMessage = ({
  message,
  handleChangeStatus,
}: NotificationMessageProps) => {
  const finalMessage = (message?.template_actions || []).reduce<
    React.ReactNode[]
  >(
    (acc, action) => {
      if (action) {
        return replaceString(acc, `{${action.key}}`, () => {
          return (
            <Link
              key={action?.text}
              onClick={() => handleChangeStatus(true)}
              to={getActionLink(action.action as NotificationActionInterface)}
            >
              <Typography
                style={{ whiteSpace: 'nowrap' }}
                variant="body2"
                component="span"
                color="tertiary"
              >
                {action?.text}
              </Typography>
            </Link>
          );
        }) as React.ReactNode[];
      }
      return acc;
    },
    [message?.template || ''],
  );

  return <div style={{ lineBreak: 'auto' }}>{finalMessage}</div>;
};

export default NotificationMessage;
