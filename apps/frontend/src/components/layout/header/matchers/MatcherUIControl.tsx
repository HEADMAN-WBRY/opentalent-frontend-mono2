import React from 'react';

import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import { Grid, Menu, MenuItem } from '@mui/material';

import Chip from '@libs/ui/components/chip';

import {
  MatcherLayoutViewTypes,
  useTalentMatcherContext,
} from './TalentMatcherContext';

interface MatcherUIControlProps {}

export const MatcherUIControl = (props: MatcherUIControlProps) => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLDivElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLDivElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const { value, setValue } = useTalentMatcherContext();
  const viewText =
    value?.currentView === MatcherLayoutViewTypes.Matcher
      ? 'Talent Matcher'
      : 'Candidate';

  return (
    <>
      <Chip
        size="small"
        color="primary"
        style={{ cursor: 'pointer' }}
        label={
          <Grid onClick={handleClick} spacing={1} container>
            <Grid item>{viewText}</Grid>
            <Grid item>
              <KeyboardArrowDownIcon
                style={{
                  verticalAlign: 'middle',
                  fontSize: 16,
                }}
              />
            </Grid>
          </Grid>
        }
      />
      <Menu
        id="MatcherUIControl"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
      >
        <MenuItem
          onClick={() => {
            handleClose();
            setValue({ currentView: MatcherLayoutViewTypes.Matcher });
          }}
        >
          Talent Matcher
        </MenuItem>
        <MenuItem
          onClick={() => {
            handleClose();
            setValue({ currentView: MatcherLayoutViewTypes.Talent });
          }}
        >
          Candidate
        </MenuItem>
      </Menu>
    </>
  );
};
