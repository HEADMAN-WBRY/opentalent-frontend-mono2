import Box from '@mui/material/Box';
import { DEFAULT_AVATAR } from 'consts/common';
import React from 'react';

import Account from './Account';

export default {
  title: 'Components/Account',
  component: Account,
};

export const Default = () => (
  <>
    <Box padding="20px" bgcolor="text.primary">
      <Account
        name="Linda Smith"
        position="Hiring Manager"
        avatar={DEFAULT_AVATAR}
      />
    </Box>
  </>
);
