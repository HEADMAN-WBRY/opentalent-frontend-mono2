import { ChatTypes } from 'components/chat';
import React from 'react';
import { pathManager } from 'routes';

import { Talent, User } from '@libs/graphql-types';
import { ButtonProps, RouterButton } from '@libs/ui/components/button';

interface ChatWithClientButtonProps extends ButtonProps {
  chatType: ChatTypes;
  person: Talent | User;
  messageText?: string;
}

export const StartChat = ({
  disabled = false,
  person,
  messageText,
  chatType,
  children,
  ...rest
}: ChatWithClientButtonProps) => {
  return (
    <RouterButton
      disabled={disabled}
      to={{
        pathname: pathManager.chat.generatePath(),
        state: {
          inputText: messageText,
          strategy: {
            type: chatType,
            data: person,
          },
        },
      }}
      size="small"
      variant="outlined"
      {...rest}
    >
      {children}
    </RouterButton>
  );
};
