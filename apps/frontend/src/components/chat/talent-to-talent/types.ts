import { CustomChatUserData } from '../common/provider/types';
import { ChatTypes } from '../types';

export interface TalentToTalentChatMeta {
  type: ChatTypes.TalentToTalent;
  talent1: CustomChatUserData;
  talent2: CustomChatUserData;
}
