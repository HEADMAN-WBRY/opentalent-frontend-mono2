import cn from 'classnames';
import { DefaultHeaderProps } from 'components/chat/common/channel-header';
import { DEFAULT_AVATAR } from 'consts/common';
import { useCurrentUser } from 'hooks/auth';
import React from 'react';
import { useHistory } from 'react-router-dom';
import { pathManager } from 'routes';
import { useChannelStateContext } from 'stream-chat-react';
import { stopEvent } from 'utils/common';

import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import {
  Avatar,
  IconButton,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from '@mui/material';
import { makeStyles } from '@mui/styles';

import { TalentToTalentChatMeta } from '../types';

interface CustomChannelHeaderProps extends DefaultHeaderProps {}

const useStyles = makeStyles((theme) => ({
  root: {
    background: 'white',
  },
  clickable: {
    cursor: 'pointer',
  },
  btn: {
    marginRight: theme.spacing(2),
    display: 'none',
    marginLeft: -10,

    [theme.breakpoints.down('md')]: {
      display: 'block',
    },
  },
}));

const DEFAULT_CHAT_VALUES = {
  title: 'Chat',
  subtitle: '',
  image: DEFAULT_AVATAR,
  link: '',
};

const useChatInfo = (): typeof DEFAULT_CHAT_VALUES => {
  const { channel } = useChannelStateContext();
  const { user } = useCurrentUser();
  const { talent1, talent2 } =
    (channel.data?.channelDetails as TalentToTalentChatMeta) || {};
  const otherTalent = talent1?.id === user?.id ? talent2 : talent1;

  if (!channel) {
    return DEFAULT_CHAT_VALUES;
  }

  const link = pathManager.talent.viewOtherTalent.generatePath({
    id: otherTalent.id,
  });

  return {
    image: otherTalent.image || DEFAULT_AVATAR,
    title: otherTalent.name,
    subtitle: otherTalent.title,
    link,
  };
};

export const TalentToTalentChannelHeader = ({
  toggleMobileView,
}: CustomChannelHeaderProps) => {
  const classes = useStyles();
  const history = useHistory();
  const { title, subtitle, image, link } = useChatInfo();

  const onUserClick = () => {
    if (link) {
      history.push(link);
    }
  };

  return (
    <ListItem
      onClick={onUserClick}
      classes={{ root: cn(classes.root, { [classes.clickable]: !!link }) }}
    >
      {toggleMobileView && (
        <IconButton
          className={classes.btn}
          onClick={(e) => {
            stopEvent(e);
            toggleMobileView();
          }}
          size="small"
        >
          <ChevronLeftIcon />
        </IconButton>
      )}
      <ListItemAvatar>
        <Avatar alt="Avatar" src={image} />
      </ListItemAvatar>
      <ListItemText primary={title} secondary={subtitle} />
    </ListItem>
  );
};
