import { Channel, Message } from 'stream-chat';

export const isItChannel = (item: unknown): item is Channel =>
  item instanceof Channel;
export const isItMessage = (item: any): item is Message => !!item.html;
