import { DefaultGenerics, StreamChat } from 'stream-chat';

export interface CustomChatUserData {
  name: string;
  image: string;
  id: string;
  chatId: string;
  token: string;
  title: string;
}

export interface CompanyUserChatData extends CustomChatUserData {
  companyName: string;
}

export interface ChatProviderValue {
  client: StreamChat<DefaultGenerics>;
  userData: CustomChatUserData | null;
}
