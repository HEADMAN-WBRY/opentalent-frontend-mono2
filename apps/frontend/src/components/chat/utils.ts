export const getChannelIdDefault = (ids: string[]) => ids.sort().join('_');
