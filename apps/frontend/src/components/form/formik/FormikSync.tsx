import { useFormikContext } from 'formik';
import { useCallback, useEffect } from 'react';

interface FormikSyncProps {
  currentValues: any;
}

const FormikSync = ({ currentValues }: FormikSyncProps) => {
  const { values, setValues } = useFormikContext<any>();

  const onUpdate = useCallback(() => {
    const needUpdate = !Object.entries(currentValues).some(
      ([key, value]) => value !== values[key],
    );
    if (needUpdate) {
      setValues({ ...values, ...currentValues });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentValues, setValues]);

  useEffect(onUpdate, [onUpdate]);

  return null;
};

export default FormikSync;
