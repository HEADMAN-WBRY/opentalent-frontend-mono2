import React, { useEffect } from 'react';
import ReactGA from 'react-ga';
import TagManager from 'react-gtm-module';

interface GoogleAnalyticsProps { }

export const setupGoogleStuff = () => {
  ReactGA.initialize('G-XRR2066RVS');
  TagManager.initialize({
    gtmId: 'GTM-WB8QVZJ',
    dataLayer: { 'gtm.start': new Date().getTime(), event: 'gtm.js' },
  });
};

export const GoogleAnalytics = (props: GoogleAnalyticsProps) => {
  useEffect(() => {
    setupGoogleStuff();
    if (window.location.host === 'opentalent.co') {
    }
  }, []);
  return <></>;
};
