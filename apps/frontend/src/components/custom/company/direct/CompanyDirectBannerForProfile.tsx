import { ReactComponent as CrownIcon } from 'assets/icons/crown-gold.svg';
import { PremiumBanner } from 'components/custom/premium-banner';
import React from 'react';

import { makeStyles } from '@mui/styles';

import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import { useOpenDirectSubscriptionModal } from '../modals/direct';

interface CompanyDirectBannerForProfileProps { }

const useStyles = makeStyles(() => ({
  button: {
    color: '#F9F12E',
    transform: 'translateY(-1px)',
  },
  buttonIcon: {
    '& *': {
      fill: `#F9F12E !important`,
    },
  },
  root: {
    textAlign: 'center',
  },
}));

export const CompanyDirectBannerForProfile = (
  props: CompanyDirectBannerForProfileProps,
) => {
  const classes = useStyles();
  const openUpgradeToPremiumModal = useOpenDirectSubscriptionModal();

  return (
    <PremiumBanner className={classes.root}>
      <Typography variant="body2" component="span">
        Unlock this profile by upgrading to OpenTalent <b>Direct</b>{' '}
      </Typography>{' '}
      <Button
        endIcon={<CrownIcon className={classes.buttonIcon} />}
        onClick={() => openUpgradeToPremiumModal()}
        className={classes.button}
      >
        Upgrade to direct
      </Button>
    </PremiumBanner>
  );
};
