import { ReactComponent as CrownIcon } from 'assets/icons/crown-gold.svg';
import { PremiumBanner } from 'components/custom/premium-banner';
import React from 'react';

import { makeStyles } from '@mui/styles';

import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import { useOpenDirectSubscriptionModal } from '../modals/direct';

interface CompanyDirectCommunityBannerProps { }

const useStyles = makeStyles((theme) => ({
  button: {
    color: '#F9F12E',
  },
  buttonIcon: {
    '& *': {
      fill: `#F9F12E !important`,
    },
  },
}));

export const CompanyDirectCommunityBanner = (
  props: CompanyDirectCommunityBannerProps,
) => {
  const classes = useStyles();
  const openUpgradeToPremiumModal = useOpenDirectSubscriptionModal();

  return (
    <PremiumBanner>
      <Typography variant="body2" component="span">
        Want to unlock full candidate profiles, direct messaging and more?
      </Typography>{' '}
      <Button
        endIcon={<CrownIcon className={classes.buttonIcon} />}
        onClick={() => openUpgradeToPremiumModal()}
        className={classes.button}
      >
        {`Upgrade to `}&nbsp;
        <b>direct</b>
      </Button>
    </PremiumBanner>
  );
};
