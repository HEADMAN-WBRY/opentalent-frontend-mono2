import cn from 'classnames';
import React from 'react';

import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

interface PremiumBannerProps extends React.PropsWithChildren<unknown> {
  className?: string;
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    background:
      'linear-gradient(180deg, rgba(255, 255, 255, 0.11) 0%, rgba(255, 255, 255, 0.11) 100%), #121212',
    color: 'white',
    padding: theme.spacing(2, 4),
    borderRadius: theme.spacing(2),
    boxShadow:
      '0px 1px 18px rgba(0, 0, 0, 0.12), 0px 6px 10px rgba(0, 0, 0, 0.14), 0px 3px 5px -1px rgba(0, 0, 0, 0.2)',
  },
}));

export const PremiumBanner = (props: PremiumBannerProps) => {
  const classes = useStyles();

  return (
    <Box className={cn(classes.root, props.className)}>{props.children}</Box>
  );
};
