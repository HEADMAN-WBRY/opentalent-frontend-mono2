import { SkillTypeEnum } from '@libs/graphql-types';

export interface CreateSkillFields {
  name: string;
  skill_type: SkillTypeEnum;
  reason?: string;
}
