import { useMutation } from '@apollo/client';
import { useFormikContext } from 'formik';
import { SUGGEST_SKILL } from 'graphql/skills';
import { useSnackbar } from 'notistack';
import { useCallback, useRef, useState } from 'react';

import { Mutation, MutationSuggestSkillArgs, Skill } from '@libs/graphql-types';
import { OptionType } from '@libs/ui/components/form/select';

import { CreateSkillFields } from './types';

const useSuggestSkillAction = () => {
  const { enqueueSnackbar } = useSnackbar();
  const [suggestSkill] = useMutation<Mutation, MutationSuggestSkillArgs>(
    SUGGEST_SKILL,
    {
      onCompleted: () => {
        enqueueSnackbar('Skill suggested!', { variant: 'success' });
      },
    },
  );

  return useCallback(
    async (values: CreateSkillFields) => {
      const data = await suggestSkill({
        variables: values,
      });
      return data?.data?.suggestSkill;
    },
    [suggestSkill],
  );
};

export const useCreationSkillDialog = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [initialValues, setInitialValues] =
    useState<CreateSkillFields | null>();
  const cbRef = useRef<any>(null);
  const suggestSkill = useSuggestSkillAction();

  const openDialog = (
    values: CreateSkillFields,
    onSuccess?: (skill: Skill) => void,
  ) => {
    setInitialValues(values);
    cbRef.current = onSuccess;
    setIsOpen(true);
  };

  const closeDialog = () => {
    setInitialValues(null);
    cbRef.current = null;
    setIsOpen(false);
  };

  const onCreation = async (values: CreateSkillFields) => {
    const result = await suggestSkill(values);
    if (result) {
      cbRef?.current?.(result);
    }
    closeDialog();
  };

  return { isOpen, openDialog, closeDialog, onCreation, initialValues };
};

export const useHandleSkillChange = () => {
  const { setFieldValue, getFieldProps } = useFormikContext();
  return useCallback(
    (path: string) => (skill: Skill) => {
      const { value = [] } = getFieldProps<OptionType[]>(path);
      const isDuplicate = value.some((opt) => opt.value === skill.id);

      if (isDuplicate) {
        return;
      }

      setFieldValue(
        path,
        (value as any).concat({
          ...skill,
          text: skill?.name,
          value: skill?.id,
          skill_type: skill?.skill_type,
        }),
      );
    },
    [getFieldProps, setFieldValue],
  );
};
