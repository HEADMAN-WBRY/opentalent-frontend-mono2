import {
  AtsRecordComment,
  AtsRecordJobMatchAction,
  AtsRecordTypeEnum,
  TalentLogRecordInterface,
} from '@libs/graphql-types';

const isATSComment = (
  record: TalentLogRecordInterface,
): record is AtsRecordComment =>
  record.type === AtsRecordTypeEnum.CompanyUserCommented;

const isATSJobMatch = (
  record: TalentLogRecordInterface,
): record is AtsRecordJobMatchAction =>
  record.type === AtsRecordTypeEnum.JobMatchAction;

export const atsMatchers = { isATSComment, isATSJobMatch };
