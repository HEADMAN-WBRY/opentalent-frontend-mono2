import React from 'react';
import { formatName } from 'utils/talent';

import { AtsRecordComment } from '@libs/graphql-types';

import DefaultRecordItem from './DefaultRecordItem';

interface CommentRecordItemProps {
  record: AtsRecordComment;
}

export const CommentRecordItem = ({ record }: CommentRecordItemProps) => {
  const actorName = formatName({
    firstName: record.actor?.first_name,
    lastName: record.actor?.last_name,
  });

  return (
    <DefaultRecordItem record={record} status={`${actorName} commented`}>
      {record.text || '[no text]'}
    </DefaultRecordItem>
  );
};
