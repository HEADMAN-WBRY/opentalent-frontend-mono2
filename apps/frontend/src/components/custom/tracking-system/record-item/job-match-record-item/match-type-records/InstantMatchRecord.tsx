import { Grid } from '@mui/material';
import React from 'react';
import { pathManager } from 'routes';

import Typography, { RouterLink } from '@libs/ui/components/typography';

import DefaultRecordItem from '../../DefaultRecordItem';
import { getJobFromRecord } from '../../utils';
import ChipsComponent from '../ChipsComponent';
import { JobMatchRecordItemProps } from '../types';

interface InstantMatchRecordProps extends JobMatchRecordItemProps {}

export const InstantMatchRecord = ({ record }: InstantMatchRecordProps) => {
  const job = getJobFromRecord(record);
  const jobId = job?.id;
  const jobName = job?.name || '[no_name_job]';
  const jobLink = jobId ? (
    <RouterLink to={pathManager.company.job.generatePath({ id: jobId })}>
      {jobName}
    </RouterLink>
  ) : (
    jobName
  );

  return (
    <DefaultRecordItem
      record={record}
      status={
        <Typography variant="body2" color="textSecondary">
          Talent was added to {jobLink} as instant match
        </Typography>
      }
    >
      <Grid alignItems="center" spacing={2} container>
        <ChipsComponent record={record} />
      </Grid>
    </DefaultRecordItem>
  );
};
