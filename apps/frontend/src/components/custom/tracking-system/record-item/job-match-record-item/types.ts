import { AtsRecordJobMatchAction } from '@libs/graphql-types';

export interface JobMatchRecordItemProps {
  record: AtsRecordJobMatchAction;
}
