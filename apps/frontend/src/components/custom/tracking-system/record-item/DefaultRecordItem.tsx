import { DEFAULT_AVATAR } from 'consts/common';
import { format, parseISO } from 'date-fns';
import React from 'react';

import { Avatar, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { TalentLogRecordInterface } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

interface DefaultRecordItemProps
  extends React.PropsWithChildren<{
    record: TalentLogRecordInterface;
    status: React.ReactNode;
  }> {}

export const formatDate = (date: string) =>
  date ? format(parseISO(date), 'dd.MM.yy hh:mm') : '';

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: 8,
  },
  content: {
    flexGrow: 1,
    marginTop: 9,
  },
  date: {
    whiteSpace: 'nowrap',
    float: 'right',
    marginTop: -2,
  },
}));

const DefaultRecordItem = ({
  record,
  status,
  children,
}: DefaultRecordItemProps) => {
  const classes = useStyles();

  return (
    <Grid className={classes.root} spacing={2} container wrap="nowrap">
      <Grid item>
        <Avatar
          variant="circular"
          alt="Avatar"
          src={record.actor?.avatar?.avatar || DEFAULT_AVATAR}
        />
      </Grid>
      <Grid className={classes.content} item>
        <Grid
          spacing={4}
          wrap="nowrap"
          justifyContent="space-between"
          container
        >
          <Grid flexGrow={1} item>
            <div className={classes.date}>
              <Typography variant="caption" color="text.secondary">
                {formatDate(record.date)}
              </Typography>
            </div>
            {status}
          </Grid>
        </Grid>
        <div>{children}</div>
      </Grid>
    </Grid>
  );
};

export default DefaultRecordItem;
