import React from 'react';

import Button, { ButtonProps } from '@libs/ui/components/button';

import { useSelectPremiumAction } from './hooks';

interface StripePaymentButtonProps
  extends React.PropsWithChildren<ButtonProps> {
  children: React.ReactNode;
  isTalentMatcher?: boolean;
}

const StripePaymentButton = ({
  children = 'Select',
  isTalentMatcher,
  ...rest
}: StripePaymentButtonProps) => {
  const { makeRequest, loading } = useSelectPremiumAction({ isTalentMatcher });

  return (
    <Button
      size="large"
      rounded
      fullWidth
      variant="contained"
      color="info"
      disabled={loading}
      onClick={() => makeRequest()}
      {...rest}
    >
      {children}
    </Button>
  );
};

export default StripePaymentButton;
