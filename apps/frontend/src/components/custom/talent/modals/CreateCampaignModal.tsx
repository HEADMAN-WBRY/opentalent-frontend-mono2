import { usePushWithQuery } from 'hooks/routing';
import React from 'react';
import { pathManager } from 'routes';
import { DefaultModalProps, withLocationStateModal } from 'utils/modals';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Button from '@libs/ui/components/button';
import { DefaultModal } from '@libs/ui/components/modals';
import Typography from '@libs/ui/components/typography';

import { TalentModals } from '../types';

const useStyles = makeStyles((theme) => ({
  paper: {
    width: 620,
  },
  content: {
    textAlign: 'left',
  },
}));

const CreateCampaignModalComponent = ({
  isOpen,
  close,
}: DefaultModalProps<true>) => {
  const classes = useStyles();
  const push = usePushWithQuery();
  const onCreate = () =>
    push({
      pathname: pathManager.company.newJob.choose.generatePath(),
      query: {},
      replace: true,
    });

  return (
    <DefaultModal
      actions={
        <Grid container spacing={4}>
          <Grid style={{ flexGrow: 1 }} item>
            <Button
              fullWidth
              color="primary"
              variant="contained"
              size="large"
              onClick={onCreate}
            >
              CREATE A JOB
            </Button>
          </Grid>
          <Grid style={{ flexGrow: 2 }} item>
            <Button
              color="info"
              size="large"
              fullWidth
              variant="text"
              onClick={close}
            >
              CANCEL
            </Button>
          </Grid>
        </Grid>
      }
      handleClose={close}
      open={isOpen}
      title="Create your first job"
      className={classes.paper}
    >
      <Box pb={4} pt={6}>
        <Typography>to invite this person you must create a job.</Typography>
      </Box>
    </DefaultModal>
  );
};

export const CreateCampaignModal = withLocationStateModal({
  id: TalentModals.CreateCompany,
})(CreateCampaignModalComponent);
