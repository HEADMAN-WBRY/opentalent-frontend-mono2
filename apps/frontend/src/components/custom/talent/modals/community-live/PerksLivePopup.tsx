import { TalentModals } from 'components/custom/talent/types';
import React from 'react';
import { pathManager } from 'routes';
import { DefaultModalProps, withLocationStateModal } from 'utils/modals';

import { Box } from '@mui/material';

import { RouterButton } from '@libs/ui/components/button';
import { DefaultModal } from '@libs/ui/components/modals';
import Typography from '@libs/ui/components/typography';

interface PerksLivePopupProps extends DefaultModalProps<true> { }

const PerksLivePopupComponent = ({ isOpen, close }: PerksLivePopupProps) => {
  return (
    <DefaultModal
      handleClose={close}
      open={isOpen}
      title={
        <>
          <Typography component="span" variant="h5" fontWeight={700}>
            Perks{' '}
          </Typography>
          <Typography component="span" variant="h5">
            are now live
          </Typography>
        </>
      }
      actions={
        <Box marginTop={-9} width="100%">
          <RouterButton
            size="large"
            rounded
            fullWidth
            variant="contained"
            color="info"
            to={pathManager.perks.generatePath()}
          >
            VISIT PERKS
          </RouterButton>
        </Box>
      }
    >
      <Box maxWidth={440} mt={4}>
        <Typography whiteSpace="break-spaces" paragraph variant="body1">
          Introducing our brand-new Perks Program, now LIVE on our platform!
          Elevate your work experience with{' '}
          <b>
            exclusive discounts, exciting offers, and a world of employee
            benefits.
          </b>
        </Typography>
      </Box>
    </DefaultModal>
  );
};

export const PerksLivePopup = withLocationStateModal({
  id: TalentModals.PerksLive,
})(PerksLivePopupComponent);
