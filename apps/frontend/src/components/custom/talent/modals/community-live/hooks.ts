import { LOCAL_STORAGE_KEYS } from 'consts/ls-keys';
import { useCurrentUser } from 'hooks/auth';
import { useIsLobbyTalent } from 'hooks/talents/useTalentAccountType';
import { useEffect } from 'react';
import { useOpenModal } from 'utils/modals';

import { TalentModals } from '../../types';

export const useOpenPerksLiveModal = () => useOpenModal(TalentModals.PerksLive);

export const useCheckPerksPopup = () => {
  const { user, isTalent } = useCurrentUser();
  const key = LOCAL_STORAGE_KEYS.perksLivePopup(user?.id || '');
  const isShown = !!localStorage.getItem(key);
  const openModal = useOpenPerksLiveModal();
  const isLobbyTalent = useIsLobbyTalent();

  useEffect(() => {
    if (!isShown && isTalent && !isLobbyTalent) {
      openModal();
      localStorage.setItem(key, 'true');
    }
  }, [isLobbyTalent, isShown, isTalent, key, openModal]);
};
