import { ReactComponent as CrownIcon } from 'assets/icons/crown-gold.svg';
import React from 'react';

import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import { useOpenTalentPremiumModal } from '../modals/limited-profile-access/TalentPremiumModal';

interface TalentPremiumBannerProps { }

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    background:
      'linear-gradient(180deg, rgba(255, 255, 255, 0.11) 0%, rgba(255, 255, 255, 0.11) 100%), #121212',
    color: 'white',
    padding: theme.spacing(2, 4),
    borderRadius: theme.spacing(1),
    boxShadow:
      '0px 1px 18px rgba(0, 0, 0, 0.12), 0px 6px 10px rgba(0, 0, 0, 0.14), 0px 3px 5px -1px rgba(0, 0, 0, 0.2)',
  },
  button: {
    color: '#F9F12E',
  },
  buttonIcon: {
    '& *': {
      fill: `#F9F12E !important`,
    },
  },
}));

export const TalentPremiumBanner = (props: TalentPremiumBannerProps) => {
  const classes = useStyles();
  const openUpgradeToPremiumModal = useOpenTalentPremiumModal();

  return (
    <Box className={classes.root}>
      <Typography variant="body2" component="span">
        Want full profile information, messaging and more?
      </Typography>{' '}
      <Button
        endIcon={<CrownIcon className={classes.buttonIcon} />}
        onClick={() => openUpgradeToPremiumModal()}
        className={classes.button}
      >
        Upgrade to member
      </Button>
    </Box>
  );
};
