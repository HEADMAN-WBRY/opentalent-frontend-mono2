import cn from 'classnames';
import { INFINITY_SIGN } from 'consts/common';
import React from 'react';

import { Card, CardContent } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { SourceTypeEnum } from '@libs/graphql-types';
import { formatNumber } from '@libs/helpers/format';
import Typography from '@libs/ui/components/typography';

interface TalentsCountProps {
  count?: number | string;
  className?: string;
  isLoading?: boolean;
  source?: SourceTypeEnum;
}

const useStyles = makeStyles((theme) => ({
  card: {
    maxWidth: 300,
    boxShadow: 'none',
  },
  cardContent: {
    padding: theme.spacing(6, 8),
    textAlign: 'center',
  },
}));

const TalentsCount = ({
  count,
  className,
  isLoading,
  source,
}: TalentsCountProps) => {
  const classes = useStyles();

  const finalIsLoading = typeof count !== 'number' || isLoading;
  const parsedValue = typeof count === 'number' ? Number(count) : 0;
  const formattedValue = formatNumber(parsedValue);
  const isSmallCount = !finalIsLoading && parsedValue < 50;
  const finalCount = isSmallCount ? 50 : formattedValue;
  const showSubtitle = !source || source === SourceTypeEnum.Opentalent;

  return (
    <Card classes={{ root: cn(classes.card, className) }}>
      <CardContent className={classes.cardContent}>
        <Typography fontWeight={500} variant="h6" gutterBottom>
          {finalIsLoading ? 'Calculating...' : 'Results:'}
        </Typography>
        {isSmallCount && (
          <Typography color="info.main" variant="subtitle1">
            Less than
          </Typography>
        )}
        <Typography color="info.main" variant="h3">
          {finalIsLoading ? INFINITY_SIGN : finalCount}
        </Typography>
        <Typography variant="subtitle1">Matching profiles</Typography>

        {showSubtitle && (
          <Typography variant="body2" color="text.secondary">
            on OpenTalent
          </Typography>
        )}
      </CardContent>
    </Card>
  );
};

export default TalentsCount;
