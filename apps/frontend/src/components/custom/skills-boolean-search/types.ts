import { BooleanOperatorsEnum } from '@libs/graphql-types';
import { OptionType } from '@libs/ui/components/form/select';

interface SkillsRowItem {
  or: OptionType[];
  and: OptionType[];
}

export interface DefaultSkillsRowItem extends SkillsRowItem {
  relationWithNext?: BooleanOperatorsEnum;
}

export interface BooleanModalState {
  items: DefaultSkillsRowItem[];
  not?: OptionType[];
}
