import cn from 'classnames';
import { renderTags } from 'components/custom/skill-select/SkillSelect';
import React from 'react';

import AddIcon from '@mui/icons-material/Add';
import CloseIcon from '@mui/icons-material/Close';
import { Box, Grid, IconButton } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { BooleanOperatorsEnum, SkillTypeEnum } from '@libs/graphql-types';
import { modelPath } from '@libs/helpers/form';
import { ConnectedMultipleSelect } from '@libs/ui/components/form/multiple-select';
import { ConnectedSelect, OptionType } from '@libs/ui/components/form/select';
import Typography from '@libs/ui/components/typography';

import { BooleanModalState } from '../types';
import { useSkillsSuggest } from './hooks';

interface BooleanRowItemProps {
  index: number;
  onDelete?: (index: number) => void;
  onAdd?: () => void;
  hideRelation?: boolean;
}

const BOOLEAN_OPTIONS: OptionType[] = Object.values(BooleanOperatorsEnum)
  .filter((i) => i !== 'NOT')
  .map((i) => ({ value: i, text: i }));

export const useStyles = makeStyles((theme) => ({
  container: {
    background: theme.palette.grey[100],
    padding: theme.spacing(10, 14, 10, 6),
    borderRadius: theme.spacing(4),
    position: 'relative',

    '&:not(:last-child)': {
      marginBottom: theme.spacing(4),
    },
  },

  booleanSelect: {
    minWidth: 0,
    width: '100%',
  },

  actionButton: {
    position: 'absolute',
  },
  addButton: {
    bottom: 10,
    right: 10,
    background: `${theme.palette.tertiary.main} !important`,
  },
  deleteButton: {
    right: 10,
    top: 10,
    background: `${theme.palette.error.main} !important`,
    color: 'white',
  },
  relationSelect: {
    background: 'transparent !important',
    border: 'none !important',

    '& > div': {
      padding: theme.spacing(2, 4),
    },
  },
}));

const BooleanRowItem = ({
  index,
  onAdd,
  onDelete,
  hideRelation = false,
}: BooleanRowItemProps) => {
  const classes = useStyles();
  const { loading, createOnChangeHandler, skillsOptions, onFocus } =
    useSkillsSuggest();

  return (
    <>
      <div className={classes.container}>
        <Box pb={4}>
          <Grid flexWrap="nowrap" spacing={4} container>
            <Grid style={{ minWidth: 140 }} item>
              <Typography variant="body2">One of the skills</Typography>
              <Typography color="text.textSecondary" variant="caption">
                (OR)
              </Typography>
            </Grid>
            <Grid flexGrow={1} item>
              <ConnectedMultipleSelect
                options={skillsOptions}
                name={modelPath<BooleanModalState>((m) => m.items[index].or)}
                loading={loading}
                noOptionsText="No options"
                chipProps={{
                  size: 'small',
                }}
                renderTags={renderTags}
                autoCompleteProps={{
                  loading,
                  filterSelectedOptions: true,
                  popupIcon: null,
                }}
                inputProps={{
                  onChange: createOnChangeHandler([
                    SkillTypeEnum.HardSkills,
                    SkillTypeEnum.SoftSkills,
                    SkillTypeEnum.Solutions,
                  ]),
                  onFocus,
                  variant: 'filled',
                  label: 'Enter Technologies, Hard Skills & Soft Skills',
                  name: 'skills',
                }}
              />
            </Grid>
          </Grid>
        </Box>
        <Grid flexWrap="nowrap" spacing={4} container>
          <Grid style={{ minWidth: 140 }} item>
            <Typography variant="body2">All of these skills</Typography>
            <Typography color="text.textSecondary" variant="caption">
              (AND)
            </Typography>
          </Grid>
          <Grid flexGrow={1} item>
            <ConnectedMultipleSelect
              options={skillsOptions}
              name={modelPath<BooleanModalState>((m) => m.items[index].and)}
              loading={loading}
              noOptionsText="No options"
              chipProps={{
                size: 'small',
              }}
              renderTags={renderTags}
              autoCompleteProps={{
                loading,
                filterSelectedOptions: true,
                popupIcon: null,
              }}
              inputProps={{
                onFocus,
                onChange: createOnChangeHandler([
                  SkillTypeEnum.HardSkills,
                  SkillTypeEnum.SoftSkills,
                  SkillTypeEnum.Solutions,
                ]),
                variant: 'filled',
                label: 'Enter Technologies, Hard Skills & Soft Skills',
                name: 'skills',
              }}
            />
          </Grid>
        </Grid>

        {onDelete && (
          <IconButton
            size="small"
            className={cn(classes.deleteButton, classes.actionButton)}
            onClick={() => onDelete(index)}
          >
            <CloseIcon />
          </IconButton>
        )}
        {onAdd && (
          <IconButton
            size="small"
            className={cn(classes.addButton, classes.actionButton)}
            onClick={() => onAdd()}
          >
            <AddIcon />
          </IconButton>
        )}
      </div>
      {!hideRelation && (
        <Box display="flex" justifyContent="center">
          <ConnectedSelect
            name={modelPath<BooleanModalState>(
              (m) => m.items[index].relationWithNext,
            )}
            hideNoneValue
            options={BOOLEAN_OPTIONS}
            variant="filled"
            className={classes.relationSelect}
            size="small"
          />
        </Box>
      )}
    </>
  );
};

export default BooleanRowItem;
