import { useDebounce } from 'react-use';

import { BooleanModalState } from '../types';

interface TrackFormChangesProps {
  onChange: (val: BooleanModalState) => void;
  values: BooleanModalState;
  timeout?: number;
}

const TrackFormChanges = ({
  onChange,
  values,
  timeout = 1000,
}: TrackFormChangesProps) => {
  useDebounce(
    () => {
      onChange?.(values);
    },
    timeout,
    [values, onChange],
  );
  return null;
};

export default TrackFormChanges;
