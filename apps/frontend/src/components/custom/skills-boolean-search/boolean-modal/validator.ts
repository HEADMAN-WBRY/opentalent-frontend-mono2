import * as yup from 'yup';

import { OptionType } from '@libs/ui/components/form/select';

export const validator = yup.object().shape({
  items: yup.array().of(
    yup.object().shape(
      {
        or: yup
          .array()
          .of(yup.object())
          .when('and', {
            is: (i: OptionType[]) => !!i?.length,
            then: (schema) => schema,
            otherwise: (schema) => schema.min(1),
          }),
        and: yup
          .array()
          .of(yup.object())
          .when('or', {
            is: (i: string[]) => !!i?.length,
            then: (schema) => schema,
            otherwise: (schema) => schema.min(1),
          }),
      },
      ['or', 'and'] as any,
    ),
  ),
  not: yup.array().of(yup.object()),
});
