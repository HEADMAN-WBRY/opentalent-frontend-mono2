import cn from 'classnames';
import React from 'react';

import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { BooleanOperatorsEnum } from '@libs/graphql-types';
import { SkillChip } from '@libs/ui/components/chip';
import { OptionType } from '@libs/ui/components/form/select';
import Typography from '@libs/ui/components/typography';

import { BooleanModalState, DefaultSkillsRowItem } from '../types';

interface BooleanSearchFormulaProps {
  state: BooleanModalState;
  className?: string;
  grey?: boolean;
}

const useStyles = makeStyles((theme) => ({
  root: {
    textAlign: 'center',
  },
  item: {
    display: 'inline',
    textAlign: 'center',
  },
  grey: {
    '& .MuiChip-root': {
      backgroundColor: 'rgba(0, 0, 0, 0.08)',
      color: theme.palette.text.primary,
    },
  },
  block: {
    display: 'inline-block',

    '& > *': {
      marginBottom: theme.spacing(2),
    },
  },
}));

const RelationText = ({ children }: React.PropsWithChildren<unknown>) => {
  return (
    <Typography style={{ padding: '0 8px' }} component="span">
      {children}
    </Typography>
  );
};

const DefaultSentence = ({
  options,
  relation,
  startParenthesis,
  endParenthesis,
}: {
  options: OptionType[];
  relation: BooleanOperatorsEnum;
  startParenthesis?: boolean;
  endParenthesis?: boolean;
}) => {
  const classes = useStyles();

  if (!options.length) {
    return null;
  }

  const needParenthesis = options.length > 1;

  return (
    <Box className={classes.block} mb={2}>
      {startParenthesis && <Typography component="span">(</Typography>}
      {needParenthesis && <Typography component="span">(</Typography>}
      {options.map((skill, index) => {
        if (index === options.length - 1) {
          return <SkillChip key={skill.value} skill={skill as any} />;
        }

        return (
          <div key={skill.value} style={{ display: 'inline-block' }}>
            <SkillChip skill={skill as any} />
            <RelationText>{relation}</RelationText>
          </div>
        );
      })}
      {needParenthesis && <Typography component="span">)</Typography>}
      {endParenthesis && <Typography component="span">)</Typography>}
    </Box>
  );
};

const ItemPart = ({
  or,
  and,
  showParenthesis = false,
}: DefaultSkillsRowItem & { showParenthesis?: boolean }) => {
  const needRelation = !!or.length && !!and.length;

  return (
    <>
      <DefaultSentence
        options={or}
        relation="OR"
        startParenthesis={showParenthesis}
      />
      {needRelation && <RelationText>AND</RelationText>}
      <DefaultSentence
        options={and}
        relation="AND"
        endParenthesis={showParenthesis}
      />
    </>
  );
};

const BooleanSearchFormula = ({
  state,
  className,
  grey,
}: BooleanSearchFormulaProps) => {
  const { items, not = [] } = state;
  const showGlobalParenthesis = items.length > 1;
  const classes = useStyles();

  return (
    <div className={cn(className, classes.root, { [classes.grey]: grey })}>
      {items.map((i, index) => {
        const isLast = index === items.length - 1;
        const showParenthesis =
          showGlobalParenthesis && !!i.and.length && !!i.or.length;
        const showNot = isLast && !!not.length;

        return (
          <div className={cn(classes.item)} key={index}>
            <ItemPart {...i} showParenthesis={showParenthesis} />
            {!isLast && <RelationText>{i.relationWithNext}</RelationText>}
            {showNot && (
              <>
                <RelationText>NOT</RelationText>
                <ItemPart and={not} or={[]} />
              </>
            )}
          </div>
        );
      })}
    </div>
  );
};

export default BooleanSearchFormula;
