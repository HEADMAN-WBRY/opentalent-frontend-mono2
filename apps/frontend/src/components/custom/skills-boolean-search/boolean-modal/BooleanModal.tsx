import cn from 'classnames';
import { Formik } from 'formik';
import React from 'react';
import {
  DefaultModalProps,
  useOpenModal,
  withLocationStateModal,
} from 'utils/modals';

import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@mui/material';
import { makeStyles } from '@mui/styles';

import { noop } from '@libs/helpers/common';
import { FormikSubmit } from '@libs/helpers/form';
import Typography from '@libs/ui/components/typography';

import { BooleanModalState } from '../types';
import { isBooleanSearchEmpty } from '../utils';
import BooleanListItems from './BooleanListItems';
import BooleanSearchFormula from './BooleanSearchFormula';
import NotRowItem from './NotRowItem';
import TrackFormChanges from './TrackFormChanges';
import { validator } from './validator';

interface ModalData {
  initialValues: BooleanModalState;
}

interface BooleanModalProps
  extends React.PropsWithChildren<DefaultModalProps<ModalData>> {
  onSave: (form: BooleanModalState) => void;
  onChange?: (form: BooleanModalState) => void;
}

const MODAL_ID = 'boolean_search';

const useStyles = makeStyles((theme) => ({
  modal: {
    padding: theme.spacing(10),
    maxWidth: 700,
    width: '100%',
    background: theme.palette.grey[200],
    borderRadius: theme.spacing(4),
  },
  title: {
    padding: 0,
    textAlign: 'center',
  },
  actions: {
    paddingTop: theme.spacing(6),
    justifyContent: 'center',
  },
  noHorizontalPadding: {
    paddingLeft: 0,
    paddingRight: 0,
    overflowY: 'visible',
  },
}));

const BooleanModalComponent = ({
  isOpen,
  close,
  modalData,
  onSave,
  onChange,
  children,
}: BooleanModalProps) => {
  const classes = useStyles();
  const initialValues = (modalData || {}).initialValues!;
  const onSubmit: FormikSubmit<BooleanModalState> = async (values) => {
    onSave(values);
    close();
    return true;
  };

  if (!initialValues) {
    return null;
  }

  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validationSchema={validator}
    >
      {({ values, submitForm, errors }) => {
        const isEmpty = isBooleanSearchEmpty(values);
        const hasErrors = !!Object.keys(errors).length;

        return (
          <Dialog
            classes={{ paper: classes.modal }}
            open={isOpen}
            onClose={close}
            maxWidth="md"
          >
            {onChange && (
              <TrackFormChanges
                values={values}
                onChange={hasErrors ? noop : onChange}
                timeout={100}
              />
            )}
            <DialogTitle className={classes.title}>Boolean search</DialogTitle>
            <DialogContent className={classes.noHorizontalPadding}>
              <Box pt={10} pb={10}>
                <Typography textAlign="center">
                  Select <b>mandatory</b> skills for this role.
                </Typography>
              </Box>

              <BooleanListItems items={values.items} />

              <NotRowItem />

              {!isEmpty && (
                <Box pt={10} pb={4}>
                  <Typography textAlign="center" variant="body2">
                    Your search will be as follows:
                  </Typography>
                </Box>
              )}
              <BooleanSearchFormula state={values} grey />
            </DialogContent>
            {children}
            <DialogActions
              className={cn(classes.actions, classes.noHorizontalPadding)}
            >
              <Button
                onClick={submitForm}
                variant="contained"
                color="primary"
                autoFocus
                size="large"
              >
                Save search
              </Button>
            </DialogActions>
          </Dialog>
        );
      }}
    </Formik>
  );
};

export const BooleanModal = withLocationStateModal({ id: MODAL_ID })(
  BooleanModalComponent,
);

export const useOpenBooleanModal = () => useOpenModal<ModalData>(MODAL_ID);
