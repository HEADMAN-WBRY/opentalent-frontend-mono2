import { TalentsBooleanSkillsFilterInput } from '@libs/graphql-types';

import { BooleanModalState } from './types';

export const mapBooleanToServer = ({
  not,
  items = [],
}: BooleanModalState): TalentsBooleanSkillsFilterInput => ({
  not_skills_ids: not?.map((i) => Number(i.value!) as any),
  boolean_skills_filter_items: items.map((i, index) => ({
    all_of_skills_ids: i.and.map((i) => Number(i.value)),
    one_of_skills_ids: i.or.map((i) => Number(i.value)),
    boolean_operator: items?.[index - 1]?.relationWithNext || 'AND',
  })) as any,
});

export const isBooleanSearchEmpty = (
  booleanState?: BooleanModalState,
): boolean => {
  const noItemsInNot = !booleanState?.not?.length;
  const noItems = !!booleanState?.items?.every(
    (i) => !i?.and?.length && !i?.or?.length,
  );

  return noItemsInNot && noItems;
};
