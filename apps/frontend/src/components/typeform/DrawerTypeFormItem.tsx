import LiveHelpOutlinedIcon from '@mui/icons-material/LiveHelpOutlined';
import { PopupButton } from '@typeform/embed-react';
import React from 'react';

import Button from '@libs/ui/components/button';

import { useFormId } from './hooks';
import useStyles from './styles';

interface DrawerTypeFormItemProps {}

const BtnComponent = PopupButton as any;

export const DrawerTypeFormItem = (props: DrawerTypeFormItemProps) => {
  const formId = useFormId();
  const classes = useStyles();

  return (
    <BtnComponent as="div" id={formId}>
      <Button className={classes.bth} startIcon={<LiveHelpOutlinedIcon />}>
        give feedback
      </Button>
    </BtnComponent>
  );
};
