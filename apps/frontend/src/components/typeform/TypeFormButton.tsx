import { Fab } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { PopupButton } from '@typeform/embed-react';
import cn from 'classnames';
import React from 'react';

import { useFormId } from './hooks';

interface TypeFormButtonProps {}

const useStyles = makeStyles({
  btn: {
    position: 'fixed',
    right: 40,
    bottom: 40,
    width: 60,
    height: 60,
    zIndex: 10,
    background:
      'url(https://images.typeform.com/images/AZesmPtAjfbC) no-repeat center',
    backgroundSize: 'contain',
  },
});

const TypeFormButton = (props: TypeFormButtonProps) => {
  const classes = useStyles();
  const formId = useFormId();

  return (
    <PopupButton
      as={Fab as any}
      className={cn(classes.btn, 'typeFormBtn')}
      id={formId}
    >
      {' '}
    </PopupButton>
  );
};

export default TypeFormButton;
