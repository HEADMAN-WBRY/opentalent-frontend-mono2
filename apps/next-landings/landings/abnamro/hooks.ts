import { FormikHelpers } from 'formik';
import { useCallback } from 'react';
import * as yup from 'yup';

import { paths } from '../../utils/consts';
import { InviteFormModel } from './types';

const ABN_COMPANY_ID = 14;

export const useInviteFormSubmit = () => {
  const onSubmit = useCallback(
    async (
      variables: InviteFormModel,
      helpers: FormikHelpers<InviteFormModel>,
    ) => {
      window.location.href = `${paths.talentOnboarding}?companyId=${ABN_COMPANY_ID}&email=${variables.email}`;
      helpers.resetForm();
    },
    [],
  );

  return { onSubmit, loading: false };
};

export const useValidator = () => {
  return yup.object().shape({
    email: yup.string().email().required(),
  });
};
