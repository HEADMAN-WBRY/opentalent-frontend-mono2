import { Grid, Typography } from '@mui/material';
import { makeStyles } from '@mui/styles';
import React from 'react';

interface LinkListProps {
  list: {
    link: string;
    Icon?: React.ComponentType<any>;
    text: string;
  }[];
}

const useStyles = makeStyles((theme) => ({
  list: {
    padding: 0,
    listStyle: 'none',
    marginTop: theme.spacing(4),
    marginBottom: 0,

    '& li': {
      height: 38,
    },
  },
  icon: {
    width: 17,
  },
}));

const LinkList = ({ list }: LinkListProps) => {
  const classes = useStyles();

  return (
    <ul className={classes.list}>
      {list.map(({ link, Icon, text }) => (
        <li key={text}>
          <Grid spacing={2} container component="a" target="_blank" href={link}>
            {Icon && (
              <Grid item>
                <Icon className={classes.icon} />
              </Grid>
            )}
            <Grid item>
              <Typography variant="body2">{text}</Typography>
            </Grid>
          </Grid>
        </li>
      ))}
    </ul>
  );
};

export default LinkList;
