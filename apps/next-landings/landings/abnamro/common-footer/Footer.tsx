import React from 'react';

import { Container } from '@mui/material';
import { makeStyles } from '@mui/styles';

import BottomBlock from './BottomBlock';
import TopBlock from './TopBlock';

interface FooterProps {}

const useStyles = makeStyles((theme) => ({
  wrapper: { color: 'white' },

  topSection: {
    padding: `${theme.spacing(16)} 0 ${theme.spacing(18)}`,
    overflow: 'hidden',
    background: theme.palette.secondary.main,

    [theme.breakpoints.down('md')]: {
      padding: `46px 0 42px`,
    },
  },

  botSection: {
    padding: `${theme.spacing(6)} 0`,
    overflow: 'hidden',
    background: theme.palette.secondary.dark,
  },
}));

const Footer = (props: FooterProps) => {
  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <div className={classes.topSection}>
        <Container>
          <TopBlock />
        </Container>
      </div>
      <div className={classes.botSection}>
        <Container>
          <BottomBlock />
        </Container>
      </div>
    </div>
  );
};

export default Footer;
