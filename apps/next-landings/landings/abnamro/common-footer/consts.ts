import InstagramIcon from '@mui/icons-material/Instagram';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import MailOutlineIcon from '@mui/icons-material/MailOutline';

import { EXTERNAL_LINKS } from '@libs/helpers/consts';

import { LANDINGS_EXTERNAL_LINKS, paths } from '../../../utils/consts';
import { ReactComponent as SlackIcon } from './assets/slack.svg';

export const LINKS_LIST_1 = [
  {
    link: 'mailto:hello@opentalent.co',
    text: 'hello@opentalent.co',
    Icon: MailOutlineIcon,
  },
  {
    link: 'https://www.linkedin.com/company/worklikeneverbefore/',
    text: 'LinkedIn',
    Icon: LinkedInIcon,
  },
  {
    link: 'https://www.instagram.com/opentalent.co/',
    text: 'Instagram',
    Icon: InstagramIcon,
  },
  { link: EXTERNAL_LINKS.slackCommunity, text: 'Slack', Icon: SlackIcon },
];

export const LINKS_LIST_2 = [
  // {
  //   link: 'https://app.opentalent.co/quick-auth',
  //   text: 'Sign In',
  //   Icon: undefined,
  // },
  {
    link: 'https://www.notion.so/opentalent/Where-Talent-Come-To-Thrive-de1d9f55a05a47b6b8f9d076187fbcbe',
    text: 'About Us',
    Icon: undefined,
  },
  // {
  //   link: EXTERNAL_LINKS.pricing,
  //   text: 'Pricing',
  //   Icon: undefined,
  // },
  {
    link: 'https://www.notion.so/opentalent/Resource-Centre-2ed1aa571b2341418527ea16a81e01ab',
    text: 'Resource Center',
    Icon: undefined,
  },
  {
    link: EXTERNAL_LINKS.privacyPolicy,
    text: 'Privacy & Cookie Policy',
    Icon: undefined,
  },
];

export const LINKS_LIST_3 = [
  {
    link: paths.talentLanding,
    text: 'Join us',
    Icon: undefined,
  },
  {
    link: 'https://www.notion.so/opentalent/Community-Guidelines-8297d78bd5664e8384876d516488879d',
    text: 'Guidelines',
    Icon: undefined,
  },
  {
    link: LANDINGS_EXTERNAL_LINKS.statistics,
    text: 'Key statistics',
    Icon: undefined,
  },
];
