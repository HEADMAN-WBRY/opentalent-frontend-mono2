import React from 'react';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import SectionTitle from '../../components/new-landings/section-title';
import { ReactComponent as ArrowRight } from './assets/arrow-right.svg';

interface VisitBlockProps {}

const useStyles = makeStyles((theme) => ({
  root: {
    background: 'rgba(250, 250, 1, 0.24)',
    textAlign: 'center',
    padding: `64px 18px`,
  },
  link: {
    textDecoration: 'underline',
    color: theme.palette.text.secondary,
  },
}));

const VisitBlock = (props: VisitBlockProps) => {
  const classes = useStyles();
  return (
    <Box className={classes.root}>
      <Box pb={4}>
        <SectionTitle fontWeight={600}>
          Not looking for FreelancING work?
        </SectionTitle>
      </Box>
      <a
        className={classes.link}
        target="_blank"
        rel="noreferrer"
        href="https://www.abnamro.com/en/careers"
      >
        <Grid
          wrap="nowrap"
          alignContent="center"
          justifyContent="center"
          container
        >
          <Grid>
            <Typography color="textSecondary" variant="body1">
              Visit the ABN AMRO careers site for permanent positions
            </Typography>
          </Grid>
          <Grid>
            <ArrowRight />
          </Grid>
        </Grid>
      </a>
    </Box>
  );
};

export default VisitBlock;
