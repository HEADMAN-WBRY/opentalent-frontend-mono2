import { OpenGraph } from 'apps/next-landings/components/meta-data';
import Head from 'next/head';
import React from 'react';

import { Box } from '@mui/material';

import CardsBlock from './CardsBlock';
import ErrorModal from './ErrorModal';
import Header from './Header';
import Intro from './Intro';
import SuccessModal from './SuccessModal';
import VisitBlock from './VisitBlock';
import Footer from './common-footer';

interface LandingProps {}

const Landing = (props: LandingProps) => {
  return (
    <>
      <Head>
        <title>Join OpenTalent 🔥</title>
      </Head>
      <OpenGraph title="Abn Amro" description="Abn Amro description" />
      <Box overflow="hidden">
        <Header />
        <Intro />
        <CardsBlock />
        <VisitBlock />
        <Footer />
        <ErrorModal />
        <SuccessModal />
      </Box>
    </>
  );
};

export default Landing;
