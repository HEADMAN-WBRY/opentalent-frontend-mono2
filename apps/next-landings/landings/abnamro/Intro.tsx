import { Formik } from 'formik';
import React from 'react';

import { Box, Grid, Container } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { EXTERNAL_LINKS } from '@libs/helpers/consts';
import { useMediaQueries } from '@libs/helpers/hooks/media-queries';
import Button from '@libs/ui/components/button';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import Typography from '@libs/ui/components/typography';

import { paths } from '../../utils/consts';
import { ReactComponent as AmroLogo } from './assets/amro-logo.svg';
import { ReactComponent as ArrowRight } from './assets/arrow-right.svg';
import { useInviteFormSubmit, useValidator } from './hooks';
import { InviteFormModel } from './types';

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 84,
    height: 540,
    display: 'flex',
    alignItems: 'center',
    background: `linear-gradient(90deg, #f6f6f6 58.333333%, #eeeeed 58.333333%)`,

    [theme.breakpoints.down('md')]: {
      'body &': {
        background: '#f6f6f6',
        alignItems: 'flex-start',
        height: 'auto',
      },
    },
  },
  container: {
    height: '100%',
    maxWidth: 1126,

    [theme.breakpoints.down('md')]: {
      'body &': {
        height: 'auto',
        padding: 0,
      },
    },
  },
  leftBlock: {
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    background: '#f6f6f6',
    paddingRight: theme.spacing(16),

    [theme.breakpoints.down('md')]: {
      'body &': {
        height: 'auto',
        padding: `${theme.spacing(12)} ${theme.spacing(3)} ${theme.spacing(3)}`,
      },
    },
  },
  rightBlock: {
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    background: '#eeeeed',
    paddingLeft: theme.spacing(16),

    [theme.breakpoints.down('md')]: {
      'body &': {
        height: 'auto',
        width: '100%',
        padding: `${theme.spacing(12)} ${theme.spacing(3)} ${theme.spacing(
          14,
        )}`,
        justifyContent: 'center',
      },
    },
  },
  textWithBg: {
    background: theme.palette.primary.main,
  },
}));

const INITIAL_STATE: InviteFormModel = {
  first_name: '',
  last_name: '',
  email: '',
};

const Intro = () => {
  const { onSubmit, loading } = useInviteFormSubmit();
  const classes = useStyles();
  const { isSM, isXS } = useMediaQueries();
  const isSmall = isSM || isXS;
  const validator = useValidator();

  return (
    <Box className={classes.root}>
      <Grid
        className={classes.container}
        alignItems="center"
        component={Container}
        container
      >
        <Grid className={classes.leftBlock} md={7} item>
          <Grid
            spacing={10}
            wrap={isSmall ? 'wrap' : 'nowrap'}
            justifyContent={isSmall ? 'center' : 'flex-start'}
            container
          >
            <Grid item>
              <AmroLogo />
            </Grid>
            <Grid item>
              <Box textAlign={isSmall ? 'center' : 'inherit'} pb={8}>
                <Typography
                  fontWeight={700}
                  fontStyle="italic"
                  variant={isSmall ? 'h4' : 'h3'}
                >
                  Hi <span className={classes.textWithBg}>Awesome You!</span>
                </Typography>
              </Box>
              <Box pr={6}>
                <Box textAlign={isSmall ? 'center' : 'inherit'} pb={8}>
                  <Typography variant="body1">
                    Welcome to the <b>ABN AMRO</b> talent community on
                    OpenTalent; the invite-only network for trusted freelancer
                    talent and leading companies to work together, on-demand.
                  </Typography>
                </Box>
                <Box textAlign={isSmall ? 'center' : 'inherit'} pb={8}>
                  <Typography variant="body2" color="textSecondary">
                    ABN AMRO is happy to invite you to their exclusive talent
                    pool. Join now to find new gigs at ABN AMRO and other
                    companies.
                  </Typography>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Grid>
        <Grid
          className={classes.rightBlock}
          component={Box}
          pl={16}
          item
          md={5}
        >
          <Box width="100%">
            <Box pb={6}>
              <Typography variant="body1">
                Request your invite to the <b>ABN AMRO</b> talent community -
                we’ll send it by email.
              </Typography>
            </Box>
            <Formik
              validationSchema={validator}
              initialValues={INITIAL_STATE}
              onSubmit={onSubmit}
            >
              {({ handleSubmit }) => (
                <Box>
                  <Box pb={4}>
                    <ConnectedTextField
                      variant="filled"
                      fullWidth
                      size="small"
                      label="Email"
                      name="email"
                    />
                  </Box>
                  <Button
                    onClick={() => handleSubmit()}
                    fullWidth
                    variant="contained"
                    color="secondary"
                    disabled={loading}
                  >
                    Request Now
                  </Button>

                  <Box pt={4}>
                    <Typography
                      fontSize={10}
                      variant="overline"
                      color="textSecondary"
                    >
                      By clicking “Request invite” you agree to our{' '}
                      <a
                        target="_blank"
                        rel="noreferrer"
                        href={EXTERNAL_LINKS.privacyPolicy}
                      >
                        <Typography
                          component="span"
                          variant="overline"
                          color="tertiary"
                          fontSize={10}
                        >
                          Privacy&nbsp;&&nbsp;Cookie&nbsp;Policy.
                        </Typography>
                      </a>
                    </Typography>
                  </Box>
                  <Box textAlign="center" pt={4}>
                    <Typography variant="body2" color="textSecondary">
                      Already have an account?&nbsp;
                      <a href={paths.mainAppRoute}>
                        <Typography
                          component="span"
                          variant="body2"
                          color="tertiary"
                        >
                          Sign In <ArrowRight style={{ marginBottom: -7 }} />
                        </Typography>
                      </a>
                    </Typography>
                  </Box>
                </Box>
              )}
            </Formik>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
};

export default Intro;
