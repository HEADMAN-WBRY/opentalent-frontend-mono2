import cn from 'classnames';
import React from 'react';

import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';
import { Carousel } from './carousel/Carousel';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    padding: '100px 0 130px 0',
    textAlign: 'center',

    [theme.breakpoints.down('md')]: {
      padding: '40px 24px 48px',
    },
  },
  blockTitle: {
    lineHeight: '40px',
    fontWeight: 600,
    fontStyle: 'italic',
    marginBottom: theme.spacing(15),
  },
}));

interface Comment {
  id: number;
  avatarSrc: string;
  name: string;
  position: string;
  company?: string;
  text: string;
}

interface IProps { }

export const CarouselBlock: React.FC<IProps> = () => {
  const classes = useStyles();

  return (
    <Box className={classes.wrapper}>
      <Typography
        transform="uppercase"
        variant="h4"
        className={classes.blockTitle}
      >
        WHAT OUR MEMBERS SAY:
      </Typography>

      <Carousel />
    </Box>
  );
};
