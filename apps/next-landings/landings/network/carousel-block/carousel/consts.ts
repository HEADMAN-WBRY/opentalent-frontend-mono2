import { CarouselItemData } from './types';

export const CARDS_DATA: CarouselItemData[] = [
  {
    text: 'Just two weeks after setting up my profile, I was contacted for a role that was a perfect fit for my skill set. I got the job soon after.',
    name: 'Adam',
    img: '/hub-page/carousel/adam@2x.png',
    role: 'Data Scientist',
  },
  {
    text: 'The job matches I get from OpenTalent are spot-on. Saves me hours in job searching.',
    name: 'Monica',
    img: '/hub-page/carousel/monica@2x.png',
    role: 'QA Tester',
  },
  {
    text: 'OpenTalent got me a great discount at a premium co-working space, which became a hub for valuable professional connections.',
    name: 'Nadia',
    img: '/hub-page/carousel/nadia@2x.png',
    role: 'Designer',
  },
  {
    text: "Found a stellar remote role in a top Dutch company thanks to OpenTalent. Couldn't be happier.",
    name: 'Mariusz',
    img: '/hub-page/carousel/mariusz@2x.png',
    role: 'Developer',
  },
];
