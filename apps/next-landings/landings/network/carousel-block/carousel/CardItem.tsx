import cn from 'classnames';
import React from 'react';

import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import { ReactComponent as Commas } from './assets/commas.svg';
import { CarouselItemData } from './types';

interface CardItemProps extends CarouselItemData { }

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 320,
    borderRadius: theme.spacing(4),
    background: 'white',
    padding: theme.spacing(12, 6, 6, 6),
    position: 'relative',
    boxShadow: '0px 11px 16px rgba(0, 0, 0, 0.1)',
    minHeight: 216,

    [theme.breakpoints.down('sm')]: {
      height: 'auto',
    },

    '& *': {
      textAlign: 'center',
    },
  },
  image: {
    position: 'absolute',
    left: 'calc(50% - 36px)',
    top: -36,
  },
  quote: {
    position: 'relative',
    marginBottom: theme.spacing(4),
    // padding: theme.spacing(0, 2),

    '& p': {
      letterSpacing: 0,
      lineHeight: '20px',
    },
  },
  userData: {},
  coma: {
    position: 'absolute',
    content: "''",
    opacity: '0.8',
  },
  comaUp: {
    top: '-8px',
    left: '-8px',

    [theme.breakpoints.down('md')]: {},
  },
  comaDown: {
    bottom: '-8px',
    right: '-8px',
  },
}));

export const CardItem = ({ name, text, img, role }: CardItemProps) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.image}>
        <img srcSet={`${img} 2x`} alt={name} />
      </div>
      <div className={classes.quote}>
        <Typography variant="body2" textAlign="center">
          {text}
        </Typography>
        <Commas className={cn(classes.coma, classes.comaUp)} />
        <Commas className={cn(classes.coma, classes.comaDown)} />
      </div>

      <div className={classes.userData}>
        <Typography fontWeight={600} variant="body2" textAlign="center">
          {name}
        </Typography>
        <Typography variant="body2" textAlign="center">
          {role}
        </Typography>
      </div>
    </div>
  );
};
