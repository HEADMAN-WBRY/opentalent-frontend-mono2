import { ReactComponent as LogoIcon } from 'apps/next-landings/assets/opentalent_dark.svg';
import { ReactComponent as WhatsAppIcon } from 'apps/next-landings/public/header/whatsapp.svg';
import { paths, RECRUITER_PAGE_NAME } from 'apps/next-landings/utils/consts';
import React from 'react';

import { ExpandLess, ExpandMore } from '@mui/icons-material';
import CloseIcon from '@mui/icons-material/Close';
import PersonOutlineIcon from '@mui/icons-material/PersonOutline';
import {
  Box,
  Collapse,
  Drawer,
  Grid,
  IconButton,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Typography,
} from '@mui/material';
import { makeStyles } from '@mui/styles';

import { EXTERNAL_LINKS } from '@libs/helpers/consts';

interface PageDrawerProps {
  isOpen: boolean;
  toggle: VoidFunction;
  className?: string;
}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    background: theme.palette.secondary.main,
    color: 'white',
    width: '90vw',
    maxWidth: '320px',
    padding: `${theme.spacing(4)} ${theme.spacing(5)}`,
  },
  logoWrapper: {
    color: theme.palette.primary.main,
  },
  listItem: {
    paddingLeft: 0,
    paddingRight: 0,
  },
}));

const PageDrawer: React.FC<PageDrawerProps> = ({
  isOpen,
  toggle,
  className,
}) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);

  const handleClick = () => {
    setOpen(!open);
  };

  return (
    <Drawer
      classes={{ paper: classes.wrapper, root: className }}
      anchor="left"
      open={isOpen}
      onClose={toggle}
    >
      <Grid container>
        <Grid item>
          <IconButton size="small" onClick={toggle} style={{ color: 'white' }}>
            <CloseIcon />
          </IconButton>
        </Grid>
        <Grid className={classes.logoWrapper} item component={Box} mb={-2}>
          <LogoIcon height={35} />
        </Grid>
      </Grid>

      <Box pb={8} pt={8}>
        <Typography variant="h5">Main menu</Typography>

        <List
          sx={{ width: '100%' }}
          component="nav"
          aria-labelledby="nested-list-subheader"
        >
          <ListItemButton className={classes.listItem} href={paths.companiesV2}>
            <ListItemText primary="COMPANIES" />
          </ListItemButton>
          <ListItemButton
            className={classes.listItem}
            href={EXTERNAL_LINKS.whatsAppForCompanies}
          >
            <ListItemText
              primary={
                <>
                  CONTACT US&nbsp;
                  <WhatsAppIcon
                    style={{
                      transform: 'translateY(3px)',
                    }}
                  />
                </>
              }
            />
          </ListItemButton>
          <ListItemButton
            className={classes.listItem}
            href={paths.mainAppRoute}
          >
            <ListItemIcon style={{ color: 'white', minWidth: '34px' }}>
              <PersonOutlineIcon />
            </ListItemIcon>
            <ListItemText primary="Sign in" />
          </ListItemButton>
        </List>
      </Box>
    </Drawer>
  );
};

export default PageDrawer;
