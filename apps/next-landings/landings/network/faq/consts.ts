import { AccordionProps } from './AccordionItem';

export const FAQ_ITEMS: Pick<AccordionProps, 'title' | 'text'>[] = [
  {
    title: 'Who is OpenTalent for?',
    text: 'OpenTalent is members-only platform and community for ‘high-skilled’ professionals across Europe to gain disproportionate advantages in building a career and/or business-of-one.',
  },
  {
    title: 'Why do I need to get verified?',
    text: 'Account verification is a crucial step that helps OpenTalent to maintain the quality and security of its platform and community. You can continue to use the platform while our team verifies your account.',
  },
  {
    title: 'How much does OpenTalent cost?',
    text: 'A membership to OpenTalent costs €4,99 per month. As a member, you get full access to the platform and its features as well as the community.',
  },
  {
    title: 'Can I use OpenTalent for FREE?',
    text: 'Yes, after creating your account on OpenTalent you can access select features for FREE, like the ‘Perks & Benefits’ and ‘Invite & Earn’ programs.',
  },
  {
    title: 'What happens if I do not pay?',
    text: 'You will not be able to access ‘member-only’ features, like the Job Board or the Community.',
  },
  {
    title: 'How do I cancel?',
    text: 'OpenTalent is flexible. You can cancel your membership anytime. There are no cancellation fees – start or stop your account whenever you want.',
  },
  {
    title: 'What happens after I activate my membership?',
    text: 'After activating your membership you will be able to access the platform and start using all its features as well as contact anyone in the community.',
  },
];
