import React, { useState } from 'react';

import { Box, Grid } from '@mui/material';

import Typography, { OuterLink } from '@libs/ui/components/typography';

import { AccordionItem } from './AccordionItem';
import { FAQ_ITEMS } from './consts';
import { makeStyles } from '@mui/styles';
import Button from '@libs/ui/components/button';

interface FAQProps { }

const useStyles = makeStyles((theme) => ({
  wrapper: {
    padding: '100px 0 100px 0',
    textAlign: 'center',
    background: theme.palette.secondary.dark,
    color: 'white',

    [theme.breakpoints.down('sm')]: {
      backgroundColor: theme.palette.secondary.dark,
    },

    [theme.breakpoints.down('md')]: {
      padding: '40px 24px 48px',
    },
  },
  blockTitle: {
    lineHeight: '40px',
    fontWeight: 700,
    fontStyle: 'italic',
    marginBottom: theme.spacing(15),
  },
}));

export const FAQ = (props: FAQProps) => {
  const classes = useStyles();
  const [activeIndex, setActiveIndex] = useState<number>();

  return (
    <Box className={classes.wrapper}>
      <Typography
        transform="uppercase"
        variant="h4"
        className={classes.blockTitle}
      >
        Frequently Asked Questions:
      </Typography>

      <Box maxWidth={600} style={{ margin: '0 auto' }}>
        {FAQ_ITEMS.map((i, index) => (
          <AccordionItem
            {...i}
            key={i.title}
            isActive={index === activeIndex}
            onChange={() =>
              setActiveIndex(index === activeIndex ? undefined : index)
            }
          />
        ))}
      </Box>
      <Box mt={6} mb={12}>
        <Typography textAlign="center">
          Do you have other questions? Write an email to{' '}
          <OuterLink color="info.main" href={`mailto:hello@opentalent.co`}>
            hello@opentalent.co
          </OuterLink>
        </Typography>
      </Box>

      <Typography
        fontStyle="italic"
        variant="h5"
        textAlign="center"
        transform="uppercase"
        fontWeight={700}
        paragraph
        whiteSpace="break-spaces"
      >
        {`JOIN THOUSANDS OF PROFESSIONALS ACROSS
EUROPE THRIVING WITH OPENTALENT.`}
      </Typography>

      <Box mt={14}>
        <Grid container justifyContent="center">
          <Grid item>
            <Button
              onClick={() => {
                window.scroll({
                  top: 0,
                  behavior: 'smooth',
                });
              }}
              variant="contained"
              color="primary"
              size="large"
              style={{ minWidth: 230 }}
            >
              <b>Apply today</b>
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
};
