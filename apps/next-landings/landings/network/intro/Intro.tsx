import { INFINITY_SIGN } from 'apps/next-landings/utils/consts';
import { Form, Formik } from 'formik';
import React from 'react';
import * as yup from 'yup';

import { Box, Button, Container, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { formatNumberSafe } from '@libs/helpers/format';
import { useMediaQueries } from '@libs/helpers/hooks/media-queries';
import { ConnectedTextField } from '@libs/ui/components/form/text-field';
import Typography from '@libs/ui/components/typography';

import { FORM_ID } from '../consts';
import { useSubmitAction } from './hooks';

interface IntroProps {
  talentsCount: number;
  countriesCount: number;
}

const useStyles = makeStyles((theme) => ({
  '@font-face': {
    fontFamily: 'MissRhinetta',
    src: 'url(/fonts/Miss-Rhinetta.otf)',
  },
  wrapper: {
    background: theme.palette.secondary.main,
    color: 'white',

    [theme.breakpoints.down('sm')]: {
      backgroundColor: theme.palette.secondary.dark,
    },
  },
  title: {
    fontSize: 130,
    lineHeight: '86px',
    fontFamily: "'MissRhinetta', sans-serif",
    fontDisplay: 'swap',
    marginBottom: '40px',

    [theme.breakpoints.down('md')]: {
      fontSize: 80,
      lineHeight: '48px',
      maxWidth: 340,
      marginBottom: '24px',
    },

    '& > b': {
      fontFamily: 'Poppins',
      fontStyle: 'italic',
      fontSize: 64,

      [theme.breakpoints.down('md')]: {
        fontSize: 34,
        lineHeight: '54px',
        maxWidth: 340,
      },
    },
  },
  titleLight: {
    color: theme.palette.other.oldPrimary,
  },
  subtitle: {
    fontStyle: 'italic',
  },
  container: {
    height: 750,
    display: 'flex',
    alignItems: 'center',
    background: `url(/network-page/intro_img@2x.png) no-repeat right 12px  center`,
    backgroundSize: '33%',

    [theme.breakpoints.down('md')]: {
      backgroundSize: '40%',
      backgroundPosition: '50% top',
      height: 'auto',
      padding: `0 ${theme.spacing(4)}`,
    },

    [theme.breakpoints.down('sm')]: {
      background: 'unset',
      backgroundSize: 'unset',
      backgroundPosition: 'unset',
    },
  },
  content: {
    [theme.breakpoints.down('md')]: {
      paddingTop: '36vw',
      paddingBottom: theme.spacing(14),
    },

    [theme.breakpoints.down('sm')]: {
      paddingTop: theme.spacing(14),
    },
  },
  info: {
    maxWidth: 640,
    paddingBottom: theme.spacing(10),
    lineHeight: '28px',

    [theme.breakpoints.down('md')]: {
      paddingBottom: theme.spacing(4),
    },
  },
  input: {
    '& > .MuiInputBase-root': {
      height: 48,
      overflow: 'hidden',
    },
  },
  success: {
    border: `1px solid ${theme.palette.primary.main}`,
    maxWidth: 310,
    padding: theme.spacing(6),
    marginTop: theme.spacing(6),
  },
  button: {
    background: `${theme.palette.other.oldPrimary} !important`,
    fontWeight: 'bold',
  },
}));

const validator = yup.object({
  email: yup.string().trim().required('Please add your email.').email(),
});

const Intro = ({ talentsCount, countriesCount }: IntroProps) => {
  const { isSM } = useMediaQueries();
  const classes = useStyles();
  const { onSubmit, loading } = useSubmitAction();

  return (
    <Formik
      onSubmit={onSubmit}
      validationSchema={validator}
      initialValues={{ email: '' }}
    >
      <Form>
        <div className={classes.wrapper}>
          <Container className={classes.container}>
            <Grid className={classes.content} container>
              <Grid id={FORM_ID} sm={isSM ? 12 : 7} item>
                <Typography
                  className={classes.title}
                  paragraph
                  color="inherit"
                  variant="h1"
                >
                  Join OpenTalent <b>Thrive. Together.</b>
                </Typography>
                <Typography className={classes.info}>
                  Unlock your full potential with OpenTalent — Europe’s #1
                  talent-centric membership for high-skilled professionals. Step
                  into a world of personalised job opportunities, networking
                  with like-minded peers, new income streams, and exclusive
                  perks & benefits.
                </Typography>
                <Box mb={6} pt={4}>
                  <Grid
                    direction={isSM ? 'column' : 'row'}
                    spacing={4}
                    container
                  >
                    <Grid xs={isSM ? 12 : 6} item>
                      <ConnectedTextField
                        placeholder="Enter your email"
                        name="email"
                        fullWidth
                        size="medium"
                        variant="outlined"
                        className={classes.input}
                      />
                    </Grid>
                    <Grid xs={isSM ? 12 : 5} item>
                      <Button
                        variant="contained"
                        color="primary"
                        fullWidth
                        type="submit"
                        size="large"
                        disabled={loading}
                        data-test-id="email-submit"
                        className={classes.button}
                      >
                        Apply to Join
                      </Button>
                    </Grid>
                  </Grid>
                </Box>
                <Typography>
                  Join{' '}
                  <b>
                    {formatNumberSafe(talentsCount, {
                      fallback: INFINITY_SIGN,
                    })}
                  </b>{' '}
                  vetted professionals across Europe on OpenTalent 🎉
                </Typography>
              </Grid>
            </Grid>
          </Container>
        </div>
      </Form>
    </Formik>
  );
};

export default Intro;
