import Image from 'next/image';
import React from 'react';

import { ReactComponent as HundredPercent } from './assets/hundred-percent.svg';
import { ReactComponent as Invitations } from './assets/invitations.svg';
import { ReactComponent as Matching } from './assets/matching.svg';
import { ReactComponent as Notifications } from './assets/notifications.svg';
import { PerkCards } from './perk-cards';

export const FORM_ID = 'FORM';

export interface ItemType {
  id: number;
  title: string;
  subtitle: string;
  points: Array<string>;
  imgComponent: JSX.Element;
  revert: boolean;
}

export const WHAT_TO_EXPECT_ITEMS: Array<ItemType> = [
  {
    id: 1,
    title: 'Where Talent is King.',
    subtitle: `At OpenTalent, we're building Europe's #1 talent-centric platform; a members-only place for people to build a business-of-one and do what they love. Welcome to a better alternative.`,
    points: [
      'Find new jobs that match your skills',
      'Connect with peers who share your drive',
      'Make and save money with new opportunities',
    ],
    imgComponent: <HundredPercent />,
    revert: false,
  },
  {
    id: 2,
    title: `Relevant roles only,
     based on your skills.`,
    subtitle: `Get matched with relevant roles only. At fast-growing, talent-centric companies. Creating your profile, tell us what you’re looking for and what you’re good at, and we’ll take care of the rest!`,
    points: [
      'Get matched with relevant jobs only',
      'ChatGPT, AI-powered job matching',
      'At fast-growing, innovative companies',
    ],
    imgComponent: <Matching />,
    revert: true,
  },
  {
    id: 3,
    title: `Top job matches,
    delivered instantly.`,
    subtitle: `When new jobs match your profile we’ll send you an email. No need to constantly login to OpenTalent - we’ll let you know when things get interesting!`,
    points: ['Top job match alerts only', 'Delivered straight to your inbox'],
    imgComponent: (
      <Image
        style={{ maxWidth: '100%', objectFit: 'contain' }}
        src="/network-page/notifications@2x.png"
        alt="Network"
        width={470}
        height={361}
      />
    ),
    revert: false,
  },
  {
    id: 4,
    title: `Extra earnings by
    spreading the word.`,
    subtitle: `We believe that talented individuals like you are best equipped to find new talent. So we productized the referral fee: invite new talent to our community and we’ll split all commissions with you!`,
    points: [
      'Get paid up to €5.000 per referral',
      'Build additional revenue streams',
    ],
    imgComponent: (
      <Image
        style={{ maxWidth: '100%', objectFit: 'contain' }}
        src="/network-page/earn@2x.png"
        alt="Network"
        width={470}
        height={361}
      />
    ),
    revert: true,
  },
  {
    id: 5,
    title: `Build quality network.`,
    subtitle: `Find, network and collaborate with anyone in the OpenTalent community. Meet new people, acquire new skills and find new colleagues`,
    points: ['Full candidate details + CVs', 'Chat with anyone'],
    imgComponent: (
      <Image
        style={{ maxWidth: '100%', objectFit: 'contain' }}
        src="/network-page/network@2x.png"
        alt="Network"
        width={470}
        height={361}
      />
    ),
    revert: false,
  },
  {
    id: 6,
    title: `Perks & Benefits`,
    subtitle: `We leverage the power of community to secure exclusive perks & benefits on professional services, like co-working spaces, software and insurances.`,
    points: [
      'Up to 50% discounts on exclusive deals',
      'New perks added every 2 weeks',
    ],
    imgComponent: <PerkCards />,
    revert: true,
  },
];

export const COMMENTS = [
  {
    id: 1,
    avatarSrc: '/network-page/avatars/avatar-1@2x.png',
    name: 'Sarah',
    position: 'Creative Designer - France',
    text: `Besides great matching jobs, OpenTalent  is a doorway to a thriving community of talented professionals.`,
  },
  {
    id: 2,
    avatarSrc: '/network-page/avatars/avatar-2@2x.png',
    name: 'Adam',
    position: 'Data Scientist - The Netherlands',
    text: `Soon after creating my profile on  OpenTalent, someone reached out about a position that perfectly matched my skills.  Within a week I landed the job.`,
  },
];
