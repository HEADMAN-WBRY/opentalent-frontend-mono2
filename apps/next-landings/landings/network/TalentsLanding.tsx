import {
  MainMetaTexts,
  OpenGraph,
} from 'apps/next-landings/components/meta-data';
import React from 'react';

import { makeStyles } from '@mui/styles';

import { useGetCompaniesLandingDataQuery } from '@libs/graphql-types';

import Comments from '../../components/comments';
import Footer from '../../components/common-footer';
import CommonHeader from '../../components/common-header';
import { COMMENTS } from './consts';
import Intro from './intro';
import Join from './join';
import WhatToExpect from './whatToExpect';
import { CarouselBlock } from './carousel-block';
import { FAQ } from './faq';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    overflow: 'hidden',

    '& .MuiButtonBase-root': {
      height: 48,
      textTransform: 'none',
      borderRadius: '4px',
    },
  },
  '@global': {
    body: {
      width: '100%',
    },
  },
  header: {
    background: `${theme.palette.secondary.main} !important`,
  },
  join: {
    '& > div': {
      maxWidth: 800,
      margin: '0 auto',
    },
  },
}));

const TalentsLanding: React.FC = () => {
  const classes = useStyles();
  const { data } = useGetCompaniesLandingDataQuery();

  return (
    <div className={classes.wrapper}>
      <MainMetaTexts
        title="OpenTalent | Only The Best For You"
        description="Meet Europe’s #1 talent-centric platform; a place where talent comes to find opportunity. Apply today!"
      />
      <OpenGraph
        title="OpenTalent | Only The Best For You"
        description="Meet Europe’s #1 talent-centric platform; a place where talent comes to find opportunity. Apply today!"
      />
      <CommonHeader activeItem="community" className={classes.header} />
      <Intro
        talentsCount={data?.commonAppInfo?.total_ot_approved_freelancers_count}
        countriesCount={
          data?.commonAppInfo?.total_ot_freelancers_countries_count
        }
      />
      <WhatToExpect />
      {/* <Comments title="WHAT OUR MEMBERS SAY:" comments={COMMENTS} /> */}
      <CarouselBlock />
      {/* <Join */}
      {/*   title={`JOIN THOUSANDS OF PROFESSIONALS ACROSS EUROPE THRIVING WITH OPENTALENT.`} */}
      {/*   btnText="Join OpenTalent" */}
      {/*   className={classes.join} */}
      {/* /> */}
      <FAQ />
      <Footer />
    </div>
  );
};

export default TalentsLanding;
