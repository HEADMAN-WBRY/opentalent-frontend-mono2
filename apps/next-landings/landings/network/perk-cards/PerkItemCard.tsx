import Typography from '@libs/ui/components/typography';
import cn from 'classnames';
import { makeStyles } from '@mui/styles';
import React from 'react';

export interface PerkItemCardProps {
  img: string;
  title: string;
  subtitle: string;
}

const useStyles = makeStyles((theme) => ({
  root: {
    textAlign: 'center',
    color: theme.palette.text.primary,
    width: 120,
    height: 72,
    position: 'relative',
    perspective: 1000,

    '&:hover $front': {
      transform: 'rotateX(180deg)',
    },

    '&:hover $back': {
      transform: 'rotateX(0deg)',
    },
  },
  sides: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: '100%',
    backfaceVisibility: 'hidden',
    transition: 'transform 1s',
    width: '100%',
    borderRadius: theme.spacing(3),
    overflow: 'hidden',
  },

  front: {
    background: 'white',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',

    '& img': {
      width: 56,
      height: 56,
    },
  },
  back: {
    transform: 'rotateX(-180deg)',
    background: theme.palette.other.oldPrimary,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },

  title: {
    fontWeight: 600,
    lineHeight: '24px',
  },
  subtitle: {},
}));

export const PerkItemCard = ({ img, title, subtitle }: PerkItemCardProps) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div className={cn(classes.back, classes.sides)}>
        <Typography variant="h5" fontStyle="italic" className={classes.title}>
          {title}
        </Typography>
        <Typography variant="overline" className={classes.subtitle}>
          {subtitle}
        </Typography>
      </div>
      <div className={cn(classes.front, classes.sides)}>
        <img srcSet={`${img} 2x`} alt={title} />
      </div>
    </div>
  );
};
