import { PerkItemCardProps } from './PerkItemCard';

export const PERKS: PerkItemCardProps[] = [
  {
    img: '/network-page/perks/safety_wings@2x.png',
    title: '35%',
    subtitle: 'discount',
  },
  {
    img: '/network-page/perks/little_emperors@2x.png',
    title: '1 year',
    subtitle: 'compliment',
  },
  {
    img: '/network-page/perks/worknmates@2x.png',
    title: '15%',
    subtitle: 'discount',
  },
  {
    img: '/network-page/perks/spotahome@2x.png',
    title: '20%',
    subtitle: 'discount',
  },
  {
    img: '/network-page/perks/genki@2x.png',
    title: 'Deal',
    subtitle: 'special',
  },
  {
    img: '/network-page/perks/hallo@2x.png',
    title: '50%',
    subtitle: 'discount',
  },
  {
    img: '/network-page/perks/better_help@2x.png',
    title: '50%',
    subtitle: 'discount',
  },

  {
    img: '/network-page/perks/airalo@2x.png',
    title: '15%',
    subtitle: 'discount',
  },
  {
    img: '/network-page/perks/parlego@2x.png',
    title: '15%',
    subtitle: 'discount',
  },
];
