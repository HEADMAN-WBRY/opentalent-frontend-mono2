import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';
import React from 'react';

import { PERKS } from './consts';
import { PerkItemCard } from './PerkItemCard';

interface PerkCardsProps { }

const useStyles = makeStyles((theme) => ({
  root: {
    textAlign: 'center',
    color: theme.palette.text.primary,
    maxWidth: 500,
  },

  grid: {
    [theme.breakpoints.down('md')]: {
      display: 'flex',
      justifyContent: 'center',
    },
  },

  perk: {
    '&:last-child': {
      [theme.breakpoints.down('sm')]: {
        display: 'none',
      },
    },
  },
}));

export const PerkCards = (props: PerkCardsProps) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Grid container spacing={4} className={classes.grid}>
        {PERKS.map((i) => (
          <Grid key={i.img} className={classes.perk} item>
            <PerkItemCard {...i} />
          </Grid>
        ))}
      </Grid>
    </Box>
  );
};
