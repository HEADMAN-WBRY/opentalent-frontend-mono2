import Link from 'next/link';
import React from 'react';

import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import Section from '../../../components/new-landings/section';
import { paths } from '../../../utils/consts';
import SectionOverline from '../shared/SectionOverline';

interface NoWorriesProps {}

const useStyles = makeStyles((theme) => ({
  topTitle: {
    display: 'block',
    marginBottom: theme.spacing(6),
    fontSize: 24,
    letterSpacing: '.02em',

    [theme.breakpoints.down('sm')]: {
      fontSize: 16,
      lineHeight: '22px',
      marginBottom: theme.spacing(4),
    },
  },
  midTitle: {
    display: 'block',
    marginBottom: theme.spacing(6),

    [theme.breakpoints.down('sm')]: {
      fontSize: 24,
      lineHeight: '32px',
    },
  },
  text: {
    display: 'block',
    maxWidth: 700,
    margin: '0 auto',
    fontWeight: 500,
    fontSize: 18,

    [theme.breakpoints.down('md')]: {
      whiteSpace: 'initial',
    },

    [theme.breakpoints.down('sm')]: {
      fontSize: 16,
    },
  },
  link: {
    color: theme.palette.primary.main,
    cursor: 'pointer',
  },
}));

const NoWorries = (props: NoWorriesProps) => {
  const classes = useStyles();

  return (
    <Section
      overline={
        <SectionOverline color="primary" index={2}>
          dedicated search
        </SectionOverline>
      }
      color="grey"
    >
      <Typography
        className={classes.topTitle}
        align="center"
        variant="h6"
        component="i"
        transform="uppercase"
        fontWeight={600}
        whiteSpace="break-spaces"
        paragraph
        color="secondary.contrastText"
      >
        Can’t find the right candidate in your hub?
      </Typography>
      <Typography
        className={classes.midTitle}
        align="center"
        variant="h4"
        component="i"
        transform="uppercase"
        fontWeight={600}
        whiteSpace="break-spaces"
        paragraph
        color="secondary.contrastText"
      >
        Don’t worry!
      </Typography>
      <Typography
        className={classes.text}
        align="center"
        variant="body1"
        whiteSpace="break-spaces"
        paragraph
        color="secondary.contrastText"
      >
        {`Try our Community-powered search solution
to close ‘hard-to-fill’ roles faster. `}
        <Link
          className={classes.link}
          href={paths.communitySearch}
        >{`Learn more >>>`}</Link>
      </Typography>
    </Section>
  );
};

export default NoWorries;
