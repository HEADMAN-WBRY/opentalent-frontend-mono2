import React from 'react';

import { Box } from '@mui/material';

import Typography from '@libs/ui/components/typography';

interface SlideContentProps {
  image: string;
  text: string;
  name: string;
  role: string;
}

export const SlideContent = ({
  image,
  text,
  name,
  role,
}: SlideContentProps) => {
  return (
    <div>
      <Box display="flex" justifyContent="center" mb={12}>
        <img
          srcSet={`${image} 2x`}
          width={100}
          height={100}
          style={{ borderRadius: '100%' }}
        />
      </Box>

      <Box maxWidth={1024} margin="0 auto" pb={8}>
        <Typography textAlign="center" variant="h5">
          &quot;{text}&quot;
        </Typography>
      </Box>

      <Box margin="0 auto" pb={2}>
        <Typography textAlign="center" variant="body2">
          - {name}
        </Typography>
      </Box>

      <Box margin="0 auto">
        <Typography
          component="i"
          display="block"
          textAlign="center"
          variant="caption"
        >
          {role}
        </Typography>
      </Box>
    </div>
  );
};
