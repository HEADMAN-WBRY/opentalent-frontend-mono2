import cn from 'classnames';
import React, { useState } from 'react';

import ArrowBackRoundedIcon from '@mui/icons-material/ArrowBackRounded';
import ArrowForwardRoundedIcon from '@mui/icons-material/ArrowForwardRounded';
import { Collapse, Grid, IconButton, Slide } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { getNextIndex, getPreviousIndex } from '@libs/helpers/common';

import Section from '../../../components/new-landings/section';
import { SlideContent } from './SlideContent';
import { ITEMS } from './consts';

interface SliderProps { }

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 1050,
    margin: '0 auto',
  },
  stepper: {
    height: 32,
    margin: '32px 0 24px',
    width: '100%',

    '& i': {
      position: 'relative',
      display: 'block',
      border: `2px solid ${theme.palette.secondary.main}`,
      width: 12,
      height: 12,
      borderRadius: '100%',

      '&::before': {
        position: 'absolute',
        content: "''",
        background: theme.palette.secondary.main,
        top: '50%',
        left: '50%',
        width: 0,
        height: 0,
        display: 'block',
        transform: 'translate(-50%, -50%)',
        transition: 'all .3s',
        borderRadius: '100%',
      },

      '&$activeStep': {
        '&::before': {
          width: 12,
          height: 12,
        },
      },
    },
  },
  activeStep: {},
  arrowButton: {
    border: '1px solid black',
  },
}));

const Slider = (props: SliderProps) => {
  const classes = useStyles();
  const [activeIndex = 0, setActiveIndex] = useState<number>();

  return (
    <Section
      // overline={
      //   <SectionOverline color="secondary" index={0}>
      //     the platform
      //   </SectionOverline>
      // }
      classes={{ root: classes.root }}
      color="white"
    >
      {ITEMS.map((i, index) => (
        <Collapse unmountOnExit key={i.image} in={index === activeIndex}>
          <div>
            <SlideContent {...i} />
          </div>
        </Collapse>
      ))}

      <Grid
        alignItems="center"
        justifyContent="center"
        spacing={4}
        className={classes.stepper}
        container
      >
        <Grid item>
          <IconButton
            aria-label="delete"
            className={classes.arrowButton}
            size="small"
            onClick={() => {
              setActiveIndex(getNextIndex(activeIndex, ITEMS.length));
            }}
          >
            <ArrowBackRoundedIcon fontSize="small" />
          </IconButton>
        </Grid>

        {ITEMS.map((item, index) => (
          <Grid onClick={() => setActiveIndex(index)} key={item.image} item>
            <i
              className={cn({ [classes.activeStep]: index === activeIndex })}
            />
          </Grid>
        ))}

        <Grid item>
          <IconButton
            aria-label="delete"
            className={classes.arrowButton}
            size="small"
            onClick={() => {
              setActiveIndex(getPreviousIndex(activeIndex, ITEMS.length));
            }}
          >
            <ArrowForwardRoundedIcon fontSize="small" />
          </IconButton>
        </Grid>
      </Grid>
    </Section>
  );
};

export default Slider;
