export const ITEMS = [
  {
    image: '/hub-page/quote/adriana@2x.jpg',
    text: 'This community-powered tool has transformed my recruiting game, delivering blazing-fast search results and unmatched efficiency, helping me close deals like never before',
    name: 'Arianna',
    role: 'Independent recruiter, ex Amazon.',
  },
  {
    image: '/hub-page/quote/brian@2x.jpg',
    text: "I've witnessed the true potential of my business unfold with the aid of this powerful tool and its comprehensive platform, equipping me with the tools to achieve remarkable feats as a recruiter.",
    name: 'Brian',
    role: 'Independent recruiter, ex BCG.',
  },
];
