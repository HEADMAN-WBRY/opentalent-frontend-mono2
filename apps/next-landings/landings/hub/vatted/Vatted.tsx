import React from 'react';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { useMediaQueries } from '@libs/helpers/hooks/media-queries';
import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import Section from '../../../components/new-landings/section';
import SectionTitle from '../../../components/new-landings/section-title';
import { paths } from '../../../utils/consts';
import { ReactComponent as TickIcon } from './assets/tick.svg';
import BulletList from './bullet-list';

interface VattedProps { }

const useStyles = makeStyles((theme) => ({
  title: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textBox: {
    '& p': {
      lineHeight: '32px',
      letterSpacing: '0.15px',

      [theme.breakpoints.down('md')]: {
        fontSize: 14,
        lineHeight: '24px',
        display: 'inline',
        textAlign: 'center',
      },
    },
  },
  withButtonText: {
    [theme.breakpoints.down('md')]: {
      textAlign: 'center',
    },
  },
  listBox: {
    paddingTop: theme.spacing(14),

    [theme.breakpoints.down('md')]: {
      paddingTop: theme.spacing(10),
    },
  },
}));

const Vatted = (props: VattedProps) => {
  const { isSM } = useMediaQueries();
  const classes = useStyles();

  return (
    <Section color="white">
      <SectionTitle className={classes.title}>
        <TickIcon />
        &nbsp; hire with confidence
      </SectionTitle>

      <div className={classes.textBox}>
        <Typography textAlign="center">
          At OpenTalent, we seek to build one of Europe’s most trusted talent
          communities and directories.
        </Typography>
        <Typography textAlign="center">
          That’s why every new member undergoes robust vetting, so our clients
          can hire with confidence.
        </Typography>
      </div>

      <Box className={classes.listBox}>
        <BulletList />
      </Box>

      {/* <Box pt={isSM ? 4 : 14}> */}
      {/*   <Grid */}
      {/*     justifyContent="center" */}
      {/*     direction={isSM ? 'column' : 'row'} */}
      {/*     spacing={isSM ? 6 : 8} */}
      {/*     container */}
      {/*   > */}
      {/*     <Grid item> */}
      {/*       <Button */}
      {/*         variant="contained" */}
      {/*         color="secondary" */}
      {/*         fullWidth */}
      {/*         type="submit" */}
      {/*         size="large" */}
      {/*         href={paths.companyOldOnboardingForm} */}
      {/*       > */}
      {/*         <b>Start My Hub</b> - it’s FREE */}
      {/*       </Button> */}
      {/*     </Grid> */}
      {/*     <Grid className={classes.withButtonText} item> */}
      {/*       <Typography variant="subtitle2" fontWeight={700}> */}
      {/*         Claim your company’s hub - */}
      {/*       </Typography> */}
      {/*       <Typography variant="subtitle2"> */}
      {/*         <span style={{ textDecoration: 'underline' }}>15.000+ hubs</span>{' '} */}
      {/*         up for grabs */}
      {/*       </Typography> */}
      {/*     </Grid> */}
      {/*   </Grid> */}
      {/* </Box> */}
    </Section>
  );
};

export default Vatted;
