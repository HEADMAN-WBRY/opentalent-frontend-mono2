import React from 'react';

import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import BilletListItem from './BilletListItem';
import { DISABLED_ITEMS, ACTIVE_ITEMS } from './consts';

interface BulletListProps { }

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    [theme.breakpoints.down('md')]: {
      flexDirection: 'column',
    },

    '& > div:first-child': {
      paddingRight: theme.spacing(20),

      [theme.breakpoints.down('md')]: {
        paddingRight: 0,
      },
    },
  },
}));

const BulletList = (props: BulletListProps) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Box>
        {ACTIVE_ITEMS.map((i) => (
          <Box key={i.title} pb={4}>
            <BilletListItem {...i} />
          </Box>
        ))}
      </Box>
      <Box>
        {DISABLED_ITEMS.map((i, index) => (
          <Box key={i.title} pb={index === DISABLED_ITEMS.length - 1 ? 0 : 4}>
            <BilletListItem {...i} isDisabled />
          </Box>
        ))}
      </Box>
    </div>
  );
};

export default BulletList;
