export const ACTIVE_ITEMS = [
  { title: 'Location check', subtitle: 'In-platform + team check' },
  { title: 'CV / Resume check', subtitle: 'In-platform + team check' },
  { title: 'LinkedIn check', subtitle: 'In-platform + team check' },
  { title: 'Availability check', subtitle: 'In-platform + team check' },
];

export const DISABLED_ITEMS = [
  { title: 'Skills verification', subtitle: 'Coming soon' },
  { title: 'Employer verification', subtitle: 'Coming soon' },
  { title: 'Background checks', subtitle: 'Coming soon' },
  { title: 'Education verification', subtitle: 'Coming soon' },
];
