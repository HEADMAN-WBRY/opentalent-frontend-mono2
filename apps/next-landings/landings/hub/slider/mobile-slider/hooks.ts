import React, { useState } from 'react';
import { useSlider } from 'react-use';

import {
  getNextIndex,
  getPreviousIndex,
} from '@libs/helpers/common/moveToIndex';

import { ITEMS } from './../consts';

const roundValue = (n: number) => Math.ceil(n * 100);
const THRESHOLD = 40;

export const useMobileSlide = ({
  activeSlide,
  wrappedChangeSlide,
}: {
  activeSlide: number;
  wrappedChangeSlide: (n: number) => void;
}) => {
  const ref = React.useRef(null);
  const [nextIndex, setNextIndex] = useState<null | number>(null);

  const { current: state } = React.useRef<{
    captured: boolean;
    pressed: boolean;
    start: null | number;
  }>({
    captured: false,
    pressed: false,
    start: null,
  });

  React.useEffect(() => {
    if (nextIndex !== null) {
      const final =
        nextIndex === 1
          ? getPreviousIndex(activeSlide, ITEMS.length)
          : getNextIndex(activeSlide, ITEMS.length);

      if (activeSlide !== final) {
        wrappedChangeSlide(final);
        setNextIndex(null);
      }
    }
  }, [activeSlide, nextIndex, wrappedChangeSlide]);

  const { isSliding } = useSlider(ref, {
    onScrub: (value: number) => {
      if (state.pressed && !state.captured) {
        state.captured = true;
        state.start = value;
      }
    },
    onScrubStart: () => {
      state.pressed = true;
    },
    onScrubStop: (value: number) => {
      if (state.start === null) {
        return;
      }

      const startValue = roundValue(state.start);
      const currentValue = roundValue(value);
      const isForward = startValue < currentValue;
      const needToSwitch = THRESHOLD <= Math.abs(startValue - currentValue);

      if (needToSwitch) {
        setNextIndex(null);
        setNextIndex(isForward ? 1 : -1);
      }

      state.pressed = false;
      state.captured = false;
      state.start = null;
    },
  });

  return { ref, isSliding };
};
