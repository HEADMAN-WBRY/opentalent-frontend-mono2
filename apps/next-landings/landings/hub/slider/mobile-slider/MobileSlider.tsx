import cn from 'classnames';
import React, { useState } from 'react';

import { Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import {
  getNextIndex,
  getPreviousIndex,
} from '@libs/helpers/common/moveToIndex';
import CircleStepper from '@libs/ui/components/circle-stepper';

import { useAutoChangeSlider } from '../../../../components/new-landings/slider/hooks';
import { ReactComponent as ArrowIcon } from '../../assets/slider_arrow.svg';
import { ITEMS } from '../consts';
import SlideItem from './SlideItem';
import { useMobileSlide } from './hooks';

interface SliderProps {
  activeIndex: number;
  items: any[];
  setActiveIndex: (index: number) => void;
}

const useStyles = makeStyles((theme) => ({
  content: {
    minHeight: 264,
    display: 'flex',
    alignItems: 'center',
    marginBottom: theme.spacing(8),
    position: 'relative',

    [theme.breakpoints.down('md')]: {
      marginBottom: theme.spacing(8),
    },
  },

  buttonWrapper: {
    display: 'flex',
    justifyContent: 'center',
    width: '100%',
    marginTop: theme.spacing(1),

    '& > a': {
      [theme.breakpoints.down('md')]: {
        width: '100%',
      },
    },
  },

  '@keyframes zoomIn': {
    from: {
      opacity: 0,
      transform: 'scale3d(0.3, 0.3, 0.3)',
    },

    to: {
      opacity: 1,
    },
  },

  slide: {
    position: 'absolute',
    left: -1000000,
    visibility: 'hidden',
  },
  slideActive: {
    position: 'static',
    visibility: 'visible',
    animation: '$zoomIn 0.5s ease-in-out',
  },

  arrow: {
    cursor: 'pointer',
  },
  arrowRight: {
    transform: 'rotate(180deg)',
  },
}));

const MobileSlider = (props: SliderProps) => {
  const classes = useStyles();
  const [activeSlide, setActiveSlide] = useState(0);
  const { wrappedChangeSlide } = useAutoChangeSlider({
    currentSlide: activeSlide,
    setSlide: setActiveSlide,
    slidesCount: ITEMS.length,
  });
  const selectPrev = () =>
    wrappedChangeSlide(getPreviousIndex(activeSlide, ITEMS.length));
  const selectNext = () =>
    wrappedChangeSlide(getNextIndex(activeSlide, ITEMS.length));
  const { ref } = useMobileSlide({
    activeSlide,
    wrappedChangeSlide,
  });

  return (
    <>
      <div ref={ref} className={classes.content}>
        {ITEMS.map((item, index) => (
          <SlideItem
            // eslint-disable-next-line react/no-array-index-key
            key={item.title}
            {...item}
            className={cn(classes.slide, {
              [classes.slideActive]: index === activeSlide,
            })}
          />
        ))}
      </div>

      <Grid
        spacing={6}
        wrap="nowrap"
        justifyContent="center"
        alignItems="center"
        container
      >
        <Grid item>
          <ArrowIcon onClick={selectPrev} className={classes.arrow} />
        </Grid>
        <Grid item>
          <CircleStepper
            steps={ITEMS.length}
            activeStep={activeSlide}
            onStepChange={wrappedChangeSlide}
            color="secondary"
          />
        </Grid>
        <Grid item>
          <ArrowIcon
            onClick={selectNext}
            className={cn(classes.arrowRight, classes.arrow)}
          />
        </Grid>
      </Grid>
    </>
  );
};

export default MobileSlider;
