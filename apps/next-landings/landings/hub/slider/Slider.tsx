import React, { useState } from 'react';

import { Hidden } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import Section from '../../../components/new-landings/section';
import SectionTitle from '../../../components/new-landings/section-title';
import SectionOverline from '../shared/SectionOverline';
import { ITEMS } from './consts';
import DesktopSlider from './desktop-slider';
import MobileSlider from './mobile-slider';

interface SliderProps { }

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 1050,
    margin: '0 auto',
  },
  title: {
    // maxWidth: 470,
    margin: '0 auto',
    paddingBottom: theme.spacing(6),
  },
  subtitle: {
    fontSize: 20,
    textTransform: 'uppercase',

    [theme.breakpoints.down('md')]: {
      fontSize: 16,
    },

    [theme.breakpoints.down('sm')]: {
      whiteSpace: 'initial',
    },
  },
}));

const Slider = (props: SliderProps) => {
  const classes = useStyles();
  const [activeIndex = 0, setActiveIndex] = useState<number>();

  return (
    <Section
      // overline={
      //   <SectionOverline color="secondary" index={0}>
      //     the platform
      //   </SectionOverline>
      // }
      classes={{ root: classes.root }}
      color="white"
    >
      <SectionTitle className={classes.title}>
        {`We have your back.`}
      </SectionTitle>

      <Typography
        className={classes.subtitle}
        textAlign="center"
        whiteSpace="break-spaces"
      >
        {`When you recruit with opentalent, you’ll find tools and services to grow your
business. expect smarter search, instant matches and new insights.`}
      </Typography>

      <Hidden mdDown>
        <DesktopSlider
          activeIndex={activeIndex}
          setActiveIndex={setActiveIndex}
          items={ITEMS}
        />
      </Hidden>
      <Hidden mdUp>
        <MobileSlider
          activeIndex={activeIndex}
          setActiveIndex={setActiveIndex}
          items={ITEMS}
        />
      </Hidden>
    </Section>
  );
};

export default Slider;
