export const ITEMS = [
  {
    img: '/hub-page/slides/slide-1@2x.png',
    title: 'Secure verification flows',
    subtitle: 'Build a hub of trusted flexible talent',
  },
  {
    img: '/hub-page/slides/slide-2@2x.png',
    title: 'Skilled-based matching',
    subtitle: 'Find candidates with the right skills',
  },
  {
    img: '/hub-page/slides/slide-3@2x.png',
    title: 'Smart ATS',
    subtitle: 'Easily track all candidates and jobs',
  },
  {
    img: '/hub-page/slides/slide-4@2x.png',
    title: 'Direct communication',
    subtitle: 'Reach out to anyone, hassle-free',
  },
  {
    img: '/hub-page/slides/slide-5@2x.png',
    title: 'Referral-based growth',
    subtitle: 'Issue Finder Fees to attract new talent',
  },
  // {
  //   img: '/hub-page/slides/slide-6@2x.png',
  //   title: 'Customised reporting',
  //   subtitle: 'Download relevant data on your hub',
  // },
];
