import { useGetCompaniesLandingDataQuery } from '@libs/graphql-types';

export const useAppInfo = () => {
  const { loading, data } = useGetCompaniesLandingDataQuery();

  return { isLoading: loading, data };
};
