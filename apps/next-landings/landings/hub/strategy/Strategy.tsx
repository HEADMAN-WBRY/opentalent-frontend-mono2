import Image from 'next/image';
import React from 'react';

import { Box, Button, Grid, Hidden } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { EXTERNAL_LINKS } from '@libs/helpers/consts';
import Typography from '@libs/ui/components/typography';

import Section from '../../../components/new-landings/section';

interface StrategyProps {}

const useStyles = makeStyles((theme) => ({
  root: {
    padding: 0,
  },
  title: {
    display: 'block',
    marginBottom: theme.spacing(12),
    maxWidth: 500,
    fontStyle: 'italic',

    [theme.breakpoints.down('md')]: {
      fontSize: 20,
      lineHeight: '21px',
      marginBottom: theme.spacing(8),
      textAlign: 'center',
      maxWidth: '100%',
    },
  },
  container: {
    height: 700,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: `0 ${theme.spacing(5)}`,

    [theme.breakpoints.down('md')]: {
      height: 'auto',
    },
  },
  content: {
    justifyContent: 'space-between',
    alignItems: 'center',

    [theme.breakpoints.down('md')]: {
      paddingTop: '48px',
      paddingBottom: 32,
      flexDirection: 'column',
    },

    [theme.breakpoints.down('sm')]: {
      backgroundSize: '70%',
    },
  },
  info: {
    maxWidth: 440,
    paddingBottom: theme.spacing(14),

    [theme.breakpoints.down('md')]: {
      paddingBottom: theme.spacing(8),
      maxWidth: '100%',
    },
  },
  abnLogo: {
    [theme.breakpoints.down('md')]: {
      textAlign: 'center',
    },
  },
  buttonWrapper: {
    '& > a': {
      fontWeight: 'bold',
    },

    [theme.breakpoints.down('md')]: {
      width: '100%',
      paddingBottom: 48,

      '& > a': {
        width: '100%',
        fontWeight: 'bold',
      },
    },
  },
  imageSection: {
    [theme.breakpoints.down('md')]: {
      textAlign: 'center',
    },

    '& > img': {
      maxWidth: '100%',
    },
  },
}));

const Strategy = (props: StrategyProps) => {
  const classes = useStyles();

  return (
    <Section
      color="white"
      classes={{ root: classes.root, container: classes.container }}
    >
      <Grid wrap="nowrap" className={classes.content} container>
        <Grid item>
          <Box className={classes.abnLogo} pb={8}>
            <Image
              width={140}
              height={60}
              style={{ objectFit: 'contain' }}
              src="/hub-page/strategy/abn_logo.png"
              alt="ABN"
            />
          </Box>
          <Typography
            className={classes.title}
            fontWeight={600}
            color="inherit"
            variant="h4"
            whiteSpace="break-spaces"
          >
            {`Optimising the
FREELANCER sourcing
strategy.`.toUpperCase()}
          </Typography>

          <Typography variant="subtitle1" className={classes.info}>
            Since 2021, OpenTalent and ABN AMRO have pioneered a new way of
            freelancer sourcing by adding in-house talent pooling and ‘remote’
            hiring to the mix. Download the case for details.
          </Typography>

          <Hidden mdDown>
            <Box className={classes.buttonWrapper}>
              <Button
                href={EXTERNAL_LINKS.ABNCase}
                variant="contained"
                size="large"
                target="_blank"
              >
                DOWNLOAD CASE
              </Button>
            </Box>
          </Hidden>
        </Grid>
        <Grid className={classes.imageSection} item>
          <Image
            width={600}
            height={600}
            style={{ objectFit: 'contain' }}
            src="/hub-page/strategy/strategy@2x.png"
            alt="Cases"
          />
        </Grid>
      </Grid>

      <Hidden mdUp>
        <Box className={classes.buttonWrapper}>
          <Button
            href={EXTERNAL_LINKS.ABNCase}
            variant="contained"
            size="large"
            target="_blank"
          >
            DOWNLOAD CASE
          </Button>
        </Box>
      </Hidden>
    </Section>
  );
};

export default Strategy;
