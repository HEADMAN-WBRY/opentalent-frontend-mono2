import {
  LANDINGS_EXTERNAL_LINKS,
  paths,
} from 'apps/next-landings/utils/consts';
import React from 'react';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { useMediaQueries } from '@libs/helpers/hooks/media-queries';
import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import Section from '../../../components/new-landings/section';
import SectionTitle from '../../../components/new-landings/section-title';
import { getValues } from './consts';

interface ValuesProps {
  talentsCount?: number;
}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    background: '#0F0F0D',
    color: 'white',
    paddingTop: 100,
    paddingBottom: 90,
    overflow: 'hidden',

    [theme.breakpoints.down('md')]: {
      paddingTop: 48,
      paddingBottom: 48,
    },
  },
  container: {
    maxWidth: 1024,
    overflow: 'hidden',
  },
  title: {
    maxWidth: 660,
    textAlign: 'center',
    margin: '0 auto',
    paddingBottom: theme.spacing(20),

    [theme.breakpoints.down('md')]: {
      paddingBottom: theme.spacing(8),
    },

    '& > i': {
      [theme.breakpoints.down('md')]: {
        fontSize: 20,
        lineHeight: '25px',
      },
    },
  },

  valueContent: {
    maxWidth: 308,

    [theme.breakpoints.down('md')]: {
      maxWidth: '100%',
    },
  },

  values: {
    maxWidth: 900,
    margin: '-40px auto 0',

    [theme.breakpoints.down('md')]: {
      justifyContent: 'center',
      margin: '-24px 0 0 -24px',
    },
  },
  valueItem: {
    [theme.breakpoints.down('md')]: {
      width: '100%',
    },

    '& p': {
      lineHeight: '28px',

      [theme.breakpoints.down('md')]: {
        lineHeight: '22px',
        whiteSpace: 'inherit',
      },
    },

    '& svg': {
      [theme.breakpoints.down('sm')]: {
        width: 42,
      },
    },
  },
  valueTitle: {
    display: 'block',
    fontSize: 20,
    lineHeight: '28px',

    [theme.breakpoints.down('sm')]: {
      fontSize: 18,
      lineHeight: '22px',
    },
  },
  iconWrap: {
    color: theme.palette.primary.main,
  },
  primaryText: {
    color: theme.palette.primary.main,
  },
  sectionTitle: {
    display: 'block !important',
  },
}));

const Values = ({ talentsCount = 0 }: ValuesProps) => {
  const classes = useStyles();
  const { isSM } = useMediaQueries();

  return (
    <Section classes={{ root: classes.wrapper, container: classes.container }}>
      <Box className={classes.title}>
        <SectionTitle className={classes.sectionTitle}>
          benefits of opentalent&nbsp;direct
        </SectionTitle>
      </Box>

      <Grid
        className={classes.values}
        container
        justifyContent="center"
        spacing={isSM ? 6 : 10}
      >
        {getValues().map(({ title, text, Icon }) => (
          <Grid xs={isSM ? 11 : 6} key={title} item>
            <Grid
              className={classes.valueItem}
              spacing={isSM ? 4 : 6}
              wrap="nowrap"
              container
            >
              <Grid className={classes.iconWrap} item>
                <Icon />
              </Grid>
              <Grid item>
                <Typography
                  whiteSpace="break-spaces"
                  className={classes.valueContent}
                  variant="body2"
                >
                  <Typography
                    className={classes.valueTitle}
                    component="span"
                    variant="body1"
                    fontWeight={600}
                  >
                    {title}
                  </Typography>
                  {text}
                </Typography>
              </Grid>
            </Grid>
          </Grid>
        ))}
      </Grid>
      <Box mt={10}>
        <Grid container spacing={4} justifyContent="center">
          <Grid item>
            <Button
              color="primary"
              variant="contained"
              href={LANDINGS_EXTERNAL_LINKS.companyOnboardingTypeform}
              size="large"
              fullWidth
              style={{ width: isSM ? '100%' : 310 }}
            >
              <b>Start my FREE trial</b>
            </Button>
          </Grid>
        </Grid>
      </Box>
    </Section>
  );
};

export default Values;
