import { ReactComponent as CloudIcon } from '../../../assets/values/cloud.svg';
import { ReactComponent as GlobeIcon } from '../../../assets/values/globe.svg';
import { ReactComponent as DiversityIcon } from '../assets/values/diversity.svg';
import { ReactComponent as FastIcon } from '../assets/values/fast.svg';
import { ReactComponent as PayIcon } from '../assets/values/pay.svg';
import { ReactComponent as ProfileIcon } from '../assets/values/profile.svg';
import { ReactComponent as RecordIcon } from '../assets/values/record.svg';
import { ReactComponent as VerificationIcon } from '../assets/values/verification.svg';

export const getValues = () => [
  {
    title: 'Quality assurance',
    text: `Rigorous vetting process
for peace of mind.`,
    Icon: VerificationIcon,
  },
  {
    title: 'Cost savings',
    text: `Escape hefty middlemen
fees with OpenTalent Direct.`,
    Icon: PayIcon,
  },
  {
    title: 'Direct access',
    text: `Reach curated
professionals, directly.`,
    Icon: RecordIcon,
  },

  {
    title: 'Time efficiency',
    text: `We instantly match you
with the skills your need.`,
    Icon: FastIcon,
  },

  {
    title: 'Wide talent reach',
    text: `Access an ever-evolving pool
of Europe-based talent.`,
    Icon: GlobeIcon,
  },

  {
    title: 'Enhanced control',
    text: `Everything in one place to
control the process.`,
    Icon: ProfileIcon,
  },

  {
    title: 'Collaborative tech',
    text: `Build-in tooling for
seamless collaboration.`,
    Icon: DiversityIcon,
  },

  {
    title: 'Scalability',
    text: `Cloud-based scalability
for evolving hiring needs.`,
    Icon: CloudIcon,
  },
];
