export const ITEMS = [
  {
    title: 'Salesforce Developer',
    image: '/hub-page/intro/salesforce@2x.png',
    name: 'Erica',
  },
  {
    title: 'DevOps Engineer',
    image: '/hub-page/intro/devops@2x.png',
    name: 'Jerome',
  },
  {
    title: 'Mulesoft Engineer',
    image: '/hub-page/intro/mulesoft@2x.png',
    name: 'Anna',
  },
];
