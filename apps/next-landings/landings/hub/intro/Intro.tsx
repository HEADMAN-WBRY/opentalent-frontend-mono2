/* eslint-disable jsx-a11y/accessible-emoji */
import {
  INFINITY_SIGN,
  LANDINGS_EXTERNAL_LINKS,
  paths,
} from 'apps/next-landings/utils/consts';
import React, { useState } from 'react';
import { useInterval } from 'react-use';

import { Box, Button, Container, Grid, Hidden } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { getNextIndex } from '@libs/helpers/common';
import { formatNumberSafe } from '@libs/helpers/format';
import { useMediaQueries } from '@libs/helpers/hooks/media-queries';
import Typography from '@libs/ui/components/typography';

import LogosLine from './LogosLineSlider';
import { ITEMS } from './consts';

interface IntroProps {
  candidatesCount?: number;
}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    background: theme.palette.secondary.dark,

    [theme.breakpoints.down('md')]: {
      paddingTop: theme.spacing(10),
      paddingBottom: theme.spacing(6),
    },
  },
  container: {
    height: 750,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: `0 ${theme.spacing(5)}`,
    background: ({ image }: any) =>
      `url(/hub-page/intro/recruiter@2x.png) no-repeat right 12px bottom -30px`,
    backgroundSize: '50% !important',

    [theme.breakpoints.down('lg')]: {
      background: ({ image }: any) =>
        `url(/hub-page/intro/recruiter@2x.png) no-repeat right 12px bottom -130px`,
      height: 500,
      backgroundSize: '45% !important',
    },

    [theme.breakpoints.down('md')]: {
      height: 'auto',
      background: 'transparent !important',
    },
  },
  textSection: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  content: {
    minHeight: 400,

    [theme.breakpoints.down('md')]: {
      background: 'none !important',
      minHeight: 'auto',
    },
  },
  button: {
    height: 48,
    minWidth: 270,
  },
  logosContainer: {
    marginTop: theme.spacing(20),

    [theme.breakpoints.down('md')]: {
      marginTop: 0,
    },
  },

  firstTitle: {
    maxWidth: 520,
    whiteSpace: 'break-spaces',
    paddingBottom: theme.spacing(3),
    fontSize: 48,
    lineHeight: '56px',
    marginBottom: theme.spacing(7),

    [theme.breakpoints.down('md')]: {
      fontSize: 36,
      lineHeight: '42px',
      paddingBottom: theme.spacing(0),
    },
  },
  secondTitle: {
    whiteSpace: 'nowrap',
    maxWidth: '100%',

    [theme.breakpoints.down('md')]: {
      fontSize: 28,
      lineHeight: '40px',
    },
  },

  mobileImage: {
    paddingTop: theme.spacing(10),
    marginBottom: theme.spacing(5),
    textAlign: 'center',

    [theme.breakpoints.down('md')]: {
      marginBottom: 0,
    },

    '& > img': {
      maxWidth: '100%',
    },
  },

  smallTitle: {
    fontSize: 10,
    textTransform: 'uppercase',
    marginBottom: theme.spacing(4),
    color: 'white',
    fontWeight: 'bold',
  },
}));

const Intro = ({ candidatesCount }: IntroProps) => {
  const { isSM } = useMediaQueries();
  const [count, setCount] = useState(0);
  const image = ITEMS[count].image;
  const title = ITEMS[count].title;
  const talentName = ITEMS[count].name;
  const classes = useStyles({ image });
  const formattedCandidatesCount = formatNumberSafe(candidatesCount, {
    fallback: INFINITY_SIGN,
  });

  useInterval(() => {
    setCount(getNextIndex(count, ITEMS.length));
  }, 5000);

  return (
    <div className={classes.wrapper}>
      {ITEMS.map((i) => (
        <link rel="preload" as="image" key={i.image} href={i.image} />
      ))}

      <Container className={classes.container}>
        <Grid className={classes.content} container>
          <Grid className={classes.textSection} sm={isSM ? 12 : 7} item>
            <Typography className={classes.smallTitle}>
              OPENTALENT direct 👋
            </Typography>

            <Typography
              fontWeight={700}
              fontStyle="italic"
              color="secondary.contrastText"
              variant="h4"
              className={classes.firstTitle}
            >{`Dominate the game,
recruit like a pro.`}</Typography>

            {/* <Grow
              timeout={{
                enter: 1000,
                exit: 3000,
              }}
              in
              key={title}
            >
              <div>
                <Typography
                  fontWeight={700}
                  fontStyle="italic"
                  color="primary.main"
                  variant="h3"
                  paragraph
                  className={classes.secondTitle}
                >
                  {title}
                </Typography>
              </div>
            </Grow> */}

            <Typography
              maxWidth={534}
              variant="body1"
              color="secondary.contrastText"
            >
              Welcome to the system built for recruiters across Europe to
              supercharge their business. Elevate your success, grow your
              earnings effortlessly - and thrive together with us.
            </Typography>

            <Box pt={10}>
              <Grid direction={isSM ? 'column' : 'row'} spacing={4} container>
                <Grid item>
                  <Button
                    variant="contained"
                    color="primary"
                    fullWidth
                    type="submit"
                    size="large"
                    className={classes.button}
                    style={{ fontWeight: 'bold' }}
                    href={paths.companyOnboardingV3}
                  >
                    Get started now
                  </Button>
                </Grid>
                {/* <Grid item> */}
                {/*   <Button */}
                {/*     variant="outlined" */}
                {/*     color="primary" */}
                {/*     fullWidth */}
                {/*     type="submit" */}
                {/*     size="large" */}
                {/*     className={classes.button} */}
                {/*     href={EXTERNAL_LINKS.pieterLink} */}
                {/*     style={{ fontWeight: 'bold' }} */}
                {/*   > */}
                {/*     Request a Demo */}
                {/*   </Button> */}
                {/* </Grid> */}
              </Grid>
            </Box>
          </Grid>
        </Grid>

        {/* <Hidden mdUp> */}
        {/*   <Box className={classes.mobileImage}> */}
        {/*     <img srcSet={`${image} 2x`} alt="Cards" /> */}
        {/*   </Box> */}
        {/* </Hidden> */}

        {/* <Box className={classes.logosContainer}> */}
        {/*   <LogosLine /> */}
        {/* </Box> */}
      </Container>
    </div>
  );
};

export default Intro;
