import SolutionsMenu from 'apps/next-landings/components/solutions-menu';
import { ReactComponent as WhatsAppIcon } from 'apps/next-landings/public/header/whatsapp.svg';
import Link from 'next/link';
import React, { useState } from 'react';

import MenuIcon from '@mui/icons-material/Menu';
import {
  AppBar,
  Box,
  Button,
  Container,
  Grid,
  Hidden,
  IconButton,
  Toolbar,
} from '@mui/material';
import { makeStyles } from '@mui/styles';

import { EXTERNAL_LINKS } from '@libs/helpers/consts';
import { useMediaQueries } from '@libs/helpers/hooks/media-queries';

import { ReactComponent as LogoIcon } from '../../../assets/opentalent_dark.svg';
import { paths } from '../../../utils/consts';
import NewLandingsPageDrawer from './NewLandingsPageDrawer';

interface HeaderProps {
  isForTalent?: boolean;
}

const useStyles = makeStyles((theme) => ({
  bar: {
    background: theme.palette.secondary.dark,
    paddingTop: theme.spacing(6),
  },
  replaceBtn: {
    transition: `color ${theme.transitions.duration.short}s ${theme.transitions.easing.easeInOut}`,

    '&:hover': {
      color: theme.palette.primary.main,
    },
  },
  logoWrapper: {
    display: 'flex',
    alignItems: 'center',
    color: theme.palette.primary.main,
    paddingLeft: theme.spacing(2),

    [theme.breakpoints.down('sm')]: {
      marginLeft: -12,
      paddingLeft: 0,
    },
  },
}));

const Header: React.FC<HeaderProps> = ({ isForTalent }) => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen((s) => !s);
  const { isXS } = useMediaQueries();
  const classes = useStyles();

  return (
    <>
      <NewLandingsPageDrawer toggle={toggle} isOpen={isOpen} />
      <AppBar className={classes.bar} color="secondary" position="static">
        <Box>
          <Container>
            <Toolbar disableGutters>
              <Grid
                justifyContent="space-between"
                alignItems="center"
                container
              >
                <Grid item>
                  <Grid container>
                    <Grid display="flex" style={{ alignItems: 'center' }} item>
                      <Hidden mdUp>
                        <IconButton
                          size="small"
                          onClick={toggle}
                          style={{ color: 'white' }}
                        >
                          <MenuIcon />
                        </IconButton>
                      </Hidden>
                    </Grid>
                    <Grid
                      className={classes.logoWrapper}
                      item
                      component={Box}
                      mb={-2}
                    >
                      <Link href="/">
                        <LogoIcon height={isXS ? 35 : 54} />
                      </Link>
                    </Grid>
                  </Grid>
                </Grid>
                <Hidden mdDown>
                  <Grid item>
                    <Grid spacing={4} container>
                      <Grid item>
                        <Button
                          color="inherit"
                          component={Link}
                          className={classes.replaceBtn}
                          href={paths.talentLanding}
                        >
                          COMMUNITY
                        </Button>
                      </Grid>
                      <Grid item>
                        <SolutionsMenu />
                      </Grid>
                      <Grid item>
                        <Button
                          color="inherit"
                          className={classes.replaceBtn}
                          href={EXTERNAL_LINKS.whatsAppForCompanies}
                          endIcon={<WhatsAppIcon />}
                        >
                          CONTACT US
                        </Button>
                      </Grid>
                      <Grid item>
                        <Button
                          href={paths.mainAppRoute}
                          variant="outlined"
                          color="inherit"
                        >
                          Sign in
                        </Button>
                      </Grid>
                    </Grid>
                  </Grid>
                </Hidden>
              </Grid>
            </Toolbar>
          </Container>
        </Box>
      </AppBar>
    </>
  );
};

export default Header;
