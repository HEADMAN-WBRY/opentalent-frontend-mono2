import React from 'react';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { formatNumberSafe } from '@libs/helpers/format';
import { useMediaQueries } from '@libs/helpers/hooks/media-queries';
import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import Section from '../../../components/new-landings/section';
import SectionTitle from '../../../components/new-landings/section-title';
import { INFINITY_SIGN, paths } from '../../../utils/consts';

interface ActionProps {
  candidatesCount?: number;
}

const useStyles = makeStyles((theme) => ({
  withButtonText: {
    [theme.breakpoints.down('md')]: {
      textAlign: 'center',
    },
  },
}));

const Action = ({ candidatesCount }: ActionProps) => {
  const { isSM } = useMediaQueries();
  const classes = useStyles();

  return (
    <Section>
      <SectionTitle color="secondary.contrastText">
        {`Find the skills you need.`}
      </SectionTitle>

      <Box pt={isSM ? 4 : 14}>
        <Grid
          justifyContent="center"
          direction={isSM ? 'column' : 'row'}
          spacing={isSM ? 6 : 8}
          container
        >
          <Grid item>
            <Button
              variant="contained"
              color="primary"
              fullWidth
              type="submit"
              size="large"
              href={`${paths.companyOldOnboardingForm}/form`}
            >
              <b>Sign up</b> - It’s FREE
            </Button>
          </Grid>
          <Grid className={classes.withButtonText} item>
            <Typography variant="subtitle2" color="secondary.contrastText">
              <Typography
                variant="subtitle2"
                color="primary.main"
                fontWeight={700}
                component="span"
              >
                Now Live{' '}
              </Typography>
              with{' '}
              {formatNumberSafe(candidatesCount, { fallback: INFINITY_SIGN })}
            </Typography>
            <Typography variant="subtitle2" color="secondary.contrastText">
              vetted candidates
            </Typography>
          </Grid>
        </Grid>
      </Box>
    </Section>
  );
};

export default Action;
