import Image from 'next/image';
import React from 'react';

import { Box, Grid, Hidden } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { CommonAppInfo, TalentsCountItem } from '@libs/graphql-types';
import { formatNumber, formatNumberSafe } from '@libs/helpers/format';
import { useMediaQueries } from '@libs/helpers/hooks/media-queries';
import Typography, { OuterLink } from '@libs/ui/components/typography';

import Section from '../../../components/new-landings/section';
import SectionTitle from '../../../components/new-landings/section-title';
import { INFINITY_SIGN } from '../../../utils/consts';
import SectionOverline from '../shared/SectionOverline';
import CategoriesBoxes from './CategoriesBoxes';
import NumberBox from './NumberBox';
import Quote from './quote';

interface AboutProps {
  appInfo?: Partial<CommonAppInfo>;
  counts: TalentsCountItem[];
}

const useStyles = makeStyles((theme) => ({
  title: {
    margin: '0 auto',
    paddingBottom: theme.spacing(8),
  },
  subtitle: {
    color: 'white',
    textTransform: 'uppercase',
    marginBottom: 32,
    fontSize: 20,

    [theme.breakpoints.down('md')]: {
      fontSize: 16,
    },

    [theme.breakpoints.down('sm')]: {
      whiteSpace: 'initial',
    },
  },
  firstLine: {
    paddingBottom: theme.spacing(8),

    [theme.breakpoints.down('md')]: {
      flexDirection: 'column',
      paddingBottom: theme.spacing(4),

      '& > div': {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      },
    },
  },

  middleWords: {
    lineHeight: '27px',
  },

  icons: {
    maxWidth: 860,
    margin: '0 auto',

    [theme.breakpoints.down('md')]: {
      maxWidth: '100%',
      paddingTop: 36,
      flexDirection: 'column',
    },
  },
  iconItem: {
    [theme.breakpoints.down('md')]: {
      marginBottom: 18,
      display: 'flex',
      minHeight: 50,
      maxWidth: '100%',
      alignItems: 'center',
    },

    '& > div': {
      maxWidth: '100%',
    },
    '& img': {
      maxWidth: '100%',
    },
  },

  categoriesTitle: {
    paddingBottom: theme.spacing(8),

    [theme.breakpoints.down('md')]: {
      paddingBottom: theme.spacing(4),
      paddingTop: theme.spacing(4),
    },
  },
}));

const ICONS = [
  '/hub-page/companies/amazon@2x.png',
  '/hub-page/companies/knab@2x.png',
  '/hub-page/companies/wish@2x.png',
  '/hub-page/companies/tesco@2x.png',
  '/hub-page/companies/boeing@2x.png',
  '/hub-page/companies/hsbc@2x.png',
  '/hub-page/companies/war-child@2x.png',
  '/hub-page/companies/red-bull@2x.png',
  '/hub-page/companies/barca@2x.png',
  '/hub-page/companies/nestle@2x.png',
  '/hub-page/companies/omega@2x.png',
];

const MOBILE_ICONS = [
  '/hub-page/logo-line/logo_line_5.png',
  '/hub-page/logo-line/logo_line_4.png',
  '/hub-page/logo-line/logo_line_3.png',
  '/hub-page/logo-line/logo_line_2.png',
  '/hub-page/logo-line/logo_line_1.png',
];

const About = ({ appInfo, counts }: AboutProps) => {
  const classes = useStyles();
  const { isSM } = useMediaQueries();

  return (
    <Section
    // overline={
    //   <SectionOverline color="primary" index={1}>
    //     the community
    //   </SectionOverline>
    // }
    >
      <SectionTitle className={classes.title} color="secondary.contrastText">
        Powered by community.
      </SectionTitle>

      <Typography
        className={classes.subtitle}
        textAlign="center"
        whiteSpace="break-spaces"
      >
        {`when you recruit with opentalent, you’ll join a community ready to help
you thrive. team up on deals and grow your earnings together. `}
      </Typography>

      <Box pt={6}>
        <Grid
          className={classes.firstLine}
          justifyContent="center"
          container
          spacing={isSM ? 6 : 10}
        >
          <Grid item>
            <NumberBox>
              {formatNumberSafe(appInfo?.total_ot_approved_freelancers_count, {
                fallback: INFINITY_SIGN,
              })}
            </NumberBox>
            <Typography color="other.whiteTransparent7" align="center">
              Professionals
            </Typography>
          </Grid>
          <Grid item>
            <Box pt={isSM ? 0 : 6}>
              <Typography
                color="other.whiteTransparent7"
                className={classes.middleWords}
                variant="h5"
              >
                in
              </Typography>
            </Box>
          </Grid>
          <Grid
            style={{ display: 'flex', flexDirection: 'column' }}
            alignItems="center"
            item
          >
            <NumberBox>
              {formatNumberSafe(appInfo?.total_ot_freelancers_countries_count, {
                fallback: INFINITY_SIGN,
              })}
            </NumberBox>
            <Typography color="other.whiteTransparent7" align="center">
              countries
            </Typography>
          </Grid>
          <Grid item>
            <Box pt={isSM ? 0 : 6}>
              <Typography
                color="other.whiteTransparent7"
                className={classes.middleWords}
                variant="h5"
              >
                with
              </Typography>
            </Box>
          </Grid>
          <Grid item>
            <NumberBox>
              {formatNumberSafe(appInfo?.total_unique_skills_count, {
                fallback: INFINITY_SIGN,
              })}
            </NumberBox>
            <Typography color="other.whiteTransparent7" align="center">
              unique skills
            </Typography>
          </Grid>
        </Grid>
      </Box>

      <Box className={classes.categoriesTitle}>
        <Typography color="other.whiteTransparent7" align="center">
          across 10 ‘high-skilled’ categories
        </Typography>
      </Box>

      <CategoriesBoxes counts={counts} />

      <Grid
        className={classes.firstLine}
        justifyContent="center"
        alignItems="center"
        container
        spacing={isSM ? 6 : 10}
      >
        <Grid item>
          <Typography
            className={classes.middleWords}
            color="other.whiteTransparent7"
          >
            who collectively worked at{' '}
          </Typography>
        </Grid>

        <Grid item>
          <NumberBox>
            {appInfo?.total_unique_companies_count
              ? formatNumber(appInfo.total_unique_companies_count)
              : INFINITY_SIGN}
          </NumberBox>

          <Hidden mdUp>
            <Typography align="center" color="other.whiteTransparent7">
              companies.
            </Typography>
          </Hidden>
        </Grid>
        <Hidden mdDown>
          <Grid item>
            <Typography align="center" color="other.whiteTransparent7">
              companies.
            </Typography>
          </Grid>
        </Hidden>
      </Grid>

      <Hidden mdDown>
        <Grid
          className={classes.icons}
          justifyContent="center"
          alignItems="center"
          container
          spacing={10}
        >
          {ICONS.map((icon) => (
            <Grid key={icon} item>
              <Image
                style={{ objectFit: 'contain' }}
                height={60}
                width={100}
                src={icon}
                alt="icon"
              />
            </Grid>
          ))}
        </Grid>
      </Hidden>

      <Hidden mdUp>
        <Grid
          className={classes.icons}
          justifyContent="center"
          alignItems="center"
          container
        >
          {MOBILE_ICONS.map((icon, index) => (
            <Grid className={classes.iconItem} key={icon} item>
              <div>
                <img
                  // width={320}
                  // height={index < 2 ? 40 : 60}
                  srcSet={`${icon} 2x`}
                  alt="icon"
                />
              </div>
            </Grid>
          ))}
        </Grid>
      </Hidden>

      <Box pt={12} textAlign="center">
        <OuterLink
          target="_blank"
          style={{ textDecoration: 'underline' }}
          color="primary"
          href="https://docs.google.com/spreadsheets/d/10LvhvKPRceMDNwC_6jhImU9ezmMqM93ZbyZ7jJ1QDXk/edit#gid=1057004131"
        >
          More insights about our community
        </OuterLink>
      </Box>

      {/* <Box> */}
      {/*   <Quote /> */}
      {/* </Box> */}
    </Section>
  );
};

export default About;
