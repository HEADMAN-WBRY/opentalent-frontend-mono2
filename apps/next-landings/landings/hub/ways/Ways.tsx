import React from 'react';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { CommonAppInfo } from '@libs/graphql-types';
import { useMediaQueries } from '@libs/helpers/hooks/media-queries';
import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import Section from '../../../components/new-landings/section';
import SectionTitle from '../../../components/new-landings/section-title';
import { paths } from '../../../utils/consts';
import Quote from './quote';
import WaysCards from './ways-cards';

interface WaysProps {
  appInfo?: Partial<CommonAppInfo>;
}

const useStyles = makeStyles((theme) => ({
  capture: {
    [theme.breakpoints.down('md')]: {
      textAlign: 'center',
    },
  },
}));

const Ways = ({ appInfo }: WaysProps) => {
  const { isSM } = useMediaQueries();
  const classes = useStyles();

  return (
    <Section color="white">
      <SectionTitle>3 ways to grow your&nbsp;talent&nbsp;hub</SectionTitle>

      <Box pt={10}>
        <WaysCards appInfo={appInfo} />
      </Box>
      <Box pt={14}>
        <Quote />
      </Box>

      <Box pt={16}>
        <Grid
          justifyContent="center"
          direction={isSM ? 'column' : 'row'}
          spacing={isSM ? 4 : 8}
          container
        >
          <Grid item>
            <Button
              variant="contained"
              color="secondary"
              fullWidth
              type="submit"
              size="large"
              href={paths.companyOldOnboardingForm}
            >
              <b>Start My Hub</b> - it’s FREE
            </Button>
          </Grid>

          <Grid className={classes.capture} item>
            <Typography variant="subtitle2" fontWeight={700}>
              Claim your company’s hub -
            </Typography>
            <Typography variant="subtitle2">
              <span style={{ textDecoration: 'underline' }}>15.000+ hubs</span>{' '}
              up for grabs
            </Typography>
          </Grid>
        </Grid>
      </Box>
    </Section>
  );
};

export default Ways;
