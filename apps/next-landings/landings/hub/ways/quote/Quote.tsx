import cn from 'classnames';
import React from 'react';

import { Avatar, Box, Card, CardContent, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import { ReactComponent as Comas } from '../../../../assets/comas.svg';

interface QuoteProps {}

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    maxWidth: 840,
    margin: '0 auto',
    boxShadow: '0px 8px 20px rgba(0, 0, 0, 0.12)',
    borderRadius: 7,
  },
  card: {
    width: '100%',
    boxShadow: '0px 8px 20px rgba(0, 0, 0, 0.12)',
    borderRadius: 8,
  },
  cardContent: {
    padding: theme.spacing(6),
  },
  userBlock: {
    minWidth: 240,
  },
  text: {
    maxWidth: 480,
    position: 'relative',
  },
  coma: {
    position: 'absolute',
    content: "''",
    opacity: '0.8',
  },
  comaUp: {
    top: '-9px',
    left: '-32px',

    [theme.breakpoints.down('md')]: {
      left: '-12px',
    },
  },
  comaDown: {
    bottom: '-10px',
    right: '-20px',
  },

  cardGrid: {
    wrap: 'nowrap',

    [theme.breakpoints.down('md')]: {
      wrap: 'wrap',
    },
  },
}));

const Quote = (props: QuoteProps) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Card className={classes.card} variant="elevation">
        <CardContent className={classes.cardContent}>
          <Grid
            spacing={8}
            className={classes.cardGrid}
            alignItems="center"
            container
          >
            <Grid item>
              <Grid
                className={classes.userBlock}
                wrap="nowrap"
                spacing={4}
                container
              >
                <Grid item>
                  <Avatar
                    sx={{ width: 60, height: 60 }}
                    alt="Avatar"
                    src="/hub-page/quote/avatar.png"
                  />
                </Grid>
                <Grid item>
                  <Typography lineHeight="20px">Evi van Splunder</Typography>
                  <Typography
                    lineHeight="18px"
                    variant="body2"
                    color="text.secondary"
                  >
                    Executive People Director
                  </Typography>
                  <Typography lineHeight="24px" color="text.secondary">
                    code d’azur
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
            <Grid item>
              <Box className={classes.text}>
                <Typography>
                  We&apos;ve been searching for a rockstar Digital Designer for
                  a long time, and found one through OpenTalent in a blink of an
                  eye. OpenTalent is easy to use and connects us to a whole new
                  network of talent.
                </Typography>
                <Comas className={cn(classes.coma, classes.comaUp)} />
                <Comas className={cn(classes.coma, classes.comaDown)} />
              </Box>
            </Grid>
          </Grid>
        </CardContent>
        <span />
      </Card>
    </Box>
  );
};

export default Quote;
