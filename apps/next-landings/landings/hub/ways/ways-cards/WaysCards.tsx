import React from 'react';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { CommonAppInfo } from '@libs/graphql-types';
import { formatNumberSafe } from '@libs/helpers/format';
import Typography, { OuterLink } from '@libs/ui/components/typography';

import { INFINITY_SIGN, paths } from '../../../../utils/consts';
import WayCardItem from './WayCardItem';

interface WaysCardsProps {
  appInfo?: Partial<CommonAppInfo>;
}

const getData = (appInfo?: Partial<CommonAppInfo>) => [
  {
    title: `Search INside
YOUR HUB`,
    subtitle: (
      <Typography
        fontSize={18}
        variant="h6"
        style={{ textDecoration: 'underline' }}
      >
        Get started for FREE
      </Typography>
    ),
    text: 'Invite anyone to your hub and hire people, commission-free! Post jobs and see instant matches based on key skills.',
  },
  {
    title: `subscribe to
our community`,
    text: (
      <>
        Search through{' '}
        <b>
          {formatNumberSafe(appInfo?.total_ot_freelancers_count, {
            fallback: INFINITY_SIGN,
          })}
        </b>{' '}
        profiles of vetted freelancers in the OpenTalent network and hire
        anyone, commission-free!
      </>
    ),
    subtitle: (
      <Typography
        fontSize={18}
        variant="h6"
        style={{ textDecoration: 'underline' }}
      >
        50-100 new profiles added daily
      </Typography>
    ),
  },
  {
    title: `work with
expert Recruiters`,
    text: (
      <>
        Work with our community of{' '}
        <b>
          {formatNumberSafe(appInfo?.total_ot_recruiters_count, {
            fallback: INFINITY_SIGN,
          })}{' '}
          recruiters
        </b>{' '}
        in{' '}
        <b>
          {formatNumberSafe(appInfo?.total_ot_freelancers_countries_count, {
            fallback: INFINITY_SIGN,
          })}{' '}
          countries
        </b>{' '}
        to add ‘high-skilled’ talent to your hub.
      </>
    ),
    subtitle: (
      <Typography
        fontSize={18}
        variant="h6"
        style={{ textDecoration: 'underline' }}
      >
        Available as of €55 p/h
      </Typography>
    ),
  },
];

const useStyles = makeStyles((theme) => ({
  root: {
    [theme.breakpoints.down('md')]: {
      flexDirection: 'column',
    },
  },
}));

const WaysCards = ({ appInfo }: WaysCardsProps) => {
  const classes = useStyles();

  return (
    <Grid className={classes.root} spacing={4} wrap="nowrap" container>
      {getData(appInfo).map((i, index) => (
        <Grid flexGrow={1} key={i.title} item>
          <WayCardItem index={index} title={i.title}>
            <Box display="flex" flexDirection="column" flexGrow={1}>
              <Typography
                flexGrow={1}
                paragraph
                variant="body1"
                color="text.secondary"
                component="div"
              >
                {i.text}
              </Typography>

              <Typography
                component="div"
                fontWeight={500}
                fontSize={18}
                variant="h6"
              >
                {i.subtitle}
              </Typography>
            </Box>
          </WayCardItem>
        </Grid>
      ))}
    </Grid>
  );
};

export default WaysCards;
