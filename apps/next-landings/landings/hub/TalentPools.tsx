import {
  OpenGraph,
  MainMetaTexts,
} from 'apps/next-landings/components/meta-data';
import Head from 'next/head';
import React from 'react';

import { makeStyles } from '@mui/styles';

import { TalentsCountItem } from '@libs/graphql-types';

import Footer from '../../components/common-footer';
import Header from '../../components/common-header';
import About from './about';
import Action from './action';
import Carousel from './carousel';
import { useAppInfo } from './hooks';
import Intro from './intro';
import NoWorries from './no-worries';
import Slider from './slider';
import Strategy from './strategy';
import Values from './values';
import Vatted from './vatted';
import Ways from './ways';

interface TalentPoolsProps { }

const useStyles = makeStyles((theme) => ({
  wrapper: {
    width: '100vw',
    overflow: 'hidden',

    '& .MuiButton-root': {
      borderRadius: 4,
      textTransform: 'none',
    },
  },
  '@global': {
    body: {
      width: '100%',
    },

    '.MuiButton-containedSecondary': {
      color: theme.palette.primary.main,
      transition: 'all .3s',

      '&:hover': {
        background: theme.palette.secondary.light,
      },
    },
  },
  '@font-face': {
    fontFamily: 'MissRhinetta',
    src: 'url(assets/fonts/Miss-Rhinetta.otf)',
  },

  join: {
    background: 'white',
    color: theme.palette.text.primary,

    '& button': {
      boxShadow: '0px 8px 16px rgba(0, 0, 0, 0.14) !important',
    },
  },
}));

const TalentPools = (props: TalentPoolsProps) => {
  const classes = useStyles();
  const { data } = useAppInfo();
  const counts = (data?.talentsCountByCategories || []) as TalentsCountItem[];
  const candidatesCount =
    data?.commonAppInfo?.total_ot_approved_freelancers_count;

  return (
    <div className={classes.wrapper}>
      <MainMetaTexts
        title="OpenTalent | Recruit smarter, not harder."
        description="The smarter way for in-house recruiting teams to find Europe’s top talent, commission-free!"
      />
      <OpenGraph
        title="OpenTalent | Recruit smarter, not harder."
        description="The smarter way for in-house recruiting teams to find Europe’s top talent, commission-free!"
      />
      <Head>
        <title>OpenTalent Direct</title>
      </Head>
      <Header activeItem="recruiters" />
      <Intro candidatesCount={candidatesCount} />
      <Slider />
      <About appInfo={data?.commonAppInfo} counts={counts} />
      <Carousel />
      {/* <Vatted /> */}
      {/* <NoWorries /> */}
      {/* <Ways appInfo={data?.commonAppInfo} /> */}
      {/* <Values /> */}
      {/* <Strategy /> */}
      {/* <Action candidatesCount={candidatesCount} /> */}
      <Footer />
    </div>
  );
};

export default TalentPools;
