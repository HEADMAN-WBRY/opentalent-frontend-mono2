import { paths } from 'apps/next-landings/utils/consts';
import { useSnackbar } from 'notistack';
import { useCallback } from 'react';

import {
  useCheckCompanyUserExistsByEmailLazyQuery,
  useCheckTalentExistsByEmailLazyQuery,
} from '@libs/graphql-types';
import { FormikSubmit } from '@libs/helpers/form';

import { FormData } from './types';

export const useSubmitAction = ({ jobId }: { jobId: string }) => {
  const { enqueueSnackbar } = useSnackbar();
  const [checkTalentEmail, { loading: isChackingTalent }] =
    useCheckTalentExistsByEmailLazyQuery();
  const [checkCompanyEmail, { loading: isCheckingCompany }] =
    useCheckCompanyUserExistsByEmailLazyQuery();
  const isLoading = isCheckingCompany || isChackingTalent;

  const onSubmit: FormikSubmit<FormData> = useCallback(
    async ({ email }) => {
      const redirectUrl = paths.talentJobApplication({ jobId });
      const talentOnboardingLink = `${paths.talentOnboarding}?appliedJobId=${jobId}&email=${email}`;

      const companyCheckResponse = await checkCompanyEmail({
        variables: { email },
      });

      if (companyCheckResponse.data?.checkCompanyUserExistsByEmail) {
        enqueueSnackbar('Only talents can apply to vacancies', {
          variant: 'info',
        });
        return;
      }

      const talentCheckResponse = await checkTalentEmail({
        variables: { talent_email: email },
      });

      if (!talentCheckResponse.data?.checkTalentExistsByEmail) {
        window.location.href = talentOnboardingLink;
        return;
      }

      if (typeof window !== 'undefined') {
        window.location.href = redirectUrl;
      }
    },
    [jobId, checkCompanyEmail, checkTalentEmail, enqueueSnackbar],
  );

  return { onSubmit, isLoading };
};
