import React from 'react';

import { Box, Container } from '@mui/material';

import { Job, PublicPageGetJobQuery } from '@libs/graphql-types';
import Typography from '@libs/ui/components/typography';

import FooterText from './footer-text';
import Header from './header';
import Intro from './intro';
import JobInfo from './job-info';

interface JobDetailsProps {
  job: PublicPageGetJobQuery['job'];
}

const JobDetails = (props: JobDetailsProps) => {
  const job = props.job as Job | undefined;

  if (job) {
    return (
      <>
        <Header />
        <Intro job={job} />
        <JobInfo job={job} />
        <FooterText job={job} />
      </>
    );
  }

  return (
    <>
      <Header />
      <Container component={Box} mt={8}>
        <Typography variant="h2">404</Typography>
        <Typography variant="h5">Job not found</Typography>
      </Container>
    </>
  );
};

export default JobDetails;
