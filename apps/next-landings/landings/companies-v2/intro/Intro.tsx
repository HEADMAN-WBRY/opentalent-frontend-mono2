/* eslint-disable jsx-a11y/accessible-emoji */
import {
  INFINITY_SIGN,
  UNBREAKABLE_WHITESPACE,
} from 'apps/next-landings/utils/consts';
import React from 'react';

import { Box, Container } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { formatNumberSafe } from '@libs/helpers/format';
import { useMediaQueries } from '@libs/helpers/hooks/media-queries';
import Button from '@libs/ui/components/button';
import Typography from '@libs/ui/components/typography';

import { TYPEFORM_LINK } from '../consts';
import JobsRowDeparture from './jobs-row-departure';

interface IntroProps {
  recruitersCount: number;
}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    background: theme.palette.secondary.dark,

    [theme.breakpoints.down('md')]: {
      paddingBottom: theme.spacing(20),
    },
  },
  title: {
    fontSize: 60,
    whiteSpace: 'break-spaces',

    [theme.breakpoints.down('md')]: {
      fontSize: 36,
      lineHeight: '44px',
    },
  },
  container: {
    height: 710,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing(12)} ${theme.spacing(5)} 0`,

    [theme.breakpoints.down('md')]: {
      height: 'auto',
      paddingTop: theme.spacing(6),
    },
  },
  smallTitle: {
    fontSize: 12,
    fontWeight: '700',
    opacity: 0.7,
    textTransform: 'uppercase',
    marginBottom: theme.spacing(8),
    color: 'rgba(255, 255, 255, 1)',
    textAlign: 'center',
  },
  underline: {
    display: 'inline-block',
    paddingBottom: 4,
    position: 'relative',
    borderBottom: `1px solid ${theme.palette.primary.main}`,
  },
}));

const Intro = ({ recruitersCount }: IntroProps) => {
  const classes = useStyles();
  const { isSM } = useMediaQueries();

  return (
    <div className={classes.wrapper}>
      <Container className={classes.container}>
        <Typography className={classes.smallTitle}>
          👋 OpenTalent RPO
        </Typography>
        <Box maxWidth={900} mb={4}>
          <Typography
            fontWeight={700}
            color="secondary.contrastText"
            variant="h2"
            fontStyle="italic"
            textAlign="center"
            fontSize={64}
            className={classes.title}
          >
            Unleash the power of{' '}
            <Typography
              fontStyle="italic"
              variant="h2"
              component="span"
              fontSize={64}
              color="primary.main"
              className={classes.title}
            >
              {`${formatNumberSafe(recruitersCount, {
                fallback: INFINITY_SIGN,
              })}${UNBREAKABLE_WHITESPACE}recruiters `}
            </Typography>
            {`to find Europe’s top talent in record time.`}
          </Typography>
        </Box>
        <Typography
          maxWidth={520}
          margin="0 auto"
          color="secondary.contrastText"
          textAlign="center"
          marginBottom={8}
        >
          Post a job, pick a Finder’s Fee and have Europe’s largest recruiter
          community source for you -{' '}
          <Typography className={classes.underline} component="span">
            no cure no pay!
          </Typography>
        </Typography>
        <Box
          width={isSM ? '100%' : 'auto'}
          display="flex"
          justifyContent="center"
          mb={10}
        >
          <Button
            href={TYPEFORM_LINK}
            {...{ target: '_blank' }}
            color="primary"
            variant="contained"
            size="large"
            fullWidth
          >
            <b>Post a job</b>
          </Button>
        </Box>
        <Box>
          <Typography
            component="p"
            paragraph
            color="secondary.contrastText"
            variant="captionSmall"
            textAlign="center"
          >
            Recently placed candidates:
          </Typography>

          <JobsRowDeparture />
        </Box>
      </Container>
    </div>
  );
};

export default Intro;
