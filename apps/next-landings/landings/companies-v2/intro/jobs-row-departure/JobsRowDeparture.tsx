import DepartureText from 'apps/next-landings/components/departure-text';
import React, { useState } from 'react';
import { useInterval } from 'react-use';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { getNextIndex, getPluralPostfix } from '@libs/helpers/common';
import { formatCurrency } from '@libs/helpers/format';
import { useMediaQueries } from '@libs/helpers/hooks/media-queries';
import Typography from '@libs/ui/components/typography';

import { JOB_ITEMS } from './consts';

interface JobsRowDepartureProps { }

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.secondary.light,
    padding: theme.spacing(4, 8),
    borderRadius: 6,
  },
  separator: {
    color: theme.palette.primary.main,
    fontSize: 30,
  },
}));

const JobsRowDeparture = (props: JobsRowDepartureProps) => {
  const classes = useStyles();
  const [count, setCount] = useState(0);
  const job = JOB_ITEMS[count];
  const { isSM } = useMediaQueries();

  useInterval(() => {
    setCount(getNextIndex(count, JOB_ITEMS.length));
  }, 10000);

  return (
    <Box className={classes.root}>
      <Grid
        marginBottom={-1}
        spacing={isSM ? 2 : 4}
        container
        direction={isSM ? 'column' : 'row'}
      >
        <Grid display="flex" justifyContent="center" item>
          <DepartureText text={job.title} length={20} />
        </Grid>
        <Grid display="flex" justifyContent="center" item>
          <span className={classes.separator}>·</span>
        </Grid>
        <Grid display="flex" justifyContent="center" item>
          <Typography color="secondary.contrastText" component="span">
            Found within&nbsp;
          </Typography>

          <DepartureText
            text={`${job.days}DAY${getPluralPostfix(job.days)}`}
            length={7}
          />
        </Grid>
        <Grid display="flex" justifyContent="center" item>
          <span className={classes.separator}>·</span>
        </Grid>
        <Grid display="flex" justifyContent="center" item>
          <Typography color="secondary.contrastText" component="span">
            {`Finder’s Fee:`}&nbsp;
          </Typography>

          <DepartureText text={formatCurrency(job.fee)} />
        </Grid>
      </Grid>
    </Box>
  );
};

export default JobsRowDeparture;
