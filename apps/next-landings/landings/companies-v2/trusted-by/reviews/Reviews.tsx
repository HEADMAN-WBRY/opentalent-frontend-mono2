import React from 'react';

import { Grid } from '@mui/material';

import ReviewCard from './ReviewCard';

interface ReviewsProps {}

const CARDS_CONTENT = [
  {
    title: 'Enterprise in Banking',
    text: 'Very good response to the sourcing campaigns. The two campaigns with OpenTalent did exceptionally well, and helped us to close the senior DevOps engineer roles that we were eager to fill in our team.',
  },
  {
    title: 'Scale-up in AI',
    text: 'OpenTalent has been a great solution for recruiting Python developers. We made three hires since starting, and have been able to interview dozens of qualified candidates. We are now using it on an ongoing basis and find that it helps us recruit people faster and easier.',
  },
];

const Reviews = (props: ReviewsProps) => {
  return (
    <Grid container spacing={4}>
      {CARDS_CONTENT.map((i) => (
        <Grid xs={12} md={6} key={i.title} item>
          <ReviewCard {...i} />
        </Grid>
      ))}
    </Grid>
  );
};

export default Reviews;
