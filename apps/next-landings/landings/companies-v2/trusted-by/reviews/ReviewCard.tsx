import Image from 'next/image';
import React from 'react';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import stars from './stars.svg';

interface ReviewCardProps {
  title: string;
  text: string;
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100%',
    background: 'linear-gradient(180deg, #FFFFFF 74.48%, #F2FF88 100%);',
    borderRadius: 12,
    padding: theme.spacing(5),

    '& *': {
      color: theme.palette.text.primary,
    },
  },
}));

const ReviewCard = ({ title, text }: ReviewCardProps) => {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Box mb={3}>
        <Grid container>
          <Grid flexGrow={1} item>
            <Typography variant="h6" fontWeight={600}>
              {title}
            </Typography>
          </Grid>
          <Grid item>
            <Image width={110} height={20} src={stars} alt="score" />
          </Grid>
        </Grid>
      </Box>

      <Typography>{text}</Typography>
    </Box>
  );
};

export default ReviewCard;
