import IconsRow from 'apps/next-landings/components/icons-row';
import React from 'react';

import { Box, Container } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import Reviews from './reviews';

interface HowItWorksProps { }

const ICONS = [
  '/search-page/logos-line/abn.svg',
  '/search-page/logos-line/fedex.svg',
  '/search-page/logos-line/pon.svg',
  '/search-page/logos-line/asml.svg',
  '/search-page/logos-line/aizon.svg',
  '/search-page/logos-line/code.svg',
  '/search-page/logos-line/hc.svg',
  '/search-page/logos-line/oyas.png',
  '/search-page/logos-line/productpine.svg',
  '/search-page/logos-line/thales.svg',
  '/search-page/logos-line/rabobank.svg',
  '/search-page/logos-line/visa.svg',
  '/search-page/logos-line/atos.svg',
];

const useStyles = makeStyles((theme) => ({
  title: {
    marginBottom: theme.spacing(18),

    [theme.breakpoints.down('md')]: {
      fontSize: 24,
      lineHeight: '28px',
      marginBottom: 44,
    },
  },
  wrapper: {
    padding: `${theme.spacing(18)} 0`,
    overflow: 'hidden',
    background: theme.palette.secondary.main,
    color: 'white',

    [theme.breakpoints.down('md')]: {
      padding: `48px 0 51px`,
    },
  },
  container: {
    [theme.breakpoints.down('md')]: {
      padding: `0 ${theme.spacing(5)}`,
    },
  },
}));

const HowItWorks = (props: HowItWorksProps) => {
  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <Container className={classes.container}>
        <Typography
          className={classes.title}
          style={{ display: 'block' }}
          align="center"
          variant="h4"
          component="i"
          fontWeight={700}
        >
          As used by companies - big&nbsp;and&nbsp;small
        </Typography>

        <Box mb={14}>
          <IconsRow icons={ICONS} />
        </Box>

        <Reviews />
      </Container>
    </div>
  );
};

export default HowItWorks;
