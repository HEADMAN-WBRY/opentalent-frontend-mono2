import React from 'react';

import { makeStyles } from '@mui/styles';

import { GetCompaniesLandingDataQuery } from '@libs/graphql-types';

import Footer from '../../components/common-footer';
import Header from '../../components/common-header';
import HowItWorks from './how-it-works';
import Intro from './intro';
import TrustedBy from './trusted-by';

interface CompaniesV2Props {
  data: GetCompaniesLandingDataQuery;
}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    width: '100vw',
    overflow: 'hidden',

    '& .MuiButton-root': {
      borderRadius: 4,
      textTransform: 'none',
    },
  },
}));

const CompaniesV2 = ({ data }: CompaniesV2Props) => {
  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <Header activeItem="companies" />
      <Intro recruitersCount={data.commonAppInfo.total_ot_recruiters_count} />
      <TrustedBy />
      <HowItWorks />
      <Footer />
    </div>
  );
};

export default CompaniesV2;
