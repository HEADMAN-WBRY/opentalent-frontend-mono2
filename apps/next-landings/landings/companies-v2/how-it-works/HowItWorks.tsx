import React from 'react';

import { Box, Button, Container, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import { TYPEFORM_LINK } from '../consts';
import WorkStep from './WorkStep';

interface HowItWorksProps { }

const useStyles = makeStyles((theme) => ({
  title: {
    marginBottom: theme.spacing(18),

    [theme.breakpoints.down('md')]: {
      fontSize: 20,
      lineHeight: '21px',
      marginBottom: 44,
    },
  },
  wrapper: {
    padding: `${theme.spacing(18)} 0`,
    overflow: 'hidden',
    background: '#0F0F0D',
    color: 'white',

    [theme.breakpoints.down('md')]: {
      padding: `48px 0 51px`,
    },
  },

  items: {
    maxWidth: 880,
    margin: '0 auto',

    [theme.breakpoints.down('md')]: {
      flexDirection: 'column',
      alignItems: 'center',

      '& > div': {
        marginBottom: theme.spacing(8),
      },
    },
  },

  form: {
    marginTop: theme.spacing(14),

    [theme.breakpoints.down('md')]: {
      marginTop: theme.spacing(2),
      flexDirection: 'column',
      alignItems: 'center',

      '& > button': {
        width: '100%',
      },
    },
  },

  container: {
    [theme.breakpoints.down('md')]: {
      padding: `0 ${theme.spacing(5)}`,
    },
  },

  arrow: {
    [theme.breakpoints.down('md')]: {
      display: 'none',
    },
  },
}));

const STEPS = [
  {
    title: 'Post a job',
    text: 'Create a job post on OpenTalent and pick a Finder’s Fee.',
  },

  {
    title: 'Start sourcing',
    text: 'Get our community to source for you - no cure no pay.',
  },

  {
    title: 'Get candidates',
    text: 'Within days, start getting vetted candidates - ready for interviews.',
  },
];

const HowItWorks = (props: HowItWorksProps) => {
  const classes = useStyles();
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  };

  return (
    <div className={classes.wrapper}>
      <Container className={classes.container}>
        <Typography
          className={classes.title}
          style={{ display: 'block' }}
          align="center"
          variant="h4"
          component="i"
          transform="uppercase"
          fontWeight={600}
        >
          How it works
        </Typography>

        <Grid
          className={classes.items}
          container
          justifyContent="space-between"
        >
          {STEPS.map(({ title, text }, index) => (
            <Grid key={title} item>
              <WorkStep index={index + 1} title={title} text={text} />
            </Grid>
          ))}
        </Grid>

        <Box textAlign="center" className={classes.form}>
          <Button
            href={TYPEFORM_LINK}
            color="primary"
            variant="contained"
            size="large"
            {...{ target: '_blank' }}
          >
            <b>Post a job</b>
          </Button>
        </Box>
      </Container>
    </div>
  );
};

export default HowItWorks;
