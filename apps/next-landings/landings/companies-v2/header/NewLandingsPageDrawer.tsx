import { ReactComponent as WhatsAppIcon } from 'apps/next-landings/public/header/whatsapp.svg';
import Link from 'next/link';
import React from 'react';

import { ExpandLess, ExpandMore } from '@mui/icons-material';
import CloseIcon from '@mui/icons-material/Close';
import PersonOutlineIcon from '@mui/icons-material/PersonOutline';
import {
  Box,
  Collapse,
  Drawer,
  Grid,
  IconButton,
  List,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Typography,
} from '@mui/material';
import { makeStyles } from '@mui/styles';

import { EXTERNAL_LINKS } from '@libs/helpers/consts';

import { ReactComponent as LogoIcon } from '../../../assets/opentalent_dark.svg';
import { paths, RECRUITER_PAGE_NAME } from '../../../utils/consts';

interface PageDrawerProps {
  isOpen: boolean;
  toggle: VoidFunction;
  className?: string;
}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    background: theme.palette.secondary.main,
    color: 'white',
    width: '90vw',
    maxWidth: '320px',
    padding: `${theme.spacing(4)} ${theme.spacing(5)}`,
  },
  logoWrapper: {
    color: theme.palette.primary.main,
  },
  listItem: {
    paddingLeft: 0,
    paddingRight: 0,
  },
}));

const PageDrawer: React.FC<PageDrawerProps> = ({
  isOpen,
  toggle,
  className,
}) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);

  const handleClick = () => {
    setOpen(!open);
  };

  return (
    <Drawer
      classes={{ paper: classes.wrapper, root: className }}
      anchor="left"
      open={isOpen}
      onClose={toggle}
    >
      <Grid container>
        <Grid item>
          <IconButton size="small" onClick={toggle} style={{ color: 'white' }}>
            <CloseIcon />
          </IconButton>
        </Grid>
        <Grid className={classes.logoWrapper} item component={Box} mb={-2}>
          <LogoIcon height={35} />
        </Grid>
      </Grid>

      <Box pb={8} pt={8}>
        <Typography variant="h5">Main menu</Typography>

        <List
          sx={{ width: '100%' }}
          component="nav"
          aria-labelledby="nested-list-subheader"
        >
          <ListItemButton
            LinkComponent={Link}
            className={classes.listItem}
            href={paths.talentLanding}
          >
            <ListItemText primary="COMMUNITY" />
          </ListItemButton>
          <ListItemButton className={classes.listItem} onClick={handleClick}>
            <ListItemText primary="HIRING SOLUTIONS" />
            {open ? <ExpandLess /> : <ExpandMore />}
          </ListItemButton>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              <ListItemButton href={paths.companyLanding} sx={{ ml: 2 }}>
                <ListItemText primary="OpenTalent Direct" />
              </ListItemButton>
              <ListItemButton href={paths.companiesV2} sx={{ ml: 2 }}>
                <ListItemText primary={RECRUITER_PAGE_NAME} />
              </ListItemButton>
            </List>
          </Collapse>
          <ListItemButton
            className={classes.listItem}
            href={EXTERNAL_LINKS.whatsAppForCompanies}
          >
            <ListItemText
              primary={
                <>
                  CONTACT US&nbsp;
                  <WhatsAppIcon
                    style={{
                      transform: 'translateY(3px)',
                    }}
                  />
                </>
              }
            />
          </ListItemButton>
          <ListItemButton
            LinkComponent={Link}
            className={classes.listItem}
            href={paths.mainAppRoute}
          >
            <ListItemIcon style={{ color: 'white', minWidth: '34px' }}>
              <PersonOutlineIcon />
            </ListItemIcon>
            <ListItemText primary="Sign in" />
          </ListItemButton>
        </List>
      </Box>
    </Drawer>
  );
};

export default PageDrawer;
