import { paths } from '../../../../utils/consts';

export const CARDS_DATA = [
  {
    title: <b>Community-powered Search</b>,
    text: `Use the power of 1K+ recruiters to
close ‘hard-to-fill’ roles 10X faster.`,
    color: 'green' as const,
    link: paths.communitySearch,
    footerText: (
      <>
        <b>No Cure No Pay</b> - only pay for success
      </>
    ),
  },
  {
    title: (
      <b>{`Internal
Skills Hub`}</b>
    ),
    text: `Build pipeline of people with
critical skills for on-demand use.`,
    color: 'yellow' as const,
    link: paths.companyLanding,
    footerText: <b>Get started for FREE</b>,
    isNew: true,
  },
];
