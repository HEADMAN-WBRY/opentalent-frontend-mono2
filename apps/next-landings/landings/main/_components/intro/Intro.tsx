/* eslint-disable jsx-a11y/accessible-emoji */
import React from 'react';

import { Box, Container, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import IconsRow from '../../../../components/icons-row';
import ColoredCard from './ColoredCard';
import { CARDS_DATA } from './consts';

interface IntroProps {}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    background: theme.palette.secondary.main,
    color: 'white',
    paddingTop: theme.spacing(14),
    flexGrow: 1,
    display: 'flex',

    [theme.breakpoints.down('md')]: {
      paddingTop: theme.spacing(10),
      paddingBottom: theme.spacing(10),
    },
  },
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: `0 ${theme.spacing(5)}`,

    [theme.breakpoints.down('md')]: {
      height: 'auto',
    },
  },
  title: {
    fontWeight: 600,
    fontSize: 40,
    lineHeight: '48px',
    textTransform: 'uppercase',
    fontStyle: 'italic',
    textAlign: 'center',
    marginBottom: theme.spacing(8),

    [theme.breakpoints.down('md')]: {
      fontSize: 32,
      lineHeight: '40px',
    },
  },
  cardsWrap: {
    [theme.breakpoints.down('md')]: {
      flexGrow: 1,
    },
  },
  text: {
    whiteSpace: 'break-spaces',

    [theme.breakpoints.down('sm')]: {
      whiteSpace: 'initial',
    },
  },

  iconsWrap: {
    marginBottom: theme.spacing(10),

    [theme.breakpoints.down('sm')]: {
      marginBottom: theme.spacing(4),
    },
  },
}));

const Intro = (props: IntroProps) => {
  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      <Container className={classes.container}>
        <Typography component="h1" className={classes.title}>
          Win the race for ‘hard-to-find’&nbsp;talent.
        </Typography>

        <Box mb={12}>
          <Typography
            className={classes.text}
            whiteSpace="break-spaces"
            textAlign="center"
          >
            {`Competition for top talent is fierce. OpenTalent helps hiring teams get to candidates with ‘hard-to-find’ skills
fastest, all while building internal pipeline of critical skills for on-demand, commission-free hiring.`}
          </Typography>
        </Box>

        {/* <Box mb={12}>
          <Typography className={classes.text} textAlign="center">
            What are you looking for?
          </Typography>
        </Box> */}

        <Box mb={12}>
          <Grid justifyContent="center" container spacing={4}>
            {CARDS_DATA.map((i) => (
              <Grid className={classes.cardsWrap} item key={i.text}>
                <ColoredCard {...i} />
              </Grid>
            ))}
          </Grid>
        </Box>

        <Box mb={10}>
          <Typography
            fontWeight={600}
            variant="body2"
            style={{ textTransform: 'uppercase' }}
            textAlign="center"
            fontStyle="italic"
          >
            Trusted by hiring managers in fast scaling teams
          </Typography>
        </Box>

        <Box className={classes.iconsWrap}>
          <IconsRow />
        </Box>
      </Container>
    </div>
  );
};

export default Intro;
