import { OpenGraph } from 'apps/next-landings/components/meta-data';

import Main from './_components/Main';

export default function Page() {
  return (
    <>
      <OpenGraph title="test title" description="test description" />
      <Main />
    </>
  );
}
