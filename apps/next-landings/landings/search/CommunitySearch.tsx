import {
  MainMetaTexts,
  OpenGraph,
} from 'apps/next-landings/components/meta-data';
import React from 'react';
import { useTitle } from 'react-use';

import { makeStyles } from '@mui/styles';

import { TalentsCountItem } from '@libs/graphql-types';

import Footer from '../../components/common-footer';
import Action from './action';
import Countries from './countries';
import Header from './header';
import Help from './help';
import { useAppInfo } from './hooks';
import Intro from './intro';
import SeniorRecruiter from './senior-recruiter';
import Values from './values';

interface CommunitySearchProps {}

const useStyles = makeStyles((theme) => ({
  wrapper: {
    width: '100vw',
    overflow: 'hidden',

    '& .MuiButton-root': {
      borderRadius: 4,
      textTransform: 'none',
    },
  },
  '@global': {
    body: {
      width: '100%',
    },

    '$wrapper .MuiButton-root': {
      color: theme.palette.green.light,
      transition: 'all 0.3s ease-in-out',
    },

    '$wrapper .MuiButton-containedSecondary': {
      '&:hover': {
        background: theme.palette.green.main,
        color: theme.palette.secondary.main,
      },
    },
  },
  '@font-face': {
    fontFamily: 'MissRhinetta',
    src: 'url(assets/fonts/Miss-Rhinetta.otf)',
  },

  join: {
    background: 'white',
    color: theme.palette.text.primary,

    '& button': {
      boxShadow: '0px 8px 16px rgba(0, 0, 0, 0.14) !important',
    },
  },
}));

const CommunitySearch = (props: CommunitySearchProps) => {
  const classes = useStyles();
  const { data = {} } = useAppInfo();
  const { commonAppInfo, talentsCountByCategories } = data;
  const counts = (talentsCountByCategories || []) as TalentsCountItem[];

  return (
    <div className={classes.wrapper}>
      <MainMetaTexts
        title="OpenTalent | Community-powered search"
        description="Community-powered recruitment to close ‘hard-to-fill’ roles in IT, Data, Product or Marketing faster at fixed competitive rates, hassle-free."
      />
      <OpenGraph
        title="OpenTalent | A-players, On-demand"
        description="Community-powered recruitment to close ‘hard-to-fill’ roles in IT, Data, Product or Marketing faster at fixed competitive rates, hassle-free."
      />
      <Header />
      <Intro appInfo={commonAppInfo} />
      <Countries counts={counts} appInfo={commonAppInfo} />
      <Help
        countriesCount={commonAppInfo?.total_ot_freelancers_countries_count}
      />
      <Values />
      <SeniorRecruiter />
      <Action />
      <Footer />
    </div>
  );
};

export default CommunitySearch;
