import IconsRow from 'apps/next-landings/components/icons-row';
import Image from 'next/image';
import React from 'react';

import { Container, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

interface LogosLineProps {}

const ICONS = [
  '/search-page/logos-line/abn.svg',
  '/search-page/logos-line/fedex.svg',
  '/search-page/logos-line/pon.svg',
  '/search-page/logos-line/asml.svg',
  '/search-page/logos-line/aizon.svg',
  '/search-page/logos-line/code.svg',
  '/search-page/logos-line/hc.svg',
  '/search-page/logos-line/oyas.png',
  '/search-page/logos-line/productpine.svg',
  '/search-page/logos-line/thales.svg',
  '/search-page/logos-line/rabobank.svg',
  '/search-page/logos-line/visa.svg',
  '/search-page/logos-line/atos.svg',
];

const useStyles = makeStyles((theme) => ({
  root: {
    background: theme.palette.green.light,
  },
  wrapper: {
    padding: theme.spacing(12, 0),
    flexWrap: 'nowrap',

    [theme.breakpoints.down('md')]: {
      justifyContent: 'space-between',
      padding: theme.spacing(6, 0),
      flexWrap: 'wrap',
    },
    '& > div': {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
  },
  text: {
    minWidth: 260,
    maxWidth: 260,
    marginRight: '3%',
    display: 'flex',
    alignItems: 'center',

    [theme.breakpoints.down('md')]: {
      margin: '0 auto',
      paddingBottom: theme.spacing(6),
    },

    '& > img': {
      marginRight: theme.spacing(3),
    },
  },
  logos: {
    flexGrow: 1,

    [theme.breakpoints.down('md')]: {
      maxWidth: '100%',
    },

    '& > div': {
      display: 'flex',
      alignItems: 'center',
    },
  },
  logo: {
    display: 'flex',
    alignItems: 'center',

    '&:not(:last-child)': {
      marginRight: '7%',
    },

    [theme.breakpoints.down('md')]: {
      flexGrow: 1,
    },

    '& img': {
      maxWidth: '100%',
    },
  },

  logosLine: {
    maxWidth: '100%',
  },
}));

const LogosLine = (props: LogosLineProps) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Container>
        <Grid className={classes.wrapper} container>
          <Grid className={classes.text} item>
            <Image
              src="/row-icons/european-union.svg"
              height="35"
              width="55"
              alt="European Union"
            />

            <Typography whiteSpace="break-spaces" variant="caption">
              {`Used by leading hiring
teams across Europe.`}
            </Typography>
          </Grid>
          <Grid md={8} lg={9} sm={6} item className={classes.logos}>
            <IconsRow
              className={classes.logosLine}
              icons={ICONS}
              shadowColor="green"
            />
          </Grid>
        </Grid>
      </Container>
    </div>
  );
};

export default LogosLine;
