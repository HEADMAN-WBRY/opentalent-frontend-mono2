import { green } from '@mui/material/colors';

import Typography from '@libs/ui/components/typography';

export const CARDS_DATA = [
  {
    name: 'Nicoleta',
    specialization: (
      <Typography>
        +10 years.{' '}
        <span style={{ color: green[300] }}>
          in <b>Tech</b>
        </span>
      </Typography>
    ),
    country: 'Romania',
    image: '/search-page/matchers/nicoleta@2x.png',
  },
  {
    name: 'Aleksandar',
    specialization: (
      <Typography>
        +8 years.{' '}
        <span style={{ color: green[300] }}>
          in <b>Marketing</b>
        </span>
      </Typography>
    ),
    country: 'Spain',
    image: '/search-page/matchers/aleksandar@2x.png',
  },
  {
    name: 'Brian',
    specialization: (
      <Typography>
        +7 years.{' '}
        <span style={{ color: green[300] }}>
          in <b>Crypto</b>
        </span>
      </Typography>
    ),
    country: 'Czech Republic',
    image: '/search-page/matchers/brian@2x.png',
  },
  {
    name: 'Amrin',
    specialization: (
      <Typography>
        +8 years.{' '}
        <span style={{ color: green[300] }}>
          in <b>Data</b>
        </span>
      </Typography>
    ),
    country: 'Netherlands',
    image: '/search-page/matchers/amrin@2x.png',
  },
  {
    name: 'Jane',
    specialization: (
      <Typography>
        +7 years.{' '}
        <span style={{ color: green[300] }}>
          in <b>Finance</b>
        </span>
      </Typography>
    ),
    country: 'Germany',
    image: '/search-page/matchers/jane@2x.png',
  },
];
