import React from 'react';

import { Box, Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { TalentsCountItem } from '@libs/graphql-types';
import { formatNumber } from '@libs/helpers/format';
import Typography from '@libs/ui/components/typography';

import TextBox3d from '../../../components/text-box-3d';

interface CategoriesProps {
  counts: TalentsCountItem[];
}

const useStyles = makeStyles((theme) => ({
  title: {
    textTransform: 'uppercase',
    marginBottom: theme.spacing(5),
    fontWeight: 600,
    fontStyle: 'italic',
    textAlign: 'center',

    [theme.breakpoints.down('md')]: {
      fontSize: 18,
      lineHeight: '24px',
    },

    '& i': {
      width: '100%',
      textAlign: 'center',
      display: 'block',
      padding: `0 ${theme.spacing(2)}px`,

      [theme.breakpoints.down('md')]: {
        fontSize: 20,
        lineHeight: '26px',
      },
    },
  },
  categoriesWrap: {
    margin: '0 auto',
    maxWidth: 900,
    width: `calc(100% - ${theme.spacing(8)})`,
  },
  categories: {
    padding: `${theme.spacing(4)} 0 0`,
  },
  categoriesItem: {
    marginRight: theme.spacing(2),
    marginLeft: theme.spacing(2),
    marginBottom: theme.spacing(4),
  },
  boxText: {
    color: theme.palette.secondary.main,
    background: theme.palette.secondary.contrastText,
    transform: 'inherit !important',
    cursor: 'initial !important',
  },
  boxValue: {
    background: theme.palette.green.main,
    transform: 'rotateX(-135deg) !important',
  },
}));

const Categories = ({ counts }: CategoriesProps) => {
  const classes = useStyles();

  return (
    <div>
      <Typography
        color="secondary.contrastText"
        className={classes.title}
        variant="h6"
        textTransform="uppercase"
      >
        recruitment power across {counts.length} ‘High-skilled’ categories
      </Typography>

      <Box className={classes.categoriesWrap}>
        <Grid className={classes.categories} justifyContent="center" container>
          {counts.map((i) => (
            <Grid className={classes.categoriesItem} key={i.name} item>
              <TextBox3d
                classes={{ text: classes.boxText, value: classes.boxValue }}
                text={i.name}
                value={
                  <>
                    <b>{formatNumber(i.talents_count)}</b> profiles
                  </>
                }
              />
            </Grid>
          ))}
        </Grid>
      </Box>
    </div>
  );
};

export default Categories;
