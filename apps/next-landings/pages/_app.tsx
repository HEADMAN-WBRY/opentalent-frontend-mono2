import { AppProps } from 'next/app';
import Head from 'next/head';

import { CssBaseline, StyledEngineProvider } from '@mui/material';
import { ThemeProvider } from '@mui/material/styles';

import { SnackbarProvider } from '@libs/ui/components/snackbar';
import defaultTheme from '@libs/ui/themes/v5-theme';

import ApolloProvider from '../apollo/ApolloProvider';
import { GoogleAnalytics } from '../components/google-analytics/GoogleAnalytics';

function CustomApp({ Component, pageProps }: AppProps) {
  return (
    <ApolloProvider>
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={defaultTheme}>
          <SnackbarProvider>
            <CssBaseline />
            <Head>
              <title>OpenTalent | You Global Flexible Talent Hub</title>
            </Head>
            <main className="app">
              <Component {...pageProps} />
              <GoogleAnalytics />
              <script
                id="cookieyes"
                type="text/javascript"
                src="https://cdn-cookieyes.com/client_data/b9105328906380b7b50c04bb/script.js"
              />
            </main>
          </SnackbarProvider>
        </ThemeProvider>
      </StyledEngineProvider>
    </ApolloProvider>
  );
}

export default CustomApp;
