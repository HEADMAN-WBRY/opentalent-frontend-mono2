import { apolloClient } from 'apps/next-landings/apollo';
import {
  MainMetaTexts,
  OpenGraph,
} from 'apps/next-landings/components/meta-data';

import {
  PublicPageGetJobDocument,
  PublicPageGetJobQuery,
  PublicPageGetJobQueryVariables,
} from '@libs/graphql-types';

import JobPage from '../../landings/job';

interface JobPageProps {
  job?: PublicPageGetJobQuery['job'];
}

export async function getServerSideProps(context) {
  const id = context?.params?.id;
  const data = await apolloClient.query<
    PublicPageGetJobQuery,
    PublicPageGetJobQueryVariables
  >({
    query: PublicPageGetJobDocument,
    variables: {
      id,
    },
  });

  const props: JobPageProps = {
    job: data?.data?.job,
  };

  return {
    props,
  };
}

const FinalJobPage = ({ job }: JobPageProps) => {
  const title = `Opentalent | Job: ${job?.name || 'not found'}`;
  const description =
    'Join top freelancers in Europe who seek high-quality jobs at leading companies without the burden of high fees, long payment periods and complex admin.';

  return (
    <>
      <OpenGraph
        title="OpenTalent | A-players, On-demand"
        description="Post a job, pick a Finder’s Fee and have Europe’s largest recruiter community source for you - no cure no pay!"
      />
      <MainMetaTexts
        title="OpenTalent | Post a Job"
        description="Post a job, pick a Finder’s Fee and have Europe’s largest recruiter community source for you - no cure no pay!"
      />
      <JobPage job={job} />
    </>
  );
};

export default FinalJobPage;
