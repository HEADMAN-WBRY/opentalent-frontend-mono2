import { apolloClient } from 'apps/next-landings/apollo';
import {
  MainMetaTexts,
  OpenGraph,
} from 'apps/next-landings/components/meta-data';
import { RECRUITER_PAGE_NAME } from 'apps/next-landings/utils/consts';
import Head from 'next/head';

import {
  GetCompaniesLandingDataDocument,
  GetCompaniesLandingDataQuery,
  GetCompaniesLandingDataQueryVariables,
} from '@libs/graphql-types';

import CompaniesV2 from '../../landings/companies-v2';

interface JobPageProps {
  data?: GetCompaniesLandingDataQuery;
}

export async function getServerSideProps(context) {
  const data = await apolloClient.query<
    GetCompaniesLandingDataQuery,
    GetCompaniesLandingDataQueryVariables
  >({
    query: GetCompaniesLandingDataDocument,
  });

  const props: JobPageProps = {
    data: data?.data,
  };

  return {
    props,
  };
}

const FinalJobPage = ({ data }: JobPageProps) => {
  return (
    <>
      <MainMetaTexts
        title="OpenTalent | Find talent, hassle-free!"
        description="Post a job, pick a Finder’s Fee and have Europe’s largest recruiter network source for you - No Cure No Pay!"
      />
      <OpenGraph
        title="OpenTalent | Find talent, hassle-free!"
        description="Post a job, pick a Finder’s Fee and have Europe’s largest recruiter network source for you - No Cure No Pay!"
      />
      <Head>
        <title>{RECRUITER_PAGE_NAME}</title>
      </Head>
      <CompaniesV2 data={data} />
    </>
  );
};

export default FinalJobPage;
