import { isDev } from 'apps/next-landings/utils/consts';
import React from 'react';

interface HotjarInitializerProps { }

const HJID = 2887928;
const HJSV = 6;

export const HotjarInitializer = (props: HotjarInitializerProps) => {
  if (isDev) {
    return;
  }

  return (
    <script
      dangerouslySetInnerHTML={{
        __html: `
  (function(h,o,t,j,a,r){
      h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
      h._hjSettings={hjid:${HJID},hjsv:${HJSV}};
      a=o.getElementsByTagName('head')[0];
      r=o.createElement('script');r.async=1;
      r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
      a.appendChild(r);
  })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
`,
      }}
    />
  );
};
