import Image from 'next/image';
import React from 'react';

import { Grid } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Typography from '@libs/ui/components/typography';

import { LINKS_LIST_1, LINKS_LIST_2, LINKS_LIST_3 } from './consts';
import LinkList from './shared/LinkList';

const useStyles = makeStyles((theme) => ({
  root: {
    '& > div': {
      maxWidth: 270,
    },

    [theme.breakpoints.down('md')]: {
      flexDirection: 'column',
      alignItems: 'center',
    },
  },
  list: {
    padding: 0,
    listStyle: 'none',
    marginTop: theme.spacing(6),

    '& li': {
      height: 38,
    },
  },
  logos: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',

    '& img': {
      display: 'block',
      marginLeft: 'auto',

      '&:first-child': {
        marginBottom: theme.spacing(6),

        [theme.breakpoints.down('sm')]: {
          marginBottom: 12,
        },
      },

      [theme.breakpoints.down('md')]: {
        marginLeft: 'auto',
        marginRight: 'auto',
      },
    },
  },
  section: {
    '&:first-child': {
      marginBottom: theme.spacing(4),
    },

    [theme.breakpoints.down('md')]: {
      width: 200,
      margin: '0 auto',
    },
  },
  awsBlock: {
    display: 'flex',
    alignItems: 'flex-end',
    paddingTop: theme.spacing(3),
    maxWidth: 'initial !important',
    justifyContent: 'flex-end',
    flexGrow: 1,
  },
  sectionAWS: {
    marginTop: 'auto',
    display: 'flex',

    [theme.breakpoints.down('md')]: {
      justifyContent: 'center',
    },
  },
  awsTitle: {
    fontSize: 12,
    lineHeight: '16px',
    marginLeft: theme.spacing(2),
  },
}));

const TopBlock = () => {
  const classes = useStyles();

  return (
    <Grid className={classes.root} spacing={4} container>
      <Grid xs={12} sm={3} md={3} item>
        <div className={classes.section}>
          <Typography fontWeight={500} variant="h5">
            Contact Us
          </Typography>

          <LinkList list={LINKS_LIST_1} />
        </div>
      </Grid>
      <Grid xs={12} sm={4} md={4} item>
        <div className={classes.section}>
          <Typography fontWeight={500} variant="h5">
            Company
          </Typography>

          <LinkList list={LINKS_LIST_2} />
        </div>
      </Grid>
      <Grid xs={12} sm={4} md={4} item>
        <div className={classes.section}>
          <Typography fontWeight={500} variant="h5">
            Community
          </Typography>

          <LinkList list={LINKS_LIST_3} />
        </div>
      </Grid>
      <Grid className={classes.logos} xs={12} sm={3} md={2} item>
        <Image height={70} width={70} src={`/footer/gdpr.png`} alt="GDPR" />
        <Image
          height={60}
          width={187}
          src={`/footer/opentalent.png`}
          alt="Opentalnet"
        />
      </Grid>
    </Grid>
  );
};

export default TopBlock;
