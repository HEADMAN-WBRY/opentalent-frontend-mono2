import InstagramIcon from '@mui/icons-material/Instagram';
import FacebookOutlinedIcon from '@mui/icons-material/FacebookOutlined';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import MailOutlineIcon from '@mui/icons-material/MailOutline';

import { EXTERNAL_LINKS } from '@libs/helpers/consts';

import {
  LANDINGS_EXTERNAL_LINKS,
  paths,
  RECRUITER_PAGE_NAME,
} from '../../utils/consts';

export const LINKS_LIST_1 = [
  {
    link: 'mailto:hello@opentalent.co',
    text: 'hello@opentalent.co',
    Icon: MailOutlineIcon,
  },
  {
    link: 'https://www.linkedin.com/company/worklikeneverbefore/',
    text: 'LinkedIn',
    Icon: LinkedInIcon,
  },
  {
    link: 'https://www.instagram.com/opentalent.co/',
    text: 'Instagram',
    Icon: InstagramIcon,
  },
  {
    link: 'https://www.facebook.com/joinopentalent',
    text: 'Facebook',
    Icon: FacebookOutlinedIcon,
  },
];

export const LINKS_LIST_2 = [
  // {
  //   link: 'https://app.opentalent.co/quick-auth',
  //   text: 'Sign In',
  //   Icon: undefined,
  // },
  {
    link: 'https://www.notion.so/opentalent/Where-Talent-Come-To-Thrive-de1d9f55a05a47b6b8f9d076187fbcbe',
    text: 'About Us',
    Icon: undefined,
  },
  {
    link: 'https://www.notion.so/opentalent/Resource-Centre-2ed1aa571b2341418527ea16a81e01ab',
    text: 'Resource Center',
    Icon: undefined,
  },
  {
    link: 'https://form.typeform.com/to/ZO6tFWQk',
    text: 'Direct Sourcing Scan',
    Icon: undefined,
  },
  {
    link: paths.companyLanding,
    text: 'OpenTalent Direct',
    Icon: undefined,
  },
  {
    link: paths.companiesV2,
    text: RECRUITER_PAGE_NAME,
    Icon: undefined,
  },
  // {
  //   link: EXTERNAL_LINKS.pricing,
  //   text: 'Pricing',
  //   Icon: undefined,
  // },
  {
    link: EXTERNAL_LINKS.privacyPolicy,
    text: 'Privacy & Cookie Policy',
    Icon: undefined,
  },
];

export const LINKS_LIST_3 = [
  {
    link: paths.talentLanding,
    text: 'Join us',
    Icon: undefined,
  },
  {
    link: 'https://www.notion.so/opentalent/Community-Guidelines-8297d78bd5664e8384876d516488879d',
    text: 'Guidelines',
    Icon: undefined,
  },
  {
    link: LANDINGS_EXTERNAL_LINKS.statistics,
    text: 'Key statistics',
    Icon: undefined,
  },
  {
    link: 'https://opentalent.notion.site/Talent-Matcher-Program-aa44498d2ac4437493503bca237e9cab?pvs=4',
    text: 'Talent Matchers',
    Icon: undefined,
  },
  {
    link: 'https://opentalent.notion.site/Talent-Partner-Program-e72b450ee69d4de3b343bbc192096141?pvs=4',
    text: 'Talent Partners',
    Icon: undefined,
  },
];
