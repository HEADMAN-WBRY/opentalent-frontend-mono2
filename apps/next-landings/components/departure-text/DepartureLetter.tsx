import cn from 'classnames';
import React from 'react';
import { usePrevious } from 'react-use';

import { makeStyles } from '@mui/styles';

import { useLetterChange } from './hooks';

interface DepartureLetterProps {
  letter: string;
}

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    height: 23,
    width: 16,
    color: theme.palette.primary.main,
    fontWeight: 600,
    background: '#232321',
    boxShadow:
      'inset 1px -1px 0px rgba(255, 255, 255, 0.15), inset -1px 1px 0px rgba(0, 0, 0, 0.4)',

    '&::after': {
      content: '""',
      width: '100%',
      height: 1,
      background: '#181818',
      position: 'absolute',
      top: 'calc(50%)',
      left: 0,
    },

    '&:not(:last-child)': {
      marginRight: 2,
    },
  },
  '@keyframes zoomIn': {
    from: {
      transform: 'scaleY(0)',
    },

    to: {
      transform: 'scaleY(1)',
    },
  },
  '@keyframes zoomInHalf': {
    from: {
      transform: 'scaleY(0)',
      zoom: 1.4,
    },

    to: {
      transform: 'scaleY(1)',
      zoom: 1,
    },
  },
  letter: {
    position: 'absolute',
    left: 0,
    width: '100%',
    height: '50%',
    overflow: 'hidden',
    textAlign: 'center',
    textTransform: 'uppercase',
    lineHeight: '23px',
    fontSize: 16,
  },
  animate: {},
  topLetter: {
    top: 0,
    animation: '$zoomIn 0.3s ease-in-out',
  },
  bottomLetter: {
    bottom: 0,
    animation: '$zoomInHalf 0.3s ease-in-out',

    '& span': {
      transform: 'translateY(-11px)',
      display: 'inline-block',
    },
  },
}));

const DepartureLetter = ({ letter }: DepartureLetterProps) => {
  const classes = useStyles();
  const tmpLetter = useLetterChange(letter, { isEnabled: !!letter.trim() });
  const prevLetter = usePrevious(tmpLetter);

  return (
    <div key={tmpLetter} className={classes.root}>
      <div
        className={cn(classes.letter, classes.topLetter, {
          [classes.animate]: prevLetter !== tmpLetter,
        })}
      >
        {tmpLetter}
      </div>
      <div className={cn(classes.letter, classes.bottomLetter)}>
        <span>{tmpLetter}</span>
      </div>
    </div>
  );
};

export default DepartureLetter;
