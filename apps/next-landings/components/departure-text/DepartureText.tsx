import React from 'react';

import { makeStyles } from '@mui/styles';

import DepartureLetter from './DepartureLetter';

interface DepartureTextProps {
  text: string;
  length?: number;
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'nowrap',
  },
}));

const DepartureText = ({ text, length }: DepartureTextProps) => {
  const classes = useStyles();
  const finalText = text.padEnd(length || text.length, ' ');

  return (
    <div className={classes.root}>
      {finalText.split('').map((letter, index) => (
        <DepartureLetter key={letter + index} letter={letter} />
      ))}
    </div>
  );
};

export default DepartureText;
