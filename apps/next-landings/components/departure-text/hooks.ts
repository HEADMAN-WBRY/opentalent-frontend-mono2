import React from 'react';
import { useBoolean, useInterval, useTimeoutFn } from 'react-use';

const STEP_MS = 300;
const LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.,':()&!?+-";

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min);
}

export const useLetterChange = (
  letter: string,
  { delay = 4000, isEnabled }: { delay?: number; isEnabled?: boolean } = {},
) => {
  const [count, setCount] = React.useState(0);
  const [isRunning, toggleIsRunning] = useBoolean(false);
  const stepOffset = getRandomInt(0, 400);
  const finalStepDuration = stepOffset + STEP_MS;

  useInterval(
    () => {
      if (delay < count) {
        toggleIsRunning(false);
        return;
      }
      setCount(count + finalStepDuration);
    },
    isRunning ? finalStepDuration : null,
  );

  const [isReady] = useTimeoutFn(() => {
    if (isEnabled && typeof window !== 'undefined') {
      toggleIsRunning(true);
    }
  }, 1000);

  const finalLetter = !isReady()
    ? ''
    : isRunning
    ? LETTERS[getRandomInt(0, LETTERS.length - 1)]
    : letter;

  return finalLetter;
};
