import { setupGoogleStuff } from 'apps/next-landings/utils/google';
import React, { useEffect } from 'react';

interface GoogleAnalyticsProps { }

export const GoogleAnalytics = (props: GoogleAnalyticsProps) => {
  useEffect(() => {
    if (window.location.host === 'opentalent.co') {
      setupGoogleStuff();
    }
  }, []);
  return <></>;
};
