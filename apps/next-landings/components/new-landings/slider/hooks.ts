import { useCallback, useEffect, useRef, useState } from 'react';

import { getNextIndex } from '@libs/helpers/common/moveToIndex';

const TIMEOUT = 7000;

export const useAutoChangeSlider = ({
  setSlide,
  slidesCount,
  currentSlide,
}: {
  setSlide: (i: number) => void;
  slidesCount: number;
  currentSlide: number;
}) => {
  const countRef = useRef<number>(currentSlide);
  const [intervalId, setIntervalId] = useState<number | null>(null);
  const wrappedChangeSlide = useCallback(
    (num: number) => {
      setIntervalId((int) => {
        if (int) {
          clearInterval(int);
        }
        return null;
      });
      countRef.current = num;
      setSlide(num);
    },
    [setSlide],
  );

  useEffect(() => {
    if (intervalId) {
      return;
    }
    const newIntervalId = setInterval(() => {
      const nextIndex = getNextIndex(countRef.current, slidesCount);
      countRef.current = nextIndex;
      setSlide(nextIndex);
    }, TIMEOUT);
    setIntervalId(newIntervalId as any);

    // eslint-disable-next-line consistent-return
    return () => {
      if (intervalId) {
        clearInterval(intervalId);
      }
    };
  }, [currentSlide, intervalId, setSlide, slidesCount]);

  return { wrappedChangeSlide };
};
