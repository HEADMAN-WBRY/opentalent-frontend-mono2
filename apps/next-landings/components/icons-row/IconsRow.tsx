import cn from 'classnames';
import Image from 'next/image';
import React from 'react';

import { Grid, Theme } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { useMediaQueries } from '@libs/helpers/hooks/media-queries';

interface IconsProps {
  shadowColor?: 'deepblack' | 'black' | 'green';
  className?: string;
  icons?: string[];
}

export const ICONS = [
  '/row-icons/abn.file.png',
  '/row-icons/cde.svg',
  '/row-icons/hc.svg',
  '/row-icons/productpine.svg',
  '/row-icons/fedex.svg',
  '/row-icons/pon.svg',
  '/row-icons/asml.svg',
  '/row-icons/aizon.svg',
  '/row-icons/thales.svg',
  '/row-icons/oyas.png',
];

const getColor = (color: IconsProps['shadowColor'], theme: Theme) => {
  switch (color) {
    case 'green':
      return theme.palette.green.light;
    case 'deepblack':
      return '#000';
    default:
      return theme.palette.secondary.main;
  }
};

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'relative',
    overflow: 'hidden',
    maxWidth: 1000,
    margin: '0 auto',

    '&::before': {
      content: '""',
      position: 'absolute',
      left: 0,
      top: 0,
      height: '100%',
      width: 30,
      zIndex: 2,
      background: ({ shadowColor }: IconsProps) =>
        `linear-gradient(to right, ${getColor(
          shadowColor,
          theme,
        )}, transparent)`,
    },

    '&::after': {
      zIndex: 2,
      content: '""',
      position: 'absolute',
      right: 0,
      top: 0,
      height: '100%',
      width: 30,
      background: ({ shadowColor }: IconsProps) =>
        `linear-gradient(to left, ${getColor(
          shadowColor,
          theme,
        )}, transparent)`,
    },
  },
  '@keyframes slideRight': {
    from: { transform: 'translateX(0px)' },
    to: { transform: 'translateX(-9000px)' },
  },
  grid: {
    flexWrap: 'nowrap',
    animationName: '$slideRight',
    animationDuration: '240s',
    animationIterationCount: 'infinite',
    animationTimingFunction: 'linear',

    '&::-webkit-scrollbar': {
      display: 'none',
    },
    '-ms-overflow-style': 'none',
    scrollbarWidth: 'none',
  },
}));

const IconsRow = (props: IconsProps) => {
  const { className, icons = ICONS } = props;
  const classes = useStyles(props);
  const { isMD } = useMediaQueries();

  return (
    <div className={cn(classes.root, className)}>
      <Grid
        className={classes.grid}
        alignItems="center"
        spacing={isMD ? 8 : 20}
        container
      >
        {Array.from({ length: 20 }, () => icons)
          .flat()
          .map((icon, index) => (
            <Grid key={index} item>
              <Image
                style={{ objectFit: 'contain' }}
                width={110}
                height={50}
                src={icon}
                alt="icon"
              />
            </Grid>
          ))}
      </Grid>
    </div>
  );
};

export default IconsRow;
