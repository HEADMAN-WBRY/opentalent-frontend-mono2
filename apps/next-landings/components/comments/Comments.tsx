import cn from 'classnames';
import React from 'react';

import { Box } from '@mui/material';
import { makeStyles } from '@mui/styles';

import { useMediaQueries } from '@libs/helpers/hooks/media-queries';
import Typography from '@libs/ui/components/typography';

import { ReactComponent as Comas } from '../../assets/comas.svg';

const useStyles = makeStyles((theme) => ({
  wrapper: {
    padding: '100px 0 130px 0',
    textAlign: 'center',

    [theme.breakpoints.down('md')]: {
      padding: '40px 24px 48px',
    },
  },
  blockTitle: {
    lineHeight: '40px',
    fontWeight: 600,
    fontStyle: 'italic',
    marginBottom: theme.spacing(15),

    [theme.breakpoints.down('md')]: {
      marginBottom: 34,
      fontSize: '20px',
    },
  },
  item: {
    maxWidth: '370px',
    width: '100%',

    '&:last-child': {
      marginLeft: theme.spacing(40),
      marginBottom: 0,
    },

    [theme.breakpoints.down('md')]: {
      marginBottom: theme.spacing(15),
      maxWidth: 'unset',

      '&:last-child': {
        marginLeft: 0,
      },
    },

    [theme.breakpoints.down('sm')]: {
      marginBottom: 42,
    },
  },
  memberCard: {
    display: 'flex',
    padding: theme.spacing(4),
    marginBottom: theme.spacing(10),
    boxShadow: theme.shadows[7],
    borderRadius: '8px',
  },
  mamberInfo: {
    marginLeft: theme.spacing(6),
    marginTop: theme.spacing(1),
  },
  name: {
    textAlign: 'left',
    fontSize: '18px',
    lineHeight: '22px',
    fontWeight: 500,
    marginBottom: theme.spacing(1),
  },
  position: {
    color: theme.palette.grey[500],
    lineHeight: '19px',
    textAlign: 'left',
  },
  company: {
    marginTop: theme.spacing(1),
    color: theme.palette.grey[500],
    textAlign: 'left',
    lineHeight: '24px',
  },
  text: {
    whiteSpace: 'pre-line',
    textAlign: 'justify',
    fontSize: '18px',
    lineHeight: '25px',
    fontWeight: 500,
    position: 'relative',

    [theme.breakpoints.down('sm')]: {
      fontSize: 16,
      lineHeight: '22px',
    },
  },
  textCard: {
    position: 'relative',

    '& > p': {
      [theme.breakpoints.down('md')]: {
        textAlign: 'center',
      },
    },
  },
  coma: {
    position: 'absolute',
    content: "''",
    opacity: '0.8',
  },
  comaUp: {
    top: '-9px',
    left: '-26px',

    [theme.breakpoints.down('md')]: {
      top: '-9px',
      left: '-8px',
    },
  },
  comaDown: {
    bottom: '-10px',
    right: '-26px',

    [theme.breakpoints.down('md')]: {
      bottom: '-8px',
      right: '0',
    },
  },
  avatar: {
    width: '64px',
    height: '64px',
  },
}));

interface Comment {
  id: number;
  avatarSrc: string;
  name: string;
  position: string;
  company?: string;
  text: string;
}

interface IProps {
  title: string;
  comments: Comment[];
}

const Comments: React.FC<IProps> = ({ title, comments }) => {
  const classes = useStyles();
  const { isSM } = useMediaQueries();

  return (
    <Box className={classes.wrapper}>
      <Typography
        transform="uppercase"
        variant="h4"
        className={classes.blockTitle}
      >
        {title}
      </Typography>
      <Box
        display="flex"
        justifyContent="center"
        flexDirection={isSM ? 'column' : undefined}
      >
        {comments.map(({ avatarSrc, ...comment }) => (
          <Box key={comment.id} className={classes.item}>
            <Box className={classes.memberCard}>
              <img
                srcSet={`${avatarSrc} 2x`}
                alt="icon"
                className={classes.avatar}
              />
              <Box className={classes.mamberInfo}>
                <Typography variant="h6" className={classes.name}>
                  {comment.name}
                </Typography>
                <Typography variant="subtitle1" className={classes.position}>
                  {comment.position}
                </Typography>
                {comment.company && (
                  <Typography variant="h6" className={classes.company}>
                    {comment.company}
                  </Typography>
                )}
              </Box>
            </Box>
            <Box className={classes.textCard}>
              <Comas className={cn(classes.coma, classes.comaUp)} />
              <Comas className={cn(classes.coma, classes.comaDown)} />
              <Typography variant="body1" className={classes.text}>
                {comment.text}
              </Typography>
            </Box>
          </Box>
        ))}
      </Box>
    </Box>
  );
};

export default Comments;
