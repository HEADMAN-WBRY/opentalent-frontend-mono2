import Link from 'next/link';
import React from 'react';

import ExpandLessIcon from '@mui/icons-material/ExpandLess';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Menu, MenuItem } from '@mui/material';
import { makeStyles } from '@mui/styles';

import Button from '@libs/ui/components/button';

import { paths, RECRUITER_PAGE_NAME } from '../../utils/consts';

interface SolutionsMenuProps { }

const useStyles = makeStyles((theme) => ({
  list: {
    background: theme.palette.secondary.main,
    color: 'white',
  },
  listItem: {
    '&:hover': {
      background: ' rgba(242, 255, 136, 0.08)',
    },
  },
}));

const SolutionsMenu = (props: SolutionsMenuProps) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <Button
        id="basic-button"
        color="inherit"
        aria-controls={open ? 'basic-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick as any}
        endIcon={open ? <ExpandLessIcon /> : <ExpandMoreIcon />}
      >
        HIRING SOLUTIONS
      </Button>
      <Menu
        classes={{ paper: classes.list }}
        id="basic-menu"
        color="primary"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button',
        }}
      >
        {/* <MenuItem */}
        {/*   component={Link} */}
        {/*   href={paths.companyLanding} */}
        {/*   className={classes.listItem} */}
        {/* > */}
        {/*   OpenTalent Direct */}
        {/* </MenuItem> */}
        <MenuItem
          component={Link}
          href={paths.companiesV2}
          className={classes.listItem}
        >
          {RECRUITER_PAGE_NAME}
        </MenuItem>
      </Menu>
    </div>
  );
};

export default SolutionsMenu;
