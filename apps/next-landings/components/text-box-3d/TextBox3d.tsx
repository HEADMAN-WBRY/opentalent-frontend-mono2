import cn from 'classnames';
import React from 'react';

import { makeStyles } from '@mui/styles';

interface TextBox3dProps {
  text: string;
  value: React.ReactNode;
  classes?: {
    root?: string;
    text?: string;
    value?: string;
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: 32,
    lineHeight: '32px',
    position: 'relative',
    perspective: '300px',
    transformStyle: 'preserve-3d',
    cursor: 'pointer',

    '&:hover': {
      '& $text': {
        transform: 'translateY(-32px) rotateX(90deg)',
      },
      '& $value': {
        transform: 'translateY(-32px) rotateX(0deg)',
      },
    },
  },

  containers: {
    width: '100%',
    height: '100%',
    padding: '0 12px',
    textAlign: 'center',
    transformStyle: 'inherit',
    transition: 'all .5s',
    backfaceVisibility: 'hidden',
  },
  text: {
    background: 'black',
    color: 'white',
    transformOrigin: 'bottom',
    transform: 'rotateX(0deg)',
  },
  value: {
    background: theme.palette.primary.main,
    color: 'black',
    transform: 'rotateX(-135deg)',
    transformOrigin: 'top',
  },
}));

const TextBox3d = ({ classes, text, value }: TextBox3dProps) => {
  const styleClasses = useStyles();

  return (
    <div className={cn(classes?.root, styleClasses.root)}>
      <div
        className={cn(
          styleClasses.text,
          styleClasses.containers,
          classes?.text,
        )}
      >
        {text}
      </div>
      <div
        className={cn(
          styleClasses.value,
          styleClasses.containers,
          classes?.value,
        )}
      >
        {value}
      </div>
    </div>
  );
};

export default TextBox3d;
