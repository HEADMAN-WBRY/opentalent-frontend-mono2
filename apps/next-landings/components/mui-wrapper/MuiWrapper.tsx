import { CssBaseline, StyledEngineProvider } from '@mui/material';
import { ThemeProvider } from '@mui/material/styles';

import { SnackbarProvider } from '@libs/ui/components/snackbar';
import defaultTheme from '@libs/ui/themes/v5-theme';

interface MuiWrapperProps extends React.PropsWithChildren<unknown> {}

const MuiWrapper = ({ children }: MuiWrapperProps) => {
  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={defaultTheme}>
        <SnackbarProvider>
          <CssBaseline />
          {children}
        </SnackbarProvider>
      </ThemeProvider>
    </StyledEngineProvider>
  );
};

export default MuiWrapper;
