import Head from 'next/head';
import React from 'react';

interface MainMetaTextsProps {
  title: string;
  description: string;
}

export const MainMetaTexts = ({ title, description }: MainMetaTextsProps) => {
  return (
    <Head>
      <title>{title}</title>
      <meta name="description" content={description} />
    </Head>
  );
};
