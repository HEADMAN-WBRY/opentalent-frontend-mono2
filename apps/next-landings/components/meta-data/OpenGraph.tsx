import Head from 'next/head';

interface OpenGraphProps {
  title: string;
  description: string;
  image?: string;
}

const DEFAULT_OPEN_GRAPH_IMAGE =
  'https://main.ottest.co/images/14bdef10-68ce-41ad-8c78-5a5de7894ef9.jpeg';

export function OpenGraph({
  title,
  description,
  image = DEFAULT_OPEN_GRAPH_IMAGE,
}: OpenGraphProps) {
  return (
    <Head>
      <meta property="og:title" content={title} />
      <meta property="og:description" content={description} />
      <meta property="og:image" content={image} />
    </Head>
  );
}
