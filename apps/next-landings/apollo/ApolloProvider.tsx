/* eslint-disable no-console */
import {
  ApolloClient,
  ApolloProvider as BaseApolloProvider,
  from,
  HttpLink,
} from '@apollo/client';
import { onError } from '@apollo/client/link/error';
import React from 'react';

import { isDev } from '../utils/consts';
import cache from './cache';

const errorLink = onError(
  ({ graphQLErrors, networkError, operation, ...rest }) => {
    if (graphQLErrors) {
      graphQLErrors.forEach((error) => {
        const { message, locations, path } = error;

        if (typeof window !== 'undefined') {
          window.alert(message);
        }

        console.error(
          `[GraphQL error]: Message: ${message}, Location: ${JSON.stringify(
            locations,
          )}, Path: ${path}`,
        );
      });
    }

    if (networkError) {
      console.error(`[Network error]: ${networkError}`);
      if ((networkError as any).statusCode === 401) {
        window.confirm(
          `Something went wrong with network, Error: ${networkError}`,
        );
      }
    }
  },
);

const API_URL = isDev
  ? 'https://otapi.wwweberry.com/graphql2'
  : 'https://main.ottest.co/graphql2';

export const apolloClient = new ApolloClient({
  link: from([errorLink, new HttpLink({ uri: API_URL })]),
  cache,
  connectToDevTools: isDev,
});

const ApolloProvider = ({
  children,
}: React.PropsWithChildren<Record<string, unknown>>) => {
  return (
    <BaseApolloProvider client={apolloClient}>{children}</BaseApolloProvider>
  );
};

export default ApolloProvider;
